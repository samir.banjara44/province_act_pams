@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        समूह
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('samuha.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Create</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>Name</th>
              <th>Sewa</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($samuhas as $samuha)
              <tr>
                <td>{{$samuha->name}}</td>
                <td>{{$samuha->sewa_id}}</td>
                <td>
                  <a type="button" href="{{route('samuha.edit',$samuha->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection