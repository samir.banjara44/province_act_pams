<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BudgetDetails extends Model
{
    protected $table = 'budget_details';
    protected $guarded = [];


    public function budgetArea()
    {
        return $this->belongsTo('App\Models\Area', 'area', 'id');
    }

    public function expense_head_by_id()
    {

        return $this->hasOne('App\Models\ExpenseHead', 'id', 'expense_head_id');
    }

    public function voucher_details()
    {

        return $this->hasOne('App\Models\VoucherDetail', 'main_activity_id', 'id');
    }

    public function get_source_type()
    {

        return $this->hasOne('App\Models\SourceType', 'id', 'source_type');
    }

    public function get_budget_source_level()
    {

        return $this->hasOne('App\Models\BudgetSourceLevel', 'id', 'source_level');
    }

    public function get_source()
    {

        return $this->hasOne('App\Models\Source', 'id', 'source');
    }



    public function get_medium()
    {
        return $this->hasOne('App\Models\Medium', 'id', 'medium');
    }

    public function budget(){
        return $this->belongsTo('App\Models\Budget');
    }

    public function firstChaumasikExpense($office_id, $budgetSubHead, $activityId)
    {

        return $expenseOnActivity = VoucherDetail::
        join('budget_details', 'voucher_details.main_activity_id', '=', 'budget_details.id')
            ->join('budget', 'budget_details.budget_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
//            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budgetSubHead)
            ->where('budget.id', '=', $activityId)
            ->whereBetween('voucher_details.month', array(1, 4))
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', [1, 4])
            ->sum('voucher_details.dr_amount');
    }

    public function secondChaumasikExpense($office_id, $budgetSubHead, $activityId)
    {

        return $expenseOnActivity = VoucherDetail::
        join('budget_details', 'voucher_details.main_activity_id', '=', 'budget_details.id')
            ->join('budget', 'budget_details.budget_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
//            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
//            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budgetSubHead)
            ->where('budget.id', '=', $activityId)
            ->whereBetween('voucher_details.month', array(5, 8))
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', [1, 4])
            ->sum('voucher_details.dr_amount');
    }

    public function thirdChaumasikExpense($office_id, $budgetSubHead, $activityId)
    {

        return $expenseOnActivity = VoucherDetail::
        join('budget_details', 'voucher_details.main_activity_id', '=', 'budget_details.id')
            ->join('budget', 'budget_details.budget_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
//            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
//            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budgetSubHead)
            ->where('budget.id', '=', $activityId)
            ->whereBetween('voucher_details.month', array(9, 12))
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', [1, 4])
            ->sum('voucher_details.dr_amount');
    }


    public function totalExpenseByActivity($office_id, $budgetSubHead, $activityId)
    {

        $firstChaumasikExpense = $this->firstChaumasikExpense($office_id, $budgetSubHead, $activityId);
        $secondChaumasikExpense = $this->secondChaumasikExpense($office_id, $budgetSubHead, $activityId);
        $thirdChaumasikExpense = $this->thirdChaumasikExpense($office_id, $budgetSubHead, $activityId);
        return $totalExpnes = (float)$firstChaumasikExpense + (float)$secondChaumasikExpense + (float)$thirdChaumasikExpense;
    }

    public function getActivityExpense(){
        return $expens = ActivityReport::where('activity_id',$this->id)->sum('expense');
    }

    public function getActivityRemain(){
        return $expens = ActivityReport::where('activity_id',$this->id)->sum('remain');

    }

}
