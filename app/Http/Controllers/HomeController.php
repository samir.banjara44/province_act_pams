<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    private $homeRepo;

    public function __construct(HomeRepository $homeRepo)
    {

        $this->middleware('auth');
        //         $this->middleware('ifSuperadmin');
        $this->homeRepo = $homeRepo;
    }

    public function index()
    {
        return view('frontend.dashboard');
    }

    public function admin()
    {
        return view('backend.admin_dashboard');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
