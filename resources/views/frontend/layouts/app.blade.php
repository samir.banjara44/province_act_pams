<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>PAMS | @yield('title')</title>
    
    <link rel="stylesheet" href="{{ asset('fontawesome5/css/all.min.css') }}">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/nepali-datepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        .alert-danger {
            background-color: #fa5c5c;
            border-color: #ebccd1;
            color: #ffffff;
        }
        .alert {
            text-align: center;
            border: 1px solid transparent;
            border-radius: 4px;
            margin: 0;
            padding: 3px;
        }
    </style>

    @yield('styles')

</head>
<body>
    @include('frontend.layouts.header')
    @yield('content')
    @include('frontend.layouts.footer')

    <link href="{{ asset('select2.4.0.7/css/select2.min.css')  }}" rel="stylesheet" />
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/nepali-datepicker.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>


    <script src="{{  asset('select2.4.0.7/js/select2.min.js')  }}"></script>
    <script src="{{ asset('sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    
    <script>
        // Global Date Converter
        $(document).ready(function () {
            
            // Nepali number date to strng
            
            $('.bs-n-t').each(function () {
                let num_date = $.trim($(this).text());
                num_date = num_date.split("-");
                if (num_date.length < 2) {
                    num_date[2] = 1
                }
                if (num_date.length < 1) {
                    num_date[1] = 1
                }
                let formattedNepaliDate = calendarFunctions.bsDateFormat("%M %d, %y", parseInt(num_date[0]), parseInt(num_date[1]), parseInt(num_date[2]));
                $(this).text(formattedNepaliDate);
            });
            $('.bsm-n-t').each(function () {
                let num_date = $.trim($(this).text());
                
                let formattedNepaliDate = calendarFunctions.bsDateFormat("%M", 2075, parseInt(num_date), 1);
                $(this).text(formattedNepaliDate);
            });
        });
    </script>
    
    {{-- nepali number --}}

    <script>
        let changeToNepali = function (text) {
            let numbers = text.split('');
            let nepaliNo = '';
            $.each(numbers, function (key, value) {
                if (value) {
                    if(value == 0)
                    nepaliNo+="०";
                    else if(value == 1)
                    nepaliNo+="१";
                    else if(value == 2)
                    nepaliNo+="२";
                    else if(value == 3)
                    nepaliNo+="३";
                    else if(value == 4)
                    nepaliNo+="४";
                    else if(value == 5)
                    nepaliNo+="५";
                    else if(value == 6)
                    nepaliNo+="६";
                    else if(value == 7)
                    nepaliNo+="७";
                    else if(value == 8)
                    nepaliNo+="८";
                    else if(value == 9)
                    nepaliNo+="०";
                }
            });
            console.log(nepaliNo);
            return nepaliNo;
        };
        
        $('.e-n-t-n-n').each(function () {
            let nepaliNo = changeToNepali($(this).text());
            $(this).text(nepaliNo);
            let nepaliVal = changeToNepali($(this).val());
            $(this).val(nepaliVal);
        });
    </script>

    @yield('scripts')

    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $('#errorMessage').fadeOut(5000);
        });
    </script>

<script>

</script>

</body>
</html>
