
@extends('frontend.layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <i class="fas fa-users"></i> पेश्की / भुक्तानी पाउने कर्मचारी खाता विवरण </h1>
        
        <button type="button" class="btn btn-primary btn-sm btn-show-form" id="showAddForm"><i class="fas fa-plus"></i> नयाँ थप गर्ने</button>
        
        {{-- <ul class="breadcrumb">
            <li> <a type="button" href="{{route('karmachari.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> नयां बनाउने</a></li>
        </ul> --}}
    </section>
    
    <!-- FORM section start -->
    <section class="formSection displayNone" id="showThisForm">
        <div class="row justify-content-md-center">
            <div class="col-md-10 col-md-offset-1 form-bg karmachari_modal">
                
                <button type="button" class="btn btn-danger btn-sm btn-show-form displayNone" id="closeAddForm"><i class="fas fa-times"></i> फर्म रद्ध गर्ने</button>
                
                <div class="form-title">पेश्की / भुक्तानी पाउने कर्मचारी खाता थप गर्ने</div>
                
                <form action="{{route('karmachari.store')}}" method="post" id="karmachari_form">
                    {{csrf_field()}}
                    
                    <!--Personal Details-->
                    <h3>व्यक्तिगत विवरण</h3>
                    
                    <div class="form-group">
                        <div class="row">
                            
                            <div class="col-md-3">
                                <label for="name_nepali">पुरा नाम नेपालीमा :</label>
                                <input type="text" class="form-control" id="name_nepali" placeholder="नाम नेपालीमा" name="name_nepali" required>
                            </div>
                            
                            <div class="col-md-3">
                                <label for="name_english">पुरा नाम अंग्रेजीमा :</label>
                                <input type="text" class="form-control" id="name_english" placeholder="नाम अंग्रेजीमा" name="name_english" >
                            </div>
                            
                            <div class="col-md-2">
                                <label for="gender">लिङ्ग:</label>
                                <select class="form-control" name="gender" id="gender">
                                    <option value="">....</option>
                                    <option value="0">महिला</option>
                                    <option value="1">पुरुष</option>
                                    <option value="2">अन्य</option>
                                </select>
                            </div>
                            
                            <div class="col-md-2">
                                <label for="date_of_birth">जन्म मिति :</label>
                                <!---<input type="hidden" id="roman_date" name="dob_roman">-->
                                <input type="text" class="form-control " id="date_of_birth" placeholder="Select Date" name="date_of_birth">
                            </div>
                            
                            <div class="col-md-2">
                                <label for="marital_status">वैवाहिक अवस्था :</label>
                                <select class="form-control" name="marital_status" id="marital_status">
                                    <option value="">....</option>
                                    <option value="0">एकल</option>
                                    <option value="1">दम्पति</option>
                                </select>
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="row">
                            
                            <div class="col-md-3">
                                <label for="bank">बैङ्कको नाम</label>
                                <select id="bank" name="bank" class="form-control select2" style="width: 202px;">
                                    <option value="" selected="">.........</option>
                                    @foreach($all_banks as $bank)
                                    <option value="{{$bank->id}}">{{$bank->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="col-md-3">
                                <label>शाखा कार्यालय</label>
                                <input type="text" class="form-control" name="bank_address" id="bank_address" >
                            </div>
                            
                            <div class="col-md-2">
                                <label>खाता नं.</label>
                                <input type="text" class="form-control" name="khata_number" id="khata_number">
                            </div>
                            
                            <div class="col-md-2">
                                <label for="vat_pan">भ्याट/प्यान नं.</label>
                                <input type="text" class="form-control" id="vat_pan"  placeholder="भ्याट/प्यान नं." name="vat_pan">
                            </div>
                            
                            <div class="col-md-2">
                                <label>पेयी कोड</label>
                                <input type="number" class="form-control" name="payee_code" id="payee_code">
                            </div>
                            
                        </div>
                    </div>
                    {{-- PersonalDetailsEnd --}}
                    <div class="clearfix"></div>
                    
                    <h3>सम्पर्क विवरण</h3>
                    <div class="form-group">
                        <div class="row">

                            <div class="col-md-2">
                                <label>प्रदेश</label>
                                <select class="form-control select2" id="province" name="province">
                                    <option>.....................</option>
                                    @foreach($provinces as $province)
                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label for="district">जिल्ला :</label>
                                <select class="form-control select2" name="district" id="district">
                                    <option>..............</option>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label for="local_level">स्थानीय तह :</label>
                                <select class="form-control select2" name="local_level" id="local_level">
                                    <option value="">....</option>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label for="phone_number">फोन नं.:</label>
                                <input type="tel" class="form-control" id="phone_number" placeholder="फोन नं" name="phone_number" >
                            </div>

                            <div class="col-md-2">
                                <label for="mobile_number">मोबाइल:</label>
                                <input type="text" class="form-control" id="mobile_number" placeholder="मोबाइल" name="mobile_number" >
                            </div>

                            <div class="col-md-2">
                                <label for="email_address">इमेल:</label>
                                <input type="email" class="form-control" id="email_address" placeholder="इमेल" name="email_address" >
                            </div>

                        </div>
                    </div>
                    
                    {{--            Contact Details End--}}
                    
                    <div class="clearfix"></div>
                    
                    {{--            Organizational Details--}}
                    <h3>कार्यालयसम्बन्धी विवरण</h3>
                    
                    <div class="form-group">
                        <div class="row">

                            <div class="col-md-3">
                                <label for="darbandi_srot">दरबन्दीको श्रोत:</label>
                                <select class="form-control" name="darbandi_srot_id" id="darbandi_srot_id">
                                    <option value=""></option>
                                    @foreach($darbandisrots as $darbandisrot)
                                    <option value="{{$darbandisrot->id}}">{{$darbandisrot->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label for="darbandi_type">दरबन्दीको प्रकार:</label>
                                <select class="form-control" name="darbandi_type_id" id="darbandi_type_id">
                                    <option value="">....</option>
                                    @foreach($darbanditypes as $darbanditype)
                                    <option value="{{$darbanditype->id}}">{{$darbanditype->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label for="sheet_roll_no">सिटरोल नंबर.:</label>
                                <input type="number" class="form-control" id="sheet_roll_no" placeholder="सिटरोल नंबर"  name="sheet_roll_no">
                            </div>

                            <div class="col-md-3">
                                <label for="sewa">सेवा वर्ग:</label>
                                <select class="form-control" name="sewa_barga" id="sewa_barga">
                                    <option value="">............</option>
                                    <option value="1">निजामती सेवा</option>
                                    <option value="2">अन्य</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="sewa">सेवा:</label>
                                <select class="form-control" name="sewa_id" id="sewa_id" >
                                    <option value="">....</option>
                                    @foreach($sewas as $sewa)
                                    <option value="{{$sewa->id}}">{{$sewa->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label for="samuha_id">समूह:</label>
                                <select class="form-control" name="samuha_id" id="samuha_id">
                                    <option value="">....</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label for="taha">श्रेणी/तह:</label>
                                <select class="form-control" name="taha_id" id="taha_id">
                                    <option value="">....</option>
                                    @foreach($tahas as $taha)
                                    <option value="{{$taha->id}}">{{$taha->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label for="pad">पद:</label>
                                <input class="form-control" type="text" name="pad_id" name="pad_id">
                             </div>

                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="row">

                            <div class="col-md-2">
                                <label for="office_head">कार्यालय प्रमुख:</label>
                                <select class="form-control" name="office_head" id="office_head">
                                    <option value="2">होईन</option>
                                    <option value="1">हो</option>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label for="acc_head">आर्थिक प्रमुख:</label>
                                <select class="form-control" name="acc_head" id="acc_head">
                                    <option value="2">होईन</option>
                                    <option value="1">हो</option>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label for="is_peski">पेश्किमा देखाउने:</label>
                                <select class="form-control" name="is_peski" id="is_peski">
                                    <option value="1">हो</option>
                                    <option value="2">होईन</option>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label for="is_payroll">तलवी देखउने:</label>
                                <select class="form-control" name="is_payroll" id="is_payroll">
                                    <option value="2">होईन</option>
                                    <option value="1">हो</option>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label for="can_accept">स्विकृत:</label>
                                <select class="form-control" name="can_accept" id="can_accept">
                                    <option value="2">होईन</option>
                                    <option value="1">हो</option>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label for="status">सक्रिय:</label>
                                <select class="form-control" name="status" id="status">
                                    <option value="1">हो</option>
                                    <option value="0">होईन</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    
                    {{-- OrganizationalDetailEnd --}}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <center>
                                    <button type="submit" class="btn btn-primary" id="btnKarmachari">Save</button>
                                </center>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </section>
    
    <!-- FORM section start -->
    
    
    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body pt-10">
                <table class="table table-bordered">
                    <thead>
                        <tr style="background-color: #dbdbdb">
                            <th>क्र.स.</th>
                            <th>नाम नेपालीमा</th>
                            <th>पेयी कोड</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($karmacharis as $index=>$karmachari)
                        
                        <tr>
                            <td>{{++ $index}}</td>
                            <td>{{$karmachari->name_nep}}</td>
                            <td>{{$karmachari->payee_code}}</td>
                            
                            <td>
                                <a type="button" href="{{route('karmachari.edit',$karmachari->karmachari_id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                                @if($karmachari->getKarmachariInVoucherDetails->count() == 0)
                                <a type="button" href="#" id="delete_karmachari" data-karmachari-id="{{$karmachari->karmachari_id}}" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i> Delete</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection

@section('scripts')
<script>
    $(document).on('click','#delete_karmachari',function () {
        
        let karmachari_id = $(this).attr('data-karmachari-id');
        let url = '{{route('karmachari.delete',123)}}';
        url = url.replace('123',karmachari_id);
        swal({
            title: "Are you sure?",
            text: "Delete भए पछि Recovere हुदैन",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            
        }).then((willDelete) => {
            
            if (willDelete) {
                
                $.ajax({
                    
                    url : url,
                    method : 'get',
                    success : function (res) {
                        if(res){
                            swal("Bank has been deleted!", {
                                icon: "success",
                            });
                            
                            location.reload();
                        }
                        
                    }
                })
                
            } else {
                // swal("Your imaginary file is safe!");
            }
        });
    })
</script>

{{--  sewa बाट तह आउने--}}
  <script>
    $('#sewa_id').change(function () {

        let sewa_id = $('select#sewa_id').val();
        let url = '{{route('get_taha_by_sewa',123)}}';
        url =url.replace('123',sewa_id);

        $.ajax({

          method :'get',
          url : url,
          success : function (res) {
            let tahas = $.parseJSON(res);
            let options = '<option value="">.........</option>'
            $.each(tahas, function () {
              options  += '<option value="'+this.id+'">'+this.name+'</option>';
            });

            $('#taha_id').html(options);
          }
        })
    })
  </script>

{{--  Province_id click हुदा district आउने--}}
<script>
  $(document).ready(function () {
      $('#province').change(function () {
          let province_id = $('select#province').val();

          let url = '{{route('get.district',123)}}';
            url = url.replace(123,province_id);

            $.ajax({
              url : url,
              method : 'get',
              success : function (res) {
                let options = '<option value="" selected>....................</option>';
                $.each($.parseJSON(res),function () {

                  options +=  '<option value="'+ this.id +'">'+ this.name +'</option>';
                });

                $('#district').html(options);
              }
            })
      })
  })
</script>

{{--  District click हुदा local level आउने--}}
<script>
  $(document).ready(function () {
    $('#district').change(function () {
      let district_id = $('select#district').val();

      let url = '{{route('get.local.level',123)}}';
              url = url.replace(123,district_id);

      $.ajax({
        url : url,
        method : 'get',
        success : function (res) {
          let options = '<option value="" selected>....................</option>';
          $.each($.parseJSON(res),function () {

            options +=  '<option value="'+ this.id +'">'+ this.name +'</option>';
          });

          $('#local_level').html(options);
        }
      })
    })
  })
</script>
{{--  Date picker--}}
<script>
  $("#date_of_birth").nepaliDatePicker({
    dateFormat: "%y-%m-%d",
    closeOnDateSelect: true
  });

  var currentDate = new Date();
  var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
  var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
  $("#date_of_birth").val(formatedNepaliDate);

</script>


  {{--  Sewa bata Samuha Aaune--}}

  <script>
    let sewaBySamuha = function(this_){
      let sewa_id = $(this_).val();

      // alert('inside sewabysamuha sewa id = '+sewa_id);

      if (sewa_id){

        let url_ = '{{route('samuha.getbysewa', '123')}}';
        url_ = url_.replace('123', sewa_id);
        $.ajax({
          url:url_,  // Route Name here
          type: "GET",
          dataType: "json",
          success:function (data) {

            let options = '<option value=""></option>';
            $('#samuha_id').empty();
            $.each(data,function (key,value) {
              options +=  '<option value="'+ this.id +'">'+ this.name +'</option>';
            });
            $('#samuha_id').append(options).change();

          }
        })
      } else {
        $('$samuha_id').empty();
      }

    };

    $(document).ready(function ()
    {

      $('#sewa_id').on('change',function() {  // Make Separate function with code inside this function and call it from here

        sewaBySamuha(this);
      });

    });
  </script>

  {{--  sreni/taha bata Pad Aaune--}}

  <script>
    let padByTaha = function(this_) {
      let taha_id = $(this_).val();

      if (taha_id){
        //alert("samuha id changed");
        let url_ = '{{route('admin.designations.getbyTaha', '123')}}';
        url_ = url_.replace('123', taha_id);
        $.ajax({
          url:url_,
          type: "GET",
          dataType: "json",
          success:function (data) {
            $('#pad_id').empty();
            let options = '<option value="">.....</option>';
            $.each(data,function (key,value) {
              options += '<option value="'+ key +'">'+ value +'</option>';
            });
            $('#pad_id').append(options).change();

          }
        })
      } else {
        $('#pad_id').empty();
      }
    };


    $(document).ready(function ()
    {

      $('#taha_id').on('change',function() {
        padByTaha(this);
      });

    });

  </script>


{{--Convert Nepali date to roman date --}}
<script>
  function convertNepaliToEnglish(input) {
    var charArray = input.split('');
    var engDate = '';
    $.each(charArray, function (key, value) {
      switch (value) {
        case '१':
          engDate += '1'
          break
        case '२':
          engDate += '2'
          break
        case '३':
          engDate += '3'
          break
        case '४':
          engDate += '4'
          break
        case '५':
          engDate += '5'
          break
        case '६':
          engDate += '6'
          break
        case '०':
          engDate += '0'
          break
        case '७':
          engDate += '7'
          break
        case '८':
          engDate += '8'
          break
        case '९':
          engDate += '9'
          break

        case '-':
          engDate += '-'
          break
      }
    })
    return engDate

  }
</script>

@endsection