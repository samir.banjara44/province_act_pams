<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKarmachariTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karmacharis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_nepali');
            $table->string('name_english')->nullable();
            $table->integer('gender')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->integer('marital_status')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('email_address')->nullable();
            $table->string('vat_pan')->nullable();
            $table->integer('prvince_id')->nullable();
            $table->integer('district_id')->nullable();
            $table->integer('staniha_taha')->nullable();
            $table->integer('office_id');
            $table->integer('darbandi_srot_id')->nullable();
            $table->integer('darbandi_type_id')->nullable();
            $table->integer('sheet_roll_no')->nullable();
            $table->integer('sewa_barga')->nullable();
            $table->integer('sewa_id')->nullable();
            $table->integer('samuha_id')->nullable();
            $table->string('pad_id')->nullable();
            $table->integer('taha_id')->nullable();
            $table->integer('is_office_head')->nullable();
            $table->integer('is_acc_head')->nullable();
            $table->integer('is_peski')->nullable();
            $table->integer('can_accept')->nullable();
            $table->integer('is_payroll')->nullable();
            $table->integer('status');

            $table->integer('bank')->nullable();
            $table->string('khata_number')->nullable();
            $table->string('bank_address')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karmacharis');
    }
}
