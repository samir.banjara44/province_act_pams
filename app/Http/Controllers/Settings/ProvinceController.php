<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class ProvinceController extends Controller
{

    private $homeRepo;

    public function __construct(HomeRepository $homeRepo)
    {

        $this->middleware('auth');
        $this->homeRepo = $homeRepo;
    }

    public function index()
    {
        $provinces = Province::all();
        return view('backend.settings.province.index', compact('provinces'));
    }

    public function create(){

        return view('backend.settings.province.create');
    }
    public function store(Request $request){

        $province = new Province();
        $province->name = $request->name;
        $province->save();
        return redirect(route('admin.province'));
    }



}
