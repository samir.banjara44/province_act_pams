@extends('frontend.layouts.app')
<style>
    th {
        color: white;
        font-size: 16px;
    }
</style>
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-window-restore"></i> अख्तियारी सारांश प्रविष्टि </h1>

            <button type="button" class="btn btn-primary btn-sm btn-show-form" id="showAddForm"><i
                        class="fas fa-plus"></i>
                नयाँ थप गर्ने
            </button>
        </section>

        <!-- FORM section start -->
        <section class="formSection displayNone" id="showThisForm">
            <div class="row justify-content-md-center">
                <div class="col-md-10 form-bg col-md-offset-1">
                    <button type="button" class="btn btn-danger btn-sm btn-show-form displayNone" id="closeAddForm"><i
                                class="fas fa-times"></i> फर्म रद्ध गर्ने
                    </button>
                    <div class="form-title">अख्तियारी विवरण प्रविष्टी गर्ने</div>

                    <form action="{{route('akhtiyari.store')}}" method="post" id="akhtiyariForm">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="fiscal_year"> मिति:</label>
                                    <input type="text" class="form-control" name="date" id="date" value="" required
                                           style="height: 32px;">
                                    <input type="hidden" name="date_in_roman" id="date_in_roman">
                                </div>

                                <div class="col-md-2">
                                    <label>आर्थिक वर्ष :</label>
                                    <select id="fiscal_year" class="form-control" name="fiscal_year" required>
                                        @foreach($fiscalYears as $fy)
                                            <option value="{{$fy->id}}"
                                                    @if($fy->id == $fiscalYear->id) selected @endif>{{$fy->year}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label>बजेट उप शीर्षक :</label>
                                    <select name="budget_sub_head" class="form-control" id="budget_sub_head" required>
                                        <option value="">................</option>
                                        @foreach($programs as $program)
                                            <option value="{{$program->id}}">{{$program->program_code}} {{$program->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="source_type">स्रोत प्रकार</label>
                                    <select name="source_type" class="form-control" id="source_type" required>
                                        <option value="">...............</option>
                                        @foreach($sources as $source)
                                            <option value="{{$source->id}}">{{$source->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="source_level">बजेट स्रोत तह</label>
                                    <select name="source_level" class="form-control" id="source_level" required>
                                        <option></option>
                                    </select>
                                </div>

                            </div>
                        </div>


                        <div class="form-group">
                            <div class=" row">

                                <div class="col-md-3">
                                    <label for="source">स्रोत</label>
                                    <select name="source" class="form-control select2" id="source" required>
                                        <option value="">-----</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="medium">प्राप्ति विधि</label>
                                    <select name="medium" class="form-control medium" id="medium" required>
                                        @foreach($mediums as $medium)
                                            <option value="{{$medium->id}}">{{$medium->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label>रकम</label>
                                    <input type="number" class="form-control" name="amount" id="amount" required>
                                </div>

                                <div class="col-md-2">
                                    <label>अख्तियारि</label>
                                    <select class="form-control" id="akhtiyari" name="akhtiyari_type" required>
                                        <option value="" selected>.........</option>
                                        <option value="1">सुरु</option>
                                        <option value="2">थप</option>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label>विवरण</label>
                                    <input type="text" name="detail" id="detail" class="form-control" required>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <center>
                                        <button type="submit" class="btn btn-primary" name="btnAkhtiyariSave"
                                                id="btnAkhtiyariSave"><i class="fas fa-save"></i> Save
                                        </button>
                                        <a class="btn btn-primary" href="{{route('akhtiyari.param')}}">Clear
                                        </a>
                                    </center>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </section>

        <section class="content">
            <div class="panel panel-default">
                <div class="panel-body pt-10">
                    <table class="table table-bordered" border="1" id="tblAkhtiyari">
                        <thead>
                        <tr style="background-color: #236286; color: #fff" class="td-text-white">
                            <th>सि न.</th>
                            <th>बजेट उपशिर्षक</th>
                            <th>स्रोत प्रकार</th>
                            <th>बजेट स्रोत तह</th>
                            <th>स्रोत</th>
                            <th>प्राप्तिको विधि</th>
                            <th>अख्तियारि किसिम</th>
                            <th>विवरण</th>
                            <th>रकम</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr></tr>
                        <tr>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('scripts')
    {{--    Fiscal Year change --}}
    <script>
    $(document).ready(function () {
        $('#fiscal_year').change(function () {
            $('#budget_sub_head').val('')
            $('#tblAkhtiyari tbody').html('');
        })
    })
    </script>
    {{--    बजेट उपशिर्षक click गर्दा आख्तियारि ल्याउने--}}
    <script>
        $(document).on('change', '#budget_sub_head', function () {
            let budget_sub_head = $('select#budget_sub_head').val();
            let fiscalYear = $('#fiscal_year').val();
            let url = '{{route('getAkhtiyari',['123','345'])}}';
            url = url.replace('123', fiscalYear);
            url = url.replace('345', budget_sub_head);
            $.ajax({
                method: 'get',
                url: url,
                success: function (res) {
                    let parseRes = $.parseJSON(res);
                    let tr = '';
                    if (parseRes.length > 0) {

                        let i = 1;
                        let total_akhtiyari = 0;
                        $.each(parseRes, function () {
                            total_akhtiyari = total_akhtiyari + parseFloat(this.amount);

                            let url = "{{route('edit.akhtiyari',123)}}";
                            url = url.replace('123', this.id);
                            let data = '';
                            if (this.akhtiyari_type == 1) {

                                data = 'शुरु'
                            } else {
                                data = 'थप'
                            }

                            tr += "<tr style='background-color: white; color: black'>" +
                                "<td>" +
                                i +
                                "</td>" +

                                "<td class='activity' data-id=''>" +
                                this.program.name + " | " + this.program.program_code +
                                "</td>" +

                                "<td class='byahora'>" +
                                this.source_type.name +
                                "</td>" +

                                "<td class='details'>" +
                                this.source_level.name +
                                "</td>" +

                                "<td class='source'>" +
                                this.source.name +
                                "</td>" +

                                "<td class='medium'>" +
                                this.medium.name +
                                "</td>" +

                                "<td class='type'>" +
                                data +
                                "</td>" +

                                "<td class='details'>" +
                                this.detail +
                                "</td>" +

                                "<td class='amount'>" +
                                this.amount +
                                "</td>" +

                                "<td class='action'>" +
                                '<a href="' + url + '"  class="edit-budget"   data-index="' + this.id + '">Edit</a> | ';
                            if (this.budget.length == 0) {
                                tr += '<a href="#"  class="delete-budget"   data-index="' + this.id + '">Delete</a>';
                            }

                            tr += "</td>";
                            i = i + 1;

                        });

                        let totalTr = '';
                        totalTr += "<tr style='background-color: white; color: black'>" +
                            "<td colspan='8' style='text-align: right'>" +
                            "जम्मा" +
                            "</td>" +

                            "<td>" +
                            +total_akhtiyari +
                            "</td>";

                        $('#tblAkhtiyari tbody').html('');
                        $('#tblAkhtiyari tbody').append(tr);
                        $('#tblAkhtiyari tr:last').after(totalTr);
                    } else {

                        tr += "<tr>" +
                            "<td colspan='8' align='center'>" +
                            "विवरण प्रविश्टि भएको छैन !!" +
                            "</td>";
                        $('#tblAkhtiyari').find('tbody').html(tr);
                    }

                }
            })
        })
    </script>

    {{-- Akhtiyari delete--}}
    <script>
        $(document).on('click', '.delete-budget', function () {

            let activity_id = $(this).attr('data-index');
            let url = '{{route('delete.akhtiyari',123)}}';
            url = url.replace(123, activity_id);

            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    $('#budget_sub_head').change();

                }
            })

        })
    </script>


    {{--स्रोत प्रकार को आधारमा स्रोत आउने--}}
    <script>
        $(document).ready(function () {
            $('#source_type').change(function () {
                let sourceTypeId = $('#source_type').val();

                let url = '{{route('admin.get_source_by_source_type',123)}}';
                url = url.replace(123, sourceTypeId);

                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {

                        let options = '';

                        $.each($.parseJSON(res), function () {
                            // console.log(this);
                            options += '<option value="' + this.id + '">' + this.name + '</option>'
                        });
                        $('#source').html(options);

                    }
                })
            })
        })
    </script>

    {{--स्रोत प्रकार को आधारमा बजेट स्रोत तह आउने--}}
    <script>
        $(document).ready(function () {
            $('#source_type').change(function () {
                let sourceTypeId = $('#source_type').val();

                let url = '{{route('admin.get_source_level_by_source_type',123)}}';
                url = url.replace(123, sourceTypeId);

                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {

                        let temp = $.parseJSON(res);
                        console.log(temp);
                        let options = '<option>............</option>';
                        if (temp.length > 1) {
                            $.each(temp, function () {

                                options += '<option value="' + this.id + '" >' + this.name + '</option>'
                            })
                        } else {
                            options += '<option value="' + temp[0].id + '" selected>' + temp[0].name + '</option>'
                        }

                        $('#source_level').html(options).change();

                    }
                })
            })
        })
    </script>

    {{--Sunmit button click हुदा--}}
    <script>
        $(document).ready(function () {
            $('#btnAkhtiyariSave').click(function (e) {
                e.preventDefault();
                let date = $('input#date').val();
                let nepali_date_in_roman = convertNepaliToEnglish(date);
                if (validation()) {
                    $('#date_in_roman').val(nepali_date_in_roman);
                    $('#akhtiyariForm').submit();
                }
            })
        })
    </script>


    {{--Date Picker --}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);
    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            var charArray = input.split('');
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1';
                        break;
                    case '२':
                        engDate += '2';
                        break;
                    case '३':
                        engDate += '3';
                        break;
                    case '४':
                        engDate += '4';
                        break;
                    case '५':
                        engDate += '5';
                        break;
                    case '६':
                        engDate += '6';
                        break;
                    case '०':
                        engDate += '0';
                        break;
                    case '७':
                        engDate += '7';
                        break;
                    case '८':
                        engDate += '8';
                        break;
                    case '९':
                        engDate += '9';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
                //console.log(engDate)
            });
            return engDate

        }
    </script>

    {{--    validation--}}
    <script>
        function validation() {

            let flag = 1;
            // budget_sub_head validation
            let budget_sub_head_id = $('#budget_sub_head');
            if (!budget_sub_head_id.val()) {
                if (budget_sub_head_id.siblings('p').length == 0) {
                    budget_sub_head_id.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                budget_sub_head_id.focus();
                flag = 0;
            } else {
                budget_sub_head_id.siblings('p').remove()
            }

            let source_type_id = $('#source_type');
            if (!source_type_id.val()) {
                if (source_type_id.siblings('p').length == 0) {
                    source_type_id.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                source_type_id.focus();
                flag = 0;
            } else {
                source_type_id.siblings('p').remove()
            }

            let source_level_id = $('#source_level');
            if (!source_level_id.val()) {
                if (source_level_id.siblings('p').length == 0) {
                    source_level_id.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                source_level_id.focus();
                flag = 0;
            } else {
                source_level_id.siblings('p').remove()
            }

            let source_id = $('#source');
            if (!source_id.val()) {
                if (source_id.siblings('p').length == 0) {
                    source_id.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                source_id.focus();
                flag = 0;
            } else {
                source_id.siblings('p').remove()
            }

            let medium_id = $('#medium');
            if (!medium_id.val()) {
                if (medium_id.siblings('p').length == 0) {
                    medium_id.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                medium_id.focus();
                flag = 0;
            } else {
                medium_id.siblings('p').remove()
            }

            let amount_val = $('#amount');
            if (!amount_val.val()) {
                if (amount_val.siblings('p').length == 0) {
                    amount_val.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                amount_val.focus();
                flag = 0;
            } else {
                amount_val.siblings('p').remove()
            }


            let akhtiyari_val = $('#akhtiyari');
            if (!akhtiyari_val.val()) {
                if (akhtiyari_val.siblings('p').length == 0) {
                    akhtiyari_val.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                akhtiyari_val.focus();
                flag = 0;
            } else {
                akhtiyari_val.siblings('p').remove()
            }

            let detail_val = $('#detail');
            if (!detail_val.val()) {
                if (detail_val.siblings('p').length == 0) {
                    detail_val.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                detail_val.focus();
                flag = 0;
            } else {
                detail_val.siblings('p').remove()
            }

            return flag;
        }

    </script>

    <script>
        let changeToNepali = function (text) {
            let numbers = $.trim(text).split('');
            let nepaliNo = '';
            $.each(numbers, function (key, value) {
                if (value) {
                    if (value == 1)
                        nepaliNo += "१";
                    else if (value == 2)

                        nepaliNo += "२";
                    else if (value == 3)

                        nepaliNo += "३";
                    else if (value == 4)

                        nepaliNo += "४";
                    else if (value == 5)

                        nepaliNo += "५";
                    else if (value == 6)

                        nepaliNo += "६";
                    else if (value == 7)

                        nepaliNo += "७";
                    else if (value == 8)

                        nepaliNo += "८";
                    else if (value == 9)

                        nepaliNo += "९";
                    else if (value == 0)

                        nepaliNo += "०";
                    else if (value == ',')

                        nepaliNo += ",";
                    else if (value == '.')

                        nepaliNo += ".";
                    else if (value == '/')

                        nepaliNo += "/";
                }
            });
            console.log(nepaliNo);
            return nepaliNo;
        };


        $('.e-n-t-n-n').each(function () {
            let nepaliNo = changeToNepali($(this).text());
            let nepaliVal = changeToNepali($(this).val());

            $(this).text(nepaliNo);
            $(this).val(nepaliVal);
        });

    </script>

@endsection