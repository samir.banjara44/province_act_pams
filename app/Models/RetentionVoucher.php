<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RetentionVoucher extends Model
{
    protected $table = 'retention_vouchers';
    protected $guarded = [];


    public function retention_voucher_details(){

        return $this->hasMany('App\Models\RetentionVoucherDetails','retention_voucher_id','id');
    }

    public function fiscalYear(){

        return $this->belongsTo('App\Models\FiscalYear','fiscal_year','id');
    }
    public function details(){

        return $this->hasMany('App\Models\RetentionVoucherDetails','retention_voucher_id','id');
    }

    public function getReceivableRetention(){
        $receiveRetention = 0;
        foreach ($this->details as $detail){
            if(($detail->dr_or_cr == 1 and $detail->byahora == 10 and $detail->hisab_number == 395) or ($detail->dr_or_cr == 2 and $detail->byahora == 10 and $detail->hisab_number == 396)){
                $receiveRetention +=  $detail->amount;
            }
        }
        return $receiveRetention;
    }
    public function getReturnableRetention(){
        $returnRetention = 0;
        foreach ($this->details as $detail){
            if($detail->dr_or_cr == 1 and $detail->byahora == 10 and $detail->hisab_number == 397){
                $returnRetention +=  $detail->amount;
            }
        }
        if($returnRetention != 0){
            return $returnRetention;
        }
    }


}
