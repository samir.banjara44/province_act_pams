
@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        बजेट स्रोत /तह प्रविस्टी
{{--        <small>Control panel</small>--}}
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('admin.budget.source.level.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> थप्ने</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>स्रोतको प्रकार</th>
              <th>स्रोतको तह</th>

              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($budgetSourceLevels as $budgetSourceLevel)
              <tr>
                <td>{{$budgetSourceLevel->source_type->name}}</td>
                <td>{{$budgetSourceLevel->name}}</td>

                <td>
                  <a type="button" href="{{route('admin.program.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection