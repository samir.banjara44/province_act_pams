<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cheque extends Model
{
    protected $table = 'cheques';
    protected $guarded = [];

    public function bank()
    {
        return $this->belongsTo('App\Models\Bank');
    }

    public function cheque_details()
    {
        return $this->hasMany('App\Models\ChequeDetails', 'cheque_id', 'id');
    }

    public function unsign_cheque_details()
    {
        return $this->hasMany('App\Models\ChequeDetails', 'cheque_id', 'id')->where('status', 0);
    }
}
