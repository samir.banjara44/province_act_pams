<?php
namespace App\Repositories;

use App\Models\Area;
use Illuminate\Support\Facades\Auth;

class AreaRepositoryEloquent implements AreaRepository
{

    public function getAllArea(){

       return $areas = Area::all();

    }
    public function create($attributes) {
        $program = new Area();
        $program->name = $attributes['name'];
        $program->status = 1;
        $program->save();
        return $program;
    }
}