@extends('frontend.layouts.app')

<style>
    tr:hover {
        background-color: #e1e1e1!important;

    }
</style>
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                कार्यालय सुची

            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td colspan="7" style="text-align: center; font-size: 25px"><b>प्रयोग गर्ने कार्यालय</b></td>
                        </tr>
                        <tr>
                            <th>क्र.स.</th>
                            <th style="width: 300px;">कार्यालय</th>
                            <th>जिल्ला</th>
                            <th style="width: 300px; text-align: center">बजेट</th>
                            <th style="width: 300px; text-align: center">खर्च</th>
                            <th style="text-align: center" >प्रतिशत</th>
                            <th>प्रयोग अन्तिम मिति र भौचर</th>
                        </tr>

                        </thead>
                        <tbody>
                        @foreach($officeUsedLists as $index=>$office)
                            <tr>
                                <td>{{++$index}}</td>
                                <td>{{$office->name}}</td>
                                <td>
                                    @if($office->district)
                                        {{$office->district->name}}
                                    @endif
                                </td>
                                <td class="kalimati" style="text-align: right">{{number_format($office->getFinalBudgetOfOffice($office->id),2)}}</td>
                                <td class="kalimati" style="text-align: right">{{number_format($office->getExpenseOfOffice($office->id),2)}}</td>
                                <td class="kalimati" style="text-align: right">{{number_format($office->getPercentages($office->id),2)}}%</td>
                                <td class="kalimati">{{$office->getLastDateTransaction($office->id)}} भौचर न. {{$office->getLastVoucher($office->id)}}</td>
                                </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right"><b>जम्मा</b></td>
                            <td style="text-align: right" class="kalimati">{{number_format($totalProvinceBudget,2)}}</td>
                            <td style="text-align: right" class="kalimati">{{number_format($totalExpMinistry,2)}}</td>
                            <td style="text-align: right" class="kalimati">{{number_format($totalPercent,2)}}%</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td colspan="4" style="text-align: center; font-size: 25px"><b>प्रयोग नगर्ने कार्यालय</b></td>
                        </tr>
                        <tr>
                            <th>क्र.स.</th>
                            <th style="width: 300px;">कार्यालय</th>
                            <th>जिल्ला</th>
                            <th>कार्यालय कोड</th>
                            {{--                            <th>Action</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($officeNotUsedLists as $index=>$office)
                            <tr>
                                <td>{{++$index}}</td>
                                <td>{{$office->name}}</td>
                                <td>
                                    @if($office->district)
                                        {{$office->district->name}}
                                    @endif
                                </td>
                                <td class="kalimati">{{$office->office_code}}</td>

                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection
