<?php
namespace App\Repositories;

use App\Models\Source;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SourceRepositoryEloquent implements SourceRepository
{
    public function create($attributes) {
        $source = new Source();
        $source->name = $attributes['name'];
        $source->source_code = $attributes['source_code'];
        $source->source_prakar = $attributes['source_prakar'];
//        $source->source_type_id = $attributes['sourceTypeId'];
        $source->status = 1;
        $source->save();
        return $source;
    }

    public function get_by_source_type($source_type_id){
        if ($source_type_id == '1' or $source_type_id=='2'){
            return $sources = Source::whereBetween('source_code',['13311','13317'])->get();
        } else {
            return $sources = Source::where('source_prakar',$source_type_id)->get();
        }
    }


}