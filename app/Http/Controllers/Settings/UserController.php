<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Office;
use App\Models\Role;
use App\User;
use App\Models\Ministry;
use App\Models\Mof;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;

class UserController extends Controller
{

    private $homeRepo;

    public function __construct(HomeRepository $homeRepo)
    {

        $this->middleware('auth');
        $this->homeRepo = $homeRepo;
    }

    public function index()
    {
        $users = User::all();
        return view('backend.settings.user.index', compact('users'));
    }

    public function create()
    {
        $provinces = Province::all();
        $mofs = Mof::all();
        $ministries = Ministry::all();
        $departments = Department::all();
        $offices = Office::all();
        $roles = Role::all();
        return view('backend.settings.user.create', compact('provinces', 'mofs', 'ministries', 'departments', 'offices', 'roles'));
    }

    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->province_id = $request->province_id;
        $user->mof_id = $request->mof_id;
        $user->ministry_id = $request->ministry_id;
        $user->department_id = $request->department_id;
        $user->office_id = $request->office_id;
        $user->status = 1;
        $user->save();
        $role = Role::find($request->role_id);
        $user->roles()->attach($role);
        return redirect(route('admin.user'));
    }


}
