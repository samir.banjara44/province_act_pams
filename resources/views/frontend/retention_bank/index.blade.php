@extends('frontend.layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <i class="fas fa-university"></i> धरौटी बैङ्क विवरण </h1>

    <button type="button" class="btn btn-primary btn-sm btn-show-form" id="showAddForm"><i class="fas fa-plus"></i> नयाँ
      थप
      गर्ने</button>

  </section>

  <!-- FORM section start -->
  <section class="formSection displayNone" id="showThisForm">
    <div class="row justify-content-md-center">
      <div class="col-md-8 form-bg col-md-offset-2">
        <button type="button" class="btn btn-danger btn-sm btn-show-form displayNone" id="closeAddForm"><i
            class="fas fa-times"></i> फर्म रद्ध गर्ने</button>
        <div class="form-title">धरौटी बैङ्क विवरण प्रविष्टी गर्ने</div>
        <form action="{{route('retention_bank_store')}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <div class="row">

              <div class="col-md-5">
                <label for="name">बैङ्कको नाम:</label>
                <input type="text" class="form-control" id="name" placeholder="बैङ्क नाम" name="name" required>
              </div>

              <div class="col-md-5">
                <label for="address">ठेगाना</label>
                <input type="text" class="form-control" id="address" placeholder="ठेगाना" name="address" required>
              </div>

              <div class="col-md-2">
                <label for="">&nbsp;</label>
                <button type="submit" class="btn btn-primary btn-block">थप गर्ने</button>
              </div>

            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- FORM section ends -->



  <!-- Main content -->
  <section class="content">
    <div class="panel panel-primary">
      <div class="panel-body pt-10">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>बैङ्कको नाम</th>
              <th>ठेगाना</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($banks as $bank)
            <tr>
              <td>{{$bank->name}}</td>
              <td>{{$bank->address}}</td>

              <td>
                <a type="button" href="{{route('retention_bank_edit',$bank->id)}}" class="btn btn-sm btn-primary"><i
                    class="fa fa-pencil"></i> Edit</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>

@endsection