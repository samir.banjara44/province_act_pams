<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\AdvanceAndPayment;
use App\Models\AllBank;
use App\Models\Bank;
use App\Models\PartyTypes;
use App\Models\VatOffice;
use App\Models\Voucher;
use App\Models\VoucherDetail;
use App\Repositories\AdvancePaymentRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class AdvanceAndPaymentController extends Controller
{

    private $advanceAndPaymentRepo;


    public function __construct(AdvancePaymentRepository $advanceAndPaymentRepo)
    {

        $this->middleware('auth');
        $this->advanceAndPaymentRepo = $advanceAndPaymentRepo;
    }

    public function index()
    {
        // $advanceAndPayments= AdvanceAndPayment::all();
        $office_id = Auth::user()->office->id;
        $advanceAndPayments = $this->advanceAndPaymentRepo->get_by_office_id($office_id);
        $party_types = PartyTypes::all();
        $all_banks = AllBank::all();
        $vat_offices = VatOffice::all();
        return view('frontend.advanceAndPayment.index', compact('advanceAndPayments', 'party_types', 'all_banks', 'vat_offices', 'all_banks'));
    }

    public function create()
    {
        $office_id = Auth::user()->office_id;
        $party_types = PartyTypes::all();
        $all_banks = AllBank::all();
        $vat_offices = VatOffice::all();
        return view('frontend.advanceAndPayment.create', compact('party_types', 'banks', 'all_banks', 'vat_offices', 'all_banks'));
    }


    public function store(Request $request)
    {

        try {
            $program = $this->advanceAndPaymentRepo->create($request->all());
            $requestName = $request->name_eng;
            $message = "Added " . $requestName . " Successfully";
            if ($request->ajax()) {

                return json_encode($program);
            }
            return redirect(route('advance'))->with('success', $message);
        } catch (\Exception $e) {
            $message = "Can not add to table. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }
    }

    public function get_party_by_party_type_and_offie(Request $request, $party_type)
    {
        $office_id = Auth::user()->office->id;
        $parties = $this->advanceAndPaymentRepo->get_by_office_and_party_type($office_id, $party_type);
        return json_encode($parties);
    }

    public function getParty(Request $request, $party_type)
    {
        $office_id = Auth::user()->office->id;
        $parties = $this->advanceAndPaymentRepo->getPartyForRetention($office_id, $party_type);
        return json_encode($parties);
    }
    public function edit($id)
    {
        $advanceandpayment = $this->advanceAndPaymentRepo->get_by_id($id);
        $preBhuktaniCount = $advanceandpayment->preBhktani->count();
        $party_types = PartyTypes::all();
        $all_banks = AllBank::all();
        $vat_offices = VatOffice::all();
        return view('frontend.advanceAndPayment.edit', compact('advanceandpayment', 'party_types', 'all_banks', 'vat_offices', 'preBhuktaniCount'));
    }

    public function update(Request $request, $id)
    {
        try {
            $advanceandpayment = $this->advanceAndPaymentRepo->update($request->all(), $id);
            $vouchersDetails = VoucherDetail::where('peski_paune_id', $id)->get();
            foreach ($vouchersDetails as $detail) {

                $detail->peski_paune_id = $request['party_type'];
                $detail->save();
            }
            $requestName = $request->name_eng;
            $message = "Updated " . $requestName . " Successfully";
            return redirect(route('advance'))->with('success', $message);
        } catch (\Exception $e) {
            $message = "Can not update. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }
    }

    public function delete($advance_payment_id)
    {

        $delete_advance_payment = AdvanceAndPayment::findorfail($advance_payment_id)->update(['status' => 0]);
        return json_encode($delete_advance_payment);
    }
}
