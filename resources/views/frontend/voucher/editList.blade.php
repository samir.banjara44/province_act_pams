@extends('frontend.layouts.app')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-edit"></i> भौचर Edit गर्ने :: खोज </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <form action="#" method="post">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>आर्थिक वर्ष : </label>
                                        <select name="fiscal_year" id="fiscal_year" class="form-control">
                                            @foreach($fiscalYears as $fy)
                                                <option value="{{$fy->id}}"
                                                        @if($fy->id == $fiscalYear->id) selected @endif>{{$fy->year}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <label>बजेट उपशिर्षक :</label>
                                        <select name="budget_sub_head" id="budget_sub_head" class="form-control">
                                            <option value="0">..............</option>
                                            @foreach($programs as $program)
                                                <option value="{{$program->id}}">{{$program->name}}
                                                    | {{$program->program_code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12" style="padding-bottom: 22px;">
                    <table id="voucher_list" class="table table-bordered table-striped">
                        <thead>
                        <th>Sn</th>
                        <th>बजेट उप शीर्षक</th>
                        <th>मिति</th>
                        <th>भौचर नं.</th>
                        <th>कारोबार रकम</th>
                        <th>कारोबारको व्यहोरा</th>
                        <th colspan="2">कार्य</th>
                        </thead>
                        <tbody>
                        <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function () {
            $('#fiscal_year').change(function () {
                $('#budget_sub_head').val('');
                $('#voucher_list tbody').html('')
            })
        })
    </script>
    {{--    बजेट उपशिर्षक click गरेर  भौचर देखाउने--}}
    <script>
        $(document).ready(function () {
            $('#budget_sub_head').change(function () {

                let fiscal_year = $('#fiscal_year :selected').val();
                let budget_sub_head = $('#budget_sub_head :selected').val();
                let budget_sub_head_name = $('#budget_sub_head :selected').text();
                let url = '{{route('get_unapproved_voucher',['123','345'])}}';
                url = url.replace('123', fiscal_year);
                url = url.replace('345', budget_sub_head);

                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {
                        let data_length = res.length;
                        $datas = $.parseJSON(res);
                        let i = 1;
                        let tr = '';
                        if ($datas.length > 0) {
                            $.each($datas, function (key, value) {
                                let urlView = '{{route('voucher.view', '123')}}';
                                urlView = urlView.replace('123', this.id);

                                let url = "{{route('get_voucher_details',123)}}";
                                url = url.replace('123', this.id);
                                tr += "<tr>" +
                                    "<td>" +
                                    i +
                                    "</td>" +

                                    "<td class='activity' data-id=''>" +
                                    budget_sub_head_name +
                                    "</td>" +

                                    "<td class='byahora'>" +
                                    this.data_nepali +
                                    "</td>" +

                                    "<td class='details kalimati'>" +
                                    this.jv_number +
                                    "</td>" +

                                    "<td class='drAmount kalimati' align='right'>" +
                                    this.payement_amount.toFixed(2) +
                                    "</td>" +

                                    "<td class='drAmount'>" +
                                    this.short_narration +
                                    "</td>" +

                                    "<td>" +
                                    // '<a href="'+url+'" class="voucher-edit" data-id="'+ this.id +'" target="_blank">Edit</a> ' +
                                    '| <a href="' + urlView + '" target="_blank">हेर्ने</a> | ';


                                tr += '<a href="' + url + '" class="voucher-edit" data-id="' + this.id + '">Edit</a> |';
                                if (value.IS_LAST == true) {
                                    tr += '<a href="#" class="delete_voucher" data-delete-id="' + this.id + '">Delete</a>';
                                }

                                tr += "</td>";
                                i = i + 1;
                            });
                            $('#voucher_list').find('tbody').last('tr').html(tr);

                        } else {

                            tr += "<tr>" +
                                "<td colspan='7' style='text-align: center'>" +
                                "भौचर उपलब्ध छैन।" +
                                "</td>";
                            $('#voucher_list').find('tbody').last('tr').html(tr);

                        }

                    }
                })

            })
        })
    </script>

    <script>
        $(document).on('click', '.delete_voucher', function () {
            let voucher_id = $(this).attr('data-delete-id');
            let url = '{{route('voucher.delete', '123')}}';
            url = url.replace('123', voucher_id);
            swal({
                title: "एक पटक डिलिट गरे पछि पुनः प्राप्त गर्न सकिने छैन!",
                text: "डिलिट नगर्नलाई Cancel बटनमा क्लिक गर्नुहोस अथवा डिलिट गर्नलाई OK बटनमा क्लिक गर्नुहोस ।",
                icon: "warning",
                buttons: true,
                dangerMode: true,

            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: url,
                        method: 'get',
                        success: function (res) {
                            if (res) {
                                // swal("भौचर डिलिट भयो ।", {
                                //     icon: "success",
                                // });
                                $('select#budget_sub_head').change();
                            }
                        }
                    })

                } else {

                }
            });
        })
    </script>


@endsection