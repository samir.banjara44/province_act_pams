<?php
namespace App\Repositories;

use App\helpers\BsHelper;
use App\Models\PreBhuktani;
use App\Models\RetentionPreBhuktani;
use App\Models\Voucher;
use App\Models\VoucherDetail;
use Illuminate\Support\Facades\Auth;
use DB;

class RetentionPreBhuktaniRepositoryEloquent implements RetentionPreBhuktaniRepository
{


    public function store($attribute){

        $preBhuktani = new RetentionPreBhuktani();
        $preBhuktani->journel_id = $attribute['retention_voucher_id'];
        $preBhuktani->office_id = Auth::user()->office_id;
        $preBhuktani->party_type = $attribute['party_type'];
        $preBhuktani->party = $attribute['party'];
        $preBhuktani->amount = $attribute['amount'];
        $preBhuktani->hisab_number = $attribute['hisab_number'];
        $preBhuktani->date_nepali = $attribute['roman_date'];
        $date_array = explode('-', $attribute['roman_date']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $preBhuktani->date_english = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];
        $preBhuktani->status = 0;

        $mymonth=(explode('-',$attribute['roman_date']));
        $month=intval($mymonth[1]);

        if($month==1){$finalmonth=10;}
        if($month==2){$finalmonth=11;}
        if($month==3){$finalmonth=12;}
        if($month==4){$finalmonth=1;}
        if($month==5){$finalmonth=2;}
        if($month==6){$finalmonth=3;}
        if($month==7){$finalmonth=4;}
        if($month==8){$finalmonth=5;}
        if($month==9){$finalmonth=6;}
        if($month==10){$finalmonth=7;}
        if($month==11){$finalmonth=8;}
        if($month==12){$finalmonth=9;}

        $preBhuktani->fiscal_year=$attribute['fiscal_year'];
        $preBhuktani->month = $finalmonth;
        $preBhuktani->save();
        return $preBhuktani;

    }


    public function get_amount_by_multiple_id($ids){

        return $totalAmount = RetentionPreBhuktani::wherein('id',$ids)->sum('amount');
    }

    public function update_status_and_set_bhuktani_id($bhuktani_id,$preBhuktaniId){
       return $preBhuktani = RetentionPreBhuktani::where('id',$preBhuktaniId)
            ->update(
                array(
                    "bhuktani_id" => $bhuktani_id,
                    "status" => 1,

                )
            );
    }

    public function get_pre_bhuktani($officeId){
        return RetentionPreBhuktani::where('office_id',$officeId)->where('status','=',0)->get();
    }

    public function getByBhuktaniId($bhuktaniId){
        return RetentionPreBhuktani::where('bhuktani_id',$bhuktaniId)->get();
    }

    public function updateStatusAndBhuktaniId($preBhuktaniId){
        $preBhuiktani =  RetentionPreBhuktani::findorfail($preBhuktaniId);
        $preBhuiktani->status = 0;
        $preBhuiktani->bhuktani_id = 0;
        return $preBhuiktani->save();
    }
}

