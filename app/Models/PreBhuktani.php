<?php

namespace App\Models;

use App\helpers\Money_words;
use Illuminate\Database\Eloquent\Model;

class PreBhuktani extends Model
{
    protected $table = 'pre_bhuktani';
    protected $guarded = [];

    public function voucher()
    {
        return $this->belongsTo('App\Models\Voucher', 'journel_id', 'id');
    }

    public function party()
    {

        return $this->belongsTo('App\Models\AdvanceAndPayment', 'bhuktani_paaune', 'id');
    }

    public function budget_sub_head()
    {
        return $this->belongsTo('App\Budget_Sub_Head', 'budget_sub_head_id');
    }

    public function party_code()
    {

        return $this->belongsTo('App\Models\AdvanceAndPayment', 'bhuktani_paaune', 'id');
    }

    public function karmachari()
    {

        return $this->belongsTo('App\Models\Karmachari', 'bhuktani_paaune', 'id');
    }

    //    changed to BudgetDetails
    public function main_activity()
    {

        return $this->belongsTo('App\Models\BudgetDetails', 'main_activity_id', 'id');
    }

    public function bhuktani()
    {

        return $this->belongsTo('App\Models\Bhuktani', 'bhuktani_id', 'id');
    }

    public function get_amount_in_word()
    {
        $amount = $this->amount;
        $obj = new Money_words();
        return $words = $obj->convert_number(abs($amount));
    }

    public function partyko_type()
    {

        return $this->belongsTo('App\Models\PartyTypes', 'party_type', 'id');
    }

    public function all_bank()
    {

        return $this->belongsTo('App\Models\AllBank', 'bank_id', 'id');
    }
}
