<?php
namespace App\Repositories;

use App\Models\SubArea;
use Illuminate\Support\Facades\Auth;

class SubAreaRepositoryEloquent implements SubAreaRepository
{

    public function getAllSubArea(){
        $subAreas = SubArea::all();
        return $subAreas;
    }
    public function create($attributes) {
        $subArea = new SubArea();
        $subArea->name = $attributes['name'];
        $subArea->area_id = $attributes['area'];
        $subArea->status = 1;
        $subArea->save();
        return $subArea;
    }

    public function get_all_by_area_id($area_id){
       return  SubArea::where('area_id',$area_id)->get();
    }
}