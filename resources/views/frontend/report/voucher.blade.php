@extends('frontend.layouts.app')
@section('content')
    <style>
        tbody tr:hover {
            background-color: #e1e1e1 !important;

        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-copy"></i> भौचर खोज्ने </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body p-15">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route('report.voucher')}}" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    {{--                                    <div class="row">--}}
                                    <div class="col-md-2">
                                        <label>आर्थिक वर्ष : </label>
                                        <select name="fiscal_year" id="fiscal_year" class="form-control">
                                            @foreach($fiscalYears as $fy)
                                                <option value="{{$fy->id}}" @if($fy->id == $fiscalYear->id) selected @endif>{{$fy->year}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for=""> बजेट उपशिर्षक </label>
                                        <select name="budget_sub_head" id="budget_sub_head" class="form-control" required>
                                            <option value="" selected>.........................</option>
                                            @foreach($budgetSubHeads as $budgetSubHead)
                                                <option value="{{$budgetSubHead->id}}">{{$budgetSubHead->name}}
                                                    | {{$budgetSubHead->program_code}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <label for=""> भौचरको किसिम </label>
                                        <select name="voucher_type" class="form-control select2">
                                            <option>खर्च विवध</option>
                                            <option>पेश्की</option>
                                            <option>पेश्की फर्छ्यौट</option>
                                            <option>गत विगत आव को पेश्की</option>
                                            <option>गत विगत आव को पेश्की फर्छ्यौट</option>
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <label for="">भौचर नं.</label>
                                        <input type="number" class="form-control" name="voucher_number">
                                    </div>

                                    <div class="col-md-2">
                                        <label for="">&nbsp;</label>
                                        <button type="submit" class="btn btn-primary btn-block"><i
                                                    class="fas fa-search"></i> खोज्ने
                                        </button>
                                    </div>

                                </div>
                                {{--                                </div>--}}

                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </section>
        <section style="padding: 0 13px">
            <div class="panel panel-primary">
                <div class="panel-body p-12" style="padding: 15px 8px;">
                    <table class="table table-striped" border="1" id="voucher-table" style="margin-bottom: 46px;">
                        <thead style="background-color: #c6c5c5">
                        <td>SN</td>
                        <td>बजेट उपशीर्षक</td>
                        <td>भौचर नं.</td>
                        <td>मिति</td>
                        <td>विवरण</td>
                        <td>कारोवार रकम</td>
                        <td>एक्सन</td>
                        </thead>
                        <tbody>
                        @if($vouchersList)
                            @foreach($vouchersList as $index=>$voucher)
                                <tr style="background-color: white">
                                    <td class="kalimati" style="text-align: center">{{++$index}}</td>
                                    <td>{{$voucher->budget_sub_head->name}} - {{$voucher->budget_sub_head->program_code}} </td>
                                    <td style="text-align: center"><a class="kalimati"
                                                                      href="{{route('voucher.view',['id'=>$voucher->id])}}"
                                                                      target="_blank"
                                                                      style="color: black">{{$voucher->jv_number}} </a>
                                    </td>
                                    <td>{{($voucher->data_nepali)}}</td>
                                    <td><a class="kalimati" href="{{route('voucher.view',['id'=>$voucher->id])}}"
                                           target="_blank" style="color: black">{{$voucher->short_narration}}</a></td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($voucher->payement_amount,2, '.', ',')}}</td>

                                    <td style="text-align: center"><a
                                                href="{{route('voucher.view',['id'=>$voucher->id])}}"
                                                target="_blank">View </a></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#fiscal_year').change(function () {
                $('#budget_sub_head').val('');
                $('#voucher_list tbody').html('')
            })
        })
    </script>
@endsection