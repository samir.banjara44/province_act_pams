<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityReportDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_report_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activity_report_id');
            $table->integer('activity_id');
            $table->float('budget',11,2)->nullable();
            $table->string('expense_head')->nullable();
            $table->integer('akhtiyari_type')->nullable();
            $table->float('expense',11,2)->nullable();
            $table->float('income',11,2)->nullable();
            $table->integer('voucher_id')->nullable();
            $table->integer('month')->nullable();
            $table->string('fiscal_year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_report_details');

    }
}
