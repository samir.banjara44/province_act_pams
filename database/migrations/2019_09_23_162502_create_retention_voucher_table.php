<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetentionVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retention_vouchers', function (Blueprint $table) {


            $table->increments('id');
            $table->integer('voucher_number');
            $table->string('amount')->nullable();
            $table->dateTime('date_eng');
            $table->string('date_nep');
            $table->integer('month');
            $table->integer('status');
            $table->string('fiscal_year');
            $table->integer('office_id');
            $table->integer('voucher_signature_id')->nullable();
            $table->text('short_narration');
            $table->longText('long_narration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retention_vouchers');
    }
}
