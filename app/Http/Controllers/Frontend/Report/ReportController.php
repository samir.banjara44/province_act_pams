<?php

namespace App\Http\Controllers\Frontend\Report;

use App\helpers\FiscalYearHelper;
use App\Http\Controllers\Controller;
use App\Imports\BudgetImport;
use App\Models\ActivityReport;
use App\Models\ActivityReportDetail;
use App\Models\ExpenseHead;
use App\Models\FiscalYear;
use App\Models\Month;
use App\Models\Office;
use App\Models\Report;
use App\Models\Voucher;
use App\Models\VoucherDetail;
use App\Repositories\ActivityReportDetailRepository;
use App\Repositories\BhuktaniRepositoryEloquent;
use App\Repositories\ExpenseHeadRepository;
use App\Repositories\PreBhuktaniRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\ProgramRepositoryEloquent;
use App\Repositories\ReportRepository;
use App\Repositories\SourceRepository;
use App\Repositories\SourceTypeRepository;
use App\Repositories\VoucherDetailsRepository;
use App\Repositories\VoucherRepository;
use App\Repositories\RetentionBhuktaniRepository;
use App\Repositories\BudgetRepository;
use App\Repositories\BudgetRepositoryEloquent;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\VoucherSignatureRepository;
use App\Repositories\RetentionVoucherRepository;
use App\Repositories\ActivityProgressRepository;


class ReportController extends Controller
{
    private $programRepo;
    private $voucherrepo;
    private $budgetRepo;
    private $programRrepo;
    private $voucheDetailsRrepo;
    private $preBuktaniRrepo;
    private $bhuktaniRrepo;
    private $expenseHeadRrepo;
    private $fiscalYearHelper;
    private $budgetRrepo;
    private $voucherSignatureRepo;
    private $retentionVoucherDetails;
    private $retentionBhuktaniRepo;
    private $sourceRepo;
    private $sourcetypeRepo;
    private $activityReportDetailRepo;
    private $activityProgressRepo;


    public function __construct(

        ProgramRepository $programRepo,
        BudgetRepository $budgetRepo,
        VoucherRepository $voucherrepo,
        VoucherDetailsRepository $voucheDetailsRrepo,
        PreBhuktaniRepository $preBhuktaniRepository,
        ExpenseHeadRepository $expenseHeadRrepo,
        ProgramRepositoryEloquent $programRrepo,
        BhuktaniRepositoryEloquent $bhuktaniRrepo,
        BudgetRepositoryEloquent $budgetRrepo,
        VoucherSignatureRepository $voucherSignatureRepo,
        RetentionVoucherRepository $retentionVoucherRepo,
        RetentionBhuktaniRepository $retentionBhuktaniRepo,
        SourceRepository $sourceRepo,
        SourceTypeRepository $sourcetypeRepo,
        ActivityReportDetailRepository $activityReportDetailRepo,
        ActivityProgressRepository $activityProgressRepo

    ) {

        $this->middleware('auth');
        $this->programRepo = $programRepo;
        $this->voucherrepo = $voucherrepo;
        $this->voucheDetailsRrepo = $voucheDetailsRrepo;
        $this->preBuktaniRrepo = $preBhuktaniRepository;
        $this->expenseHeadRrepo = $expenseHeadRrepo;
        $this->fiscalYearHelper = new FiscalYearHelper();
        $this->expenseHeadRrepo = $expenseHeadRrepo;
        $this->programRrepo = $programRrepo;
        $this->bhuktaniRrepo = $bhuktaniRrepo;
        $this->budgetRrepo = $budgetRrepo;
        $this->voucherSignatureRepo = $voucherSignatureRepo;
        $this->retentionVoucherRepo = $retentionVoucherRepo;
        $this->retentionBhuktaniRepo = $retentionBhuktaniRepo;
        $this->budgetRepo = $budgetRepo;
        $this->sourceRepo = $sourceRepo;
        $this->sourcetypeRepo = $sourcetypeRepo;
        $this->activityReportDetailRepo = $activityReportDetailRepo;
        $this->activityProgressRepo = $activityProgressRepo;
    }

    public function voucher(Request $request)
    {
        $data = $request->all();
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $budgetSubHeads = $this->programRepo->get_program_for_report();
        $vouchersList = null;
        $budget_sub_head = $request['budget_sub_head'];
        if ($request->has('budget_sub_head')) {
            $vouchersList = $this->voucherrepo->get_all_voucher_budget_sub_head_id($data);
        }
        return view('frontend.report.voucher', compact('budgetSubHeads', 'vouchersList', 'fiscalYear', 'fiscalYears'));
    }

    public function voucher_view(Request $request, $id)
    {
        try {
            $preBhuktanies = $this->preBuktaniRrepo->get_preBhuktani_by_voucher_id($id);
            $total_bhuktani = $preBhuktanies->sum('amount');
            $total_bhuktani_word = $this->bhuktaniRrepo->get_amount_in_word(abs($total_bhuktani));
            $voucher = $this->voucherrepo->find($id);
            $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($voucher->fiscal_year);
            $voucherDetails = $this->voucheDetailsRrepo->get_details_by_voucher_id_with_activity($id);
        } catch (Exception $e) {
            //            return $e->getMessage();
            return back()->withError($e->getMessage())->withInput();
        }
        return view('frontend.report.voucher_view', compact('voucher', 'preBhuktanies', 'voucherDetails', 'total_bhuktani', 'total_bhuktani_word', 'fiscalYear'));
    }

    public function bhuktani_adesh_param()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.bhuktaniParam', compact('programs', 'fiscalYear', 'fiscalYears'));
    }


    public function bhuktani_adesh_report(Request $request, $id)
    {

        $bhuktani = $this->bhuktaniRrepo->get_bhuktani_by_bhuktani_id($id);
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($bhuktani->fiscal_year);
        if ($bhuktani->amount != 0) {
            $pre_bhuktanies = $this->preBuktaniRrepo->get_pre_bhuktani_by_bhuktani_id($id);
            $bhuktani_amount_word = $this->bhuktaniRrepo->get_amount_in_word(abs($bhuktani['amount']));
            $voucher = ($bhuktani->preBhuktani[0]->voucher);
            $program = $this->programRepo->get_by_program_id($bhuktani['budget_sub_head']);
            return view('frontend.report.bhuktaniReport', compact('bhuktani', 'program', 'voucher', 'bhuktani_amount_word', 'pre_bhuktani', 'pre_bhuktani_array', 'pre_bhuktanies', 'fiscalYear'));
        } else {
            $pre_bhuktanies = $this->preBuktaniRrepo->get_pre_bhuktani_by_bhuktani_id($id);
            $bhuktani_amount_word = $this->bhuktaniRrepo->get_amount_in_word($bhuktani['amount']);
            $voucher = ($bhuktani->preBhuktani[0]->voucher);
            $program = $this->programRepo->get_by_program_id($bhuktani['budget_sub_head']);
            return view('frontend.report.samayojanBhuktaniReport', compact('bhuktani', 'program', 'voucher', 'bhuktani_amount_word', 'pre_bhuktani', 'pre_bhuktani_array', 'pre_bhuktanies', 'fiscalYear'));
        }
    }

    public function malepa_five_param()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.malepaFiveParam', compact('programs', 'fiscalYear', 'fiscalYears'));
    }


    public function malepa_five_report(Request $request)
    {
        $datas = $request->all();
        $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);

        if ($datas['month'] == 1234) {
            $voucherLists = $this->voucherrepo->get_all_voucher_budget_sub_head_id_group_by_month($datas['budget_sub_head'], $datas['fiscal_year']);
            $voucherModel = new Voucher();
            $thisMonthExp = [];
            $voucherModel['srawan'] = $voucherLists->where('month', 1);
            $srawanMonthTotal = 0;
            $bhadraMonthTotal = 0;
            $asojMonthTotal = 0;
            $kartikMonthTotal = 0;
            $margaMonthTotal = 0;
            $paushMonthTotal = 0;
            $maghMonthTotal = 0;
            $fagunMonthTotal = 0;
            $chaitraMonthTotal = 0;
            $baisakhMonthTotal = 0;
            $jesthaMonthTotal = 0;
            $asharMonthTotal = 0;
            if ($voucherModel['srawan']->count() > 0) {
                $thisMonthExp['srawan'] = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office'], 1);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'month' => 1, 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $srawanMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($thisMonthVoucherDetailList, $datas, 1);
            }
            $voucherModel['bhadra'] = $voucherLists->where('month', 2);
            if ($voucherModel['bhadra']->count() > 0) {
                $thisMonthExp['bhadra'] = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office'], 2);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'month' => 2, 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $bhadraMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($thisMonthVoucherDetailList, $datas, 2);
            }
            $voucherModel['ashoj'] = $voucherLists->where('month', 3);
            if ($voucherModel['ashoj']->count() > 0) {
                $thisMonthExp['ashoj'] = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office'], 3);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'month' => 3, 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $asojMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($thisMonthVoucherDetailList, $datas, 3);
            }
            $voucherModel['kartik'] = $voucherLists->where('month', 4);
            if ($voucherModel['kartik']->count() > 0) {
                $thisMonthExp['kartik'] = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office'], 4);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'month' => 4, 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $kartikMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($thisMonthVoucherDetailList, $datas, 4);
            }
            $voucherModel['marga'] = $voucherLists->where('month', 5);
            if ($voucherModel['marga']->count() > 0) {
                $thisMonthExp['marga'] = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office'], 5);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'month' => 5, 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $margaMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($thisMonthVoucherDetailList, $datas, 5);
            }
            $voucherModel['paush'] = $voucherLists->where('month', 6);
            if ($voucherModel['paush']->count() > 0) {
                $thisMonthExp['paush'] = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office'], 6);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'month' => 6, 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $paushMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($thisMonthVoucherDetailList, $datas, 6);
            }
            $voucherModel['magh'] = $voucherLists->where('month', 7);
            if ($voucherModel['magh']->count() > 0) {
                $thisMonthExp['magh'] = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office'], 7);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'month' => 7, 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $maghMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($thisMonthVoucherDetailList, $datas, 7);
            }
            $voucherModel['fagun'] = $voucherLists->where('month', 8);
            if ($voucherModel['fagun']->count() > 0) {
                $thisMonthExp['fagun'] = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office'], 8);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'month' => 8, 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $fagunMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($thisMonthVoucherDetailList, $datas, 8);
            }
            $voucherModel['chaitra'] = $voucherLists->where('month', 9);
            if ($voucherModel['chaitra']->count() > 0) {
                $thisMonthExp['chaitra'] = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office'], 9);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'month' => 9, 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $chaitraMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($thisMonthVoucherDetailList, $datas, 9);
            }
            $voucherModel['baisakh'] = $voucherLists->where('month', 10);
            if ($voucherModel['baisakh']->count() > 0) {
                $thisMonthExp['baisakh'] = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office'], 10);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'month' => 10, 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $baisakhMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($thisMonthVoucherDetailList, $datas, 10);
            }
            $voucherModel['jestha'] = $voucherLists->where('month', 11);
            if ($voucherModel['jestha']->count() > 0) {
                $thisMonthExp['jestha'] = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office'], 11);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'month' => 11, 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $jesthaMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($thisMonthVoucherDetailList, $datas, 11);
            }
            $voucherModel['ashar'] = $voucherLists->where('month', 12);
            if ($voucherModel['ashar']->count() > 0) {
                $thisMonthExp['ashar'] = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office'], 12);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'month' => 12, 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $asharMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($thisMonthVoucherDetailList, $datas, 12);
            }
            $datas['month'] = 12;
            $upTOMonthExp = $this->activityReportDetailRepo->get_up_to_this_month_exp_by_budget_sub_head_and_office($datas);
            $UpToThisVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'up_to_this_month' => 12, 'status' => 1, 'office_id' => Auth::user()->office_id]);
            $upTothisMonthTotal = $this->voucheDetailsRrepo->get_all_field_total_cahs_book_all_month($UpToThisVoucherDetailList, $datas, 12);

            return view('frontend.report.malepaFive_all_month', compact('voucherLists', 'voucherModel', 'program', 'thisMonthExp', 'srawanMonthTotal', 'bhadraMonthTotal', 'asojMonthTotal', 'kartikMonthTotal', 'margaMonthTotal', 'paushMonthTotal', 'maghMonthTotal', 'fagunMonthTotal', 'chaitraMonthTotal', 'baisakhMonthTotal', 'jesthaMonthTotal', 'asharMonthTotal', 'upTothisMonthTotal', 'upTOMonthExp'));
        } else {

            $month_post = $datas['month'];
            $month = Month::findorfail($month_post);
            if ($month) {
                $month_name = ($month->name);
            }
            $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($datas['fiscal_year']);
            if ($datas['month'] <= 9) {
                $year = substr($fiscalYear->year, 0, 4);
            } else {
                $year = substr($fiscalYear->year, 0, 4) + 1;
            }
            $voucherList = $this->voucherrepo->get_all_voucher_by_budget_sub_head_id($datas['budget_sub_head'], $datas['fiscal_year'], $datas['month']);
            if ($voucherList->count() > 0) {
                $budget_sub_head_name = $voucherList[0]->budget_sub_head->name;
            }
            $preVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscalYear' => $datas['fiscal_year'], 'less_than_this_month' => $datas['month'], 'status' => 1, 'office_id' => Auth::user()->office_id]);
            $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscalYear' => $datas['fiscal_year'], 'month' => $datas['month'], 'status' => 1, 'office_id' => Auth::user()->office_id]);
            $UpToThisVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscalYear' => $datas['fiscal_year'], 'up_to_this_month' => $datas['month'], 'status' => 1, 'office_id' => Auth::user()->office_id]);

            $preMonthExp = $this->activityReportDetailRepo->get_pre_month_exp_by_budget_sub_head_and_office($datas);
            $thisMonthExp = $this->activityReportDetailRepo->get_this_month_exp_by_budget_sub_head_and_office($datas);
            $upTOMonthExp = $this->activityReportDetailRepo->get_up_to_this_month_exp_by_budget_sub_head_and_office($datas);

            if ($preVoucherDetailList) {
                $preMonthTotal = $this->voucheDetailsRrepo->get_all_field_total($preVoucherDetailList, $datas);
                $thisMonthTotal = $this->voucheDetailsRrepo->get_all_field_total($thisMonthVoucherDetailList, $datas);
                $upTothisMonthTotal = $this->voucheDetailsRrepo->get_all_field_total($UpToThisVoucherDetailList, $datas);
            }
            $lastMonthNikasa  = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
                ->where('vouchers.status', '=', 1)
                ->where('vouchers.month', '<', $datas['month'])
                ->where('vouchers.fiscal_year', '=', $datas['fiscal_year'])
                ->where('voucher_details.office_id', '=', Auth::user()->office_id)
                ->where('voucher_details.budget_sub_head_id', '=', $datas['budget_sub_head'])
                ->where('voucher_details.dr_or_cr', '=', 2)
                ->where('voucher_details.ledger_type_id', '=', 8)->sum('voucher_details.cr_amount');


            $lastMonthCrTsa  = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
                ->where('vouchers.status', '=', 1)
                ->where('vouchers.month', '<', $datas['month'])
                ->where('vouchers.fiscal_year', '=', $datas['fiscal_year'])
                ->where('voucher_details.office_id', '=', Auth::user()->office_id)
                ->where('voucher_details.budget_sub_head_id', '=', $datas['budget_sub_head'])
                ->where('voucher_details.dr_or_cr', '=', 2)
                ->where('voucher_details.ledger_type_id', '=', 3)->sum('voucher_details.cr_amount');
            $lastMonthAlya = floatval($lastMonthNikasa) - floatval($lastMonthCrTsa);
            if ($lastMonthAlya < 0) {

                $lastMonthAlya = (float)((-1) * (float)$lastMonthAlya);
                $lastMonthAlya = number_format($lastMonthAlya, 2);
                $lastMonthAlya = '(' . $lastMonthAlya . ')';
            } else {

                $lastMonthAlya = $lastMonthAlya;
            }
            $voucher_signature = $this->voucherSignatureRepo->get_by_office_id(Auth::user()->office_id);

            return view('frontend.report.malepaFive', compact('voucherList', 'thisMonthTotal', 'program', 'preMonthTotal', 'upTothisMonthTotal', 'year', 'month_name', 'voucher_signature', 'preMonthExp', 'thisMonthExp', 'upTOMonthExp', 'lastMonthAlya'));
        }
    }



    public function malepa_thirteen_param()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.malepaThirteenParam', compact('programs', 'fiscalYear', 'fiscalYears'));
    }

    public function malepa_thirteen_source_wise_param()
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.malepaThirteenSourceWiseParam', compact('programs', 'fiscalYear', 'fiscalYears'));
    }

    public function malepa_thirteen_source_wise(Request $request)
    {
        $requestData = $request->all();
        $month_post = $requestData['month'];
        $month = Month::findorfail($requestData['month']);
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($requestData['fiscal_year']);
        if ($month_post <= 9) {
            $year = substr($fiscalYear->year, 0, 4);
        } else {
            $year = substr($fiscalYear->year, 0, 4) + 1;
        }
        $programs = $this->programRrepo->get_by_program_id($requestData['budget_sub_head']);
        $sourceTypes = $this->sourcetypeRepo->getSourceByOfficeAndBudgetSubHead($requestData['office'], $requestData['fiscal_year'], $requestData['budget_sub_head']);
        return view('frontend.report.malepaThirteenSourceWise', compact('requestData', 'programs', 'fiscalYear', 'sourceTypes', 'year', 'month'));
    }

    public function malepa_fourteen_param()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.malepaFourteenParam', compact('programs', 'fiscalYear', 'fiscalYears'));
    }

    public function malepa_thirteen_report(Request $request)
    {

        $datas = $request->all();
        $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);
        $month = Month::findorfail($datas['month']);
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($datas['fiscal_year']);
        if ($datas['month'] <= 9) {
            $year = substr($fiscalYear->year, 0, 4);
        } else {
            $year = substr($fiscalYear->year, 0, 4) + 1;
        }
        if (array_key_exists('monthFromTo', $datas)) {
            if ($datas['monthFromTo'] == "on") {
                $monthFrom = Month::findorfail($datas['monthFrom']);
                $monthTo = Month::findorfail($datas['monthTo']);
                $budgetByExpenseHeads = $this->voucheDetailsRrepo->get_all_voucher_by_group_by_expense_head($datas['budget_sub_head'], $datas['fiscal_year']);
                $upToLastMonthVoucherDetailList = $this->voucheDetailsRrepo->searchByGroupBy(['budget_sub_head' => $datas['budget_sub_head'], 'fiscalYear' => $datas['fiscal_year'], 'less_than_this_month' => $datas['monthFrom'], 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $upToThisMonthVoucherDetailList = $this->voucheDetailsRrepo->searchByGroupBy(['budget_sub_head' => $datas['budget_sub_head'], 'fiscalYear' => $datas['fiscal_year'], 'up_to_this_month' => $datas['monthTo'], 'status' => 1, 'office_id' => Auth::user()->office_id]);
                $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->searchByGroupBy(['budget_sub_head' => $datas['budget_sub_head'], 'fiscalYear' => $datas['fiscal_year'], 'monthTo' => $datas['monthTo'], 'status' => 1, 'office_id' => Auth::user()->office_id]);
                if ($thisMonthVoucherDetailList) {
                    $upToLastMonthExp = $this->voucheDetailsRrepo->get_all_field_total($upToLastMonthVoucherDetailList, $datas);
                    $upToThisMonthExp = $this->voucheDetailsRrepo->get_all_field_total($upToThisMonthVoucherDetailList, $datas);
                    $thisMonthExp = $this->voucheDetailsRrepo->get_all_field_total($thisMonthVoucherDetailList, $datas);
                    $upToThisMonthpeski = $this->voucheDetailsRrepo->get_all_field_total($upToThisMonthVoucherDetailList, $datas);
                    $expense_with_out_peski = ($upToThisMonthExp['expense'] - $upToThisMonthExp['peski']);
                }

                $voucher_signature = $this->voucherSignatureRepo->get_by_office_id(Auth::user()->office_id);
                return view('frontend.report.malepaThirteenBetweenMonth', compact('year', 'fiscalYear', 'datas', 'month_post', 'monthTo', 'monthFrom', 'lastMonthName', 'program', 'voucherDetails', 'month_id', 'upToLastMonthExp', 'upToThisMonthExp', 'thisMonthExp', 'expense_with_out_peski', 'budgetByExpenseHeads', 'voucher_signature'));
            }
        } else {

            $budgetByExpenseHeads = $this->voucheDetailsRrepo->get_all_voucher_by_group_by_expense_head($datas['budget_sub_head'], $datas['fiscal_year']);
            $upToLastMonthVoucherDetailList = $this->voucheDetailsRrepo->searchByGroupBy(['budget_sub_head' => $datas['budget_sub_head'], 'fiscalYear' => $datas['fiscal_year'], 'less_than_this_month' => $datas['month'], 'status' => 1, 'office_id' => Auth::user()->office_id]);

            $upToThisMonthVoucherDetailList = $this->voucheDetailsRrepo->searchByGroupBy(['budget_sub_head' => $datas['budget_sub_head'], 'fiscalYear' => $datas['fiscal_year'], 'up_to_this_month' => $datas['month'], 'status' => 1, 'office_id' => Auth::user()->office_id]);

            $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->searchByGroupBy(['budget_sub_head' => $datas['budget_sub_head'], 'fiscalYear' => $datas['fiscal_year'], 'month' => $datas['month'], 'status' => 1, 'office_id' => Auth::user()->office_id]);

            if ($thisMonthVoucherDetailList) {
                $upToLastMonthExp = $this->voucheDetailsRrepo->get_all_field_total($upToLastMonthVoucherDetailList, $datas);

                $upToThisMonthExp = $this->voucheDetailsRrepo->get_all_field_total($upToThisMonthVoucherDetailList, $datas);

                $thisMonthExp = $this->voucheDetailsRrepo->get_all_field_total($thisMonthVoucherDetailList, $datas);

                $upToThisMonthpeski = $this->voucheDetailsRrepo->get_all_field_total($upToThisMonthVoucherDetailList, $datas);
                $expense_with_out_peski = ($upToThisMonthExp['expense'] - $upToThisMonthExp['peski']);
            }
            $totalExpUpToThisMonth = $this->activityReportDetailRepo->get_up_to_this_month_exp_by_budget_sub_head_and_office($datas);
            // dd($totalExpUpToThisMonth);

            // $totalExpUpToThisMonth = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            //     ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            //     ->where('vouchers.status', 1)
            //     ->where('activity_report.budget_sub_head_id', $budget_sub_head_id)
            //     ->where('activity_report_details.month', '<=', $month_post)->sum('activity_report_details.expense');
            $voucher_signature = $this->voucherSignatureRepo->get_by_office_id(Auth::user()->office_id);
            return view('frontend.report.malepaThirteen', compact('year', 'fiscalYear', 'datas', 'month', 'program', 'upToLastMonthExp', 'upToThisMonthExp', 'thisMonthExp', 'expense_with_out_peski', 'budgetByExpenseHeads', 'voucher_signature', 'totalExpUpToThisMonth'));
        }
    }

    public function malepa_fourteen_report(Request $request)
    {
        $datas = $request->all();
        $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);
        $month = Month::findorfail($datas['month']);
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($datas['fiscal_year']);
        if ($datas['month'] <= 9) {
            $year = substr($fiscalYear->year, 0, 4);
        } else {
            $year = substr($fiscalYear->year, 0, 4) + 1;
        }
        $UpToThisVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $datas['budget_sub_head'], 'fiscal_year' => $datas['fiscal_year'], 'office_id' => Auth::user()->office_id]);
        $voucherDetailsList = $this->voucheDetailsRrepo->getApprovedDetailsByBudgetSubHead($datas);
        $karmachariAdvance = $this->voucheDetailsRrepo->get_advance($voucherDetailsList);
        $total_peski = [];

        //        उपभोक्ता
        $total_peski['upBhokta_peski_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 4)->where('party_type', 1)->sum('dr_amount');
        $total_peski['upBhokta_clearance_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 2)->where('ledger_type_id', 9)->where('party_type', 1)->sum('cr_amount');
        $upaBhokta_remain_peski = $total_peski['upBhokta_peski_total'] - $total_peski['upBhokta_clearance_total'];


        $total_peski['upBhokta_last_peski_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 7)->where('party_type', 1)->sum('dr_amount');
        $total_peski['upBhokta_last_clearance_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 2)->where('ledger_type_id', 12)->where('party_type', 1)->sum('cr_amount');
        $upaBhokta_last_remain_peski = $total_peski['upBhokta_last_peski_total'] - $total_peski['upBhokta_last_clearance_total'];

        //        ठेकेदार
        $total_peski['thekedar_peski_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 4)->where('party_type', 2)->sum('dr_amount');
        $total_peski['thekedar_clearance_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 2)->where('ledger_type_id', 9)->where('party_type', 2)->sum('cr_amount');
        $thekedar_remain_peski = $total_peski['thekedar_peski_total'] - $total_peski['thekedar_clearance_total'];


        $total_peski['thekedar_last_peski_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 7)->where('party_type', 2)->sum('dr_amount');
        $total_peski['thekedar_last_clearance_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 2)->where('ledger_type_id', 12)->where('party_type', 2)->sum('cr_amount');
        $thekedar_last_remain_peski = $total_peski['thekedar_last_peski_total'] - $total_peski['thekedar_last_clearance_total'];


        //        व्यक्तिगत
        $total_peski['byaktigat_peski_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 4)->where('party_type', 3)->sum('dr_amount');
        $total_peski['byaktigat_clearance_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 2)->where('ledger_type_id', 9)->where('party_type', 3)->sum('cr_amount');
        $byaktigat_remain_peski = $total_peski['byaktigat_peski_total'] - $total_peski['byaktigat_clearance_total'];

        $total_peski['byaktigat_last_peski_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 7)->where('party_type', 3)->sum('dr_amount');
        $total_peski['byaktigat_last_clearance_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 2)->where('ledger_type_id', 12)->where('party_type', 3)->sum('cr_amount');
        $byaktigat_last_remain_peski = $total_peski['byaktigat_last_peski_total'] - $total_peski['byaktigat_last_clearance_total'];


        //        संस्थागन
        $total_peski['sasthagat_peski_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 4)->where('party_type', 4)->sum('dr_amount');
        $total_peski['thekedar_clearance_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 2)->where('ledger_type_id', 9)->where('party_type', 4)->sum('cr_amount');
        $sasthagat_remain_peski = $total_peski['sasthagat_peski_total'] - $total_peski['thekedar_clearance_total'];

        $total_peski['sasthagat_last_peski_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 7)->where('party_type', 4)->sum('dr_amount');
        $total_peski['sasthagat_last_clearance_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 2)->where('ledger_type_id', 12)->where('party_type', 4)->sum('cr_amount');
        $sasthagat_last_remain_peski = $total_peski['sasthagat_last_peski_total'] - $total_peski['sasthagat_last_clearance_total'];


        //        Karmachari
        $total_peski['karmachari_peski_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 4)->where('party_type', 5)->sum('dr_amount');
        $total_peski['karmachari_clearance_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 2)->where('ledger_type_id', 9)->where('party_type', 5)->sum('cr_amount');
        $karmachari_remain_peski = $total_peski['karmachari_peski_total'] - $total_peski['karmachari_clearance_total'];

        $total_peski['karmachari_last_peski_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 7)->where('party_type', 5)->sum('dr_amount');
        $total_peski['karmachari_last_clearance_total'] = $UpToThisVoucherDetailList->where('dr_or_cr', 2)->where('ledger_type_id', 12)->where('party_type', 5)->sum('cr_amount');
        $karmachari_last_remain_peski = $total_peski['karmachari_last_peski_total'] - $total_peski['karmachari_last_clearance_total'];


        $total_peski = $UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 4)->sum('dr_amount');
        $total_clearance = $UpToThisVoucherDetailList->where('dr_or_cr', 2)->where('ledger_type_id', 9)->sum('cr_amount');
        $total_remain_peski = (float)$total_peski - (float)$total_clearance;

        $total_last_peski = $UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 7)->sum('dr_amount');

        $total_last_clearance = $UpToThisVoucherDetailList->where('dr_or_cr', 2)->where('ledger_type_id', 12)->sum('cr_amount');
        $total_last_remain_peski = (float)$total_last_peski - (float)$total_last_clearance;


        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id($datas['office']);
        return view('frontend.report.malepaFourteen', compact('program', 'year', 'month', 'karmachariAdvance', 'total_remain_peski', 'total_current_peski', 'voucher_signature', 'karmachari_remain_peski', 'upaBhokta_remain_peski', 'thekedar_remain_peski', 'sasthagat_remain_peski', 'byaktigat_remain_peski', 'upaBhokta_last_remain_peski', 'thekedar_last_remain_peski', 'byaktigat_last_remain_peski', 'sasthagat_last_remain_peski', 'karmachari_last_remain_peski', 'total_last_remain_peski', 'total_peski', 'total_remain_peski'));
    }

    public function malepa_seventeen_param()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.malepaSeventeenParam', compact('programs', 'fiscalYear', 'fiscalYears'));
    }

    public function malepa_seventeen_report(Request $request)
    {
        $datas = $request->all();
        if ($datas['program'] == 1234 and $datas['source_type'] == 123) {

            $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($datas['fiscal_year']);
            $budgetSubHeadLists = $this->programRepo->get_by_office_id_for_ministry($datas['office_id'], $datas['fiscal_year']);
            $granTotalInitialBudget = $this->budgetRepo->getTotalInitialBudgetByOffice($datas['office_id'], $datas['fiscal_year']);
            $granTotalAddBudget = $this->budgetRepo->getTotalAddBudgetByOffice($datas['office_id'], $datas['fiscal_year']);
            $granReduceAddBudget = $this->budgetRepo->getTotalReduceBudgetByOffice($datas['office_id'], $datas['fiscal_year']);
            $finalBudget = $this->budgetRepo->getFinalBudget($datas['office_id'], $datas['fiscal_year']);
            $totalExpense = $this->voucheDetailsRrepo->getTotalExpenseByOffice($datas['office_id'], $datas['fiscal_year']);
            $totalRemainBudget = (float)$finalBudget - (float)$totalExpense;
            return view('frontend.report.malepaSeventeenAllBudgetSubAndAllSources', compact('fiscalYear', 'budgetSubHeadLists', 'datas', 'granTotalInitialBudget', 'granTotalAddBudget', 'granReduceAddBudget', 'finalBudget', 'totalExpense', 'totalRemainBudget'));
        }
        if ($datas['program'] != 1234 and $datas['source_type'] != 123) {

            $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($datas['fiscal_year']);
            $program = $this->programRepo->get_by_program_id($datas['program']);
            $office = Office::where('id', $datas['office_id'])->get();
            $sourceTypes = $this->sourcetypeRepo->getSourceByOfficeAndBudgetSubHeadAndSourceType($datas['office_id'], $datas['fiscal_year'], $datas['program'], $datas['source_type']);
            return view('frontend.report.malepaSeventeenNotAllBudgetSubNotAllSources', compact('fiscalYear', 'program', 'office', 'sourceTypes', 'datas'));
        }

        if ($datas['program'] != 1234 and $datas['source_type'] == 123) {

            $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($datas['fiscal_year']);
            $program = $this->programRepo->get_by_program_id($datas['program']);
            $office = Office::where('id', $datas['office_id'])->get();
            $sourceTypes = $this->sourcetypeRepo->getSourceByOfficeAndBudgetSubHead($datas['office_id'], $datas['fiscal_year'], $datas['program']);
            $granTotalInitialBudget = $this->budgetRepo->getTotalInitialBudgetByBudgetSubHead($datas['fiscal_year'], $datas['program']);
            $granTotalAddBudget = $this->budgetRepo->getTotalAddBudgetByBudgetSubHead($datas['fiscal_year'], $datas['program']);
            $granReduceAddBudget = $this->budgetRepo->getTotalReduceBudgetByBudgetSubHead($datas['fiscal_year'], $datas['program']);
            $finalBudget = $this->budgetRepo->getFinalBudgetByBudgetSubHead($datas['office_id'], $datas['fiscal_year'], $datas['program']);
            $totalExpense = $this->voucheDetailsRrepo->getTotalExpenseByBudgetSubHead($datas['office_id'], $datas['fiscal_year'], $datas['program']);
            $totalRemainBudget = (float)$finalBudget - (float)$totalExpense;

            return view('frontend.report.malepaSeventeenNotAllBudgetSubHeadButAllSource', compact('fiscalYear', 'program', 'office', 'sourceTypes', 'datas', 'granTotalInitialBudget', 'granTotalAddBudget', 'granReduceAddBudget', 'finalBudget', 'totalExpense', 'totalRemainBudget'));
        }
    }

    public function budgetSheetParam()
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.budgetKhataParam', compact('programs', 'fiscalYear', 'fiscalYears'));
    }

    public function budgetSheet(Request $request)
    {
        $datas = $request->all();
        $budget_sub_head_id = $datas['budget_sub_head'];
        $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);
        $vouchers = Voucher::where('office_id', $datas['office_id'])
            ->where('fiscal_year', $datas['fiscal_year'])
            ->where('budget_sub_head_id', $datas['budget_sub_head'])
            ->where('status', '=', 1)
            ->get();
        $thisMonthVouchers = $vouchers->where('month', $datas['month']);
        $thisMonthTotalNikasa = 0;
        $upToPreMonthTotalNikasa = 0;
        $thisMonthTotalExpense = 0;
        $totalNikasa = 0;
        $upToPreMonthTotalExpense = 0;
        $totalExpense = 0;
        $nikasaCount = 0;
        foreach ($thisMonthVouchers as $monthVoucher) {
            $nikasa = $monthVoucher->details->where('dr_or_cr', 2)->where('ledger_type_id', 8);
            if ($nikasa->count() > 0) {
                $nikasaCount++;
            }
        }
        foreach ($vouchers as $voucher) {
            if ($voucher->month == $datas['month']) {
                $thisMonthTotalNikasa += $voucher->details->where('dr_or_cr', 2)->where('ledger_type_id', 8)->sum('cr_amount');

                $thisMonthTotalExpense = ActivityReportDetail::join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
                    ->join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
                    ->where('vouchers.status', '=', 1)
                    ->where('activity_report.budget_sub_head_id', $datas['budget_sub_head'])
                    ->where('activity_report_details.fiscal_year', '=', $datas['fiscal_year'])
                    ->where('activity_report_details.month', '=', $datas['month'])
                    ->sum('activity_report_details.expense');
            }

            if ($voucher->month <= ($datas['month'] - 1)) {

                $upToPreMonthTotalNikasa += $voucher->details->where('dr_or_cr', 2)->where('ledger_type_id', 8)->sum('cr_amount');

                $upToPreMonthTotalExpense = ActivityReportDetail::join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
                    ->join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
                    ->where('vouchers.status', '=', 1)
                    ->where('activity_report.budget_sub_head_id', $datas['budget_sub_head'])
                    ->where('activity_report_details.fiscal_year', '=', $datas['fiscal_year'])
                    ->where('activity_report_details.month', '<=', $datas['month'] - 1)
                    ->sum('activity_report_details.expense');
            }
            if ($voucher->month <= $datas['month']) {
                $totalNikasa += $voucher->details->where('dr_or_cr', 2)->where('ledger_type_id', 8)->sum('cr_amount');
            }

            $totalExpense = (float)$thisMonthTotalExpense + (float)$upToPreMonthTotalExpense;
        }
        $totalBudget = $this->budgetRepo->getTotalBudgetByBudgetSubHead($datas['office_id'], $datas['fiscal_year'], $datas['budget_sub_head']);
        $remainBudget = (float)$totalBudget - (float)$totalExpense;
        $month_post = $datas['month'];
        $month = Month::findorfail($month_post);
        if ($month) {
            $month_name = ($month->name);
            $month_id = $month->id;
        }
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($datas['fiscal_year']);

        //        voucherDetails repo भए पनि data Budget बाटै तानेको छ। पछि मिलाउने गरी
        $budgetByExpenseHeads = $this->voucheDetailsRrepo->get_all_voucher_by_group_by_expense_head($datas['budget_sub_head'], $datas['fiscal_year']);
        $office_id = Auth::user()->office_id;
        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id($office_id);
        return view('frontend.report.budgetKhata2', compact('fiscalYear', 'program', 'datas', 'month_name', 'budgetByExpenseHeads', 'thisMonthVouchers', 'vouchers', 'thisMonthTotalNikasa', 'upToPreMonthTotalNikasa', 'totalNikasa', 'thisMonthTotalExpense', 'upToPreMonthTotalExpense', 'totalExpense', 'totalBudget', 'remainBudget', 'nikasaCount', 'voucher_signature'));
    }

    public function getKhataByKhataPrakar($fiscal_year, $budget_sub_head_id, $ledger_type_id)
    {
        if ($ledger_type_id == 1) {
            return $this->voucheDetailsRrepo->get_expense_head_by_budget_sub_head_and_office_id($fiscal_year, $budget_sub_head_id, $ledger_type_id);
            // return json_encode($expenseHeadList);
        } else if ($ledger_type_id == 2) {

            return $this->voucheDetailsRrepo->get_liability_by_budget_sub_head_and_office_id($fiscal_year, $budget_sub_head_id, $ledger_type_id);
            // return json_encode($expenseHeadList);
        } else if ($ledger_type_id == 3) {

            return $this->preBuktaniRrepo->get_parties_by_budget_head_and_office($fiscal_year, $budget_sub_head_id);
            // return json_encode($bhuktani_list);
        } else if ($ledger_type_id == 4) {

            return $this->voucheDetailsRrepo->get_peski_party_by_budget_head_and_office($fiscal_year, $budget_sub_head_id, $ledger_type_id);
            // return json_encode($peski_party_list);
        } else if ($ledger_type_id == 5) {

            return $this->voucheDetailsRrepo->get_activities_by_budget_head_and_office($fiscal_year, $budget_sub_head_id, $ledger_type_id);
            // return json_encode($activitiesList);
        } else if ($ledger_type_id == 6) {

            return $this->preBuktaniRrepo->get_parties_by_budget_head_and_office($fiscal_year, $budget_sub_head_id);
            // return json_encode($bhuktani_list);
        }
    }

    public function malepa_twentyTwo_param()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.malepaTwentyTwoParam', compact('programs', 'fiscalYear', 'fiscalYears'));
    }

    public function malepa_tweentyTwo_report(Request $request)
    {
        $sn = 1;
        $data = $request->all();
        $budgetSubHead = $this->programRrepo->getById($data['program']);
        $office_id = $data['office'];
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($data['fiscal_year']);
        if ($data['month'] != 1234 and $data['month'] != null) {
            $month = Month::findorfail($data['month']);
            if ($data['month'] <= 9) {
                $year = substr($fiscalYear->year, 0, 4);
            } else {
                $year = substr($fiscalYear->year, 0, 4) + 1;
            }
        }
        if ($data['khata-prakar'] == 1) {
            if ($data['khata'] != 1234) {
                $expenseHead = ExpenseHead::findorfail($data['khata']);
            }
            if ($data['khata'] == 1234 and $data['month'] != 1234) {
                //                खाता All
                $program = $this->programRepo->get_by_program_id($data['program']);
                $vouchers = $this->activityReportDetailRepo->getVoucher($data);
                return view('frontend.report.malepa22.malepa22_karcha_all', compact('program', 'vouchers', 'data'));
            } else if ($data['khata'] == 1234 and $data['month'] == 1234) { {
                    //                खाता All
                    $program = $this->programRepo->get_by_program_id($data['program']);
                    $vouchers = $this->activityReportDetailRepo->getVoucher($data);
                    return view('frontend.report.malepa22.malepa22_karcha_all', compact('program', 'vouchers', 'data'));
                }
            } else if ($data['khata'] != 1234 and $data['month'] == 1234) {
                $program = $this->programRepo->get_by_program_id($data['program']);
                $dr_amount = 0;
                $cr_amount = 0;
                $vouchers = $this->activityReportDetailRepo->getAllMonthVoucherGroupByJvNumber($data, $expenseHead);
                $vouchers2 = $this->activityReportDetailRepo->getAllMonthVoucher($data, $expenseHead);
                $collection = collect($vouchers);
                $collection2 = collect($vouchers2);
                $totalExpense = $collection2->sum('expense');
                $vouchersLists = $this->voucheDetailsRrepo->get_all_voucher_by_budget_sub_head_and_ledger_type_id($data['fiscal_year'], $data['program'], $data['khata']);
                foreach ($vouchersLists as $voucher) {
                    $dr_amount = ($voucher->dr_amount);
                    $cr_amount = ($voucher->cr_amount);
                    $remain = (float)$dr_amount - (float)$cr_amount;
                }
                $dr_toptal = $vouchersLists->sum('dr_amount');
                $cr_toptal = $vouchersLists->sum('cr_amount');
                $total_remain = (float)$dr_toptal - (float)$cr_toptal;
                $expense_head_sirsak = ($vouchersLists[0]->expense_head->expense_head_sirsak);
                $expense_head_code = ($vouchersLists[0]->expense_head->expense_head_code);

                return view('frontend.report.malepa22.malepa22_karcha_one_month_all', compact('vouchersLists', 'data', 'month', 'year', 'program', 'expense_head_sirsak', 'expense_head_code', 'dr_toptal', 'cr_toptal', 'total_remain', 'remain', 'vouchers', 'totalExpense'));
            } else {

                $program = $this->programRepo->get_by_program_id($data['program']);
                $dr_amount = 0;
                $cr_amount = 0;
                $data['khata'] = $data['khata'];

                $vouchers = $this->activityReportDetailRepo->getThisMonthVoucherGroupByJvNumber($data, $expenseHead);
                $vouchers2 = $this->activityReportDetailRepo->GetThisMonthVoucher($data, $expenseHead);
                $collection = collect($vouchers);
                $collection2 = collect($vouchers2);
                $totalExpense = $collection2->sum('expense');
                $vouchersLists = $this->voucheDetailsRrepo->get_all_voucher_by_budget_sub_head_and_ledger_type_id($data['fiscal_year'], $data['program'], $data['khata']);
                foreach ($vouchersLists as $voucher) {
                    $dr_amount = ($voucher->dr_amount);
                    $cr_amount = ($voucher->cr_amount);
                    $remain = (float)$dr_amount - (float)$cr_amount;
                }
                $dr_toptal = $vouchersLists->sum('dr_amount');
                $cr_toptal = $vouchersLists->sum('cr_amount');
                $total_remain = (float)$dr_toptal - (float)$cr_toptal;
                $expense_head_sirsak = ($vouchersLists[0]->expense_head->expense_head_sirsak);
                $expense_head_code = ($vouchersLists[0]->expense_head->expense_head_code);


                return view('frontend.report.malepa22.malepa22_karcha', compact('vouchersLists', 'data', 'month', 'year', 'program', 'expense_head_sirsak', 'expense_head_code', 'dr_toptal', 'cr_toptal', 'total_remain', 'remain', 'vouchers', 'totalExpense'));
            }
        }
        if ($data['khata-prakar'] == 2) {

            if ($data['khata'] == 1234) {
                //                खाता All
                $liabilityHeads = $this->voucheDetailsRrepo->getLiabilityHeadsByBudgetSubHead($data);
                $totalDrAmount = ($liabilityHeads->sum('total_dr_amount'));
                $totalCrAmount = ($liabilityHeads->sum('total_cr_amount'));
                $remain_total = (float)$totalDrAmount - (float)$totalCrAmount;
                if ($remain_total < 0) {

                    $remain_total = (float)((-1) * (float)$remain_total);
                    $remain_total = number_format($remain_total, 2);
                    $remain_total = '(' . $remain_total . ')';
                } else {

                    $remain_total = $remain_total;
                }
                return view('frontend.report.malepa22.liability_all', compact('data', 'liabilityHeads', 'totalDrAmount', 'totalCrAmount', 'remain_total'));
            } else {
                $liabilityLists = [];
                $liabilityLists = $this->voucheDetailsRrepo->get_liability_expenses_by_liability_id($data);
                if ($liabilityLists->count() > 0) {
                    $dr_amount_total = $liabilityLists->sum('dr_amount');
                    $cr_amount_total = $liabilityLists->sum('cr_amount');
                    $remain_total_temp = (float)$dr_amount_total - (float)$cr_amount_total;
                    if ($remain_total_temp < 0) {

                        $remain_total = (float)((-1) * (float)$remain_total_temp);
                        $remain_total = number_format($remain_total, 2);
                        $remain_total = '(' . $remain_total . ')';
                    } else {

                        $remain_total = $remain_total_temp;
                    }
                    $expense_head_code = (float)($liabilityLists[0]->expense_head->expense_head_code);
                    $expense_head_sirsak = $liabilityLists[0]->expense_head->expense_head_sirsak;
                }
                return view('frontend.report.malepa22.liability_expense', compact('liabilityLists', 'data', 'expense_head_code', 'expense_head_sirsak', 'dr_amount_total', 'cr_amount_total', 'remain_total', 'fiscalYear'));
            }
        }
        if ($data['khata-prakar'] == 3) {
            if ($data['khata'] == 1234) {
                return view('frontend.report.malepa22.advance_receiver_all');
            } else {
                $preBhuktaniList = $this->preBuktaniRrepo->get_party_by_party_id($data);
                $total_bhuktani = $preBhuktaniList->sum('amount');
                $total_advance_vat_deduction = $preBhuktaniList->sum('advance_vat_deduction');
                $total_vat_amount = $preBhuktaniList->sum('vat_amount');
                $party_name = $preBhuktaniList[0]->party->name_nep;
                return view('frontend.report.malepa22.payment_receiver', compact('preBhuktaniList', 'data', 'party_name', 'total_bhuktani', 'total_advance_vat_deduction', 'total_vat_amount', 'fiscalYear'));
            }
        }
        if ($data['khata-prakar'] == 4) {
            $remain = 0;
            $peskiDetails = $this->voucheDetailsRrepo->getPeskiDetailsByParty($data);
            if ($data['khata'] == 1234 and $data['month'] == 1234) {
            } else if ($data['khata'] == 1234 and $data['month'] != 1234) {

                $party_name = ($peskiDetails[0]->advancePayment->name_nep);
                $dr_amount_total = $peskiDetails->sum('dr_amount');
                $cr_amount_total = $peskiDetails->sum('cr_amount');
                $total_remain = (float)$dr_amount_total - (float)$cr_amount_total;
                return view('frontend.report.malepa22.peski', compact('peskiByMonth', 'data', 'dr_amount_total', 'cr_amount_total', 'party_name', 'total_remain', 'budgetSubHead'));
            } else if ($data['khata'] != 1234 and $data['month'] == 1234) {

                $party_name = ($peskiDetails[0]->advancePayment->name_nep);
                $dr_amount_total = $peskiDetails->sum('dr_amount');
                $cr_amount_total = $peskiDetails->sum('cr_amount');
                $total_remain = (float)$dr_amount_total - (float)$cr_amount_total;
                return view('frontend.report.malepa22.peski_one_party_all_month', compact('peskiDetails', 'data', 'dr_amount_total', 'cr_amount_total', 'party_name', 'total_remain', 'budgetSubHead', 'fiscalYear'));
            } else {

                $peskiDetails = $this->voucheDetailsRrepo->getPeskiDetailsByParty($data);
                $peskiByMonth = $peskiDetails->where('month', '<=', $data['month']);
                $party_name = ($peskiDetails[0]->advancePayment->name_nep);
                $dr_amount_total = $peskiByMonth->sum('dr_amount');
                $cr_amount_total = $peskiByMonth->sum('cr_amount');
                $total_remain = (float)$dr_amount_total - (float)$cr_amount_total;
                return view('frontend.report.malepa22.peski_one_party_one_month', compact('peskiByMonth', 'data', 'dr_amount_total', 'cr_amount_total', 'party_name', 'total_remain', 'budgetSubHead', 'fiscalYear'));
            }
        }
        if ($data['khata-prakar'] == 5) {
            $remain = 0;
            if ($data['khata'] == 1234) {
            } else {

                $activities = $this->activityReportDetailRepo->getVoucherActivity($data);
                $program = $this->programRepo->get_by_program_id($data['program']);
                $totalExpense = $activities->sum('expense');
                return view('frontend.report.malepa22.malepa22_activity', compact('activities', 'totalExpense', 'program'));
            }
        }
        if ($data['khata-prakar'] == 6) {
            if ($data['khata'] == 1234) {
            } else {

                $preBhuktaniList = $this->preBuktaniRrepo->get_party_by_party_id($data);
                $total_bhuktani_amount = $preBhuktaniList->sum('amount');
                $total_advance_vat_deduction = $preBhuktaniList->sum('advance_vat_deduction');
                $total_vat_amount = $preBhuktaniList->sum('vat_amount');
                $party_name = $preBhuktaniList[0]->party->name_nep;

                return view('frontend.report.malepa22.vat', compact('data', 'party_name', 'preBhuktaniList', 'total_bhuktani_amount', 'total_advance_vat_deduction', 'total_vat_amount'));
            }
        }
    }

    public function getActivityParam()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_programs_for_budget();
        return view('frontend.report.activityParam', compact('fiscalYear', 'programs', 'fiscalYears'));
    }

    public function getActivitySummaryParam()
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_programs_for_budget();
        return view('frontend.report.activitySummaryParam', compact('fiscalYear', 'programs'));
    }

    public function getActivityDetailParam()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_programs_for_budget();
        return view('frontend.report.activityDetailParam', compact('fiscalYear', 'programs', 'fiscalYears'));
    }

    public function getActivityDetail(Request $request)
    {
        $datas = $request->all();
        $office_id = Auth::user()->office_id;
        $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($datas['fiscal_year']);
        $activities = $this->budgetRrepo->get_activities_by_budget_sub_head_and_budget_type($datas['fiscal_year'], $datas['budget_sub_head'], $office_id);
        return view('frontend.report.activityDetailList', compact('program', 'fiscalYear', 'datas', 'activities'));
    }

    public function getActivityByBudgetSubHead(Request $request)
    {
        $datas = $request->all();
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($datas['fiscal_year']);
        $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);
        $activities = $this->budgetRrepo->get_activities_by_budget_sub_head_and_budget_type($datas['fiscal_year'], $datas['budget_sub_head'], $datas['office']);
        $budget_sum = $activities->sum('total_budget');
        $first_quarter_budget_total = $activities->sum('first_quarter_budget');
        $second_quarter_budget_total = $activities->sum('second_quarter_budget');
        $third_quarter_budget_total = $activities->sum('third_quarter_budget');

        $total_first_quarter_expense = $this->voucheDetailsRrepo->getFirstChaumasikTotalExpense($datas['office'], $datas['budget_sub_head'], $datas['fiscal_year']);
        $total_second_quarter_expense = $this->voucheDetailsRrepo->getSecondChaumasikTotalExpense($datas['office'], $datas['budget_sub_head'], $datas['fiscal_year']);
        $total_third_quarter_expense = $this->voucheDetailsRrepo->getThirdChaumasikTotalExpense($datas['office'], $datas['budget_sub_head'], $datas['fiscal_year']);
        $totalExpenseByBudgetSubHead = (float)$total_first_quarter_expense + (float)$total_second_quarter_expense + (float)$total_third_quarter_expense;
        return view('frontend.report.activityList', compact('program', 'fiscalYear', 'datas', 'activities', 'budget_sum', 'first_quarter_budget_total', 'second_quarter_budget_total', 'third_quarter_budget_total', 'total_first_quarter_expense', 'total_second_quarter_expense', 'total_third_quarter_expense', 'totalExpenseByBudgetSubHead'));
    }

    public function getActivitySummary(Request $request)
    {

        $datas = $request->all();
        $office_id = Auth::user()->office_id;
        $budget_sub_head_id = $datas['budget_sub_head'];
        $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);
        $fiscalYear = $datas['fiscal_year'];


        //        $activities = $this->budgetRrepo->get_activities_by_budget_sub_head_and_budget_type($budget_sub_head_id, $office_id);
        //        $budget_sum = $activities->sum('total_budget');
        $activities = ActivityReport::where('budget_sub_head_id', $datas['budget_sub_head'])->get();
        $totalBudget = $activities->sum('budget');
        $totalExpense = $activities->sum('expense');
        $totalRemain = $activities->sum('remain');

        return view('frontend.report.activitySummaryList', compact('program', 'fiscalYear', 'datas', 'activities', 'totalBudget', 'totalExpense', 'totalRemain'));
    }

    public function goswaraDharautiKhataParameter()
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.report.goswara_dharauti_khata_param', compact('fiscalYear'));
    }

    public function goswaraDharautiKhata(Request $request)
    {

        $requestData = $request->all();
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id(($requestData['fiscal_year']));
        $retentionVouchers = $this->retentionVoucherRepo->get_by_office_id($requestData['office']);
        if ($retentionVouchers) {
            foreach ($retentionVouchers as $details) {
                $totalRetentionIncome = $retentionVouchers
                    ->where('dr_or_cr', 2)
                    ->where('byahora', 9)
                    ->where('hisab_number', 395)
                    ->sum('amount');


                $totalRetentionReturn = $retentionVouchers
                    ->where('dr_or_cr', 1)
                    ->where('byahora', 9)
                    ->where('hisab_number', 397)
                    ->sum('amount');

                $totalRetentionCollaps = $retentionVouchers
                    ->where('dr_or_cr', 1)
                    ->where('byahora', 9)
                    ->where('hisab_number', 398)
                    ->sum('amount');
            }
        }

        return view('frontend.report.goswara_dharauti_khata', compact('fiscalYear', 'retentionVouchers', 'totalRetentionIncome', 'totalRetentionReturn', 'totalRetentionCollaps'));
    }

    public function byaktigatDharautiKhataParam()
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $office_id = Auth::user()->office_id;
        $depositorTypes = $this->retentionVoucherRepo->get_party_by_office_id($office_id);
        return view('frontend.report.byaktigat_dharauti_khata_param', compact('fiscalYear', 'depositorTypes'));
    }

    public function byaktigatDharautiKhata(Request $request)
    {
        $postData = $request->all();
        $retentionDatas = $this->retentionVoucherRepo->getDetailsByDepositor($postData['depositor'], $postData['office']);
        foreach ($retentionDatas as $details) {
            $totalRetentionIncome = $retentionDatas
                ->where('dr_or_cr', 2)
                ->where('byahora', 10)
                ->where('hisab_number', 395)
                ->sum('amount');

            $totalRetentionReturn = $retentionDatas
                ->where('dr_or_cr', 2)
                ->where('byahora', 10)
                ->where('hisab_number', 397)
                ->sum('amount');
            $totalRetentionCollaps = $retentionDatas
                ->where('dr_or_cr', 2)
                ->where('byahora', 10)
                ->where('hisab_number', 398)
                ->sum('amount');
        }
        return view('frontend.report.byaktigat_dharauti_khata', compact('retentionDatas', 'totalRetentionIncome', 'totalRetentionReturn', 'totalRetentionCollaps', 'postData'));
    }

    public function dharautiCashBookParam()
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.report.dharauti_cash_book_param', compact('fiscalYear'));
    }

    public function dharautiCashBook(Request $request)
    {
        $datas = $request->all();
        $month = Month::findorfail($datas['month']);
        if ($month) {
            $month_name = ($month->name);
        }
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($datas['fiscal_year']);
        if ($datas['month'] <= 9) {
            $year = substr($fiscalYear->year, 0, 4);
        } else {
            $year = substr($fiscalYear->year, 0, 4) + 1;
        }
        $retentionVouchers = $this->retentionVoucherRepo->getVoucherByMonth($datas);
        $retentionThisVouchers = $this->retentionVoucherRepo->search(["fiscal_year" => $datas['fiscal_year'], "month" => $datas['month']]);
        $retentionPreVouchers = $this->retentionVoucherRepo->search(["fiscal_year" => $datas['fiscal_year'], "less_than_this" => $datas['month']]);
        $retentionUpToVouchers = $this->retentionVoucherRepo->search(["fiscal_year" => $datas['fiscal_year'], "up_to_this" => $datas['month']]);

        $thisMonthTotal = $this->retentionVoucherRepo->getAllTotal($retentionThisVouchers);
        $preMonthTotal = $this->retentionVoucherRepo->getAllTotal($retentionPreVouchers);
        $upToMonthTotal = $this->retentionVoucherRepo->getAllTotal($retentionUpToVouchers);
        $upTolastMonthRetentionVouchers = $this->retentionVoucherRepo->getVoucherByMonth($datas);
        return view('frontend.report.dharauti_cash_book', compact('year', 'month_name', 'fiscalYear', 'retentionVouchers', 'preMonthReceiveRetention', 'preMonthReturnRetention', 'thisMonthTotal', 'preMonthTotal', 'upToMonthTotal'));
    }

    public function retentionDharautiParam()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.report.retentionBhuktaniParam', compact('fiscalYear'));
    }

    public function getRetentionBhuktani(Request $request)
    {
        $parameter = $request->all();
        $bhuktani = $this->retentionBhuktaniRepo->get_by_office($parameter);
        return json_encode($bhuktani);
    }

    public function retentionBhuktaniAdeshReport($id)
    {
        $bhuktani = $this->retentionBhuktaniRepo->getById($id);
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($bhuktani->fiscal_year);
        $bhuktani_amount_word = $this->retentionBhuktaniRepo->get_amount_in_word($bhuktani['amount']);
        return view('frontend.report.retention_bhuktaniReport', compact('bhuktani', 'bhuktani_amount_word', 'fiscalYear'));
    }

    public function progressReportParam()
    {

        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_programs_for_budget();
        return view('frontend.report.progressReportParam', compact('fiscalYear', 'programs', 'fiscalYears'));
    }

    public function progressReport(Request $request)
    {

        $data = $request->all();
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($data['fiscal_year']);
        $budget_sub_head_id = $data['budget_sub_head'];
        $program = $this->programRepo->get_by_program_id($data['budget_sub_head']);
        $activities = $this->budgetRrepo->get_activities_by_budget_sub_head_and_budget_type($data['fiscal_year'], $budget_sub_head_id, $data['office']);
        $totalAkhtiyari = $this->budgetRepo->get_total_akhtiyari_by_budget_sub_head($data['fiscal_year'], $data['budget_sub_head']);
        $budget_sum = $activities->sum('total_budget');
        $progressReport = $this->activityProgressRepo->getByBudgetSubHead($data);
        return view('frontend.report.progressReport', compact('data', 'activities', 'program', 'totalAkhtiyari', 'fiscalYear'));
    }

    public function importExcel(Request $request)
    {
        return view('frontend.report.import_form');
    }

    public function storeExcel(Request $request)
    {
        Excel::import(new BudgetImport(), request()->file('excel'));
    }
}
