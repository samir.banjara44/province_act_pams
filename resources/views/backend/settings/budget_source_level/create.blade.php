
@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        बजेट स्रोत तह प्रविश्टि
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('admin.area')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('admin.budget.source.level.store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <label for="sourceType">स्रोतको प्रकार:</label>
             <select class="form-control" id="sourceType" name="sourceType">
               @foreach($sourceTypes as $sourceType)
               <option value="{{$sourceType->id}}">{{$sourceType->name}}</option>
               @endforeach
             </select>
            </div>
            <div class="form-group">
              <label for="name">बजेट स्रोत तह:</label>
              <input type="text" class="form-control" id="name" placeholder="बजेट स्रोत तह" name="name">
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection