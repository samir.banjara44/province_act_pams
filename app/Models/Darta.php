<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Darta extends Model
{
    protected $table = 'darta_chalani';
    protected $guarded = [];

    public function getBranch(){

        return $this->belongsTo('App\Models\Branch','branch','id');
    }

    public function GetUser()
    {
        return $this->belongsTo('App\Models\User','created_by','id');
    }
}
