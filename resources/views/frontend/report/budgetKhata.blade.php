<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
{{--<link href="{{ asset('css/nepali-datepicker.css') }}" rel="stylesheet">--}}
<link rel="stylesheet" href="{{asset('css/styles.css')}}">
<style>
    table {
        border-collapse: collapse;
        table-layout: fixed;
        font-size: 12px !important;
    }

    body {
        overflow-x: scroll !important;
    }

    .table {
        width: 100%;
        overflow-x: scroll;
    }

    .table2 {
        width: auto;
        overflow-x: scroll;
    }

    td {
        min-height: 10px;
    }

    body {

        overflow-x: hidden;
    }

    .floatLeft {
        width: 50%;
        float: left;
    }

    .floatRight {
        width: 50%;
        float: right;
    }

    .container {
        overflow: hidden;
    }

    @media print {
        #downloadMe {
            display: none;
        }
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <button class="fixPrintBtnRightTop pull-right noprint" onclick="window.print()">Print</button>
    <input type="button" style="position: absolute; top:20px;right: 20px" value="Export to Excel" id="downloadMe">
    <input type="button" style="position: absolute; top:20px;right: 130px;color: blue" value="Fix Report" id="fixReport">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div id="tableHeadingWrapper">
            <table class="table" style="border-collapse: collapse; font-size:13px; width: 100%">
                <tr>
                    <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                    <td colspan="6" style="text-align: center"><b>@if((Auth::user()->office->id != 43) or  (Auth::user()->office->id != 47))प्रदेश सरकार@else प्रदेश सभा @endif</b></td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">
                        @if(Auth::user()->office->department)
                            {{Auth::user()->office->department->name}}</td>
                    @endif
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->name}}</b></td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>
                </tr>
                <tr>
                    <td class="kalimati" colspan="6" style="text-align: center"><b>कार्यालय कोड
                            नं. <span class="e-n-t-n-n">{{Auth::user()->office->office_code}}</span></b></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div style="width: 100%; text-align: center">
                            <b>बजेट खाता</b>
                        </div>
                        <div style="float: right; margin-top: -20px">
                            म.ले.प.फा.नं. २०८
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td class="kalimati" >आर्थिक वर्ष :{{$datas['fiscal_year']}} <span class="e-n-t-n-n"></span><br>
                    </td>
                </tr>
                <tr>
                    <td class="">बजेट उप शीर्षक : {{$program->name}}</td>
                </tr>
            </table>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $b_count = $budgetByExpenseHeads->count() ?>
        <?php $v_count = $thisMonthVouchers->count() + 2 ?>
        <div style="position: relative" id="tableWrapper">
            <table class="table" border="1">
                <tr>
                    <td style="width: 45px">
                        प्रथम खण्ड
                    </td>
                    <td style="width: 50px">बजेट</td>
                    <td style="width: 500px;">
                        <table style="width: 500px!important; font-size: 13px!important;margin-top: 18px">
                            <tr>
                                <td colspan="4" style="text-align:left;height: 23px">खर्च उपशिर्षकको नाम</td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align:left;height: 100px"></td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align:left;height: 23px">स्विकृत आर्थिक वर्षको बजेट बिनियोजन
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align:left;height: 23px">स्विकृत आर्थिक वर्षको थप बजेट
                                    बिनियोजन
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align:left;height: 23px">रकमान्तरबाट थप/घट रकम</td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align:left;height: 23px">श्रोतान्तरबाट थप/घट</td>

                            </tr>
                            <tr>
                                <td colspan="3" style="text-align:left;height: 23px;border-top: 1px solid #afafaf;">
                                    <b>जमा कायम भएको बजेट</b></td>

                                <td class="kalimati">{{number_format($totalBudget,2)}}</td>
                            </tr>

                        </table>
                    </td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td style="vertical-align: top; padding: 0!important; margin: 0; width: 130px">
                            <table class="table" style="width: 130px!important;">
                                <tr>
                                    <td class="kalimati"
                                        style="vertical-align: middle;text-align:center; border-bottom: 1px solid #000; height: 23px">{{$budgetExpenseHead->expense_head}}</td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle;text-align:center; border-bottom: 1px solid #000;  height: 100px">
                                        {{$budgetExpenseHead->expense_by_expense_head->expense_head_sirsak}}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="kalimati"
                                        style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;  height: 23px">
                                        {{number_format($budgetExpenseHead->getInitialBudget($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id),2)}}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="kalimati"
                                        style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;  height: 23px">
                                        {{number_format($budgetExpenseHead->getAddBudget($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id),2)}}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="kalimati"
                                        style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;  height: 23px">
                                        {{number_format($budgetExpenseHead->getReduceBudget($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id),2)}}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="kalimati"
                                        style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;  height: 23px">
                                        {{number_format($budgetExpenseHead->getReduceBudget($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id),2)}}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($budgetExpenseHead->getFinalBudget($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id),2)}}</td>
                                </tr>
                            </table>
                        </td>
                    @endforeach
                </tr>

            </table>

            <table class="table" border="1">
                <tr>
                    <td rowspan="{{$v_count+3}}" style="width: 45px; border-top: 1px solid white">
                        द्वितिय खण्ड
                    </td>
                    <td rowspan="{{$v_count+3}}" style="width: 50px; border-top: 1px solid white">निकासा</td>
                    <td style="width: 100px;border-top: 1px solid white">मिति</td>
                    <td style="width: 50px;border-top: 1px solid white">भौ न</td>
                    {{--                    <td><p style="width: 60px">संकेत न.</p></td>--}}
                    <td style="width: 200px;border-top: 1px solid white">विवरण</td>
                    <td style="width: 118px;border-top: 1px solid white">जम्मा</td>
                    @for($i=0; $i<$b_count; $i++)
                        <td style="vertical-align: top; padding: 0!important; margin: 0; width: 130px;border-top: 1px solid white">
                        </td>
                    @endfor
                </tr>
                @foreach ($thisMonthVouchers as $voucher)
                    @if($voucher->isNikasa())
                        <tr>
                            <td style="text-align: center">{{$voucher->data_nepali}}</td>
                            <td class="" style="text-align: center"><a class="kalimati" href="{{route('voucher.view',['id'=>$voucher->id])}}" target="_blank">{{$voucher->jv_number}}</a></td>
                            {{--                                    <td>2</td>--}}
                            <td>{{$voucher->short_narration}}</td>
                            <td class="kalimati" style="text-align: right">{{number_format($voucher->payement_amount,2)}}</td>
                            @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                                <td class="kalimati"
                                    style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                                    {{number_format($budgetExpenseHead->get_nikasa_by_budget_expense_head_id($voucher->id,$budgetExpenseHead->expense_head_id),2)}}
                                </td>
                            @endforeach
                        </tr>
                    @endif
                @endforeach
                <tr>
                    <td colspan="3" style="text-align: right"><b>यो महिनाको निकासा</b></td>
                    <td class="kalimati" style="text-align: right">{{number_format($thisMonthTotalNikasa,2)}}
                    </td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                            {{number_format($budgetExpenseHead->thisMonthNikasa($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']),2)}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right"><b>अघिल्लो महिनासम्मको निकासा</b></td>
                    <td class="kalimati" style="text-align: right">{{number_format($upToPreMonthTotalNikasa,2)}}
                    </td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                            {{number_format($budgetExpenseHead->upToPreMonthNikasa($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']-1),2)}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right"><b>जम्मा निकासा</b></td>
                    <td class="kalimati" style="text-align: right">{{number_format($totalNikasa,2)}}
                    </td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                            {{number_format($budgetExpenseHead->totalNikasa($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']),2)}}

                        </td>
                    @endforeach
                </tr>
            </table>

            <table class="table" border="1">
                <tr>
                    <td rowspan="{{$v_count+4}}" style="width: 45px; border-top: 1px solid white">
                        तृतीय खण्ड
                    </td>
                    <td rowspan="{{$v_count+4}}" style="width: 50px; border-top: 1px solid white">खर्च</td>
                    <td style="width: 100px; border-top: 1px solid white"></td>
                    <td style="width: 50px; border-top: 1px solid white"></td>
                    {{--                    <td><p style="width: 60px; -topborder: 1px solid white;">संकेत न.</p></td>--}}
                    <td style="width: 200px; border-top: 1px solid white"></td>
                    <td style="width: 118px; border-top: 1px solid white"></td>
                    @for($i=0; $i<$b_count; $i++)
                        <td style="vertical-align: top; padding: 0!important; margin: 0; width: 130px; border-top: 1px solid white">
                        </td>
                    @endfor
                </tr>
                @foreach ($thisMonthVouchers as $voucher)
                    @if($voucher->isExpense())
                        <tr>
                            <td style="text-align: center; width: 100px">{{$voucher->data_nepali}}</td>
                            <td class="" style="text-align: center; width: 50px"><a class="kalimati" href="{{route('voucher.view',['id'=>$voucher->id])}}" target="_blank">{{$voucher->jv_number}}</a></td>
                            {{--                        <td><p style="width: 60px">2</p></td>--}}
                            <td style="width: 200px">{{$voucher->short_narration}}</td>
                            <td class="kalimati"
                                style="text-align: right; width: 80px">{{number_format($voucher->thisTotalExp(),2)}}</td>
                            @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                                <td class="kalimati" style="vertical-align: top; padding: 0!important; margin: 0; text-align: right;width: 130px">
                                    {{number_format($voucher->get_kharcha_by_budget_expense_head_id($datas['budget_sub_head'],$budgetExpenseHead->expense_head,$datas['month']),2)}}
                                </td>
                            @endforeach
                        </tr>
                    @endif
                @endforeach
                <tr>
                    <td colspan="3" style="text-align: right"><b>यो महिनाको जम्मा खर्च</b></td>
                    <td class="kalimati" style="text-align: right">{{number_format($thisMonthTotalExpense,2)}}</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                            {{number_format($budgetExpenseHead->thisMonthTotalExpense($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']),2)}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right"><b>अधिल्लो महिनाकोसम्मको जम्मा खर्च</b></td>
                    <td class="kalimati" style="text-align: right">{{number_format($upToPreMonthTotalExpense,2)}}</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                            {{number_format($budgetExpenseHead->preMonthTotalExpense($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']-1),2)}}

                        </td>

                    @endforeach

                </tr>
                <tr>
                    <td colspan="3" style="text-align: right"><b>जम्मा खर्च</b></td>
                    <td class="kalimati" style="text-align: right">{{number_format($totalExpense,2)}}</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                            {{number_format($budgetExpenseHead->totalExpense($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']),2)}}

                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right"><b>बाकीँ बजेट</b></td>
                    <td class="kalimati" style="text-align: right">{{number_format($remainBudget,2)}}</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                            {{number_format($budgetExpenseHead->totalRemainBudget($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']),2)}}
                        </td>
                    @endforeach
                </tr>
            </table>
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
    $("#downloadMe").click(function (e) {
        let allTable = $('div[id$=tableHeadingWrapper]').html() + $('div[id$=tableWrapper]').html();
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(allTable));
        e.preventDefault();
    });

    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '/')

                    nepaliNo += "/";
            }
        });
        return nepaliNo;
    };

    // let change_all_to_nepali_number = function(){
    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });
    // }


</script>

<script>
    $(document).ready(function () {

        let month = $('span.month').text();
        // alert(month);

        $('#fixReport').click(function () {
            let $tables = $('.table');
            $tables.each(function () {
                if ($(this).hasClass('table')) {
                    $(this).addClass('table2');
                    $(this).removeClass('table');
                } else {
                    $(this).removeClass('table2');
                    $(this).addClass('table');
                }
            });
        })

    })
</script>

