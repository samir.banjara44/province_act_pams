<?php
namespace App\Repositories;
 
interface DesignationRepository {

    public function get_all_designations();
}