
@extends('backend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Ministry
                <small>Control panel</small>
            </h1>
            <ul class="breadcrumb">
                <li> <a type="button" href="{{route('admin.ministry.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Create</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Province</th>
                            <th>MOF</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ministries as $ministry)
                            <tr>
                                <td>{{$ministry->name}}</td>
                                <td>{{$ministry->province->name}}</td>
                                <td>{{$ministry->mof->name}}</td>
                                <td>{{$ministry->status}}</td>
                                <td>
                                    <a type="button" href="" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection