
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <form action="{{route('bhuktani.store')}}" method="post" id="bhuktaniForm">
    <section class="content-header">
      <h1><i class="fas fa-file"></i> भुक्तानी आदेश बनाउने </h1>
{{--      @foreach($programs as $program)--}}
{{--        --}}
{{--      @endforeach  --}}

      <h3><center>बजेट उपशिर्षक : {{$program->name}}</center></h3>
      <input type="hidden" id="bs-roman" name="bs-roman">
      <input type="hidden" value="{{$program->id}}" name="budget_sub_head">
      <h4><center>शिर्षक नं. : {{$program->program_code}}</center></h4>

      <a type="button" class="btn btn-md btn-primary" href="{{route('bhuktani.index.back',[$fiscalYear->id,$program->id])}}" ><i
                class="fa fa-pencil"></i>पछाडी फर्कने</a>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body pt-10">
            {{ csrf_field() }}
            <div class="mb-10 col-md-12">
              <div class="row">
                <div class="col-md-2">
                  <label>आर्थिक वर्ष : </label>
                  <select name="fiscal_year" id="fiscal_year" class="form-control">
                    <option value="{{$fiscalYear->id}}" selected>{{$fiscalYear->year}}</option>
                  </select>
                </div>

                <div class="col-md-2">
                  <label class="">मिति : </label>
                  <input type="text" class="form-control" name="date" id="date" value="">
                </div>

                <div class="col-md-2">
                  <label >आदेश नं. : </label>
                  <input type="text" name="adesh_number" class="form-control" value="{{$bhuktani_adesh_number}}" readonly>
                </div>
              </div>
            </div>
            
            <table class="table" border="1" >
              <thead>
              <tr style="background-color: #dbdbdb;">
                <th class="text-center">भौचर नं.</th>
                <th class="text-center">प्रापक</th>
                <th class="text-center">शीर्षक</th>
                <th class="text-center">स्रोत</th>
                <th class="text-center">रकम</th>
                <th class="text-center">एक्सन</th>
              </tr>
              </thead>

              <tbody>
              @if($preBhuktanies->count() > 0)
                @foreach($preBhuktanies as $preBhuktani)
                  <tr>
                    <td align="center">

                        {{$preBhuktani->voucher->jv_number}}
                    </td>
                    <td>
                      @if($preBhuktani->party)
                      {{$preBhuktani->party->name_nep}}
                      @endif
                    </td>
                    <td class="kalimati">
                      @if($preBhuktani->main_activity)
                        {{$preBhuktani->main_activity->sub_activity}} {{$preBhuktani->main_activity->expense_head_by_id->expense_head_code}}
                      @endif
                    </td>
                    <td> नेपाल सरकार</td>
                    <td>{{number_format($preBhuktani->amount,2)}}</td>
                    <td align="center">
                      <input type="checkbox" name="bhuktani[]" class="bhuktani_check_box" value="{{$preBhuktani->id}}" required disabled>
                    </td>
                  </tr>
                </tbody>
                @endforeach
              <tr>
                <td colspan="6" style="text-align: center">
                  <button class="btn btn-primary" id="btnBhuktaniSubmit">Submit</button>
                </td>
              </tr>
              @else
                <td colspan="6" style="text-align: center">डाटा उपलब्ध छैन।</td>

              @endif
            </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
    </form>
  </div>


@endsection

@section('scripts')

{{--validation--}}
<script>
  $(document).on('click','button#btnBhuktaniSubmit',function (e) {

    e.preventDefault();


  })
</script>
  {{--    Date Picker --}}
  <script>
    $("#date").nepaliDatePicker({
      dateFormat: "%y-%m-%d",
      closeOnDateSelect: true
    });

    var currentDate = new Date();
    var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
    var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
    $("#date").val(formatedNepaliDate);
  </script>



  {{--Convert Nepali date to roman date --}}
  <script>
   function convertNepaliToEnglish(input) {
      var charArray = input.split('');
      var engDate = '';
      $.each(charArray, function (key, value) {
        switch (value) {
          case '१':
            engDate += '1'
            break
          case '२':
            engDate += '2'
            break
          case '३':
            engDate += '3'
            break
          case '४':
            engDate += '4'
            break
          case '५':
            engDate += '5'
            break
          case '६':
            engDate += '6'
            break
          case '०':
            engDate += '0'
            break
          case '७':
            engDate += '7'
            break
          case '८':
            engDate += '8'
            break
          case '९':
            engDate += '9'
            break

          case '-':
            engDate += '-'
            break
        }
      })
      return engDate

    }
  </script>



  <script>
    $(document).ready(function () {
      $('.bhuktani_check_box').attr('disabled',false)
      $('#btnBhuktaniSubmit').click(function (e) {
        e.preventDefault();
        let nepali_date = $('#date').val();
        let bs_roman =  convertNepaliToEnglish(nepali_date);
       $('#bs-roman').val(bs_roman);
        if($('input.bhuktani_check_box:checked').length){

          $('#bhuktaniForm').submit();
        } else {

          alert("डाटा select भएन!!")
        }

      })
    });
  </script>


@endsection