@extends('frontend.layouts.app')
@section('content')
    <style>
       .image img {
            border: 1px solid #ddd; /* Gray border */
            border-radius: 4px;  /* Rounded border */
            padding: 5px; /* Some padding */
            width: 150px; /* Set a small width */
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-edit"></i>दर्ता सम्पादन </h1>

            <a href="{{route('darta')}}" class="btn btn-sm btn-primary btn-show-form"><i class="fa fa-arrow-left"></i>
                लिष्टमा जाने</a>
        </section>

        <!-- FORM section start -->
        <section class="formSection">
            <div class="row justify-content-md-center">
                <div class="col-md-8 form-bg col-md-offset-2">
                    <div class="form-title">दर्ता सम्पादन फारम</div>
                    <form action="{{route('darta.update', $darta->id)}}" method="post" id="dartaForm" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="name">मिति </label>
                                <input type="text" class="form-control kalimati" id="date" placeholder="date"
                                       name="date"
                                       required>
                            </div>
                            <div class="col-md-6">
                                <label for="name">दर्ता न.</label>
                                <input type="text" class="form-control kalimati" value="{{$darta->darta_number}}"
                                       id="dartanumber" placeholder="दर्ता न."
                                       name="dartanumber" required disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="from_office_name">दर्ता आएको कार्यालय</label>
                                <input type="text" class="form-control" id="from_office_name"
                                       value="{{$darta->from_office_name}}" placeholder="दर्ता आएको कार्यालय"
                                       name="name" required>
                            </div>
                            <div class="col-md-6">
                                <label for="address">दर्ता आएको कार्यालय ठेगाना</label>
                                <input type="text" class="form-control" id="from_office_address"
                                       value="{{$darta->from_office_address}}" placeholder="दर्ता आएको कार्यालय ठेगाना"
                                       name="address" required>
                            </div>
                            <div class="col-md-6">
                                <label for="person">दर्ता गर्न आएको व्यक्ति</label>
                                <input type="text" class="form-control" id="person"
                                       placeholder="दर्ता गर्न आएको व्यक्ति" value="{{$darta->from_office_address}}"
                                       name="person" required>
                            </div>
                            <div class="col-md-6">
                                <label for="purpose">प्रायोजन</label>
                                <input type="text" class="form-control" id="purpose" value="{{$darta->purpose}}"
                                       placeholder="प्रायोजन"
                                       name="purpose" required>
                            </div>
                            {{-- <div class="col-md-6">
                                <label for="branch">सम्बन्धित शाखा</label>
                                <input type="text" class="form-control" id="branch" value="{{$darta->branch}}"
                                       placeholder="शाखा" name="branch"
                                       required>
                            </div> --}}
                            <div class="col-md-6">
                                <label for="branch">सम्बन्धित शाखा</label>
                                <select name="branch" required class="form-control branch" style="width:100%;" id="branch">
                                    <option value="">...........</option>
                                    @foreach($branch as $branch)
                                    <option value="{{$branch->id}}" @if($branch->id == $darta->branch) selected @endif>{{$branch->name_nep}}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="col-md-6">
                                <label for="prepared_date">डोकुमेन्ट तयार भएको मिति</label>
                                <input type="text" class="form-control" id="prepared_date"
                                       value="" placeholder="Prepared_Date"
                                       name="prepared_date" required>
                            </div>
                            <div class="col-md-6">
                                <label for="received_date">डोकुमेन्ट दर्ता भएको मिति</label>
                                <input type="text" class="form-control" id="received_date"
                                       value="" placeholder="Received_Date"
                                       name="received_date">
                            </div>
                            <div class="col-md-6">
                                <label for="image">Upload</label>
                                <input type="file" class="form-control" id="image" name="image">
                            </div>
                            <div class="image col-md-6">
                                <label>पुरानो फोटो</label>
                                <img src="{{asset('images/').'/'.$darta->image}}">
                            </div>
                            <div class="col-md-6">
                                <label for="remarks">कैफियत</label>
                                <input class="form-control" id="remarks" value="{{$darta->remarks}}" placeholder="Remarks" name="remarks" />
                            </div>
                        </div>

                        <center>
                            <button type="submit" class="btn btn-success btn-show-form" id="dartaSubmit"><i
                                        class="fas fa-check-square"></i> सम्पादन गर्ने
                            </button>
                        </center>
                    </form>
                </div>
            </div>
        </section>
        <!-- FORM section ends -->
    </div>
        @endsection

        @section('scripts')

            <script>
                $(document).ready(function () {
                    let date_roman = '{{$darta->date}}';
                    let date_nep = convertEnglishToNepali(date_roman);
                    $('#date').val(date_nep);

                    let preapare_date_roman = '{{$darta->prepared_date}}';
                    let prepared_date = preapare_date_roman.replace(' ', '').replace('00:00:00', '');
                    let prepared_date_nep = convertEnglishToNepali(prepared_date);
                    $('#prepared_date').val(prepared_date_nep);

                    let receive_date_roman = '{{$darta->received_date}}';
                    let receive_date = receive_date_roman.replace(' ', '').replace('00:00:00', '');
                    let receive_date_nep = convertEnglishToNepali(prepared_date);
                    $('#received_date').val(receive_date_nep);
                })
            </script>

            {{--      Update darta--}}
            <script>
                $(document).ready(function () {
                    $('#dartaSubmit').click(function (e) {
                        e.preventDefault();
                        let today_nepali_date = $('#date').val();
                        let nepali_date_in_roman = convertNepaliToEnglish(today_nepali_date);
                        let prepared_date = $('#prepared_date').val();
                        let prepared_date_in_roman = convertNepaliToEnglish(prepared_date);

                        let received_date = $('#received_date').val();
                        let received_date_in_roman = convertNepaliToEnglish(received_date);

                        $('#date').val(nepali_date_in_roman);
                        $('#prepared_date').val(prepared_date_in_roman);
                        $('#received_date').val(received_date_in_roman);
                        if (validation()) {
                            $('#dartaForm').submit();
                        }
                    })
                })
            </script>

            {{--            Today date  Date picker--}}
            <script>
                $("#date").nepaliDatePicker({
                    dateFormat: "%y-%m-%d",
                    closeOnDateSelect: true
                });
                var currentDate = new Date();
                var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
                var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);

            </script>

            {{--             Prepared date, Date picker--}}
            <script>
                $("#prepared_date").nepaliDatePicker({
                    dateFormat: "%y-%m-%d",
                    closeOnDateSelect: true
                });
                var currentDate = new Date();
                var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
                var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);

            </script>
            {{--                         Receive date, Date picker--}}
            <script>
                $("#received_date").nepaliDatePicker({
                    dateFormat: "%y-%m-%d",
                    closeOnDateSelect: true
                });
                var currentDate = new Date();
                var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
                var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
            </script>


            {{--Convert Nepali date to roman date --}}
            <script>
                function convertNepaliToEnglish(input) {
                    var charArray = input.split('');
                    var engDate = '';
                    $.each(charArray, function (key, value) {
                        switch (value) {
                            case '१':
                                engDate += '1';
                                break;
                            case '२':
                                engDate += '2';
                                break;
                            case '३':
                                engDate += '3';
                                break;
                            case '४':
                                engDate += '4';
                                break;
                            case '५':
                                engDate += '5';
                                break;
                            case '६':
                                engDate += '6';
                                break;
                            case '०':
                                engDate += '0';
                                break;
                            case '७':
                                engDate += '7';
                                break;
                            case '८':
                                engDate += '8';
                                break;
                            case '९':
                                engDate += '9';
                                break;

                            case '-':
                                engDate += '-';
                                break
                        }
                    });
                    return engDate

                }
            </script>

            {{-- Validation   --}}
            <script>
                function validation() {
                    let flag = 1;
                    let fromOfficeName = $('#from_office_name');
                    if (!fromOfficeName.val()) {
                        if (fromOfficeName.siblings('p').length == 0) {
                            fromOfficeName.after('<p style="color:red" class="validation-error">कुन अफिसबाट आएको हो लेख्नुहोस!</p>')
                        }
                        fromOfficeName.focus();
                        flag = 0;
                    } else {
                        fromOfficeName.siblings('p').remove()
                    }

                    let fromOfficeAddress = $('#from_office_address');
                    if (!fromOfficeAddress.val()) {
                        if (fromOfficeAddress.siblings('p').length == 0) {
                            fromOfficeAddress.after('<p style="color:red" class="validation-error">Where is the document brought from. Write the address.</p>')
                        }
                        fromOfficeAddress.focus();
                        flag = 0;
                    } else {
                        fromOfficeAddress.siblings('p').remove()
                    }

                    let fromOfficePerson = $('#person');
                    if (!fromOfficePerson.val()) {
                        if (fromOfficePerson.siblings('p').length == 0) {
                            fromOfficePerson.after('<p style="color:red" class="validation-error">who brought this document. please write name of person.</p>')
                        }
                        fromOfficePerson.focus();
                        flag = 0;
                    } else {
                        fromOfficePerson.siblings('p').remove()
                    }

                    let purpose = $('#purpose');
                    if (!purpose.val()) {
                        if (purpose.siblings('p').length == 0) {
                            purpose.after('<p style="color:red" class="validation-error">What is the purpose of the document.</p>')
                        }
                        purpose.focus();
                        flag = 0;
                    } else {
                        purpose.siblings('p').remove()
                    }

                    let branch = $('#branch');
                    if (!branch.val()) {
                        if (branch.siblings('p').length == 0) {
                            branch.after('<p style="color:red" class="validation-error">From which branch</p>')
                        }
                        branch.focus();
                        flag = 0;
                    } else {
                        branch.siblings('p').remove()
                    }
                    return flag;
                }
            </script>

            {{--Convert ROman date to Nepali date --}}
            <script>
                function convertEnglishToNepali(input) {
                    var charArray = input.split('');
                    var engDate = '';
                    $.each(charArray, function (key, value) {
                        switch (value) {
                            case '1':
                                engDate += '१';
                                break;
                            case '2':
                                engDate += '२';
                                break;
                            case '3':
                                engDate += '३';
                                break;
                            case '4':
                                engDate += '४';
                                break;
                            case '5':
                                engDate += '५';
                                break;
                            case '6':
                                engDate += '६';
                                break;
                            case '7':
                                engDate += '७';
                                break;
                            case '8':
                                engDate += '८';
                                break;
                            case '9':
                                engDate += '९';
                                break;
                            case '0':
                                engDate += '०';
                                break;

                            case '-':
                                engDate += '-';
                                break
                        }
                    });
                    return engDate;

                }
            </script>
@endsection
