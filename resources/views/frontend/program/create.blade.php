@extends('frontend.layouts.app')
@section('title')
  Program Create
@stop
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        बजेट उपशिर्षक बनाउने

      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('program')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> सुची हेर्ने</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('program.store')}}" method="post" id="programCreateForm">
            {{csrf_field()}}
            <div class="form-group">
              <label for="name">बजेट उपशिर्षक:</label>
              <input type="text" class="form-control" id="name" placeholder="बजेट उपशिर्षक" name="name">
            </div>
            <div class="form-group">
              <label for="program_code">बजेट उपशिर्षक कोड:</label>
              <input type="text" class="form-control" id="program_code" placeholder="बजेट उपशिर्षक कोड" name="program_code">
            </div>
            <div class="form-group">
              <label for="expense_type">प्रकार:</label>
              <select class="form-control" name="expense_type">
                <option value="1">चालु</option>
                <option value="2">पुजीं</option>
              </select>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary" id="btnProgramSave" style="margin-top: 18px;">थप</button>
            </div>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('scripts')

  <script>
    // $(document).on('click','#btnProgramSave', function (e) {
    //   e.preventDefault();
    //   let program_code_eng
    // })
  </script>
  <script>
    $("#programCreateForm").validate({
      rules: {
        name: {
          required: true,
        },
        program_code: {
          required: true,
        }
      },
      messages: {
        name: "Name Field is Required"
      }
    });
  </script>

  <script>
    function convertNepaliToEnglish(input) {
      var charArray = input.split('');
      var engDate = '';
      $.each(charArray, function (key, value) {
        switch (value) {
          case '१':
            engDate += '1'
            break
          case '२':
            engDate += '2'
            break
          case '३':
            engDate += '3'
            break
          case '४':
            engDate += '4'
            break
          case '५':
            engDate += '5'
            break
          case '६':
            engDate += '6'
            break
          case '०':
            engDate += '0'
            break
          case '७':
            engDate += '7'
            break
          case '८':
            engDate += '8'
            break
          case '९':
            engDate += '9'
            break

          case '-':
            engDate += '-'
            break
        }
      })
      return engDate

    }
  </script>
@stop