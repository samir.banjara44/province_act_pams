<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDartaChalaniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('darta_chalani', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->integer('darta_number')->unique();
            $table->string('from_office_name')->nullable();
            $table->string('from_office_address')->nullable();
            $table->string('from_office_person')->nullable();
            $table->string('purpose');
            $table->string('branch');
            $table->dateTime('prepared_date');
            $table->dateTime('received_date');
            $table->string('created_by');
            $table->boolean('status');
            $table->string('remarks')->nullable();
            $table->integer('office_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('darta_chalani');
    }
}
