<?php

use App\Http\Controllers\Settings\ProvinceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/logout', 'HomeController@logout')->name('logout');
Route::get('/', 'HomeController@index')->name('home')->middleware('ifSuperadmin');
Route::get('/admin-panel', 'HomeController@admin')->name('admin')->middleware('ifSuperadmin');
//Province
Route::get('/admin/setting/province', 'Settings\ProvinceController@index')->name('admin.province');
Route::get('/admin/setting/province/create', 'Settings\ProvinceController@create')->name('admin.province.create');
Route::post('/admin/setting/province/store', 'Settings\ProvinceController@store')->name('admin.province.store');


//MOF
Route::get('/admin/setting/mof', 'Settings\MofController@index')->name('admin.mof');
Route::get('/admin/setting/mof/create', 'Settings\MofController@create')->name('admin.mof.create');
Route::post('/admin/setting/mof/store', 'Settings\MofController@store')->name('admin.mof.store');

//Ministry
Route::get('/admin/setting/ministry', 'Settings\MinistryController@index')->name('admin.ministry');
Route::get('/admin/setting/ministry/create', 'Settings\MinistryController@create')->name('admin.ministry.create');
Route::post('/admin/setting/ministry/store', 'Settings\MinistryController@store')->name('admin.ministry.store');

//Department
Route::get('/admin/setting/department', 'Settings\DepartmentController@index')->name('admin.department');
Route::get('/admin/setting/department/create', 'Settings\DepartmentController@create')->name('admin.department.create');
Route::post('/admin/setting/department/store', 'Settings\DepartmentController@store')->name('admin.department.store');

//Office
Route::get('/admin/setting/office', 'Settings\OfficeController@index')->name('admin.office');
Route::get('/admin/setting/office/create', 'Settings\OfficeController@create')->name('admin.office.create');
Route::post('/admin/setting/office/store', 'Settings\OfficeController@store')->name('admin.office.store');

//Role
Route::get('/admin/setting/role', 'Settings\RoleController@index')->name('admin.role');
Route::get('/admin/setting/role/create', 'Settings\RoleController@create')->name('admin.role.create');
Route::post('/admin/setting/role/store', 'Settings\RoleController@store')->name('admin.role.store');

//User
Route::get('/admin/setting/user', 'Settings\UserController@index')->name('admin.user');
Route::get('/admin/setting/user/create', 'Settings\UserController@create')->name('admin.user.create');
Route::post('/admin/setting/user/store', 'Settings\UserController@store')->name('admin.user.store');
Route::get('/admin/setting/user/store', 'Settings\UserController@store')->name('admin.user.store');

//User Frontend
Route::get('/user/index/{office_id}', 'Frontend\UserController@index')->name('front.user.index');
Route::get('/user/create', 'Frontend\UserController@createFronUser')->name('front.user.create');
Route::post('/user/store/', 'Frontend\UserController@store')->name('front.user.store');
Route::get('/user/edit/{user_id}', 'Frontend\UserController@edit')->name('user.edit');
Route::post('/user/update/{user_id}', 'Frontend\UserController@update')->name('user.update');


//Program
Route::get('/admin/programs', 'Settings\ProgramsController@index')->name('admin.program');
Route::get('/admin/programs/create', 'Settings\ProgramsController@create')->name('admin.program.create');
Route::post('/admin/programs/store', 'Settings\ProgramsController@store')->name('admin.program.store');
Route::get('/admin/programs/edit/{id}', 'Settings\ProgramsController@edit')->name('admin.program.edit');
Route::post('/admin/programs/update/{id}', 'Settings\ProgramsController@update')->name('admin.program.update');


//Bank
Route::get('/admin/bank', 'Settings\BankController@index')->name('admin.bank');
Route::get('/admin/bank/create', 'Settings\BankController@create')->name('admin.bank.create');
Route::post('/admin/bank/store', 'Settings\BankController@store')->name('admin.bank.store');
Route::get('/admin/bank/edit/{id}', 'Settings\BankController@edit')->name('admin.bank.edit');
Route::post('/admin/bank/update/{id}', 'Settings\BankController@update')->name('admin.bank.update');



//पेश्कि / भुक्तानि पाउने
Route::get('/admin/advance-payment', 'Settings\AdvanceAndPaymentController@index')->name('admin.advance');
Route::get('/admin/advance-payment/create', 'Settings\AdvanceAndPaymentController@create')->name('admin.AdvanceAndPayment.create');
Route::post('/admin/advance-payment/store', 'Settings\AdvanceAndPaymentController@store')->name('admin.AdvanceAndPayment.store');
Route::get('/admin/advance-payment/edit/{id}', 'Settings\AdvanceAndPaymentController@edit')->name('admin.AdvanceAndPayment.edit');
Route::post('/admin/advance-payment/update/{id}', 'Settings\AdvanceAndPaymentController@update')->name('admin.AdvanceAndPayment.update');
Route::get('/party/get_by_party_type/{id}', 'Frontend\AdvanceAndPaymentController@get_party_by_party_type_and_offie')->name('get_party_name_by_party_type_and_office');
Route::get('/party/get_party/{id}', 'Frontend\AdvanceAndPaymentController@getParty')->name('get_party');

//source
Route::get('/admin/source', 'Settings\SourceController@index')->name('admin.source');
Route::get('/admin/source/create', 'Settings\SourceController@create')->name('admin.source.create');
Route::post('/admin/source/store', 'Settings\SourceController@store')->name('admin.source.store');
Route::get('/admin/source/get_by_source_type/{id}', 'Settings\SourceController@get_source_by_source_type')->name('admin.get_source_by_source_type');


// क्षेत्र
Route::get('/admin/area', 'Settings\AreaController@index')->name('admin.area');
Route::get('/admin/area/create', 'Settings\AreaController@create')->name('admin.area.create');
Route::post('/admin/area/store', 'Settings\AreaController@store')->name('admin.area.store');

//Sub Area
Route::get('/admin/sub-area', 'Settings\SubAreaController@index')->name('admin.sub.area');
Route::get('/admin/sub-area/create', 'Settings\SubAreaController@create')->name('admin.sub.area.create');
Route::post('/admin/sub-area/store', 'Settings\SubAreaController@store')->name('admin.sub.area.store');
Route::get('/admin/sub-area/get-by-area/{id}', 'Settings\SubAreaController@get_sub_area_by_area')->name('admin.get_sub_area_by_area');


//Main Program
Route::get('/admin/main-program', 'Settings\MainProgramController@index')->name('admin.main.program');
Route::get('/admin/main-program/create', 'Settings\MainProgramController@create')->name('admin.main.program.create');
Route::post('/admin/main-program/store', 'Settings\MainProgramController@store')->name('admin.main.program.store');
Route::get('/admin/main-program/get_by_sub_area/{id}', 'Settings\MainProgramController@get_main_program_by_sub_area')->name('admin.get_main_program_by_sub_area');


//General Activity
Route::get('/admin/main-program/get_by_main_program/{id}', 'Frontend\MainActivityController@get_general_activity_by_main_program_and_office_id')->name('admin.get_general_activity_by_main_program_and_office_id');


//Front End-  Main Activity
Route::post('/main-activity/store', 'Frontend\MainActivityController@store')->name('main.activity.store');

//Note : General Activity and Main Activity has same Controller . पछि मिलाउने ।।



//भुक्तानुुी तथा पेश्की
Route::get('/admin/AdvanceAndPayment', 'Settings\AdvanceAndPaymentController@index')->name('admin.advance');
Route::get('/admin/AdvanceAndPayment/create', 'Settings\AdvanceAndPaymentController@create')->name('admin.AdvanceAndPayment.create');
Route::post('/admin/AdvanceAndPayment/store', 'Settings\AdvanceAndPaymentController@store')->name('admin.AdvanceAndPayment.store');

//darbandi ko srot

Route::get('/admin/DarbandiSrot', 'Settings\DarbandiSrotController@index')->name('admin.darbandisrot');
Route::get('/admin/DarbandiSrot/create', 'Settings\DarbandiSrotController@create')->name('admin.darbandisrot.create');
Route::post('/admin/DarbandiSrot/store', 'Settings\DarbandiSrotController@store')->name('admin.darbandisrot.store');

//darbanditype
Route::get('/admin/DarbandiType', 'Settings\DarbandiTypeController@index')->name('admin.darbanditype');
Route::get('/admin/DarbandiType/create', 'Settings\DarbandiTypeController@create')->name('admin.darbanditype.create');
Route::post('/admin/DarbandiType/store', 'Settings\DarbandiTypeController@store')->name('admin.darbanditype.store');

//sewas
Route::get('/admin/Sewa', 'Settings\SewaController@index')->name('admin.sewas');
Route::get('/admin/Sewa/create', 'Settings\SewaController@create')->name('admin.sewas.create');
Route::post('/admin/Sewa/store', 'Settings\SewaController@store')->name('admin.sewas.store');

//designation
Route::get('/admin/designation', 'Settings\DesignationController@index')->name('admin.designations');
Route::get('/admin/Designation/create', 'Settings\DesignationController@create')->name('admin.designations.create');
Route::get('/admin/designation/getpad/{samuha_id}', 'Settings\DesignationController@get_pad_by_sreni')->name('admin.designations.getbyTaha');
Route::post('/admin/Designation/store', 'Settings\DesignationController@store')->name('admin.designations.store');

//taha
Route::get('/admin/taha', 'Settings\TahaController@index')->name('admin.tahas');
Route::get('/admin/Taha/create', 'Settings\TahaController@create')->name('admin.tahas.create');
Route::get('/admin/Taha/get_taha/{id}', 'Settings\TahaController@get_taha_by_sewa')->name('get_taha_by_sewa');
Route::post('/admin/Taha/store', 'Settings\TahaController@store')->name('admin.tahas.store');

//बजेट स्रोत तह

Route::get('/admin/source/level', 'Settings\BudgetSourceLevelController@index')->name('admin.budget.source');
Route::get('/admin/BudgetSourceLevel/create', 'Settings\BudgetSourceLevelController@create')->name('admin.budget.source.level.create');
Route::post('/admin/BudgetSourceLevel/store', 'Settings\BudgetSourceLevelController@store')->name('admin.budget.source.level.store');
Route::get('/admin/source-type/get_by_source_type/{id}', 'Settings\BudgetSourceLevelController@get_source_level_by_source_type')->name('admin.get_source_level_by_source_type');


// Fiscal Year
Route::get('/fiscal-year/{id}', 'Frontend\FiscalYearController@index')->name('fiscal.year');

// Cheque
Route::get('/cheque', 'Frontend\ChequeController@index')->name('cheque');
Route::post('/cheque/store', 'Frontend\ChequeController@store')->name('cheque.store');
Route::get('/cheque/print', 'Frontend\ChequeController@chequePrint')->name('voucher.cheque.print');
Route::get('/cheque/print/confirm/', 'Frontend\ChequeController@confirmChequePrint')->name('cheque.print.confirm');

//user.programs
Route::get('/programs', 'Frontend\ProgramsController@index')->name('program');
Route::get('/programs/create', 'Frontend\ProgramsController@create')->name('program.create');
Route::get('/programs/edit/{id}', 'Frontend\ProgramsController@edit')->name('program.edit');
Route::post('/programs/store', 'Frontend\ProgramsController@store')->name('program.store');
Route::post('/programs/update/{id}', 'Frontend\ProgramsController@update')->name('program.update');
Route::get('/programs/delete/{id}', 'Frontend\ProgramsController@delete')->name('program.delete');

//Route::get('/program', 'Frontend\ProgramsController@listByOfficeId')->name('list.program');



//user.bank
Route::get('/bank', 'Frontend\BankController@index')->name('bank');
Route::get('/bank/create', 'Frontend\BankController@create')->name('bank.create');
Route::get('/bank/edit/{id}', 'Frontend\BankController@edit')->name('bank.edit');
Route::post('/bank/store', 'Frontend\BankController@store')->name('bank.store');
Route::post('/bank/update/{id}', 'Frontend\BankController@update')->name('bank.update');
Route::get('/bank/delete/{id}', 'Frontend\BankController@delete')->name('bank.delete');



// Front End पेश्कि / भुक्तानि पाउने
Route::get('/advance-payment', 'Frontend\AdvanceAndPaymentController@index')->name('advance');
Route::get('/advance-payment/create', 'Frontend\AdvanceAndPaymentController@create')->name('AdvanceAndPayment.create');
Route::get('/advance-payment/edit/{id}', 'Frontend\AdvanceAndPaymentController@edit')->name('AdvanceAndPayment.edit');
Route::post('/advance-payment/store', 'Frontend\AdvanceAndPaymentController@store')->name('AdvanceAndPayment.store');
Route::post('/advance-payment/update/{id}', 'Frontend\AdvanceAndPaymentController@update')->name('AdvanceAndPayment.update');
Route::get('/advance-payment/update/{id}', 'Frontend\AdvanceAndPaymentController@delete')->name('advance.payment.delete');

// बजेट
Route::get('/budget/create', 'Frontend\BudgetController@create')->name('budget.create');
Route::get('/budget/edit', 'Frontend\BudgetController@edit')->name('budget.edit');
Route::post('/budget/update/{id}', 'Frontend\BudgetController@update')->name('budget.update');
Route::get('/budget/create/continue', 'Frontend\BudgetController@create_continue')->name('budget.create.continue');
Route::post('/budget/store', 'Frontend\BudgetController@store')->name('budget.store');
Route::get('/budget/get_main_activity_by_akhtiyari/{id}', 'Frontend\BudgetController@get_main_activity_by_akhtiyari')->name('get_main_activity_by_akhtiyari');
Route::get('/budget/get_main_activity/{id}/{id2}', 'Frontend\BudgetController@get_main_activity_by_budget_sub_head')->name('get_main_activity_by_budget_sub_head');
Route::get('/budget/get_expense_head_by_activity_id/{id}', 'Frontend\BudgetController@get_expense_head_by_activity_id')->name('get_expense_head_by_activity_id');
Route::get('/budget/get_total_budget/{id}', 'Frontend\BudgetController@get_total_budget_by_akhtiyari')->name('get_total_budget_by_akhtiyari');
Route::get('/budget/get_budget_by_activity/{id}', 'Frontend\BudgetController@get_budget_by_activity')->name('get_budget_by_activity');
Route::get('/budget/get_budget_by_activity_id/{id}', 'Frontend\BudgetController@get_budget_details_by_id')->name('get_budget_details_by_id');
Route::get('/budget/get_total_aktiyari/{fs}/{bs}', 'Frontend\BudgetController@get_total_akhtiyari_by_budget_sub_head')->name('get_total_akhtiyari_by_budget_sub_head');
Route::get('/budget/delete-activity/{id}', 'Frontend\BudgetController@delete_activity')->name('delete.activity');
Route::get('/budget/get_budgets/{id1}/{id2}', 'Frontend\BudgetController@getBudgets')->name('get.budget');

//रकमान्तर
Route::get('/budget/rakamantar-param/', 'Frontend\BudgetController@rakamantarParam')->name('budget.rakamantar');
Route::post('/rakamantar-budget/update/{id}', 'Frontend\BudgetController@updateRakamantar')->name('rakamantar.budget.update');

// Activity Progress
Route::get('/budget/activity-progress/', 'Frontend\BudgetController@activityProgress')->name('activity.progress');
Route::get('/budget/activity-progress-create/', 'Frontend\BudgetController@activityProgressCreate')->name('activity.progress.create');
Route::post('/budget/activity-progress-save/', 'Frontend\BudgetController@activityProgressStore')->name('progress.store');
Route::get('/budget/activity-progress-edit/{id}', 'Frontend\BudgetController@activityProgressEdit')->name('activity.process.edit');
Route::post('/budget/activity-progress-update/{id}', 'Frontend\BudgetController@activityProgressUpdate')->name('progress.update');

//medium
Route::get('/admin/medium', 'Settings\MediumController@index')->name('admin.mediums');
Route::get('/admin/Medium/create', 'Settings\MediumController@create')->name('admin.mediums.create');
Route::post('/admin/Medium/store', 'Settings\MediumController@store')->name('admin.mediums.store');


//sourcetype
Route::get('/admin/sourcetype', 'Settings\SourceTypeController@index')->name('admin.sourcetypes');
Route::get('/admin/SourceType/create', 'Settings\SourceTypeController@create')->name('admin.sourcetypes.create');
Route::post('/admin/SourceType/store', 'Settings\SourceTypeController@store')->name('admin.sourcetypes.store');


//Voucher
Route::get('/voucher', 'Frontend\VoucherController@create')->name('voucher');
Route::get('/summary_voucher', 'Frontend\VoucherController@summaryVoucherCreate')->name('summary.voucher');
Route::post('/voucher/create', 'Frontend\VoucherController@store')->name('voucher.store');
Route::get('/voucher/bhoucher_by_budget_sub_head/{id}/{id2}', 'Frontend\VoucherController@get_voucher_by_budget_sub_head_and_office')->name('get_voucher_by_budget_sub_head_and_office');
Route::get('/voucher/voucher_accept_create/', 'Frontend\VoucherController@voucher_accept_create')->name('voucher.accept_create');
Route::get('/voucher/voucher-list/{id}/{id2}', 'Frontend\VoucherController@get_voucher_list_by_status')->name('get_unapproved_voucher');
Route::get('/voucher/update-status/{id}', 'Frontend\VoucherController@set_approved')->name('set.voucher.stauts.update');
Route::get('/voucher/edit-voucher-list/', 'Frontend\VoucherController@voucher_edit_parameter')->name('voucher.list.edit');
Route::post('/voucher/update/', 'Frontend\VoucherController@update')->name('voucher.update');
Route::get('/voucher/delete/{id}', 'Frontend\VoucherController@delete')->name('voucher.delete');
Route::get('/voucher/un_approved', 'Frontend\VoucherController@voucherUnApprovedParam')->name('voucher.unapprove.parameter');
Route::post('/voucher/unapproved', 'Frontend\VoucherController@voucherUnApprove')->name('voucher.unapprove');

Route::get('/voucher/signature/', 'Frontend\VoucherController@getVoucherSignature')->name('voucher.signature');
Route::post('/voucher/signature/create', 'Frontend\VoucherController@voucherSignatureCreate')->name('voucher.signature.create');
Route::get('/voucher/signature/list/{id}', 'Frontend\VoucherController@voucherSignatureList')->name('voucher.signature.list');
Route::get('/voucher/grant-voucher-param', 'Frontend\VoucherController@grantVoucherParam')->name('grant.voucher.parameter');
Route::post('/voucher/grant-voucher/', 'Frontend\VoucherController@grantVoucher')->name('grant.voucher');
Route::post('/voucher/grant-voucher-store/', 'Frontend\VoucherController@grantVoucherStore')->name('grant.voucher.store');

//Voucher Details
Route::get('/voucher-details/delete/{id}', 'Frontend\VoucherDetailsController@voucherDetailsDeleteById')->name('delete.voucher.details');
Route::get('/voucher/voucher/{id}', 'Frontend\VoucherController@get_budget_and_expe_by_activity')->name('get_budget_and_expe_by_activity');


// भौचर डिटेल्स

Route::get('/voucher_details/expense_head_by_budget_sub_head/', 'Frontend\VoucherDetailsController@get_expense_head_in_voucher_details_by_budget_sub_head_id')->name('get_expense_head_in_voucher_details_by_budget_sub_head_id');
Route::get('/voucher/edit-voucher-details/{id}', 'Frontend\VoucherDetailsController@get_voucher_details_by_voucher_id')->name('get_voucher_details');
Route::get('/voucher-details/liability/{program_id}/{expense_head_id}', 'Frontend\VoucherDetailsController@getLiabilityByLiabilityHead')->name('get_liability');
Route::Post('/voucher-details/peski/', 'Frontend\VoucherDetailsController@getRemainPeski')->name('get.reamin.peski');

//Alya
Route::get('/voucher-details/alya/{fiscalYear}/{budgetSubHead}', 'Frontend\VoucherDetailsController@getAlya')->name('get.alya');
Route::post('/voucher-alya/store/', 'Frontend\VoucherController@storeAlyaVoucher')->name('store.alya.voucher');




//Expense Head
Route::get('/expense-head/get_expense_head_by_byahora/{id}', 'Frontend\ExpenseHeadController@get_expense_head__by_ledger_type')->name('get_expense_head__by_ledger_type');
Route::get('/expense-head/get_expense_head', 'Frontend\ExpenseHeadController@get_expense_head_by_only_one')->name('get.expense.head');
Route::get('/expense-head/get_used_expense_head/{id}', 'Frontend\ExpenseHeadController@get_used_expense_head')->name('get.used.expense.head');

//Bhuktani
Route::get('/pre-bhuktani-adesh', 'Frontend\BhuktaniController@preIndex')->name('bhuktani');
Route::post('/bhuktani-adesh/create/', 'Frontend\BhuktaniController@create')->name('bhuktani.create');
Route::post('/bhuktani-adesh/store', 'Frontend\BhuktaniController@store')->name('bhuktani.store');
Route::get('/bhuktani-adesh/list', 'Frontend\BhuktaniController@get_bhuktani')->name('get.bhuktani.list');
Route::get('/bhuktani-delete/{id}', 'Frontend\BhuktaniController@bhuktani_delete')->name('bhuktani.delete');
Route::post('/bhuktani-adesh/', 'Frontend\BhuktaniController@Index')->name('bhuktani.index');
Route::get('/bhuktani-adesh/{id}/{id2}', 'Frontend\BhuktaniController@indexBack')->name('bhuktani.index.back');



// Report

// म ले प फा न Report Param
Route::get('/report/malepa-five/parameter/', 'Frontend\Report\ReportController@malepa_five_param')->name('report.malepa.five.param');
Route::get('/report/malepa-thirteen/parameter/', 'Frontend\Report\ReportController@malepa_thirteen_param')->name('report.malepa.thirteen.param');
Route::get('/report/malepa-thirteen-sourcewise/parameter/', 'Frontend\Report\ReportController@malepa_thirteen_source_wise_param')->name('report.malepa.thirteen.sourcewise.param');
Route::post('/report/malepa-thirteen-sourcewise/', 'Frontend\Report\ReportController@malepa_thirteen_source_wise')->name('report.malepa.thireteen.source.wise');
Route::get('/report/malepa-fourteen/parameter/', 'Frontend\Report\ReportController@malepa_fourteen_param')->name('report.malepa.fourteen.param');
Route::get('/report/malepa-seventeen/parameter/', 'Frontend\Report\ReportController@malepa_seventeen_param')->name('report.malepa.seventeen.param');
Route::get('/report/malepa-twentytwo/parameter/', 'Frontend\Report\ReportController@malepa_twentyTwo_param')->name('report.malepa.twentyTwo.param');
Route::get('/report/bhuktani-adesh/parameter/', 'Frontend\Report\ReportController@bhuktani_adesh_param')->name('report.bhuktani.adesh.param');
Route::get('/report/bhuktani-adesh/report/{id}', 'Frontend\Report\ReportController@bhuktani_adesh_report')->name('report.bhuktani.adesh');

// Report
Route::post('/report/malepa-five/', 'Frontend\Report\ReportController@malepa_five_report')->name('report.malepa.five');
Route::post('/report/malepa-thirteen/', 'Frontend\Report\ReportController@malepa_thirteen_report')->name('report.malepa.thireteen');
Route::post('/report/malepa-seventeen/', 'Frontend\Report\ReportController@malepa_seventeen_report')->name('report.malepa.seventeen');
Route::post('/report/malepa-tweentyTwo/', 'Frontend\Report\ReportController@malepa_tweentyTwo_report')->name('report.malepa.tweentyTwo');
Route::post('/report/malepa-fourteen/', 'Frontend\Report\ReportController@malepa_fourteen_report')->name('report.malepa.fourteen');
Route::any('/report/voucher/', 'Frontend\Report\ReportController@voucher')->name('report.voucher');
Route::get('/report/voucher/{id}', 'Frontend\Report\ReportController@voucher_view')->name('voucher.view');
Route::get('/report/activity-param/', 'Frontend\Report\ReportController@getActivityParam')->name('report.activity');
Route::post('/report/activity-list/', 'Frontend\Report\ReportController@getActivityByBudgetSubHead')->name('get.activity.list');
Route::get('/report/activity-summary-param/', 'Frontend\Report\ReportController@getActivitySummaryParam')->name('report.activity.summary.param');
Route::post('/report/activity-summary-list/', 'Frontend\Report\ReportController@getActivitySummary')->name('get.activity.summary.list');
Route::get('/report/activity-detail-param/', 'Frontend\Report\ReportController@getActivityDetailParam')->name('report.activity.detail.param');
Route::post('/report/activity-detail-list/', 'Frontend\Report\ReportController@getActivityDetail')->name('get.activity.detail.list');

Route::get('/get-khata/{id}/{khataId}/{id2}', 'Frontend\Report\ReportController@getKhataByKhataPrakar')->name('get_khata_by_khata_prakar');
Route::get('/report/goswara-dharauti-khata-param/', 'Frontend\Report\ReportController@goswaraDharautiKhataParameter')->name('report.goswara.dharauti.khata.param');
Route::post('/report/goswara-dharauti-khata/', 'Frontend\Report\ReportController@goswaraDharautiKhata')->name('report.goswara.dharauti.khata');
Route::get('/report/dharauti-byktigat-khata-param/', 'Frontend\Report\ReportController@byaktigatDharautiKhataParam')->name('report.byaktigat.dharauti.khata.param');
Route::post('/report/dharauti-byktigat-khata/', 'Frontend\Report\ReportController@byaktigatDharautiKhata')->name('report.byaktigat.dharauti.khata');
Route::get('/report/dharauti-cash-book-param/', 'Frontend\Report\ReportController@dharautiCashBookParam')->name('report.dharauti.cash.book.param');
Route::post('/report/dharauti-cash-book/', 'Frontend\Report\ReportController@dharautiCashBook')->name('report.dharauti.cash.book');
Route::get('/report/budgetsheet-param/', 'Frontend\Report\ReportController@budgetSheetParam')->name('report.budget.sheet.param');
Route::post('/report/budgetsheet/', 'Frontend\Report\ReportController@budgetSheet')->name('report.budget.khata');
Route::get('/budget/activity-progress-report-param/', 'Frontend\Report\ReportController@progressReportParam')->name('progress.report.param');
Route::post('/budget/activity-progress-report/', 'Frontend\Report\ReportController@progressReport')->name('progress.report');

//Retention Bhuktani
Route::get('/report/retention-dharauti-param/', 'Frontend\Report\ReportController@retentionDharautiParam')->name('report.retention.bhuktani.adesh.param');
Route::get('/report/retention-dharauti/', 'Frontend\Report\ReportController@getRetentionBhuktani')->name('get.retention.bhuktani.list');
Route::get('/report/retention-bhuktani-adesh/report/{id}', 'Frontend\Report\ReportController@retentionBhuktaniAdeshReport')->name('report.retention.bhuktani.adesh');

//Route::post('/report/voucher/list', 'Frontend\Report\ReportController@get_voucher_list')->name('report.get.voucher.list');


//karmachari
Route::get('/karmachari', 'Frontend\KarmachariController@index')->name('karmachari');
Route::get('/karmachari/create', 'Frontend\KarmachariController@create')->name('karmachari.create');
Route::get('/karmachari/edit/{id}', 'Frontend\KarmachariController@edit')->name('karmachari.edit');
Route::post('/karmachari/store', 'Frontend\KarmachariController@store')->name('karmachari.store');
Route::post('/karmachari/update/{id}', 'Frontend\KarmachariController@update')->name('karmachari.update');
Route::get('/karmachari/delete/{id}', 'Frontend\KarmachariController@delete')->name('karmachari.delete');
Route::get('/karmachari/get-karmachari', 'Frontend\KarmachariController@get_karmachari_by_office')->name('get.karmachari');

//samuha in backend
Route::get('/admin/samuha', 'Settings\SamuhaController@index')->name('samuha');
Route::get('/admin/samuha/create', 'Settings\SamuhaController@create')->name('samuha.create');
Route::get('/admin/samuha/edit/{id}', 'Settings\SamuhaController@edit')->name('samuha.edit');
Route::get('/admin/samuha/getsamuhas/{sewa_id}', 'Settings\SamuhaController@get_samuhas_by_sewa_id')->name('samuha.getbysewa');
Route::post('/admin/samuha/store', 'Settings\SamuhaController@store')->name('samuha.store');
Route::post('/admin/samuha/update/{id}', 'Settings\SamuhaController@update')->name('samuha.update');



//party types in backend
Route::get('/admin/partytypes', 'Settings\PartyTypesController@index')->name('partytypes');
Route::get('/admin/partytypes/create', 'Settings\PartyTypesController@create')->name('partytypes.create');
Route::get('/admin/partytypes/edit/{id}', 'Settings\PartyTypesController@edit')->name('partytypes.edit');
Route::post('/admin/partytypes/store', 'Settings\PartyTypesController@store')->name('partytypes.store');
Route::post('/admin/partytypes/update/{id}', 'Settings\PartyTypesController@update')->name('partytypes.update');


//आख्तुियारि

Route::get('/frontend/akhtiyari', 'Frontend\BudgetController@akhtiyariIndex')->name('akhtiyari.param');
Route::post('/frontend/akhtiyari-store', 'Frontend\BudgetController@akhtiyariStore')->name('akhtiyari.store');
Route::get('/frontend/akhtiyari_add_param/{id}', 'Frontend\BudgetController@akhtiyariAddParam')->name('akhtiyariAdd');
Route::get('/frontend/akhtiyari_list/{fiscalYear}/{budgetSubHead}', 'Frontend\BudgetController@getAkhtiyariByBudgetSubHeadId')->name('getAkhtiyari');
Route::get('/frontend/akhtiyari_edit/{id}', 'Frontend\BudgetController@editAkhtiyari')->name('edit.akhtiyari');
Route::post('/frontend/akhtiyari_update/{id}', 'Frontend\BudgetController@akhtiyariUpdate')->name('akhtiyari.update');
Route::get('/frontend/akhtiyari_delete/{id}', 'Frontend\BudgetController@akhtiyariDelete')->name('delete.akhtiyari');

//District
Route::get('/admin/get-district/{id}', 'Settings\DistrictController@get_districts_by_province')->name('get.district');

//Local level
Route::get('/admin/get-local-level/{id}', 'Settings\LocalLevelController@get_local_level_by_district')->name('get.local.level');



// धरौटि

// धरौटि bank
Route::get('/retention/retension_bank', 'Frontend\RetentionController@retensionBankIndex')->name('retention_bank');
Route::get('/retention/retension_bank/create', 'Frontend\RetentionController@retensionBankCreate')->name('retention_bank_create');
Route::post('/retention/retension_bank/store', 'Frontend\RetentionController@retensionBankStore')->name('retention_bank_store');
Route::get('/retention/retension_bank/edit/{id}', 'Frontend\RetentionController@retensionBankEdit')->name('retention_bank_edit');
Route::post('/retention/retension_bank/update/{id}', 'Frontend\RetentionController@retensionBankUpdate')->name('retention.bank.update');
Route::get('/retention/bank', 'Frontend\BankController@getBankByByahora')->name('get.bank');



//Retention Depositor
Route::get('/retention/retension_depositor', 'Frontend\RetentionController@retensionDepositor')->name('retention.depositor');
Route::get('/retention/retension_depositor/create', 'Frontend\RetentionController@retensionDepositorCreate')->name('retention.depositor.create');
Route::post('/retention/retension_depositor/store', 'Frontend\RetentionController@retensionDepositorStore')->name('retention.depositor.store');
Route::get('/retention/retension_depositor/edit{id}', 'Frontend\RetentionController@retensionDepositorEdit')->name('retention.deposotor.edit');
Route::post('/retention/retension_depositor/update{id}', 'Frontend\RetentionController@retensionDepositorUpdate')->name('retention.deposotor.update');

// Retention Record
Route::get('/deposit/retension_record/index', 'Frontend\RetentionController@retensionRecordIndex')->name('retention.record');
Route::get('/retention_record/create', 'Frontend\RetentionController@retentionRecordCreate')->name('retention.record.create');
Route::post('/retention_record/store', 'Frontend\RetentionController@retentionRecordStore')->name('retention.record.store');
Route::get('/retention_record/edit/{id}', 'Frontend\RetentionController@retentionRecordEdit')->name('retention.record.edit');
Route::post('/retention_record/update{id}', 'Frontend\RetentionController@retentionRecordUpdate')->name('retention.record.update');


//Retention Bank Guarantee
Route::get('/retension_bank_guarantee/index', 'Frontend\RetentionController@retensionBankGuaranteeIndex')->name('retention.bank.guarantee');
Route::get('/retension_bank_guarantee/create', 'Frontend\RetentionController@retentionBankGuaranteeCreate')->name('retention.bank.guarantee.create');
Route::post('/retension_bank_guarantee/store', 'Frontend\RetentionController@retentionBankGuaranteeStore')->name('retention.bank.guarantee.store');
Route::get('/retension_bank_guarantee/edit/{id}', 'Frontend\RetentionController@retentionBankGuaranteeEdit')->name('retention.bank.guarantee.edit');
Route::post('/retension_bank_guarantee/update/{id}', 'Frontend\RetentionController@retentionBankGuaranteeUpdate')->name('retention.bank.guarantee.update');

//Retention Voucher
Route::get('/retension-voucher/parameter', 'Frontend\RetentionController@retentionVoucherParameter')->name('retention.voucher.parameter');
Route::get('/retension_voucher/voucher/index/', 'Frontend\RetentionController@retentionVoucherIndex')->name('retention.voucher.index');
Route::get('/retension/voucher/create/{id}', 'Frontend\RetentionController@retentionVoucherDetailsCreate')->name('retention.voucher.details');
Route::post('/retension/voucher/store', 'Frontend\RetentionController@retentionVoucherStore')->name('retention.voucher.store');
Route::post('/retension/voucher-details/store', 'Frontend\RetentionController@retentionVoucherDetailsStore')->name('retention.voucher.details.store');
Route::get('/retension_voucher_details/edit/{id}', 'Frontend\RetentionController@retentionVoucherEdit')->name('retention.voucher.edit');
Route::post('/retension_voucher/update/{id}', 'Frontend\RetentionController@retentionVoucherUpdate')->name('retention.voucher.update');
Route::post('/retension_voucher_details/update/{id}', 'Frontend\RetentionController@retentionVoucherDetailsUpdate')->name('retention.voucher.details.update');
Route::get('/retension_voucher_details/delete/{id}', 'Frontend\RetentionController@retentionVoucherDetailsDelete')->name('retention.voucherDetails.delete');
Route::get('/retension_voucher_view/{id}', 'Frontend\RetentionController@retentionVoucherView')->name('retention.voucher.view');
Route::get('/retension_voucher_approve/', 'Frontend\RetentionController@retentionVoucherApprovedParam')->name('retention.voucher.approve.param');
Route::get('/retension_voucher_approved/{id}', 'Frontend\RetentionController@retentionVoucherApproved')->name('retention.voucher.approved');
Route::get('/retension-bhuktani/', 'Frontend\RetentionController@retentionBhuktani')->name('retention.bhuktani');
Route::post('/retension-bhuktani-store/', 'Frontend\RetentionController@retentionBhuktaniStore')->name('retention.bhuktani.store');
Route::get('/retension-bhuktani-delete/{id}', 'Frontend\RetentionController@retentionBhuktaniDelete')->name('delete.retention.bhuktani');

//धरौटी भुक्तानी आदेश
//Route::get('/retension_voucher_approved/{id}', 'Frontend\RetentionController@retentionPaymentOrderApproved')->name('retention.voucher.bhuktani.param');

// अनुगमन / Supervision
Route::get('/ministry/office-list/{id}', 'Frontend\SuperVisionController@getAllOfficeListUnderMinistry')->name('ministry.office.list');
Route::get('/ministry/activities-list/{id}', 'Frontend\SuperVisionController@getAllActivitiesParameter')->name('ministry.activity.list.parameter');
Route::get('/ministry/used-office-list-param/{id}', 'Frontend\SuperVisionController@getUsedOfficeParameter')->name('ministry.office.Used.List.Param');
Route::get('/ministry/budget_sub_head_list/{id1}/{id2}', 'Frontend\SuperVisionController@getAllBudgetSubHead')->name('get.budget.sub.head');
Route::post('/ministry/activities_list/', 'Frontend\SuperVisionController@activitiesByProgramId')->name('activities.list');
Route::get('/ministry/fatwari-param/{id}', 'Frontend\SuperVisionController@fatwariParam')->name('ministry.fatwari.parameter');
Route::post('/ministry/fatwari/', 'Frontend\SuperVisionController@fatwari')->name('fatwari');
Route::get('/ministry/source_and_ledger_report_param/{id}', 'Frontend\SuperVisionController@reportOnSourceAndLedgerParam')->name('report.on.source.ledger.parameter');
Route::post('/ministry/source_and_ledger_report/', 'Frontend\SuperVisionController@reportOnSourceAndLedger')->name('source.ledger.wise.report');
Route::get('/ministry/expense_head_wise_budget_and_expense_param/{id}', 'Frontend\SuperVisionController@reportOnExpenseHeadWiseBudgetAndExpenseParam')->name('report.expense.head.wise.annual.expense.parameter');
Route::post('/ministry/expense_head_wise_budget_and_expense', 'Frontend\SuperVisionController@reportOnExpenseHeadWiseBUdgetAndExpense')->name('report.expense.head.annual.wise.expense');
Route::get('/ministry/source_wise_annual_report_param/{id}', 'Frontend\SuperVisionController@reportOnSourceWiseAnnualReportParam')->name('report.source.wise.annual.report.parameter');
Route::post('/ministry/source_wise_annual_report/', 'Frontend\SuperVisionController@reportOnSourceWiseAnnualReport')->name('report.source.wise.annual.report');
Route::get('/ministry/expense_head_report_param/{id}', 'Frontend\SuperVisionController@reportExpenseHeadWiseReportParam')->name('report.expense.head.wise.expense.parameter');
Route::post('/ministry/expense_head_report/', 'Frontend\SuperVisionController@reportExpenseHeadWiseReport')->name('report.expense.head.wise');





// Route for Frontend-Chalani
Route::get('/chalani/index', 'Frontend\ChalaniController@chalaniIndex')->name('chalani');
Route::get('/chalani/create', 'Frontend\ChalaniController@chalaniCreate')->name('chalani.create');
Route::post('/chalani/store', 'Frontend\ChalaniController@chalaniStore')->name('chalani.store');
Route::get('/chalani/edit/{id}', 'Frontend\ChalaniController@chalaniEdit')->name('chalani.edit');
Route::post('/chalani/update/{id}', 'Frontend\ChalaniController@chalaniUpdate')->name('chalani.update');
Route::get('/chalani/delete/{id}', 'Frontend\ChalaniController@delete')->name('chalani.delete');
Route::get('/chalani/letter-show/{id}', 'Frontend\ChalaniController@letterShow')->name('chalani.letter.show');
Route::get('/chalani/show/{id}', 'Frontend\ChalaniController@show')->name('chalani.show');
Route::get('/chalani/show-all/{id}', 'Frontend\ChalaniController@showAll')->name('chalan.show.all');


// Route for Frontend-Darta
Route::get('/darta/index', 'Frontend\DartaController@dartaIndex')->name('darta');
Route::get('/darta/create', 'Frontend\DartaController@dartaCreate')->name('darta.create');
Route::post('/darta/store', 'Frontend\DartaController@dartaStore')->name('darta.store');
Route::get('/darta/edit/{id}', 'Frontend\DartaController@dartaEdit')->name('darta.edit');
Route::post('/darta/update/{id}', 'Frontend\DartaController@dartaUpdate')->name('darta.update');
Route::get('/darta/delete/{id}', 'Frontend\DartaController@delete')->name('darta.delete');
Route::get('/darta/show/{id}', 'Frontend\DartaController@show')->name('darta.show');
Route::get('/darta/show-all/{id}', 'Frontend\DartaController@showAll')->name('darta.show.all');

//route for branch
Route::get('/branch/index', 'Frontend\BranchController@branchIndex')->name('branch');
Route::get('/branch/create', 'Frontend\BranchController@branchCreate')->name('branch.create');
Route::post('/branch/store', 'Frontend\BranchController@branchStore')->name('branch.store');
Route::get('/branch/edit/{id}', 'Frontend\BranchController@branchEdit')->name('branch.edit');
Route::post('/branch/update/{id}', 'Frontend\BranchController@branchUpdate')->name('branch.update');
Route::get('/branch/delete/{id}', 'Frontend\BranchController@branchdelete')->name('branch.delete');



//Excell import start
Route::get('downloadExcel/', 'Frontend\Report\ReportController@downloadExcel')->name('downloadExcel/xls');;
Route::get('import/excel', 'Frontend\Report\ReportController@importExcel')->name('import/excel');;
Route::post('store/excel', 'Frontend\Report\ReportController@storeExcel')->name('excel.store');;

// Route for view/blade file.
Route::any('importExport', 'MaatwebsiteController@importExport');
// Route for export/download tabledata to .csv, .xls or .xlsx
Route::any('downloadExcel/{type}', 'MaatwebsiteController@downloadExcel');
// Route for import excel data to database.
Route::post('importExcel', 'MaatwebsiteController@setActivityReportDetailsGorakhaSichai');

Route::any('importExcellUsers', 'MaatwebsiteController@importUser');
Route::post('importUsers', 'MaatwebsiteController@storeUser');

// Add budget Details of missing budget
Route::get('importMissingData', 'MaatwebsiteController@importBudgetDetails');

// set Operational Activities in One Unit. Like : 21111
Route::get('setOperationalActivity', 'MaatwebsiteController@setOperationalActivity')->getAction();

//change akhtiyari type in budget table
Route::get('reSetAkhtiyariType', 'MaatwebsiteController@reSetAkhtiyariTypeInBudget')->getAction();

//change akhtiyari type in budget table
Route::get('setActivityReport', 'MaatwebsiteController@setActivityReport')->getAction();


Route::get('setPremSirData', 'MaatwebsiteController@setPreSirData')->getAction();
Route::get('setPremSirDataTwo', 'MaatwebsiteController@setPremSirDataTwo')->getAction();
Route::get('test', 'MaatwebsiteController@getPefaVoucher')->getAction();
Route::get('BudgetInReportTable', 'MaatwebsiteController@setBudgetInReportTable')->getAction();
Route::get('setExpenseOnReportTable', 'MaatwebsiteController@setExpenseOnReportTable')->getAction();
Route::get('getUnBalancedVoucher', 'MaatwebsiteController@getUnBalancedVoucher')->getAction();
Route::post('getConditionalActivity', 'MaatwebsiteController@getConditionalActivity')->getAction();
Route::get('setBudgetDetailsName', 'MaatwebsiteController@setBudgetDetailsName')->getAction();
Route::get('setGorakhaData', 'MaatwebsiteController@setGorakhaSichaiData')->getAction();
Route::get('findUsedAndUnusedUser', 'MaatwebsiteController@findUser')->getAction();
Route::get('deleteGorakhaData', 'MaatwebsiteController@deleteGorakhaSichainActivityReportDetails')->getAction();
Route::get('checkBhutaniAmount', 'MaatwebsiteController@checkBhutaniAmount')->getAction();

// Fix pefa
Route::get('fixCurrentPefa', 'MaatwebsiteController@fixCurrentPefa')->getAction();
