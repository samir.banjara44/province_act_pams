<?php

namespace App\Http\Controllers\Frontend;

use App\Models\AdvanceAndPayment;
use App\Models\AllBank;
use App\Models\DarbandiSrot;
use App\Models\DarbandiType;
use App\Models\Designation;
use App\Models\District;
use App\Models\Karmachari;
use App\Models\LocalLevel;
use App\Models\Office;
use App\Models\Province;
use App\Models\Samuha;
use App\Models\Sewa;
use App\Models\Taha;
use App\Repositories\AdvancePaymentRepository;
use App\Repositories\KarmachariRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class KarmachariController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $karmachariRepo, $apcRepo, $advancePaymentRepo;

    public function __construct(KarmachariRepository $karmachariRepo, AdvancePaymentRepository $advancePaymentRepo)
    {

        $this->middleware('auth');
        $this->karmachariRepo = $karmachariRepo;
        $this->advancePaymentRepo = $advancePaymentRepo;

    }

    public function index()
    {
        $office_id =  Auth::user()->office->id;
       $karmacharis = AdvanceAndPayment::where('office_id',$office_id)->where('status',1)->where('party_type',5)->with('getKarmachariInVoucherDetails')->get();

        $provinces = Province::all();
        $offices = Office::all();
        $darbandisrots = DarbandiSrot::all();
        $darbanditypes = DarbandiType::all();
        $sewas = Sewa::all();
        $samuhas = Samuha::all();
        $pads = Designation::all();
        $tahas = Taha::all();
        $all_banks = AllBank::all();

        return view('frontend.karmachari.index', compact('karmacharis', 'offices', 'darbandisrots', 'darbanditypes', 'sewas', 'samuhas', 'pads', 'tahas', 'provinces', 'all_banks'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::all();
        $offices = Office::all();
        $darbandisrots = DarbandiSrot::all();
        $darbanditypes = DarbandiType::all();
        $sewas = Sewa::all();
        $samuhas = Samuha::all();
        $pads = Designation::all();
        $tahas = Taha::all();
        $all_banks = AllBank::all();

        return view('frontend.karmachari.create',compact('offices','darbandisrots','darbanditypes','sewas','samuhas','pads','tahas','provinces','all_banks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $karmachariAdd = $request->all();
//        try {
            $karmachari = $this->karmachariRepo->create($karmachariAdd);

            $karmachariAdd['karmachari_id'] = $karmachari;
            $karmachariAdd['party_type'] = 5;
            $karmachariAdd['name_nep'] =   $karmachariAdd['name_nepali'];
            $karmachariAdd['name_eng'] =   $karmachariAdd['name_english'];
            $karmachariAdd['citizen_number'] = $karmachariAdd['name_english'];
            $karmachariAdd['mobile_number']=   $karmachariAdd['mobile_number'];
            $karmachariAdd['vat_pan_number']=   $karmachariAdd['vat_pan'];
            $karmachariAdd['bank']=   $karmachariAdd['bank'];
            $karmachariAdd['party_khata_number']=   $karmachariAdd['khata_number'];
            $karmachariAdd['bank_address']=   $karmachariAdd['bank_address'];
            $karmachariAdd['payee_code']=   $karmachariAdd['payee_code'];

            $party = $this->advancePaymentRepo->createKarmachariParty($karmachariAdd);

            $requestName = $request->name_nepali;
            $message = "Created " . $requestName . " Successfully";
            if($request->ajax()) {

                return json_encode($party);
            }
            return redirect(route('karmachari'))->with('success',$message);
//        } catch (\Exception $e){

            $message = "Can not Create. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);

//        }



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $provinces = Province::all();
        $offices = Office::all();
        $darbandisrots = DarbandiSrot::all();
        $darbanditypes = DarbandiType::all();
        $sewas = Sewa::all();
       $samuhas = Samuha::all();
        $pads = Designation::all();
        $tahas = Taha::all();
        $karmachari = $this->karmachariRepo->get_by_id($id);
        $all_banks = AllBank::all();
        $districts = District::where('province_id',$karmachari['prvince_id'])->get();
        $local_levels = LocalLevel::where('district_id',$karmachari['district_id'])->get();

        return view('frontend.karmachari.edit',compact('karmachari','offices','darbandisrots','darbanditypes','sewas','samuhas','pads','tahas','provinces','districts','local_levels','all_banks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $karmachari = $this->karmachariRepo->update($request->all(), $id);


//            $party = $this->advancePaymentRepo->createKarmachariParty($karmachariAdd);

//            $karmachariAdd['party_type'] = 5;
//            $karmachariAdd['name_nep'] =   $karmachariAdd['name_nepali'];
//            $karmachariAdd['name_eng'] =   $karmachariAdd['name_english'];
//            $karmachariAdd['citizen_number'] = 123;
//            $karmachariAdd['mobile_number']=   $karmachariAdd['mobile_number'];
//            $karmachariAdd['vat_pan_number']=   $karmachariAdd['vat_pan'];

        $party = $this->advancePaymentRepo->UpdateKarmachariParty($request->all(),$id);


        $requestName = $request->name_nepali;
            $message = "Updated" . $requestName . " Successfully";
            return redirect('karmachari')->with('success', $message);
        } catch (\Exception $e){
            $message = "Can not update. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function get_karmachari_by_office(){

        $office_id= Auth::user()->office->id;
        $karmachariList = $this->karmachariRepo->get_by_office_id($office_id);
        return json_encode($karmachariList);
    }



    public function delete($karmachari_id){

        $advancePayment = AdvanceAndPayment::where('karmachari_id',$karmachari_id)->delete();
//        $karmachari_party_id = $advancePayment[0]->id;

            $deleteKarmachari = Karmachari::where('id',$karmachari_id)->delete();
//        $update = Karmachari::findorfail($karmachari_id)->delete(['status'=>0]);
//        $update1 = AdvanceAndPayment::findorfail($karmachari_party_id)->update(['status'=>0]);
        return json_encode($deleteKarmachari);
    }
}
