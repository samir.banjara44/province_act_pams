<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">
<style>
    table {
        border-collapse: collapse;
    }

    .accept-table {
    }

    .pull-right {
        float: right;
    }

    .fixPrintBtnRightTop {
        position: fixed;
        top: 0;
        right: 0;
    }

    table {
        margin: 0 auto;
    }

    ​
    tr.body td {
        border-collapse: separate;
        border-spacing: 5em;
    }

    .spacer {
        width: 100%;
        height: 31px;
    }

    .body {
        padding-left: 31px;
        text-align: justify;
    }

    @media print {
        .noprint {
            display: none;
        }
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <button class="fixPrintBtnRightTop pull-right noprint" onclick="window.print()">Print</button>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <br>
        <table width="99%" style="font-size:13px" class="">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 41px;height: 80px;width:100px;
" alt="">
                <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
                    सभा @endif</td>
            </tr>

            <tr>
                <td colspan="6"
                    style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    @if(Auth::user()->office->department)
                        {{Auth::user()->office->department->name}}
                    @endif
                </td>

            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}
                    , {{Auth::user()->office->district->name}}</td>
            </tr>

            <tr>
                <td style="padding-top: 59px; padding-left: 15px;font-size: 12px;">आर्थिक वर्ष : <span class="e-n-t-n-n">{{$fiscalYear->year}}</span></td>
            </tr>
        </table>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table table-bordered" border="1" width="98%">
                    <thead>
                    <tr style="background-color: #dddddd">
                        <th colspan="2">चलानी</th>
                        <th colspan="2">चलानी हुने पत्रको</th>
                        <th rowspan="2" width="15%">विषय</th>
                        <th rowspan="2" width="10%">पत्र चलान गर्ने कार्यालय</th>
                        <th rowspan="2">प्रकार</th>
                        <th rowspan="2">पठाउने माध्यम</th>
                        <th colspan="2">चलानी गर्नेको</th>
                        <th rowspan="2">कैफियत</th>
                    </tr>
                    <tr style="background-color: #dddddd">
                        <th width="2%">नम्बर</th>
                        <th width="10%">मिति</th>
                        <th>पत्र संख्या</th>
                        <th width="10%">पत्र मिति</th>
                        <th width="6%">नाम</th>
                        <th>दस्तखत</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($chalanis as $index=>$chalani)
                        <tr>
                            <td class="kalimati" align="center"
                                style="font-size: 10px;">{{$chalani->chalani_number}}</td>
                            <td class="kalimati" align="center" style="font-size: 10px;">{{$chalani->date}}</td>
                            <td class="kalimati" align="center"
                                style="font-size: 10px;">{{$chalani->letter_number}}</td>
                            <td class="kalimati" align="center" style="font-size: 10px;">{{$chalani->chalan_date}}</td>
                            <td style="font-size: 12px;">{{$chalani->purpose}}</td>
                            <td style="font-size: 12px;">{{$chalani->to_office}}</td>
                            <td style="font-size: 12px;">{{$chalani->type}}</td>
                            <td style="font-size: 12px;">{{$chalani->medium}}</td>
                            <td style="font-size: 12px;">{{$chalani->getUser->name}}</td>
                            <td width="12%"></td>
                            <td>{{$chalani->remarks}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <br>
                <br>

                <br>
            </div>
        </div>
    </section>

    <!-- /.content -->
</div>


<script>
    let changeToNepali = function (text) {
        let numbers = $.trim(text).split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (!isNaN(value) && value != ' ') {
                    if (value == 1)
                        nepaliNo += "१";

                    else if (value == 2)
                        nepaliNo += "२";

                    else if (value == 3)
                        nepaliNo += "३";

                    else if (value == 4)
                        nepaliNo += "४";

                    else if (value == 5)
                        nepaliNo += "५";

                    else if (value == 6)
                        nepaliNo += "६";

                    else if (value == 7)
                        nepaliNo += "७";

                    else if (value == 8)
                        nepaliNo += "८";

                    else if (value == 9)
                        nepaliNo += "९";

                    else if (value == 0)
                        nepaliNo += "०";
                } else {
                    nepaliNo += value
                }
            }
        });
        // console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });

</script>


