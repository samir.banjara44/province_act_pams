<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\CanResetPassword;

class User extends Model
{
    protected $table = 'users';
    protected $guarded = [];

    public function office(){
        return $this->belongsTo('App\Models\Office');
    }

    public function department(){
        return $this->belongsTo('App\Models\Department','department_id','id');
    }


    public function userRole() {
        return $this->hasOne('App\Models\UserRole','user_id','id');
    }
    public function hasRole($role)
    {
        if ($this->roles()) {
            return $this->roles()->contains('name', $role);
        }
        return false;
    }

    public function attachRole($role) {
        if (is_object($role)) {
            $role = $role->getKey();
        }
        if (is_array($role)) {
            $role = $role['id'];
        }
        $this->roles()->attach($role);
    }

    public function detachRole($role) {
        if (is_object($role)) {
            $role = $role->getKey();
        }
        if (is_array($role)) {
            $role = $role['id'];
        }
        $this->roles()->detach($role);
    }

    public function attachRoles($roles) {
        foreach ($roles as $role) {
            $this->attachRole($role);
        }
    }

    public function detachRoles($roles) {
        foreach ($roles as $role) {
            $this->detachRole($role);
        }
    }

    public function isSuperUser() {
        return (bool)$this->is_admin;
    }

    public function hasAccess() {
        if ($this->isSuperUser()) {
            return true;
        }
    }



    public function getRoles() {
        $roles = [];
        if ($this->roles()) {
            $roles = $this->roles()->get();
        }
        return $roles;
    }

}
