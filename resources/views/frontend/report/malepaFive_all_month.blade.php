<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">

<style>
    table {
        border-collapse: collapse;
    }

    table tbody tr:hover {
        background-color: #e1e1e1 !important;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<style type="text/css" media="print"> @page {
        size: landscape;
    } </style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="border-collapse: collapse; font-size:13px">
            <thead>
            <tr>
                <td colspan="6" style="text-align: center">
                    <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                    <b>@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश सभा @endif</b>
                </td>
            </tr>
            {{--            <tr>--}}
            {{--                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>--}}
            {{--            </tr>--}}
            <tr>
                <td colspan="6"
                    style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    @if(Auth::user()->office->department)
                        {{Auth::user()->office->department->name}}</td>
                @endif
            </tr>
            <tr>
                <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->name}}</b></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->province->name}}
                        , {{Auth::user()->office->district->name}}</b></td>
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>बैङ्क नगदी किताब</b><br>
                        {{--                        <b><span class="e-n-t-n-n">{{$year}}</span> साल {{$month_name}} महिना</b>--}}
                    </div>
                    <div style="float: right; margin-top: -20px">
                        म.ले.प.फा.नं. २०९
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                    <br>

                </td>
            </tr>
            <tr>
                <td class="kalimati">बजेट उपशीर्षक : {{$program['name']}}- {{$program['program_code']}}
                </td>
            </tr>
            </thead>
        </table>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">

                <table class="table" id="malepa-five-table" width="100%" border="1"
                       style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th rowspan="2">मिति</th>
                        <th rowspan="2">भौ.न.</th>
                        <th rowspan="2">विवरण</th>
                        <th colspan="2">नगद मौज्दात</th>
                        <th colspan="4">बैङ्क मौज्दात</th>
                        <th colspan="2">बजेट खर्च</th>
                        <th colspan="2">चालु आर्थिक वर्षको पेश्की</th>
                        <th colspan="2">गत आवको वर्षको पेश्की</th>
                        <th colspan="2">विविध</th>
                        <th colspan="2">कैफियत</th>
                    </tr>
                    <tr>
                        <td style="text-align: center">डे</td>
                        <td style="text-align: center">क्रे</td>
                        <td style="text-align: center">डे</td>
                        <td style="text-align: center">क्रे</td>
                        <td style="text-align: center">चेक नं</td>
                        <td style="text-align: center">बाकिँ</td>
                        <td style="text-align: center">खर्च शीर्षक</td>
                        <td style="text-align: center">रकम</td>
                        <td style="text-align: center">दिइएको</td>
                        <td style="text-align: center">फर्छ्यौट</td>
                        <td style="text-align: center">जिम्मेवारी सारिएको</td>
                        <td style="text-align: center">फर्छ्यौट</td>
                        <td style="text-align: center">डे</td>
                        <td style="text-align: center">क्रे</td>
                        <td colspan="2"></td>
                    </tr>
                    </thead>
                    <tbody>
                    @if($voucherModel['srawan']->count())
                        <td colspan="17" style="text-align: left">श्रावण महिना</td>
                        @foreach($voucherModel['srawan'] as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td class="kalimati"><span class="" style="display: block; text-align: center"><a
                                                class="e-n-t-n-n" style="text-decoration: none"
                                                href="{{route('voucher.view',$voucher->id)}}"
                                                target="_blank">{{$voucher->jv_number}}</a></span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">

                                    {{--                                    @if($voucher->nikasa_check())--}}
                                    <span class="e-n-t-n-n">@if($voucher->nikasa_check())
                                            @moneyFormat($voucher->nikasa_check())@endif</span>
                                    {{--                                    @--}}
                                </td>
                                <td style="text-align:right">
                                    @if(($voucher->check_payment()))
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati"
                                    style="text-align:left; font-size: 9px">{{$voucher->get_expense_heads()}}</td>
                                {{--                                ब ख रकम--}}
                                <td class="cr-amount" style="text-align:right">
                                    <span class="e-n-t-n-n">{{$voucher->calculate_total_expense()}}</span>
                                </td>
                                <td style="text-align: right"><span
                                            class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class="" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeski())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeskiClearance())</span>
                                </td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right">श्रावण महिनाको जम्मा</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($srawanMonthTotal['nikasa'])</span>
                            </td>
                            <td class="bank-cr-amount" id="this_month_total" style="text-align:right">
                                <span class="e-n-t-n-n">@moneyFormat($srawanMonthTotal['cr_tsa'])</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right">
                                <span class="e-n-t-n-n">@moneyFormat($thisMonthExp['srawan'])</span>
                            </td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($srawanMonthTotal['peski'])</span>
                            </td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($srawanMonthTotal['advance_clearance'])</span>
                            </td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($srawanMonthTotal['last_year_advance'])</span>
                            </td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($srawanMonthTotal['last_year_clearance'])</span>
                            </td>

                            <td class="liability-dr" id="liability-dr" style="text-align:right">
                                <span class="e-n-t-n-n">@moneyFormat($srawanMonthTotal['dr_liability'])</span>
                            </td>

                            <td class="liability-cr" id="liability-cr" style="text-align:right">
                                <span class="e-n-t-n-n">@moneyFormat($srawanMonthTotal['cr_liability'])</span>
                            </td>
                            <td></td>
                        </tr>
                    @endif

                    @if($voucherModel['bhadra']->count())
                        <td colspan="17" style="text-align: left">भाद्र महिना</td>
                        @foreach($voucherModel['bhadra'] as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td class="kalimati"><span class="" style="display: block; text-align: center"><a
                                                class="e-n-t-n-n" style="text-decoration: none"
                                                href="{{route('voucher.view',$voucher->id)}}"
                                                target="_blank">{{$voucher->jv_number}}</a></span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@if($voucher->nikasa_check())@moneyFormat($voucher->nikasa_check())@endif</span>
                                    @endif
                                </td>
                                <td style="text-align:right">
                                    @if(($voucher->check_payment()))
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati"
                                    style="text-align:left; font-size: 9px">{{$voucher->get_expense_heads()}}</td>
                                {{--                                ब ख रकम--}}
                                <td class="cr-amount" style="text-align:right">
                                    <span class="e-n-t-n-n">{{$voucher->calculate_total_expense()}}</span>
                                </td>
                                <td style="text-align: right"><span
                                            class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class="" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeski())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeskiClearance())</span>
                                </td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right">भाद्र महिनाको जम्मा</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($bhadraMonthTotal['nikasa'])</span>
                            </td>
                            <td class="bank-cr-amount" id="this_month_total" style="text-align:right">
                                <span class="e-n-t-n-n">@moneyFormat($bhadraMonthTotal['cr_tsa'])</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right">
                                <span class="e-n-t-n-n">@moneyFormat($thisMonthExp['bhadra'])</span>
                            </td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($bhadraMonthTotal['peski'])</span>
                            </td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($bhadraMonthTotal['advance_clearance'])</span>
                            </td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($bhadraMonthTotal['last_year_advance'])</span>
                            </td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($bhadraMonthTotal['last_year_clearance'])</span>
                            </td>

                            <td class="liability-dr" id="liability-dr" style="text-align:right">
                                <span class="e-n-t-n-n">@moneyFormat($bhadraMonthTotal['dr_liability'])</span>
                            </td>

                            <td class="liability-cr" id="liability-cr" style="text-align:right">
                                <span class="e-n-t-n-n">@moneyFormat($bhadraMonthTotal['cr_liability'])</span>
                            </td>
                            <td></td>
                        </tr>
                    @endif
                    @if($voucherModel['ashoj']->count())
                        <td colspan="17" style="text-align: left">असोज महिना</td>
                        @foreach($voucherModel['ashoj'] as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td class="kalimati"><span class="" style="display: block; text-align: center"><a
                                                class="e-n-t-n-n" style="text-decoration: none"
                                                href="{{route('voucher.view',$voucher->id)}}"
                                                target="_blank">{{$voucher->jv_number}}</a></span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">@if($voucher->nikasa_check())<span class="e-n-t-n-n">@if($voucher->nikasa_check())
                                            @moneyFormat($voucher->nikasa_check())@endif</span>@endif</td>
                                <td style="text-align:right">@if(($voucher->check_payment()))<span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>@endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati"
                                    style="text-align:left; font-size: 9px">{{$voucher->get_expense_heads()}}</td>
                                <td class="cr-amount" style="text-align:right"><span
                                            class="e-n-t-n-n">{{$voucher->calculate_total_expense()}}</span></td>
                                <td style="text-align: right"><span
                                            class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class="" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeski())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeskiClearance())</span>
                                </td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right">असोज महिनाको जम्मा</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($asojMonthTotal['nikasa'])</span>
                            </td>
                            <td class="bank-cr-amount" id="this_month_total" style="text-align:right">
                                <span class="e-n-t-n-n">@moneyFormat($asojMonthTotal['cr_tsa'])</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right">
                                <span class="e-n-t-n-n">@moneyFormat($thisMonthExp['ashoj'])</span>
                            </td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($asojMonthTotal['peski'])</span>
                            </td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($asojMonthTotal['advance_clearance'])</span>
                            </td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($asojMonthTotal['last_year_advance'])</span>
                            </td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($asojMonthTotal['last_year_clearance'])</span>
                            </td>

                            <td class="liability-dr" id="liability-dr" style="text-align:right">
                                <span class="e-n-t-n-n">@moneyFormat($asojMonthTotal['dr_liability'])</span>
                            </td>

                            <td class="liability-cr" id="liability-cr" style="text-align:right">
                                <span class="e-n-t-n-n">@moneyFormat($asojMonthTotal['cr_liability'])</span>
                            </td>
                            <td></td>
                        </tr>
                    @endif
                    @if($voucherModel['kartik']->count())
                        <td colspan="17" style="text-align: left">कार्तिक महिना</td>
                        @foreach($voucherModel['kartik'] as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td class="kalimati"><span class="" style="display: block; text-align: center"><a
                                                class="e-n-t-n-n" style="text-decoration: none"
                                                href="{{route('voucher.view',$voucher->id)}}"
                                                target="_blank">{{$voucher->jv_number}}</a></span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@if($voucher->nikasa_check())@moneyFormat($voucher->nikasa_check())@endif</span>
                                    @endif
                                </td>
                                <td style="text-align:right">
                                    @if(($voucher->check_payment()))
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati"
                                    style="text-align:left; font-size: 9px">{{$voucher->get_expense_heads()}}</td>
                                {{--                                ब ख रकम--}}
                                <td class="cr-amount" style="text-align:right">
                                    <span class="e-n-t-n-n">{{$voucher->calculate_total_expense()}}</span>
                                </td>
                                <td style="text-align: right"><span
                                            class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class="" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeski())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeskiClearance())</span>
                                </td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right">कार्तिक महिनाको जम्मा</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($kartikMonthTotal['nikasa'])</span>
                            </td>
                            <td class="bank-cr-amount" id="this_month_total" style="text-align:right"><span
                                        class="e-n-t-n-n">@moneyFormat($kartikMonthTotal['cr_tsa'])</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right"><span
                                        class="e-n-t-n-n">@moneyFormat($thisMonthExp['kartik'])</span></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($kartikMonthTotal['peski'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($kartikMonthTotal['advance_clearance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($kartikMonthTotal['last_year_advance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($kartikMonthTotal['last_year_clearance'])</span>
                            </td>
                            <td class="liability-dr" id="liability-dr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($kartikMonthTotal['dr_liability'])</span>
                            </td>
                            <td class="liability-cr" id="liability-cr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($kartikMonthTotal['cr_liability'])</span>
                            </td>
                            <td></td>
                        </tr>
                    @endif
                    @if($voucherModel['marga']->count())
                        <td colspan="17" style="text-align: left">मंसिर महिना</td>
                        @foreach($voucherModel['marga'] as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td class="kalimati"><span class="" style="display: block; text-align: center"><a
                                                class="e-n-t-n-n" style="text-decoration: none"
                                                href="{{route('voucher.view',$voucher->id)}}"
                                                target="_blank">{{$voucher->jv_number}}</a></span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">

                                    {{--                                    @if($voucher->nikasa_check())--}}
                                    <span class="e-n-t-n-n">@if($voucher->nikasa_check())
                                            @moneyFormat($voucher->nikasa_check())@endif</span>
                                    {{--                                    @--}}
                                </td>
                                <td style="text-align:right">
                                    @if(($voucher->check_payment()))
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati"
                                    style="text-align:left; font-size: 9px">{{$voucher->get_expense_heads()}}</td>
                                {{--                                ब ख रकम--}}
                                <td class="cr-amount" style="text-align:right">
                                    <span class="e-n-t-n-n">{{$voucher->calculate_total_expense()}}</span>
                                </td>
                                <td style="text-align: right"><span
                                            class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class="" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeski())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeskiClearance())</span>
                                </td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right">मंसिर महिनाको जम्मा</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($margaMonthTotal['nikasa'])</span>
                            </td>
                            <td class="bank-cr-amount" id="this_month_total" style="text-align:right"><span
                                        class="e-n-t-n-n">@moneyFormat($margaMonthTotal['cr_tsa'])</span></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right"><span
                                        class="e-n-t-n-n">@moneyFormat($thisMonthExp['marga'])</span></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($margaMonthTotal['peski'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($margaMonthTotal['advance_clearance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($margaMonthTotal['last_year_advance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($margaMonthTotal['last_year_clearance'])</span>
                            </td>
                            <td class="liability-dr" id="liability-dr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($margaMonthTotal['dr_liability'])</span>
                            </td>
                            <td class="liability-cr" id="liability-cr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($margaMonthTotal['cr_liability'])</span>
                            </td>
                            <td></td>
                        </tr>
                    @endif
                    @if($voucherModel['paush']->count())
                        <td colspan="17" style="text-align: left">पौष महिना</td>
                        @foreach($voucherModel['paush'] as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td class="kalimati"><span class="" style="display: block; text-align: center"><a
                                                class="e-n-t-n-n" style="text-decoration: none"
                                                href="{{route('voucher.view',$voucher->id)}}"
                                                target="_blank">{{$voucher->jv_number}}</a></span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">

                                    {{--                                    @if($voucher->nikasa_check())--}}
                                    <span class="e-n-t-n-n">@if($voucher->nikasa_check())
                                            @moneyFormat($voucher->nikasa_check())@endif</span>
                                    {{--                                    @--}}
                                </td>
                                <td style="text-align:right">
                                    @if(($voucher->check_payment()))
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati"
                                    style="text-align:left; font-size: 9px">{{$voucher->get_expense_heads()}}</td>
                                {{--                                ब ख रकम--}}
                                <td class="cr-amount" style="text-align:right">
                                    <span class="e-n-t-n-n">{{$voucher->calculate_total_expense()}}</span>
                                </td>
                                <td style="text-align: right"><span
                                            class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class="" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeski())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeskiClearance())</span>
                                </td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right">पौष महिनाको जम्मा</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($paushMonthTotal['nikasa'])</span>
                            </td>
                            <td class="bank-cr-amount" id="this_month_total" style="text-align:right"><span
                                        class="e-n-t-n-n">@moneyFormat($paushMonthTotal['cr_tsa'])</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right"><span
                                        class="e-n-t-n-n">@moneyFormat($thisMonthExp['paush'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($paushMonthTotal['peski'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($paushMonthTotal['advance_clearance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($paushMonthTotal['last_year_advance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($paushMonthTotal['last_year_clearance'])</span>
                            </td>
                            <td class="liability-dr" id="liability-dr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($paushMonthTotal['dr_liability'])</span>
                            </td>
                            <td class="liability-cr" id="liability-cr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($paushMonthTotal['cr_liability'])</span>
                            </td>
                            <td></td>
                        </tr>
                    @endif
                    @if($voucherModel['magh']->count())
                        <td colspan="17" style="text-align: left">माघ महिना</td>
                        @foreach($voucherModel['magh'] as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td class="kalimati"><span class="" style="display: block; text-align: center"><a
                                                class="e-n-t-n-n" style="text-decoration: none"
                                                href="{{route('voucher.view',$voucher->id)}}"
                                                target="_blank">{{$voucher->jv_number}}</a></span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">

                                    {{--                                    @if($voucher->nikasa_check())--}}
                                    <span class="e-n-t-n-n">@if($voucher->nikasa_check())
                                            @moneyFormat($voucher->nikasa_check())@endif</span>
                                    {{--                                    @--}}
                                </td>
                                <td style="text-align:right">
                                    @if(($voucher->check_payment()))
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati"
                                    style="text-align:left; font-size: 9px">{{$voucher->get_expense_heads()}}</td>
                                {{--                                ब ख रकम--}}
                                <td class="cr-amount" style="text-align:right">
                                    <span class="e-n-t-n-n">{{$voucher->calculate_total_expense()}}</span>
                                </td>
                                <td style="text-align: right"><span
                                            class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class="" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeski())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeskiClearance())</span>
                                </td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right">माघ महिनाको जम्मा</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($maghMonthTotal['nikasa'])</span>
                            </td>
                            <td class="bank-cr-amount" id="this_month_total" style="text-align:right"><span
                                        class="e-n-t-n-n">@moneyFormat($maghMonthTotal['cr_tsa'])</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right"><span
                                        class="e-n-t-n-n">@moneyFormat($thisMonthExp['magh'])</span>
                            </td>
                            <td style="text-align: right"><span
                                        class="e-n-t-n-n">@moneyFormat($maghMonthTotal['peski'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($maghMonthTotal['advance_clearance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($maghMonthTotal['last_year_advance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($maghMonthTotal['last_year_clearance'])</span>
                            </td>
                            <td class="liability-dr" id="liability-dr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($maghMonthTotal['dr_liability'])</span>
                            </td>
                            <td class="liability-cr" id="liability-cr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($maghMonthTotal['cr_liability'])</span>
                            </td>
                            <td></td>
                        </tr>
                    @endif
                    @if($voucherModel['fagun']->count())
                        <td colspan="17" style="text-align: left">फागुन महिना</td>
                        @foreach($voucherModel['fagun'] as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td class="kalimati"><span class="" style="display: block; text-align: center"><a
                                                class="e-n-t-n-n" style="text-decoration: none"
                                                href="{{route('voucher.view',$voucher->id)}}"
                                                target="_blank">{{$voucher->jv_number}}</a></span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">

                                    {{--                                    @if($voucher->nikasa_check())--}}
                                    <span class="e-n-t-n-n">@if($voucher->nikasa_check())
                                            @moneyFormat($voucher->nikasa_check())@endif</span>
                                    {{--                                    @--}}
                                </td>
                                <td style="text-align:right">
                                    @if(($voucher->check_payment()))
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati"
                                    style="text-align:left; font-size: 9px">{{$voucher->get_expense_heads()}}</td>
                                {{--                                ब ख रकम--}}
                                <td class="cr-amount" style="text-align:right">
                                    <span class="e-n-t-n-n">{{$voucher->calculate_total_expense()}}</span>
                                </td>
                                <td style="text-align: right"><span
                                            class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class="" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeski())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeskiClearance())</span>
                                </td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right">फागुन महिनाको जम्मा</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($fagunMonthTotal['nikasa'])</span>
                            </td>
                            <td class="bank-cr-amount" id="this_month_total" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($fagunMonthTotal['cr_tsa'])</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($thisMonthExp['fagun'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($fagunMonthTotal['peski'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($fagunMonthTotal['advance_clearance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($fagunMonthTotal['last_year_advance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($fagunMonthTotal['last_year_clearance'])</span>
                            </td>
                            <td class="liability-dr" id="liability-dr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($fagunMonthTotal['dr_liability'])</span>
                            </td>
                            <td class="liability-cr" id="liability-cr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($fagunMonthTotal['cr_liability'])</span>
                            </td>
                            <td></td>
                        </tr>
                    @endif
                    @if($voucherModel['chaitra']->count())
                        <td colspan="17" style="text-align: left">चैत्र महिना</td>
                        @foreach($voucherModel['chaitra'] as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td class="kalimati"><span class="" style="display: block; text-align: center"><a
                                                class="e-n-t-n-n" style="text-decoration: none"
                                                href="{{route('voucher.view',$voucher->id)}}"
                                                target="_blank">{{$voucher->jv_number}}</a></span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">

                                    {{--                                    @if($voucher->nikasa_check())--}}
                                    <span class="e-n-t-n-n">@if($voucher->nikasa_check())
                                            @moneyFormat($voucher->nikasa_check())@endif</span>
                                    {{--                                    @--}}
                                </td>
                                <td style="text-align:right">
                                    @if(($voucher->check_payment()))
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati"
                                    style="text-align:left; font-size: 9px">{{$voucher->get_expense_heads()}}</td>
                                {{--                                ब ख रकम--}}
                                <td class="cr-amount" style="text-align:right">
                                    <span class="e-n-t-n-n">{{$voucher->calculate_total_expense()}}</span>
                                </td>
                                <td style="text-align: right"><span
                                            class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class="" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeski())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeskiClearance())</span>
                                </td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right">चैत्र महिनाको जम्मा</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($chaitraMonthTotal['nikasa'])</span>
                            </td>
                            <td class="bank-cr-amount" id="this_month_total" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($chaitraMonthTotal['cr_tsa'])</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($thisMonthExp['chaitra'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($chaitraMonthTotal['peski'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($chaitraMonthTotal['advance_clearance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($chaitraMonthTotal['last_year_advance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($chaitraMonthTotal['last_year_clearance'])</span>
                            </td>

                            <td class="liability-dr" id="liability-dr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($chaitraMonthTotal['dr_liability'])</span>
                            </td>

                            <td class="liability-cr" id="liability-cr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($chaitraMonthTotal['cr_liability'])</span>
                            </td>
                            <td></td>
                        </tr>
                    @endif
                    @if($voucherModel['baisakh']->count())
                        <td colspan="17" style="text-align: left">वैशाख महिना</td>
                        @foreach($voucherModel['baisakh'] as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td class="kalimati"><span class="" style="display: block; text-align: center"><a
                                                class="e-n-t-n-n" style="text-decoration: none"
                                                href="{{route('voucher.view',$voucher->id)}}"
                                                target="_blank">{{$voucher->jv_number}}</a></span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">

                                    {{--                                    @if($voucher->nikasa_check())--}}
                                    <span class="e-n-t-n-n">@if($voucher->nikasa_check())
                                            @moneyFormat($voucher->nikasa_check())@endif</span>
                                    {{--                                    @--}}
                                </td>
                                <td style="text-align:right">
                                    @if(($voucher->check_payment()))
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati"
                                    style="text-align:left; font-size: 9px">{{$voucher->get_expense_heads()}}</td>
                                {{--                                ब ख रकम--}}
                                <td class="cr-amount" style="text-align:right">
                                    <span class="e-n-t-n-n">{{$voucher->calculate_total_expense()}}</span>
                                </td>
                                <td style="text-align: right"><span
                                            class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class="" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeski())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeskiClearance())</span>
                                </td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right">वैशाख महिनाको जम्मा</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right">
                                <span class="e-n-t-n-n">@moneyFormat($baisakhMonthTotal['nikasa'])</span>
                            </td>
                            <td class="bank-cr-amount" id="this_month_total" style="text-align:right"><span
                                        class="e-n-t-n-n">@moneyFormat($baisakhMonthTotal['cr_tsa'])</span></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right"><span
                                        class="e-n-t-n-n">@moneyFormat($thisMonthExp['baisakh'])</span></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($baisakhMonthTotal['peski'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($baisakhMonthTotal['advance_clearance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($baisakhMonthTotal['last_year_advance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($baisakhMonthTotal['last_year_clearance'])</span>
                            </td>
                            <td class="liability-dr" id="liability-dr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($baisakhMonthTotal['dr_liability'])</span>
                            </td>
                            <td class="liability-cr" id="liability-cr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($baisakhMonthTotal['cr_liability'])</span>
                            </td>
                            <td></td>
                        </tr>
                    @endif
                    @if($voucherModel['jestha']->count())
                        <td colspan="17" style="text-align: left">जेठ महिना</td>
                        @foreach($voucherModel['jestha'] as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td class="kalimati"><span class="" style="display: block; text-align: center"><a
                                                class="e-n-t-n-n" style="text-decoration: none"
                                                href="{{route('voucher.view',$voucher->id)}}"
                                                target="_blank">{{$voucher->jv_number}}</a></span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">

                                    {{--                                    @if($voucher->nikasa_check())--}}
                                    <span class="e-n-t-n-n">@if($voucher->nikasa_check())
                                            @moneyFormat($voucher->nikasa_check())@endif</span>
                                    {{--                                    @--}}
                                </td>
                                <td style="text-align:right">
                                    @if(($voucher->check_payment()))
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati"
                                    style="text-align:left; font-size: 9px">{{$voucher->get_expense_heads()}}</td>
                                {{--                                ब ख रकम--}}
                                <td class="cr-amount" style="text-align:right">
                                    <span class="e-n-t-n-n">{{$voucher->calculate_total_expense()}}</span>
                                </td>
                                <td style="text-align: right"><span
                                            class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class="" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeski())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeskiClearance())</span>
                                </td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right">जेष्ठ महिनाको जम्मा</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($jesthaMonthTotal['nikasa'])</span>
                            </td>
                            <td class="bank-cr-amount" id="this_month_total" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($jesthaMonthTotal['cr_tsa'])</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($thisMonthExp['jestha'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($jesthaMonthTotal['peski'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($jesthaMonthTotal['advance_clearance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($jesthaMonthTotal['last_year_advance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($jesthaMonthTotal['last_year_clearance'])</span>
                            </td>
                            <td class="liability-dr" id="liability-dr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($jesthaMonthTotal['dr_liability'])</span>
                            </td>
                            <td class="liability-cr" id="liability-cr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($jesthaMonthTotal['cr_liability'])</span>
                            </td>
                            <td></td>
                        </tr>
                    @endif
                    @if($voucherModel['ashar']->count())
                        <td colspan="17" style="text-align: left">असार महिना</td>
                        @foreach($voucherModel['ashar'] as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td class="kalimati"><span class="" style="display: block; text-align: center"><a
                                                class="e-n-t-n-n" style="text-decoration: none"
                                                href="{{route('voucher.view',$voucher->id)}}"
                                                target="_blank">{{$voucher->jv_number}}</a></span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">

                                    {{--                                    @if($voucher->nikasa_check())--}}
                                    <span class="e-n-t-n-n">@if($voucher->nikasa_check())
                                            @moneyFormat($voucher->nikasa_check())@endif</span>
                                    {{--                                    @--}}
                                </td>
                                <td style="text-align:right">
                                    @if(($voucher->check_payment()))
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati"
                                    style="text-align:left; font-size: 9px">{{$voucher->get_expense_heads()}}</td>
                                {{--                                ब ख रकम--}}
                                <td class="cr-amount" style="text-align:right">
                                    <span class="e-n-t-n-n">{{$voucher->calculate_total_expense()}}</span>
                                </td>
                                <td style="text-align: right"><span
                                            class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class="" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeski())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->getLastYearPeskiClearance())</span>
                                </td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right">असाढ महिनाको जम्मा</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($asharMonthTotal['nikasa'])</span>
                            </td>
                            <td class="bank-cr-amount" id="this_month_total" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($asharMonthTotal['cr_tsa'])</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($thisMonthExp['ashar'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($asharMonthTotal['peski'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($asharMonthTotal['advance_clearance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($asharMonthTotal['last_year_advance'])</span>
                            </td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($asharMonthTotal['last_year_clearance'])</span>
                            </td>
                            <td class="liability-dr" id="liability-dr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($asharMonthTotal['dr_liability'])</span>
                            </td>
                            <td class="liability-cr" id="liability-cr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($asharMonthTotal['cr_liability'])</span>
                            </td>
                            <td></td>
                        </tr>
                    @endif
                    <tr>
                        <td colspan="3" style="text-align: right">कुल जम्मा</td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right">
                                                        <span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['nikasa'])</span>
                        </td>
                        <td style="text-align: right">
                                                        <span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['cr_tsa'])</span>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTOMonthExp)</span>
                        </td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['peski'])</span>
                        </td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['advance_clearance'])</span>
                        </td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['last_year_advance'])</span>
                        </td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['last_year_clearance'])</span>
                        </td>

                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['dr_liability'])</span>
                        </td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['cr_liability'])</span>
                        </td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
    let changeToNepali = function (text) {
        let trimNumber = $.trim(text);
        let numbers = trimNumber.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '')

                    nepaliNo += " ";
                else if (value == '(')

                    nepaliNo += "(";
                else if (value == ')')
                    nepaliNo += ")";
                else if (value == ' ')
                    nepaliNo += "";
            }
        });
        return nepaliNo;
    };

    let change_all_to_nepali_number = function () {
        $('.e-n-t-n-n').each(function () {
            let nepaliNo = changeToNepali($(this).text());
            let nepaliVal = changeToNepali($(this).val());

            $(this).text(nepaliNo);
            $(this).val(nepaliVal);
        });
    }
</script>

<script>
    $(document).ready(function () {
        change_all_to_nepali_number();
    })
</script>


<script>
    $(document).ready(function () {
        let current_payment = $('#bhuktani_amount').val();
        let trs = $('#malepa-five-table tbody').find('tr');
        let crAmount = 0;
        let dr_liability = 0;
        let cr_liability = 0;
        $.each(trs, function (key, tr) {

            let bank_remain = $.trim($(this).find('td.bank-cr-amount').text());
            let advanceGIven = $.trim($(this).find('td.advance_given').text());


        });

    })
</script>


<script>
    function convertNepaliToEnglish(input) {
        var charArray = input.split('.');
        charArray = charArray[0].split('');
        var engDate = '';
        $.each(charArray, function (key, value) {

            switch (value) {
                case '१':
                    engDate += '1';
                    break;
                case '२':
                    engDate += '2';
                    break;
                case '३':
                    engDate += '3';
                    break;
                case '४':
                    engDate += '4';
                    break;
                case '५':
                    engDate += '5';
                    break;
                case '६':
                    engDate += '6';
                    break;
                case '०':
                    engDate += '0';
                    break;
                case '७':
                    engDate += '7';
                    break;
                case '८':
                    engDate += '8';
                    break;
                case '९':
                    engDate += '9';
                    break;

                case '-':
                    engDate += '-';
                    break
                case ' ':
                    engDate += '';
                    break
            }
        });

        return engDate

    }
</script>

