
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>

        <small>धरौटी व्यक्तिगत खाता</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('retention.depositor.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> नयां बनाउने</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">

          <table class="table table-bordered">
            <thead>
            <tr>
              <th>प्रकार</th>
              <th>नाम नेपाली</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($advanceAndPayments as $advanceAndPayment)
              @if($advanceAndPayment->party_type !=5)
              <tr>
                <td>
                  {{$advanceAndPayment->get_party_type->name}}
                </td>
                <td>{{$advanceAndPayment->name_nep}}</td>

                <td>
                  <a type="button" href="{{route('retention.deposotor.edit',$advanceAndPayment->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                </td>
              </tr>
              @endif
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection