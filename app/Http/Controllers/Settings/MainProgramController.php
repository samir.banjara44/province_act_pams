<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\MainProgram;
use App\Models\Area;
use App\Models\SubArea;
use App\Repositories\MainProgramRepository;
use App\Repositories\AreaRepository;
use App\Repositories\SubAreaRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainProgramController extends Controller
{

    private $mainProgramRepo;
    private $areaRepo;
    private $subAreaRepo;

    public function __construct(
        MainProgramRepository $mainProgramRepo,
        areaRepository $areaRepo,
        subAreaRepository $SubAreaRepo
    )
    {
        $this->middleware('auth');
        $this->mainProgramRepo = $mainProgramRepo;
        $this->areaRepo = $areaRepo;
        $this->subAreaRepo = $SubAreaRepo;
    }

    public function index()
    {

        $mainPrograms= MainProgram::all();
        return view('backend.settings.main_program.index', compact('mainPrograms'));
    }

    public function create(){
        $areas = Area::all();
        $subAreas = SubArea::all();
        return view('backend.settings.main_program.create',compact('areas','subAreas'));
    }
    public function store(Request $request){

        $program = $this->mainProgramRepo->create($request->all());
        $areas = Area::all();
        $subAreas = SubArea::all();
        return view('backend.settings.main_program.create',compact('areas','subAreas'));

//        return redirect(route('admin.main.program'));
    }

    public function get_main_program_by_sub_area(Request $request, $sub_area_id){

        $mainPrograms = $this->mainProgramRepo->get_all_by_sub_area_id($sub_area_id);
        return json_encode($mainPrograms);
    }

}
