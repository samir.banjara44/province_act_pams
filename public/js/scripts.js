$(document).ready(function() {
  $("#showAddForm").click(function() {
    $(this).hide();
    $("#showThisForm").show();
    $("#closeAddForm").show();
  });

  $("#closeAddForm").click(function() {
    $(this).hide();
    $("#showThisForm").hide();
    $("#showAddForm").show();
  });
});
