@extends('backend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User
                <small>Control panel</small>
            </h1>
            <ul class="breadcrumb">
                <li><a type="button" href="{{route('admin.user')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('admin.user.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" placeholder="Name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="text" class="form-control" id="email" placeholder="Email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" class="form-control" id="username" placeholder="Username"
                                   name="username">
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="text" class="form-control" id="password" placeholder="Password"
                                   name="password">
                        </div>
                        <div class="form-group">
                            <label for="province">Province</label>
                            <select name="province_id" class="form-control">
                                <option>.........................</option>
                                @foreach($provinces as $province)
                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="mof">MOF</label>
                            <select name="mof_id" class="form-control" id="mof">
                                <option>.........................</option>
                                @foreach($mofs as $mof)
                                    <option value="{{$mof->id}}">{{$mof->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ministry">Ministry</label>
                            <select name="ministry_id" class="form-control" id="ministry">
                                @foreach($ministries as $ministry)
                                    <option value="{{$ministry->id}}">{{$ministry->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="department">Department</label>
                            <select name="department_id" class="form-control" id="department">
                                <option>.........................</option>
                                @foreach($departments as $department)
                                    <option value="{{$department->id}}">{{$department->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="office">Office</label>
                            <select name="office_id" class="form-control" id="office">
                                <option>.........................</option>

                                @foreach($offices as $office)
                                    <option value="{{$office->id}}">{{$office->name}}
                                     @if($office->ministry)| {{$office->ministry->name}}@endif</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="role">Role</label>
                            <select name="role_id" class="form-control" id="role">
                                <option>.........................</option>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('script')
    <script>
        $(document).on('click', '#ministry', function () {
            alert("test");
        })
    </script>

@endsection