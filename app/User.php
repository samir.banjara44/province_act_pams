<?php

namespace App;

use App\Models\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'province_id', 'mof_id', 'ministry_id', 'department_id', 'office_id', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles() {
        return $this->belongsToMany('App\Models\Role', 'user_roles', 'user_id', 'role_id');
    }

    public function province(){
        return $this->belongsTo('App\Models\Province');
    }

    public function mof(){
        return $this->belongsTo('App\Models\Mof');
    }

    public function ministry(){
        return $this->belongsTo('App\Models\Ministry');
    }

    public function department(){
        return $this->belongsTo('App\Models\Department');
    }

    public function office(){
        return $this->belongsTo('App\Models\Office');
    }

    public function hasRole($role)  // Check if given role exists
    {
        if ($this->roles()) {
            $roles = $this->roles;
            return $roles->contains('name', $role);
        }
        return false;
    }

    public function attachRole($role) {
        if (is_object($role)) {
            $role = $role->getKey();
        }
        if (is_array($role)) {
            $role = $role['id'];
        }
        $this->roles()->attach($role);
    }

    public function detachRole($role) {
        if (is_object($role)) {
            $role = $role->getKey();
        }
        if (is_array($role)) {
            $role = $role['id'];
        }
        $this->roles()->detach($role);
    }

    public function attachRoles($roles) {
        foreach ($roles as $role) {
            $this->attachRole($role);
        }
    }

    public function detachRoles($roles) {
        foreach ($roles as $role) {
            $this->detachRole($role);
        }
    }

    public function isSuperUser() {
        return (bool)$this->is_admin;
    }


}
