
@extends('frontend.layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <i class="fas fa-atlas"></i> बजेट उपशिर्षक विवरण </h1>

    <button type="button" class="btn btn-primary btn-sm btn-show-form" id="showAddForm"><i class="fas fa-plus"></i> नयाँ थप गर्ने</button>
  </section>

  <section class="formSection displayNone" id="showThisForm">
    <div class="row justify-content-md-center">
      <div class="col-md-8 form-bg col-md-offset-2">
        <button type="button" class="btn btn-danger btn-sm btn-show-form displayNone" id="closeAddForm"><i class="fas fa-times"></i> फर्म रद्ध गर्ने</button>
        <div class="form-title">बजेट उपशिर्षक बनाउने</div>
        <form action="{{route('program.store')}}" method="post" id="programCreateForm">
          <div class="row">
            <div class="col-md-12">
              {{csrf_field()}}
              <div class="form-group">
                <div class="row">
                  <div class="col-md-4">
                    <label for="name">बजेट उपशिर्षक नाम:</label>
                    <input type="text" class="form-control" id="name" placeholder="बजेट उपशिर्षक" name="name">
                  </div>
                  <div class="col-md-4">
                    <label for="program_code">बजेट उपशिर्षक नम्बर:</label>
                    <input type="text" class="form-control" id="program_code" placeholder="बजेट उपशिर्षक कोड" name="program_code">
                  </div>
                  <div class="col-md-2">
                    <label for="expense_type">प्रकार:</label>
                    <select class="form-control" name="expense_type">
                      <option value="1">चालु</option>
                      <option value="2">पुजीं</option>
                    </select>
                  </div>
                  <div class="col-md-2">
                    <button type="submit" class="btn btn-primary btn-block" id="btnProgramSave" style="margin-top: 18px;"> <i class="fas fa-save"></i> सेभ गर्ने</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>क्र.स.</th>
              <th>बजेट उपशिर्षक नाम</th>
              <th>बजेट उपशिर्षक कोड</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($programes as $index=>$program)
            <tr>
              <td>{{++$index}}</td>
              <td>{{$program->name}}</td>
              <td>{{$program->program_code}}</td>

              <td>
                <a type="button" href="{{route('program.edit',$program->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                @if($program->akhtiyari == null)
                <a type="button" href="#" id="delete_program" data-program-id="{{$program->id}}" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i> Delete</a>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>

@endsection

@section('scripts')
<script>
  $(document).on('click','#delete_program',function () {

    let program_id = $(this).attr('data-program-id');
    let url = '{{route('program.delete',123)}}';
    url = url.replace('123',program_id);
    swal({
      title: "Are you sure?",
      text: "Delete भए पछि Recovere हुदैन",
      icon: "warning",
      buttons: true,
      dangerMode: true,

    }).then((willDelete) => {

      if (willDelete) {

        $.ajax({

          url : url,
          method : 'get',
          success : function (res) {
            if(res){
              swal("Budget Sub Head has been deleted!", {
                icon: "success",
              });

              location.reload();
            }

          }
        })

      } else {
      // swal("Your imaginary file is safe!");
      }
    });
  });
</script>

@endsection