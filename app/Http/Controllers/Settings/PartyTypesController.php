<?php

namespace App\Http\Controllers\Settings;

use App\Models\PartyTypes;
use App\Repositories\PartyTypesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartyTypesController extends Controller
{

    public function __construct(PartyTypesRepository $partytypesRepo)
    {

        $this->middleware('auth');
        $this->partytypesRepo = $partytypesRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partytypes = PartyTypes::all();
        return view('backend.settings.PartyTypes.index',compact('partytypes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.settings.PartyTypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $partytype = $this->partytypesRepo->create($request->all());
        return redirect(route('partytypes'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
