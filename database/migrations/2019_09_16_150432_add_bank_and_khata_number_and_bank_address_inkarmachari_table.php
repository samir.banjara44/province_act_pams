<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankAndKhataNumberAndBankAddressInkarmachariTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('karmacharis', function (Blueprint $table) {

        //     $table->integer('bank')->nullable();
        //     $table->string('khata_number')->nullable();
        //     $table->string('bank_address')->nullable();

        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::drop('bank');
        Schema::drop('khata_number');
        Schema::drop('bank_address');
    }
}
