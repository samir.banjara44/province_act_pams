@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-edit"></i> प्रयोगकर्ता सम्पादन फारम </h1>

            <a href="{{route('front.user.index',Auth::user()->office->id)}}" class="btn btn-sm btn-primary btn-show-form"><i class="fa fa-arrow-left"></i>
                लिष्टमा जाने</a>
        </section>

        <!-- FORM section start -->
        <section class="formSection">
            <div class="row justify-content-md-center">
                <div class="col-md-8 form-bg col-md-offset-2">
                    <div class="form-title">प्रयोग कर्ता सम्पादन गर्ने</div>
                    <form action="{{route('user.update',$user_id)}}" method="post">
                        {{csrf_field()}}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="name">पुरा नाम:</label>
                                        <input type="text" class="form-control" value="{{$user->name}}" id="name" placeholder="Name" name="name"
                                               required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="email">ई-मेल ठगाना:</label>
                                        <input type="text" class="form-control" value="{{$user->email}}" id="email" placeholder="Email" name="email"
                                               required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="username">युजर नेम:</label>
                                        <input type="text" value="{{$user->username}}" class="form-control" id="username"
                                               name="username" required>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="password">पासवर्ड:</label>
                                        <input type="password"    class="form-control" id="" placeholder=""
                                               name="password" autocomplete="off">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="role">रोल</label>
                                        <select name="role_id" class="form-control" id="role" required>
                                            <option value="">.........................</option>
                                            <option @if($userRole->role_id == 2) selected @endif value="2">Admin</option>
                                            <option @if($userRole->role_id == 5) selected @endif value="5">Report</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="role">स्टाटस</label>
                                        <select name="status" class="form-control" id="status" required>
                                            <option value="">.........................</option>
                                            <option @if($user->status == 1) selected @endif value="1">सक्रिय</option>
                                            <option @if($user->status == 2) selected @endif value="2">निस्क्रिय</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <center>
                            <button type="submit" class="btn btn-success btn-show-form"><i class="fas fa-check-square"></i> सम्पादन गर्ने</button>
                        </center>
                    </form>
                </div>
            </div>
        </section>
        <!-- FORM section ends -->

    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            setTimeout(
                function() { $(':password').val(''); },
                800  //1,000 milliseconds = 1 second
            );
        })
    </script>

@endsection