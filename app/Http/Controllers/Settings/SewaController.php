<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Sewa;
use App\Repositories\SewaRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SewaController extends Controller
{

    private $sewaRepo;

    public function __construct(SewaRepository $sewaRepo)
    {
        $this->middleware('auth');
        $this->sewaRepo = $sewaRepo;
    }

    public function index()
    {
        $sewas= $this->sewaRepo->get_all_sewas();
        return view('backend.settings.sewas.index', compact('sewas'));
    }

    public function create(){

        return view('backend.settings.sewas.create');
    }
    public function store(Request $request){
        $sewa = $this->sewaRepo->create($request->all());
        return redirect(route('admin.sewas'));
    }

}
