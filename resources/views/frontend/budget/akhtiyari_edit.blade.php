@extends('frontend.layouts.app')
<style>
    th {
        color: white;
        font-size: 16px;
    }
</style>
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-edit"></i> अख्तियारी विवरण संसोधन</h1>
        </section>

        <!-- FORM section start -->
        <section class="formSection">
            <div class="row justify-content-md-center">
                <div class="col-md-10 form-bg col-md-offset-1">
                    <form action="" method="post" id="akhtiyariUpdateForm" class="pt-10">
                        {{csrf_field()}}
                        {{-- <p>बजेट विनियोजन : <span style="color: red">{{$totalBudgetOnAkhtiyar}}</span> </p>--}}
                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-2">
                                    <label for="fiscal_year"> मिति:</label>
                                    <input type="text" class="form-control" name="date" id="date" value="" required>
                                    <input type="hidden" name="date_in_roman" id="date_in_roman" value="">
                                </div>

                                <div class="col-md-2">
                                    <label>आर्थिक वर्ष :</label>
                                    <select id="fiscal_year" class="form-control" name="fiscal_year" required>
                                        @foreach($fiscalYears as $fy)
                                            <option value="{{$fy->id}}" @if($fy->id == $akhtiyary->fiscal_year) selected @endif>{{$fy->year}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label>बजेट उप शीर्षक :</label>
                                    <select name="budget_sub_head" class="form-control" id="budget_sub_head" required disabled>
                                        <option value="">................</option>
                                        @foreach($programs as $program)
                                            <option value="{{$program->id}}" @if($program->id ==
                                        $akhtiyary->budget_sub_head_id)
                                            selected @endif>{{$program->program_code}} {{$program->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="source_type">स्रोत प्रकार</label>
                                    <select name="source_type" class="form-control" id="source_type" required>
                                        <option value="">...............</option>
                                        @foreach($sources as $source)
                                            <option value="{{$source->id}}" @if($source->id == $akhtiyary->source_type)
                                            selected
                                                    @endif>{{$source->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="source_level">बजेट स्रोत तह</label>
                                    <select name="source_level" class="form-control" id="source_level" required>
                                        <option value=""></option>
                                    </select>
                                </div>

                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-3">
                                    <label for="source">स्रोत</label>
                                    <select name="source" class="form-control select2" id="source" required>
                                        <option value="">-----</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="medium">प्राप्ति विधि</label>
                                    <select name="medium" class="form-control medium" id="medium" required>
                                        @foreach($mediums as $medium)
                                            <option value="{{$medium->id}}">{{$medium->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label>रकम</label>
                                    <input type="number" class="form-control" name="amount" id="amount"
                                           value="{{$akhtiyary->amount}}">
                                    <input type="hidden" name="hidden_total_budget" id="hidden_total_budget"
                                           value="{{$totalBudgetOnAkhtiyar}}">
                                </div>

                                <div class="col-md-2">
                                    <label>अख्तियारि</label>
                                    <select class="form-control" id="akhtiyari" name="akhtiyar_type">
                                        <option value="" selected>.........</option>
                                        <option value="1" @if($akhtiyary->akhtiyari_type == 1) @endif selected>सुरु
                                        </option>
                                        <option value="2" @if($akhtiyary->akhtiyari_type == 2) @endif selected>थप
                                        </option>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label>विवरण</label>
                                    <input type="text" name="detail" id="detail" class="form-control"
                                           value="{{$akhtiyary->detail}}">
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <center>
                                        <input type="submit" class="btn btn-primary" name="btnAkhtiyariUpdate"
                                               id="btnAkhtiyariUpdate" value="Update">
                                        <a type="button" class="btn btn-primary" href="{{route('akhtiyari.param')}}">Clear
                                        </a>
                                    </center>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </section>

        <!-- /.content -->

    </div>

@endsection

@section('scripts')



    {{--    बजेट उपशिर्षक click गर्दा आख्तियारि ल्याउने--}}
    <script>
        $(document).on('change', '#budget_sub_head', function () {
            let budget_sub_head = $('select#budget_sub_head').val();
            let fiscalYear = $('#fiscal_year').val();
            let url = '{{route('getAkhtiyari',['123','345'])}}';
            url = url.replace('123', fiscalYear);
            url = url.replace('345', budget_sub_head);
            $.ajax({
                method: 'get',
                url: url,
                success: function (res) {
                    let parseRes = $.parseJSON(res);
                    let tr = '';
                    if (parseRes.length > 0) {

                        let i = 1;
                        $.each($.parseJSON(res), function () {
                            let url = "{{route('edit.akhtiyari',123)}}";
                            url = url.replace('123', this.id);
                            let data = '';
                            if (this.akhtiyari_type == 1) {

                                data = 'शुरु'
                            } else {
                                data = 'थप'
                            }

                            tr += "<tr style='background-color: white; color: black'>" +
                                "<td>" +
                                i +
                                "</td>" +

                                "<td class='activity' data-id=''>" +
                                this.program.name +
                                "</td>" +

                                "<td class='byahora'>" +
                                this.source_type.name +
                                "</td>" +

                                "<td class='details'>" +
                                this.source_level.name +
                                "</td>" +

                                "<td class='drAmount'>" +
                                this.source.name +
                                "</td>" +

                                "<td class='drAmount'>" +
                                this.medium.name +
                                "</td>" +

                                "<td class='drAmount'>" +
                                data +
                                "</td>" +

                                "<td class='drAmount'>" +
                                this.detail +
                                "</td>" +

                                "<td class='drAmount'>" +
                                this.amount +
                                "</td>";
                            i = i + 1;

                        });

                        let totalTr = '';
                        totalTr += "<tr style='background-color: white; color: black'>" +
                            "<td colspan='8' style='text-align: right'>" +
                            "जम्मा" +
                            "</td>" +
                            "<td>" +

                            "</td>";

                        $('#tblAkhtiyari tbody').html('');
                        $('#tblAkhtiyari tbody').append(tr);
                        $('#tblAkhtiyari tr:last').after(totalTr);
                    } else {

                        tr += "<tr>" +
                            "<td colspan='8' align='center'>" +
                            "विवरण प्रविश्टि भएको छैन !!" +
                            "</td>";
                        $('#tblAkhtiyari').find('tbody').html(tr);
                    }

                }
            })
        })
    </script>


    {{--स्रोत प्रकार को आधारमा स्रोत आउने--}}
    <script>
        $(document).ready(function () {
            $('#source_type').change(function () {
                getSourceBySourceType()
            })
        });


        let getSourceBySourceType = function () {
            let sourceTypeId = $('#source_type').val();

            let url = '{{route('admin.get_source_by_source_type',123)}}';
            url = url.replace(123, sourceTypeId);
            let source = '{{$akhtiyary->source}}';
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {

                    let options = '';

                    let result = $.parseJSON(res);
                    if (result.length > 0) {

                        $.each($.parseJSON(res), function () {
                            if (source == this.id)
                                options += '<option value="' + this.id + '" selected>' + this.name + '</option>';
                            else
                                options += '<option value="' + this.id + '">' + this.name + '</option>'


                        })
                    }

                    $('#source').html(options);

                }
            })
        }
    </script>




    {{--स्रोत प्रकार को आधारमा बजेट स्रोत तह आउने--}}
    <script>
        $(document).ready(function () {
            $('#source_type').change(function () {
                getSourceLevelBySourceType();
            })
        });


        let getSourceLevelBySourceType = function () {
            let sourceTypeId = $('#source_type').val();

            let url = '{{route('admin.get_source_level_by_source_type',123)}}';
            url = url.replace(123, sourceTypeId);
            let source_level = '{{$akhtiyary->source_level}}';
            // alert(source_level);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {

                    let options = '<option>............</option>';
                    let temp = $.parseJSON(res);
                    if (temp.length > 1) {
                        $.each($.parseJSON(res), function () {
                            if (source_level == this.id)
                                options += '<option value="' + this.id + '" selected>' + this.name + '</option>';
                            else
                                options += '<option value="' + this.id + '">' + this.name + '</option>'
                        })
                    } else {
                        options += '<option value="' + temp[0].id + '" selected>' + temp[0].name + '</option>'
                    }

                    $('#source_level').html(options).change();
                    $()

                }
            })
        }
    </script>


    <script>
        $(document).ready(function () {
            getSourceBySourceType();
            getSourceLevelBySourceType();
        })
    </script>


    {{--Sunmit button click हुदा--}}
    <script>
        $(document).ready(function () {
            $('#btnAkhtiyariUpdate').click(function (e) {
                e.preventDefault();

                let totalBudgetOnAkhtiyar = $('#hidden_total_budget').val();
                let current_amount = $('#amount').val();
                if ((parseFloat(current_amount) >= parseFloat(totalBudgetOnAkhtiyar))) {
                    // alert("ok");
                    // return false;
                    let date = $('input#date').val();
                    let nepali_date_in_roman = convertNepaliToEnglish(date);
                    let akhtiyariID = '{{$akhtiyary->id}}';

                    $('#date_in_roman').val(nepali_date_in_roman);

                    let url = '{{route('akhtiyari.update',123)}}';
                    url = url.replace('123', akhtiyariID);
                    $('form#akhtiyariUpdateForm').attr('action', url);

                    $('#akhtiyariUpdateForm').submit();


                } else {

                    alert("बजेट विनियोजन भन्दा कम भयो");
                    $('#amount').val(totalBudgetOnAkhtiyar);
                }
                // alert(totalBudgetOnAkhtiyar);


            })
        })
    </script>


    {{--Date Picker --}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);
    </script>


    <script>
        $(document).ready(function () {
            let nepali_date_roman = '{{$akhtiyary->date_nepali_roman}}';
            let nepali_temp_nepali = convertEnglishToNepali(nepali_date_roman.substr(0, 10));
            $('input#date').val(nepali_temp_nepali);
        })
    </script>
    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            var charArray = input.split('');
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1';
                        break;
                    case '२':
                        engDate += '2';
                        break;
                    case '३':
                        engDate += '3';
                        break;
                    case '४':
                        engDate += '4';
                        break;
                    case '५':
                        engDate += '5';
                        break;
                    case '६':
                        engDate += '6';
                        break;
                    case '०':
                        engDate += '0';
                        break;
                    case '७':
                        engDate += '7';
                        break;
                    case '८':
                        engDate += '8';
                        break;
                    case '९':
                        engDate += '9';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
            });
            return engDate

        }
    </script>

    {{--Convert ROman date to Nepali date --}}
    <script>
        function convertEnglishToNepali(input) {
            var charArray = input.split('');
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '1':
                        engDate += '१';
                        break;
                    case '2':
                        engDate += '२';
                        break;
                    case '3':
                        engDate += '३';
                        break;
                    case '4':
                        engDate += '४';
                        break;
                    case '5':
                        engDate += '५';
                        break;
                    case '6':
                        engDate += '६';
                        break;
                    case '7':
                        engDate += '७';
                        break;
                    case '8':
                        engDate += '८';
                        break;
                    case '9':
                        engDate += '९';
                        break;
                    case '0':
                        engDate += '०';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
            });
            return engDate;

        }
    </script>

    {{--    validation--}}
    <script>
        function validation() {

            let flag = 1;
            // budget_sub_head validation
            let budget_sub_head_id = $('#budget_sub_head');
            if (!budget_sub_head_id.val()) {
                if (budget_sub_head_id.siblings('p').length == 0) {
                    budget_sub_head_id.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                budget_sub_head_id.focus();
                flag = 0;
            } else {
                budget_sub_head_id.siblings('p').remove()
            }

            let source_type_id = $('#source_type');
            if (!source_type_id.val()) {
                if (source_type_id.siblings('p').length == 0) {
                    source_type_id.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                source_type_id.focus();
                flag = 0;
            } else {
                source_type_id.siblings('p').remove()
            }

            let source_level_id = $('#source_level');
            if (!source_level_id.val()) {
                if (source_level_id.siblings('p').length == 0) {
                    source_level_id.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                source_level_id.focus();
                flag = 0;
            } else {
                source_level_id.siblings('p').remove()
            }

            let source_id = $('#source');
            if (!source_id.val()) {
                if (source_id.siblings('p').length == 0) {
                    source_id.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                source_id.focus();
                flag = 0;
            } else {
                source_id.siblings('p').remove()
            }

            let medium_id = $('#medium');
            if (!medium_id.val()) {
                if (medium_id.siblings('p').length == 0) {
                    medium_id.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                medium_id.focus();
                flag = 0;
            } else {
                medium_id.siblings('p').remove()
            }

            let amount_val = $('#amount');
            if (!amount_val.val()) {
                if (amount_val.siblings('p').length == 0) {
                    amount_val.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                amount_val.focus();
                flag = 0;
            } else {
                amount_val.siblings('p').remove()
            }


            let akhtiyari_val = $('#akhtiyari');
            if (!akhtiyari_val.val()) {
                if (akhtiyari_val.siblings('p').length == 0) {
                    akhtiyari_val.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                akhtiyari_val.focus();
                flag = 0;
            } else {
                akhtiyari_val.siblings('p').remove()
            }

            let detail_val = $('#detail');
            if (!detail_val.val()) {
                if (detail_val.siblings('p').length == 0) {
                    detail_val.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                detail_val.focus();
                flag = 0;
            } else {
                detail_val.siblings('p').remove()
            }

            return flag;
        }

    </script>

@endsection