@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        समूह
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('samuha')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('samuha.store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <label for="name">Name:</label>
              <input type="text" class="form-control" id="name" placeholder="Samuha Name" name="name">
            </div>

            <div class="form-group">
              <label for="sewa">Sewa:</label>
              <select class="form-control" id="sewa_id" name="sewa_id">
                <option value="11">......</option>
                @foreach($sewas as $sewa)
                <option value="{{$sewa->id}}">{{$sewa->name}}</option>
                @endforeach
              </select>
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection