<?php

namespace App\Repositories;

use App\Models\ActivityProgress;
use http\Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class ActivityProgressRepositoryEloquent implements ActivityProgressRepository
{
   public function store($attributes){
       $activityProgress = new ActivityProgress();
       $activityProgress->office_id = $attributes['expense_center'];
       $activityProgress->budget_sub_head_id = $attributes['program'];
       $activityProgress->budget_details_id = $attributes['main_activity_id'];
       $activityProgress->activity_progress_detail = $attributes['progress'];
       $activityProgress->date_nep = $attributes['date'];
       $activityProgress->chaumasik_type = $attributes['chaumasik_type'];
       $activityProgress->progress_quantity = $attributes['progress_quantity'];
       $dateExplode = explode('-',$attributes['date']);
       $month = (int)$dateExplode[1];
       if ($month == 1) {
           $finalmonth = 10;
       }
       if ($month == 2) {
           $finalmonth = 11;
       }
       if ($month == 3) {
           $finalmonth = 12;
       }
       if ($month == 4) {
           $finalmonth = 1;
       }
       if ($month == 5) {
           $finalmonth = 2;
       }
       if ($month == 6) {
           $finalmonth = 3;
       }
       if ($month == 7) {
           $finalmonth = 4;
       }
       if ($month == 8) {
           $finalmonth = 5;
       }
       if ($month == 9) {
           $finalmonth = 6;
       }
       if ($month == 10) {
           $finalmonth = 7;
       }
       if ($month == 11) {
           $finalmonth = 8;
       }
       if ($month == 12) {
           $finalmonth = 9;
       }
       $activityProgress->month = $finalmonth;
       $activityProgress->status = 1;
       $activityProgress->user_id = Auth::user()->id;
       $activityProgress->fiscal_year = $attributes['fiscal_year'];
       $activityProgress->save();
   }

   public function getProgressByOffice($officeId){
       return ActivityProgress::where('office_id',$officeId)->get();
   }

   public function findById($id){
       return ActivityProgress::findorfail($id);
   }

   public function update($attributes, $activityProgressId){
      $activityProgress =  $this->findById($activityProgressId);
       $activityProgress->budget_sub_head_id = $attributes['program'];
       $activityProgress->budget_details_id = $attributes['main_activity_id'];
       $activityProgress->activity_progress_detail = $attributes['progress'];
       $activityProgress->date_nep = $attributes['date'];
       $activityProgress->chaumasik_type = $attributes['chaumasik_type'];
       $activityProgress->progress_quantity = $attributes['progress_quantity'];
       $dateExplode = explode('-',$attributes['date']);
       $month = (int)$dateExplode[1];
       if ($month == 1) {
           $finalmonth = 10;
       }
       if ($month == 2) {
           $finalmonth = 11;
       }
       if ($month == 3) {
           $finalmonth = 12;
       }
       if ($month == 4) {
           $finalmonth = 1;
       }
       if ($month == 5) {
           $finalmonth = 2;
       }
       if ($month == 6) {
           $finalmonth = 3;
       }
       if ($month == 7) {
           $finalmonth = 4;
       }
       if ($month == 8) {
           $finalmonth = 5;
       }
       if ($month == 9) {
           $finalmonth = 6;
       }
       if ($month == 10) {
           $finalmonth = 7;
       }
       if ($month == 11) {
           $finalmonth = 8;
       }
       if ($month == 12) {
           $finalmonth = 9;
       }
       $activityProgress->month = $finalmonth;
       $activityProgress->status = 1;
       $activityProgress->user_id = Auth::user()->id;
       $activityProgress->fiscal_year = $attributes['fiscal_year'];
      return $activityProgress->save();
   }

   public function getByBudgetSubHead($data){
       return $progressReport = ActivityProgress::where('office_id',$data['office'])->where('budget_sub_head_id',$data['budget_sub_head'])->get();
   }
}
