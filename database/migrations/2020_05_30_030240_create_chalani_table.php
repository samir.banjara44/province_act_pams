<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChalaniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chalani', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('office_id');
            $table->integer('chalani_number');
            $table->string('letter_number');
            $table->dateTime('chalan_date');
            $table->text('body');
            $table->string('to_office');
            $table->string('type')->nullable();;
            $table->string('medium')->nullable();;
            $table->string('to_office_address')->nullable();;
            $table->string('purpose')->nullable();;
            $table->integer('status');
            $table->string('branch');
            $table->string('fiscal_year');
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chalani');
    }
}
