<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">


<style>
    table {
        border-collapse: collapse;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="font-size:13px">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                {{--          <td colspan="6" style="text-align: center"><b>प्रदेश सरकार</b></td>--}}
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
            </tr>
            @if(Auth::user()->office->department)
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->department->name}}</td>
                </tr>
            @endif
            <tr>
                <td colspan="6" style="text-align: center"><b>{{$office[0]->name}}</b></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>दातृ निकायगत बजेट र खर्चको आर्थिक बिवरण</b><br>
                    </div>

                </td>
            </tr>
            <tr>
                <div>
                    <td style="text-align: center">
                        <b>आ.व./अवधिको : <span class="e-n-t-n-n">{{$fiscalYear->year}}</span></b>
                    </td>
                </div>
            </tr>
            <tr>
                <td>
                    <br>
                    <br>

                </td>
            </tr>
            <tr>
                <td class="kalimati">बजेट उपशीर्षक न. : <b><span
                                class="kalimati">{{$program->program_code}}</span></b><br>
                    बजेट उपशीर्षक नाम: <b>{{$program->name}}</b></td>


            </tr>

        </table>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table" width="99%" border="1" style="background-color:#dbdbdb; font-size: 12px"
                       id="fatwari_table">
                    <thead>
                    <tr>
                        <th rowspan="2">दातृ निकाय</th>
                        <th rowspan="2">खर्च संकेत</th>
                        <th colspan="2">स्रोतको</th>
                        <th rowspan="2">शुरु बजेट</th>
                        <th colspan="2">संशोधन /रकमान्तर/श्रोतान्तर बाट</th>
                        <th rowspan="2">अन्तिम बजेट</th>
                        <th rowspan="2">निकासा</th>
                        <th rowspan="2">जम्मा खर्च</th>
                        <th rowspan="2">बाँकी बजेट</th>
                        <th rowspan="2">गत वर्षको खर्च</th>
                    </tr>
                    <tr>
                        <th>प्रकार</th>
                        <th>भुक्तानी विधि</th>
                        <th>थप</th>
                        <th>घट</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sourceTypes as $index=>$sourceType)
                        <?php
                        $i = 1;
                        ?>
                        @foreach($sourceType->getExpenseHead($datas['officeId'],$datas['budgetSUbHead'],$sourceType->id) as $index=>$expenseHead)
                            <tr style="background-color: white">

                                <td class="kalimati">@if($i){{$sourceType->name}}@endif</td>
                                <td class=""><span class="e-n-t-n-n">{{$expenseHead->expense_head_by_id->expense_head_code}}</span></td>
                                <td>@if($sourceType->id == 2)प्रदेश सरकार @elseसघिंय सरकार @endif</td>
                                <td>नगद अनुदान</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->getInitialBudget($datas['officeId'],$datas['budgetSUbHead'],$expenseHead->expense_head_id,$sourceType->id)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->getAddBudget($datas['officeId'],$datas['budgetSUbHead'],$expenseHead->expense_head_id,$sourceType->id)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->getReduceBudget($datas['officeId'],$datas['budgetSUbHead'],$expenseHead->expense_head_id,$sourceType->id)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->getFinalBudget($datas['officeId'],$datas['budgetSUbHead'],$expenseHead->expense_head_id,$sourceType->id)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->getTotalExpense($datas['officeId'],$datas['budgetSUbHead'],$expenseHead->expense_head_id,$sourceType->id)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->getTotalExpense($datas['officeId'],$datas['budgetSUbHead'],$expenseHead->expense_head_id,$sourceType->id)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->remainBudget($datas['officeId'],$datas['budgetSUbHead'],$expenseHead->expense_head_id,$sourceType->id)}}</td>
                                <td></td>

                                <?php
                                $i = 0;
                                ?>
                                @endforeach
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align: right">जम्मा</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->getTotalInitialBudget($datas['officeId'],$datas['budgetSUbHead'],$sourceType->id)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->getTotalAddBudget($datas['officeId'],$datas['budgetSUbHead'],$sourceType->id)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->getTotalReduceBudget($datas['officeId'],$datas['budgetSUbHead'],$sourceType->id)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->getTotalFinalBudget($datas['officeId'],$datas['budgetSUbHead'],$sourceType->id)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->getGranTotalExpense($datas['officeId'],$datas['budgetSUbHead'],$sourceType->id)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->getGranTotalExpense($datas['officeId'],$datas['budgetSUbHead'],$sourceType->id)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{$sourceType->totalRemainBudget($datas['officeId'],$datas['budgetSUbHead'],$sourceType->id)}}</td>
                                <td></td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>


                <table width="99%" style="font-size: 13px">

                    <tr>
                        <td style="padding-left: 52px;">तयार गर्ने :</td>
                        <td>पेश गर्ने :</td>
                        <td>सदर गर्ने :</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">पद :</td>
                        <td>पद :</td>
                        <td>पद :</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">मिति :</td>
                        <td>मिति :</td>
                        <td>मिति :</td>
                    </tr>

                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script>
    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '/')

                    nepaliNo += "/";
            }
        });
        // console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });


</script>

<script>
    $(document).ready(function () {
        let trs = $('#fatwari_table tbody').find('tr:not(.second_last,.last)');
        let total_budget = 0;
        let total_this_month_expense = 0;
        let total_up_to_prev_month_expense = 0;
        let total_up_to_this_month_expense = 0;
        let total_advance = 0;
        let total_exp_with_out_advance = 0;
        let total_remain_budget = 0;
        let expense_percentage = 0;
        // alert(trs.length);
        $.each(trs, function () {

            let total_budget_nep = $.trim($(this).find('td.total-budget').text());
            let total_this_month_expense_nep = $.trim($(this).find('td.this-month-exp').text());
            let total_up_to_prev_month_expense_nep = $.trim($(this).find('td.up-to-pre-month-exp').text());
            let total_up_to_this_month_expense_nep = $.trim($(this).find('td.up-to-this-month-exp').text());
            let advance_nep = $.trim($(this).find('td.advance').text());
            let exp_with_out_advance_nep = $.trim($(this).find('td.exp-with-out-advance').text());
            let remain_budget_nep = $.trim($(this).find('td.remain-budget').text());


            total_budget_roman = convertNepaliToEnglish(total_budget_nep);
            total_this_month_expense_roamn = convertNepaliToEnglish(total_this_month_expense_nep);

            total_up_to_prev_month_expense_roamn = convertNepaliToEnglish(total_up_to_prev_month_expense_nep);
            total_up_to_this_month_expense_roamn = convertNepaliToEnglish(total_up_to_this_month_expense_nep);
            advence_roamn = convertNepaliToEnglish(advance_nep);
            exp_with_out_advance_roman = convertNepaliToEnglish(exp_with_out_advance_nep);
            remain_budget_roman = convertNepaliToEnglish(remain_budget_nep);
            // alert(remain_budget_roman);

            total_budget = parseFloat(total_budget) + parseFloat(total_budget_roman);
            total_this_month_expense = parseFloat(total_this_month_expense) + parseFloat(total_this_month_expense_roamn);
            total_up_to_prev_month_expense = parseFloat(total_up_to_prev_month_expense) + parseFloat(total_up_to_prev_month_expense_roamn);
            total_up_to_this_month_expense = parseFloat(total_up_to_this_month_expense) + parseFloat(total_up_to_this_month_expense_roamn);
            total_advance = parseFloat(total_advance) + parseFloat(advence_roamn);
            total_exp_with_out_advance = parseFloat(total_exp_with_out_advance) + parseFloat(exp_with_out_advance_roman);
            total_remain_budget = parseFloat(total_remain_budget) + parseFloat(remain_budget_roman);
            expense_percentage = (parseFloat(total_up_to_this_month_expense) * 100) / parseFloat(total_budget)


        });


        $('#total_last_budget').text(changeToNepali(total_budget.toString()));
        $('#this_month_nikasa').text(changeToNepali(total_up_to_this_month_expense.toString()));
        $('#this_month_exp').text(changeToNepali(total_this_month_expense.toString()));
        $('#up_to_prev_month_exp').text(changeToNepali(total_up_to_prev_month_expense.toString()));
        $('#up_to_this_month_exp').text(changeToNepali(total_up_to_this_month_expense.toString()));
        $('#advance').text(changeToNepali(total_advance.toString()));
        $('#exp_with_out_advance').text(changeToNepali(total_exp_with_out_advance.toString()));
        $('#remain_budget').text(changeToNepali(total_remain_budget.toString()));
        $('#total_expense_percentage').text(changeToNepali(expense_percentage.toFixed(3)));

        // console.log(expense_percentage,'%');

        // alert(total_remain_budget);
    })
</script>

<script>
    function convertNepaliToEnglish(input) {
        // console.log(input);
        var charArray = input.split('');
        console.log('Test', charArray[0]);
        // charArray = charArray[0].split('');
        var engDate = '';
        $.each(charArray, function (key, value) {

            switch (value) {
                case '१':
                    engDate += '1'
                    break
                case '२':
                    engDate += '2'
                    break
                case '३':
                    engDate += '3'
                    break
                case '४':
                    engDate += '4'
                    break
                case '५':
                    engDate += '5'
                    break
                case '६':
                    engDate += '6'
                    break
                case '०':
                    engDate += '0'
                    break
                case '७':
                    engDate += '7'
                    break
                case '८':
                    engDate += '8'
                    break
                case '९':
                    engDate += '9'
                    break

                case '-':
                    engDate += '-'
                    break
                case '.':
                    engDate += '.'
                    break
            }
            // console.log(engDate)
        })

        return engDate

    }
</script>