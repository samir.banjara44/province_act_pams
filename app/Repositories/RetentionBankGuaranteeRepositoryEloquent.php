<?php
namespace App\Repositories;

use App\helpers\BsHelper;
use App\Models\AdvanceAndPayment;
use App\Models\RetentionBankGuarantee;
use App\Models\RetentionRecord;
use http\Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;

class RetentionBankGuaranteeRepositoryEloquent implements RetentionBankGuaranteeRepository
{
    public function create($attributes) {

        $retention_bank_guarantee = new RetentionBankGuarantee();
        $retention_bank_guarantee->office_id = Auth::user()->office->id;
        $retention_bank_guarantee->fiscal_year_id = $attributes['fiscal_year'];
        $retention_bank_guarantee->retention_record_purpose_id = $attributes['retention_purpose'];
        $retention_bank_guarantee->retention_depositor_type_id = $attributes['party_type'];
        $retention_bank_guarantee->retention_depositor_id = $attributes['party_name'];
        $retention_bank_guarantee->bank_id = $attributes['bank'];
        $retention_bank_guarantee->reference_number = $attributes['reference_number'];
        $retention_bank_guarantee->retention_bank_guarantee_number = $attributes['retention_bank_guarantee_number'];
        $retention_bank_guarantee->amount = $attributes['amount'];

        $date_array = explode('-', $attributes['guarantee_start_date']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $retention_bank_guarantee->guarantee_start_date = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];


        $date_array = explode('-', $attributes['guarantee_end_date']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $retention_bank_guarantee->guarantee_end_date = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];

        $date_array = explode('-', $attributes['date']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $retention_bank_guarantee->date = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];

        $created_date = $attributes['date'];
        $myyearfirst=(substr($created_date, 0,2));
        $myyearlast=(substr($created_date, 2,2));
        $mymonth=(explode('-',$created_date));
        $month=intval($mymonth[1]);
        if($month==1){$finalmonth=10;}
        if($month==2){$finalmonth=11;}
        if($month==3){$finalmonth=12;}
        if($month==4){$finalmonth=1;}
        if($month==5){$finalmonth=2;}
        if($month==6){$finalmonth=3;}
        if($month==7){$finalmonth=4;}
        if($month==8){$finalmonth=5;}
        if($month==9){$finalmonth=6;}
        if($month==10){$finalmonth=7;}
        if($month==11){$finalmonth=8;}
        if($month==12){$finalmonth=9;}

        $retention_bank_guarantee->month = $finalmonth;
        $retention_bank_guarantee->save();
        return $retention_bank_guarantee;


    }


        public function get_by_id($id){
        $retentionBankGuarantee = retentionBankGuarantee::findOrFail($id);
        return $retentionBankGuarantee;
        }


        public function update($attributes ,$id) {
            $retentionBankGuarantee = $this->get_by_id($id);
            $retentionBankGuarantee->retention_record_purpose_id = $attributes['retention_purpose'];
            $retentionBankGuarantee->retention_depositor_type_id = $attributes['party_type'];
            $retentionBankGuarantee->retention_depositor_id = $attributes['party_name'];
            $retentionBankGuarantee->bank_id = $attributes['bank'];
            $retentionBankGuarantee->reference_number = $attributes['reference_number'];
            $retentionBankGuarantee->retention_bank_guarantee_number = $attributes['retention_bank_guarantee_number'];
            $retentionBankGuarantee->amount = $attributes['amount'];
            $retentionBankGuarantee->save();
            return $retentionBankGuarantee;

    }

}