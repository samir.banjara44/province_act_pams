<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">
<style>
    table {
        border-collapse: collapse;
    }
    .accept-table {
    }
    .pull-right{
        float: right;
    }
    .fixPrintBtnRightTop{
        position: fixed;
        top: 0;
        right: 0;
    }
    table {
        margin: 0 auto;
    }​
    tr.body td {
        border-collapse:separate;
        border-spacing:5em;
    }

    .spacer {
        width: 100%;
        height: 31px;
    }
    .body {
        padding-left: 31px;
        text-align: justify;
    }

    @media print {
        .noprint {
            display: none;
        }
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <button class="fixPrintBtnRightTop pull-right noprint" onclick="window.print()">Print</button>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <br>
        <table width="99%" style="font-size:13px" class="">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 41px;height: 80px;width:100px;
" alt="">
                <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश सभा @endif</td>
            </tr>

            <tr>
                <td colspan="6" style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    @if(Auth::user()->office->department)
                        {{Auth::user()->office->department->name}}
                    @endif
                </td>

            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}, {{Auth::user()->office->district->name}}</td>
            </tr>

            <tr>
                <td style="padding-top: 59px; padding-left: 27px;">आर्थिक वर्ष : <span class="e-n-t-n-n">{{$chalani->fiscal_year}}</span></td>
            </tr>
            <tr>
                <td style="padding-left: 27px;">चलान न. : <span class="e-n-t-n-n"> {{$chalani->chalani_number}}</span></td>
            </tr> <tr>
                <td style="padding-left: 27px;">पत्र संख्या. : <span class="e-n-t-n-n"> {{$chalani->letter_number}}</span></td>
            </tr>
            <tr>
                <td class="kalimati" style="padding-left: 27px;">मिति : {{$chalani->date}}</td>
            </tr>
            <tr>

            </tr>
        </table>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table width="100%" class="table" id="voucher_table"
                       style="font-size: 12px">
                    <thead>
                    </thead>
                    <tbody>
                        <tr>
                            <td align="center" style="font-size: 20px; padding-top: 59px;"> विषयः {{$chalani->purpose}}</td>
                        </tr>
                        <tr>
                            <td colspan="">
                                    <div class="spacer"></div>
                            </td>
                        </tr>
                    <tr>
                        <td align="center" style="font-size: 20px"><div class="body">{!! $chalani->body !!}</div></td>
                    </tr>
                    </tbody>
                </table>
                <br>
                <br>

                <br>
{{--                <table width="99%" style="font-size: 13px" >--}}
{{--                    <tr>--}}
{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <td style="padding-left: 52px;">तयार गर्ने :--}}

{{--                        </td>--}}
{{--                        <td style="padding-left: 40px;width: 358px;">पेश गर्ने :</td>--}}
{{--                        <td>सदर गर्ने :--}}

{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <td style="padding-left: 52px;">पद :--}}

{{--                        </td>--}}
{{--                        <td style="padding-left: 40px;width: 358px;">पद :</td>--}}
{{--                        <td>पद :--}}

{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <td style="padding-left: 52px;">मिति : </td>--}}
{{--                        <td style="padding-left: 40px; width: 358px;">मिति : </td>--}}
{{--                        <td>मिति : </td>--}}
{{--                    </tr>--}}

{{--                </table>--}}
            </div>
        </div>
    </section>

    <!-- /.content -->
</div>


<script>
    let changeToNepali = function (text) {
        let numbers = $.trim(text).split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (!isNaN(value) && value != ' ') {
                    if (value == 1)
                        nepaliNo += "१";

                    else if (value == 2)
                        nepaliNo += "२";

                    else if (value == 3)
                        nepaliNo += "३";

                    else if (value == 4)
                        nepaliNo += "४";

                    else if (value == 5)
                        nepaliNo += "५";

                    else if (value == 6)
                        nepaliNo += "६";

                    else if (value == 7)
                        nepaliNo += "७";

                    else if (value == 8)
                        nepaliNo += "८";

                    else if (value == 9)
                        nepaliNo += "९";

                    else if (value == 0)
                        nepaliNo += "०";
                } else {
                    nepaliNo += value
                }
            }
        });
        // console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });

</script>


