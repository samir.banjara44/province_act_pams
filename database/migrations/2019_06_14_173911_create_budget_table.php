<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expense_center');
            $table->integer('budget_sub_head');
            $table->integer('akhtiyari_type');
            $table->integer('area')->nullable();
            $table->integer('sub_area')->nullable();
            $table->integer('main_program')->nullable();
            $table->integer('general_activity')->nullable();
            $table->string('activity');
            $table->integer('expense_head');
            $table->integer('expense_head_id');
            $table->integer('target_group')->nullable();
            $table->integer('unit')->nullable();
//            $table->string('per_unit_cost');
            $table->integer('total_unit');
            $table->float('total_budget');
            $table->integer('first_quarter_unit')->nullable();
            $table->integer('second_quarter_unit')->nullable();
            $table->integer('third_quarter_unit')->nullable();
            $table->float('first_quarter_budget')->nullable();
            $table->float('second_quarter_budget')->nullable();
            $table->float('third_quarter_budget')->nullable();
            $table->float('first_chaimasik_bhar', 10, 2)->nullable();
            $table->float('second_chaimasik_bhar', 10, 2)->nullable();
            $table->float('third_chaimasik_bhar', 10, 2)->nullable();
            $table->integer('source_type');
            $table->integer('source_level');
            $table->integer('source');
            $table->integer('medium');
            $table->integer('office_id');
            $table->integer('user_id');
            $table->integer('fiscal_year');
            $table->dateTime('date');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget');
    }
}
