<?php

namespace App\Repositories;

use App\Models\ActivityReportDetail;
use App\Models\AdvanceAndPayment;
use http\Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Collection;

class ActivityReportDetailRepositoryEloquent implements ActivityReportDetailRepository
{
    public function get_pre_month_exp_by_budget_sub_head_and_office($datas)
    {
        return ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->where('vouchers.status', '=',1)
            ->where('activity_report.office_id', $datas['office'])
            ->where('activity_report.budget_sub_head_id', $datas['budget_sub_head'])
            ->where('activity_report_details.month', '<=', $datas['month'] - 1)
            ->where('activity_report_details.fiscal_year', '=', $datas['fiscal_year'])
            ->sum('activity_report_details.expense');
    }

    public function get_this_month_exp_by_budget_sub_head_and_office($datas)
    {
        return ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->where('vouchers.status', '=', 1)
            ->where('activity_report.office_id', $datas['office'])
            ->where('activity_report.budget_sub_head_id', $datas['budget_sub_head'])
            ->where('activity_report_details.month', '=', $datas['month'])
            ->where('activity_report_details.fiscal_year', '=', $datas['fiscal_year'])
            ->sum('activity_report_details.expense');
    }

    public function get_up_to_this_month_exp_by_budget_sub_head_and_office($datas)
    {
        return ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->where('vouchers.status', '=', 1)
            ->where('activity_report.office_id', $datas['office'])
            ->where('activity_report.budget_sub_head_id', $datas['budget_sub_head'])
            ->where('activity_report_details.fiscal_year', '=', $datas['fiscal_year'])
            ->where('activity_report_details.month', '<=', $datas['month'])->sum('activity_report_details.expense');
    }

    public function get_this_month_exp_by_budget_sub_head($fiscal_year,$budget_sub_head,$office,$month){

        return ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->where('vouchers.status', '=', 1)
            ->where('activity_report.office_id', $office)
            ->where('activity_report.fiscal_year', $fiscal_year)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.month', '=', $month)->sum('activity_report_details.expense');
    }

    public function getExpenseByBudgetDetailsId($budgetDetailsId){
        return $expenseTemp = ActivityReportDetail::where('activity_id',$budgetDetailsId)->sum('expense');
    }

    public function getVoucher($data){

       return ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->select('activity_report_details.expense_head', 'activity_report_details.voucher_id', 'activity_report_details.expense', 'budget_details.budget_sub_head_id')
            ->where('vouchers.status', '=', 1)
            ->where('activity_report.budget_sub_head_id', $data['program'])
            ->where('activity_report_details.fiscal_year', '=', $data['fiscal_year'])
            ->where('activity_report_details.month', '<=', $data['month'])
            ->groupBy(['activity_report_details.expense_head'])->get();

    }

    public function getAllMonthVoucher($data, $expenseHead){
      return  ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->select('activity_report_details.expense_head', 'activity_report_details.expense', 'vouchers.data_nepali', 'vouchers.jv_number', 'vouchers.short_narration', 'vouchers.id')
            ->where('vouchers.status', '=', 1)
            ->where('activity_report_details.fiscal_year', $data['fiscal_year'])
            ->where('activity_report_details.expense_head', $expenseHead->expense_head_code)
            ->where('activity_report.budget_sub_head_id', $data['program'])->get();
    }
    public function getAllMonthVoucherGroupByJvNumber($data,$expenseHead){

       return ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->select('activity_report_details.expense_head', 'activity_report_details.expense', 'vouchers.data_nepali', 'vouchers.jv_number', 'vouchers.short_narration', 'vouchers.id')
            ->where('vouchers.status', '=', 1)
            ->where('activity_report_details.fiscal_year', $data['fiscal_year'])
            ->where('activity_report_details.expense_head', $expenseHead->expense_head_code)
            ->where('activity_report.budget_sub_head_id', $data['program'])->groupBy(['vouchers.jv_number'])->get();
    }

    public function getThisMonthVoucherGroupByJvNumber($data,$expenseHead){

       return ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
           ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
           ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
           ->select('activity_report_details.expense_head', 'activity_report_details.expense', 'vouchers.data_nepali', 'vouchers.jv_number', 'vouchers.short_narration', 'vouchers.id')
           ->where('vouchers.status', '=', 1)
           ->where('activity_report_details.expense_head', $expenseHead->expense_head_code)
           ->where('activity_report_details.fiscal_year', $data['fiscal_year'])
           ->where('activity_report_details.month', $data['month'])
           ->where('activity_report.budget_sub_head_id', $data['program'])->groupBy(['vouchers.jv_number'])->get();
    }

    public function getThisMonthVoucher($data, $expenseHead){
       return ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->select('activity_report_details.expense_head', 'activity_report_details.expense', 'vouchers.data_nepali', 'vouchers.jv_number', 'vouchers.short_narration', 'vouchers.id')
            ->where('vouchers.status', '=', 1)
            ->where('activity_report_details.expense_head', $expenseHead->expense_head_code)
           ->where('activity_report_details.fiscal_year', $data['fiscal_year'])
           ->where('activity_report_details.month', $data['month'])
            ->where('activity_report.budget_sub_head_id', $data['program'])->get();
    }

    public function getVoucherActivity($data){
        return ActivityReportDetail::where('activity_id', $data['khata'])
            ->select('expense', DB::raw('sum(expense) expense'), 'voucher_id', 'expense_head', 'activity_id')
            ->groupBy('voucher_id')
            ->where('fiscal_year',$data['fiscal_year'])
            ->whereNotNull('voucher_id')->get();
    }


}
