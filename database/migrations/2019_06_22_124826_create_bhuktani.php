<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBhuktani extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bhuktani', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('adesh_number');
            $table->integer('budget_sub_head');
            $table->integer('office_id');
            $table->float('amount');
            $table->string('date_nepali');
            $table->dateTime('date_english');
            $table->integer('status');
            $table->string('fiscal_year');
            $table->integer('month');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bhuktani');

    }
}
