<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Repositories\BankRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class BankController extends Controller
{

    private $bankRepo;

    public function __construct(BankRepository $bankRepo)
    {
        $this->middleware('auth');
        $this->bankRepo = $bankRepo;
    }

    public function index()
    {
        $banks = $this->bankRepo->get_by_office_id();

        return view('frontend.bank.index', compact('banks'));
    }

    public function create()
    {

        return view('frontend.bank.create');
    }

    public function store(Request $request)
    {

        try {
            $bank = $this->bankRepo->create($request->all());
            $requestName = $request->name;
            $message = "Created " . $requestName . " Successfully";
            return redirect(route('bank'))->with('success', $message);
        } catch (\Exception $e) {
            $message = "Can not add to table. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }
    }

    public function edit($id)
    {
        $bank = $this->bankRepo->get_by_id($id);
        return view('frontend.bank.edit', compact('bank'));
    }

    public function update(Request $request, $id)
    {
        try {
            $bank = $this->bankRepo->update($request->all(), $id);
            $requestName = $request->name;
            $message = "Created " . $requestName . " Successfully";
            return redirect(route('bank'))->with('success', $message);
        } catch (\Exception $e) {
            $message = "Can not update. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }
    }

    public function delete($bank_id)
    {

        $update = Bank::findorfail($bank_id)->update(['status' => 0]);
        return json_encode($update);
    }

    public function dharautiIndex()
    {


        return view('frontend.deposit_bank.index');
    }

    public function dharautCreate()
    {
        return view('frontend.deposit_bank.create');
    }

    public function getBankByByahora(){
        $banks = Bank::where('office_id',Auth::user()->office_id)->get();
        return json_encode($banks);
 
     }
}
