<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\LocalLevel;
use App\Repositories\LocalLevelRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class LocalLevelController extends Controller
{

//    private $localLeveltRepo;

//    public function __construct(LocalLevelRepository $localLeveltRepo)
//    {
//        $this->middleware('auth');
//        $this->localLeveltRepo = $localLeveltRepo;
//    }

    public function get_local_level_by_district($district_id)
    {
        $localLevels= LocalLevel::where('district_id',$district_id)->get();
        return json_encode($localLevels);

    }

}
