@extends('layouts.app')

@section('content')
<div class="login_page_bg">
    <div class="login_page">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-6">
                    <div class="display-flex">
                        <div class="panel panel-default">
                            <div class="login-panel">
                                <h2 class="login_title">प्रदेश लेखा प्रणाली २०७६</h2>
                                <p class="login_msg">पासवर्ड रिसेट गर्नको लागि कृपया इमेल आई.डि. राख्नुहोस् ।</p>

                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                    {{ csrf_field() }}

                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                            <div class="col-md-8">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                पासवर्ड रिसेट अनुरोधको लागि इमेल पठाउने
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop