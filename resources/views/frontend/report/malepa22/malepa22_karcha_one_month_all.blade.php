<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">

<style>
  table{
    border-collapse: collapse;
  }

  tbody tr:hover{
    background-color: #e1e1e1 !important;
  }

  /*th{*/
  /*  font-weight: 200;*/
  /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <table class="kalimati" width="99%" style="border-collapse: collapse; font-size:13px">
        <tr>
          <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
          <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
            सभा @endif</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
        </tr>
        <tr>
          <td colspan="6"
              style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">
            @if(Auth::user()->office->department)
              {{Auth::user()->office->department->name}}
            @endif
          </td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}, , {{Auth::user()->office->district->name}}</td>
        </tr>

        <tr>
          <td colspan="6" style="padding-top: 17px;">
            <div style="width: 100%; text-align: center">
              <b>{{$expense_head_sirsak}}</b><br>
{{--              <b>{{$year}} साल {{$month_name}} महिना</b>--}}
            </div>
            <div style="float: right; margin-top: -20px">
              म.ले.प.फा.नं. २२
            </div>
          </td>
        </tr>
{{--        <tr>--}}
{{--          <td>--}}
{{--            <br>--}}
{{--            <br>--}}
{{--          </td>--}}
{{--        </tr>--}}
        <tr>
          <td> खाता : - <span>{{$expense_head_sirsak}}</span> - <span class="kalimati">{{$expense_head_code}}</span></td>
{{--          <td align="right"><span class="kalimati">{{$year}}</span> - <span>{{$month->name}}</span> महिना</td>--}}
        </tr>

      </table>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table id="expense_head_expense_table" class="kalimati" width="100%" border="1" style="background-color:#dbdbdb; font-size: 12px">
            <thead>
            <tr>
              <th>सि.न.</th>
              <th>भौचर न.</th>
              <th>मिति</th>
              <th>विवरण</th>
              <th>हिसाब न.</th>
              <th>डेबिट</th>
{{--              <th>क्रेडिट</th>--}}
{{--              <th>बाकीं</th>--}}
            </tr>
            </thead>
            <tbody>
              @foreach($vouchers as $index=> $voucherDetail)
                <tr style="background-color: white;">
                  <td class="kalimati" style="text-align: center">{{++$index}}</td>
                  <td class="kalimati" style="text-align: center"><a href="{{route('voucher.view',$voucherDetail['id'])}}" target="_blank" class="kalimati">{{$voucherDetail['jv_number']}}</a> </td>
                  <td align="center">{{$voucherDetail['data_nepali']}}</td>
                  <td align="left">{{$voucherDetail['short_narration']}}</td>

                  <td class="kalimati" style="text-align: center">{{$voucherDetail['expense_head']}}</td>
                    <td  class="dr-amount kalimati" style="text-align: right">{{number_format($voucherDetail->getThisVoucherAExpense($expense_head_code),2)}}</td>
{{--                  <td class="cr-amount" style="text-align: center">{{$voucherDetail->cr_amount}}</td>--}}
{{--                  <td class="remain"  style="text-align: center"></td>--}}
                </tr>
              @endforeach
            <tr>
              <td colspan="5" style="text-align: right">जम्मा</td>
              <td class="kalimati" style="text-align: right">{{number_format($totalExpense,2)}}</td>
{{--              <td style="text-align: center">{{$cr_toptal}}</td>--}}
{{--              <td style="text-align: center">{{$total_remain}}</td>--}}
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>


{{--Dr and Cr amount को हिसाब किताब--}}
<script>
  $(document).ready(function () {

    let drAmount = 0;
    let trs = $('#expense_head_expense_table tbody').find('tr').not(':last');
    let drEng = 0;
    let crEng = 0;
    let remain = 0;
      $.each(trs, function () {

        let drAmount = ($.trim($(this).find('td.dr-amount').text()));
        let crAmount = ($.trim($(this).find('td.cr-amount').text()));

        if(drAmount){

          remain = parseFloat(drEng) + parseFloat(drAmount);
          drEng +=  parseFloat(drAmount);
          $(this).find('td.remain').text(remain)
        }
        if(crAmount){

          remain = parseFloat(remain) - parseFloat(crAmount);
          crEng +=  parseFloat(crAmount);
          $(this).find('td.remain').text(remain)

        }

      })
    // alert(drEng);
  })
</script>

{{--nep to english--}}
<script>
  function convertNepaliToEnglish(input) {
    console.log(input);
    var charArray = input.split('.');
    charArray = charArray[0].split('');
    // console.log('Test',charArray[0]);
    var engDate = '';
    $.each(charArray, function (key,value) {

      switch (value) {
        case '१':
          engDate += '1'
          break
        case '२':
          engDate += '2'
          break
        case '३':
          engDate += '3'
          break
        case '४':
          engDate += '4'
          break
        case '५':
          engDate += '5'
          break
        case '६':
          engDate += '6'
          break
        case '०':
          engDate += '0'
          break
        case '७':
          engDate += '7'
          break
        case '८':
          engDate += '8'
          break
        case '९':
          engDate += '9'
          break

        case '-':
          engDate += '-'
          break
      }
      //console.log(engDate)
    })

    return engDate

  }
</script>


{{--eng to nep--}}
<script>
  let changeToNepali = function (text) {
    let numbers = $.trim(text).split('');
    let nepaliNo = '';
    $.each(numbers, function (key, value) {
      if (value) {
        if(!isNaN(value) && value != ' ') {
          if (value == 1)
            nepaliNo += "१";

          else if (value == 2)
            nepaliNo += "२";

          else if (value == 3)
            nepaliNo += "३";

          else if (value == 4)
            nepaliNo += "४";

          else if (value == 5)
            nepaliNo += "५";

          else if (value == 6)
            nepaliNo += "६";

          else if (value == 7)
            nepaliNo += "७";

          else if (value == 8)
            nepaliNo += "८";

          else if (value == 9)
            nepaliNo += "९";

          else if (value == 0)
            nepaliNo += "०";
        }else{
          nepaliNo += value
        }
      }
    });
    // console.log(nepaliNo);
    return nepaliNo;
  };


  $('.e-n-t-n-n').each(function () {
    let nepaliNo = changeToNepali($(this).text());
    let nepaliVal = changeToNepali($(this).val());

    $(this).text(nepaliNo);
    $(this).val(nepaliVal);
  });

</script>

