<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">

<style>
    table {
        border-collapse: collapse;
    }

    table tbody tr:hover {
        background-color: #e1e1e1 !important;
    }

    table tbody tr {
        background-color: white;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<style type="text/css" media="print"> @page {
        size: landscape;
    } </style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="border-collapse: collapse; font-size:13px">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
                    सभा @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6"
                    style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    @if(Auth::user()->office->department)
                        {{Auth::user()->office->department->name}}
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->name}},
                    , {{Auth::user()->office->district->name}}</td>
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>बैङ्क नगदी किताब</b><br>
                        <b><span class="e-n-t-n-n">{{$year}}</span> साल {{$month_name}} महिना</b>
                    </div>
                    <div style="float: right; margin-top: -20px; padding-right: 10px">
                        म.ले.प.फा.नं. ६०३
                    </div>
                </td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td style="padding-left: 10px">आर्थिक वर्ष : <span class="e-n-t-n-n">{{$fiscalYear->year}}</span>
                </td>
            </tr>
        </table>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">

                <table class="table" id="malepa-five-table" width="100%" border="1"
                       style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th rowspan="2" style="width: 8%;">मिति</th>
                        <th rowspan="2" style="width: 6%;">भौ.न.</th>
                        <th rowspan="2" style="width: 24%;">विवरण</th>
                        <th colspan="2" style="width: 5%;">नगद मौज्दात</th>
                        <th colspan="3">बैङ्क मौज्दात</th>
                        <th colspan="2">विविध</th>
                        <th colspan="2">कैफियत</th>
                    </tr>
                    <tr>
                        <td style="text-align: center">डे</td>
                        <td style="text-align: center">क्रे</td>
                        <td style="text-align: center">डे</td>
                        <td style="text-align: center">क्रे</td>
                        <td style="text-align: center">बकी</td>

                        <td style="text-align: center">डे</td>
                        <td style="text-align: center">क्रे</td>
                        <td colspan="2"></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($retentionVouchers as $index=>$voucher)
                        <tr>
                            <td class="kalimati" align="center">{{$voucher->date_nep}}</td>
                            <td class="kalimati" align="center">{{$voucher->voucher_number}}</td>
                            <td class="kalimati" align="left">{{$voucher->short_narration}}</td>
                            <td class="kalimati" align="center"></td>
                            <td class="kalimati" align="center"></td>
                            <td class="kalimati" align="right">{{number_format($voucher->getReceivableRetention(),2)}}</td>
                            <td class="kalimati" align="right">{{number_format($voucher->getReturnableRetention(),2)}}</td>
                            <td></td>
                            <td class="kalimati" align="right">{{number_format($voucher->getReturnableRetention(),2)}}</td>
                            <td class="kalimati" align="right">{{number_format($voucher->getReceivableRetention(),2)}}</td>
                            <td></td>
                        </tr>
                    @endforeach
                    <tr style="background-color:#dbdbdb; font-size: 12px">
                        <td align="right" colspan="3"><b>यो महिनाको जम्मा</b></td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" align="right">{{number_format($thisMonthTotal['receiveRetention'],2)}}</td>
                        <td class="kalimati" align="right">{{number_format($thisMonthTotal['returnRetention'],2)}}</td>
                        <td></td>
                        <td class="kalimati" align="right">{{number_format($thisMonthTotal['returnRetention'],2)}}</td>
                        <td class="kalimati" align="right">{{number_format($thisMonthTotal['receiveRetention'],2)}}</td>
                        <td></td>
                    </tr>
                    <tr style="background-color:#dbdbdb; font-size: 12px">
                        <td align="right" colspan="3"><b>अघिल्लो महिनाको जम्मा</b></td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" align="right">{{number_format($preMonthTotal['receiveRetention'],2)}}</td>
                        <td class="kalimati" align="right">{{number_format($preMonthTotal['returnRetention'],2)}}</td>
                        <td></td>
                        <td class="kalimati" align="right"></td>
                        <td class="kalimati" align="right"></td>
                        <td></td>
                    </tr>
                    <tr style="background-color:#dbdbdb; font-size: 12px">
                        <td align="right" colspan="3"><b>कुल जम्मा</b></td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" align="right">{{number_format($upToMonthTotal['receiveRetention'],2)}}</td>
                        <td class="kalimati" align="right">{{number_format($upToMonthTotal['returnRetention'],2)}}</td>
                        <td></td>
                        <td class="kalimati" align="right">{{number_format($upToMonthTotal['returnRetention'],2)}}</td>
                        <td class="kalimati" align="right">{{number_format($upToMonthTotal['receiveRetention'],2)}}</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <table width="99%" style="font-size: 13px">
                    <tr>
                        <br>
                        <br>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">तयार गर्ने :
                            {{--                            @if($voucher_signature and $voucher_signature->karmachari_prepare_by)--}}
                            {{--                                {{$voucher_signature->karmachari_prepare_by->name_nepali}}--}}
                            {{--                            @endif--}}
                        </td>
                        <td>पेश गर्ने :
                            {{--                            @if($voucher_signature and $voucher_signature->karmachari_submit_by)--}}
                            {{--                                {{$voucher_signature->karmachari_submit_by->name_nepali}}--}}
                            {{--                            @endif--}}
                        </td>
                        <td>सदर गर्ने :
                        {{--                            @if($voucher_signature and $voucher_signature->karmachari_approved_by)--}}
                        {{--                                {{$voucher_signature->karmachari_approved_by->name_nepali}}</td>--}}
                        {{--                        @endif--}}
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">पद :
                            {{--                            @if($voucher_signature and $voucher_signature->karmachari_prepare_by->get_pad)--}}
                            {{--                                {{$voucher_signature->karmachari_prepare_by->get_pad->name}}--}}
                            {{--                            @endif--}}
                        </td>
                        <td>पद :
                            {{--                            @if($voucher_signature and $voucher_signature->karmachari_submit_by->get_pad)--}}
                            {{--                                {{$voucher_signature->karmachari_submit_by->get_pad->name}}--}}
                            {{--                            @endif--}}
                        </td>
                        <td>पद :
                            {{--                            @if($voucher_signature and $voucher_signature->karmachari_approved_by->get_pad)--}}
                            {{--                                {{$voucher_signature->karmachari_approved_by->get_pad->name}}--}}
                            {{--                            @endif--}}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">मिति :</td>
                        <td>मिति :</td>
                        <td>मिति :</td>
                    </tr>

                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '/')

                    nepaliNo += "/";
            }
        });
        // console.log(nepaliNo);
        return nepaliNo;
    };

    let change_all_to_nepali_number = function () {
        $('.e-n-t-n-n').each(function () {
            let nepaliNo = changeToNepali($(this).text());
            let nepaliVal = changeToNepali($(this).val());

            $(this).text(nepaliNo);
            $(this).val(nepaliVal);
        });
    }
</script>

<script>
    $(document).ready(function () {
        change_all_to_nepali_number();
    })
</script>


<script>
    $(document).ready(function () {
        let current_payment = $('#bhuktani_amount').val();
        let trs = $('#malepa-five-table tbody').find('tr');
        let crAmount = 0;
        let dr_liability = 0;
        let cr_liability = 0;
        $.each(trs, function (key, tr) {

            let bank_remain = $.trim($(this).find('td.bank-cr-amount').text());

        });
        // alert(cr_liability);
        // $('#cr_bank').text(bank_remain);
        // $('#cr_bank_two').text(crAmount);
        // $('td#liability-dr span').text(dr_liability);
        // $('td#liability-cr span').text(cr_liability);
        // change_all_to_nepali_number();


    })
</script>


<script>
    function convertNepaliToEnglish(input) {
        console.log(input);
        var charArray = input.split('.');
        charArray = charArray[0].split('');
        // console.log('Test',charArray[0]);
        var engDate = '';
        $.each(charArray, function (key, value) {

            switch (value) {
                case '१':
                    engDate += '1';
                    break;
                case '२':
                    engDate += '2';
                    break;
                case '३':
                    engDate += '3';
                    break;
                case '४':
                    engDate += '4';
                    break;
                case '५':
                    engDate += '5';
                    break;
                case '६':
                    engDate += '6';
                    break;
                case '०':
                    engDate += '0';
                    break;
                case '७':
                    engDate += '7';
                    break;
                case '८':
                    engDate += '8';
                    break;
                case '९':
                    engDate += '9';
                    break;

                case '-':
                    engDate += '-';
                    break
            }
            //console.log(engDate)
        });

        return engDate

    }
</script>

