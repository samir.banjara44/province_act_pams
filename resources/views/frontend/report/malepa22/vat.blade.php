<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<style>
  table{
    border-collapse: collapse;
  }

  /*th{*/
  /*  font-weight: 200;*/
  /*}*/
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <table width="99%" >
        <tr>
          <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
          <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
            सभा @endif</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
        </tr>
        <tr>
          <td colspan="6"
              style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">
            @if(Auth::user()->office->department)
              {{Auth::user()->office->department->name}}
            @endif
          </td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}, , {{Auth::user()->office->district->name}}</td>
        </tr>
        <tr>
          <td colspan="4" style="padding-left:670px;">
            <b>खाता</b>
          </td>

          <td style="text-align: right;">म.ले.प.फा.नं. २२</td>
        </tr>
        <tr>
          <td>
            <br>
            <br>

          </td>
        </tr>
        <tr style="float: left">
          <td>आर्थीक वर्ष : <span class="e-n-t-n-n">{{$data['fiscal_year']}}</span></td>
          <td >खाता :{{$party_name}}</td>

        </tr>

      </table>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table" width="100%" border="1" style="background-color:#dbdbdb; font-size: 12px">
            <thead>
            <tr><th width="67px">सि.नं.</th>
              <th>भौचर नम्बर </th>
              <th>मिति </th>
              <th>भुक्तानी पाउने </th>
              <th>भ्याट/प्यान </th>
              <th>पान  जारी गर्ने कार्यालय र ठेगाना </th>
              <th>बिल नं. </th>
              <th>भुक्तानी रकम </th>
              <th>अग्रिम कर कट्टी रकम </th>
              <th>भ्याट रकम </th>
              <th>मुअकर कट्टी </th>
              <th>कैफियत </th>
            </tr>
            </thead>
            <tbody>
            @foreach($preBhuktaniList as $index=>$preBhuktani)
              <tr style="background-color: white;">
                <td style="text-align: center"><span class="e-n-t-n-n">{{++$index}}</span></td>
                <td style="text-align: center"><a href='{{route('voucher.view',$preBhuktani->voucher->id)}}' target="_blank"><span class="e-n-t-n-n">{{$preBhuktani->voucher->jv_number}}</span></a></td>
                <td style="text-align: center">{{$preBhuktani->voucher->data_nepali}}</td>
                <td style="text-align: center">
                  @if($preBhuktani->party)
                    {{$preBhuktani->party->name_nep}}
                  @endif
                </td>
                <td style="text-align: center">
                  @if($preBhuktani->party and $preBhuktani->party->vat_pan_number)
                    <span class="e-n-t-n-n">{{$preBhuktani->party->vat_pan_number}}</span>
                  @endif
                </td>
                <td style="text-align: center">
                  @if($preBhuktani->party and $preBhuktani->party->vatOffice)
                    {{$preBhuktani->party->vatOffice->name}}
                  @endif
                </td>
                <td style="text-align: center">
                  @if($preBhuktani->vat_bill_number)
                    <span class="e-n-t-n-n">{{$preBhuktani->vat_bill_number}}</span>
                  @endif
                </td>
                <td style="text-align: center">
                  @if($preBhuktani->amount)
                    <span class="e-n-t-n-n">{{$preBhuktani->amount}}</span>
                  @endif
                </td>
                <td style="text-align: center">
                  @if($preBhuktani->advance_vat_deduction)
                    <span class="e-n-t-n-n">{{$preBhuktani->advance_vat_deduction}}</span>
                  @endif
                </td>
                <td style="text-align: center">
                  @if($preBhuktani->vat_amount)
                    <span class="e-n-t-n-n">{{$preBhuktani->vat_amount}}</span>
                  @endif
                </td>
                <td style="text-align: center">

                </td>

{{--                <td style="text-align: center"><span class="e-n-t-n-n">{{$preBhuktani->amount}}</span></td>--}}
{{--                <td style="text-align: center"><span class="e-n-t-n-n">{{$preBhuktani->advance_vat_deduction}}</span></td>--}}
{{--                <td style="text-align: center"><span class="e-n-t-n-n">{{$preBhuktani->vat_amount}}</span></td>--}}
                <td style="text-align: center"></td>
              </tr>
            @endforeach
            <tr>
              <td colspan="7"  style="text-align: right">जम्मा</td>
              <td style="text-align: center"><span class="e-n-t-n-n">{{$total_bhuktani_amount}}</span></td>
              <td style="text-align: center"><span class="e-n-t-n-n">{{$total_advance_vat_deduction}}</span></td>
              <td style="text-align: center"><span class="e-n-t-n-n">{{$total_advance_vat_deduction}}</span></td>
              <td></td>
              <td></td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
<script>
  let changeToNepali = function (text) {
    let numbers = text.split('');
    let nepaliNo = '';
    $.each(numbers, function (key, value) {
      if (value) {
        if(value == 1)
          nepaliNo+="१";
        else if(value == 2)

          nepaliNo+="२";
        else if(value == 3)

          nepaliNo+="३";
        else if(value == 4)

          nepaliNo+="४";
        else if(value == 5)

          nepaliNo+="५";
        else if(value == 6)

          nepaliNo+="६";
        else if(value == 7)

          nepaliNo+="७";
        else if(value == 8)

          nepaliNo+="८";
        else if(value == 9)

          nepaliNo+="९";
        else if(value == 0)

          nepaliNo+="०";
        else if(value == ',')

          nepaliNo+=",";
        else if(value == '/')

          nepaliNo+="/";
        else if(value == '.')

          nepaliNo+=".";
      }
    });
    console.log(nepaliNo);
    return nepaliNo;
  };

  $('.e-n-t-n-n').each(function () {
    let nepaliNo = changeToNepali($(this).text());
    $(this).text(nepaliNo);
  });


</script>