<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ifSuperadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $role = $user->roles ? $user->roles->first()->name : '';
        $url = $request->url();

        if(strpos($url,'setting/')){
            if($role == 'superadmin'){
                return $next($request);
            }
        }elseif($_SERVER['REQUEST_URI'] == '/'){
            if($role == 'superadmin'){
                return redirect(route('admin.user'));
            }
        }
//        elseif($_SERVER['REQUEST_URI'] == '/admin-panel'){
//            if($role !== 'superadmin'){
//                return redirect(route('home'));
//            }
//        }


        return $next($request);
    }
}
