@extends('layouts.app')

@section('content')
<div class="login_page_bg">
    <div class="login_page">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-6">
                    <div class="display-flex">
                        <div class="panel panel-default">
                            <div class="login-panel">
                                <h2 class="login_title">प्रदेश लेखा प्रणाली २०७६</h2>
                                <p class="login_msg">कृपया युजर नेम  र पासवर्ड राखि लगइन गर्नुहोस् ।</p>
                                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="email" class="col-sm-3 control-label">E-Mail Address </label>
                                            <div class="col-sm-9">
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"></div>
                                                    </div>
                                                    <input id="" type="text" class="form-control" name="username" value="{{ old('email') }}" required autofocus>
                                                </div>

{{--                                                @if ($errors->has('email'))--}}
{{--                                                    <span class="help-block">--}}
{{--                                                        <strong>{{ $errors->first('email') }}</strong>--}}
{{--                                                    </span>--}}
{{--                                                @endif--}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="col-md-3 control-label">Password : </label>

                                            <div class="col-md-9">
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"></div>
                                                    </div>
                                                    <input id="password" type="password" class="form-control" name="password" required>
                                                </div>

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

{{--                                    <div class="form-group">--}}
{{--                                        <div class="col-md-6 col-md-offset-4">--}}
{{--                                            <div class="checkbox">--}}
{{--                                                <label>--}}
{{--                                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me--}}
{{--                                                </label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <center>
                                                <button type="submit" class="btn btn-primary">
                                                    Login
                                                </button>

{{--                                                <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                                    Forgot Your Password?--}}
{{--                                                </a>--}}
                                            </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
