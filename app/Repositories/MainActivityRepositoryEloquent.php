<?php
namespace App\Repositories;

use App\Models\MainActivity;
use Illuminate\Support\Facades\Auth;

class MainActivityRepositoryEloquent implements MainActivityRepository
{

    public function create($attributes) {

        $mainActivity = new MainActivity();
        $mainActivity->name = $attributes['general_activity'];
        $mainActivity->area_id = $attributes['area_id'];
        $mainActivity->sub_area_id = $attributes['sub_area_id'];
        $mainActivity->main_program_id = $attributes['main_program_id'];
        $mainActivity->office_id = Auth::user()->office->id;
        $mainActivity->user_id = Auth::user()->id;
        $mainActivity->status = 1;
        $mainActivity->save();
        return $mainActivity;
    }

    public function get_by_main_program_and_office($main_program_id,$office_id){
        $mainActivities = MainActivity::where('main_program_id', $main_program_id)
            ->where('office_id',$office_id)
            ->get();
            return $mainActivities;
    }
}