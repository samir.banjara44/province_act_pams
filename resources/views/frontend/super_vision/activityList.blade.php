<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">


<style>
    table {
        border-collapse: collapse;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="font-size:13px">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                {{--          <td colspan="6" style="text-align: center"><b>प्रदेश सरकार</b></td>--}}
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
            </tr>
            @if(Auth::user()->office->department)
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->department->name}}</td>
                </tr>
            @endif
            <tr>
                {{--          <td colspan="6" style="text-align: center"><b>{{$office[0]->name}}</b></td>--}}
            </tr>
            <tr>
                <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>कार्यक्रम / परियोजना अनुसार बजेट बिनियोजन</b><br>
                    </div>
                    <div style="float: right; margin-top: -20px">

                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                    <br>

                </td>
            </tr>
            <tr>
                <td class="kalimati">कार्यालयः
                    <span>{{$office[0]->name}}, {{$office[0]->district->name}}</span>
                    <br>
                    बजेट उपशीर्षक न. :
                    <b><span class="kalimati">{{$program->program_code}}</span></b><br>
                    बजेट उपशीर्षक नाम: <b>{{$program->name}}</b></td>
                <td style="text-align: right">आर्थीक वर्ष : <span class="kalimati">{{$fiscal_year->year}}</span></td>

            </tr>

        </table>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table width="99%" border="1" style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th rowspan="3" width="67px">सि.नं.</th>
                        <th rowspan="3" style="width: 28%;">कार्यक्रम/आयोजनाको नाम</th>
                        <th rowspan="3">खर्च शीर्षक</th>
                        <th rowspan="3" width="237px">स्रोत</th>
                        <th rowspan="3">लक्ष</th>
                        <th colspan="7">विनियोजन रु.</th>
                        <th colspan="4">खर्च</th>
                        <th rowspan="3">कैफियत</th>
                    </tr>

                    <tr>
                        <th colspan="2">प्रथम चौमासिक</th>
                        <th colspan="2">दोश्रो चौमासिक</th>
                        <th colspan="2">तेस्रो चौमासिक</th>
                        <th rowspan="2">जम्मा</th>
                        <th rowspan="2">प्रथम चौमासिक</th>
                        <th rowspan="2">दोश्रो चौमासिक</th>
                        <th rowspan="2">तेस्रो चौमासिक</th>
                        <th rowspan="3">जम्मा</th>

                    </tr>
                    <tr>
                        <td style="text-align: center">बजेट</td>
                        <td style="text-align: center">भार</td>
                        <td style="text-align: center">बजेट</td>
                        <td style="text-align: center">भार</td>
                        <td style="text-align: center">बजेट</td>
                        <td style="text-align: center">भार</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($activities as $index=>$activity)
                        <tr style="background-color: #ffffff">

                            <td class="kalimati" style="text-align: center">{{++$index}}</td>
                            <td>{{$activity->activity}}</td>
                            <td class="kalimati" style="text-align: center">{{$activity->expense_head}}</td>
                            <td>{{$activity->get_source_type->name}}-{{$activity->get_budget_source_level->name}}
                                -{{$activity->get_source->name}}-{{$activity->get_medium->name}}</td>
                            <td class="kalimati" style="text-align: center">{{$activity->total_unit}}</td>
                            <td class="kalimati"
                                style="text-align: center">{{number_format($activity->first_quarter_budget)}}</td>
                            <td class="kalimati" style="text-align: center">{{$activity->first_chaimasik_bhar}}</td>
                            <td class="kalimati" style="text-align: center">{{$activity->second_quarter_budget}}</td>
                            <td class="kalimati" style="text-align: center">{{$activity->second_chaimasik_bhar}}</td>
                            <td class="kalimati" style="text-align: center">{{$activity->third_quarter_budget}}</td>
                            <td class="kalimati" style="text-align: center">{{$activity->third_chaimasik_bhar}}</td>
                            <td class="kalimati" style="text-align: center">{{$activity->total_budget}}</td>
                            <td class="kalimati"
                                style="text-align: center">{{$activity->firstChaumasikExpense($data['officeId'],$data['budgetSUbHead'],$activity->id)}}</td>
                            <td class="kalimati"
                                style="text-align: center">{{$activity->secondChaumasikExpense($data['officeId'],$data['budgetSUbHead'],$activity->id)}}</td>
                            <td class="kalimati"
                                style="text-align: center">{{$activity->thirdChaumasikExpense($data['officeId'],$data['budgetSUbHead'],$activity->id)}}</td>
                            <td class="kalimati"
                                style="text-align: center">{{$activity->totalExpenseByActivity($data['officeId'],$data['budgetSUbHead'],$activity->id)}}</td>
                            <td class="kalimati" style="text-align: center">

                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="5" style="text-align: right">जम्मा</td>
                        <td style="text-align: right"><span
                                    class="kalimati">{{number_format($first_quarter_budget_total)}}</span></td>
                        <td style="text-align: center"></td>
                        <td style="text-align: right"><span
                                    class="kalimati">{{number_format($second_quarter_budget_total)}}</span></td>
                        <td style="text-align: center"></td>
                        <td style="text-align: right"><span
                                    class="kalimati">{{number_format($third_quarter_budget_total)}}</span></td>
                        <td style="text-align: center"></td>
                        <td style="text-align: right"><span class="kalimati">{{number_format($budget_sum)}}</span></td>
                        <td class="kalimati" style="text-align: right">{{$total_first_quarter_expense}}</td>
                        <td class="kalimati" style="text-align: right">{{$total_second_quarter_expense}}</td>
                        <td class="kalimati" style="text-align: right">{{$total_third_quarter_expense}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalExpenseByBudgetSubHead}}</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script>
    let changeToNepali = function (text) {
        // alert(text);
        textTrim = $.trim(text)
        let numbers = textTrim.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (!isNaN(value) && value != ' ') {
                    if (value == 1)
                        nepaliNo += "१";
                    else if (value == 2)

                        nepaliNo += "२";
                    else if (value == 3)

                        nepaliNo += "३";
                    else if (value == 4)

                        nepaliNo += "४";
                    else if (value == 5)

                        nepaliNo += "५";
                    else if (value == 6)

                        nepaliNo += "६";
                    else if (value == 7)

                        nepaliNo += "७";
                    else if (value == 8)

                        nepaliNo += "८";
                    else if (value == 9)

                        nepaliNo += "९";
                    else if (value == 0)

                        nepaliNo += "०";
                    else if (value == ',')

                        nepaliNo += ",";
                    else if (value == '.')

                        nepaliNo += ".";
                    else if (value == '/')

                        nepaliNo += "/";

                }
            }
        });
        console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });


</script>