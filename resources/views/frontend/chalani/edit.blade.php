@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-edit"></i>चलानी सम्पादन </h1>

            <a href="{{route('chalani')}}" class="btn btn-sm btn-primary btn-show-form"><i class="fa fa-arrow-left"></i>
                लिष्टमा जाने</a>
        </section>

        <!-- FORM section start -->
        <section class="formSection">
            <div class="row justify-content-md-center">
                <div class="col-md-8 form-bg col-md-offset-2">
                    <div class="form-title">चलानी सम्पादन गर्ने</div>
                    <form action="{{route('chalani.update', $chalanis->id)}}" method="post" id="chalaniForm" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="date">मिति </label>
                                <input type="text" class="form-control" id="date" placeholder="मिति " name="date"
                                       required>
                            </div>
                            <div class="col-md-6">
                                <label for="chalani_number">चलानी न. </label>
                                <input type="text" class="form-control kalimati" value="{{$chalanis->chalani_number}}"
                                       id="chalani_number" name="chalani_number" required disabled>
                            </div>
{{--                            <div class="col-md-6">--}}
{{--                                <label for="name">पत्र सङ्ख्या</label>--}}
{{--                                <input type="text" class="form-control" id="letter_number"--}}
{{--                                       value="{{$chalanis->letter_number}}" placeholder="" name="letter_number"--}}
{{--                                       >--}}
{{--                            </div>--}}
                            <div class="col-md-6">
                                <label for="chalan_date">चलान मिति </label>
                                <input type="text" class="form-control" id="chalan_date" placeholder="चलान मिति "
                                       name="chalan_date" required>
                            </div>
{{--                            <div class="col-md-6">--}}
{{--                                <label for="body">व्याहोरा </label>--}}
{{--                                <textarea class="form-control" id="body"  name="body">{{$chalanis->body}}</textarea>--}}
{{--                            </div>--}}

                            <div class="col-md-6">
                                <label for="to_office">चलानी जाने कार्यालय</label>
                                <input type="text" class="form-control" id="to_office" value="{{$chalanis->to_office}}"
                                       placeholder="चलानी जाने कार्यालय" name="to_office" required>
                            </div>
                            <div class="col-md-6">
                                <label for="type">प्रकार</label>
                                <input type="text" class="form-control" id="type" value="{{$chalanis->type}}"
                                       placeholder="प्रकार" name="type" >
                            </div>
                            <div class="col-md-6">
                                <label for="medium">पठाउने माध्यम</label>
                                <input type="text" class="form-control" id="medium" value="{{$chalanis->medium}}"
                                       placeholder="पठाउने माध्यम" name="medium" >
                            </div>

                            <div class="col-md-6">
                                <label for="address">चलानी जाने कार्यालय ठेगाना</label>
                                <input type="text" class="form-control" id="address"
                                       value="{{$chalanis->to_office_address}}" placeholder="चलानी जाने कार्यालय ठेगान"
                                       name="address" >
                            </div>
                            <div class="col-md-6">
                                <label for="purpose">प्रायोजन</label>
                                <input type="text" class="form-control" id="purpose" value="{{$chalanis->purpose}}"
                                       placeholder="प्रायोजन" name="purpose">
                            </div>
                            <div class="col-md-6">
                                <label for="branch">शाखा</label>
                                <select name="branch" required class="form-control branch" style="width:100%;" id="branch">
                                    <option value="">...........</option>
                                    @foreach($branch as $branch)
                                    <option value="{{$branch->id}}" @if($branch->id == $chalanis->branch) selected @endif>{{$branch->name_nep}}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="col-md-6">
                                <label for="image">Upload</label>
                                <input type="file" class="form-control" id="image" name="image">
                            </div>
                            <div class="image col-md-6">
                                <label>पुरानो फोटो</label>
                                <img src="{{asset('images/').'/'.$chalanis->image}}">
                            </div>


                            <div class="col-md-6">
                                <label for="remarks">कैफियत</label>
                                <input class="form-control" id="remarks" value="{{$chalanis->remarks}}"
                                       placeholder="कैफियत" name="remarks" required/>
                            </div>
                        </div>

                        <center>
                            <button type="submit" id="chalaniSubmit" class="btn btn-success btn-show-form"><i
                                        class="fas fa-check-square"></i> सम्पादन गर्ने
                            </button>
                        </center>
                    </form>
                </div>
            </div>
        </section>
        <!-- FORM section ends -->
    </div>
@endsection
@section('scripts')
    <script>
        $('#chalaniSubmit').click(function (e) {
            e.preventDefault();
            let today_nepali_date = $('#date').val();
            let nepali_date_in_roman = convertNepaliToEnglish(today_nepali_date);
            let chalan_date = $('#chalan_date').val();
            let chalan_date_in_roman = convertNepaliToEnglish(chalan_date);


            $('#date').val(nepali_date_in_roman);
            $('#chalan_date').val(chalan_date_in_roman);
            // if (validation()) {
                $('#chalaniForm').submit();
            // }
        })
    </script>
    {{--  ck editor--}}
    <script>
        CKEDITOR.replace('body');
    </script>

    {{--    Set nepali date in date filed--}}
    <script>
        $(document).ready(function () {
            let date_roman = '{{$chalanis->date}}';
            let date_nep = convertEnglishToNepali(date_roman);
            $('#date').val(date_nep);

            let chalani_date_roman = '{{$chalanis->chalan_date}}';
            let chalan_date = chalani_date_roman.replace(' ', '').replace('00:00:00', '');
            let chalani_date_nep = convertEnglishToNepali(chalan_date);
            $('#chalan_date').val(chalani_date_nep);

        })
    </script>

    {{--Today date  Date picker--}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });
        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(),
            currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear,
            currentNepaliDate.bsMonth, currentNepaliDate.bsDate);

    </script>

    {{-- chalan date, Date picker--}}
    <script>
        $("#chalan_date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });
        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);

    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            var charArray = input.split('');
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1';
                        break;
                    case '२':
                        engDate += '2';
                        break;
                    case '३':
                        engDate += '3';
                        break;
                    case '४':
                        engDate += '4';
                        break;
                    case '५':
                        engDate += '5';
                        break;
                    case '६':
                        engDate += '6';
                        break;
                    case '०':
                        engDate += '0';
                        break;
                    case '७':
                        engDate += '7';
                        break;
                    case '८':
                        engDate += '8';
                        break;
                    case '९':
                        engDate += '9';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
            });
            return engDate

        }
    </script>

    {{-- Validation   --}}
    <script>
        function validation() {
            let flag = 1;
            // let letter_number = $('#letter_number');
            // if (!letter_number.val()) {
            //     if (letter_number.siblings('p').length == 0) {
            //         letter_number.after('<p style="color:red" class="validation-error">पत्र संख्या लेख्नुहोस!</p>')
            //     }
            //     letter_number.focus();
            //     flag = 0;
            // } else {
            //     letter_number.siblings('p').remove()
            // }

            // let body = $('#body');
            // if (!body.val()) {
            //     if (body.siblings('p').length == 0) {
            //         body.after('<p style="color:red" class="validation-error">Write down the body</p>')
            //     }
            //     body.focus();
            //     flag = 0;
            // } else {
            //     body.siblings('p').remove()
            // }

            let to_office = $('#to_office');
            if (!to_office.val()) {
                if (to_office.siblings('p').length == 0) {
                    to_office.after('<p style="color:red" class="validation-error">चलानी जाने कार्यालय लेख्नुहोस!</p>')
                }
                to_office.focus();
                flag = 0;
            } else {
                to_office.siblings('p').remove()
            }

            // let type = $('#type');
            // if (!type.val()) {
            //     if (type.siblings('p').length == 0) {
            //         type.after('<p style="color:red" class="validation-error">प्रकार लेख्नुहोस!</p>')
            //     }
            //     type.focus();
            //     flag = 0;
            // } else {
            //     type.siblings('p').remove()
            // }

            // let medium = $('#medium');
            // if (!medium.val()) {
            //     if (medium.siblings('p').length == 0) {
            //         medium.after('<p style="color:red" class="validation-error">माध्यम लेख्नुहोस!</p>')
            //     }
            //     medium.focus();
            //     flag = 0;
            // } else {
            //     medium.siblings('p').remove()
            // }

            // let address = $('#address');
            // if (!address.val()) {
            //     if (address.siblings('p').length == 0) {
            //         address.after('<p style="color:red" class="validation-error">कार्यालय ठेगाना लेख्नुहोस!</p>')
            //     }
            //     address.focus();
            //     flag = 0;
            // } else {
            //     address.siblings('p').remove()
            // }

            let purpose = $('#purpose');
            if (!purpose.val()) {
                if (purpose.siblings('p').length == 0) {
                    purpose.after('<p style="color:red" class="validation-error">प्रायोजन लेख्नुहोस!</p>')
                }
                purpose.focus();
                flag = 0;
            } else {
                purpose.siblings('p').remove()
            }

            let branch = $('#branch');
            if (!branch.val()) {
                if (branch.siblings('p').length == 0) {
                    branch.after('<p style="color:red" class="validation-error">शाखा लेख्नुहोस!</p>')
                }
                branch.focus();
                flag = 0;
            } else {
                branch.siblings('p').remove()
            }
            return flag;
        }
    </script>

    {{--Convert ROman date to Nepali date --}}
    <script>
        function convertEnglishToNepali(input) {
            var charArray = input.split('');
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '1':
                        engDate += '१';
                        break;
                    case '2':
                        engDate += '२';
                        break;
                    case '3':
                        engDate += '३';
                        break;
                    case '4':
                        engDate += '४';
                        break;
                    case '5':
                        engDate += '५';
                        break;
                    case '6':
                        engDate += '६';
                        break;
                    case '7':
                        engDate += '७';
                        break;
                    case '8':
                        engDate += '८';
                        break;
                    case '9':
                        engDate += '९';
                        break;
                    case '0':
                        engDate += '०';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
            });
            return engDate;

        }
    </script>
@endsection