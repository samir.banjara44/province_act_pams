@extends('frontend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>

                <small>बैङ्क प्रविशटी</small>
            </h1>
            <ul class="breadcrumb">
                <li> <a type="button" href="{{ route('bank') }}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i>
                        सुची हेर्ने</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{ route('bank.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="col-md-2">
                            <label for="bank_id">बैङ्क</label>
                            <select id="bank_id" name="bank_id" class="form-control select2" style="width: 202px;">
                                <option value="" selected="">.........</option>
                                @foreach ($banks as $bank)
                                    <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="address">From</label>
                            <input type="text" class="form-control" id="from" name="from" required>
                        </div>
                        <div class="form-group">
                            <label for="to">To</label>
                            <input type="text" class="form-control" id="to" name="to" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" style="margin-top: 18px;">थप</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
