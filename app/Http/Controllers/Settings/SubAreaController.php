<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\SubArea;
use App\Models\Area;
use App\Repositories\SubAreaRepository;
use App\Repositories\AreaRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubAreaController extends Controller
{

    private $subAreaRepo;
    private $areaRepo;

    public function __construct(
        SubAreaRepository $subAreaRepo,
        AreaRepository $areaRepo
    )
    {
        $this->middleware('auth');
        $this->subAreaRepo = $subAreaRepo;
        $this->areaRepo = $areaRepo;
    }

    public function index()
    {
        $subAreas= SubArea::all();

        return view('backend.settings.sub_area.index', compact('subAreas'));
    }

    public function create(){
        $areas = Area::all();
        return view('backend.settings.sub_area.create',compact('areas'));
    }
    public function store(Request $request){

        $program = $this->subAreaRepo->create($request->all());
        return redirect(route('admin.sub.area'));
    }

    public function get_sub_area_by_area(Request $request, $area_id){

        $sub_areas = $this->subAreaRepo->get_all_by_area_id($area_id);
        return json_encode($sub_areas);
    }

}
