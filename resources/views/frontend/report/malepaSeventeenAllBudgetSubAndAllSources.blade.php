<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">
<link href="{{ asset('css/nepali-datepicker.css') }}" rel="stylesheet">
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
<script src="{{ asset('js/nepali-datepicker.js') }}"></script>

<style>
    table {
        border-collapse: collapse;
    }
    @media print {
        #downloadMe {
            display: none;
        }
    }
    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <input type="button" style="position: absolute; top:20px;right: 20px" value="Export to Excel" id="downloadMe">
    <section class="content-header">
        <div id="tableHeadingWrapper">
            <table id="headingTable" width="99%" style="font-size:13px">
                <tr>
                    <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                    <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
                        सभा @endif</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
                </tr>
                <tr>
                    <td colspan="6"
                        style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">
                        @if(Auth::user()->office->department)
                            {{Auth::user()->office->department->name}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}, , {{Auth::user()->office->district->name}}</td>
                </tr>
                <tr>
                    <td class="kalimati" colspan="6" style="text-align: center"><b>कार्यालय कोड: <span
                                    class="kalimati">{{Auth::user()->office->office_code}}</span></b></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div style="width: 100%; text-align: center">
                            <b>बजेट र खर्चको आर्थिक बिवरण</b><br>
                        </div>
                        <div style="float: right; margin-top: -20px; padding-right: 21px">
                            म ले प फा न. २१३
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="" style="text-align: center">आ.व. : <span class="kalimati">{{$fiscalYear->year}}</span>
                    </td>
                </tr>
                <tr>
                    <td class="" style="text-align: left; padding-left: 12px">मिति : <span id="date"></span>
                    </td>
                    <input type="hidden" id="hiddenDate" value="{{ date('Y-m-d') }}">
                </tr>

            </table>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body" id="tableWrapper">
                <table class="table" width="99%" border="1" style="background-color:#dbdbdb; font-size: 12px"
                       id="fatwari_table">
                    <thead>
                    <tr>
                        <th rowspan="2">बजेट उपशिर्षक</th>
                        <th rowspan="2">दातृ निकाय</th>
                        <th rowspan="2">खर्च संकेत</th>
                        <th rowspan="2">विवरण</th>
                        <th colspan="2">स्रोतको</th>
                        <th rowspan="2">शुरु बजेट</th>
                        <th colspan="2">संशोधन /रकमान्तर/श्रोतान्तर बाट</th>
                        <th rowspan="2">अन्तिम बजेट</th>
                        <th rowspan="2">निकासा</th>
                        <th rowspan="2">जम्मा खर्च</th>
                        <th rowspan="2">बाँकी बजेट</th>
                        <th rowspan="2">गत वर्षको खर्च</th>
                    </tr>
                    <tr>
                        <th>प्रकार</th>
                        <th style="width: 3%">भुक्तानी विधि</th>
                        <th>थप</th>
                        <th>घट</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($budgetSubHeadLists as $index=>$budgetSubHeadList)
                        <?php
                        $i = 1;
                        ?>
                        @foreach($budgetSubHeadList->getSourceType($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id) as $sourceType)
                            <?php
                            $x = 1;
                            ?>
                            @foreach($sourceType->getExpenseHead($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$sourceType->id) as $expenseHead)
                                <tr style="background-color: white">

                                    <td class="kalimati"
                                        style="width: 6%;">@if($i){{$budgetSubHeadList->name}} {{$budgetSubHeadList->program_code}}@endif</td>
                                    <td>@if($x){{$sourceType->name}}@endif</td>
                                    <td class=""><span
                                                class="e-n-t-n-n">{{$expenseHead->expense_head_by_id->expense_head_code}}</span>
                                    </td>
                                    <td class=""
                                        style="width: 10%;">{{$expenseHead->expense_head_by_id->expense_head_sirsak}}</td>
                                    <td>@if($sourceType->id == 1) संघिय सरकार @elseif($sourceType->id == 2)प्रदेश
                                        सरकार @endif</td>
                                    <td>@if($expenseHead->medium == 1)नगद @endif</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->getInitialBudget($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->getAddBudget($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->getReduceBudget($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->getFinalBudget($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->getTotalExpense($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->getTotalExpense($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->remainBudget($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td></td>
                                </tr>
                                <?php
                                $i = 0;
                                $x = 0;
                                ?>
                            @endforeach
                            <tr>
                                <td colspan="6" style="text-align: right"><b>@if($sourceType->id == 1)
                                            संघीय @elseif($sourceType->id == 2) प्रदेश </b>@endif स्रोत जम्मा
                                </td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->getTotalInitialBudget($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$sourceType->id),2)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->getTotalAddBudget($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$sourceType->id),2)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->getTotalReduceBudget($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$sourceType->id),2)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->getTotalFinalBudget($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$sourceType->id),2)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->getGranTotalExpense($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$sourceType->id),2)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->getGranTotalExpense($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$sourceType->id),2)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->totalRemainBudget($datas['office_id'],$datas['fiscal_year'],$budgetSubHeadList->id,$sourceType->id),2)}}</td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="6" style="text-align: right">बजेट उपशिर्षक जम्मा</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($budgetSubHeadList->totalInitialBudgetByBudgetSubHead($datas['fiscal_year'],$budgetSubHeadList->id),2)}}</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($budgetSubHeadList->totalAddedBudgetByBudgetSubHead($datas['fiscal_year'],$budgetSubHeadList->id),2)}}</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($budgetSubHeadList->totalReduceBudgetByBudgetSubHead($datas['fiscal_year'],$budgetSubHeadList->id),2)}}</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($budgetSubHeadList->getTotalFinalBudgetByOfficeAndBudgetSubHead($budgetSubHeadList->office_id,$datas['fiscal_year'],$budgetSubHeadList->id),2)}}</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($budgetSubHeadList->getTotalExpenseByOfficeAndBudgetSubHead($budgetSubHeadList->office_id,$datas['fiscal_year'],$budgetSubHeadList->id),2)}}</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($budgetSubHeadList->getTotalExpenseByOfficeAndBudgetSubHead($budgetSubHeadList->office_id,$datas['fiscal_year'],$budgetSubHeadList->id),2)}}</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($budgetSubHeadList->getRemainBudgetByBudgetSubHeadAndOffice($budgetSubHeadList->office_id,$datas['fiscal_year'],$budgetSubHeadList->id),2)}}</td>
                            <td></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="6" style="text-align: right"><b>कार्यालय जम्मा</b></td>
                        <td class="kalimati" style="text-align: right">{{number_format($granTotalInitialBudget,2)}}</td>
                        <td class="kalimati" style="text-align: right">{{number_format($granTotalAddBudget,2)}}</td>
                        <td class="kalimati" style="text-align: right">{{number_format($granReduceAddBudget,2)}}</td>
                        <td class="kalimati" style="text-align: right">{{number_format($finalBudget,2)}}</td>
                        <td class="kalimati" style="text-align: right">{{number_format($totalExpense,2)}}</td>
                        <td class="kalimati" style="text-align: right">{{number_format($totalExpense,2)}}</td>
                        <td class="kalimati" style="text-align: right">{{number_format($totalRemainBudget,2)}}</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <table width="99%" style="font-size: 13px">

                    <tr>
                        <td style="padding-left: 52px;">तयार गर्ने :</td>
                        <td>पेश गर्ने :</td>
                        <td>सदर गर्ने :</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">पद :</td>
                        <td>पद :</td>
                        <td>पद :</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">मिति :</td>
                        <td>मिति :</td>
                        <td>मिति :</td>
                    </tr>

                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script>
    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '/')

                    nepaliNo += "/";
            }
        });
        // console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });


</script>

<script>
    $(document).ready(function () {
        let trs = $('#fatwari_table tbody').find('tr:not(.second_last,.last)');
        let total_budget = 0;
        let total_this_month_expense = 0;
        let total_up_to_prev_month_expense = 0;
        let total_up_to_this_month_expense = 0;
        let total_advance = 0;
        let total_exp_with_out_advance = 0;
        let total_remain_budget = 0;
        let expense_percentage = 0;
        $.each(trs, function () {

            let total_budget_nep = $.trim($(this).find('td.total-budget').text());
            let total_this_month_expense_nep = $.trim($(this).find('td.this-month-exp').text());
            let total_up_to_prev_month_expense_nep = $.trim($(this).find('td.up-to-pre-month-exp').text());
            let total_up_to_this_month_expense_nep = $.trim($(this).find('td.up-to-this-month-exp').text());
            let advance_nep = $.trim($(this).find('td.advance').text());
            let exp_with_out_advance_nep = $.trim($(this).find('td.exp-with-out-advance').text());
            let remain_budget_nep = $.trim($(this).find('td.remain-budget').text());


            total_budget_roman = convertNepaliToEnglish(total_budget_nep);
            total_this_month_expense_roamn = convertNepaliToEnglish(total_this_month_expense_nep);

            total_up_to_prev_month_expense_roamn = convertNepaliToEnglish(total_up_to_prev_month_expense_nep);
            total_up_to_this_month_expense_roamn = convertNepaliToEnglish(total_up_to_this_month_expense_nep);
            advence_roamn = convertNepaliToEnglish(advance_nep);
            exp_with_out_advance_roman = convertNepaliToEnglish(exp_with_out_advance_nep);
            remain_budget_roman = convertNepaliToEnglish(remain_budget_nep);

            total_budget = parseFloat(total_budget) + parseFloat(total_budget_roman);
            total_this_month_expense = parseFloat(total_this_month_expense) + parseFloat(total_this_month_expense_roamn);
            total_up_to_prev_month_expense = parseFloat(total_up_to_prev_month_expense) + parseFloat(total_up_to_prev_month_expense_roamn);
            total_up_to_this_month_expense = parseFloat(total_up_to_this_month_expense) + parseFloat(total_up_to_this_month_expense_roamn);
            total_advance = parseFloat(total_advance) + parseFloat(advence_roamn);
            total_exp_with_out_advance = parseFloat(total_exp_with_out_advance) + parseFloat(exp_with_out_advance_roman);
            total_remain_budget = parseFloat(total_remain_budget) + parseFloat(remain_budget_roman);
            expense_percentage = (parseFloat(total_up_to_this_month_expense) * 100) / parseFloat(total_budget)


        });


        $('#total_last_budget').text(changeToNepali(total_budget.toString()));
        $('#this_month_nikasa').text(changeToNepali(total_up_to_this_month_expense.toString()));
        $('#this_month_exp').text(changeToNepali(total_this_month_expense.toString()));
        $('#up_to_prev_month_exp').text(changeToNepali(total_up_to_prev_month_expense.toString()));
        $('#up_to_this_month_exp').text(changeToNepali(total_up_to_this_month_expense.toString()));
        $('#advance').text(changeToNepali(total_advance.toString()));
        $('#exp_with_out_advance').text(changeToNepali(total_exp_with_out_advance.toString()));
        $('#remain_budget').text(changeToNepali(total_remain_budget.toString()));
        $('#total_expense_percentage').text(changeToNepali(expense_percentage.toFixed(3)));

    })
</script>

<script>
    function convertNepaliToEnglish(input) {
        var charArray = input.split('');
        var engDate = '';
        $.each(charArray, function (key, value) {

            switch (value) {
                case '१':
                    engDate += '1';
                    break;
                case '२':
                    engDate += '2';
                    break;
                case '३':
                    engDate += '3';
                    break;
                case '४':
                    engDate += '4';
                    break;
                case '५':
                    engDate += '5';
                    break;
                case '६':
                    engDate += '6';
                    break;
                case '०':
                    engDate += '0';
                    break;
                case '७':
                    engDate += '7';
                    break;
                case '८':
                    engDate += '8';
                    break;
                case '९':
                    engDate += '9';
                    break;

                case '-':
                    engDate += '-';
                    break;
                case '.':
                    engDate += '.';
                    break
            }
            // console.log(engDate)
        });

        return engDate

    }
</script>

<script>
    $("#downloadMe").click(function (e) {
        let allTable = $('div[id$=tableHeadingWrapper]').html() + $('div[id$=tableWrapper]').html();
        // console.log(allTable);
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(allTable));
        e.preventDefault();
    });
</script>


<script>
    var currentDate = new Date();
    var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
    var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
    $("#date").text(formatedNepaliDate);
</script>


