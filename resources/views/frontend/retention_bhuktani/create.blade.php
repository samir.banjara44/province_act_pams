@extends('frontend.layouts.app')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <form action="{{route('retention.bhuktani.store')}}" method="post" id="bhuktaniForm">
            <section class="content-header">
                <h1>
                    धरौटी भुक्तानी आदेश बनाउने
                </h1>
                <input type="hidden" id="bs_roman" name="bs_roman">
            </section>

            <!-- Main content -->

            <section class="content">
                <div class="panel panel-primary">
                    <div class="panel-body">

                        {{csrf_field()}}
                        <table>
                            <tr>
                                <td>आदेश न:</td>
                                <td class="kalimati">
                                    <input type="text" name="adesh_number" class="form-control"
                                           value="{{$bhuktani_adesh_number}}" readonly>
                                </td>
                                    <td>आर्थिक वर्ष : </td>
                                <td>
                                    <select name="fiscal_year" id="fiscal_year" class="form-control">
                                        <option value="{{$fiscalYear->id}}" selected>{{$fiscalYear->year}}</option>
                                    </select>
                                </td>
                                <td>मिति</td>
                                <td>
                                    <input type="text" class="form-control" name="date" id="date" value="">
                                </td>
                            </tr>
                        </table>
                        <table class="table" border="1">
                            <thead>
                            <tr style="background-color: #dbdbdb;">
                                <td>भौचर न.</td>
                                <td>मिति</td>
                                <td>व्यहोरा</td>
                                <td>रकम</td>
                                <td>एक्सन</td>
                            </tr>
                            </thead>

                            <tbody>
                            @if($retentionPreBhuktanies->count() > 0)
                                @foreach($retentionPreBhuktanies as $index=>$retentionPreBhuktanie)
                                    <tr>
                                        <td class="kalimati">{{$retentionPreBhuktanie->voucher->voucher_number}}</td>
                                        <td class="kalimati">{{$retentionPreBhuktanie->voucher->date_nep}}</td>
                                        <td>{{$retentionPreBhuktanie->voucher->short_narration}}</td>
                                        <td class="kalimati">{{$retentionPreBhuktanie->amount}}</td>
                                        <td>
                                            <input type="checkbox" name="retentionBhuktani[]"
                                                   class="bhuktani_check_box" value="{{$retentionPreBhuktanie->id}}"
                                                   required>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tr>
                                <td colspan="6" style="text-align: center">
                                    <button class="btn btn-primary" id="btnBhuktaniSubmit">Submit</button>
                                </td>
                            </tr>
                            @else
                                <td colspan="6" style="text-align: center">डाटा उपलब्ध छैन।</td>
                            @endif
                        </table>
                    </div>

                </div>
            </section>
    </form>
            <section class="content">
                <div class="panel-body">
                    <table class="table" border="1">
                        <thead>
                        <tr style="background-color: #dbdbdb;">
                            <td>सि.न.</td>
                            <td>मिति</td>
                            <td>आदेश न.</td>
                            <td>रकम</td>
                            <td>एक्सन</td>
                        </tr>
                        </thead>
                        <tbody>
                        @if($bhuktaniAdeshs->count() > 0)
                            @foreach($bhuktaniAdeshs as $index=>$retentionBhuktani)
                                <tr>
                                    <td>{{++$index}}</td>
                                    <td class="kalimati">{{$retentionBhuktani->date_nepali}}</td>
                                    <td class="kalimati">{{$retentionBhuktani->adesh_number}}</td>
                                    <td>{{$retentionBhuktani->amount}}</td>
                                    <td>
                                        <a type="button" class="btn btn-primary" href="{{route('report.retention.bhuktani.adesh',$retentionBhuktani->id)}}" target="_blank">हेर्ने</a> |
                                        @if (count($bhuktaniAdeshs) - $index == count($bhuktaniAdeshs)-1)
                                        <a type="button" id="bhuktani_delete" data-bhuktani-id='{{$retentionBhuktani->id}}'

                                           class="btn btn-sm btn-primary bhuktani_delete"><i class="fa fa-pencil"></i> Delete</a>
                                            @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </section>
            <!-- /.content -->

    </div>
@endsection

@section('scripts')

    <script>
        $(document).ready(function () {
            $(document).on('click','.bhuktani_delete',function () {
                let bhuktaniId = $(this).attr('data-bhuktani-id')
                let url = '{{route('delete.retention.bhuktani',123)}}'
                url = url.replace(123,bhuktaniId);
                swal({
                    title: "Are you sure?",
                    text: "Delete भए पछि Recovere हुदैन",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,

                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url : url,
                            method : 'get',
                            success : function (res) {
                                    console.log("=====",res);
                                if(res){
                                    swal("Voucher has been deleted!", {
                                        icon: "success",
                                    });
                                    location.reload();
                                } else {
                                    swal("भुक्तानी delete भएन!", {

                                    });
                                }
                            }
                        })
                    }
                });
            })
        })
    </script>

    {{--validation--}}
    <script>
        $(document).on('click', 'button#btnBhuktaniSubmit', function (e) {
            e.preventDefault();
            let nepali_date = $('#date').val();
            let bs_roman = convertNepaliToEnglish(nepali_date);
            $('#bs-roman').val(bs_roman);
            if ($('input.bhuktani_check_box:checked').length) {

                $('#bhuktaniForm').submit();
            } else {

                alert("डाटा select भएन!!")
            }


        })
    </script>
    {{--    Date Picker --}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);
    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            // console.log(input);
            var charArray = input.split('');
            //console.log(charArray);
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1';
                        break;
                    case '२':
                        engDate += '2';
                        break;
                    case '३':
                        engDate += '3';
                        break;
                    case '४':
                        engDate += '4';
                        break;
                    case '५':
                        engDate += '5';
                        break;
                    case '६':
                        engDate += '6';
                        break;
                    case '०':
                        engDate += '0';
                        break;
                    case '७':
                        engDate += '7';
                        break;
                    case '८':
                        engDate += '8';
                        break;
                    case '९':
                        engDate += '9';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
                //console.log(engDate)
            });
            return engDate

        }
    </script>

    <script>
        $(document).ready(function () {
            $('#btnBhuktaniSubmit').click(function (e) {
                e.preventDefault();
                let nepali_date = $('#date').val();
                let bs_roman = convertNepaliToEnglish(nepali_date);
                $('#bs_roman').val(bs_roman);
                if ($('input.bhuktani_check_box:checked').length) {

                    $('#bhuktaniForm').submit();
                } else {

                    alert("कृपया एक्सनको चेक बक्समा चेक लगाउनुहोस् !");
                    $('.bhuktani_check_box').focus();
                }

            })
        });
    </script>
@endsection