<?php

namespace App\Repositories;

use App\helpers\BsHelper;
use App\helpers\FiscalYearHelper;
use App\Models\ActivityReport;
use App\Models\ActivityReportDetail;
use App\Models\BhuktaniVoucher;
use App\Models\Budget;
use App\Models\BudgetDetails;
use App\Models\ExpenseHead;
use App\Models\Voucher;
use App\Models\VoucherDetail;
use Illuminate\Support\Facades\Auth;
use DB;
use phpDocumentor\Reflection\Types\Array_;


class VoucherDetailsRepositoryEloquent implements VoucherDetailsRepository
{
    public function __construct()
    {
        $this->fiscalYearHelper = new FiscalYearHelper();
    }


    public function find($id)
    {
        return VoucherDetail::findorfail($id);
    }

    public function create($attributes, $voucher, $voucherId)
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        foreach ($attributes as $attribute) {
            $voucherDetails = new VoucherDetail();
            $voucherDetails->office_id = Auth::user()->office->id;
            $voucherDetails->journel_id = $voucherId;
            $voucherDetails->budget_sub_head_id = $attribute['budget_sub_head'];
            $voucherDetails->main_activity_id = $attribute['activity_id'];
            $voucherDetails->dr_or_cr = $attribute['drOrCr'];
            $voucherDetails->ledger_type_id = $attribute['ledger_type_id'];
            $voucherDetails->expense_head_id = $attribute['hisab_number'];
            $voucherDetails->bibaran = $attribute['bibran'];
            $voucherDetails->party_type = $attribute['party_type'];
            $voucherDetails->voucher_details_id = $attribute['voucher_details_id'];
            $party_id = $attribute['party_id'];
            if (!$party_id) {
                $voucherDetails->peski_paune_id = 0;
            } else {
                $voucherDetails->peski_paune_id = $attribute['party_id'];
            }
            if ($attribute['drOrCr'] == 1) {
                $voucherDetails->dr_amount = $attribute['amount'];
                $voucherDetails->cr_amount = 0;
            }
            if ($attribute['drOrCr'] == 2) {
                $voucherDetails->dr_amount = 0;
                $voucherDetails->cr_amount = $attribute['amount'];
            }
            $voucherDetails->status = 1;
            $voucherDetails->date_nepali = $attribute['date_nepali'];
            $voucherDetails->date_english = $attribute['date_english'];

            $date_array = explode('-', $attribute['date_english']);
            $bsObj = new BsHelper();
            $data_ad_array = $bsObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
            $voucherDetails->date_english = $data_ad_array['year'] . '-' . $data_ad_array['month'] . '-' . $data_ad_array['date'];

            $date_eng = $attribute['date_english'];
            $myyearfirst = (substr($date_eng, 0, 2));
            $myyearlast = (substr($date_eng, 2, 2));

            $mymonth = (explode('-', $date_eng));
            $month = intval($mymonth[1]);

            if ($month == 1) {
                $finalmonth = 10;
            }
            if ($month == 2) {
                $finalmonth = 11;
            }
            if ($month == 3) {
                $finalmonth = 12;
            }
            if ($month == 4) {
                $finalmonth = 1;
            }
            if ($month == 5) {
                $finalmonth = 2;
            }
            if ($month == 6) {
                $finalmonth = 3;
            }
            if ($month == 7) {
                $finalmonth = 4;
            }
            if ($month == 8) {
                $finalmonth = 5;
            }
            if ($month == 9) {
                $finalmonth = 6;
            }
            if ($month == 10) {
                $finalmonth = 7;
            }
            if ($month == 11) {
                $finalmonth = 8;
            }
            if ($month == 12) {
                $finalmonth = 9;
            }
            $voucherDetails->fiscal_year = $fiscalYear->id;
            $voucherDetails->month = $finalmonth;
            $voucherDetails->save();
        }
        $voucherCollection = collect($attributes);


        $expense = $voucherCollection->where('drOrCr', '=', 1)->whereIn('ledger_type_id', [1, 13]);
        if ($expense->count() > 0) {
            $pefa = $voucherCollection->where('drOrCr', '=', 2)->where('ledger_type_id', '=', 9);
            if ($pefa->count() > 0) {
                $overExp = $voucherCollection->contains(function ($value, $key) {
                    if (($value['drOrCr'] == 2 and $value['ledger_type_id'] == 3) || ($value['drOrCr'] == 2 and $value['ledger_type_id'] == 2)) {
                        return true;
                    }
                });
                $lessExp = $voucherCollection->contains(function ($value, $key) {
                    if ($value['drOrCr'] == 1 and $value['ledger_type_id'] == 3) {
                        return true;
                    }
                });
                if ($overExp && $lessExp) {
                    foreach ($voucherCollection as $detail) {
                        $activityReport = ActivityReport::where('activity_id', $detail['activity_id'])->first();
                        $budgetDetails = BudgetDetails::findorfail($detail['activity_id']);
                        if ($activityReport->count() > 0) {
                            $expenseHead = ExpenseHead::where('id', $budgetDetails->expense_head_id)->where('status', 1)->first();
                            if ($detail['drOrCr'] == 1 and $detail['ledger_type_id'] == 3) {
                                $data['income'] = $detail['amount'];
                                $total_expense = $activityReport->expense = $activityReport->expense - $detail['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $detail['activity_id'];
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $detail['amount'] * (-1);
                                $activityReportDetails->voucher_id = $voucherId;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }
                        if (($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 2)) {
                            if ($activityReport->count() > 0) {
                                $total_expense = $activityReport->expense = $activityReport->expense + $detail['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $detail['activity_id'];
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $detail['amount'];
                                $activityReportDetails->voucher_id = $voucherId;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }
                    }
                } elseif ($overExp and !$lessExp) {
                    foreach ($voucherCollection as $detail) {
                        if (($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 2) || ($detail['drOrCr'] == 1 and $detail['ledger_type_id'] == 1)) {

                            $activityReport = ActivityReport::where('activity_id', $detail['activity_id'])->first();
                            $total_expense = $activityReport->expense = (float)$activityReport->expense + (float)$detail['amount'];
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $budgetDetails = BudgetDetails::findorfail($detail['activity_id']);
                            $expenseHead = ExpenseHead::where('id', $budgetDetails->expense_head_id)->where('status', 1)->first();
                            $activityReportDetails = new ActivityReportDetail();
                            $activityReportDetails->activity_report_id = $activityReport->id;
                            $activityReportDetails->activity_id = $detail['activity_id'];
                            $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                            $activityReportDetails->expense = $detail['amount'];
                            $activityReportDetails->voucher_id = $voucherId;
                            $activityReportDetails->month = $finalmonth;
                            $activityReportDetails->fiscal_year = $fiscalYear->id;
                            $activityReportDetails->save();
                        }
                    }
                } elseif ($lessExp and !$overExp) {
                    foreach ($voucherCollection as $detail) {
                        if ($detail['drOrCr'] == 1 and $detail['ledger_type_id'] == 3) {

                            $data['income'] = $detail['amount'];
                            $activityReport = ActivityReport::where('activity_id', $detail['activity_id'])->first();

                            $total_expense = $activityReport->expense = $activityReport->expense - (float)$detail['amount'];
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $voucher = VoucherDetail::findorfail($attribute['voucher_details_id']);
                            $expenseHead = ExpenseHead::where('id', $detail['hisab_number'])->where('status', 1)->first();

                            $activityReportDetails = new ActivityReportDetail();
                            $activityReportDetails->activity_report_id = $activityReport->id;
                            $activityReportDetails->activity_id = $detail['activity_id'];
                            $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                            $activityReportDetails->expense = $detail['amount'] * (-1);
                            $activityReportDetails->voucher_id = $voucherId;
                            $activityReportDetails->month = $finalmonth;
                            $activityReportDetails->fiscal_year = $fiscalYear->id;
                            $activityReportDetails->save();
                        }
                    }
                } else {
                    $pefa = $voucherCollection->where('drOrCr', '=', 2)->where('ledger_type_id', '=', 9)->first();
                    $activity_id = $pefa['activity_id'];
                    $count = 0;
                    foreach ($voucherCollection as $attribute) {
                        $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', '=', 1)->first();
                        if ($activity_id != $attribute['activity_id']) {
                            if (($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 1)) {
                                $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                                $total_expense = 0;
                                if ($activityReport) {
                                    $activityReport->expense = $total_expense = $activityReport->expense + (float)$attribute['amount'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activityReport->activity_id;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $attribute['amount'];
                                    $activityReportDetails->voucher_id = $voucherId;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                            }
                        } else {
                            if (($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 1)) {
                                $count = 1;
                                $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                                $total_expense = 0;
                                if ($activityReport) {
                                    $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activityReport->activity_id;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $attribute['amount'] * (-1);
                                    $activityReportDetails->voucher_id = $voucherId;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                            }
                            if (($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 9)) {
                                if ($count == 0) {
                                    $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                                    $total_expense = 0;
                                    if ($activityReport) {
                                        $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                        $activityReport->save();

                                        $activityReportDetails = new ActivityReportDetail();
                                        $activityReportDetails->activity_report_id = $activityReport->id;
                                        $activityReportDetails->activity_id = $activityReport->activity_id;
                                        $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                        $activityReportDetails->expense = $attribute['amount'] * (-1);
                                        $activityReportDetails->voucher_id = $voucherId;
                                        $activityReportDetails->month = $finalmonth;
                                        $activityReportDetails->fiscal_year = $fiscalYear->id;
                                        $activityReportDetails->save();
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $adjustExpense = $voucherCollection->where('drOrCr', '=', 2)->where('ledger_type_id', '=', 1);
                if ($adjustExpense->count() > 0) {
                    foreach ($voucherCollection as $attribute) {
                        $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', '=', 1)->first();
                        if (($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 1)) {
                            $data['expense'] = $attribute['amount'];
                            $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                            $total_expense = 0;
                            if ($activityReport) {
                                $currentExpense = $activityReport->expense;
                                $activityReport->expense = $total_expense = (float)$currentExpense + (float)$attribute['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $attribute['activity_id'];
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $attribute['amount'];
                                $activityReportDetails->voucher_id = $voucherId;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }

                        if ($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 1) {
                            $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                            $total_expense = 0;
                            if ($activityReport) {
                                $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $activityReport->activity_id;
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $attribute['amount'] * (-1);
                                $activityReportDetails->voucher_id = $voucherId;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }
                    }
                } else {
                    // ba khaa
                    foreach ($voucherCollection as $attribute) {
                        $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', 1)->first();
                        if (($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 1) || ($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 4)) {
                            $data['expense'] = $attribute['amount'];
                            $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                            $total_expense = 0;
                            if ($activityReport) {
                                $currentExpense = $activityReport->expense;
                                $activityReport->expense = $total_expense = (float)$currentExpense + (float)$attribute['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $attribute['activity_id'];
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $attribute['amount'];
                                $activityReportDetails->voucher_id = $voucherId;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }

                        if ($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 1) {
                            $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                            $total_expense = 0;
                            if ($activityReport) {
                                $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $activityReport->activity_id;
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $attribute['amount'] * (-1);
                                $activityReportDetails->voucher_id = $voucherId;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }
                    }
                }
            }
        } else {
            $adjustPeski = $voucherCollection->where('drOrCr', '=', 2)->where('ledger_type_id', '=', 9);
            if ($adjustPeski->count() > 0) {
                foreach ($voucherCollection as $attribute) {
                    $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', 1)->first();
                    if ($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 4) {
                        $data['expense'] = $attribute['amount'];
                        $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                        $total_expense = 0;
                        if ($activityReport) {
                            $currentExpense = $activityReport->expense;
                            $activityReport->expense = $total_expense = (float)$currentExpense + (float)$attribute['amount'];
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = new ActivityReportDetail();
                            $activityReportDetails->activity_report_id = $activityReport->id;
                            $activityReportDetails->activity_id = $attribute['activity_id'];
                            $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                            $activityReportDetails->expense = $attribute['amount'];
                            $activityReportDetails->voucher_id = $voucherId;
                            $activityReportDetails->month = $finalmonth;
                            $activityReportDetails->fiscal_year = $fiscalYear->id;
                            $activityReportDetails->save();
                        }
                    }

                    if ($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 2) {
                        $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                        $total_expense = 0;
                        if ($activityReport) {
                            $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = new ActivityReportDetail();
                            $activityReportDetails->activity_report_id = $activityReport->id;
                            $activityReportDetails->activity_id = $activityReport->activity_id;
                            $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                            $activityReportDetails->expense = $attribute['amount'] * (-1);
                            $activityReportDetails->voucher_id = $voucherId;
                            $activityReportDetails->month = $finalmonth;
                            $activityReportDetails->fiscal_year = $fiscalYear->id;
                            $activityReportDetails->save();
                        }
                    }

                    if ($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 9) {
                        $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                        $total_expense = 0;
                        if ($activityReport) {
                            $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = new ActivityReportDetail();
                            $activityReportDetails->activity_report_id = $activityReport->id;
                            $activityReportDetails->activity_id = $activityReport->activity_id;
                            $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                            $activityReportDetails->expense = $attribute['amount'] * (-1);
                            $activityReportDetails->voucher_id = $voucherId;
                            $activityReportDetails->month = $finalmonth;
                            $activityReportDetails->fiscal_year = $fiscalYear->id;
                            $activityReportDetails->save();
                        }
                    }
                }
            } else {
                $lastYearPefa = $voucherCollection->where('drOrCr', '=', 2)->where('ledger_type_id', '=', 12);
                if ($lastYearPefa->count() > 0) {
                    $overExp = $voucherCollection->contains(function ($value, $key) {
                        if ($value['drOrCr'] == 2 and $value['ledger_type_id'] == 3) {
                            return "over";
                        }
                    });
                    $lessExp = $voucherCollection->contains(function ($value, $key) {
                        if ($value['drOrCr'] == 1 and $value['ledger_type_id'] == 3) {
                            return "less";
                        }
                    });

                    if ($overExp && $lessExp) {
                        foreach ($voucherCollection as $detail) {
                            if ($detail['drOrCr'] == 1 and $detail['ledger_type_id'] == 3) {
                                $data['income'] = $detail['amount'];
                            }
                            if (($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 2)) {

                                $data['expense'] = $detail['amount'];
                            }

                            $activityReport = ActivityReport::where('activity_id', $detail['activity_id'])->first();
                            if ($activityReport->count() > 0) {
                                if (array_key_exists('expense', $data)) {
                                    $total_expense = $activityReport->expense = $activityReport->expense + $data['expense'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $budgetDetails = BudgetDetails::findorfail($detail['activity_id']);
                                    $expenseHead = ExpenseHead::where('id', $budgetDetails->expense_head_id)->where('status', 1)->first();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $detail['activity_id'];
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $data['expense'];
                                    $activityReportDetails->voucher_id = $voucherId;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                                if (array_key_exists('income', $data)) {

                                    $currentExpense = $activityReport->total_expense;
                                    $total_expense = $activityReport->expense = $currentExpense - $data['income'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $detail['activity_id'];
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $data['income'] * (-1);
                                    $activityReportDetails->voucher_id = $voucherId;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                            }
                        }
                    } elseif ($overExp and !$lessExp) {

                        foreach ($voucherCollection as $detail) {
                            if (($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 2)) {

                                $activityReport = ActivityReport::where('activity_id', $detail['activity_id'])->first();
                                if ($activityReport) {
                                    $total_expense = $activityReport->expense = (float)$activityReport->expense + (float)$detail['amount'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $budgetDetails = BudgetDetails::findorfail($detail['activity_id']);
                                    $expenseHead = ExpenseHead::where('id', $budgetDetails->expense_head_id)->where('status', 1)->first();
                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $detail['activity_id'];
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $detail['amount'];
                                    $activityReportDetails->voucher_id = $voucherId;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                            }
                        }
                    } elseif ($lessExp and !$overExp) {
                        foreach ($voucherCollection as $detail) {
                            if ($detail['drOrCr'] == 1 and $detail['ledger_type_id'] == 3) {

                                $data['income'] = $detail['amount'];
                                $activityReport = ActivityReport::where('activity_id', $detail['activity_id'])->first();

                                $total_expense = $activityReport->expense = $activityReport->expense - (float)$detail['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $voucher = VoucherDetail::findorfail($attribute['voucher_details_id']);
                                $expenseHead = ExpenseHead::where('id', $detail['hisab_number'])->where('status', 1)->first();

                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $detail['activity_id'];
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $detail['amount'] * (-1);
                                $activityReportDetails->voucher_id = $voucherId;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }
                    }
                } else {
                    $peski = $voucherCollection->where('drOrCr', '=', 1)->where('ledger_type_id', '=', 4);
                    if ($peski->count() > 0) {
                        // peski
                        foreach ($voucherCollection as $attribute) {
                            $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', 1)->first();
                            if ($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 4) {
                                $data['expense'] = $attribute['amount'];
                                $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                                $total_expense = 0;
                                if ($activityReport) {
                                    $currentExpense = $activityReport->expense;
                                    $activityReport->expense = $total_expense = (float)$currentExpense + (float)$attribute['amount'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $attribute['activity_id'];
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $attribute['amount'];
                                    $activityReportDetails->voucher_id = $voucherId;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                            }
                        }
                    } else {
                        foreach ($voucherCollection as $attribute) {
                            $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', '=', 1)->first();

                            if ($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 1) {
                                $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                                $total_expense = 0;
                                if ($activityReport) {
                                    $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activityReport->activity_id;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $attribute['amount'] * (-1);
                                    $activityReportDetails->voucher_id = $voucherId;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                            }
                        }
                    }
                }
            }
        }
        return $voucherDetails;
    }


    public function createNikasaVoucherDetails($attributes, $voucher_id, $budget = null)
    {

        $voucherDetails = new VoucherDetail();
        $voucherDetails->office_id = Auth::user()->office->id;
        $voucherDetails->journel_id = $voucher_id;
        $voucherDetails->budget_sub_head_id = $attributes['budget_sub_head'];

        if (array_key_exists('activity_id', $attributes)) {
            $voucherDetails->main_activity_id = $attributes['activity_id'];
        }
        $voucherDetails->dr_or_cr = $attributes['drOrCr'];

        $voucherDetails->ledger_type_id = $attributes['ledger_type_id'];
        if ($budget) {
            $voucherDetails->expense_head_id = $budget[0]->expense_head_by_id->id;
            $voucherDetails->bibaran = $budget[0]->expense_head_by_id->expense_head_sirsak;
        } else {
            $voucherDetails->expense_head_id = 187;
            $voucherDetails->bibaran = "एकल कोष खाता";
        }
        $voucherDetails->party_type = NULL;
        //        $voucherDetails->peski_type = $attributes['peski_type'];
        $party_id = NULL;

        if ($attributes['drOrCr'] == 1) {
            $voucherDetails->dr_amount = $attributes['amount'];
            $voucherDetails->cr_amount = 0;
        }
        if ($attributes['drOrCr'] == 2) {

            $voucherDetails->dr_amount = 0;
            $voucherDetails->cr_amount = $attributes['amount'];
        }
        $voucherDetails->status = 1;
        $voucherDetails->date_nepali = $attributes['date'];
        $date_array = explode('-', $attributes['bs-roman']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
        $voucherDetails->date_english = $data_ad_array['year'] . '-' . $data_ad_array['month'] . '-' . $data_ad_array['date'];

        $date_eng = $attributes['bs-roman'];
        $myyearfirst = (substr($date_eng, 0, 2));
        $myyearlast = (substr($date_eng, 2, 2));
        $mymonth = (explode('-', $date_eng));
        $month = intval($mymonth[1]);
        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }
        if (intval($month) >= 4) {
            $voucherDetails->fiscal_year = $myyearfirst . $myyearlast . "/" . (intval($myyearlast) + 1);
        } else {
            $voucherDetails->fiscal_year = $myyearfirst . (intval($myyearlast) - 1) . "/" . $myyearlast;
        }
        $voucherDetails->month = $finalmonth;
        $voucherDetails->save();
        return $voucherDetails;
    }

    public function get_all_pre_month_voucher_by_budget_sub_head_id($budget_sub_head_id, $fiscalYear, $month)
    {

        $office_id = Auth::user()->office->id;
        return $voucherList = VoucherDetail::where('office_id', $office_id)
            ->where('fiscal_year', $fiscalYear)
            ->where('month', $month)
            ->where('budget_sub_head_id', $budget_sub_head_id)->get();
    }

    public function get_expense_head_by_budget_sub_head_and_office_id($fiscal_year, $budget_sub_head_id, $ledger_type_id)
    {
        $vouchers = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->join('main_expense_head', 'voucher_details.expense_head_id', '=', 'main_expense_head.id')
            ->select('main_expense_head.id', 'main_expense_head.expense_head_sirsak', 'main_expense_head.expense_head_code')
            ->select('main_expense_head.*')
            ->where('voucher_details.budget_sub_head_id', $budget_sub_head_id)
            ->where('vouchers.status', 1)
            ->where('vouchers.fiscal_year', $fiscal_year)
            ->where('voucher_details.ledger_type_id', $ledger_type_id)->get()->toArray();

        $all_expense_heads = [];
        foreach ($vouchers as $voucher) {
            $expense_heads = $voucher->id;
            $temp = [];
            $temp["id"] = $voucher->id;
            $temp["sirsak"] = $voucher->expense_head_sirsak;
            $temp["code"] = $voucher->expense_head_code;
            if (!$this->check_unique($all_expense_heads, 'id', $voucher->id))
                array_push($all_expense_heads, $temp);
        }
        $unique_heads = $this->multi_unique($all_expense_heads);
        return $all_expense_heads;
    }

    public function get_liability_by_budget_sub_head_and_office_id($fiscal_year, $budget_sub_head_id, $ledger_type_id)
    {
        $vouchers = VoucherDetail::where('budget_sub_head_id', $budget_sub_head_id)
            ->where('fiscal_year', $fiscal_year)
            ->where('ledger_type_id', $ledger_type_id)
            ->with('expense_head')->get()->toArray();
        $all_expense_heads = [];
        foreach ($vouchers as $voucher) {
            $expense_heads = $voucher['expense_head'];
            $temp = [];
            $temp["id"] = $expense_heads['id'];
            $temp["sirsak"] = $expense_heads["expense_head_sirsak"];
            $temp["code"] = $expense_heads["expense_head_code"];
            if (!$this->check_unique($all_expense_heads, 'id', $expense_heads['id']))
                array_push($all_expense_heads, $temp);
        }
        return $all_expense_heads;
    }


    function check_unique($array, $key, $val)
    {
        foreach ($array as $item)
            if (isset($item[$key]) && $item[$key] == $val)
                return true;
        return false;
    }

    public function get_peski_party_by_budget_head_and_office($fiscal_year, $budget_sub_head_id, $ledger_type_id)
    {

        $vouchers = VoucherDetail::where('budget_sub_head_id', $budget_sub_head_id)
            ->where('ledger_type_id', $ledger_type_id)
            ->where('fiscal_year', $fiscal_year)
            ->with('advancePayment')->get()->toArray();
        $all_peski_party = [];
        foreach ($vouchers as $voucher) {
            $peski_party = $voucher['advance_payment'];

            $temp = [];
            $temp["id"] = $peski_party['id'];
            $temp["sirsak"] = $peski_party["name_nep"];
            if (!$this->check_unique($all_peski_party, 'id', $peski_party['id']))
                array_push($all_peski_party, $temp);
        }
        return $all_peski_party;
    }

    public function get_activities_by_budget_head_and_office($fiscal_year, $budget_sub_head_id, $ledger_type_id)
    {
        $activities = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->select('activity_report_details.*')
            ->where('activity_report.budget_sub_head_id', $budget_sub_head_id)
            ->where('activity_report.fiscal_year', $fiscal_year)->get();

        $all_activities = [];
        foreach ($activities as $activity) {
            $temp = [];
            $temp["sirsak"] = $activity->budgetDetails->sub_activity;
            $temp["id"] = $activity->activity_id;
            $temp["code"] = $activity->expense_head;
            if (!$this->check_unique($all_activities, 'id', $activity->activity_id))
                array_push($all_activities, $temp);
        }
        $unique_activity = $this->multi_unique($all_activities);
        return $unique_activity;
    }

    function multi_unique($src)
    {
        $output = array_map(
            "unserialize",
            array_unique(array_map("serialize", $src))
        );
        return $output;
    }

    public function get_all_voucher_by_budget_sub_head_and_ledger_type_id($fiscal_year, $budget_sub_head_id, $ledger_type_id)
    {
        return  VoucherDetail::where('budget_sub_head_id', $budget_sub_head_id)
            ->where('fiscal_year', $fiscal_year)
            ->where('expense_head_id', $ledger_type_id)->get();
    }

    public function get_details_by_voucher_id($voucher_id)
    {

        return $voucherDetailsList = VoucherDetail::where('journel_id', $voucher_id)->with('voucher')->get();
    }

    public function get_details_by_voucher_id_with_activity($id)
    {
        return $voucherDetailsList = VoucherDetail::where('journel_id', $id)
            ->with(['mainActivity'])
            ->orderBy('dr_or_cr', 'asc')
            ->get();
    }


    //    Update voucher detail
    public function updateVoucherDetails($voucher_id, $voucher_detail_data)
    {
        $voucher = Voucher::findorfail($voucher_id);
        $voucherDetails_temp = $voucher->details;
        $voucher->details()->delete();
        foreach ($voucher_detail_data as $detail) {
            $voucherDetails = new VoucherDetail();
            $voucherDetails->office_id = Auth::user()->office->id;
            $voucherDetails->journel_id = $voucher->id;
            $voucherDetails->budget_sub_head_id = $detail['budget_sub_head'];
            $voucherDetails->main_activity_id = $detail['activity_id'];
            $voucherDetails->dr_or_cr = $detail['drOrCr'];
            $voucherDetails->ledger_type_id = $detail['ledger_type_id'];
            $voucherDetails->expense_head_id = $detail['hisab_number'];
            $voucherDetails->bibaran = $detail['bibran'];
            $voucherDetails->party_type = array_key_exists('party_type', $detail) ? $detail['party_type'] : null;
            $voucherDetails->peski_paune_id = array_key_exists('party_id', $detail) ? $detail['party_id'] : null;
            $voucherDetails->voucher_details_id = array_key_exists('voucher_details_id', $detail) ? $detail['voucher_details_id'] : null;

            if ($detail['drOrCr'] == 1) {
                $voucherDetails->dr_amount = $detail['amount'];
                $voucherDetails->cr_amount = 0;
            }
            if ($detail['drOrCr'] == 2) {
                $voucherDetails->dr_amount = 0;
                $voucherDetails->cr_amount = $detail['amount'];
            }
            $voucherDetails->status = 0;
            $voucherDetails->date_nepali = $detail['date_nepali'];
            $date_array = explode('-', $detail['date_english']);
            $bsObj = new BsHelper();
            $data_ad_array = $bsObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
            $voucherDetails->date_english = $data_ad_array['year'] . '-' . $data_ad_array['month'] . '-' . $data_ad_array['date'];
            $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
            $date_eng = $detail['date_english'];
            $myyearfirst = (substr($date_eng, 0, 2));
            $myyearlast = (substr($date_eng, 2, 2));
            $mymonth = (explode('-', $date_eng));
            $month = intval($mymonth[1]);

            if ($month == 1) {
                $finalmonth = 10;
            }
            if ($month == 2) {
                $finalmonth = 11;
            }
            if ($month == 3) {
                $finalmonth = 12;
            }
            if ($month == 4) {
                $finalmonth = 1;
            }
            if ($month == 5) {
                $finalmonth = 2;
            }
            if ($month == 6) {
                $finalmonth = 3;
            }
            if ($month == 7) {
                $finalmonth = 4;
            }
            if ($month == 8) {
                $finalmonth = 5;
            }
            if ($month == 9) {
                $finalmonth = 6;
            }
            if ($month == 10) {
                $finalmonth = 7;
            }
            if ($month == 11) {
                $finalmonth = 8;
            }
            if ($month == 12) {
                $finalmonth = 9;
            }
            // if (intval($month) >= 4) {
            //     $voucherDetails->fiscal_year = $myyearfirst . $myyearlast . "/" . (intval($myyearlast) + 1);
            // } else {
            //     $voucherDetails->fiscal_year = $myyearfirst . (intval($myyearlast) - 1) . "/" . $myyearlast;
            // }
            $voucherDetails->fiscal_year = $fiscalYear->id;
            $voucherDetails->month = $finalmonth;
            $voucherDetails->save();
        }

        $voucherCollection = collect($voucher_detail_data);
        $activityreportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->get();
        foreach ($activityreportDetails as $activityReportDetail) {
            $activityReport = ActivityReport::where('activity_id', $activityReportDetail->activity_id)->first();
            if ($activityReportDetail->expense > 0) {
                $activityReport->expense = $currentExpense = (float)$activityReport->expense - (float)$activityReportDetail->expense;
                $activityReport->remain = (float)$activityReport->budget - (float)$currentExpense;
            } else if ($activityReportDetail->expense < 0) {
                $activityReport->expense = $currentExpense = (float)$activityReport->expense + (float)(-1) * $activityReportDetail->expense;
                $activityReport->remain = (float)$activityReport->budget - (float)$currentExpense;
            }
            $activityReport->save();
            $activityReportDetail->delete();
        }
        $expense = $voucherCollection->where('drOrCr', '=', 1)->whereIn('ledger_type_id', [1, 13]);
        $data = [];
        if ($expense->count() > 0) {
            $pefa = $voucherCollection->where('drOrCr', '=', 2)->where('ledger_type_id', '=', 9);
            if ($pefa->count() > 0) {
                $overExp = $voucherCollection->contains(function ($value, $key) {
                    if (($value['drOrCr'] == 2 and $value['ledger_type_id'] == 3) or ($value['drOrCr'] == 2 and $value['ledger_type_id'] == 2)) {
                        return true;
                    }
                });
                $lessExp = $voucherCollection->contains(function ($value, $key) {
                    if ($value['drOrCr'] == 1 and $value['ledger_type_id'] == 3) {
                        return true;
                    }
                });
                if ($overExp && $lessExp) {
                    foreach ($voucherCollection as $detail) {
                        $activityReport = ActivityReport::where('activity_id', $detail['activity_id'])->first();
                        if ($detail['drOrCr'] == 1 and $detail['ledger_type_id'] == 3) {
                            if ($activityReport->count() > 0) {
                                $total_expense = $activityReport->expense = $activityReport->expense - $detail['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $budgetDetails = BudgetDetails::findorfail($detail['activity_id']);
                                $expenseHead = ExpenseHead::where('id', $budgetDetails->expense_head_id)->where('status', 1)->first();
                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $activityReport->activity_id;
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = -$detail['amount'];
                                $activityReportDetails->voucher_id = $voucher_id;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }
                        if (($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 2)) {
                            if ($activityReport->count() > 0) {
                                $currentExpense = $activityReport->expense;
                                $total_expense = $activityReport->expense = $currentExpense + $detail['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();
                                $budgetDetails = BudgetDetails::findorfail($detail['activity_id']);
                                $expenseHead = ExpenseHead::where('id', $budgetDetails->expense_head_id)->where('status', 1)->first();
                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_id = $detail['activity_id'];
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $detail['amount'];
                                $activityReportDetails->voucher_id = $voucher_id;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }
                    }
                } elseif ($overExp && !$lessExp) {
                    foreach ($voucherCollection as $detail) {
                        //                      over expense
                        if (($voucherCollection->where('drOrCr', '=', 2) and $voucherCollection->where('ledger_type_id', '=', 3)) or ($voucherCollection->where('drOrCr', '=', 2) and $voucherCollection->where('ledger_type_id', '=', 2))) {
                            if (($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 2)) {

                                $activityReport = ActivityReport::where('activity_id', $detail['activity_id'])->first();
                                $currentExpense = $activityReport->expense;
                                $total_expense = $activityReport->expense = $currentExpense + $detail['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $budgetDetails = BudgetDetails::findorfail($detail['activity_id']);
                                $expenseHead = ExpenseHead::where('id', $budgetDetails->expense_head_id)->where('status', 1)->first();
                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_id = $detail['activity_id'];
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $detail['amount'];
                                $activityReportDetails->voucher_id = $voucher_id;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }
                    }
                } elseif ($lessExp && !$overExp) {
                    foreach ($voucherCollection as $detail) {
                        if ($detail['drOrCr'] == 1 and $detail['ledger_type_id'] == 3) {
                            $data['income'] = $detail['amount'];
                            $activityReport = ActivityReport::where('activity_id', $detail['activity_id'])->first();
                            $total_expense = $activityReport->expense = $activityReport->expense - $detail['amount'];
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $budgetDetails = BudgetDetails::findorfail($detail['activity_id']);
                            $expenseHead = ExpenseHead::where('id', $budgetDetails->expense_head_id)->where('status', 1)->first();
                            $activityReportDetails = new ActivityReportDetail();
                            $activityReportDetails->activity_report_id = $activityReport->id;
                            $activityReportDetails->activity_id = $activityReport->activity_id;
                            $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                            $activityReportDetails->expense = -$detail['amount'];
                            $activityReportDetails->voucher_id = $voucher_id;
                            $activityReportDetails->month = $finalmonth;
                            $activityReportDetails->fiscal_year = $fiscalYear->id;
                            $activityReportDetails->save();
                        }
                    }
                } else {
                    $pefa = $voucherCollection->where('drOrCr', '=', 2)->where('ledger_type_id', '=', 9)->first();
                    $activity_id = $pefa['activity_id'];
                    $count = 0;
                    foreach ($voucherCollection as $attribute) {
                        $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', '=', 1)->first();
                        if ($activity_id != $attribute['activity_id']) {
                            if (($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 1)) {
                                $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                                $total_expense = 0;
                                if ($activityReport) {
                                    $activityReport->expense = $total_expense = $activityReport->expense + (float)$attribute['amount'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activityReport->activity_id;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $attribute['amount'];
                                    $activityReportDetails->voucher_id = $voucher_id;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                            }
                        } else {
                            if (($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 1)) {
                                $count = 1;
                                $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                                $total_expense = 0;
                                if ($activityReport) {
                                    $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activityReport->activity_id;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $attribute['amount'] * (-1);
                                    $activityReportDetails->voucher_id = $voucher_id;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                            }
                            if (($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 9)) {
                                if ($count == 0) {
                                    $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                                    $total_expense = 0;
                                    if ($activityReport) {
                                        $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                        $activityReport->save();

                                        $activityReportDetails = new ActivityReportDetail();
                                        $activityReportDetails->activity_report_id = $activityReport->id;
                                        $activityReportDetails->activity_id = $activityReport->activity_id;
                                        $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                        $activityReportDetails->expense = $attribute['amount'] * (-1);
                                        $activityReportDetails->voucher_id = $voucher_id;
                                        $activityReportDetails->month = $finalmonth;
                                        $activityReportDetails->fiscal_year = $fiscalYear->id;
                                        $activityReportDetails->save();
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $adjustExpense = $voucherCollection->where('drOrCr', '=', 2)->where('ledger_type_id', '=', 1);
                if ($adjustExpense->count() > 0) {
                    foreach ($voucherCollection as $attribute) {

                        $activityReportDetail = ActivityReportDetail::where('voucher_id', $voucher_id)->get();
                        $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', 1)->first();
                        if (($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 1)) {
                            $data['expense'] = $attribute['amount'];
                            $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                            $total_expense = 0;
                            if ($activityReport) {
                                $currentExpense = $activityReport->expense;
                                $activityReport->expense = $total_expense = (float)$currentExpense + (float)$attribute['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $attribute['activity_id'];
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $attribute['amount'];
                                $activityReportDetails->voucher_id = $voucher_id;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }

                        if ($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 1) {
                            $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                            $total_expense = 0;
                            if ($activityReport) {
                                $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $activityReport->activity_id;
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $attribute['amount'] * (-1);
                                $activityReportDetails->voucher_id = $voucher_id;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }
                    }
                } else {
                    foreach ($voucherCollection as $attribute) {
                        $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', 1)->first();
                        if (($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 1) or ($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 4)) {

                            $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                            $total_expense = 0;
                            if ($activityReport) {
                                $activityReport->expense = $total_expense = $activityReport->expense + (float)$attribute['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $activityReport->activity_id;
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $attribute['amount'];
                                $activityReportDetails->voucher_id = $voucher_id;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }

                        if ($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 1) {
                            $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                            $total_expense = 0;
                            if ($activityReport) {
                                $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $activityReport->activity_id;
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = $attribute['amount'] * (-1);
                                $activityReportDetails->voucher_id = $voucher_id;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }
                    }
                }
            }
        } else {
            $adjustPeski = $voucherCollection->where('drOrCr', '=', 2)->where('ledger_type_id', '=', 9);
            if ($adjustPeski->count() > 0) {
                foreach ($voucherCollection as $attribute) {
                    $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', 1)->first();
                    if ($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 4) {
                        $data['expense'] = $attribute['amount'];
                        $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                        $total_expense = 0;
                        if ($activityReport) {
                            $currentExpense = $activityReport->expense;
                            $activityReport->expense = $total_expense = (float)$currentExpense + (float)$attribute['amount'];
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = new ActivityReportDetail();
                            $activityReportDetails->activity_report_id = $activityReport->id;
                            $activityReportDetails->activity_id = $attribute['activity_id'];
                            $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                            $activityReportDetails->expense = $attribute['amount'];
                            $activityReportDetails->voucher_id = $voucher_id;
                            $activityReportDetails->month = $finalmonth;
                            $activityReportDetails->fiscal_year = $fiscalYear->id;
                            $activityReportDetails->save();
                        }
                    }

                    if ($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 2) {
                        $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                        $total_expense = 0;
                        if ($activityReport) {
                            $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = new ActivityReportDetail();
                            $activityReportDetails->activity_report_id = $activityReport->id;
                            $activityReportDetails->activity_id = $activityReport->activity_id;
                            $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                            $activityReportDetails->expense = $attribute['amount'] * (-1);
                            $activityReportDetails->voucher_id = $voucher_id;
                            $activityReportDetails->month = $finalmonth;
                            $activityReportDetails->fiscal_year = $fiscalYear->id;
                            $activityReportDetails->save();
                        }
                    }
                    if ($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 9) {
                        $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                        $total_expense = 0;
                        if ($activityReport) {
                            $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = new ActivityReportDetail();
                            $activityReportDetails->activity_report_id = $activityReport->id;
                            $activityReportDetails->activity_id = $activityReport->activity_id;
                            $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                            $activityReportDetails->expense = $attribute['amount'] * (-1);
                            $activityReportDetails->voucher_id = $voucher_id;
                            $activityReportDetails->month = $finalmonth;
                            $activityReportDetails->fiscal_year = $fiscalYear->id;
                            $activityReportDetails->save();
                        }
                    }
                }
            } else {
                $lastYearPefa = $voucherCollection->where('drOrCr', '=', 2)->where('ledger_type_id', '=', 12);
                if ($lastYearPefa->count() > 0) {
                    $overExp = $voucherCollection->contains(function ($value, $key) {
                        if ($value['drOrCr'] == 2 and $value['ledger_type_id'] == 3) {
                            return "over";
                        }
                    });
                    $lessExp = $voucherCollection->contains(function ($value, $key) {
                        if ($value['drOrCr'] == 1 and $value['ledger_type_id'] == 3) {
                            return "less";
                        }
                    });

                    if ($overExp && $lessExp) {
                        foreach ($voucherCollection as $detail) {
                            if ($detail['drOrCr'] == 1 and $detail['ledger_type_id'] == 3) {
                                $data['income'] = $detail['amount'];
                            }
                            if (($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 2)) {
                                $data['expense'] = $detail['amount'];
                            }

                            $activityReport = ActivityReport::where('activity_id', $detail['activity_id'])->first();
                            if ($activityReport->count() > 0) {
                                if (array_key_exists('expense', $data)) {

                                    $currentExpense = $activityReport->expense;
                                    $total_expense = $activityReport->expense = $currentExpense + $data['expense'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $budgetDetails = BudgetDetails::findorfail($detail['activity_id']);
                                    $expenseHead = ExpenseHead::where('id', $budgetDetails->expense_head_id)->where('status', 1)->first();
                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_id = $detail['activity_id'];
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $data['expense'];
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                                if (array_key_exists('income', $data)) {

                                    $currentExpense = $activityReport->expense;
                                    $total_expense = $activityReport->expense = $currentExpense - $data['income'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $budgetDetails = BudgetDetails::findorfail($detail['activity_id']);
                                    $expenseHead = ExpenseHead::where('id', $budgetDetails->expense_head_id)->where('status', 1)->first();
                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activityReport->activity_id;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = -$detail['amount'];
                                    $activityReportDetails->voucher_id = $voucher_id;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                            }
                        }
                    } elseif ($overExp and !$lessExp) {
                        foreach ($voucherCollection as $detail) {
                            //                                    over expense
                            if (($voucherCollection->where('drOrCr', '=', 2) and $voucherCollection->where('ledger_type_id', '=', 3))) {
                                if (($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['drOrCr'] == 2 and $detail['ledger_type_id'] == 2)) {

                                    $activityReport = ActivityReport::where('activity_id', $detail['activity_id'])->first();
                                    if ($activityReport) {
                                        $currentExpense = $activityReport->expense;
                                        $total_expense = $activityReport->expense = $currentExpense + $detail['amount'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                        $activityReport->save();

                                        $budgetDetails = BudgetDetails::findorfail($detail['activity_id']);
                                        $expenseHead = ExpenseHead::where('id', $budgetDetails->expense_head_id)->where('status', '=', 1)->first();
                                        $activityReportDetails = new ActivityReportDetail();
                                        $activityReportDetails->activity_report_id = $activityReport->id;
                                        $activityReportDetails->activity_id = $detail['activity_id'];
                                        $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                        $activityReportDetails->expense = $detail['amount'];
                                        $activityReportDetails->voucher_id = $voucher_id;
                                        $activityReportDetails->month = $finalmonth;
                                        $activityReportDetails->fiscal_year = $fiscalYear->id;
                                        $activityReportDetails->save();
                                    }
                                }
                            }
                        }
                    } elseif ($lessExp and !$overExp) {
                        foreach ($voucherCollection as $detail) {

                            if ($detail['drOrCr'] == 1 and $detail['ledger_type_id'] == 3) {

                                $data['income'] = $detail['amount'];
                                $activityReport = ActivityReport::where('activity_id', $detail['activity_id'])->first();
                                $total_expense = $activityReport->expense = $activityReport->expense - $detail['amount'];
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $budgetDetails = BudgetDetails::findorfail($detail['activity_id']);
                                $expenseHead = ExpenseHead::where('id', $budgetDetails->expense_head_id)->where('status', 1)->first();
                                $activityReportDetails = new ActivityReportDetail();
                                $activityReportDetails->activity_report_id = $activityReport->id;
                                $activityReportDetails->activity_id = $activityReport->activity_id;
                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                $activityReportDetails->expense = -$detail['amount'];
                                $activityReportDetails->voucher_id = $voucher_id;
                                $activityReportDetails->month = $finalmonth;
                                $activityReportDetails->fiscal_year = $fiscalYear->id;
                                $activityReportDetails->save();
                            }
                        }
                    }
                } else {
                    $peski = $voucherCollection->where('drOrCr', '=', 1)->where('ledger_type_id', '=', 4);
                    if ($peski->count() > 0) {
                        foreach ($voucherCollection as $attribute) {
                            $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', 1)->first();
                            if (($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 1) || ($attribute['drOrCr'] == 1 and $attribute['ledger_type_id'] == 4)) {

                                $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                                $total_expense = 0;
                                if ($activityReport) {
                                    $activityReport->expense = $total_expense = $activityReport->expense + (float)$attribute['amount'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activityReport->activity_id;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $attribute['amount'];
                                    $activityReportDetails->voucher_id = $voucher_id;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                            }

                            if ($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 1) {
                                $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                                $total_expense = 0;
                                if ($activityReport) {
                                    $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activityReport->activity_id;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $attribute['amount'] * (-1);
                                    $activityReportDetails->voucher_id = $voucher_id;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                            }
                        }
                    } else {
                        foreach ($voucherCollection as $attribute) {
                            $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', '=', 1)->first();

                            if ($attribute['drOrCr'] == 2 and $attribute['ledger_type_id'] == 1) {
                                $activityReport = ActivityReport::where('activity_id', $attribute['activity_id'])->first();
                                $total_expense = 0;
                                if ($activityReport) {
                                    $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['amount'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activityReport->activity_id;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $attribute['amount'] * (-1);
                                    $activityReportDetails->voucher_id = $voucher_id;
                                    $activityReportDetails->month = $finalmonth;
                                    $activityReportDetails->fiscal_year = $fiscalYear->id;
                                    $activityReportDetails->save();
                                }
                            }
                        }
                    }
                }
            }
        }
        return $voucher;
    }

    public function get_all_field_total($VoucherList, $datas)
    {
        $data = [];
        $data['cr_tsa'] = $VoucherList->where('dr_or_cr', 2)->where('ledger_type_id', 3)->sum('cr_amount');
        $data['nikasa'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 3)->sum('dr_amount');
        // dd($data['nikasa']);
        //        $data['expense'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 1)->sum('dr_amount');
        $data['dr_liability'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 2)->sum('dr_amount');
        $data['cr_liability'] = $VoucherList->where('dr_or_cr', 2)->whereIn('ledger_type_id', ['2', '8'])->sum('cr_amount');


        $data['expense'] = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('activity_report.office_id', $datas['office'])
            ->where('activity_report.budget_sub_head_id', $datas['budget_sub_head'])
            ->where('activity_report_details.month', '=', $datas['month'])->sum('activity_report_details.expense');

        $data['peski'] = $VoucherList->where('dr_or_cr', 1)->whereIn('ledger_type_id', [4])->sum('dr_amount');

        $data['advance_given'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 4)->sum('dr_amount');
        $data['last_year_advance'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 7)->sum('dr_amount');
        $data['advance_clearance'] = $VoucherList->where('dr_or_cr', 2)->where('ledger_type_id', 9)->sum('cr_amount');
        $data['last_year_clearance'] = $VoucherList->where('dr_or_cr', 2)->where('ledger_type_id', 12)->sum('cr_amount');
        return $data;
    }

    public function get_all_field_total_cahs_book_all_month($VoucherList, $datas, $month)
    {
        $data = [];
        $data['cr_tsa'] = $VoucherList->where('dr_or_cr', 2)->where('ledger_type_id', 3)->sum('cr_amount');
        $data['nikasa'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 3)->sum('dr_amount');

        //        $data['expense'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 1)->sum('dr_amount');
        $data['dr_liability'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 2)->sum('dr_amount');
        $data['cr_liability'] = $VoucherList->where('dr_or_cr', 2)->whereIn('ledger_type_id', ['2', '8'])->sum('cr_amount');


        $data['expense'] = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->where('vouchers.status', '=', 1)
            ->where('activity_report.office_id', $datas['office'])
            ->where('activity_report.budget_sub_head_id', $datas['budget_sub_head'])
            ->where('activity_report_details.month', '=', $month)->sum('activity_report_details.expense');

        $data['peski'] = $VoucherList->where('dr_or_cr', 1)->whereIn('ledger_type_id', [4])->sum('dr_amount');

        $data['advance_given'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 4)->sum('dr_amount');
        $data['last_year_advance'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 7)->sum('dr_amount');
        $data['advance_clearance'] = $VoucherList->where('dr_or_cr', 2)->where('ledger_type_id', 9)->sum('cr_amount');
        $data['last_year_clearance'] = $VoucherList->where('dr_or_cr', 2)->where('ledger_type_id', 12)->sum('cr_amount');
        return $data;
    }


    public function getApprovedDetailsByBudgetSubHead($data)
    {
        return $voucherDetails = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $data['office'])
            ->where('vouchers.status', 1)
            ->where('voucher_details.budget_sub_head_id', $data['budget_sub_head'])
            ->get();
    }

    public function get_advance($VoucherList)
    {
        $data = [];
        //        upa bhokta
        $granTotalPeski = 0;
        $data['upaBhokta'] = $VoucherList->whereIn('dr_or_cr', ['1', '2'])->whereIn('ledger_type_id', ['4', '9', '7', '12'])->where('party_type', 1);
        $peskiVoucherDetails = $data['upaBhokta']->where('dr_or_cr', 1)->whereIn('ledger_type_id', ['4', '7']);
        $clearanceVoucherDetails = $data['upaBhokta']->where('dr_or_cr', 2)->whereIn('ledger_type_id', ['9', '12']);
        $remain_peski['upaBhokta'] = [];
        $totalUpaBhoktaAdvance = 0;
        foreach ($peskiVoucherDetails as $peski) {

            $temp = [];
            $temp['id'] = $peski->id;
            $temp['journel_id'] = $peski->journel_id;
            $temp['date'] = $peski->voucher->data_nepali;
            $temp['voucher_no'] = $peski->voucher->jv_number;
            $temp['narration'] = $peski->voucher->short_narration;
            $temp['party_name'] = $peski->advancePayment->name_nep;
            $temp['expense_head_title'] = $peski->expense_head->expense_head_code;
            $temp['activity'] = $peski->mainActivity->sub_activity;
            $temp['baki'] = 0;
            $temp['last_year_baki'] = 0;
            $temp['total_peski'] = 0;
            if ($peski->ledger_type_id == 4) {
                $temp['peski'] = $peski->dr_amount;
                $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->where('ledger_type_id', 9)->sum('cr_amount');
                $remaining = $peski->dr_amount;
                if ($clearance_amount) {

                    $remaining = $remaining - $clearance_amount;
                }

                if ($peski->ledger_type_id == 4) {

                    $temp['baki'] = $remaining;
                }
            }

            if ($peski->ledger_type_id == 7) {

                $temp['last_year_peski'] = $peski->dr_amount;
                $clearance_amount = $clearanceVoucherDetails->whereIn('voucher_details_id', $peski->id)->where('ledger_type_id', 12)->sum('cr_amount');
                $remaining = $peski->dr_amount;
                if ($clearance_amount) {
                    $remaining = $remaining - $clearance_amount;
                }
                $temp['last_year_baki'] = $remaining;
            }

            $totalUpaBhoktaAdvance += $temp['total_peski'] = (float)$temp['baki'] + (float)$temp['last_year_baki'];
            $granTotalPeski += $temp['total_peski'] = (float)$temp['baki'] + (float)$temp['last_year_baki'];
            if ($temp['baki'] > 0 || $temp['last_year_baki'] > 0) {

                array_push($remain_peski['upaBhokta'], $temp);
            }
        }

        //        Thekedar
        $data['thekedar'] = $VoucherList->whereIn('dr_or_cr', ['1', '2'])->whereIn('ledger_type_id', ['4', '9', '7', '12'])->where('party_type', 2);
        $peskiVoucherDetails = $data['thekedar']->where('dr_or_cr', 1)->whereIn('ledger_type_id', ['4', '7']);
        $clearanceVoucherDetails = $data['thekedar']->where('dr_or_cr', 2)->whereIn('ledger_type_id', ['9', '12']);
        $remain_peski['thekedar'] = [];
        $totalThekedarAdvance = 0;
        foreach ($peskiVoucherDetails as $peski) {
            $temp = [];
            $temp['id'] = $peski->id;
            $temp['journel_id'] = $peski->journel_id;
            $temp['date'] = $peski->voucher->data_nepali;
            $temp['voucher_no'] = $peski->voucher->jv_number;
            $temp['narration'] = $peski->voucher->short_narration;
            if ($peski->mainActivity and $peski->mainActivity->sub_activity) {
                $temp['activity'] = $peski->mainActivity->sub_activity;
            }
            if ($peski->advancePayment) {
                $temp['party_name'] = $peski->advancePayment->name_nep;
            } else {
                $temp['party_name'] = '';
            }
            $temp['expense_head_title'] = $peski->expense_head->expense_head_code;
            $temp['baki'] = 0;
            $temp['last_year_baki'] = 0;
            $temp['total_peski'] = 0;
            if ($peski->ledger_type_id == 4) {
                $temp['peski'] = $peski->dr_amount;
                $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->where('ledger_type_id', 9)->sum('cr_amount');
                $remaining = $peski->dr_amount;
                if ($clearance_amount) {
                    $remaining = $remaining - $clearance_amount;
                }
                $temp['baki'] = $remaining;
            }
            if ($peski->ledger_type_id == 7) {

                $temp['last_year_peski'] = $peski->dr_amount;
                $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->where('ledger_type_id', 12)->sum('cr_amount');
                $remaining = $peski->dr_amount;
                if ($clearance_amount) {
                    $remaining = $remaining - $clearance_amount;
                }
                $temp['last_year_baki'] = $remaining;
            }
            $totalThekedarAdvance += (float)$temp['baki'] + (float)$temp['last_year_baki'];
            $granTotalPeski += $temp['total_peski'] = (float)$temp['baki'] + (float)$temp['last_year_baki'];
            if ($temp['baki'] > 0 || $temp['last_year_baki'] > 0) {

                array_push($remain_peski['thekedar'], $temp);
            }
        }

        //        sasthagat
        $data['sasthagat'] = $VoucherList->whereIn('dr_or_cr', ['1', '2'])->whereIn('ledger_type_id', ['4', '9', '7', '12'])->where('party_type', 4);
        $peskiVoucherDetails = $data['sasthagat']->whereIn('dr_or_cr', 1)->whereIn('ledger_type_id', ['4', '7']);
        $clearanceVoucherDetails = $data['sasthagat']->where('dr_or_cr', 2)->whereIn('ledger_type_id', ['9', '12']);
        $remain_peski['sasthagat'] = [];
        $totalSasthagatAdvance = 0;
        foreach ($peskiVoucherDetails as $peski) {

            $temp = [];
            $temp['id'] = $peski->id;
            $temp['journel_id'] = $peski->journel_id;
            $temp['date'] = $peski->voucher->data_nepali;
            $temp['voucher_no'] = $peski->voucher->jv_number;
            $temp['narration'] = $peski->voucher->short_narration;
            $temp['party_name'] = $peski->advancePayment->name_nep;
            $temp['expense_head_title'] = $peski->expense_head->expense_head_code;
            $temp['activity'] = $peski->mainActivity->sub_activity;
            $temp['baki'] = 0;
            $temp['last_year_baki'] = 0;
            $temp['total_peski'] = 0;
            if ($peski->ledger_type_id == 4) {

                $temp['peski'] = $peski->dr_amount;
                $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->where('ledger_type_id', 9)->sum('cr_amount');
                $remaining = $peski->dr_amount;
                if ($clearance_amount) {
                    $remaining = $remaining - $clearance_amount;
                }
                if ($peski->ledger_type_id == 4) {

                    $temp['baki'] = $remaining;
                }
            }

            if ($peski->ledger_type_id == 7) {

                $temp['last_year_peski'] = $peski->dr_amount;
                $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->where('ledger_type_id', 12)->sum('cr_amount');
                $remaining = $peski->dr_amount;
                if ($clearance_amount) {
                    $remaining = $remaining - $clearance_amount;
                }
                $temp['last_year_baki'] = $remaining;
            }
            $totalSasthagatAdvance += $temp['total_peski'] = (float)$temp['baki'] + (float)$temp['last_year_baki'];
            $granTotalPeski += $temp['total_peski'] = (float)$temp['baki'] + (float)$temp['last_year_baki'];
            if ($temp['baki'] > 0 || $temp['last_year_baki'] > 0) {

                array_push($remain_peski['sasthagat'], $temp);
            }
        }

        //        karmachari
        $data['karmachari'] = $VoucherList->whereIn('dr_or_cr', ['1', '2'])->whereIn('ledger_type_id', ['4', '9', '7', '12'])->where('party_type', 5);
        $peskiVoucherDetails = $data['karmachari']->where('dr_or_cr', 1)->whereIn('ledger_type_id', ['4', '7']);
        $clearanceVoucherDetails = $data['karmachari']->where('dr_or_cr', 2)->whereIn('ledger_type_id', ['9', '12']);
        $remain_peski['karmachari'] = [];
        $totalKarmachariAdvance = 0;
        foreach ($peskiVoucherDetails as $peski) {

            $temp = [];
            $temp['id'] = $peski->id;
            $temp['journel_id'] = $peski->journel_id;
            $temp['date'] = $peski->voucher->data_nepali;
            $temp['voucher_no'] = $peski->voucher->jv_number;
            $temp['narration'] = $peski->voucher->short_narration;
            $temp['party_name'] = $peski->advancePayment->name_nep;
            $temp['expense_head_title'] = $peski->expense_head->expense_head_code;
            $temp['activity'] = $peski->mainActivity->sub_activity;
            $temp['baki'] = 0;
            $temp['last_year_baki'] = 0;
            $temp['total_peski'] = 0;
            if ($peski->ledger_type_id == 4) {
                $temp['peski'] = $peski->dr_amount;
                $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->where('ledger_type_id', 9)->sum('cr_amount');
                $remaining = $peski->dr_amount;
                if ($clearance_amount) {
                    $remaining = $remaining - $clearance_amount;
                }
                $temp['baki'] = $remaining;
            }

            if ($peski->ledger_type_id == 7) {
                $temp['last_year_peski'] = $peski->dr_amount;
                $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->where('ledger_type_id', 12)->sum('cr_amount');
                $remaining = $peski->dr_amount;
                if ($clearance_amount) {
                    $remaining = $remaining - $clearance_amount;
                }
                $temp['last_year_baki'] = $remaining;
            }

            $totalKarmachariAdvance += $temp['total_peski'] = (float)$temp['baki'] + (float)$temp['last_year_baki'];
            $granTotalPeski += $temp['total_peski'] = (float)$temp['baki'] + (float)$temp['last_year_baki'];
            if ($temp['baki'] > 0 || $temp['last_year_baki'] > 0) {
                array_push($remain_peski['karmachari'], $temp);
            }
        }

        //        Personal
        $data['byaktigat'] = $VoucherList->whereIn('dr_or_cr', ['1', '2'])->whereIn('ledger_type_id', ['4', '9', '7', '12'])->where('party_type', 3);
        $peskiVoucherDetails = $data['byaktigat']->where('dr_or_cr', 1)->whereIn('ledger_type_id', ['4', '7']);
        $clearanceVoucherDetails = $data['byaktigat']->where('dr_or_cr', 2)->whereIn('ledger_type_id', ['9', '12']);
        $remain_peski['byaktigat'] = [];
        $totalByaktigatAdvance = 0;
        foreach ($peskiVoucherDetails as $peski) {
            $temp = [];
            $temp['id'] = $peski->id;
            $temp['journel_id'] = $peski->journel_id;
            $temp['date'] = $peski->voucher->data_nepali;
            $temp['voucher_no'] = $peski->voucher->jv_number;
            $temp['narration'] = $peski->voucher->short_narration;
            $temp['party_name'] = $peski->advancePayment->name_nep;
            $temp['expense_head_title'] = $peski->expense_head->expense_head_code;
            $temp['activity'] = $peski->mainActivity->sub_activity;
            $temp['baki'] = 0;
            $temp['last_year_baki'] = 0;
            $temp['total_peski'] = 0;
            if ($peski->ledger_type_id == 4) {
                $temp['peski'] = $peski->dr_amount;
                $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->where('ledger_type_id', 9)->sum('cr_amount');
                $remaining = $peski->dr_amount;
                if ($clearance_amount) {
                    $remaining = $remaining - $clearance_amount;
                }
                if ($peski->ledger_type_id == 4) {

                    $temp['baki'] = $remaining;
                }
            }

            if ($peski->ledger_type_id == 7) {

                $temp['last_year_peski'] = $peski->dr_amount;
                $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->where('ledger_type_id', 12)->sum('cr_amount');
                $remaining = $peski->dr_amount;
                if ($clearance_amount) {
                    $remaining = $remaining - $clearance_amount;
                }
                $temp['last_year_baki'] = $remaining;
            }
            $totalByaktigatAdvance += $temp['total_peski'] = (float)$temp['baki'] + (float)$temp['last_year_baki'];

            $granTotalPeski += $temp['total_peski'] = (float)$temp['baki'] + (float)$temp['last_year_baki'];

            if ($temp['baki'] > 0 || $temp['last_year_baki'] > 0) {

                array_push($remain_peski['byaktigat'], $temp);
            }
        }
        $remain_peski['gran_total'] = $granTotalPeski;
        $remain_peski['total_karmachari_peski'] = $totalKarmachariAdvance;
        $remain_peski['total_upabhokta_peski'] = $totalUpaBhoktaAdvance;
        $remain_peski['total_sasthagat_peski'] = $totalSasthagatAdvance;
        $remain_peski['total_byagtigat_peski'] = $totalByaktigatAdvance;
        $remain_peski['total_thekedar_peski'] = $totalThekedarAdvance;
        return $remain_peski;
    }


    public function report_by_month($data)
    {
        $voucherDetail = VoucherDetail::query();
        if (array_key_exists('month', $data)) {
            $voucherDetail->where('month', $data['month']);
        }
        if (array_key_exists('less_than_this_month', $data)) {
            $voucherDetail->where('month', '<', $data['less_than_this_month']);
        }
        if (array_key_exists('up_to_this_month', $data)) {
            $voucherDetail->where('month', '<=', $data['up_to_this_month']);
        }
        if (array_key_exists('year', $data)) {
            $voucherDetail->where('year', $data['year']);
        }
        if (array_key_exists('fiscal_year', $data)) {
            $voucherDetail->where('fiscal_year', $data['fiscal_year']);
        }
        if (array_key_exists('budget_sub_head_id', $data)) {
            $voucherDetail->where('budget_sub_head_id', $data['budget_sub_head_id']);
        }


        $voucherDetails = $voucherDetail->groupBy('expense_head_id')->selectRaw('*, sum(cr_amount) as total_cr')->get();
        return $voucherDetails;
    }

    public function search($data)
    {
        // dd($data);
        $voucherDetail = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', '=', 1)->where('vouchers.office_id', $data['office_id']);

        if (array_key_exists('less_than_this_month', $data)) {
            $voucherDetail->where('vouchers.month', '<', $data['less_than_this_month']);
        }

        if (array_key_exists('month', $data)) {
            $voucherDetail->where('vouchers.month', '=', $data['month']);
        }

        if (array_key_exists('up_to_this_month', $data)) {
            $voucherDetail->where('vouchers.month', '<=', $data['up_to_this_month']);
        }

        if (array_key_exists('year', $data)) {
            $voucherDetail->where('year', $data['year']);
        }

        if (array_key_exists('fiscalYear', $data)) {
            $voucherDetail->where('vouchers.fiscal_year', $data['fiscalYear']);
        }

        if (array_key_exists('budget_sub_head', $data)) {
            $voucherDetail->where('vouchers.budget_sub_head_id', $data['budget_sub_head']);
        }

        return $voucherDetail->get();
    }

    public function searchByGroupBy($data)
    {
        //        use query if this does not done

        $voucherDetail = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1);


        if (array_key_exists('month', $data)) {
            $voucherDetail->where('voucher_details.month', $data['month']);
        }

        //        if (array_key_exists('status', $data)) {
        //            $voucherDetail->where('status', $data['status']);
        //        }

        if (array_key_exists('less_than_this_month', $data)) {
            $voucherDetail->where('voucher_details.month', '<', $data['less_than_this_month']);
        }
        if (array_key_exists('up_to_this_month', $data)) {
            $voucherDetail->where('voucher_details.month', '<=', $data['up_to_this_month']);
        }

        if (array_key_exists('monthFrom', $data)) {
            $voucherDetail->whereBetween('voucher_details.month', [$data['monthFrom'], $data['monthTo']]);
        }
        //
        //        if (array_key_exists('year', $data)) {
        //            $voucherDetail->where('year', $data['year']);
        //        }
        if (array_key_exists('fiscalYear', $data)) {
            $voucherDetail->where('voucher_details.fiscal_year', $data['fiscalYear']);
        }
        if (array_key_exists('budget_sub_head', $data)) {
            $voucherDetail->where('voucher_details.budget_sub_head_id', $data['budget_sub_head']);
        }


        return $voucherDetail->get();
    }

    public function get_expense_by_activity_id($activity_id)
    {

        return $total_expense_of_activity = VoucherDetail::where('main_activity_id', $activity_id)->where('dr_or_cr', 1)->where('ledger_type_id', 1)->sum('dr_amount');
    }

    public function get_advance_by_activity_id($activity_id)
    {
        return $total_expense_of_activity = VoucherDetail::where('main_activity_id', $activity_id)->where('dr_or_cr', 1)->where('ledger_type_id', 4)->sum('dr_amount');
    }

    public function getCuttingLiability($activityId, $expenseHeadId)
    {
        return $cuttingLiability = VoucherDetail::where('main_activity_id', $activityId)
            ->where('expense_head_id', $expenseHeadId)
            ->where('dr_or_cr', 2)
            ->where('ledger_type_id', 2)->sum('cr_amount');
    }

    public function getdepositLiability($activityId, $expenseHeadId)
    {
        return $cuttingLiability = VoucherDetail::where('main_activity_id', $activityId)
            ->where('main_activity_id', $activityId)
            ->where('expense_head_id', $expenseHeadId)
            ->where('dr_or_cr', '=', 1)
            ->where('ledger_type_id', 2)->sum('dr_amount');
    }

    public function get_all_voucher_by_group_by_expense_head($budget_sub_head_id, $fiscalYear)
    {
        $office_id = Auth::user()->office_id;
        return $budgetByExpenseHead = Budget::where('office_id', $office_id)
            ->where('budget_sub_head', $budget_sub_head_id)
            ->where('fiscal_year', $fiscalYear)
            ->groupBy('expense_head')
            ->orderBy('expense_head')
            ->get();
    }

    public function get_expense_in_budget_for_ministry($budget_sub_head_id, $fiscalYear, $office_id)
    {
        return $budgetByExpenseHead = Budget::where('office_id', $office_id)
            ->where('budget_sub_head', $budget_sub_head_id)
            ->groupBy('expense_head')
            ->orderBy('expense_head')
            ->get();
    }

    public function get_expense_in_budget_by_ministry($ministry_id)
    {
        return $budgetByExpenseHead = Budget::join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->groupBy('expense_head')
            ->orderBy('expense_head')
            ->get();
    }

    public function delete_by_id($voucher_details_id)
    {
        return $delete_voucher_details = VoucherDetail::where('id', $voucher_details_id)->delete();
    }

    public function get_liability_expenses_by_liability_id($data)
    {
        $office_id = Auth::user()->office_id;
        return $liabilityExpenseList = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $data['fiscal_year'])
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', $data['program'])
            ->where('voucher_details.expense_head_id', $data['khata'])
            ->get();
    }

    public function getLiabilityHeadsByBudgetSubHead($data)
    {
        $office_id = Auth::user()->office_id;
        return $liabilityExpenseList = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select([
                'voucher_details.id',
                'voucher_details.expense_head_id',
                DB::raw("SUM(dr_amount) as total_dr_amount"),
                DB::raw("SUM(cr_amount) as total_cr_amount"),
            ])
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $data['fiscal_year'])
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', $data['program'])
            ->where('voucher_details.ledger_type_id', 2)
            ->groupBy('expense_head_id')
            ->get();
    }

    public function getPeskiDetailsByParty($data)
    {
        $office_id = Auth::user()->office_id;
        return $peskiDetails = VoucherDetail::where('office_id', $office_id)
            ->where('budget_sub_head_id', $data['program'])
            ->where('peski_paune_id', $data['khata'])
            ->get();
    }

    public function gettotalProvinceChaluExpense($ministry_id)
    {
        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('budget.source_type', 2)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function gettotalProvincePujiExpense($ministry_id)
    {
        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('budget.source_type', 2)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function gettotalProvinceExpense($ministry_id)
    {
        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalFederalChaluExpense($ministry_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('budget.source_type', 1)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalFederalPujiExpense($ministry_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('budget.source_type', 1)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalFederalExpense($ministry_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('budget.source_type', 1)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpense($ministry_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }


    public function totalProvinceChaluExpenseByOffice($office_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->select('voucher_details.*')
            ->where('budget.source_type', 2)
            ->where('voucher_details.office_id', $office_id)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function totalFederalChaluExpenseByOffice($office_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->select('voucher_details.*')
            ->where('budget.source_type', 1)
            ->where('voucher_details.office_id', $office_id)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function totalProvincePujiExpenseByOffice($office_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $office_id)
            ->where('budget.source_type', 2)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function totalFederalPujiExpenseByOffice($office_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $office_id)
            ->where('budget.source_type', 1)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }


    //    Annual Report Source Wise

    public function getTotalExpenseByOffice($office_id)
    {
        return $expense = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->select('activity_report_details.*')
            ->where('vouchers.status', '=', 1)
            ->where('activity_report.office_id', '=', $office_id)
            ->sum('activity_report_details.expense');
    }

    public function getTotalExpenseByMinistry($ministry_id)
    {

        return $expense = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('offices', 'activity_report.office_id', '=', 'offices.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->select('activity_report_details.*')
            ->where('vouchers.status', '=', 1)
            ->where('ministries.id', '=', $ministry_id)
            ->sum('activity_report_details.expense');
    }


    public function getTotalExpenseUpToYesterday($ministry_id)
    {
        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function gettotalTodayExpense($ministry_id)
    {
        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->where('voucher_details.date_english', '=', $today)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpenseUpToToday($ministry_id)
    {
        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->where('voucher_details.date_english', '<=', $today)
            ->where('voucher_details.dr_or_cr', 1)
            ->where('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalAdvance($ministry_id)
    {
        return $expense = DB::table('voucher_details')
            ->join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('dr_amount');
    }


    //    Office wise expense
    public function getTotalExpenseUpToYesterdayByOffice($office_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpenseTodayByOffice($office_id)
    {
        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.date_english', '=', $today)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpenseUpToTodayByOffice($office_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.date_english', '<=', $today)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalAdvanceByOffice($office_id)
    {
        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('voucher_details.dr_amount');
    }


    //    Budget Sub Head Wise

    public function getTotalExpenseByBudgetSubHead($office_id, $budget_sub_head_id)
    {

        return $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->where('vouchers.status', 1)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head_id)
            ->sum('activity_report_details.expense');
    }


    public function getTotalExpenseUpToYesterdayByBudgetSubHead($office_id, $budget_sub_head_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpenseTodayByBudgetSubHead($office_id, $budget_sub_head_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.date_english', '=', $today)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpenseUpToTodayByBudgetSubHead($office_id, $budget_sub_head_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.date_english', '<=', $today)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalAdvanceByByBudgetSubHead($office_id, $budget_sub_head_id)
    {

        return $totalExpense = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', $office_id)
            ->where('voucher_details.budget_sub_head_id', $budget_sub_head_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('voucher_details.dr_amount');
    }

    public function getFirstChaumasikTotalExpense($office_id, $budgetSubHead, $fiscal_year)
    {
        return $firstQuarterExpense = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->select('activity_report_details.*')
            ->where('vouchers.status', 1)
            ->where('activity_report.office_id', '=', $office_id)
            ->where('activity_report.budget_sub_head_id', '=', $budgetSubHead)
            ->whereBetween('activity_report_details.month', [2, 4])
            ->sum('activity_report_details.expense');
    }

    public function getSecondChaumasikTotalExpense($office_id, $budgetSubHead, $fiscal_year)
    {

        return $firstQuarterExpense = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->select('activity_report_details.*')
            ->where('vouchers.status', 1)
            ->where('activity_report.office_id', '=', $office_id)
            ->where('activity_report.budget_sub_head_id', '=', $budgetSubHead)
            ->whereBetween('activity_report_details.month', [5, 8])
            ->sum('activity_report_details.expense');
    }

    public function getThirdChaumasikTotalExpense($office_id, $budgetSubHead, $fiscal_year)
    {

        return $firstQuarterExpense = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->select('activity_report_details.*')
            ->where('vouchers.status', 1)
            ->where('activity_report.office_id', '=', $office_id)
            ->where('activity_report.budget_sub_head_id', '=', $budgetSubHead)
            ->whereBetween('activity_report_details.month', [9, 12])
            ->sum('activity_report_details.expense');
    }

    public function getTotalAdvanceByActivity($activity_id, $party_id, $budget_sub_head_id)
    {
        $office_id = Auth::user()->office_id;
        return $expenseOnBudgetSubHead = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            //            ->where('voucher_details.main_activity_id', '=', $activity_id)
            ->where('voucher_details.peski_paune_id', '=', $party_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('voucher_details.dr_amount');
    }


    public function getTotalClearedAdvanceByActivity($budget_sub_head_id, $activity_id, $party_id)
    {

        $office_id = Auth::user()->office_id;
        return $expenseOnBudgetSubHead = VoucherDetail::join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', '=', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.main_activity_id', '=', $activity_id)
            ->where('voucher_details.peski_paune_id', '=', $party_id)
            ->where('voucher_details.dr_or_cr', '=', 2)
            ->where('voucher_details.ledger_type_id', '=', 9)
            ->sum('voucher_details.dr_amount');
    }

    public function getVouchersByBudgetSubHeadAndParty($datas)
    {
        if ($datas['activity_id']) {
            if ($datas['byahora'] == 9) {
                $voucherDetails = VoucherDetail::where('budget_sub_head_id', $datas['budget_sub_head_id'])
                    ->where('main_activity_id', $datas['activity_id'])
                    ->where('peski_paune_id', $datas['party_id'])
                    ->whereIn('dr_or_cr', ['1', '2'])
                    ->whereIn('ledger_type_id', ['4', '9'])
                    ->get();
            } else {
                $voucherDetails = VoucherDetail::where('budget_sub_head_id', $datas['budget_sub_head_id'])
                    ->where('main_activity_id', $datas['activity_id'])
                    ->where('peski_paune_id', $datas['party_id'])
                    ->whereIn('dr_or_cr', ['1', '2'])
                    ->whereIn('ledger_type_id', ['7', '12'])
                    ->get();
            }


            $peskiVoucherDetails = $voucherDetails->where('dr_or_cr', '=', 1)->where('ledger_type_id', '=', 4);
            $clearanceVoucherDetails = $voucherDetails->where('dr_or_cr', '=', 2)->where('ledger_type_id', '=', 9);
            $voucher_details = [];
            if ($peskiVoucherDetails->count() > 0) {
                foreach ($peskiVoucherDetails as $peski) {
                    $temp = [];
                    $temp['id'] = $peski->id;
                    $temp['date'] = $peski->voucher->data_nepali;
                    $temp['voucher_no'] = $peski->voucher->jv_number;
                    $temp['activity'] = $peski->mainActivity->sub_activity;
                    $temp['peski'] = $peski->dr_amount;
                    $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->sum('cr_amount');
                    $remaining = $peski->dr_amount;
                    if ($clearance_amount) {
                        $remaining = $remaining - $clearance_amount;
                    }
                    $temp['baki'] = $remaining;
                    array_push($voucher_details, $temp);
                }
            }
            $lastYearPeskiVoucherDetails = $voucherDetails->where('dr_or_cr', '=', 1)->where('ledger_type_id', '=', 7);
            $lastYearclearanceVoucherDetails = $voucherDetails->where('dr_or_cr', '=', 2)->where('ledger_type_id', '=', 12);
            if ($lastYearPeskiVoucherDetails->count() > 0) {
                foreach ($lastYearPeskiVoucherDetails as $peski) {
                    $temp = [];
                    $temp['id'] = $peski->id;
                    $temp['date'] = $peski->voucher->data_nepali;
                    $temp['voucher_no'] = $peski->voucher->jv_number;
                    $temp['activity'] = $peski->expense_head->expense_head_code;
                    $temp['peski'] = $peski->dr_amount;
                    $clearance_amount = $lastYearclearanceVoucherDetails->where('voucher_details_id', $peski->id)->sum('cr_amount');
                    $remaining = $peski->dr_amount;
                    if ($clearance_amount) {
                        $remaining = $remaining - $clearance_amount;
                    }
                    $temp['baki'] = $remaining;
                    array_push($voucher_details, $temp);
                }
            }
        } else {
            $voucherDetails = VoucherDetail::where('budget_sub_head_id', $datas['budget_sub_head_id'])
                ->where('peski_paune_id', $datas['party_id'])
                ->where('expense_head_id', $datas['expense_head_id'])
                ->whereIn('dr_or_cr', ['1', '2'])
                ->whereIn('ledger_type_id', ['7', '12'])
                ->get();
            $peskiVoucherDetails = $voucherDetails->where('dr_or_cr', '=', 1)->where('ledger_type_id', '=', 7);
            $clearanceVoucherDetails = $voucherDetails->where('dr_or_cr', '=', 2)->where('ledger_type_id', '=', 12);
            $voucher_details = [];
            if ($peskiVoucherDetails) {
                foreach ($peskiVoucherDetails as $peski) {

                    $temp = [];
                    $temp['id'] = $peski->id;
                    $temp['date'] = $peski->voucher->data_nepali;
                    $temp['voucher_no'] = $peski->voucher->jv_number;
                    $temp['activity'] = $peski->expense_head->expense_head_code;
                    $temp['peski'] = $peski->dr_amount;
                    $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->sum('cr_amount');
                    $remaining = $peski->dr_amount;
                    if ($clearance_amount) {
                        $remaining = $remaining - $clearance_amount;
                    }
                    $temp['baki'] = $remaining;
                    array_push($voucher_details, $temp);
                }
            }
        }
        return $voucher_details;
    }


    public function createBhuktaniVoucher($data, $voucher_id, $encodedBhuktaniId)
    {

        $voucherBhuktani = new BhuktaniVoucher();
        $voucherBhuktani->office_id = Auth::user()->office_id;
        $voucherBhuktani->budget_sub_head_id = $data['budget_sub_head'];
        $voucherBhuktani->voucher_id = $voucher_id;
        $voucherBhuktani->bhuktani_id = $encodedBhuktaniId;
        $voucherBhuktani->date_english = date('Y-m-d H:i:s');
        return $voucherBhuktani->save();
    }


    //    Update budget_id by budget_details_id
    public function UpdateMainActivityId($activity_id, $budget_detail_id)
    {

        $voucher_details = VoucherDetail::where('main_activity_id', $activity_id)->get();
        foreach ($voucher_details as $voucher_detail) {
            $voucher_detail->main_activity_id = $budget_detail_id;
            $voucher_detail->save();
            echo "Succefully Updated";
            echo "Voucher Detail = " . $voucher_detail->id . ' from budget ' . $activity_id . ' to budget_detail_id = ' . $budget_detail_id;
            echo "===============================================================\r\n";
        }
        return true;
    }
}
