<?php

namespace App\Http\Controllers\Frontend;

use App\helpers\FiscalYearHelper;
use App\Http\Controllers\Controller;
use App\Models\ActivityReport;
use App\Models\ActivityReportDetail;
use App\Models\AllBank;
use App\Models\Bank;
use App\Models\BudgetDetails;
use App\Models\DarbandiSrot;
use App\Models\DarbandiType;
use App\Models\Designation;
use App\Models\ExpenseHead;
use App\Models\LedgerType;
use App\Models\Medium;
use App\Models\Office;
use App\Models\PartyTypes;
use App\Models\Province;
use App\Models\Samuha;
use App\Models\Sewa;
use App\Models\SourceType;
use App\Models\Taha;
use App\Models\VatOffice;
use App\Models\Voucher;
use App\Models\VoucherDetail;
use App\Repositories\PreBhuktaniRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\VoucherDetailsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VoucherDetailsController extends Controller
{


    private $voucherDetailsRepo;
    private $programRepo;
    private $preBuktaniRrepo;
    public function __construct(VoucherDetailsRepository $voucherDetailsRepo, ProgramRepository $programRepo, PreBhuktaniRepository $preBuktaniRrepo)
    {
        $this->middleware('auth');
        $this->voucherDetailsRepo = $voucherDetailsRepo;
        $this->programRepo = $programRepo;
        $this->preBuktaniRrepo = $preBuktaniRrepo;
        $this->fiscalYearHelper = new FiscalYearHelper();
    }

    public function get_expense_head_in_voucher_details_by_budget_sub_head_id(Request $request)
    {
        $budget_sub_head_id = $request['budget_sub_head_id'];
        $ledger_type_id = $request['ledger_type_id'];
        $expenseHeadList = $this->voucherDetailsRepo->get_expense_head_by_budget_sub_head_and_office_id($budget_sub_head_id, $ledger_type_id);
        return json_encode($expenseHeadList);
    }

    //voucher
    public function get_voucher_details_by_voucher_id(Request $request, $voucher_id)
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $provinces = Province::all();
        $offices = Office::all();
        $darbandisrots = DarbandiSrot::all();
        $darbanditypes = DarbandiType::all();
        $sewas = Sewa::all();
        $samuhas = Samuha::all();
        $pads = Designation::all();
        $tahas = Taha::all();

        $sources = SourceType::all();
        $ledgerTypes = LedgerType::all();
        $programs = $this->programRepo->get_program_for_voucher();
        $expenseHeads = ExpenseHead::all();
        $mediums = Medium::all();

        //        for advance payment modal
        $office_id = Auth::user()->office_id;
        $party_types = PartyTypes::all();
        $all_banks = AllBank::all();
        $vat_offices = VatOffice::all();

        $sources = SourceType::all();
        $ledgerTypes = LedgerType::all();
        $programs = $this->programRepo->get_by_office_id();
        $expenseHeads = ExpenseHead::all();
        $mediums = Medium::all();
        $preBhuktanies = $this->preBuktaniRrepo->get_preBhuktani_by_voucher_id($voucher_id);
        $voucherDetailsLIst = $this->voucherDetailsRepo->get_details_by_voucher_id($voucher_id);
        $voucher = Voucher::findorfail($voucher_id);
        $all_banks = AllBank::all();


        return view('frontend.voucher.edit', compact('fiscalYear','fiscalYears','programs', 'expenseHeads', 'sources', 'mediums', 'ledgerTypes', 'voucherDetailsLIst', 'voucher', 'preBhuktanies', 'all_banks', 'all_banks', 'office_id', 'party_types', 'vat_offices', 'provinces', 'offices', 'darbandisrots', 'darbanditypes', 'sewas', 'samuhas', 'pads', 'tahas'));
    }

    public function get_expense_by_activity($activity_id)
    {
        $total_expense = $this->voucherDetailsRepo->get_expense_by_activity_id($activity_id);
        return json_encode($total_expense);
    }

    public function getLiabilityByLiabilityHead($activityId, $expenseHeadId)
    {
        $CuttingLiability = $this->voucherDetailsRepo->getCuttingLiability($activityId, $expenseHeadId);
        $depositLiability = $this->voucherDetailsRepo->getdepositLiability($activityId, $expenseHeadId);
        $remainLiability = floatval($CuttingLiability) - floatval($depositLiability);
        return json_encode($remainLiability);
    }

    public function voucherDetailsDeleteById($voucher_detail_id)
    {
        $voucher_detail = $this->voucherDetailsRepo->find($voucher_detail_id);
        $voucher_details = VoucherDetail::where('journel_id',$voucher_detail->journel_id)->get();
        $expense = $voucher_details->where('dr_or_cr', '=', 1)->where('ledger_type_id', '=', 1);
        if($expense->count() > 0){
            $pefa = $voucher_details->where('dr_or_cr', '=', 2)->where('ledger_type_id', '=', 9);
            if($pefa->count() > 0){
                $overExp = $voucher_details->contains(function ($value, $key) {
                    if ($value['dr_or_cr'] == 2 and $value['ledger_type_id'] == 3) {
                        return true;
                    }
                });
                $lessExp = $voucher_details->contains(function ($value, $key) {
                    if ($value['dr_or_cr'] == 1 and $value['ledger_type_id'] == 3) {
                        return true;
                    }
                });
                if ($overExp && $lessExp) {
                    if ($voucher_detail['dr_or_cr'] == 1 and $voucher_detail['ledger_type_id'] == 3) {
                        $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                        if ($activityReport->count() > 0) {
                            $total_expense = $activityReport->expense = $activityReport->expense + $voucher_detail->dr_amount;
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                ->where('expense','=',(-1)* $voucher_detail->dr_amount)
                                ->first();
                            if ($activityReportDetails) {
                                $activityReportDetails->delete();
                            }
                        }

                    }
                    if (($voucher_detail['dr_or_cr'] == 2 and $voucher_detail['ledger_type_id'] == 3) || ($voucher_detail['dr_or_cr'] == 2 and $voucher_detail['ledger_type_id'] == 2)) {
                        $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                        if($activityReport->count() > 0){
                            $total_expense = $activityReport->expense = $activityReport->expense - $voucher_detail->cr_amount;
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                ->where('expense',$voucher_detail->cr_amount)
                                ->first();
                            if ($activityReportDetails) {
                                $activityReportDetails->delete();
                            }
                        }
                    }
                }
                if ($overExp && !$lessExp) {
                    if (($voucher_detail['dr_or_cr'] == 2 and $voucher_detail['ledger_type_id'] == 3) || ($voucher_detail['dr_or_cr'] == 2 and $voucher_detail['ledger_type_id'] == 2)) {
                        $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                        if($activityReport->count() > 0){
                            $total_expense = $activityReport->expense = $activityReport->expense - $voucher_detail->cr_amount;
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                ->where('expense',$voucher_detail->cr_amount)
                                ->first();
                            if ($activityReportDetails) {
                                $activityReportDetails->delete();
                            }
                        }
                    }
                }
                if (!$overExp && $lessExp) {
                    if ($voucher_detail['dr_or_cr'] == 1 and $voucher_detail['ledger_type_id'] == 3) {
                        $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                        if ($activityReport->count() > 0) {
                            $total_expense = $activityReport->expense = $activityReport->expense + $voucher_detail->dr_amount;
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                ->where('expense','=',(-1)* $voucher_detail->dr_amount)
                                ->first();
                            if ($activityReportDetails) {
                                $activityReportDetails->delete();
                            }
                        }

                    }
                }
            }
            else {
                $adjustExpense = $voucher_details->where('dr_or_cr', '=', 2)->where('ledger_type_id', '=', 1);
                if($adjustExpense->count() > 0){
                    if ($voucher_detail->dr_or_cr == 1 and $voucher_detail->ledger_type_id == 1) {
                        $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                        $total_expense = 0;
                        if ($activityReport) {
                            $activityReport->expense = $total_expense = $activityReport->expense - (float)$voucher_detail->dr_amount;
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                ->where('expense',$voucher_detail->dr_amount)
                                ->first();
                            if ($activityReportDetails) {
                                $activityReportDetails->delete();

                            }
                        }
                    }
                    if ($voucher_detail->dr_or_cr == 2 and $voucher_detail->ledger_type_id == 1) {
                        $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                        $total_expense = 0;
                        if ($activityReport) {
                            $activityReport->expense = $total_expense = $activityReport->expense + (float)$voucher_detail->cr_amount;
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                ->where('expense',$voucher_detail->dr_amount)
                                ->first();
                            if ($activityReportDetails) {
                                $activityReportDetails->delete();
                            }
                        }
                    }
                }
                else {
                    if (($voucher_detail->dr_or_cr == 1 and $voucher_detail->ledger_type_id == 1) || ($voucher_detail->dr_or_cr == 1 and $voucher_detail->ledger_type_id == 4)) {
                        $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                        $total_expense = 0;
                        if ($activityReport) {
                            $currentExpense = $activityReport->expense;
                            $activityReport->expense = $total_expense = (float)$currentExpense - $voucher_detail->dr_amount;
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                ->where('expense',$voucher_detail->dr_amount)
                                ->first();
                            if ($activityReportDetails) {
                                $activityReportDetails->delete();
                            }
                        }
                    }
                    if ($voucher_detail->dr_or_cr == 2 and $voucher_detail->ledger_type_id == 1) {
                        $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                        $total_expense = 0;
                        if ($activityReport) {
                            $activityReport->expense = $total_expense = $activityReport->expense + (float)$voucher_detail->cr_amount;
                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                            $activityReport->save();

                            $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                ->where('expense',$voucher_detail->cr_amount)
                                ->first();
                            if ($activityReportDetails) {
                                $activityReportDetails->delete();
                            }
                        }
                    }
                }
            }
        }

    else {
            $adjustPeski = $voucher_details->where('dr_or_cr', '=', 2)->where('ledger_type_id', '=', 9);
            if($adjustPeski->count() > 0){
                if ($voucher_detail->dr_or_cr == 1 and $voucher_detail->ledger_type_id == 4) {
                    $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                    $total_expense = 0;
                    if ($activityReport) {
                        $currentExpense = $activityReport->expense;
                        $activityReport->expense = $total_expense = (float)$currentExpense - $voucher_detail->dr_amount;
                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                        $activityReport->save();

                        $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                            ->where('expense',$voucher_detail->dr_amount)
                            ->first();
                        if ($activityReportDetails) {
                            $activityReportDetails->delete();
                        }
                    }
                }
                if ($voucher_detail->dr_or_cr == 2 and $voucher_detail->ledger_type_id == 9) {
                    $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                    $total_expense = 0;
                    if ($activityReport) {
                        $activityReport->expense = $total_expense = $activityReport->expense + (float)$voucher_detail->cr_amount;
                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                        $activityReport->save();

                        $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                            ->where('expense',$voucher_detail->cr_amount)
                            ->first();
                        if ($activityReportDetails) {
                            $activityReportDetails->delete();
                        }
                    }
                }
            }
            else {
                $lastYearPefa = $voucher_details->where('dr_or_cr', '=', 2)->where('ledger_type_id', '=', 12);
                if($lastYearPefa->count()){
                    $overExp = $voucher_details->contains(function ($value, $key) {
                        if ($value['dr_or_cr'] == 2 and $value['ledger_type_id'] == 3) {
                            return true;
                        }
                    });
                    $lessExp = $voucher_details->contains(function ($value, $key) {
                        if ($value['dr_or_cr'] == 1 and $value['ledger_type_id'] == 3) {
                            return true;
                        }
                    });
                    if ($overExp && $lessExp) {
                        if ($voucher_detail['dr_or_cr'] == 1 and $voucher_detail['ledger_type_id'] == 3) {
                            $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                            if ($activityReport->count() > 0) {
                                $total_expense = $activityReport->expense = $activityReport->expense + $voucher_detail->dr_amount;
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                    ->where('expense','=',(-1)* $voucher_detail->dr_amount)
                                    ->first();
                                if ($activityReportDetails) {
                                    $activityReportDetails->delete();
                                }
                            }

                        }
                        if (($voucher_detail['dr_or_cr'] == 2 and $voucher_detail['ledger_type_id'] == 3) || ($voucher_detail['dr_or_cr'] == 2 and $voucher_detail['ledger_type_id'] == 2)) {
                            $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                            if($activityReport->count() > 0){
                                $total_expense = $activityReport->expense = $activityReport->expense - $voucher_detail->cr_amount;
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                    ->where('expense',$voucher_detail->cr_amount)
                                    ->first();
                                if ($activityReportDetails) {
                                    $activityReportDetails->delete();
                                }
                            }
                        }
                    }
                    if ($overExp && !$lessExp) {
                        if (($voucher_detail['dr_or_cr'] == 2 and $voucher_detail['ledger_type_id'] == 3) || ($voucher_detail['dr_or_cr'] == 2 and $voucher_detail['ledger_type_id'] == 2)) {
                            $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                            if($activityReport->count() > 0){
                                $total_expense = $activityReport->expense = $activityReport->expense - $voucher_detail->cr_amount;
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                    ->where('expense',$voucher_detail->cr_amount)
                                    ->first();
                                if ($activityReportDetails) {
                                    $activityReportDetails->delete();
                                }
                            }
                        }
                    }
                    if (!$overExp && $lessExp) {
                        if ($voucher_detail['dr_or_cr'] == 1 and $voucher_detail['ledger_type_id'] == 3) {
                            $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                            if ($activityReport->count() > 0) {
                                $total_expense = $activityReport->expense = $activityReport->expense + $voucher_detail->dr_amount;
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                    ->where('expense','=',(-1)* $voucher_detail->dr_amount)
                                    ->first();
                                if ($activityReportDetails) {
                                    $activityReportDetails->delete();
                                }
                            }

                        }
                    }
                }
                else {
                    $peski = $voucher_details->where('dr_or_cr', '=', 1)->where('ledger_type_id', '=', 4);
                    if($peski->count() > 0){
                        if ($voucher_detail->dr_or_cr == 1 and $voucher_detail->ledger_type_id == 4) {
                            $activityReport = ActivityReport::where('activity_id', $voucher_detail->main_activity_id)->first();
                            $total_expense = 0;
                            if ($activityReport) {
                                $currentExpense = $activityReport->expense;
                                $activityReport->expense = $total_expense = (float)$currentExpense - $voucher_detail->dr_amount;
                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                $activityReport->save();

                                $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_detail->journel_id)->where('activity_id', $voucher_detail->main_activity_id)
                                    ->where('expense',$voucher_detail->dr_amount)
                                    ->first();
                                if ($activityReportDetails) {
                                    $activityReportDetails->delete();
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($voucher_detail->count() > 0) {
            $delete_voucher_details = $this->voucherDetailsRepo->delete_by_id($voucher_detail_id);
        }
        return json_encode($delete_voucher_details);
    }

    public function getRemainPeski(Request $request)
    {
        $datas = $request->all();
        $voucherDetails = $this->voucherDetailsRepo->getVouchersByBudgetSubHeadAndParty($datas);
        return json_encode($voucherDetails);
    }

    public function getAlya($fiscalYear, $budgetSubHead){
        $alya = $this->voucherDetailsRepo->getAlyaByBudgetSubHead($fiscalYear,$budgetSubHead);
        return json_encode($alya);
    }


}
