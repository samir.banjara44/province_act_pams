@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                धरौटी खाता प्रविश्टी
                {{--        <small>Control panel</small>--}}
            </h1>
            <ul class="breadcrumb">
                <li><a type="button" href="{{route('retention.depositor')}}" class="btn btn-sm btn-primary"><i
                                class="fa fa-pencil"></i> सुची हेर्ने</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">

                    <form action="{{route('retention.depositor.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="party_type" style="display: block;">प्रकार</label>
                                    <select name="party_type" required class="form-control">
                                        @foreach($party_types as $party_type)
                                            @if($party_type->id != 5)
                                                <option value="{{$party_type->id}}">{{$party_type->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="name_nep">नाम नेपाली</label>
                                    <input type="text" class="form-control" id="name_nep" placeholder="नाम नेपाली"
                                           name="name_nep" required>
                                </div>
                                <div class="col-md-2">
                                    <label for="name_eng">नाम अग्रेजी</label>
                                    <input type="text" class="form-control" id="name_eng" placeholder="नाम अग्रेजी"
                                           name="name_eng">
                                </div>
                                <div class="col-md-2">
                                    <label for="citizen_number">नागरिकता न.</label>
                                    <input type="text" class="form-control" id="citizen_number"
                                           placeholder="नागरिकता न."
                                           name="citizen_number">
                                </div>
                                <div class="col-md-2">
                                    <label for="vat_number">भ्याट/प्यान न.</label>
                                    <input type="text" class="form-control" id="vat_number" placeholder="भ्याट/प्यान न."
                                           name="vat_number">
                                </div>
                                <div class="col-md-2">
                                    <label for="vat_office">स्थायी लेखा नम्वर जारी गर्ने कार्यालय र ठेगाना</label>
                                    <select id="vat_office" class="form-control select2" name="vat_office"
                                            style="width: 202px;">
                                        <option value="" selected="">.........</option>
                                        @foreach($vat_offices as $vat_office)
                                            <option value="{{$vat_office->id}}">{{$vat_office->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="mobile_number">मोबाइल न.</label>
                                    <input type="text" class="form-control" id="mobile_number" placeholder="मोबाइल न."
                                           name="mobile_number">
                                </div>
                                <div class="col-md-2">
                                    <label for="bank">बैङ्क</label>
                                    <select id="bank" name="bank" class="form-control select2" style="width: 202px;">
                                        <option value="" selected="">.........</option>
                                        @foreach($all_banks as $bank)
                                            <option value="{{$bank->id}}">{{$bank->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label>पेयी कोड</label>
                                    <input type="number" class="form-control" name="payee_code" id="payee_code">
                                </div>

                                <div class="col-md-2">
                                    <label>शाखा कार्यालय</label>
                                    <input type="text" class="form-control" name="bank_address" id="bank_address"
                                           value="">
                                </div>
                                <div class="col-md-2">
                                    <label>खाता न.</label>
                                    <input type="number" class="form-control" name="khata_number" id="khata_number">
                                </div>
                                <div class="col-md-2">
                                    <label for="is_advance">पेश्किमा देखाउने</label>
                                    <select name="is_advance" class="form-control">
                                        <option value="0">हो</option>
                                        <option value="2">होइन</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="is_bhuktani">भुक्तानीमा देखाउने</label>
                                    <select name="is_bhuktani" class="form-control">
                                        <option value="0">हो</option>
                                        <option value="2">होइन</option>

                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="is_dharauti">धरौटीमा देखाउने</label>
                                    <select name="is_dharauti" class="form-control">
                                        <option value="0">हो</option>
                                        <option value="2">होइन</option>
                                    </select>
                                </div>
                                {{--              <div class="clearfix"></div>--}}
                                <div class="col-md-3">
                                    <button type="submit" style="margin-top: 18px;" class="btn btn-primary btn-block">थप
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

