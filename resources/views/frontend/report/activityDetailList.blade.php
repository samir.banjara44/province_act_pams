<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">


<style>
    table {
        border-collapse: collapse;
    }

    @media print {
        #downloadMe {
            display: none;
        }
    }

    body tr:hover {
        background-color: #e1e1e1 !important;
    }
    body th {
        background: #e1e1e1 !important;
        position: sticky;
        top: 0;
        box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4);
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <input type="button" style="position: absolute; top:20px;right: 20px" value="Export to Excel" id="downloadMe">
    <section class="content-header">
        <div id="tableHeadingWrapper">
            <table width="99%" id="headingTable" style="font-size:13px">
                <tr>
                    <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                    <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
                        सभा @endif</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
                </tr>
                <tr>
                    <td colspan="6"
                        style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">
                        @if(Auth::user()->office->department)
                            {{Auth::user()->office->department->name}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}, , {{Auth::user()->office->district->name}}</td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div style="width: 100%; text-align: center">
                            <b>कार्यक्रम र कन्टेन्जेन्सी अनुसार बजेट बिनियोजन</b><br>
                        </div>
                        <div style="float: right; margin-top: -20px">

                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 15px">बजेट उपशिर्षक नाम : <b><span class="kalimati">{{$program->name}}</span></b><br>
                        बजेट उपशिर्षक न.: <b>{{$program->program_code}}</b></td>
                    <td style="text-align: right; padding-right: 20px">आर्थिक वर्ष : <span class="kalimati">{{$fiscalYear->year}}</span></td>

                </tr>
            </table>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body" id="tableWrapper">
                <table width="99%" border="1" id="contentTable" style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th rowspan="3" width="67px">सि.नं.</th>
                        <th rowspan="3" style="width: 28%;">कार्यक्रम/आयोजनाको नाम</th>
                        <th rowspan="3">कार्यक्रम कोड</th>
                        <th rowspan="3">खर्च शीर्षक</th>
                        <th colspan="2">विनियोजन रु.</th>
                        <th rowspan="3">जम्मा</th>
                        <th colspan="2">खर्च</th>
                        <th rowspan="3">जम्मा</th>
                        <th colspan="2">बाकींँ</th>
                        <th rowspan="3">जम्मा</th>
                    </tr>
                    <tr>
                        <th>योजनागत</th>
                        <th>कन्टेन्जेन्सी</th>
                        <th>योजनागत</th>
                        <th>कन्टेन्जेन्सी</th>
                        <th>योजनागत</th>
                        <th>कन्टेन्जेन्सी</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($activities as $index=>$activity)

                        <tr style="background-color: white">
                            <td class="kalimati" style="text-align: center">{{++$index}}</td>
                            <td>{{$activity->activity}}</td>
                            <td class="kalimati" align="center">{{$activity->activity_code}}</td>
                            <td class="kalimati" align="center">{{$activity->expense_head}}</td>
                            @if($activity->hasDetails()->count() > 1)
                                @foreach($activity->getDetail($activity->id) as $activityDetail)
                                    <td class="budget kalimati" data-ad-client=""
                                        style="text-align: right">{{number_format($activityDetail->total_budget,2)}}</td>
                                @endforeach
                            @else
                                <td class="budget kalimati"
                                    style="text-align: right">{{number_format($activity->total_budget,2)}}</td>
                                <td class="kalimati" style="text-align: right">0.00</td>
                            @endif
                            <td class="total_budget kalimati"
                                style="text-align: right">{{number_format($activity->total_budget,2)}}</td>

                            {{--expense--}}
                            <?php
                            $details = $activity->getDetail($activity->id);
                            ?>
                            @if($activity->hasDetails()->count() == 2)
                                @foreach($details as $activityDetail)
                                    <td class="expense kalimati"
                                        style="text-align: right">{{number_format($activityDetail->getActivityExpense(),2)}}
                                    </td>
                                @endforeach
                            @elseif($activity->hasDetails()->count() == 1)
                                <td class="expense kalimati"
                                    style="text-align: right">{{number_format($details[0]->getActivityExpense(),2)}}</td>
                                <td class="kalimati" style="text-align: right">0.00</td>
                            @endif
                            <td class=" total_expense kalimati"
                                style="text-align: right">{{number_format($activity->totalActivityExpense(),2)}}</td>

                            {{--                            बाकी--}}
                            @if($activity->hasDetails()->count() == 2)
                                @foreach($details as $activityDetail)
                                    <td class="remain_budget kalimati"
                                        style="text-align: right">{{number_format($activityDetail->getActivityRemain(),2)}}</td>
                                @endforeach
                            @elseif($activity->hasDetails()->count() == 1)
                                <td class="remain_budget kalimati"
                                    style="text-align: right">{{number_format($details[0]->getActivityRemain(),2)}}</td>
                                <td class="kalimati" style="text-align: right">0.00</td>
                            @endif
                            <td class="total_remain kalimati"
                                style="text-align: right">{{number_format($activity->totalActivityRemain(),2)}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="4" style="text-align: right"> कुल जम्मा</td>
                        <td class="kalimati" style="text-align: right"><span class="kalimati" id="yojana_total"></span>
                        </td>
                        <td class="kalimati" style="text-align: right"><span class="kalimati" id="contenjency_total"></span></td>
                        <td class="kalimati" style="text-align: right"><span class="kalimati" id="gran_total_budget"></span></td>
                        <td style="text-align: right"><span class="kalimati" id="yojana_expense"></span></td>
                        <td style="text-align: right"><span class="kalimati" id="contenjency_expense"></span></td>
                        <td style="text-align: right"><span class="kalimati" id="gran_total_expense"></span></td>
                        <td style="text-align: right"><span class="kalimati" id="gran_total_yojana_remain"></span></td>
                        <td style="text-align: right"><span class="kalimati" id="gran_tota_contenjency_remain"></span>
                        </td>
                        <td style="text-align: right"><span class="kalimati" id="gran_total_remain"></span></td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script>
    $(document).ready(function () {
        let trs = $('#contentTable tbody').find('tr').not(':last');
        let yojana = 0;
        let Totalyojana = 0;
        let contenjency = 0;
        let Totalcontenjency = 0;
        let TotalBudget = 0;

        let yojanaExpense = 0;
        let totalYojanaExpense = 0;
        let contenjencyExpense = 0;
        let totalcontenjencyExpense = 0;
        let totalExpense = 0;

        let yojanaRemain = 0;
        let contenjencyRemain = 0;
        let totalyojanaRemain = 0;
        let totalcontenjencyRemain = 0;
        let totalRemain = 0;
        $.each(trs, function () {

            let budgetTdsds = $(this).find('td.budget');
            $.each(budgetTdsds, function (index) {
                if (index == 0) {
                    yojana = $.trim($(this).text());
                    yojana = parseFloat(yojana.replace(/,/g, ""));
                    Totalyojana += yojana;
                }
                if (index == 1) {
                    contenjency = $.trim($(this).text());
                    contenjency = parseFloat(contenjency.replace(/,/g, ""));
                    Totalcontenjency += contenjency
                }
            });
            let totalBudgetTd = $.trim($(this).find('td.total_budget').text());
            totalBudgetTd = parseFloat(totalBudgetTd.replace(/,/g, ""));
            TotalBudget += totalBudgetTd;

            let expenseTds = $(this).find('td.expense');
            $.each(expenseTds, function (index) {
                if (index == 0) {

                    yojanaExpense = $.trim($(this).text());
                    yojanaExpense = parseFloat(yojanaExpense.replace(/,/g, ""));
                    totalYojanaExpense += yojanaExpense;
                }
                if (index == 1) {
                    contenjencyExpense = $.trim($(this).text());
                    contenjencyExpense = parseFloat(contenjencyExpense.replace(/,/g, ""));
                    totalcontenjencyExpense += contenjencyExpense
                }
            });

            let totalExpenseTd = $.trim($(this).find('td.total_expense').text());
            totalExpenseTd = parseFloat(totalExpenseTd.replace(/,/g, ""));
            totalExpense += totalExpenseTd;

            let remainTds = $(this).find('td.remain_budget');
            $.each(remainTds, function (index) {
                if (index == 0) {
                    yojanaRemain = $.trim($(this).text());
                    yojanaRemain = parseFloat(yojanaRemain.replace(/,/g, ""));
                    totalyojanaRemain += yojanaRemain;
                }
                if (index == 1) {
                    contenjencyRemain = $.trim($(this).text());
                    contenjencyRemain = parseFloat(contenjencyRemain.replace(/,/g, ""));
                    totalcontenjencyRemain += contenjencyRemain
                }
            });

            let totalRemainTd = $.trim($(this).find('td.total_remain').text());
            totalRemainTd = parseFloat(totalRemainTd.replace(/,/g, ""));
            totalRemain += totalRemainTd


        });
        $('#yojana_total').html(Totalyojana.toFixed(2));
        $('#contenjency_total').html(Totalcontenjency.toFixed(2));
        $('#gran_total_budget').html(TotalBudget.toFixed(2));
        $('#yojana_expense').html(totalYojanaExpense.toFixed(2));
        $('#contenjency_expense').html(totalcontenjencyExpense.toFixed(2));
        $('#gran_total_expense').html(totalExpense.toFixed(2));
        $('#gran_total_yojana_remain').html(totalyojanaRemain.toFixed(2));
        $('#gran_tota_contenjency_remain').html(totalcontenjencyRemain.toFixed(2));
        $('#gran_total_remain').html(totalRemain.toFixed(2));
    })
</script>

<script>
    let changeToNepali = function (text) {
        // alert(text);
        textTrim = $.trim(text);
        let numbers = textTrim.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (!isNaN(value) && value != ' ') {
                    if (value == 1)
                        nepaliNo += "१";
                    else if (value == 2)

                        nepaliNo += "२";
                    else if (value == 3)

                        nepaliNo += "३";
                    else if (value == 4)

                        nepaliNo += "४";
                    else if (value == 5)

                        nepaliNo += "५";
                    else if (value == 6)

                        nepaliNo += "६";
                    else if (value == 7)

                        nepaliNo += "७";
                    else if (value == 8)

                        nepaliNo += "८";
                    else if (value == 9)

                        nepaliNo += "९";
                    else if (value == 0)

                        nepaliNo += "०";
                    else if (value == ',')

                        nepaliNo += ",";
                    else if (value == '.')

                        nepaliNo += ".";
                    else if (value == '/')

                        nepaliNo += "/";

                }
            }
        });
        console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });


    var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
            ,
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta charset="utf-8"/><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
            , format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            };

        return function (table, name, filename) {
            var table1 = '';
            if (!table1.nodeType) table1 = document.getElementById('headingTable');
            if (!table.nodeType) table = document.getElementById(table);
            var bigTable = table1.innerHTML + table.innerHTML;
            var ctx = {worksheet: name || 'Worksheet', table: bigTable};

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })();

    $("#downloadMe").click(function (e) {
        let allTable = $('div[id$=tableHeadingWrapper]').html() + $('div[id$=tableWrapper]').html();
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(allTable));
        e.preventDefault();
    });

</script>