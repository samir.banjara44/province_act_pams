
@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        क्षेत्र प्रविश्टि
{{--        <small>Control panel</small>--}}
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('admin.area.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> थप्ने</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>Name</th>
              <th>Sub Areas</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($areas as $area)
              <tr>
                <td>{{$area->name}}</td>
                <td>
                  <ul>
                    @foreach($area->sub_areas as $sub_area)
                      <li>{{$sub_area->name}}</li>
                      @endforeach
                  </ul>
                </td>

                <td>
                  <a type="button" href="{{route('admin.program.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection