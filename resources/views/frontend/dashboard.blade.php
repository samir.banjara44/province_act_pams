@extends('frontend.layouts.app')

@section('title')
    प्रदेश लेखा व्यवस्थापन प्रणाली
@endsection

@section('styles')
<style>
    .subtitle {

        font-size: 17px;
        text-shadow: 2px 1px #fdcdcd;
        color: #295fae;

    }
    .title {

        font-size: 40px;
        color: #ff5454;

    }
    .content-wrapper {

        text-align: center;
        padding-top: 10%;

    }
    .footer {
        position: fixed;
        bottom: 0;
        width: 100%;
        height: 30px;
        background-color: #236286;
        text-align: center;
        padding: 5px;
        font-weight: 800;
        color: #fff;

    }
    .footer a{
       color: #fff;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="content-wrapper">
            <div class="title">प्रदेश लेखा व्यवस्थापन प्रणाली (PAMS)</div>

            <div class="title">Province Accounting Management System</div>
            <div class="subtitle">@if(Auth::user()->office){{Auth::user()->office->name}} @endif, {{Auth::user()->province->name}}, @if(Auth::user()->office and Auth::user()->office->district){{Auth::user()->office->district->name}}@endif</div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    {{--$(document).ready(function () {--}}
    {{--let office = '{!! Auth::user()->office->id !!}'--}}
    {{--    office = $.parseJSON(office);--}}
    {{--    if(office >= 9 && office <= 42){--}}
    {{--        alert("पेश्की अल्याको भौचर भोलि २०७६/११/१४ गते बाट उठाउनु होला। असमर्थताको लागि क्षमाप्राथि छौं !! ")--}}
    {{--    }--}}
    {{--})--}}

</script>
{{--    <script>--}}
{{--        $(document).ready(function () {--}}
{{--            alert("आ.व ७७/७८ को सेटअप सिस्टममा हालिएकोले ७६/७७ को काम गर्दा आर्थिक वर्ष छान्न नबिर्सनु होला!! नया सेटअपले सिस्टममा केही समस्या देखिएमा सपोर्ट सेन्टरलाई तुरुन्त सम्पर्क गर्नु होला!")--}}
{{--        })--}}
{{--    </script>--}}
@endsection