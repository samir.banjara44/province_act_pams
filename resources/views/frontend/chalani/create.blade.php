@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>

                <small>Chalani</small>
            </h1>
            <ul class="breadcrumb">
                <li><a type="button" href="{{route('chalani')}}" class="btn btn-sm btn-primary"><i
                                class="fa fa-pencil"></i> सुची हेर्ने</a></li>
            </ul>
        </section>

        <!-- FORM section start -->
        <section class="formSection displayNone" id="showThisForm">
            <div class="row justify-content-md-center">
                <div class="col-md-8 form-bg col-md-offset-2">
                    <form action="{{route('chalani.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="col-md-6">
                              <label for="date">मिति </label>
                              <input type="text" class="form-control" id="date" placeholder="मिति " name="date" required>
                            </div>
                            <div class="col-md-6">
                              <label for="chalani_number">चलानी न. </label>
                              <input type="text" class="form-control kalimati" id="chalani_number"  name="chalani_number" required disabled>
                            </div>
                            <div class="col-md-6">
                              <label for="name">पत्र सङ्ख्या</label>
                              <input type="text" class="form-control" id="letter_number" placeholder="" name="letter_number" required>
                            </div>
                            <div class="col-md-6">
                              <label for="chalan_date">चलान मिति </label>
                              <input type="text" class="form-control" id="chalan_date" placeholder="चलान मिति " name="date" required>
                            </div>
                            <label for="body">व्याहोरा </label>
                            <textarea class="form-control" id="body" name="body"></textarea>
                
                            <div class="col-md-6">
                              <label for="to_office">चलानी जाने कार्यालय</label>
                              <input type="text" class="form-control" id="to_office" placeholder="चलानी जाने कार्यालय" name="to_office" required>
                            </div>
                            <div class="col-md-6">
                              <label for="type">प्रकार</label>
                              <input type="text" class="form-control" id="type" placeholder="प्रकार" name="type" required>
                            </div>
                            <div class="col-md-6">
                              <label for="medium">पठाउने माध्यम</label>
                              <input type="text" class="form-control" id="medium" placeholder="पठाउने माध्यम" name="medium" required>
                            </div>
                
                            <div class="col-md-6">
                              <label for="address">चलानी जाने कार्यालय ठेगाना</label>
                              <input type="text" class="form-control" id="address" placeholder="चलानी जाने कार्यालय ठेगान" name="address" required>
                            </div>
                            <div class="col-md-6">
                              <label for="purpose">प्रायोजन</label>
                              <input type="text" class="form-control" id="purpose" placeholder="प्रायोजन" name="purpose" required>
                            </div>
                            <div class="form-group">
                              <label for="branch">शाखा</label>
                              <select name="branch" required class="branch" style="width:100%;" id="branch">
                                  <option>Select Branch:</option>
                                  @foreach($branch as $branch)
                                  <option value="{{ $branch->name_eng}}">{{ $branch->name_eng }}</option>
                                  @endforeach
                              </select> 
                          </div>
                
                
                            <div class="col-md-6">
                              <label for="remarks">कैफियत</label>
                              <textarea class="form-control" id="remarks" placeholder="कैफियत" name="remarks"></textarea>
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <div class="col-md-2 col-md-offset-5">
                                <button type="button" class="btn btn-primary btn-show-form btn-block" onclick="saveChalani()"
                                        style="margin-top: 18px;">थप
                                </button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    <!-- FORM section ends -->

@section('scripts')



    {{--  Date picker--}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });
        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);

    </script>

@endsection