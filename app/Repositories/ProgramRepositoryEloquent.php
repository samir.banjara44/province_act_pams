<?php

namespace App\Repositories;

use App\Models\PreBhuktani;
use App\Models\Programs;
use App\Models\Voucher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProgramRepositoryEloquent implements ProgramRepository
{

    public function getAllProgram()
    {
        $programs = Programs::all();
        return $programs;
    }

    public function getById($budget_sub_head_id)
    {
        return $budget_sub_head = Programs::findorfail($budget_sub_head_id);
    }


    public function create($attributes)
    {

        $program = new Programs();
        $program->name = $attributes['name'];
        $program->program_code = $attributes['program_code'];
        $program->expense_type = $attributes['expense_type'];
        $program->office_id = Auth::user()->office_id;
//       only for data import, above is correct for regular
//        $program->office_id = $attributes['office_id'];
        $program->status = 1;
        $program->save();
        return $program->id;
    }

    public function createImport($attributes)
    {

        $program = new Programs();
        $program->name = $attributes['name'];
        $program->program_code = $attributes['program_code'];
        $program->expense_type = $attributes['expense_type'];
//        $program->office_id = Auth::user()->office_id;
//       only for data import, above is correct for regular
        $program->office_id = $attributes['office_id'];
        $program->status = 1;
        $program->save();
        return $program->id;
    }

    public function get_by_program_id($program_id)
    {
        return $program = Programs::findorfail($program_id);
    }

    public function get_by_office_id()
    {

        $office_id = Auth::user()->office->id;
        return $programs = Programs::where('office_id', $office_id)->where('status', 1)->with('akhtiyari')->get();
    }

    public function get_by_office_id_for_ministry($office_id,$fiscal_year)
    {
        return $programs = Programs::join('budget','programs.id','=','budget.budget_sub_head')
            ->where('budget.fiscal_year',$fiscal_year)
            ->select('programs.*')
            ->where('programs.office_id', $office_id)
            ->where('programs.status', '=',1)
            ->groupBy('programs.id')
            ->get();
    }

    public function get_programs_for_budget()
    {
        $office_id = Auth::user()->office->id;
        $programs = Programs::select('programs.*')
            ->join('akhtiyari', 'akhtiyari.budget_sub_head_id', '=', 'programs.id')
            ->where('programs.office_id', $office_id)
            ->where('programs.status', '=',1)
            ->groupBy('id')
            ->orderBy('id')
            ->get();
        return $programs;
    }

    public function get_program_for_voucher()
    {

        $office_id = Auth::user()->office->id;
        $programs = Programs::select('programs.*')
            ->join('budget', 'budget.budget_sub_head', '=', 'programs.id')
            ->where(['programs.office_id' => $office_id, 'programs.status' => 1])
            ->groupBy('programs.id')
            ->get();
        return $programs;

    }

    public function get_program_for_report()
    {

        $office_id = Auth::user()->office->id;

//        2020/1/29 पछि तलको ले काम चल्यो भने यो delete गर्ने
//        $programs = Programs::select('programs.*')
//            ->join('vouchers','programs.id','=','vouchers.budget_sub_head_id')
//            ->where('programs.office_id',$office_id)
////            ->where('programs.status', 1)
////            ->orderBy('id')
//            ->groupBy('program_code')
//            ->get();
        return $program = Programs::where('office_id', $office_id)
            ->where('status', '=',1)
            ->with('voucher')
            ->orderBy('id')
            ->get();
    }

    public function update($attributes, $id)
    {
        $program = $this->get_by_program_id($id);
        $program->name = $attributes['name'];
        $program->program_code = $attributes['program_code'];
        $program->expense_type = $attributes['expense_type'];
        $program->save();
        return $program;
    }

    public function getBudgetSubHeadByMinistry($ministryId)
    {

        return $budgetSubHeadList = DB::table('programs')->join('offices', 'programs.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->select('programs.*')
            ->where('ministries.id', '=', $ministryId)
            ->get();

    }

}