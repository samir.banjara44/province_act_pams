<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ministry extends Model
{
    protected $table = 'ministries';
    protected $guarded = [];

    public function province(){
        return $this->belongsTo('App\Models\Province');
    }

    public function mof(){
        return $this->belongsTo('App\Models\Mof');
    }

    public function departments(){
        return $this->hasMany('App\Models\Department');
    }

    public function offices(){
        return $this->hasMany('App\Models\Office');
    }

    public function users(){
        return $this->hasMany('App\Users');
    }
}
