<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';
    protected $guarded = [];

    public function mofs(){
        return $this->belongsTo('App\Models\Mof','id','province_id');
    }

    public function ministries(){
        return $this->hasMany('App\Models\Ministries');
    }

    public function departments(){
        return $this->hasMany('App\Models\Department');
    }

    public function offices(){
        return $this->hasMany('App\Models\Office');
    }

    public function users(){
        return $this->hasMany('App\Users');
    }
}
