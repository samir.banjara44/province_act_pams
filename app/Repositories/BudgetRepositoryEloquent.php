<?php

namespace App\Repositories;

use App\Models\ActivityReport;
use App\Models\ActivityReportDetail;
use App\Models\Budget;
use App\Models\Akhtiyari;
use App\Models\BudgetDetails;
use App\Models\ExpenseHead;
use App\Models\Programs;
use App\Models\VoucherDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Null_;
use Psy\Util\Json;

class BudgetRepositoryEloquent implements BudgetRepository
{
    public function create($attributes)
    {

        $budget = new Budget();
        $budget->expense_center = $attributes['expense_center'];
        $budget->budget_sub_head = $attributes['budget_sub_head'];
        $budget->akhtiyari_type = $attributes['akhtiyari'];

        $budget->activity = $attributes['main_activity'];
        $budget->expense_head_id = $attributes['expense_head'];
        $budget->expense_head = $attributes['expense_head_code'];
//        $budget->target_group = $attributes['target_group'];
        $budget->unit = $attributes['unit'];
//        $budget->per_unit_cost = $attributes['per_unit_cost'];
        $budget->total_unit = $attributes['total_unit'];
        $budget->total_budget = (int)$attributes['total_budget'];
        $budget->first_quarter_unit = $attributes['first_quarter'];
        $budget->second_quarter_unit = $attributes['second_quarter'];
        $budget->third_quarter_unit = $attributes['third_quarter'];
        $budget->first_quarter_budget = $attributes['first_total_budget'];
        $budget->first_chaimasik_bhar = $attributes['firstChaumasikBharHidden'];
        $budget->second_quarter_budget = $attributes['second_total_budget'];
        $budget->second_chaimasik_bhar = $attributes['secondChaumasikBharHidden'];
        $budget->third_quarter_budget = $attributes['third_total_budget'];
        $budget->third_chaimasik_bhar = $attributes['thirdChaumasikBharHidden'];
        $budget->source_type = $attributes['source_type'];
        $budget->source_level = $attributes['source_level'];
        $budget->source = $attributes['source'];
        $budget->medium = $attributes['medium'];
        $budget->office_id = Auth::user()->office->id;
        $budget->user_id = Auth::user()->id;
        $budget->status = 1;
        $budget->fiscal_year = $attributes['fiscal_year'];
        if (array_key_exists('contenjency_check_box', $attributes)) {
            $budget->is_contenjency = 1;
            $budget->contenjency = $attributes['hidden_contenjency_percent'];
        } else {
            $budget->is_contenjency = 0;
            $budget->contenjency = 0;
        }
        $budget->date = date('Y-m-d H:i:s');
        $budget->save();

        $attributes['budget_id'] = $budget->id;

        if (array_key_exists('contenjency_check_box', $attributes)) {
            for ($x = 1; $x <= 2; $x++) {
                $budgetDetals = new BudgetDetails();
                $amount = $attributes['total_budget'] * ($attributes['hidden_contenjency_percent'] * 0.01);
                $sub_activity = $attributes['main_activity'] . " - " . "कन्टेन्जेन्सी";
                if ($x == 1) {
                    $remain = 100 - $attributes['hidden_contenjency_percent'];
                    $amount = $attributes['total_budget'] * ($remain * 0.01);
                    $sub_activity = $attributes['main_activity'];
                } else {
                    $budgetDetals->contenjency = $attributes['hidden_contenjency_percent'];
                }

                $budgetDetals->office_id = $attributes['expense_center'];
                $budgetDetals->budget_sub_head_id = $attributes['budget_sub_head'];
                $budgetDetals->budget_id = $attributes['budget_id'];
                $budgetDetals->sub_activity = $sub_activity;
                $budgetDetals->expense_head_id = $attributes['expense_head'];
                $budgetDetals->unit = $attributes['unit'];
                $budgetDetals->total_unit = $attributes['total_unit'];
                $budgetDetals->total_budget = (float)$amount;
                $budgetDetals->first_quarter_unit = $attributes['first_quarter'];
                $budgetDetals->second_quarter_unit = $attributes['second_quarter'];
                $budgetDetals->third_quarter_unit = $attributes['third_quarter'];
                if($attributes['first_total_budget'] != null){
                    $budgetDetals->first_quarter_budget = (float)$amount;
                }
                $budgetDetals->first_chaimasik_bhar = $attributes['firstChaumasikBharHidden'];
                if($attributes['second_total_budget'] != null){
                    $budgetDetals->second_quarter_budget = (float)$amount;
                }
                $budgetDetals->second_chaimasik_bhar = $attributes['secondChaumasikBharHidden'];
                if($attributes['third_total_budget'] != null){
                    $budgetDetals->third_quarter_budget = (float)$amount;
                }
                $budgetDetals->third_chaimasik_bhar = $attributes['thirdChaumasikBharHidden'];
                $budgetDetals->source_type = $attributes['source_type'];
                $budgetDetals->source_level = $attributes['source_level'];
                $budgetDetals->source = $attributes['source'];
                $budgetDetals->medium = $attributes['medium'];
                $budgetDetals->user_id = Auth::user()->id;
                $budgetDetals->status = 1;
                $budgetDetals->fiscal_year = $attributes['fiscal_year'];
                $budgetDetals->date = date('Y-m-d H:i:s');
                $budgetDetals->save();

//                activity report from here
                $expenseHead = ExpenseHead::where('id', $attributes['expense_head'])->where('status', 1)->first();

                $activityReport = new ActivityReport();
                $activityReport->office_id = Auth::user()->office_id;
                $activityReport->budget_sub_head_id = $attributes['budget_sub_head'];
                $activityReport->activity_id = $budgetDetals->id;
                $activityReport->budget = (float)$amount;
                $activityReport->expense_head = $expenseHead->expense_head_code;
                $activityReport->expense = 0;
                $activityReport->remain = (float)$amount;
                $activityReport->fiscal_year = $attributes['fiscal_year'];
                $activityReport->save();

                $activityReportDetail = new ActivityReportDetail();
                $activityReportDetail->activity_report_id = $activityReport->id;
                $activityReportDetail->activity_id = $budgetDetals->id;
                $activityReportDetail->budget = (float)$amount;
                $activityReportDetail->expense_head = $expenseHead->expense_head_code;
                $activityReportDetail->expense = 0;
                $activityReportDetail->akhtiyari_type = 1;
                $activityReportDetail->fiscal_year = $attributes['fiscal_year'];
                $activityReportDetail->save();

            }
        } else {

            $budgetDetals = new BudgetDetails();
            $budgetDetals->office_id = Auth::user()->office->id;
            $budgetDetals->budget_sub_head_id = $attributes['budget_sub_head'];
            $budgetDetals->budget_id = $attributes['budget_id'];
            $budgetDetals->sub_activity = $attributes['main_activity'];
            $budgetDetals->expense_head_id = $attributes['expense_head'];
            $budgetDetals->unit = $attributes['unit'];
            $budgetDetals->total_unit = $attributes['total_unit'];
            $budgetDetals->total_budget = $attributes['total_budget'];
            $budgetDetals->first_quarter_unit = $attributes['first_quarter'];
            $budgetDetals->second_quarter_unit = $attributes['second_quarter'];
            $budgetDetals->third_quarter_unit = $attributes['third_quarter'];
            $budgetDetals->first_quarter_budget = $attributes['first_total_budget'];
            $budgetDetals->first_chaimasik_bhar = $attributes['firstChaumasikBharHidden'];
            $budgetDetals->second_quarter_budget = $attributes['second_total_budget'];
            $budgetDetals->second_chaimasik_bhar = $attributes['secondChaumasikBharHidden'];
            $budgetDetals->third_quarter_budget = $attributes['third_total_budget'];
            $budgetDetals->third_chaimasik_bhar = $attributes['thirdChaumasikBharHidden'];
            $budgetDetals->source_type = $attributes['source_type'];
            $budgetDetals->source_level = $attributes['source_level'];
            $budgetDetals->source = $attributes['source'];
            $budgetDetals->medium = $attributes['medium'];
            $budgetDetals->user_id = Auth::user()->id;
            $budgetDetals->status = 1;
            $budgetDetals->fiscal_year = $attributes['fiscal_year'];
            $budgetDetals->date = date('Y-m-d H:i:s');
            $budgetDetals->save();

//            activity report from here
            $expenseHead = ExpenseHead::where('id', $attributes['expense_head'])->where('status', 1)->first();

            $activityReport = new ActivityReport();
            $activityReport->office_id = Auth::user()->office_id;
            $activityReport->budget_sub_head_id = $attributes['budget_sub_head'];
            $activityReport->activity_id = $budgetDetals->id;
            $activityReport->budget = $attributes['total_budget'];
            $activityReport->expense_head = $expenseHead->expense_head_code;
            $activityReport->expense = 0;
            $activityReport->remain = $attributes['total_budget'];
            $activityReport->fiscal_year = $attributes['fiscal_year'];
            $activityReport->save();

            $activityReportDetail = new ActivityReportDetail();
            $activityReportDetail->activity_report_id = $activityReport->id;
            $activityReportDetail->activity_id = $budgetDetals->id;
            $activityReportDetail->budget = $attributes['total_budget'];
            $activityReportDetail->expense_head = $expenseHead->expense_head_code;
            $activityReportDetail->expense = 0;
            $activityReportDetail->akhtiyari_type = 1;
            $activityReportDetail->fiscal_year = $attributes['fiscal_year'];
            $activityReportDetail->save();

        }
        return $budgetDetals;
    }


    public function createTemp($attributes)
    {

        $budget = new Budget();
        $budget->expense_center = $attributes['office_id'];
        $budget->budget_sub_head = $attributes['budget_sub_head_id'];
        $budget->akhtiyari_type = $attributes['akhtiyar_id'];
        $budget->activity = $attributes['activity_name'];
        $budget->expense_head_id = $attributes['expense_head_id'];
        $budget->expense_head = $attributes['expense_head_code'];
        $budget->unit = 1;
        $budget->total_unit = 1;
        $budget->total_budget = (float)$attributes['amount'];
        $budget->first_quarter_unit = NULL;
        $budget->second_quarter_unit = NULL;
        $budget->third_quarter_unit = 1;
        $budget->first_quarter_budget = NULL;
        $budget->first_chaimasik_bhar = NULL;
        $budget->second_quarter_budget = NULL;
        $budget->second_chaimasik_bhar = NULL;
        $budget->third_quarter_budget = (float)$attributes['amount'];
        $budget->third_chaimasik_bhar = NULL;
        $budget->source_type = $attributes['source_type'];
        $budget->source_level = $attributes['source_level'];
        $budget->source = $attributes['source'];
        $budget->medium = $attributes['medium'];
        $budget->office_id = $attributes['office_id'];
        $budget->user_id = 1;
        $budget->status = 1;
        $budget->fiscal_year = "2076/77";
        $budget->date = date('Y-m-d H:i:s');
//        $budget->project_code = $attributes['project_code'];
//        $budget->sub_project_code = $attributes['sub_project_code'];
//        $budget->component_code = $attributes['component_code'];
//        $budget->component_ndesc = $attributes['component_ndesc'];
//        $budget->activity_code = $attributes['activity_code'];


        if ($attributes['expense_head_code'] == '31159' or $attributes['expense_head_code'] == '31157' or $attributes['expense_head_code'] == '22231' or $attributes['expense_head_code'] == '26423') {
            $budget->is_contenjency = 1;
            $budget->contenjency = 3;
        }
        $budget->save();
        $attributes['budget_id'] = $budget->id;
        $expense_type = (substr($attributes['expense_head_code'], 0, 1));

        if ($attributes['expense_head_code'] == '31159' or $attributes['expense_head_code'] == '31157' or $attributes['expense_head_code'] == '22231' or $attributes['expense_head_code'] == '26423') {

            for ($x = 1; $x <= 2; $x++) {

                $amount = $attributes['amount'] * 0.03;
                $sub_activity = $attributes['activity_name'] . " - " . "कन्टेन्जेन्सी";
                if ($x == 1) {
                    $amount = $attributes['amount'] * 0.97;
                    $sub_activity = $attributes['activity_name'];
                }
                $budgetDetals = new BudgetDetails();
                $budgetDetals->office_id = $attributes['office_id'];
                $budgetDetals->budget_sub_head_id = $attributes['budget_sub_head_id'];
                $budgetDetals->budget_id = $attributes['budget_id'];
                $budgetDetals->sub_activity = $sub_activity;
                $budgetDetals->expense_head_id = $attributes['expense_head_id'];
                $budgetDetals->unit = 1;
                $program_amount = $amount;
                $budgetDetals->total_budget = (float)$program_amount;
                $budgetDetals->first_quarter_unit = NULL;
                $budgetDetals->second_quarter_unit = NULL;
                $budgetDetals->third_quarter_unit = 1;
                $budgetDetals->first_quarter_budget = NULL;
                $budgetDetals->first_chaimasik_bhar = NULL;
                $budgetDetals->second_quarter_budget = NULL;
                $budgetDetals->second_chaimasik_bhar = NULL;
                $budgetDetals->third_quarter_budget = (float)$program_amount;
                $budgetDetals->third_chaimasik_bhar = NULL;
                $budgetDetals->source_type = $attributes['source_type'];
                $budgetDetals->source_level = $attributes['source_level'];
                $budgetDetals->source = $attributes['source'];
                $budgetDetals->medium = $attributes['medium'];
                $budgetDetals->office_id = $attributes['office_id'];
                $budgetDetals->user_id = 1;
                $budgetDetals->status = 1;
                $budgetDetals->fiscal_year = "2076/77";
                $budgetDetals->date = date('Y-m-d H:i:s');
                $budgetDetals->save();

                //                activity report from here
//                $expenseHead = ExpenseHead::where('id', $attributes['expense_head'])->where('status', 1)->first();

                $activityReport = new ActivityReport();
                $activityReport->office_id = $attributes['office_id'];
                $activityReport->budget_sub_head_id = $attributes['budget_sub_head_id'];
                $activityReport->activity_id = $budgetDetals->id;
                $activityReport->budget = (float)$amount;
                $activityReport->expense_head = $attributes['expense_head_code'];
                $activityReport->expense = 0;
                $activityReport->remain = (float)$amount;
                $activityReport->fiscal_year = "2076/77";
                $activityReport->save();

                $activityReportDetail = new ActivityReportDetail();
                $activityReportDetail->activity_report_id = $activityReport->id;
                $activityReportDetail->activity_id = $budgetDetals->id;
                $activityReportDetail->budget = (float)$amount;
                $activityReportDetail->expense_head = $attributes['expense_head_code'];
                $activityReportDetail->expense = 0;
                $activityReportDetail->akhtiyari_type = 1;
                $activityReportDetail->fiscal_year = "2076/77";
                $activityReportDetail->save();


            }

        } else {

            $budgetDetals = new BudgetDetails();
            $budgetDetals->office_id = $attributes['office_id'];
            $budgetDetals->budget_sub_head_id = $attributes['budget_sub_head_id'];
            $budgetDetals->budget_id = $attributes['budget_id'];
            $budgetDetals->sub_activity = $attributes['activity_name'];
            $budgetDetals->expense_head_id = $attributes['expense_head_id'];
            $budgetDetals->unit = 1;
            $budgetDetals->total_budget = (float)$attributes['amount'];
            $budgetDetals->first_quarter_unit = NULL;
            $budgetDetals->second_quarter_unit = NULL;
            $budgetDetals->third_quarter_unit = 1;
            $budgetDetals->first_quarter_budget = NULL;
            $budgetDetals->first_chaimasik_bhar = NULL;
            $budgetDetals->second_quarter_budget = NULL;
            $budgetDetals->second_chaimasik_bhar = NULL;
            $budgetDetals->third_quarter_budget = (float)$attributes['amount'];
            $budgetDetals->third_chaimasik_bhar = NULL;
            $budgetDetals->source_type = $attributes['source_type'];
            $budgetDetals->source_level = $attributes['source_level'];
            $budgetDetals->source = $attributes['source'];
            $budgetDetals->medium = $attributes['medium'];
            $budgetDetals->office_id = $attributes['office_id'];
            $budgetDetals->user_id = 1;
            $budgetDetals->status = 1;
            $budgetDetals->fiscal_year = "2076/77";
            $budgetDetals->date = date('Y-m-d H:i:s');
            $budgetDetals->save();

            //            activity report from here
//            $expenseHead = ExpenseHead::where('id', $attributes['expense_head'])->where('status', 1)->first();

            $activityReport = new ActivityReport();
            $activityReport->office_id = $attributes['office_id'];
            $activityReport->budget_sub_head_id = $attributes['budget_sub_head_id'];
            $activityReport->activity_id = $budgetDetals->id;
            $activityReport->budget = $attributes['amount'];
            $activityReport->expense_head = $attributes['expense_head_code'];
            $activityReport->expense = 0;
            $activityReport->remain = $attributes['amount'];
            $activityReport->fiscal_year = "2076/77";
            $activityReport->save();

            $activityReportDetail = new ActivityReportDetail();
            $activityReportDetail->activity_report_id = $activityReport->id;
            $activityReportDetail->activity_id = $budgetDetals->id;
            $activityReportDetail->budget = $attributes['amount'];
            $activityReportDetail->expense_head = $attributes['expense_head_code'];
            $activityReportDetail->expense = 0;
            $activityReportDetail->akhtiyari_type = 1;
            $activityReportDetail->fiscal_year = "2076/77";
            $activityReportDetail->save();
        }
        return $budgetDetals;
    }


    public function createBudgetForOperationalActivity($attributes, $total_budget)
    {

        $budget = new Budget();
        $budget->expense_center = $attributes['expense_center'];
        $budget->budget_sub_head = $attributes['budget_sub_head'];
        $budget->akhtiyari_type = $attributes['akhtiyari_type'];
        $budget->activity = $attributes['activity'];
        $budget->expense_head_id = $attributes['expense_head_id'];
        $budget->expense_head = $attributes['expense_head'];
        $budget->unit = $attributes['unit'];
        $budget->activity = "ईन्धन कार्यालय प्रायोजन";
        $budget->total_unit = $attributes['total_unit'];
        $budget->total_budget = (float)$total_budget;
        $budget->first_quarter_unit = NULL;
        $budget->second_quarter_unit = NULL;
        $budget->third_quarter_unit = 1;
        $budget->first_quarter_budget = NULL;
        $budget->first_chaimasik_bhar = NULL;
        $budget->second_quarter_budget = NULL;
        $budget->second_chaimasik_bhar = NULL;
        $budget->third_quarter_budget = (float)$total_budget;
        $budget->third_chaimasik_bhar = NULL;
        $budget->source_type = $attributes['source_type'];
        $budget->source_level = $attributes['source_level'];
        $budget->source = $attributes['source'];
        $budget->medium = $attributes['medium'];
        $budget->office_id = $attributes['office_id'];
        $budget->user_id = Auth::user()->id;
        $budget->status = 1;
        $budget->project_code = $attributes['project_code'];
        $budget->sub_project_code = $attributes['sub_project_code'];
        $budget->component_code = $attributes['component_code'];
        $budget->component_ndesc = $attributes['component_ndesc'];
        $budget->activity_code = "1.1.1";   //for 21111
        $budget->fiscal_year = $attributes['fiscal_year'];
        $budget->is_contenjency = 0;
        $budget->contenjency = 0;
        $budget->date = $attributes['date'];
        $budget->save();
        return $budget->id;

    }

//    changes to budgetDetails
    public function get_by_budget_sub_head_and_office_id($office_id, $fiscal_year,$budget_sub_head_id)
    {
        return $main_actvities = BudgetDetails::where('office_id', $office_id)
            ->where('fiscal_year',$fiscal_year)
            ->where('budget_sub_head_id', $budget_sub_head_id)
            ->with('expense_head_by_id')
            ->with('budget')
            ->orderBy('expense_head_id')
            ->get();
    }

//    changed to budgetDetails
    public function get_by_id($id)
    {
        return BudgetDetails::where('id', $id)->with('expense_head_by_id')->get();
    }

//    budget detail
    public function getBudgetDetailByBudgetId($budgetId)
    {
        return $budgetDetail = BudgetDetails::where('budget_id', $budgetId)->get();
    }

    public function getByBudgetSubHead($fiscalYear,$budgetSubHeadId){
        return $budgets = Budget::where('office_id',Auth::user()->office->id)->where('fiscal_year',$fiscalYear)->where('budget_sub_head',$budgetSubHeadId)->get();
    }

//    माथिको र यो function लाई पछि मिलाउने गरि
    public function get_by_id_for_pre_bhuktani($id)
    {
        return BudgetDetails::findorfail($id);
    }

    public function get_total_budget_by_akhtiyari($akhtiyari_id)
    {

        return $totalBudget = Budget::where('akhtiyari_type', $akhtiyari_id)->sum('total_budget');
    }

    public function get_total_akhtiyari_by_budget_sub_head($fiscalYear,$budget_sub_head)
    {
        return $totalAkhtiyari = Akhtiyari::where('fiscal_year',$fiscalYear)->where('budget_sub_head_id', $budget_sub_head)->sum('amount');
    }

//    changed to budgetDetails
    public function get_total_budget_by_Activity_id($id)
    {

        return $total_budget_by = BudgetDetails::where('id', $id)->sum('total_budget');
    }

//    fix name in future and change its dependency to other function which has called it.
    public function get_budget_details_by_id($activity_id)
    {
        return $budget_details = Budget::findorfail($activity_id);
    }

    public function update($attribute, $id)
    {
        $budget = Budget::findorfail($id);
        $budgetOldContenjency = $budget->is_contenjency;
        if (array_key_exists('area', $attribute)) {
            $budget->area = $attribute['area'];
        }
        if (array_key_exists('sub_area', $attribute)) {
            $budget->sub_area = $attribute['sub_area'];
        }
        if (array_key_exists('main_program', $attribute)) {
            $budget->main_program = $attribute['main_program'];
        }
        if (array_key_exists('general_activity', $attribute)) {
            $budget->general_activity = $attribute['general_activity'];
        }
        if (array_key_exists('main_activity', $attribute)) {
            $budget->activity = $attribute['main_activity'];
        }
        if (array_key_exists('expense_head_code', $attribute)) {
            $budget->expense_head = $attribute['expense_head_code'];
        }

        if (array_key_exists('expense_head', $attribute)) {
            $budget->expense_head_id = $attribute['expense_head'];
        }

        if (array_key_exists('target_group', $attribute)) {
            $budget->target_group = $attribute['target_group'];
        }
        if (array_key_exists('unit', $attribute)) {
            $budget->unit = $attribute['unit'];
        }
        if (array_key_exists('per_unit_cost', $attribute)) {
            $budget->per_unit_cost = $attribute['per_unit_cost'];
        }
        if (array_key_exists('total_unit', $attribute)) {
            $budget->total_unit = $attribute['total_unit'];
        }
        if (array_key_exists('total_budget', $attribute)) {
            $budget->total_budget = $attribute['total_budget'];
        }
        if (array_key_exists('first_quarter', $attribute)) {
            $budget->first_quarter_unit = $attribute['first_quarter'];
        }
        if (array_key_exists('second_quarter', $attribute)) {
            $budget->second_quarter_unit = $attribute['second_quarter'];
        }
        if (array_key_exists('third_quarter', $attribute)) {
            $budget->third_quarter_unit = $attribute['third_quarter'];
        }

        if (array_key_exists('first_total_budget', $attribute)) {
            $budget->first_quarter_budget = $attribute['first_total_budget'];
        }
        if (array_key_exists('second_total_budget', $attribute)) {
            $budget->second_quarter_budget = $attribute['second_total_budget'];
        }
        if (array_key_exists('third_total_budget', $attribute)) {
            $budget->third_quarter_budget = $attribute['third_total_budget'];
        }
        if (array_key_exists('source_type', $attribute)) {
            $budget->source_type = $attribute['source_type'];
        }
        if (array_key_exists('source_level', $attribute)) {
            $budget->source_level = $attribute['source_level'];
        }
        if (array_key_exists('source', $attribute)) {
            $budget->source = $attribute['source'];
        }
        if (array_key_exists('medium', $attribute)) {
            $budget->medium = $attribute['medium'];
        }

        if (array_key_exists('akhtiyari_type', $attribute)) {
            $budget->akhtiyari_type = $attribute['akhtiyari_type'];
        }
        if (array_key_exists('firstChaumasikBharHidden', $attribute)) {
            $budget->first_chaimasik_bhar = $attribute['firstChaumasikBharHidden'];
        }
        if (array_key_exists('secondChaumasikBharHidden', $attribute)) {
            $budget->second_chaimasik_bhar = $attribute['secondChaumasikBharHidden'];
        }
        if (array_key_exists('thirdChaumasikBharHidden', $attribute)) {
            $budget->third_chaimasik_bhar = $attribute['thirdChaumasikBharHidden'];
        }
        if (array_key_exists('thirdChaumasikBharHidden', $attribute)) {
            $budget->third_chaimasik_bhar = $attribute['thirdChaumasikBharHidden'];
        }

        if (array_key_exists('contenjency_check_box', $attribute)) {

            $budget->is_contenjency = 1;
            $budget->contenjency = $attribute['hidden_contenjency_percent'];
        } else {
            $budget->is_contenjency = 0;
            $budget->contenjency = 0;
        }

        $budget->save();
        $budgetDetails = BudgetDetails::where('budget_id', $id)->get();
        if ((array_key_exists('contenjency_check_box', $attribute) and $budgetOldContenjency == 1)) {
            foreach ($budgetDetails as $index => $budgetDetail) {

                $amount = $attribute['total_budget'] * ($attribute['hidden_contenjency_percent'] * 0.01);
                $sub_activity = $attribute['main_activity'] . " - " . "कन्टेन्जेन्सी";
                if (++$index == 1) {
                    $remain = 100 - $attribute['hidden_contenjency_percent'];
                    $amount = $attribute['total_budget'] * ($remain * 0.01);
                    $sub_activity = $attribute['main_activity'];
                } else {
                    $budgetDetail->contenjency = $attribute['hidden_contenjency_percent'];
                }
                if (array_key_exists('main_activity', $attribute)) {
                    $budgetDetail->sub_activity = $sub_activity;
                }

                if (array_key_exists('expense_head', $attribute)) {
                    $budgetDetail->expense_head_id = $attribute['expense_head'];
                }

                if (array_key_exists('unit', $attribute)) {
                    $budgetDetail->unit = $attribute['unit'];
                }

                if (array_key_exists('total_unit', $attribute)) {
                    $budgetDetail->total_unit = $attribute['total_unit'];
                }
                if (array_key_exists('total_budget', $attribute)) {
                    $budgetDetail->total_budget = (float)$amount;
                }

                if (array_key_exists('first_quarter', $attribute)) {
                    if ($attribute['first_quarter'] != null) {
                        $budgetDetail->first_quarter_unit = $attribute['first_quarter'];
                    } else {
                        $budgetDetail->first_quarter_unit = NULL;

                    }
                }
                if (array_key_exists('second_quarter', $attribute)) {
                    if ($attribute['second_quarter'] != null) {
                        $budgetDetail->second_quarter_unit = $attribute['second_quarter'];
                    } else {
                        $budgetDetail->second_quarter_unit = NULL;
                    }
                }
                if (array_key_exists('third_quarter', $attribute)) {
                    if ($attribute['third_quarter'] != null) {
                        $budgetDetail->third_quarter_unit = $attribute['third_quarter'];
                    } else {
                        $budgetDetail->third_quarter_unit = NULL;

                    }
                }

                if ($attribute['first_total_budget'] != null) {
                    $budgetDetail->first_quarter_budget = (float)$amount;
                } else {
                    $budgetDetail->first_quarter_budget = NULL;
                }
                if ($attribute['second_total_budget'] != null) {
                    $budgetDetail->second_quarter_budget = (float)$amount;
                } else {
                    $budgetDetail->second_quarter_budget = NULL;
                }
                if ($attribute['third_total_budget'] != null) {
                    $budgetDetail->third_quarter_budget = (float)$amount;
                } else {
                    $budgetDetail->third_quarter_budget = NULL;
                }

                if (array_key_exists('source_type', $attribute)) {
                    $budgetDetail->source_type = $attribute['source_type'];
                }
                if (array_key_exists('source_level', $attribute)) {
                    $budgetDetail->source_level = $attribute['source_level'];
                }
                if (array_key_exists('source', $attribute)) {
                    $budgetDetail->source = $attribute['source'];
                }
                if (array_key_exists('medium', $attribute)) {

                    $budgetDetail->medium = $attribute['medium'];
                }

                if (array_key_exists('akhtiyari_type', $attribute)) {
                    $budgetDetail->akhtiyari_type = $attribute['akhtiyari_type'];
                }
                if (array_key_exists('firstChaumasikBharHidden', $attribute)) {
                    $budgetDetail->first_chaimasik_bhar = $attribute['firstChaumasikBharHidden'];
                }
                if (array_key_exists('secondChaumasikBharHidden', $attribute)) {
                    $budgetDetail->second_chaimasik_bhar = $attribute['secondChaumasikBharHidden'];
                }
                if (array_key_exists('thirdChaumasikBharHidden', $attribute)) {
                    $budgetDetail->third_chaimasik_bhar = $attribute['thirdChaumasikBharHidden'];
                }
                $budgetDetail->save();
                $expenseHead = ExpenseHead::where('id', $attribute['expense_head'])->where('status', '=',1)->first();
                $activityReport = ActivityReport::where('activity_id', $budgetDetail->id)->first();
                if ($activityReport) {
                    $activityReport->budget = (float)$amount;
                    $expense = $activityReport->expense;
                    $activityReport->remain = (float)$amount - (float)$expense;
                    $activityReport->expense_head = $attribute['expense_head_code'];
                    $activityReport->save();
                }

                $activityReportDetails = ActivityReportDetail::where('activity_report_id', $activityReport->id)->first();
                if ($activityReportDetails) {
                    $activityReportDetails->activity_report_id = $activityReport->id;
                    $activityReportDetails->activity_id = $budgetDetail->id;
                    $activityReportDetails->budget = (float)$amount;
                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                    $activityReportDetails->save();

                } else {

                    $activityReportDetail = new ActivityReportDetail();
                    $activityReportDetail->activity_report_id = $activityReport->id;
                    $activityReportDetail->activity_id = $budgetDetails->id;
                    $activityReportDetail->budget = (float)$amount;
                    $activityReportDetail->expense_head = $expenseHead->expense_head_code;
                    $activityReportDetail->akhtiyari_type = 1;
                    $activityReportDetail->fiscal_year = $attribute['fiscal_year'];
                    $activityReportDetail->save();
                }
            }
        }
        else if ((array_key_exists('contenjency_check_box', $attribute) and $budgetOldContenjency == 0)) {
            $budgetDetail = BudgetDetails::where('budget_id', $id)->first();
            for ($i = 1; $i <= 2; $i++) {

                $amount = $attribute['total_budget'] * ($attribute['hidden_contenjency_percent'] * 0.01);
                $sub_activity = $attribute['main_activity'] . " - " . "कन्टेन्जेन्सी";

                if ($i == 1) {

                    $remain = 100 - $attribute['hidden_contenjency_percent'];
                    $amount = $attribute['total_budget'] * ($remain * 0.01);
                    $sub_activity = $attribute['main_activity'];

                    if (array_key_exists('main_activity', $attribute)) {
                        $budgetDetail->sub_activity = $sub_activity;
                    }

                    if (array_key_exists('expense_head', $attribute)) {
                        $budgetDetail->expense_head_id = $attribute['expense_head'];
                    }

                    if (array_key_exists('unit', $attribute)) {
                        $budgetDetail->unit = $attribute['unit'];
                    }

                    if (array_key_exists('total_unit', $attribute)) {
                        $budgetDetail->total_unit = $attribute['total_unit'];
                    }
                    if (array_key_exists('total_budget', $attribute)) {
                        $budgetDetail->total_budget = (float)$amount;
                    }

                    if (array_key_exists('first_quarter', $attribute)) {
                        if ($attribute['first_quarter'] != null) {
                            $budgetDetail->first_quarter_unit = $attribute['first_quarter'];
                        } else {
                            $budgetDetail->first_quarter_unit = NULL;

                        }
                    }
                    if (array_key_exists('second_quarter', $attribute)) {
                        if ($attribute['second_quarter'] != null) {
                            $budgetDetail->second_quarter_unit = $attribute['second_quarter'];
                        } else {
                            $budgetDetail->second_quarter_unit = NULL;
                        }
                    }
                    if (array_key_exists('third_quarter', $attribute)) {
                        if ($attribute['third_quarter'] != null) {
                            $budgetDetail->third_quarter_unit = $attribute['third_quarter'];
                        } else {
                            $budgetDetail->third_quarter_unit = NULL;

                        }
                    }

                    if ($attribute['first_total_budget'] != null) {
                        $budgetDetail->first_quarter_budget = (float)$amount;
                    } else {
                        $budgetDetail->first_quarter_budget = NULL;
                    }
                    if ($attribute['second_total_budget'] != null) {
                        $budgetDetail->second_quarter_budget = (float)$amount;
                    } else {
                        $budgetDetail->second_quarter_budget = NULL;
                    }
                    if ($attribute['third_total_budget'] != null) {
                        $budgetDetail->third_quarter_budget = (float)$amount;
                    } else {
                        $budgetDetail->third_quarter_budget = NULL;
                    }

                    if (array_key_exists('source_type', $attribute)) {
                        $budgetDetail->source_type = $attribute['source_type'];
                    }
                    if (array_key_exists('source_level', $attribute)) {
                        $budgetDetail->source_level = $attribute['source_level'];
                    }
                    if (array_key_exists('source', $attribute)) {
                        $budgetDetail->source = $attribute['source'];
                    }
                    if (array_key_exists('medium', $attribute)) {

                        $budgetDetail->medium = $attribute['medium'];
                    }

                    if (array_key_exists('akhtiyari_type', $attribute)) {
                        $budgetDetail->akhtiyari_type = $attribute['akhtiyari_type'];
                    }
                    if (array_key_exists('firstChaumasikBharHidden', $attribute)) {
                        $budgetDetail->first_chaimasik_bhar = $attribute['firstChaumasikBharHidden'];
                    }
                    if (array_key_exists('secondChaumasikBharHidden', $attribute)) {
                        $budgetDetail->second_chaimasik_bhar = $attribute['secondChaumasikBharHidden'];
                    }
                    if (array_key_exists('thirdChaumasikBharHidden', $attribute)) {
                        $budgetDetail->third_chaimasik_bhar = $attribute['thirdChaumasikBharHidden'];
                    }

                    $budgetDetail->save();
                    $expenseHead = ExpenseHead::where('id', $attribute['expense_head'])->where('status', 1)->first();
                    $activityReport = ActivityReport::where('activity_id', $budgetDetail->id)->first();
                    if ($activityReport) {
                        $activityReport->budget = (float)$amount;
                        $expense = $activityReport->expense;
                        $activityReport->remain = (float)$amount - (float)$expense;
                        $activityReport->save();
                    }

                    $activityReportDetails = ActivityReportDetail::where('activity_report_id', $activityReport->id)->first();
                    if ($activityReportDetails) {

                        $activityReportDetails->activity_report_id = $activityReport->id;
                        $activityReportDetails->activity_id = $budgetDetail->id;
                        $activityReportDetails->budget = (float)$amount;
                        $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                        $activityReportDetails->save();
                    }
                }
                else {
                    $budgetDetail = new BudgetDetails();
                    $budgetDetail->office_id = $attribute['expense_center'];
                    $budgetDetail->budget_sub_head_id = $attribute['budget_sub_head'];
                    $budgetDetail->budget_id = $budget->id;
                    $budgetDetail->sub_activity = $sub_activity;
                    $budgetDetail->expense_head_id = $attribute['expense_head'];
                    $budgetDetail->unit = $attribute['unit'];
                    $budgetDetail->total_unit = $attribute['total_unit'];
                    $budgetDetail->total_budget = (float)$amount;
                    if($attribute['first_quarter'] != null){
                        $budgetDetail->first_quarter_unit = $attribute['first_quarter'];
                    }
                    if($attribute['second_quarter'] != null){
                        $budgetDetail->second_quarter_unit = $attribute['second_quarter'];
                    }
                    if($attribute['third_quarter'] != null){
                        $budgetDetail->third_quarter_unit = $attribute['third_quarter'];
                    }

                    if($attribute['first_total_budget'] != null){
                        $budgetDetail->first_quarter_budget = (float)$amount;
                    }
                    $budgetDetail->first_chaimasik_bhar = $attribute['firstChaumasikBharHidden'];

                    if($attribute['second_total_budget'] != null){
                        $budgetDetail->second_quarter_budget = (float)$amount;
                    }
                    $budgetDetail->second_chaimasik_bhar = $attribute['secondChaumasikBharHidden'];

                    if($attribute['third_total_budget'] != null){
                        $budgetDetail->third_quarter_budget = (float)$amount;
                    }
                    $budgetDetail->third_chaimasik_bhar = $attribute['thirdChaumasikBharHidden'];
                    $budgetDetail->source_type = $attribute['source_type'];
                    $budgetDetail->source_level = $attribute['source_level'];
                    $budgetDetail->source = $attribute['source'];
                    $budgetDetail->medium = $attribute['medium'];
                    $budgetDetail->user_id = Auth::user()->id;
                    $budgetDetail->status = 1;
                    $budgetDetail->fiscal_year = $attribute['fiscal_year'];
                    if($i == 2){
                        $budgetDetail->contenjency = $attribute['hidden_contenjency_percent'];
                    }
                    $budgetDetail->date = date('Y-m-d H:i:s');
                    $budgetDetail->save();


                    $expenseHead = ExpenseHead::where('id', $attribute['expense_head'])->where('status', '=', 1)->first();
                    $activityReport = new ActivityReport();
                    $activityReport->office_id = $attribute['expense_center'];
                    $activityReport->budget_sub_head_id = $attribute['budget_sub_head'];
                    $activityReport->activity_id = $budgetDetail->id;
                    $activityReport->budget = (float)$amount;
                    $activityReport->expense_head = $expenseHead->expense_head_code;
                    $activityReport->expense = 0;
                    $activityReport->remain = (float)$amount;
                    $activityReport->fiscal_year = $attribute['fiscal_year'];
                    $activityReport->save();

                    $activityReportDetail = new ActivityReportDetail();
                    $activityReportDetail->activity_report_id = $activityReport->id;
                    $activityReportDetail->activity_id = $budgetDetail->id;
                    $activityReportDetail->budget = (float)$amount;
                    $activityReportDetail->expense_head = $expenseHead->expense_head_code;
                    $activityReportDetail->expense = 0;
                    $activityReportDetail->akhtiyari_type = 1;
                    $activityReportDetail->fiscal_year = $attribute['fiscal_year'];
                    $activityReportDetail->save();

                }
            }
        }
        else {
            if ($budgetOldContenjency == 1) {
                foreach ($budgetDetails as $index => $budgetDetail) {

                    if (++$index == 1) {
                        if (array_key_exists('main_activity', $attribute)) {
                            $budgetDetail->sub_activity = $attribute['main_activity'];
                        }

                        if (array_key_exists('expense_head', $attribute)) {
                            $budgetDetail->expense_head_id = $attribute['expense_head'];
                        }

                        if (array_key_exists('unit', $attribute)) {
                            $budgetDetail->unit = $attribute['unit'];
                        }

                        if (array_key_exists('total_unit', $attribute)) {
                            $budgetDetail->total_unit = $attribute['total_unit'];
                        }
                        if (array_key_exists('total_budget', $attribute)) {
                            $budgetDetail->total_budget = $attribute['total_budget'];
                        }

                        if (array_key_exists('first_quarter', $attribute)) {
                            if ($attribute['first_quarter'] != null) {
                                $budgetDetail->first_quarter_unit = $attribute['first_quarter'];
                            } else {
                                $budgetDetail->first_quarter_unit = NULL;

                            }
                        }
                        if (array_key_exists('second_quarter', $attribute)) {
                            if ($attribute['second_quarter'] != null) {
                                $budgetDetail->second_quarter_unit = $attribute['second_quarter'];
                            } else {
                                $budgetDetail->second_quarter_unit = NULL;
                            }
                        }
                        if (array_key_exists('third_quarter', $attribute)) {
                            if ($attribute['third_quarter'] != null) {
                                $budgetDetail->third_quarter_unit = $attribute['third_quarter'];
                            } else {
                                $budgetDetail->third_quarter_unit = NULL;

                            }
                        }

                        if ($attribute['first_total_budget'] != null) {
                            $budgetDetail->first_quarter_budget = $attribute['total_budget'];
                        } else {
                            $budgetDetail->first_quarter_budget = NULL;
                        }
                        if ($attribute['second_total_budget'] != null) {
                            $budgetDetail->second_quarter_budget = $attribute['total_budget'];
                        } else {
                            $budgetDetail->second_quarter_budget = NULL;
                        }
                        if ($attribute['third_total_budget'] != null) {
                            $budgetDetail->third_quarter_budget = $attribute['total_budget'];
                        } else {
                            $budgetDetail->third_quarter_budget = NULL;
                        }

                        if (array_key_exists('source_type', $attribute)) {
                            $budgetDetail->source_type = $attribute['source_type'];
                        }
                        if (array_key_exists('source_level', $attribute)) {
                            $budgetDetail->source_level = $attribute['source_level'];
                        }
                        if (array_key_exists('source', $attribute)) {
                            $budgetDetail->source = $attribute['source'];
                        }
                        if (array_key_exists('medium', $attribute)) {

                            $budgetDetail->medium = $attribute['medium'];
                        }

                        if (array_key_exists('akhtiyari_type', $attribute)) {
                            $budgetDetail->akhtiyari_type = $attribute['akhtiyari_type'];
                        }
                        if (array_key_exists('firstChaumasikBharHidden', $attribute)) {
                            $budgetDetail->first_chaimasik_bhar = $attribute['firstChaumasikBharHidden'];
                        }
                        if (array_key_exists('secondChaumasikBharHidden', $attribute)) {
                            $budgetDetail->second_chaimasik_bhar = $attribute['secondChaumasikBharHidden'];
                        }
                        if (array_key_exists('thirdChaumasikBharHidden', $attribute)) {
                            $budgetDetail->third_chaimasik_bhar = $attribute['thirdChaumasikBharHidden'];
                        }
                        if(++$index == 2){
                            $budgetDetail->contenjency = $attribute['hidden_contenjency_percent'];
                        }
                        $budgetDetail->save();
                        $expenseHead = ExpenseHead::where('id', $attribute['expense_head'])->where('status', 1)->first();
                        $activityReport = ActivityReport::where('activity_id', $budgetDetail->id)->first();
                        if ($activityReport) {
//
                            $activityReport->budget = $attribute['total_budget'];
                            $expense = $activityReport->expense;
                            $activityReport->remain = $attribute['total_budget'] - (float)$expense;
                            $activityReport->save();
                        }

                        $activityReportDetails = ActivityReportDetail::where('activity_report_id', $activityReport->id)->first();
                        if ($activityReportDetails) {

                            $activityReportDetails->activity_report_id = $activityReport->id;
                            $activityReportDetails->activity_id = $budgetDetail->id;
                            $activityReportDetails->budget = $attribute['total_budget'];
                            $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                            $activityReportDetails->save();

                        } else {

                            $activityReportDetail = new ActivityReportDetail();
                            $activityReportDetail->activity_report_id = $activityReport->id;
                            $activityReportDetail->activity_id = $budgetDetails->id;
                            $activityReportDetail->budget = $attribute['total_budget'];
                            $activityReportDetail->expense_head = $expenseHead->expense_head_code;
                            $activityReportDetail->akhtiyari_type = 1;
                            $activityReportDetail->fiscal_year = $attribute['fiscal_year'];
                            $activityReportDetail->save();
                        }
                    } else {
                        $budgetDetail->delete();
                        $activityReport = ActivityReport::where('activity_id', $budgetDetail->id)->first();
                        if ($activityReport) {
                            $activityReport->delete();
                        }
                        $activityReportDetail = ActivityReportDetail::where('activity_report_id', $activityReport->id)->first();
                        $activityReportDetail->delete();
                    }
                }
            }
            else {

                $budgetDetails = BudgetDetails::where('budget_id', $id)->first();
                if (array_key_exists('main_activity', $attribute)) {
                    $budgetDetails->sub_activity = $attribute['main_activity'];
                }

                if (array_key_exists('expense_head', $attribute)) {
                    $budgetDetails->expense_head_id = $attribute['expense_head'];
                }

                if (array_key_exists('unit', $attribute)) {
                    $budgetDetails->unit = $attribute['unit'];
                }

                if (array_key_exists('total_unit', $attribute)) {
                    $budgetDetails->total_unit = $attribute['total_unit'];
                }
                if (array_key_exists('total_budget', $attribute)) {
                    $budgetDetails->total_budget = $attribute['total_budget'];
                }
                if (array_key_exists('first_quarter', $attribute)) {
                    $budgetDetails->first_quarter_unit = $attribute['first_quarter'];
                }
                if (array_key_exists('second_quarter', $attribute)) {
                    $budgetDetails->second_quarter_unit = $attribute['second_quarter'];
                }
                if (array_key_exists('third_quarter', $attribute)) {
                    $budgetDetails->third_quarter_unit = $attribute['third_quarter'];
                }

                if (array_key_exists('first_total_budget', $attribute)) {
                    $budgetDetails->first_quarter_budget = $attribute['first_total_budget'];
                }
                if (array_key_exists('second_total_budget', $attribute)) {
                    $budgetDetails->second_quarter_budget = $attribute['second_total_budget'];
                }
                if (array_key_exists('third_total_budget', $attribute)) {
                    $budgetDetails->third_quarter_budget = $attribute['third_total_budget'];
                }
                if (array_key_exists('source_type', $attribute)) {
                    $budgetDetails->source_type = $attribute['source_type'];
                }
                if (array_key_exists('source_level', $attribute)) {
                    $budgetDetails->source_level = $attribute['source_level'];
                }
                if (array_key_exists('source', $attribute)) {
                    $budgetDetails->source = $attribute['source'];
                }
                if (array_key_exists('medium', $attribute)) {
                    $budgetDetails->medium = $attribute['medium'];
                }

                if (array_key_exists('akhtiyari_type', $attribute)) {
                    $budgetDetails->akhtiyari_type = $attribute['akhtiyari_type'];
                }
                if (array_key_exists('firstChaumasikBharHidden', $attribute)) {
                    $budgetDetails->first_chaimasik_bhar = $attribute['firstChaumasikBharHidden'];
                }
                if (array_key_exists('secondChaumasikBharHidden', $attribute)) {
                    $budgetDetails->second_chaimasik_bhar = $attribute['secondChaumasikBharHidden'];
                }
                if (array_key_exists('thirdChaumasikBharHidden', $attribute)) {
                    $budgetDetails->third_chaimasik_bhar = $attribute['thirdChaumasikBharHidden'];
                }
                $budgetDetails->save();

                $activityReport = ActivityReport::where('activity_id', $budgetDetails->id)->first();
                if ($activityReport) {

                    $activityReport->budget = $attribute['total_budget'];
                    $expense = $activityReport->expense;
                    $activityReport->remain = $attribute['total_budget'] - (float)$expense;
                    $activityReport->expense_head = $attribute['expense_head_code'];
                    $activityReport->save();
                }

                $activityReportDetails = ActivityReportDetail::where('activity_report_id', $activityReport->id)->first();
                if ($activityReportDetails) {
                    $activityReportDetails->budget = $attribute['total_budget'];
                    $activityReportDetails->expense_head = $attribute['expense_head_code'];
                    $activityReportDetails->save();
                }
            }
        }
        return $budgetDetails;
    }

    public function createAkhtiyari($attributes)
    {

        $office_id = Auth::user()->office_id;
        $akhtiyari = new Akhtiyari();
        $akhtiyari->budget_sub_head_id = $attributes['budget_sub_head'];
        $akhtiyari->office_id = $office_id;
        $akhtiyari->akhtiyari_type = $attributes['akhtiyari_type'];
        $akhtiyari->amount = $attributes['amount'];
        $akhtiyari->source_type = $attributes['source_type'];
        $akhtiyari->source_level = $attributes['source_level'];
        $akhtiyari->source = $attributes['source'];
        $akhtiyari->medium = $attributes['medium'];
        $akhtiyari->date_nepali_roman = $attributes['date_in_roman'];
        $akhtiyari->date_english = date('Y-m-d H:i:s');
        $akhtiyari->fiscal_year = $attributes['fiscal_year'];
        $akhtiyari->detail = $attributes['detail'];
        return $akhtiyari->save();


    }

    public function createAkhtiyariTemp($attributes)
    {
        $akhtiyari = new Akhtiyari();
        $akhtiyari->office_id = $attributes['office_id'];
        $akhtiyari->budget_sub_head_id = $attributes['budget_sub_head_id'];
        $akhtiyari->akhtiyari_type = 1;
        $akhtiyari->amount = $attributes['akhtiyari_amount'];
        $akhtiyari->source_type = $attributes['source_type'];
        $akhtiyari->source_level = $attributes['source_level'];
        $akhtiyari->source = $attributes['source'];
        $akhtiyari->medium = $attributes['medium'];
        $akhtiyari->date_nepali_roman = "2077-03-15";
        $akhtiyari->date_english = date('Y-m-d H:i:s');
        $akhtiyari->fiscal_year = "2076/77";
        $akhtiyari->detail = "सुरु अख्तियारी";
        $akhtiyari->save();
        return $akhtiyari->id;

    }

    public function getAkhtiyariByBudgetSubHeadId($fiscalYear,$budget_sub_head)
    {

        $office_id = Auth::user()->office_id;
        return $akhtiyrai = Akhtiyari::where('office_id', $office_id)
            ->where('fiscal_year',$fiscalYear)
            ->where('budget_sub_head_id', $budget_sub_head)->with(['program', 'source_type', 'source_level', 'source', 'medium', 'budget'])->get();

    }

    public function get_by_akhtiyari_id($akhtiyari_id)
    {

        $activities = Budget::where('akhtiyari_type', $akhtiyari_id)->with(['budgetArea', 'expense_head_by_id', 'chaineDetail.voucher_details'])->get();
        $activities->map(function ($activity) {
            $activity['has_voucher'] = 0;
            foreach ($activity->chaineDetail as $detail) {
                if ($detail->voucher_details) {
                    $activity['has_voucher'] = 1;
                }
            }
            return $activity;
        });
//        foreach ($activities as $activity){
//
////            foreach ($activity->chaineDetail as $detail){
////                $has_voucher = $detail->voucher_details->count();
////                $activity->has_voucher = $has_voucher;
////            }
//            $chaine_details = $activity->chaineDetail;
//            $chaine_details->map(function ($detail) use ($chaine_details)
//            {
//                $has_voucher = $detail->voucher_details->count();
//                $activity->has_voucher = $has_voucher;
//                $item['status'] = $itemStatus;
//                return $item;
//            });
//        }
//        $test = $activities[0]->chaineDetail[0]->voucher_details;
        return $activities;
    }

    public function updateAkhtiyari($attribute, $id)
    {

        $akhtiyari = Akhtiyari::findorfail($id);

        if (array_key_exists('budget_sub_head', $attribute)) {
            $akhtiyari->budget_sub_head_id = $attribute['budget_sub_head'];
        }

        if (array_key_exists('akhtiyar_type', $attribute)) {
            $akhtiyari->akhtiyari_type = $attribute['akhtiyar_type'];
        }

        if (array_key_exists('amount', $attribute)) {
            $akhtiyari->amount = $attribute['amount'];
        }

        if (array_key_exists('source_type', $attribute)) {
            $akhtiyari->source_type = $attribute['source_type'];
        }

        if (array_key_exists('source_level', $attribute)) {
            $akhtiyari->source_level = $attribute['source_level'];
        }

        if (array_key_exists('source', $attribute)) {
            $akhtiyari->source = $attribute['source'];
        }

        if (array_key_exists('medium', $attribute)) {
            $akhtiyari->medium = $attribute['medium'];
        }

        if (array_key_exists('date_in_roman', $attribute)) {
            $akhtiyari->date_nepali_roman = $attribute['date_in_roman'];
        }

        if (array_key_exists('detail', $attribute)) {
            $akhtiyari->detail = $attribute['detail'];
        }

        $akhtiyari->date_english = date('Y-m-d H:i:s');

        return $akhtiyari->save();
    }

    public function deleteAkhtiyariById($activity_id)
    {

        return $deleteAkhtiyari = Akhtiyari::where('id', $activity_id)->delete();
    }

    public function delete_activity_by_id($id)
    {
//        $check_voucher_exists = VoucherDetail::where('main_activity_id', $id)->get();
        $budget = Budget::findorfail($id);
        $budgetDetails = BudgetDetails::where('budget_id', $id)->get();
        foreach ($budgetDetails as $budgetDetail) {

            $activityReport = ActivityReport::where('activity_id', $budgetDetail->id)->first();
            $activityReportDetails = ActivityReportDetail::where('activity_report_id', $activityReport->id)->get();
            foreach ($activityReportDetails as $activityReportDetail) {

                $activityReportDetail->delete();
            }
            $activityReport->delete();
            $budgetDetail->delete();
        }
        $budget->delete();

    }

    public function get_akhtiyari_by_akhtiyari_id($akhtiyari)
    {

        return $akhtiyari = Akhtiyari::findorfail($akhtiyari);
    }

    public function get_activities_by_budget_sub_head_and_budget_type($fiscal_year,$budget_sub_head_id, $office_id)
    {
        return $activities = Budget::where('budget_sub_head', $budget_sub_head_id)
                     ->where('office_id', $office_id)
                     ->where('fiscal_year', $fiscal_year)
                    ->orderBy('expense_head')->get();
    }

    public function getTotalProvinceChaluBudget($ministry_id)
    {

        return $totalProviceChaluBudget = DB::table('budget')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('budget.source_type', 2)
            ->sum('total_budget');
    }

    public function getTotalProvincePujiBudget($ministry_id)
    {
        return $totalProviceChaluBudget = DB::table('budget')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('budget.source_type', 2)
            ->sum('total_budget');
    }

    public function getTotalProvinceBudget($ministry_id)
    {

        return $totalProviceChaluBudget = DB::table('budget')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->where('budget.source_type', 2)
            ->sum('total_budget');
    }

    public function getTotalFederalChaluBudget($ministry_id)
    {

        return $totalProviceChaluBudget = DB::table('budget')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('budget.source_type', 1)
            ->sum('total_budget');
    }

    public function getTotalFederalPujiBudget($ministry_id)
    {

        return $totalProviceChaluBudget = DB::table('budget')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('budget.source_type', 1)
            ->sum('total_budget');
    }

    public function getTotalFederalBudget($ministry_id)
    {

        return $totalProviceChaluBudget = DB::table('budget')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->where('budget.source_type', 1)
            ->sum('total_budget');
    }

    public function getTotalbudgetOfMinistry($ministry_id)
    {
        return $totalProviceChaluBudget = Budget::
        join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->sum('total_budget');
    }

    public function totalProvinceChaluBudgetByOfficeId($office_id)
    {

        return $provinceBudgetChalu = Budget::where('office_id', $office_id)
            ->where('source_type', 2)
            ->whereBetween('expense_head', ['21000', '29000'])->sum('total_budget');
    }

    public function totalProvincePujiBudgetByOfficeId($office_id)
    {

        return $provinceBudgetPuji = Budget::where('office_id', $office_id)
            ->where('source_type', 2)
            ->whereBetween('expense_head', ['30000', '31511'])->sum('total_budget');
    }

    public function totalFederalChaluBudgetByOfficeId($office_id)
    {

        return $provinceBudgetChalu = Budget::where('office_id', $office_id)
            ->where('source_type', 1)
            ->whereBetween('expense_head', ['21000', '29000'])->sum('total_budget');
    }

    public function totalFederalPujiBudgetByOfficeId($office_id)
    {

        return $provinceBudgetPuji = Budget::where('office_id', $office_id)
            ->where('source_type', 1)
            ->whereBetween('expense_head', ['30000', '31511'])->sum('total_budget');
    }

    public function getTotalInitialBudgetByOffice($office_id,$fiscal_year)
    {
        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $office_id)
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('akhtiyari.akhtiyari_type', '=',1)
            ->sum('budget.total_budget');

    }

    public function getTotalInitialBudgetByBudgetSubHead($fiscal_year,$budgetSubHead)
    {
        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('budget.budget_sub_head', $budgetSubHead)
            ->where('akhtiyari.akhtiyari_type','=' ,1)
            ->sum('budget.total_budget');

    }

    public function getTotalAddBudgetByBudgetSubHead($fiscal_year,$budgetSubHead)
    {
        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('budget.budget_sub_head', $budgetSubHead)
            ->where('akhtiyari.akhtiyari_type', '=',2)
            ->sum('budget.total_budget');

    }

    public function getTotalReduceBudgetByBudgetSubHead($fiscal_year,$budgetSubHead)
    {
        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('budget.budget_sub_head', $budgetSubHead)
            ->where('akhtiyari.akhtiyari_type','=' ,3)
            ->sum('budget.total_budget');

    }

    public function getTotalAddBudgetByOffice($office_id,$fiscal_year)
    {
        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $office_id)
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('akhtiyari.akhtiyari_type', '=',2)
            ->sum('budget.total_budget');

    }

    public function getTotalReduceBudgetByOffice($office_id,$fiscal_year)
    {
        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $office_id)
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('akhtiyari.akhtiyari_type','=' ,3)
            ->sum('budget.total_budget');

    }

    public function getFinalBudget($office_id,$fiscal_year)
    {
        return $initialBudget = Budget::where('budget.office_id', $office_id)
            ->where('budget.fiscal_year', $fiscal_year)
            ->sum('budget.total_budget');

    }

    public function getTotalInitialBudgetByMinistry($ministry_id)
    {

        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->where('akhtiyari.akhtiyari_type','=' ,1)
            ->sum('budget.total_budget');

    }

    public function getTotalAddBudgetByMinistry($ministry_id)
    {

        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->where('akhtiyari.akhtiyari_type', 2)
            ->sum('budget.total_budget');

    }

    public function getTotalReduceBudgetByMinistry($ministry_id)
    {

        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->where('akhtiyari.akhtiyari_type', 3)
            ->sum('budget.total_budget');

    }

    public function getFinalBudgetByMinistry($ministry_id)
    {

        return $initialBudget = Budget::
        join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', 4)
            ->sum('budget.total_budget');

    }


    public function deleteById($activity_id)
    {

        $activity = Budget::findorfail($activity_id);
        return $activity->delete();
    }

//    By Office, BudgetSub HEad
    public function getFinalBudgetByBudgetSubHead($office_id,$fiscal_year, $budget_sub_head_id)
    {
        return $finalBudget = Budget::where('budget.office_id', $office_id)
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('budget.budget_sub_head', $budget_sub_head_id)
            ->sum('budget.total_budget');

    }

    public function createBudgetDetailsForOperationalActivity($activity, $total_budget, $budget_id)
    {


        $budgetDetals = new BudgetDetails();
        $budgetDetals->office_id = $activity['expense_center'];
        $budgetDetals->budget_sub_head_id = $activity['budget_sub_head'];
        $budgetDetals->budget_id = $budget_id;
        $budgetDetals->sub_activity = "ईन्धन कार्यालय प्रायोजन";
        $budgetDetals->expense_head_id = $activity['expense_head_id'];
        $budgetDetals->unit = 1;
        $budgetDetals->total_budget = (float)$total_budget;
        $budgetDetals->first_quarter_unit = NULL;
        $budgetDetals->second_quarter_unit = NULL;
        $budgetDetals->third_quarter_unit = 1;
        $budgetDetals->first_quarter_budget = NULL;
        $budgetDetals->first_chaimasik_bhar = NULL;
        $budgetDetals->second_quarter_budget = NULL;
        $budgetDetals->second_chaimasik_bhar = NULL;
        $budgetDetals->third_quarter_budget = (float)$total_budget;
        $budgetDetals->third_chaimasik_bhar = NULL;
        $budgetDetals->source_type = $activity['source_type'];
        $budgetDetals->source_level = $activity['source_level'];
        $budgetDetals->source = $activity['source'];
        $budgetDetals->medium = $activity['medium'];
        $budgetDetals->office_id = $activity['office_id'];
        $budgetDetals->user_id = 1;
        $budgetDetals->status = 1;
        $budgetDetals->fiscal_year = "2076/77";
        $budgetDetals->date = date('Y-m-d H:i:s');
        return $budgetDetals->save();
    }

    public function deleteBudgetDetailsByActivityId($budget_id)
    {

        $budgetDetails = BudgetDetails::where('budget_id', $budget_id)->get();
        foreach ($budgetDetails as $budgetDetail) {

            $budgetDetail->delete();
        }
        return $budgetDetail;
    }

    public function getTotalBudgetByBudgetSubHead($office_id, $fiscal_year,$budgetSubHeadId)
    {
        return $totalBudget = Budget::where('office_id', $office_id)->where('fiscal_year',$fiscal_year)->where('budget_sub_head', $budgetSubHeadId)->sum('total_budget');
    }

    public function UpdataeImportBudget($attribute){
        $programs = Programs::where('program_code', $attribute['program_code'])->where('office_id', $attribute['office_id'])->first();
        $budget = Budget::where('office_id',$attribute['office_id'])
            ->where('budget_sub_head',$programs->id)
            ->where('activity','like','%'. $attribute['activity_name']. '%')->first();
        if($budget){
            $budget->activity_code = $attribute['activity_code'];
            return $budget->save();
        }

    }
}