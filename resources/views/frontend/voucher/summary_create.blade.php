@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-file"></i> भौचर प्रविष्टी विवरण </h1>
            <button type="button" class="btn btn-primary btn-sm btn-show-form displayNone" id="showAddForm"><i
                        class="fas fa-plus"></i> भौचर थप गर्ने
            </button>
        </section>
        <!-- Main content -->
        <section class="content" id="showThisForm">
            <div class="panel panel-default panel-form">
                <div class="panel-body">
                    @if(session()->has('success'))
                        <div class="flash-message">
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        </div>
                    @endif
                    <div class="myNewForm">
                        <button type="button" class="btn btn-danger btn-sm btn-show-form closeOnClick"
                                id="closeAddForm"><i class="fas fa-times"></i> फर्म बन्द गर्ने
                        </button>
                        <form action="{{route('voucher.store')}}" method="post" id="voucherStoreCreateFrom">
                            {{csrf_field()}}
                            <div class="row">
                                <!-- main input fields -->
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="fiscal_year"> मिति </label>
                                                <input type="text" class="form-control" name="date" id="date" value=""
                                                       required style="height: 32px;">
                                            </div>
                                            <div class="col-md-5">
                                                <label for="program">बजेट उपशिर्षक</label>
                                                <select name="program" class="form-control" id="program" required>
                                                    <option value="" selected>..................</option>
                                                    @foreach($programs as $program)
                                                        <option value="{{$program->id}}">{{$program->name}}
                                                            | {{$program->program_code}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <label for="mainActivityName">कार्यक्रम आयोजनाको नाम</label>
                                                <select name="main_activity_name" class="form-control select2"
                                                        id="main_activity_name" required>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>डेविट/क्रेडिट</label>
                                                <select name="drOrCr" class="form-control" id="DrOrCr" required
                                                        style="height: 32px;">
                                                    <option value="1">डेबिट</option>
                                                    <option value="2">क्रेडिट</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>व्यहोरा</label>
                                                <select name="byahora" class="form-control" id="byahora" required>
                                                    <option value="">.......</option>
                                                    @foreach($ledgerTypes as $ledgerType)
                                                        <option value="{{$ledgerType->id}}">{{$ledgerType->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>हिसाब नं.</label>
                                                <select name="hisab_number" class="form-control select2"
                                                        id="hisab_number"
                                                        required>
                                                    <option>...................</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>बिबरण</label>
                                                <input type="text" class="form-control" name="details" id="details"
                                                       required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>पेश्कि पाउने प्रकार</label>
                                                <select name="party_type" class="form-control" id="party_type" disabled>
                                                    <option value="">..............</option>
                                                    <option value="1">उपभोक्ता समिति</option>
                                                    <option value="2">ठेकेदार</option>
                                                    <option value="3">व्यक्तिगत</option>
                                                    <option value="4">संस्थागत</option>
                                                    <option value="5">कर्मचारी</option>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <label>पेश्कि पाउने</label>
                                                <select name="party-name" id="party-name" class="form-control select2">
                                                    <option value="" selected>..............</option>
                                                </select>

                                            </div>
                                            <div class="col-md-3">
                                                <label>रकम</label>
                                                <input type="number" name="amount" min="1" id="amount"
                                                       class="form-control"
                                                       required style="height: 32px;">
                                            </div>
                                            <div class="col-md-2">
                                                <label for="voucher_submit">&nbsp;</label>
                                                <button type="button" class="btn btn-primary btn-block"
                                                        id="voucher_submit"> थप
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- main input fields -->

                                <div class="col-md-3">
                                    <table style="color: red;">
                                        <tr>
                                            <input type="hidden" name="voucher_details_id" id="voucher_details_id">
                                            <td style="inline-size: 377px;" class="voucher_number_show">भौचर नं.:
                                                <span id="voucher_number"></span></td>
                                            <td class="voucher_numbmer_td hidden">
                                                <span id="hidden_voucher_numbmer"></span>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>बजेट : <span id="total_budget"></span></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>खर्च : <span id="total_expense"></span></td>
                                            <input type="hidden" id="hidden_expense">
                                            <td></td>
                                        </tr>
                                        {{--                                        <tr>--}}
                                        {{--                                            <td>पेश्की : <span id="total_advance"></span></td>--}}
                                        {{--                                            <td></td>--}}
                                        {{--                                        </tr>--}}
                                        {{--                                        <tr>--}}
                                        <td>दायित्व : <span id="remain_liability"></span></td>
                                        {{--                                            <td></td>--}}
                                        {{--                                        </tr>--}}
                                        <tr>
                                            <td>मौज्दात : <span id="remain_budget"></span></td>
                                            <input type="hidden" id="hidden_remain_budget">
                                            {{--                                                <td class="hidden" id="hidden_remain_budget"></td>--}}
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div>
                                <table width="100%" border="1"
                                       style="background-color: #d3d3d3; margin-bottom: 20px; display: none"
                                       class="mt-15" id="peskiVoucherTable">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="200">
                                    <thead>
                                    <th class="text-center"></th>
                                    <th class="text-center">मिति</th>
                                    <th class="text-center">भौचर न.</th>
                                    <th class="text-center">खर्च शिर्षक न.</th>
                                    <th class="text-center">पेश्की</th>
                                    <th class="text-center">बाकीं</th>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->

        <section class="content">
            <div class="panel panel-default panel-form pt-10 pb-10">
                <div class="panel-body">
                    <table class="table" id="voucher_table" border="1">
                        <thead>
                        <tr style="background-color:  #dbdbdb;">
                            <th>SN</th>
                            <th>डे|क्रे</th>
                            <th>कार्यक्रम|आयोजनाको नाम</th>
                            <th>व्यहोरा हिसाब न।</th>
                            <th>विवरण</th>
                            <th>डेबिट रकम</th>
                            <th>क्रेडिट रकम</th>
                            <th>प्रापक प्रकार</th>
                            <th>पेश्क पाउने</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr></tr>
                        </tbody>
                        <tfoot>
                        <tr style="height: 3px; background-color: white"></tr>
                            <tr style="background-color: #dbdbdb; margin-top: 5px">
                                <td colspan="5" style="text-align: right">
                                    <span>जम्मा|</span>
                                </td>
                                <td id="dr_amount" style="width: 9%; text-align: right;">0|</td>
                                <td id="cr_amount" style="text-align: right;">0|</td>
                                <td style="color: red; width: 29%;text-align: center" id="amount_difference">बाकिंः 0|</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>कारोवारको संक्षिप्त व्यहोरा</label>
                                        <textarea class="form-control shortInfo" name="shortInfo" id="shortInfo"
                                                  style="margin: 0px; height: 35px; resize: none"></textarea>
                                    </div>
                                    <div class="col-md-8">
                                        <label>कारोवारको विस्तृत व्यहोरा</label>
                                        <textarea class="form-control detailsInfo" name="detailsInfo" id="detailsInfo"
                                                  style="margin: 0px; height: 35px; resize: none "></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

{{--        <section class="content">--}}
{{--            <div class="panel panel-primary">--}}
{{--                <div class="panel-body">--}}
{{--                    <div class="pt-10 pb-10">--}}
{{--                        <div class="sectionTitle">--}}
{{--                            भुक्तानी प्रायोजनको लागि--}}
{{--                        </div>--}}
{{--                        <div class="col-md-12">--}}
{{--                            <div class="form-group">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-4">--}}
{{--                                        <label>कार्यक्रम / आयोजना / क्रियाकलापको नाम</label>--}}
{{--                                        <select class="form-control bhuktani_main_activity" id="bhuktani_main_activity">--}}
{{--                                            <option value="">.........</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-2">--}}
{{--                                        <label>प्रापक प्रकार / प्राप्तकर्ता</label>--}}
{{--                                        <select name="bhuktani_party_type" id="bhuktani_party_type"--}}
{{--                                                class="form-control">--}}
{{--                                            <option value="">..............</option>--}}
{{--                                            <option value=1>उपभोक्ता समिति</option>--}}
{{--                                            <option value="2">ठेकेदार</option>--}}
{{--                                            <option value="3">व्यक्तिगत</option>--}}
{{--                                            <option value="4">संस्थागत</option>--}}
{{--                                            <option value="5">कर्मचारी</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-2">--}}
{{--                                        <label>प्राप्त कर्ता</label>--}}
{{--                                        <select class="form-control select2 bhuktani_party" id="bhuktani_party">--}}
{{--                                            <option value="">.................</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-2">--}}
{{--                                        <label> भुक्तानी रकम</label>--}}
{{--                                        <input type="number" name="bhuktani_amount" min="1" id="bhuktani_amount"--}}
{{--                                               class="form-control">--}}
{{--                                    </div>--}}

{{--                                    <div class="col-md-2">--}}
{{--                                        <label>अग्रिम कर कट्टी रकम</label>--}}
{{--                                        <input type="number" name="advance_tax_deduction_amount" min="1"--}}
{{--                                               id="advance_tax_deduction_amount" class="form-control">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="form-group">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-2">--}}
{{--                                        <label>भ्याट रकम</label>--}}
{{--                                        <input type="number" name="vat_amount" min="1" id="vat_amount"--}}
{{--                                               class="form-control">--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-2">--}}
{{--                                        <label>भ्याट बिल नं.</label>--}}
{{--                                        <input type="number" name="vat_bill_number" min="1" id="vat_bill_number"--}}
{{--                                               class="vat_bill_number form-control">--}}
{{--                                    </div>--}}

{{--                                    <div class="col-md-2">--}}
{{--                                        <label>प्रतिबद्धता नं.</label>--}}
{{--                                        <input type="number" name="pratibadhata_number" min="1" id="pratibadhata_number"--}}
{{--                                               class="pratibadhata_number form-control">--}}
{{--                                    </div>--}}

{{--                                    <div class="col-md-2">--}}
{{--                                        <label>चेक किसिम</label>--}}
{{--                                        <select class="form-control" name="cheque_type" id="chequeType">--}}
{{--                                            <option value="">.........</option>--}}
{{--                                            <option value="1">A/c payee</option>--}}
{{--                                            <option value="2">Barreer</option>--}}
{{--                                            <option value="3">Transfer</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-md-3">--}}
{{--                                        <button type="button" class="btn btn-primary btn-block" name="buktaniButton"--}}
{{--                                                id="buktaniButton"--}}
{{--                                                style="margin-top: 18px;"--}}
{{--                                                onclick="bhuktanivalidation()"> थप--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <table class="table" id="bhuktani" border="1">--}}
{{--                        <thead>--}}
{{--                        <tr style="background-color:  #dbdbdb;">--}}
{{--                            <th>सि न</th>--}}
{{--                            <th>कार्यक्रम/आयोजना/क्रियाकलापको नाम</th>--}}
{{--                            <th>प्रापक प्रकार</th>--}}
{{--                            <th> प्राप्त कर्ता</th>--}}
{{--                            <th> भुक्तानी रकम</th>--}}
{{--                            <th>अग्रिम कर कट्टी रकम</th>--}}
{{--                            <th>भ्याट रकम</th>--}}
{{--                            <th>भ्याट बिल न.</th>--}}
{{--                            <th>प्रतिबद्धता न.</th>--}}
{{--                            <th>चेक किसिम</th>--}}
{{--                            <th>Action</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        <tr></tr>--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                    <table class="table-striped" id="bhuktaniAmountTotal" width="100%" border="1">--}}
{{--                        <tr style="background-color: #dbdbdb">--}}
{{--                            <td style="text-align: right;width:769px;padding-right: 10px;">--}}
{{--                                <span>जम्मा</span>--}}
{{--                            </td>--}}
{{--                            <td id="bhuktaniTotal" style="padding-left: 11px;">--}}
{{--                                0--}}
{{--                            </td>--}}
{{--                        </tr>--}}
{{--                    </table>--}}
                    <div class="submit-buttonss btn-group">
                        <button class="btn btn-primary" id="VoucherSave" onclick="voucherValidationAndSave()"
                                type="button" disabled> Save
                        </button>
                        <a type="button" class="btn btn-primary" href="{{route('voucher')}}"> Clear </a>
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{--    उपभोक्ता modal--}}
    <div class="modal fade party_modal" id="party_modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">पेश्की/भुक्तानी पाउने विवरण प्रविष्टी</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="party_type" style="display: block;">प्रकार</label>
                                <select name="party_type" id="moda_party_type" required class="form-control">
                                    @foreach($party_types as $party_type)
                                        @if($party_type->id != 5)
                                            <option value="{{$party_type->id}}">{{$party_type->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-8">
                                <label for="name_nep">नाम नेपाली</label>
                                <input type="text" class="form-control" id="name_nep" placeholder="नाम नेपाली"
                                       name="name_nep" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="name_eng">नाम अंग्रेजी</label>
                                <input type="text" class="form-control" id="name_eng" placeholder="नाम अंग्रेजी"
                                       name="name_eng"
                                       required>
                            </div>
                            <div class="col-md-6">
                                <label for="citizen_number">नागरिकता नं.</label>
                                <input type="text" class="form-control" id="citizen_number" placeholder="नागरिकता नं."
                                       name="citizen_number" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="vat_number">भ्याट/प्यान नं.</label>
                                <input type="text" class="form-control" id="vat_number" placeholder="भ्याट/प्यान न."
                                       name="vat_number">
                            </div>
                            <div class="col-md-6">
                                <label for="vat_office">स्थायी लेखा नम्वर जारी गर्ने कार्यालय र ठेगाना</label>
                                <select id="vat_office" class="form-control select2" name="vat_office">
                                    <option value="" selected="">.........</option>
                                    @foreach($vat_offices as $vat_office)
                                        <option value="{{$vat_office->id}}">{{$vat_office->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="mobile_number">मोबाइल नं.</label>
                                <input type="text" class="form-control" id="mobile_number" placeholder="मोबाइल नं."
                                       name="mobile_number">
                            </div>
                            <div class="col-md-6">
                                <label for="payee_code">पेयी कोड नं.</label>
                                <input type="text" class="form-control" id="payee_code" name="payee_code">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="bank">बैङ्क</label>
                                <select id="bank" name="bank" class="form-control select2">
                                    <option value="" selected="">.........</option>
                                    @foreach($all_banks as $bank)
                                        <option value="{{$bank->id}}">{{$bank->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label>शाखा कार्यालय</label>
                                <input type="text" class="form-control" name="bank_address" id="bank_address" value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>खाता नं.</label>
                                <input type="number" class="form-control" name="khata_number" id="khata_number">
                            </div>

                            <div class="col-md-6">
                                <label for="is_advance">पेश्किमा देखाउने</label>
                                <select name="is_advance" id="is_advance" class="form-control">
                                    <option value="0">हो</option>
                                    <option value="2">होइन</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="is_bhuktani">भुक्तानीमा देखाउने</label>
                                <select name="is_bhuktani" id="is_bhuktani" class="form-control">
                                    <option value="0">हो</option>
                                    <option value="2">होइन</option>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label for="is_dharauti">धरौटीमा देखाउने</label>
                                <select name="is_dharauti" id="is_dharauti" class="form-control">
                                    <option value="0">हो</option>
                                    <option value="2">होइन</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            id="party_modal_close">Close
                    </button>
                    <button type="button" class="btn btn-success" id="party_modal_submit">Add</button>
                </div>
            </div>

        </div>
    </div>


    {{--    कर्मचारी modal--}}
    <div class="modal fade karmachari_modal" id="karmachari_modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">पेशकी लिने कर्मचारीको विवरण थप गर्ने</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">

                            <div class="col-md-6">
                                <label for="name_nepali">पुरा नाम नेपालीमा:</label>
                                <input type="text" class="form-control" id="name_nepali" placeholder="नाम नेपालीमा"
                                       name="name_nepali" required>
                            </div>
                            <div class="col-md-6">
                                <label for="name_english">पुरा नाम अंग्रेजीमा:</label>
                                <input type="text" class="form-control" id="name_english" placeholder="नाम अंग्रेजीमा"
                                       name="name_english" required>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="gender">लिङ्ग:</label>
                                <select class="form-control" name="gender" id="gender" required>
                                    <option value="">....</option>
                                    <option value="0">महिला</option>
                                    <option value="1">पुरुष</option>
                                    <option value="2">अन्य</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="date_of_birth">जन्म मिति :</label>
                                {{-- <input type="hidden" id="roman_date" name="dob_roman"> --}}
                                <input type="text" class="form-control" id="date_of_birth" placeholder="Select Date"
                                       name="date_of_birth" required>
                            </div>
                            <div class="col-md-4">
                                <label for="marital_status">वैवाहिक स्थिति:</label>
                                <select class="form-control" name="marital_status" id="marital_status" required>
                                    <option value="">....</option>
                                    <option value="0">एकल</option>
                                    <option value="1">दम्पति</option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="bank">बैङ्क</label>
                                <select id="bank" name="bank" class="form-control select2" width="100%">
                                    <option value="" selected="">.........</option>
                                    @foreach($all_banks as $bank)
                                        <option value="{{$bank->id}}">{{$bank->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label>शाखा कार्यालय</label>
                                <input type="text" class="form-control" name="bank_address" id="bank_address">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label>खाता नं.</label>
                                <input type="text" class="form-control" name="khata_number" id="khata_number">
                            </div>
                            <div class="col-md-4">
                                <label for="payee_code">पेयी कोड नं.</label>
                                <input type="text" class="form-control" id="payee_code" name="payee_code" required>
                            </div>
                            <div class="col-md-4">
                                <label for="vat_pan">भ्याट/प्यान नं.</label>
                                <input type="number" class="form-control" id="vat_pan" placeholder="भ्याट/प्यान नं."
                                       name="vat_pan">
                            </div>
                        </div>
                    </div>

                    {{-- Personal Details End --}}

                    <div class="clearfix"></div>

                    <h3>सम्पर्क विवरण</h3>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>प्रदेश</label>
                                <select class="form-control select2" id="province" name="province">
                                    <option>.....................</option>
                                    @foreach($provinces as $province)
                                        <option value="{{$province->id}}">{{$province->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="district">जिल्ला :</label>
                                <select class="form-control select2" name="district" id="district" required>
                                    <option>..............</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="local_level">स्थानीय तह :</label>
                                <select class="form-control select2" name="local_level" id="local_level" required>
                                    <option value="">....</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="phone_number">फोन नं.:</label>
                                <input type="tel" class="form-control" id="phone_number" placeholder="फोन नं"
                                       name="phone_number" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="mobile_number">मोबाइल:</label>
                                <input type="text" class="form-control mobile_number" id="mobile_number"
                                       placeholder="मोबाइल" name="mobile_number" required>
                            </div>
                            <div class="col-md-6">
                                <label for="email_address">इमेल:</label>
                                <input type="email" class="form-control" id="email_address" placeholder="इमेल"
                                       name="email_address" required>
                            </div>
                        </div>
                    </div>

                    {{--Contact Details End--}}

                    <div class="clearfix"></div>

                    {{-- Organizational Details--}}
                    <h3>कार्यालयसम्बन्धी विवरण</h3>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="darbandi_srot">दरबन्दीको श्रोत:</label>
                                <select class="form-control" name="darbandi_srot_id" id="darbandi_srot_id" required>
                                    <option value="">....</option>
                                    @foreach($darbandisrots as $darbandisrot)
                                        <option value="{{$darbandisrot->id}}">{{$darbandisrot->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="darbandi_type">दरबन्दीको प्रकार:</label>
                                <select class="form-control" name="darbandi_type_id" id="darbandi_type_id" required>
                                    <option value="">....</option>
                                    @foreach($darbanditypes as $darbanditype)
                                        <option value="{{$darbanditype->id}}">{{$darbanditype->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="sheet_roll_no">सिटरोल नंबर.:</label>
                                <input type="number" class="form-control" id="sheet_roll_no" placeholder="सिटरोल नंबर"
                                       name="sheet_roll_no" required>
                            </div>
                            <div class="col-md-6">
                                <label for="sewa">सेवा वर्ग:</label>
                                <select class="form-control" name="sewa_barga" id="sewa_barga" required>
                                    <option value="">............</option>
                                    <option value="1">निजामती सेवा</option>
                                    <option value="2">अन्य</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="sewa">सेवा:</label>
                                <select class="form-control" name="sewa_id" id="sewa_id" required>
                                    <option value="">....</option>
                                    @foreach($sewas as $sewa)
                                        <option value="{{$sewa->id}}">{{$sewa->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="samuha_id">समूह:</label>
                                <select class="form-control" name="samuha_id" id="samuha_id" required>
                                    <option value="">....</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="taha">श्रेणी/तह:</label>
                                <select class="form-control" name="taha_id" id="taha_id" required>
                                    <option value="">....</option>
                                    @foreach($tahas as $taha)
                                        <option value="{{$taha->id}}">{{$taha->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="pad">पद:</label>
                                <select class="form-control" name="pad_id" id="pad_id" required>
                                    <option value="">....</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="office_head">कार्यालय प्रमुख:</label>
                                <select class="form-control" name="office_head" id="office_head" required>
                                    <option value="2">होईन</option>
                                    <option value="1">हो</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="acc_head">आर्थिक प्रमुख:</label>
                                <select class="form-control" name="acc_head" id="acc_head" required>
                                    <option value="2">होईन</option>
                                    <option value="1">हो</option>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label for="is_peski">पेश्किमा देखाउने:</label>
                                <select class="form-control" name="is_peski" id="is_peski" required>
                                    <option value="1">हो</option>
                                    <option value="2">होईन</option>
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="is_payroll">तलवीमा देखाउने:</label>
                                <select class="form-control" name="is_payroll" id="is_payroll" required>
                                    <option value="2">होईन</option>
                                    <option value="1">हो</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="can_accept">स्विकृत:</label>
                                <select class="form-control" name="can_accept" id="can_accept" required>
                                    <option value="2">होईन</option>
                                    <option value="1">हो</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="status">सक्रिय:</label>
                                <select class="form-control" name="status" id="status" required>
                                    <option value="1">हो</option>
                                    <option value="0">होईन</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            id="karmachari_modal_close">Close
                    </button>
                    <button type="button" class="btn btn-success" id="karmachari_modal_submit">Add</button>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('scripts')
    {{-- बजेट उपशिर्षक change हुदा  कार्यक्रम आउने  Event--}}
    <script>
        $(document).ready(function () {
            $('#program').change(function () {
                let program_code = $('#program').val();
                get_main_activity_by_budget_sub_head(program_code);

            })
        })

    </script>

    {{-- बजेट उपशिर्षक change हुदा कार्यक्रम आउने  function--}}
    <script>
        let get_main_activity_by_budget_sub_head = function (program_code, activity_id = '') {
            let url = '{{route('get_main_activity_by_budget_sub_head',123)}}';
            url = url.replace(123, program_code);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let option = '<option value=""  selected>.......</option>';
                    let activities = $.parseJSON(res);
                    if (res) {

                        $.each(activities, function () {
                            if (this.id == activity_id) {
                                option += '<option value="' + this.id + '" selected>' + this.sub_activity + this.expense_head_by_id.expense_head_code + ' |रु. ' + this.total_budget + '</option>';
                                get_expense_head_by_activity_id(this.id);
                            } else {
                                option += '<option value="' + this.id + '">' + this.sub_activity + ' | ' + this.expense_head_by_id.expense_head_code + ' | ' + this.total_budget + '</option>'
                            }
                        })
                    }
                    $('#main_activity_name').html(option).change();

                }
            })

        }
    </script>

    {{-- बजेट उपशिर्षक click गर्दा भौचर न. आउने Event--}}
    <script>
        $(document).ready(function () {
            $('#program').change(function () {
                let budget_sub_head_id = $('#program').val();
                get_voucher_number(budget_sub_head_id);
            })
        })
    </script>

    {{--  बजेट उपशिर्षक click गर्दा भौचर न. आउने Function--}}
    <script>
        let get_voucher_number = function (budget_sub_head_id, vn) {
            let voucher_number = 0;
            let url = '{{route('get_voucher_by_budget_sub_head_and_office',123)}}';
            url = url.replace(123, budget_sub_head_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let nepaliVal = changeToNepali(res);
                    $('#voucher_number').text(res);
                    $('td.voucher_numbmer_td span#hidden_voucher_numbmer').text(res);
                }
            });
        }
    </script>

    {{-- कार्यक्रम change हुदा Event--}}
    <script>
        $(document).ready(function () {
            $('#main_activity_name').change(function () {
                let main_activity_id = $('#main_activity_name').val();
                let byahora = $('#byahora').val();
                if (byahora.length < 1) {
                    $("#byahora").val("1").change();
                }
                if (main_activity_id != '') {
                    get_expense_head_by_activity_id(main_activity_id);

                } else {
                    if (byahora == 1 || byahora == 4) {
                        $('#hisab_number').html("");
                        $('#details').val('')

                    } else if (byahora == 6 || byahora == 7) {
                        get_expense_head();
                        $('#details').val('');

                    } else {

                    }
                }
            })
        })
    </script>

    {{--कार्यक्रम change हुदा बजेट र खर्च देखिने Event--}}
    <script>
        let global_remain_budget = 0;
        $(document).ready(function () {
            $('#main_activity_name').change(function () {
                let main_activity_id = $('#main_activity_name').val();
                let byahora = $('#byahora').val();
                if (byahora.length < 1) {

                    $("#byahora").val("1").change();
                }

                if (main_activity_id != '') {

                    get_expense_head_by_activity_id(main_activity_id);

                } else {
                    if (byahora == 1 || byahora == 4 || byahora == 13) {
                        $('#hisab_number').html("");
                        $('#details').val('')
                    } else {

                    }
                }
                get_budget_by_main_Activity(main_activity_id);
            })
        });
    </script>

    {{--  कार्यक्रम change हुदा बजेट र खर्च देखिने Function  --}}
    <script>
        let get_budget_by_main_Activity = function () {
            let main_activity_id = $('#main_activity_name').val();
            let url = '{{route('get_budget_and_expe_by_activity',123)}}';
            url = url.replace(123, main_activity_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (result) {
                    let data = $.parseJSON(result);
                    let $existingDebitTds = $('#voucher_table tbody td.activity[data-id="' + main_activity_id + '"]');

                    let existingDebitTotal = 0;
                    let existingAdvanceTotal = 0;
                    $.each($existingDebitTds, function () {
                        if (!$(this).closest('tr').hasClass(editingSN)) {

                            existingDebitTotal += parseFloat($(this).closest('tr').find('td.drAmount').text());
                            existingAdvanceTotal += parseFloat($(this).closest('tr').find('td.drAmount').text())
                        }
                    });
                    let totalExpense = parseFloat(data['expense']) + existingDebitTotal;
                    let remainingBudget = parseFloat(data['remain']) - existingDebitTotal;

                    $('#total_budget').text(data['budget']).addClass('e-n-t-n-n');
                    $('#total_expense').text(totalExpense.toFixed(2)).addClass('e-n-t-n-n');
                    $('#total_advance').text(data['advance']).addClass('e-n-t-n-n');
                    $('#remain_budget').text(remainingBudget.toFixed(2)).addClass('e-n-t-n-n');
                    $('#hidden_remain_budget').val(remainingBudget.toFixed(2));
                    global_remain_budget = remainingBudget;
                }
            })
        };
    </script>

    {{-- get_expense_head_by_activity_id --}}
    <script>
        let get_expense_head_by_activity_id = function (main_activity_id) {

            let url = '{{route('get_expense_head_by_activity_id',123)}}';
            url = url.replace(123, main_activity_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let data = $.parseJSON(res);
                    let options = '<option selected>.........................</option>';
                    options += '<option value="' + data[0].expense_head_by_id.id + '" selected>' + data[0].expense_head_by_id.expense_head_code + ' | ' + data[0].expense_head_by_id.expense_head_sirsak + '</option>';
                    var selected_option = $('#byahora option:selected');
                    if (selected_option.length == 0 || selected_option.val() == 1 || selected_option.val() == 4 || selected_option.val() == 9 || selected_option.val() == 7 || selected_option.val() == 6 || selected_option.val() == 13) {

                        $('#hisab_number').html(options);
                        $('#details').val(data[0].expense_head_by_id.expense_head_sirsak);
                        $('input#details').closest('div').find('p.validation-error').remove();

                    }
                }
            })
        }
    </script>

    {{--Dr  Or Cr change हुदा--}}
    <script>
        $(document).ready(function () {
            $('#DrOrCr').change(function () {
                $('#byahora').val("");
                $('#hisab_number').val("");
                $('#details').val("");
            })
        })
    </script>

    {{--व्यहोरा change हुदा--}}
    <script>
        $(document).on('change', '#byahora', function () {

            let program_id = $('#program').val();
            let main_activity_id = $('#main_activity_name').val();
            let byahora = $('#byahora :selected').val();
            let DrOrCr = $('#DrOrCr').val();
            if (byahora == 4 || byahora == 9) {
                $('#party_type').prop('disabled', false);
                get_expense_head_by_activity_id(main_activity_id);

            } else if (byahora == 1 || byahora == 13) {

                $('#main_activity_name').focus();
                get_expense_head_by_activity_id(main_activity_id);
                $('#party_type').attr("disabled", true);

            } else if (DrOrCr == 2 && byahora == 3) {

                $('#party_type').attr("disabled", "disabled");
                get_hisab_number_by_byahora(byahora);
                $('#amount').focus();

            } else if (byahora == 12) {

                $('#hisab_number').select2('focus');
                $('#party_type').attr("disabled", false);
                getUsedExpenseHead();

            } else if (byahora == 7) {
                if (main_activity_id) {
                    $('#party_type').prop('disabled', false);
                    get_expense_head_by_activity_id(main_activity_id);

                } else {
                    $('#hisab_number').select2('focus');
                    $('#party_type').prop('disabled', false);
                    $('#details').val('');
                    get_expense_head();
                }

            } else if (byahora == 6) {
                if (main_activity_id) {
                    get_expense_head_by_activity_id(main_activity_id);

                } else {
                    $('#details').val('');
                    get_expense_head();
                }

            } else if (byahora == 8){
                if(program_id){
                    $('#hisab_number').select2('focus');
                    $('#party_type').prop('disabled', true);
                    $('#details').val('');
                    get_expense_head();
                }
            } else {

                $('#party_type').attr("disabled", "disabled");
                get_hisab_number_by_byahora(byahora);
            }
        });

        let get_expense_head = function (expenseHeadId = '') {

            let url = '{{route('get.expense.head')}}';
            $.ajax({

                method: 'get',
                url: url,
                success: function (result) {
                    let expenseHeads = $.parseJSON(result);
                    let options = '<option selected>............</option>';
                    $.each(expenseHeads, function () {
                        if (expenseHeadId == this.id) {
                            options += '<option value="' + this.id + '" selected>' + this.expense_head_code + ' | ' + this.expense_head_sirsak + '</option>'
                        } else {

                            options += '<option value="' + this.id + '">' + this.expense_head_code + ' | ' + this.expense_head_sirsak + '</option>'
                        }

                    });
                    $('#hisab_number').html(options);

                }
            })
        };

        let getUsedExpenseHead = function (expenseHeadId = '') {

            let budgetSubHead = $('#program').val();
            if (budgetSubHead) {

                let url = '{{route('get.used.expense.head',123)}}';
                url = url.replace(123, budgetSubHead);
                $.ajax({

                    url: url,
                    method: 'get',
                    success: function (result) {

                        let expense_heads = $.parseJSON(result);
                        let options = '<option selected>.................</option>';
                        $.each(expense_heads, function () {

                            if (expenseHeadId == this.id) {

                                options += '<option value="' + this.id + '" selected>' + this.expense_head_code + ' | ' + this.expense_head_sirsak + '</option>'

                            } else {
                                options += '<option value="' + this.id + '">' + this.expense_head_code + ' | ' + this.expense_head_sirsak + '</option>'

                            }

                        });
                        $('#hisab_number').html(options);
                    }
                })
            }
        }


    </script>

    {{--    पेश्की पाउने change हुदा--}}

    <script>
        $(document).on('change', '#party-name', function () {

            let activity_id = $('select#main_activity_name').val();
            let party = $('select#party-name').val();
            let dr_cr = $('select#DrOrCr').val();
            let budget_sub_head_id = $('#program').val();
            let expense_head_id = $('#hisab_number').val();
            if (party) {
                if (dr_cr == 2) {

                    let url = '{{route('get.reamin.peski')}}';
                    let data = {

                        "budget_sub_head_id": budget_sub_head_id,
                        "activity_id": activity_id,
                        "expense_head_id": expense_head_id,
                        "party_id": party,
                        "_token": '{{csrf_token()}}'
                    };
                    $.ajax({

                        url: url,
                        method: 'post',
                        data: data,
                        success: function (res) {

                            let peskiVouchers = $.parseJSON(res);
                            let $table = $('table#peskiVoucherTable');
                            let voucherDetailId = $('#voucher_details_id').val();
                            if (peskiVouchers.length > 0) {
                                $table.find('tbody').html('');
                                let remainFlag = 0;
                                $.each(peskiVouchers, function () {
                                    if (this.baki > 0) {
                                        remainFlag = 1;

                                        let tr = '<tr>';

                                        if (voucherDetailId == this.id && editingSN) {

                                            tr += '<td style="text-align: center" class="voucher-detail-id"><input checked type="checkbox" class="peski-voucher-detail-id" value="' + this.id + '" name="peski_farsyot_voucher_id"></td>';

                                        } else if (voucherDetailId == this.id) {

                                            tr += '<td style="text-align: center" class="voucher-detail-id"><input checked type="checkbox" class="peski-voucher-detail-id" value="' + this.id + '" name="peski_farsyot_voucher_id" disabled></td>';

                                        } else {
                                            tr += '<td style="text-align: center" class="voucher-detail-id"><input  type="checkbox" class="peski-voucher-detail-id" value="' + this.id + '" name="peski_farsyot_voucher_id"></td>';

                                        }
                                        tr += '<td class="date">';
                                        tr += this.date;
                                        tr += '</td><td class="voucher_no">';
                                        tr += this.voucher_no;
                                        tr += '</td><td class="voucher_no">';
                                        tr += this.activity;
                                        tr += '</td><td class="activity">';
                                        tr += this.peski;
                                        tr += '</td><td class="baki">';
                                        tr += this.baki;
                                        tr += '</td></tr>';
                                        $table.find('tbody').append(tr)
                                    }
                                });
                                if (remainFlag) {
                                    $table.show();
                                } else {
                                    alert("पेश्की बाकीँ छैन !!");
                                    $table.hide();
                                }
                            } else {

                                alert("पेश्की बाकीँ छैन११");
                                $table.hide();
                            }
                        }

                    })
                }
            }

        })
    </script>

    {{--    Peski voucher options --}}
    <script>
        $(document).on('change', 'input.peski-voucher-detail-id', function () {
            let $tr = $(this).closest('tr');
            let baki = $.trim($tr.find('td.baki').text());
            if (this.checked) {
                $('input#amount').val(baki);
                $tr.find('td.baki').text(0);
                $('input.peski-voucher-detail-id').prop('disabled', true);
                let test = $(this).val();

                let voucherDetailsId = $.trim(test);

                $('#voucher_details_id').val(test);
                $(this).prop('disabled', false);

            } else {
                $tr.find('td.baki').text($('input#amount').val());
                $('input#amount').val('');
                $('input#voucher_details_id').val('');
                $('input.peski-voucher-detail-id').prop('disabled', false);
            }
        });
    </script>

    {{--Get Remain Liability on change of liability head--}}

    <script>
        $(document).on('change', '#hisab_number', function () {
            let hisab_id = $('#hisab_number').val();
            let ledger_type = $('#byahora').val();
            let drOrCr = $('#DrOrCr').val();
            if (drOrCr == 1 && ledger_type == 2) {
                let activity_id = $('#main_activity_name').val();
                let budget_sub_head_id = $('#program').val();
                let url = '{{route('get_liability',['123','234'])}}';
                url = url.replace(123, activity_id);
                url = url.replace(234, hisab_id);
                if (activity_id) {
                    url =
                        $.ajax({
                            url: url,
                            method: 'get',
                            success: function (res) {
                                let Remainliability = $.parseJSON(res);
                                console.log(Remainliability)
                                $('#remain_liability').text(Remainliability);
                                $('#amount').val(Remainliability);
                            }
                        })
                }

            }
        })
    </script>

    {{--on change party_type for Vocuher --}}
    <script>
        $(document).ready(function () {
            $('#party_type').change(function () {
                let party_type_id = $('#party_type :selected').val();
                getPartyByPartyType(party_type_id);
            })
        });
        let getKarmachari = function () {

            let url = '{{route('get.karmachari')}}';
            $.ajax({

                url: url,
                method: 'get',
                success: function (res) {
                    let option = '<option selected>....................</option>';
                    $.each($.parseJSON(res), function () {
                        option += '<option value="' + this.id + '">' + this.name_nepali + '</option>'

                    });
                    $('#party-name').html(option);
                }
            });
        }
    </script>

    {{--get  party name for voucher--}}
    <script>
        let getPartyByPartyType = function (party_type_id, party_name = '') {
            let url = '{{route('get_party_name_by_party_type_and_office', 123)}}';
            url = url.replace(123, party_type_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let options = '<option value="" selected>-----------</option>';
                    options += '<option value="add" style="font-style: italic; color: blue; text-decoration: underline">Add New</option>';
                    $.each($.parseJSON(res), function () {
                        if (party_name == this.name_nep) {
                            options += '<option value="' + this.id + '" selected>' + this.name_nep + '</option>'

                        } else {

                            options += '<option value="' + this.id + '">' + this.name_nep + '</option>'
                        }
                    });
                    $('#party-name').html(options);
                    // console.log($('#party-name').val());
                    if ($('#party-name').val()) {

                        $('#party-name').change();

                    }

                }
            })
        }
    </script>

    {{--Modal Satart --}}

    {{--party name click हुदा--}}
    <script>
        let adding_party_id = '';
        $(document).on('change', '#party-name, #bhuktani_party', function () {

            let party_type = '';
            let party_type_id = $(this).prop('id');
            if (party_type_id == "party-name") {

                party_type = $('#party_type').val();
            } else {

                party_type = $('#bhuktani_party_type').val();

            }

            if ($(this).val() == 'add') {
                if (party_type != 5) {

                    get_modal_show(this, party_type_id);

                } else {

                    get_karmachari_modal_show(this);
                }

            } else {
            }
        });

        let get_modal_show = function (this_, party_type_id) {

            if (party_type_id == "party-name") {

                party_type = $('select#party_type').val();

            } else {
                party_type = $('select#bhuktani_party_type').val();
            }

            $('#moda_party_type').val(party_type);
            adding_party_id = $(this_).prop('id');
            $('#party_modal').modal('show');
        };


        $('#party_modal_close').click(function () {
            $('select#party-name').val('');
            $('select#bhuktani_party').val('');
        });


        let get_karmachari_modal_show = function (this_) {
            adding_party_id = $(this_).prop('id');
            $('#karmachari_modal').modal('show');


        };

        $('#karmachari_modal_close').click(function () {
            $('select#party-name').val('');
        })
    </script>

    {{--  party  modal submit हुदा--}}

    <script>
        $(document).on('click', '#party_modal_submit', function () {

            let data = {};
            data['party_type'] = $('#moda_party_type').val();
            data['name_nep'] = $('#name_nep').val();
            data['name_eng'] = $('#name_eng').val();
            data['citizen_number'] = $('#citizen_number').val();
            data['vat_number'] = $('#vat_number').val();
            data['vat_office'] = $('#vat_office').val();
            data['mobile_number'] = $('#mobile_number').val();
            data['bank'] = $('#bank').val();
            data['payee_code'] = $('#payee_code').val();
            data['bank_address'] = $('#bank_address').val();
            data['khata_number'] = $('#khata_number').val();
            data['is_advance'] = $('#is_advance').val();
            data['is_bhuktani'] = $('#is_bhuktani').val();
            data['is_dharauti'] = $('#is_dharauti').val();
            data['_token'] = '{{csrf_token()}}';

            let url = '{{route('AdvanceAndPayment.store')}}';
            $.ajax({

                method: 'post',
                url: url,
                data: data,
                success: function (res) {
                    let options = '';
                    let party = $.parseJSON(res);
                    options += '<option value="' + party.id + '" selected>' + party.name_nep + '</option>';

                    $('#' + adding_party_id).append(options);
                    $('#party_modal').modal('hide');

                }
            })
        })
    </script>

    {{--    Karmachari modal submit हुदा--}}

    {{--    modal submit हुदा--}}

    <script>
        $(document).on('click', '#karmachari_modal_submit', function () {


            let data = {};
            data['name_nepali'] = $('#name_nepali').val();
            data['name_english'] = $('#name_english').val();
            data['gender'] = $('#gender').val();
            data['date_of_birth'] = $('#date_of_birth').val();
            data['marital_status'] = $('#marital_status').val();
            data['bank'] = $('#bank').val();
            data['bank_address'] = $('#bank_address').val();
            data['khata_number'] = $('#khata_number').val();
            data['vat_pan'] = $('#vat_pan').val();
            data['province'] = $('#province').val();
            data['district'] = $('#district').val();
            data['local_level'] = $('#local_level').val();
            data['phone_number'] = $('#phone_number').val();
            data['mobile_number'] = $('.mobile_number').val();
            data['email_address'] = $('#email_address').val();
            data['payee_code'] = $('#payee_code').val();
            data['darbandi_srot_id'] = $('#darbandi_srot_id').val();
            data['darbandi_type_id'] = $('#darbandi_type_id').val();
            data['sheet_roll_no'] = $('#sheet_roll_no').val();
            data['sewa_barga'] = $('#sewa_barga').val();
            data['sewa_id'] = $('#sewa_id').val();
            data['samuha_id'] = $('#samuha_id').val();
            data['taha_id'] = $('#taha_id').val();
            data['pad_id'] = $('#pad_id').val();
            data['office_head'] = $('#office_head').val();
            data['acc_head'] = $('#acc_head').val();
            data['is_peski'] = $('#is_peski').val();
            data['is_payroll'] = $('#is_payroll').val();
            data['can_accept'] = $('#can_accept').val();
            data['status'] = $('#status').val();
            data['_token'] = '{{csrf_token()}}';

            let url = '{{route('karmachari.store')}}';
            $.ajax({

                method: 'post',
                url: url,
                data: data,
                success: function (res) {
                    let options = '';
                    let party = $.parseJSON(res);
                    options += '<option value="' + party.id + '" selected>' + party.name_nep + '</option>';

                    $('#' + adding_party_id).append(options);
                    $('#karmachari_modal').modal('hide');

                }
            })
        })
    </script>

    {{--    modal end --}}

    {{--Get हिसाब Number By व्यहोरा--}}
    <script>
        let get_hisab_number_by_byahora = function (byahora) {

            // $('#hisab_number').val('');
            let ledger_type = byahora;

            //बजेट खर्च र पेश्कि हुदा
            if (ledger_type == 1 || ledger_type == 4) {
                let main_activity_id = $('#main_activity_name').val();

                get_expense_head_by_activity_id(main_activity_id);
            }

            let url = '{{route('get_expense_head__by_ledger_type',123)}}';
            url = url.replace(123, ledger_type);

            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let data = $.parseJSON(res);
                    let option = '<option value="">.........</option>';
                    let hisab_number = $.parseJSON(res);

                    if (hisab_number.length > 1) {
                        $.each(data, function () {
                            if (this.id == expenseHeadId) {

                                option += '<option value="' + this.id + '" selected>' + this.expense_head_sirsak + '</option>'
                            } else {
                                option += '<option value="' + this.id + '">' + this.expense_head_sirsak + '</option>'
                            }

                        })
                    } else {
                        option += '<option value="' + hisab_number[0].id + '" selected>' + hisab_number[0].expense_head_sirsak + '</option>'

                    }

                    $('#hisab_number').html(option);
                    getDetailsByHisabNumber();

                }
            })
        }
    </script>

    {{--hisab number change huda--}}
    <script>
        let getDetailsByHisabNumber = function () {
            let bibran = $('#hisab_number option:selected').text();
            $('input#details').val(bibran);
            $('input#details').closest('div').find('p.validation-error').remove();
        };
        $(document).ready(function () {
            $('#hisab_number').change(function () {
                getDetailsByHisabNumber();
            })
        })
    </script>

    {{-- भौचर थप गर्दा हुने  --}}
    <script>
                {{-- Ekal kos thapne bela--}}
        let checkForPeFaAndMakeProgramRequired = function () {

                let ledger_type_id = parseInt($('#byahora').val());
                if (ledger_type_id === 3 || ledger_type_id === 2) {
                    let $table = $('#voucher_table');
                    let $trs = $table.find('tr');
                    $trs.each(function () {
                        let byahora = $.trim($(this).find('td.byahora').attr('data-byahora'));
                        let activity = $.trim($(this).find('td.activity').attr('data-id'));
                        if (byahora == 9) {
                            $('select#main_activity_name').prop('required', true);
                        }
                    });
                }
            };
        let getSerialNumber = function () {
            return $('#voucher_table tbody').find('tr').length;
        };
        let getSerialNumberIf = function () {
            if (editingSN) {
                return editingSN;
            }
            return getSerialNumber();
        };
        let expense = 0;
        let activityForBhuktani = [];
        let voucherDetails = [];
        let crBankAmount = 0;
        let Dr = 0;
        let Cr = 0;
        let paymentAmount = 0;
        $('#voucher_submit').click(function (e) {
            if (validation()) {
                checkForPeFaAndMakeProgramRequired();

                e.preventDefault();
                let crAmount = 0;
                let drAmount = 0;
                let date = $('input#date').val();
                let budget_sub_head_text = $('#program option:selected').text();
                let budget_sub_head_id = $('#program :selected').val();
                let activity_name = $('#main_activity_name option:selected').text();
                activity_name = activity_name.replace(/\d+/g, '').replace(/\|/g, '');
                let activity_id = $('#main_activity_name :selected').val();
                let dr_or_cr_name = $('#DrOrCr option:selected').text();
                let drOrCr = $('#DrOrCr :selected').val();
                let byahora_text = $('#byahora option:selected').text();
                let ledger_type_id = $('#byahora').val(); //व्यहोराको field
                let hisab_number = $('select#hisab_number').val();
                let details = $('#details').val();
                let party_type_id = $('select#party_type').val();
                let party_type = $('#party_type option:selected').text();
                let party_name = $('#party-name option:selected').text();
                let party_id = $('select#party-name').val();
                let amount = $('input#amount').val();
                let nepali_date = $('#date').val();
                let nepali_date_in_english = convertNepaliToEnglish(nepali_date);
                let voucherDetailsId = $('#voucher_details_id').val();

                if (parseInt(drOrCr) == 1 && parseInt(ledger_type_id) == 1) {
                    if (checkRemain()) {
                        //Voucher थप  गर्दा तल Voucher List को  डे र क्रे को Total आउने
                        if (drOrCr == '1') {
                            drAmount = $('#amount').val();
                            Dr = parseFloat(Dr) + parseFloat(drAmount);
                            $('#dr_amount').html(Dr.toFixed(2));
                        }
                        if (drOrCr == '2') {
                            crAmount = $('#amount').val();
                            Cr = parseFloat(Cr) + parseFloat(crAmount);
                            $('#cr_amount').html(Cr.toFixed(2));
                        }

                        //भुक्तानी आदेश मा रकम बाडफाडलाई  validation गर्ने रकम , यो रकम भन्दा बढि भुक्तानि गर्न रोक्ने गरि।
                        if ((drOrCr == '2' && ledger_type_id == 3) || (drOrCr == '2' && ledger_type_id == 8)) {
                            paymentAmount = parseFloat(paymentAmount) + parseFloat(amount);
                        }
                        //भुक्तानि मा कार्यक्रम देखाउन लाइ
                        let obj = activityForBhuktani.find(o => o.activity_id === activity_id);
                        if (!obj) {
                            activityForBhuktani.push({
                                activity_name: activity_name,
                                activity_id: activity_id,
                            })
                        }
                        if (editingSN) {

                            editVoucherDetails = {};
                            editVoucherDetails['drOrCr'] = drOrCr;
                            editVoucherDetails['budget_sub_head'] = budget_sub_head_id;
                            editVoucherDetails['activity_id'] = activity_id;
                            editVoucherDetails['ledger_type_id'] = ledger_type_id;
                            editVoucherDetails['hisab_number'] = hisab_number;
                            editVoucherDetails['amount'] = amount;
                            editVoucherDetails['date_nepali'] = nepali_date;
                            editVoucherDetails['date_english'] = nepali_date_in_english;
                            editVoucherDetails['bibran'] = details;
                            editVoucherDetails['party_type'] = party_type_id;
                            editVoucherDetails['party_id'] = party_id;
                            editVoucherDetails['voucher_details_id'] = voucherDetailsId;

                            voucherDetails[editingSN - 1] = editVoucherDetails;

                        } else {

                            voucherDetails.push({

                                drOrCr: drOrCr,
                                budget_sub_head: budget_sub_head_id,
                                activity_id: activity_id,
                                ledger_type_id: ledger_type_id,
                                hisab_number: hisab_number,
                                amount: amount,
                                date_nepali: nepali_date,
                                date_english: nepali_date_in_english,
                                bibran: details,
                                party_type: party_type_id,
                                party_id: party_id,
                                voucher_details_id: voucherDetailsId,

                            });
                        }

                        let tr = "<tr class='" + getSerialNumberIf() + "' style='background-color: white;'>" +
                            "<td class='sn'>" + getSerialNumberIf() +
                            "</td>" +
                            "<td class='dr-cr'>" +
                            dr_or_cr_name +
                            "</td>" +

                            "<td class='activity' data-id='" + activity_id + "'>" +
                            activity_name +
                            "</td>" +

                            "<td class='byahora' data-byahora='" + ledger_type_id + "'>" +
                            byahora_text +
                            "</td>" +

                            "<td class='hidden expense_head' data-expense-head='" + hisab_number + "'>" +

                            "</td>" +

                            "<td class='details'>" +
                            details +
                            "</td>" +

                            "<td class='drAmount' style='text-align: right'>" +
                            drAmount +
                            "</td>" +

                            "<td class='crAmount' style='text-align: right'>" +
                            crAmount +
                            "</td>" +
                            "<td class='partyType'>" +
                            party_type +
                            "</td>" +
                            "<td class='party'>" +
                            party_name +
                            "</td>" +

                            "<td class='edit-voucher-td'>" +
                            '<a href="#" class="edit-voucher" data-sn="' + getSerialNumberIf() + '">Edit</a> | <a href="#" class="delete-row" data-sn="' + getSerialNumberIf() + '">Delete</a>' +
                            "</td>";
                        if (editingSN) {
                            $(tr).css('background-color', '#fff');
                            $(tr).find('td.sn').text(editingSN);
                            $('#voucher_table').find('tr.' + editingSN).replaceWith(tr);
                        } else {

                            $("#voucher_table tbody tr:last").after(tr);
                        }
                        editingSN = 0;
                        dr_cr_total();

                        //एक पटक थप गरेपछि बजेट उपशि्रषक disabled हुने गरि
                        $('#program').prop("disabled", 'disabled');
                        clearVoucherField();
                    }
                } else {

                    //Voucher थप  गर्दा तल Voucher List को  डे र क्रे को Total आउने
                    if (drOrCr == '1') {
                        drAmount = $('#amount').val();
                        Dr = parseFloat(Dr) + parseFloat($('#amount').val());
                        $('#dr_amount').html(Dr.toFixed(2));
                    }
                    if (drOrCr == '2') {
                        crAmount = $('#amount').val();
                        crAmount = parseFloat(crAmount);
                        Cr = parseFloat(Cr) + parseFloat(crAmount);
                        $('#cr_amount').html(Cr.toFixed(2));
                    }
                    //भुक्तानी आदेश मा रकम बाडफाडलाई  validation गर्ने रकम , यो रकम भन्दा बढि भुक्तानि गर्न रोक्ने गरि।
                    if ((drOrCr == '2' && ledger_type_id == 3) || (drOrCr == '2' && ledger_type_id == 8)) {
                        paymentAmount = parseFloat(paymentAmount) + parseFloat(amount);
                    }

                    //भुक्तानि मा कार्यक्रम देखाउन लाइ
                    let obj = activityForBhuktani.find(o => o.activity_id === activity_id);
                    if (!obj) {
                        activityForBhuktani.push({
                            activity_name: activity_name,
                            activity_id: activity_id,
                        })
                    }

                    if (editingSN) {
                        editVoucherDetails = {};
                        editVoucherDetails['drOrCr'] = drOrCr;
                        editVoucherDetails['budget_sub_head'] = budget_sub_head_id;
                        editVoucherDetails['activity_id'] = activity_id;
                        editVoucherDetails['ledger_type_id'] = ledger_type_id;
                        editVoucherDetails['hisab_number'] = hisab_number;
                        editVoucherDetails['amount'] = amount;
                        editVoucherDetails['date_nepali'] = nepali_date;
                        editVoucherDetails['date_english'] = nepali_date_in_english;
                        editVoucherDetails['bibran'] = details;
                        editVoucherDetails['party_type'] = party_type_id;
                        editVoucherDetails['party_id'] = party_id;
                        editVoucherDetails['voucher_details_id'] = voucherDetailsId;

                        voucherDetails[editingSN - 1] = editVoucherDetails;


                    } else {

                        voucherDetails.push({
                            drOrCr: drOrCr,
                            budget_sub_head: budget_sub_head_id,
                            activity_id: activity_id,
                            ledger_type_id: ledger_type_id,
                            hisab_number: hisab_number,
                            amount: amount,
                            date_nepali: nepali_date,
                            date_english: nepali_date_in_english,
                            bibran: details,
                            party_type: party_type_id,
                            party_id: party_id,
                            voucher_details_id: voucherDetailsId
                        });
                    }

                    let tr = "<tr class='" + getSerialNumberIf() + "' style='background-color: white;'>" +
                        "<td class='sn'>" + getSerialNumberIf() +
                        "</td>" +
                        "<td class='dr-cr'>" +
                        dr_or_cr_name +
                        "</td>" +

                        "<td class='activity' data-id='" + activity_id + "'>" +
                        activity_name +
                        "</td>" +

                        "<td class='byahora' data-byahora='" + ledger_type_id + "'>" +
                        byahora_text +
                        "</td>" +

                        "<td class='hidden expense_head' data-expense-head='" + hisab_number + "'>" +

                        "</td>" +
                        "<td class='details'>" +
                        details +
                        "</td>" +

                        "<td class='drAmount' style='text-align: right'>" +
                        drAmount +
                        "</td>" +

                        "<td class='crAmount' style='text-align: right'>" +
                        crAmount +
                        "</td>" +
                        "<td class='partyType'>" +
                        party_type +
                        "</td>" +
                        "<td class='party'>" +
                        party_name +
                        "</td>" +

                        "<td class='edit-voucher-td'>" +
                        '<a href="#" class="edit-voucher" data-sn="' + getSerialNumberIf() + '">Edit</a> | <a href="#" class="delete-row" data-sn="' + getSerialNumberIf() + '">Delete</a>' +
                        "</td>";
                    if (editingSN) {
                        $(tr).css('background-color', '#fff');
                        $(tr).find('td.sn').text(editingSN);
                        $('#voucher_table').find('tr.' + editingSN).replaceWith(tr);
                    } else {

                        $("#voucher_table tbody tr:last").after(tr);
                    }
                    editingSN = 0;
                    dr_cr_total();

                    //एक पटक थप गरेपछि बजेट उपशि्रषक disabled हुने गरि
                    $('#program').prop("disabled", 'disabled');
                    clearVoucherField();
                }
                let ledger_type_id_ = parseInt($('#byahora').val());
                if (ledger_type_id_ === 9) {

                    let $table = $('#voucher_table');
                    let $trs = $table.find('tr');
                    let ekal_kos_count = 0;
                    $trs.each(function () {
                        let byahora = $.trim($(this).find('td.byahora').attr('data-byahora'));
                        let activity = $.trim($(this).find('td.activity').attr('data-id'));
                        if ((byahora == 3 || byahora == 2) && activity == "") {
                            ekal_kos_count++;
                            if (ekal_kos_count === 1) {
                                $(this).find('td.edit-voucher-td').find('.edit-voucher').trigger('click');
                                $('select#main_activity_name').prop('required', true);
                            } else {
                                $(this).remove();

                            }
                        }
                    });
                }

            } else {
            }
            // For peski farsyot. To remove selected voucher.
            if ($('input.peski-voucher-detail-id').length > 0) {

                let baki = $.trim($('input.peski-voucher-detail-id:checked').closest('tr').find('td.baki').text());
                if (parseInt(baki) == 0) {
                    $('input.peski-voucher-detail-id:checked').closest('tr').remove();
                }
                $('input.peski-voucher-detail-id').prop('disabled', false);
            }
            $('input.peski-voucher-detail-id').prop("checked", false);
            // $('input.peski-voucher-detail-id').prop("checked", false);
            // $('#peskiVoucherTable').hide();

        })
    </script>

    {{--checkRemain funcion--}}

    <script>
        let checkRemain = () => {
            let $hiddenRemainingBudget = $('#hidden_remain_budget');
            let amount = $('input#amount').val();
            let mainActivityId = $('#main_activity_name').val();
            let $existingDebitTds = $('#voucher_table tbody td.activity[data-id="' + mainActivityId + '"]');
            let existingDebitTotal = 0;
            $.each($existingDebitTds, function () {
                if (!$(this).closest('tr').hasClass(editingSN)) {
                    existingDebitTotal += parseFloat($(this).closest('tr').find('td.drAmount').text())
                }
            });
            let remain = $hiddenRemainingBudget.val();
            if (parseFloat(remain) >= parseFloat(amount)) {
                remain = parseFloat(remain) - parseFloat(amount);
                expense = parseFloat(amount) + existingDebitTotal;
                $('#total_expense').text(expense).addClass('e-n-t-n-n');
                $('#hidden_expense').val(expense);
                $('#hidden_remain_budget').val(remain);
                $('#remain_budget').text(remain).addClass('e-n-t-n-n');
                return true;
            } else {
                alert('मौज्दात भन्दा बढि भयो। बाकी:' + remain);
                $('#amount').focus();
                $('#amount').val(remain);
                return false;
            }
            return true;
        }
    </script>


    {{--voucher details ma peski prakar save huna lai--}}
    <script>
        // let peski_type=0;
        // let get_peski_type = function(){
        //     let party_type = $('select#party_type').val();
        //
        //     if(party_type == 5){
        //
        //         return peski_type = 1;
        //     }
        //     else if(party_type == ''){
        //
        //         return 0;
        //     } else {
        //         return  peski_type = 2;
        //
        //     }
        // };
        // $(document).ready(function () {
        //     $('#party_type').change(function () {
        //         get_peski_type();
        //     })
        // })
    </script>

    {{-- भौचर एडिट गर्ने--}}
    <script>
        let editingSN;
        let expenseHeadId = 0;
        $(document).on('click', '.edit-voucher', function () {


            editingSN = parseInt($(this).attr('data-sn'));

            let tds = $(this).parents('tr').find('td');
            $(this).parents('tbody').find('tr').css('background-color', '#FFF');
            $(this).parents('tr').css('background-color', '#FFF1CC');
            let item = [];
            let program_id = $('#program').val();
            $.each(tds, async function () {

                let className = $(this).prop('class');
                if (className == 'dr-cr') {
                    item['dr-cr'] = $(this).html();

                }

                if (className == 'activity') {

                    item['activity'] = $(this).html();
                    item['activity_id'] = $(this).attr('data-id');
                    item['byahora_id'] = $('.byahora').attr('data-byahora');

                }

                if ($(this).hasClass('expense_head')) {

                    expenseHeadId = item['expense_head_id'] = $(this).attr('data-expense-head');
                }

                if (className == 'byahora') {

                    item['byahora'] = $(this).html();
                    item['byahora_id'] = $(this).attr('data-byahora');
                    item['expense_head'] = $('');

                }

                if (className == 'details') {
                    item['details'] = $(this).html();
                }

                if (className == 'crAmount' || className == 'drAmount') {
                    let test = parseInt($(this).html());
                    if (test > 0) {
                        item['amount'] = $(this).html();
                    }
                }

                if (className == 'partyType') {

                    item['partyType'] = $(this).html();
                    $('#party_type').prop("disabled", false);
                }
                if (className == 'party') {

                    item['party'] = $(this).html();
                }

            });

            // बजेट खर्च वा पेश्कि छ भने
            if (item['byahora_id'] == 1 || item['byahora_id'] == 4 || item['byahora_id'] == 9) {

                get_expense_head_by_activity_id(item['activity_id']);
            }

            if (item['byahora_id'] == 2 || item['byahora_id'] == 3) {
                get_hisab_number_by_byahora(item['byahora_id']);

            }
            if (item['byahora_id'] == 12) {

                getUsedExpenseHead(expenseHeadId);

            }

            if (item['byahora_id'] == 7 || item['byahora_id'] == 6) {

                get_expense_head(expenseHeadId);

            }


            let drOrCrOption = $('#DrOrCr').find('option');
            $.each(drOrCrOption, function () {
                if (this.text == item['dr-cr']) {
                    $(this).prop('selected', true);
                }
            });

            $budget_sub_head = $('#program :selected').val();

            let activity_option = $('#main_activity_name').find('option');
            $.each(activity_option, function () {

                if ($(this).val() == item['activity_id']) {
                    $(this).prop('selected', true).parent('select').change();
                }
            });
            //व्यहोराको option लेको।
            let byahoraOption = $('#byahora').find('option');
            $.each(byahoraOption, function () {

                if (this.text == item['byahora']) {

                    $(this).prop('selected', true);
                    let byahora = $('#byahora :selected').val();
                }

            });

            let hisabNumber = $('#hisab_number').val(expenseHeadId).change();
            $.each(hisabNumber, function () {

                if ($(this).val() == expenseHeadId) {

                    $(this).val(expenseHeadId);
                }
            });

            //कार्यक्रमको option लेको।
            let activityOption = $('#main_activity_name').find('option');
            $.each(activityOption, function () {

                if (this.text == item['activity']) {
                    $(this).prop('selected', true);
                }
            });


            let partyTypeOption = $('#party_type').find('option');
            $.each(partyTypeOption, function () {

                if ($(this).text() == item['partyType']) {

                    $(this).prop('selected', true);
                    getPartyByPartyType($(this).val(), item['party']);

                }
            });
            $('#details').val(item['details']).change();
            $('#amount').val(item['amount']);
            // $(e.target).parents('tr').remove();

            let totalExpense = parseFloat($('#hidden_expense').val()) - parseFloat(item['amount']);
            let remainBudget = parseFloat($('#hidden_remain_budget').val()) + parseFloat(item['amount']);

            dr_cr_total();
            dr_cr_equal_check();
            $('#VoucherSave').attr("disabled", true);
            return false;

        });
    </script>

    {{--थप गरेको भौचर delete  गर्ने--}}
    <script>
        let resetSn = function (tabelId) {

            let $trs = $('#' + tabelId + ' tbody').find('tr');
            $.each($trs, function (key, value) {
                $(value).removeClass();
                $(value).find('td.sn').html(key);
                $(value).find('td.edit-voucher-td').find('a').first().attr('data-sn', key);
                $(value).attr('class', key);
            });
            $('select#main_activity_name').change();
        };

        $(document).on('click', '.delete-row', function () {

            var listItemId = $(this).attr('data-sn');
            voucherDetails.splice(listItemId - 1, 1);
            console.clear();
            console.table(voucherDetails);
            let comfirm_result = confirm("are you sure. It will delete the item");
            if (comfirm_result == true) {
                $(this).closest('tr').remove();
                resetSn('voucher_table');
                dr_cr_total();
                return false;
            }

        })

    </script>

    {{-- क्रे र एकल कोष खाता amount total गर्ने--}}
    <script>
        let get_cr_tsa = function () {
            // let bhuktani_main_activity = $('#bhuktani_main_activity').val();
            //
            // let $mainActivityTds = $('#voucher_table tbody td.activity[data-id="'+bhuktani_main_activity+'"]');
            //
            let cr_tsa = 0;
            // let totalDebit = 0;
            // let totalCredit = 0;
            // $.each($mainActivityTds, function () {
            //     let $row = $(this).closest('tr');
            //     let drOrCr = $.trim($row.find('td.dr-cr').text());
            //     if(drOrCr === 'डेबिट'){
            //         let rowDr = parseFloat($row.find('td.drAmount').text());
            //         if(rowDr > 0){
            //             totalDebit += rowDr;
            //         }
            //     }else if(drOrCr === 'क्रेडिट'){
            //         if($.trim($row.find('td.byahora').text()) !== "एकल कोष खाता") {
            //             let rowCr = parseFloat($row.find('td.crAmount').text());
            //             if(rowCr > 0){
            //                 totalCredit += rowCr;
            //             }
            //         }
            //     }
            // });
            // cr_tsa = totalDebit - totalCredit;
            let trs = $('#voucher_table tbody').find('tr').not(':first');
            $.each(trs, function (key, tr) {
                // $.each($(tr).find('td'),function (key, td) {
                //
                // })
                // dr_or_cr_temp = $(tr).find('td.dr-cr').text();
                // dr_or_cr = $.trim(dr_or_cr_temp);
                if (($(tr).find('td.dr-cr').text() == 'क्रेडिट' && $(tr).find('td.byahora').text() == 'एकल कोष खाता') || ($(tr).find('td.dr-cr').text() == 'डेबिट' && $(tr).find('td.byahora').text() == 'एकल कोष खाता')) {
                    cr_tsa += parseFloat($(tr).find('td.crAmount').text())
                }
                //change in 3/4/2020 (computer format)
                // if (($(tr).find('td.dr-cr').text() == 'डेबिट' && $(tr).find('td.byahora').text() == 'पेश्की')) {
                //     cr_tsa += parseFloat($(tr).find('td.drAmount').text())
                // }
            });
            // console.log(cr_tsa);
            return cr_tsa.toFixed(2);

        }
    </script>

    {{--dr_cr_equal_check finction--}}
    <script>
        let dr_cr_equal_check = function () {
            $('#VoucherSave').prop('disabled', false);
            let option = '<option selected>...................</option>';
            $.each(activityForBhuktani, function () {
                option += '<option value="' + this.activity_id + '">' + this.activity_name + '</option>';
                $('#bhuktani_main_activity').html(option);
            })
        }
    </script>

    {{--    Cr Tsa amount set on amoun field--}}
    <script>
        $(document).ready(function () {
            $('#byahora').change(function () {
                let drOrCr = $('#DrOrCr').val();
                let byahora = $('#byahora').val();
                if (drOrCr == 2 && byahora == 3) {
                    let remain = drCrRemaining.toFixed(2) * (-1);
                    $('#amount').val(remain.toFixed(2))
                }
            })
        })
    </script>

    {{-- Dr and Cr total देखाउने function --}}
    <script>
        let drCrRemaining = 0;
        let dr_cr_total = function () {
            let trs = $('#voucher_table tbody').find('tr');
            let DrAmount = 0;
            let CrAmount = 0;
            $.each(trs, function (key, tr) {
                if (!$(this).hasClass(editingSN)) {
                    let newDr = parseFloat($(this).find('td.drAmount').text());
                    if (newDr > 0) {
                        DrAmount += newDr;
                    }
                    let newCr = parseFloat($(this).find('td.crAmount').text());
                    if (newCr > 0) {
                        CrAmount += newCr;
                    }
                }
                drCrRemaining = parseFloat(CrAmount) - parseFloat(DrAmount);
                if (CrAmount.toFixed(2) === DrAmount.toFixed(2)) {
                    $('#VoucherSave').prop('disabled', false);
                    let option = '<option selected>...................</option>';
                    $.each(activityForBhuktani, function () {
                        option += '<option value="' + this.activity_id + '">' + this.activity_name + '</option>';
                        $('#bhuktani_main_activity').html(option);
                    })
                } else {
                    $('#VoucherSave').prop('disabled', "disabled");
                }
            });
            $('#dr_amount').text(DrAmount.toFixed(2));
            $('#cr_amount').text(CrAmount.toFixed(2));
            $('#amount_difference').text(drCrRemaining.toFixed(2));
        }

    </script>

    {{--short narration key up --}}
    <script>
        $(document).ready(function () {
            $('#shortInfo').keyup(function () {
                let shortNarration = $('#shortInfo').val();
                $('#detailsInfo').val(shortNarration);
            })
        })
    </script>

    {{--VOucher End--}}


    {{--Bhuktani start--}}

    {{--on change party_type evvent --}}
    <script>
        $(document).ready(function () {
            $('#bhuktani_party_type').change(function () {

                let party_type_id = $('#bhuktani_party_type :selected').val();
                // if(party_type_id == 5){
                //     getKarmachariForBhuktani();
                // } else {
                getBhuktaniPartyByPartyType(party_type_id);
                // }
            })
        });

        let getKarmachariForBhuktani = function () {

            let url = '{{route('get.karmachari')}}';
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    // console.log($.parseJSON(res));
                    let option = '<option selected>....................</option>';
                    $.each($.parseJSON(res), function () {

                        option += '<option value="' + this.id + '">' + this.name_nepali + '</option>'

                    });
                    $('#bhuktani_party').html(option);
                }
            });
        }

    </script>

    {{--on change party_type function --}}
    <script>
        let getBhuktaniPartyByPartyType = function (party_type_id, party_name = '') {

            let url = '{{route('get_party_name_by_party_type_and_office', 123)}}';
            url = url.replace(123, party_type_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    // console.log($.parseJSON(res));
                    let options = '<option selected>-----------</option>';
                    options += '<option value="add" style="font-style: italic; color: blue; text-decoration: underline">Add New</option>';
                    $.each($.parseJSON(res), function () {
                        // console.log(party_name,this.name_nep);
                        if (party_name == this.name_nep) {
                            options += '<option value="' + this.id + '" selected>' + this.name_nep + '</option>'

                        } else {
                            options += '<option value="' + this.id + '">' + this.name_nep + '</option>'

                        }
                    });
                    $('#bhuktani_party').html(options);
                }
            })
        }
    </script>


    {{-- भुक्तानी थप हुदा --}}
    <script>
        let getBhuktaniSn = function () {
            return $('#bhuktani tbody').find('tr').length;
        };

        let getBhktaniSerialNumberIf = function () {
            if (editingBhuktaniSN) {
                return editingBhuktaniSN;
            }
            return getBhuktaniSn();
        };

        let preBhuktani = [];
        $remaining = 0;
        $(document).ready(function () {
            $('#buktaniButton').click(function () {

                if (bhuktanivalidation()) {

                    let nepali_date = $('#date').val();
                    let nepali_date_in_english = convertNepaliToEnglish(nepali_date);
                    let main_activity_name = $('#bhuktani_main_activity :selected').text();
                    let main_activity_id = $('select#bhuktani_main_activity').val();
                    let party_type_id = $('select#bhuktani_party_type').val();
                    let party_type_text = $('#bhuktani_party_type :selected').text();
                    let bhuktani_party_text = $('#bhuktani_party :selected').text();
                    let bhuktani_party_id = $('select#bhuktani_party').val();
                    let bhuktani_amount = $('#bhuktani_amount').val();
                    let advance_tax_deduction_amount = $('#advance_tax_deduction_amount').val();
                    let vat_amount = $('input#vat_amount').val();
                    let vat_bill_number = $('input#vat_bill_number').val();
                    let pratibadhata_number = $('input#pratibadhata_number').val();
                    let budget_sub_head_id = $('#program :selected').val();
                    let chequeTypeName = $('#chequeType :selected').text();
                    let chequeTypeId = $('select#chequeType').val();

                    // if (bhuktani_total_payment_check()) {

                    var tr = "<tr class='" + getBhktaniSerialNumberIf() + "' style='background-color: white;'>" +
                        "<td class='sn'>" + getBhktaniSerialNumberIf() +

                        "</td>" +

                        "<td class='bhuktani_main_activity' data-bhuktani_activity='" + main_activity_id + "'>" +
                        main_activity_name +
                        "</td>" +

                        "<td class='bhuktani_party_type' data-bhuktani_party_type='" + party_type_id + "'>" +
                        party_type_text +
                        "</td>" +

                        "<td class='bhuktani_party' data-bhuktani_party='" + bhuktani_party_id + "'>" +
                        bhuktani_party_text +
                        "</td>" +

                        "<td class='bhuktani_amount' >" +
                        bhuktani_amount +
                        "</td>" +

                        "<td class='advance_tax_deduction' >" +
                        advance_tax_deduction_amount +
                        "</td>" +

                        "<td class='vat' >" +
                        vat_amount +
                        "</td>" +

                        "<td class='vat_number' >" +
                        vat_bill_number +
                        "</td>" +

                        "<td class='pratibadhata_number'>" +
                        pratibadhata_number +
                        "</td>" +

                        "<td class='cheque_type' data-cheque_type='" + chequeTypeId + "'>" +
                        chequeTypeName +
                        "</td>" +

                        "<td class='edit-voucher-td'>" +
                        '<a href="#" class="edit-bhuktani-row" data-sn="' + getBhktaniSerialNumberIf() + '">Edit</a> | <a href="#"  class="delete-bhuktani" data-sn="' + getBhktaniSerialNumberIf() + '">Delete</a>' +
                        "</td>";

                    if (editingBhuktaniSN) {
                        $(tr).css('background-color', '#fff');
                        $(tr).find('td.sn').text(editingBhuktaniSN);
                        $('#bhuktani').find('tr.' + editingBhuktaniSN).replaceWith(tr);

                        preBhuktaniEdit = [];
                        preBhuktaniEdit['budget_sub_head_id'] = budget_sub_head_id;
                        preBhuktaniEdit['main_activity_id'] = main_activity_id;
                        preBhuktaniEdit['bhuktani_party_type'] = party_type_id;
                        preBhuktaniEdit['bhuktani_party_id'] = bhuktani_party_id;
                        preBhuktaniEdit['bhuktani_amount'] = bhuktani_amount;
                        preBhuktaniEdit['date_nepali'] = nepali_date;
                        preBhuktaniEdit['date_english'] = nepali_date_in_english;
                        preBhuktaniEdit['advance_tax_deduction'] = advance_tax_deduction_amount;
                        preBhuktaniEdit['vat_amount'] = vat_amount;
                        preBhuktaniEdit['vat_bill_number'] = vat_bill_number;
                        preBhuktaniEdit['pratibadhata_number'] = pratibadhata_number;
                        preBhuktaniEdit['chequeTypeId'] = chequeTypeId;

                        let index = editingBhuktaniSN - 1;
                        preBhuktani.slice(index, 1);
                        preBhuktani.splice(index, 1, {
                            budget_sub_head_id: budget_sub_head_id,
                            main_activity_id: main_activity_id,
                            bhuktani_party_type: party_type_id,
                            bhuktani_party_id: bhuktani_party_id,
                            bhuktani_amount: bhuktani_amount,
                            date_nepali: nepali_date,
                            date_english: nepali_date_in_english,
                            advance_tax_deduction: advance_tax_deduction_amount,
                            vat_amount: vat_amount,
                            vat_bill_number: vat_bill_number,
                            pratibadhata_number: pratibadhata_number,
                            chequeTypeId: chequeTypeId
                        });
                    } else {

                        preBhuktani.push({
                            budget_sub_head_id: budget_sub_head_id,
                            main_activity_id: main_activity_id,
                            bhuktani_party_type: party_type_id,
                            bhuktani_party_id: bhuktani_party_id,
                            bhuktani_amount: bhuktani_amount,
                            date_nepali: nepali_date,
                            date_english: nepali_date_in_english,
                            advance_tax_deduction: advance_tax_deduction_amount,
                            vat_amount: vat_amount,
                            vat_bill_number: vat_bill_number,
                            pratibadhata_number: pratibadhata_number,
                            chequeTypeId: chequeTypeId
                        });
                        $('#bhuktani').find('tbody').last('tr').append(tr);
                    }
                    editingBhuktaniSN = 0;
                    bhuktani_total();
                    clearBhuktaniField();
                    dr_cr_total();
                    // } else {
                    //
                    //     alert(" भुक्तानी रकम भन्दा बढी भयो !!");
                    //     $('#bhuktani_amount').val("");
                    //     $('#bhuktani_amount').focus();
                    // }
                }

            });
        })
    </script>

    {{--get_bhuktani_peski_type Function--}}
    <script>
        // let bhuktani_peski_type=0;
        // let get_bhuktani_peski_type = function(){
        //     let bhuktani_party_type = $('select#bhuktani_party_type').val();
        //
        //     if(bhuktani_party_type == 5){
        //
        //         return bhuktani_peski_type = 1;
        //     }
        //     else if(bhuktani_party_type == ''){
        //
        //         return bhuktani_peski_type = 0;
        //
        //     } else {
        //         return  bhuktani_peski_type = 2;
        //
        //     }
        // };
        // $(document).ready(function () {
        //     $('#bhuktani_party_type').change(function () {
        //
        //         get_bhuktani_peski_type();
        //     })
        // })
    </script>

    {{-- थप गरेको भुक्तानि एडिट गर्न --}}
    <script>
        let editingBhuktaniSN;
        $(document).on('click', '.edit-bhuktani-row', function () {
            editingBhuktaniSN = parseInt($(this).attr('data-sn'));
            $(this).parents('tr').css('background-color', '#FFF1CC');
            let bhuktaniTds = $(this).parents('tr').find('td');
            let item = [];
            $.each(bhuktaniTds, function () {

                let className = $(this).prop('class');
                if (className == 'bhuktani_main_activity') {

                    item['bhuktani_main_activity'] = $(this).html();
                    item['data_bhuktani_activity'] = $(this).attr('data-bhuktani_activity');

                }
                if (className == 'bhuktani_party_type') {

                    item['bhuktani_party_type'] = $(this).html();
                    item['data_bhuktani_party_type'] = $(this).attr('data-bhuktani_party_type');

                }
                if (className == 'bhuktani_party') {

                    item['bhuktani_party'] = $(this).html();
                    item['data_bhuktani_party'] = $(this).attr('data-bhuktani_party');
                }
                if (className == 'bhuktani_amount') {

                    item['bhuktani_amount'] = $(this).html();
                }
                if (className == 'advance_tax_deduction') {

                    item['advance_tax_deduction'] = $(this).html();
                }
                if (className == 'vat') {

                    item['vat'] = $(this).html();
                }
                if (className == 'cheque_type') {

                    item['cheque_type'] = $(this).attr('data-cheque_type');

                }

                if (className == 'vat_number') {

                    item['vat_bill_number'] = $(this).html();

                }
                if (className == 'pratibadhata_number') {
                    item['pratibadhata_number'] = $(this).html();
                }
            });

            let activityOption = $('#bhuktani_main_activity').find('option');
            $.each(activityOption, function () {

                if ($(this).val() == item['data_bhuktani_activity']) {
                    $(this).prop('selected', true);
                }
            });

            let partyTypeOption = $('#bhuktani_party_type').find('option');
            $.each(partyTypeOption, function () {

                if ($(this).val() == item['data_bhuktani_party_type']) {

                    $(this).prop('selected', true);
                    getBhuktaniPartyByPartyType($(this).val(), item['bhuktani_party']);

                }
            });


            $('#bhuktani_amount').val(item['bhuktani_amount']);
            $('#advance_tax_deduction_amount').val(item['advance_tax_deduction']);
            $('#vat_amount').val(item['vat']);


            let chequeTypeOptions = $('#chequeType').find('option');
            $.each(chequeTypeOptions, function () {
                if ($(this).val() == item['cheque_type']) {

                    $(this).prop('selected', true);
                }
            });

            $('#vat_bill_number').val(item['vat_bill_number']);
            $('#pratibadhata_number').val(item['pratibadhata_number']);
            $('#VoucherSave').attr('disabled', true);
            return false;

        })
    </script>


    {{--थप गरेको भुक्तानि Delete गर्न--}}
    <script>
        $(document).on('click', '.delete-bhuktani', function () {

            let index = parseInt($(this).closest('td').find('.edit-bhuktani-row').attr('data-sn')) - 1;
            $(this).closest('tr').remove();
            preBhuktani.splice(index, 1);
            resetSn('bhuktani');
            bhuktani_total();
            return false;

        })
    </script>


    {{--Bhuktani amount total calculation--}}
    <script>
        let bhuktani_total = function () {

            let trs = $('#bhuktani').find('tr');
            let bhuktaniAmount = 0;
            $.each(trs, function (key, tr) {
                $.each($(tr).find('td'), function (key_, td) {

                    if ($(td).prop('class') == 'bhuktani_amount') {

                        let newAmount = $(this).text();
                        if (newAmount) {
                            bhuktaniAmount += parseFloat(newAmount);

                        }
                    }
                })
            });
            $('#bhuktaniTotal').text(bhuktaniAmount.toFixed(2));
        }
    </script>


    {{--Function for take total in bhuktani table and compare with cr. bank value--}}
    <script>
        let bhuktani_total_payment_check = function () {

            let current_payment = $('#bhuktani_amount').val();
            let bhuktani_main_activity = $('#bhuktani_main_activity').val();
            let $bhuktaniTds = $('table#bhuktani tbody td.bhuktani_main_activity[data-bhuktani_activity="' + bhuktani_main_activity + '"]');
            let trs = $('#bhuktani').find('tr');
            let bhuktaniAmount = 0;

            // $.each($bhuktaniTds, function () {
            //
            //     let $row = $(this).closest('tr');
            //     let bhuktaniAmt = $row.find('td.bhuktani_amount').text();
            //     bhuktaniAmount += parseFloat(bhuktaniAmt);
            // });
            $.each(trs, function (key, tr) {
                $.each($(tr).find('td'), function (key_, td) {

                    if ($(td).prop('class') == 'bhuktani_amount') {

                        let newAmount = $(this).text();
                        if (newAmount) {
                            bhuktaniAmount += parseFloat(newAmount);
                        }
                    }
                })
            });
            let x;
            let previous_value = 0;
            if (editingBhuktaniSN) {
                previous_value = $('table#bhuktani').find('tr' + '.' + editingBhuktaniSN).find('td.bhuktani_amount').text();
                x = parseFloat(bhuktaniAmount) + parseFloat(current_payment) - previous_value;
            } else {
                x = parseFloat(bhuktaniAmount) + parseFloat(current_payment);
            }
            let tsa_amount = get_cr_tsa();
            return parseFloat(x) <= parseFloat(tsa_amount);
        }
    </script>

    {{--Bhuktani end--}}


    {{-- भौचर Save --}}

    {{-- भौचर save हुने --}}
    <script>
        function voucherValidationAndSave() {

            let shortInfo = $('#shortInfo').val();
            let detailsInfo = $('#detailsInfo').val();
            if (shortInfo && detailsInfo) {
                voucherSave();
                alert("Voucher Saved");
            } else {
                if (!shortInfo) {
                    if ($('#shortInfo').siblings('p').length == 0) {
                        $('#shortInfo').after('<p style="color:red">लेख्नुहोस​!</p>');
                        $('#shortInfo').focus();
                    }
                    if (!detailsInfo) {
                        if ($('#detailsInfo').siblings('p').length == 0) {
                            $('#detailsInfo').after('<p style="color:red">लेख्नुहोस​!</p>');
                            $('#detailsInfo').focus();
                        }
                    }
                }
            }
        }

        function voucherSave() {

            let token = '{{csrf_token()}}';
            let nepali_date = $('#date').val();
            let nepali_date_in_english = convertNepaliToEnglish(nepali_date);
            let budget_sub_head_id = $('#program :selected').val();
            let voucher_number = parseInt($('#hidden_voucher_numbmer').text());
            tsa_amount = get_cr_tsa();
            let payment_amount = tsa_amount;
            let narration_short = $('#shortInfo').val();
            let narration_long = $('#detailsInfo').val();
            let voucher = [];
            voucher = {
                voucher_number: voucher_number,
                budget_sub_head_id: budget_sub_head_id,
                paymentAmount: payment_amount,
                narration_short: narration_short,
                narration_long: narration_long,
                date_nepali: nepali_date,
                date_english: nepali_date_in_english,

            };
            $.ajax({

                method: 'post',
                url: '{{route('voucher.store')}}',
                data: {
                    voucher: voucher,
                    voucherDetails: voucherDetails,
                    preBhuktani: preBhuktani,
                    _token: token,
                },
                success: function (resp) {
                    if (resp.success) {
                        let msg = "Good";

                        $('.flash-message').fadeIn("fast");
                        setTimeout(function () {
                            $('.flash-message').fadeOut("slow");
                        }, 1000);
                        voucher = null;
                        voucherDetails = [];
                        preBhuktani = [];
                        $('#VoucherSave').prop("disabled", true);
                        location.reload();
                    } else {
                        $('.flash-message').fadeIn("fast");
                        setTimeout(function () {
                            $('.flash-message').fadeOut("slow");
                        }, 1000);
                        $('.flash-message').html(resp.responseText);
                    }
                }
            })
        }
    </script>


    {{-- Fixed --}}

    {{-- Validation   --}}
    <script>
        $('.form-control').change(function () {
            let val = $(this).val();
            if (val) {
                $(this).closest('div').find('p.validation-error').remove();
            }
        });

        function validation() {
            let flag = 1;
            // program name validation
            let programId = $('#program');
            if (!programId.val()) {
                if (programId.siblings('p').length == 0) {
                    programId.after('<p class="validation-error">छान्नुहोस!</p>');
                    programId.addClass("error");
                }
                programId.focus();
                flag = 0;
            } else {
                programId.siblings('p').remove()
            }
            // program name validation end

            //main_activity_name validation

            let mainactivitynameId = $('#main_activity_name');
            let DrOrCr = $('#DrOrCr').val();
            let ledgerType = $('#byahora').val();

            if ((DrOrCr == 2 && ledgerType == 8) || (DrOrCr == 2 && ledgerType == 12) || (DrOrCr == 1 && ledgerType == 11) || (DrOrCr == 1 && ledgerType == 2) || (DrOrCr == 1 && ledgerType == 3) || (DrOrCr == 2 && ledgerType == 3) || (DrOrCr == 2 && ledgerType == 2) || (DrOrCr == 1 && ledgerType == 6) || (DrOrCr == 1 && ledgerType == 7) || (DrOrCr == 2 && ledgerType == 11) || (DrOrCr == 2 && ledgerType == 6)) {
                mainactivitynameId.siblings('p').remove()
            } else {
                if (!mainactivitynameId.val()) {
                    if (mainactivitynameId.siblings('p').length == 0) {
                        mainactivitynameId.after('<p class="validation-error">छान्नुहोस!</p>')
                    }
                    mainactivitynameId.focus();
                    flag = 0;
                } else {
                    mainactivitynameId.siblings('p').remove()
                }
            }

            if (ledgerType == 3) {
                let $table = $('#voucher_table');
                let $trs = $table.find('tbody tr');
                $trs.each(function () {
                    let byahora = $.trim($(this).find('td.byahora').attr('data-byahora'));
                    if (byahora == 9) {
                        if (!mainactivitynameId.val()) {
                            if (mainactivitynameId.siblings('p').length == 0) {
                                mainactivitynameId.after('<p class="validation-error">छान्नुहोस!</p>')
                            }
                            mainactivitynameId.focus();
                            flag = 0;
                        }
                    }
                });
            }
            if (ledgerType == 2) {
                let $table = $('#voucher_table');
                let $trs = $table.find('tbody tr');
                $trs.each(function () {
                    let byahora = $.trim($(this).find('td.byahora').attr('data-byahora'));
                    if (byahora == 9) {
                        if (!mainactivitynameId.val()) {
                            if (mainactivitynameId.siblings('p').length == 0) {
                                mainactivitynameId.after('<p class="validation-error">छान्नुहोस!</p>')
                            }
                            mainactivitynameId.focus();
                            flag = 0;
                        }
                    }
                });
            }

            let partyTypeId = $('#party_type');
            if (ledgerType == 4 || ledgerType == 7 || ledgerType == 9 || ledgerType == 12) {

                if (!partyTypeId.val()) {
                    if (partyTypeId.siblings('p').length == 0) {

                        partyTypeId.after('<p class="validation-error">छान्नुहोस!</p>')
                    }
                    partyTypeId.focus();
                    flag = 0;
                } else {
                    partyTypeId.siblings('p').remove();

                }
            }

            let partyName = $('#party-name');
            if (ledgerType == 4 || ledgerType == 7 || ledgerType == 9 || ledgerType == 12) {

                if (!partyName.val()) {
                    if (partyName.siblings('p').length == 0) {

                        partyName.after('<p class="validation-error">छान्नुहोस!</p>')
                    }
                    partyName.focus();
                    flag = 0;
                } else {
                    partyName.siblings('p').remove()

                }
            }


            //main_activity_name validation end
            //hisab number start
            let hisab_number = $('#hisab_number');
            if (!hisab_number.val()) {
                if (hisab_number.siblings('p').length == 0) {
                    hisab_number.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                hisab_number.focus();
                flag = 0;
            } else {
                hisab_number.siblings('p').remove()
            }
            //details validation

            let detailsId = $('#details');
            if (!detailsId.val()) {
                if (detailsId.siblings('p').length == 0) {
                    detailsId.after('<p class="validation-error">लेख्नुहोस​!</p>')
                }
                detailsId.focus();
                flag = 0;
            } else {
                detailsId.siblings('p').remove()
            }

            //details validation end


            //amount validation

            let amountsId = $('#amount');

            if ((!amountsId.val() || parseInt(amountsId.val()) < 1) && amountsId.length > 0) {
                if (amountsId.siblings('p').length == 0) {
                    amountsId.after('<p class="validation-error">लेख्नुहोस​!</p>')
                }
                amountsId.focus();
                flag = 0;
            } else {
                amountsId.siblings('p').remove()
            }
            //amount validation end
            return flag;


        }

        function bhuktanivalidation() {

            let flag = 1;
            let mainActivityId = $('#bhuktani_main_activity');
            if (!mainActivityId.val()) {
                if (mainActivityId.siblings('p').length == 0) {
                    mainActivityId.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                mainActivityId.focus();
                flag = 0;
            } else {
                mainActivityId.siblings('p').remove()
            }

            let partyTypeId = $('#bhuktani_party_type');
            if (!partyTypeId.val()) {
                if (partyTypeId.siblings('p').length == 0) {
                    partyTypeId.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                partyTypeId.focus();
                flag = 0;
            } else {
                partyTypeId.siblings('p').remove()
            }

            let partyId = $('#bhuktani_party');
            if (!partyId.val()) {
                if (partyId.siblings('p').length == 0) {
                    partyId.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                partyId.focus();
                flag = 0;
            } else {
                partyId.siblings('p').remove()
            }

            let bhuktaniAmount = $('#bhuktani_amount');
            if (!bhuktaniAmount.val()) {
                if (bhuktaniAmount.siblings('p').length == 0) {
                    bhuktaniAmount.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                bhuktaniAmount.focus();
                flag = 0;
            } else {
                bhuktaniAmount.siblings('p').remove()
            }
            return flag;
        }


    </script>

    {{--Date Picker --}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);
    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            var charArray = input.split('');
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1';
                        break;
                    case '२':
                        engDate += '2';
                        break;
                    case '३':
                        engDate += '3';
                        break;
                    case '४':
                        engDate += '4';
                        break;
                    case '५':
                        engDate += '5';
                        break;
                    case '६':
                        engDate += '6';
                        break;
                    case '०':
                        engDate += '0';
                        break;
                    case '७':
                        engDate += '7';
                        break;
                    case '८':
                        engDate += '8';
                        break;
                    case '९':
                        engDate += '9';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
            });
            return engDate

        }
    </script>

    {{--Clear Voucher and Bhuktani field when  Add button click--}}
    <script>
        function clearVoucherField() {

            $('#hisab_number').val(null).trigger('change');
            $('#details').val('');
            $('#party_type').val('');
            $('#party-name').val(null).trigger('change');
            $('#amount').val('');

        }

        function clearBhuktaniField() {

            $('select#bhuktani_main_activity').val('');
            $('#bhuktani_party_type').val('');
            $('#bhuktani_party').val(null).trigger('change');
            $('#bhuktani_amount').val('');
            $('#advance_tax_deduction_amount').val('');
            $('#vat_amount').val('');
            $('#bank').val('');
            $('#vat_bill_number').val('');
            $('#pratibadhata_number').val('');

        }
    </script>




    {{--   For karmachari modal--}}


    {{--  sewa बाट तह आउने--}}
    <script>
        $('#sewa_id').change(function () {

            let sewa_id = $('select#sewa_id').val();
            let url = '{{route('get_taha_by_sewa',123)}}';
            url = url.replace('123', sewa_id);

            $.ajax({

                method: 'get',
                url: url,
                success: function (res) {

                    let tahas = $.parseJSON(res);

                    let options = '<option>.........</option>';

                    $.each(tahas, function () {
                        options += '<option value="' + this.id + '">' + this.name + '</option>';
                    });

                    $('#taha_id').html(options);
                }
            })
        })
    </script>

    {{--  Province_id click हुदा district आउने--}}
    <script>
        $(document).ready(function () {
            $('#province').change(function () {
                let province_id = $('select#province').val();

                let url = '{{route('get.district',123)}}';
                url = url.replace(123, province_id);

                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {
                        let options = '<option selected>....................</option>';
                        // console.log($.parseJSON(res));
                        $.each($.parseJSON(res), function () {

                            options += '<option value="' + this.id + '">' + this.name + '</option>';
                        });

                        $('#district').html(options);
                    }
                })
            })
        })
    </script>

    {{--  District click हुदा local level आउने--}}
    <script>
        $(document).ready(function () {
            $('#district').change(function () {
                let district_id = $('select#district').val();

                let url = '{{route('get.local.level',123)}}';
                url = url.replace(123, district_id);

                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {
                        let options = '<option selected>....................</option>';
                        $.each($.parseJSON(res), function () {

                            options += '<option value="' + this.id + '">' + this.name + '</option>';
                        });

                        $('#local_level').html(options);
                    }
                })
            })
        })
    </script>
    {{--  Date picker--}}
    <script>
        $("#date_of_birth").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date_of_birth").val(formatedNepaliDate);

    </script>


    {{--  Sewa bata Samuha Aaune--}}

    <script>
        let sewaBySamuha = function (this_) {
            let sewa_id = $(this_).val();

            // alert('inside sewabysamuha sewa id = '+sewa_id);

            if (sewa_id) {

                let url_ = '{{route('samuha.getbysewa', '123')}}';
                url_ = url_.replace('123', sewa_id);
                $.ajax({
                    url: url_,  // Route Name here
                    type: "GET",
                    dataType: "json",
                    success: function (data) {

                        // console.log(data);
                        let options = '';

                        $('#samuha_id').empty();
                        $.each(data, function (key, value) {
                            options += '<option value="' + this.id + '">' + this.name + '</option>';
                        });
                        $('#samuha_id').append(options).change();

                    }
                })
            } else {
                $('$samuha_id').empty();
            }

        };

        $(document).ready(function () {

            $('#sewa_id').on('change', function () {  // Make Separate function with code inside this function and call it from here

                sewaBySamuha(this);
            });

        });
    </script>

    {{--  sreni/taha bata Pad Aaune--}}

    <script>
        let padByTaha = function (this_) {
            let taha_id = $(this_).val();

            if (taha_id) {
                //alert("samuha id changed");
                let url_ = '{{route('admin.designations.getbyTaha', '123')}}';
                url_ = url_.replace('123', taha_id);
                $.ajax({
                    url: url_,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        //alert('on success');
                        //console.log(data);
                        $('#pad_id').empty();
                        let options = '';
                        $.each(data, function (key, value) {
                            options += '<option value="' + key + '">' + value + '</option>';
                        });
                        $('#pad_id').append(options).change();

                    }
                })
            } else {
                $('#pad_id').empty();
            }
        };


        $(document).ready(function () {

            $('#taha_id').on('change', function () {
                padByTaha(this);
            });

        });

    </script>


    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            // console.log(input);
            var charArray = input.split('');
            //console.log(charArray);
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1';
                        break;
                    case '२':
                        engDate += '2';
                        break;
                    case '३':
                        engDate += '3';
                        break;
                    case '४':
                        engDate += '4';
                        break;
                    case '५':
                        engDate += '5';
                        break;
                    case '६':
                        engDate += '6';
                        break;
                    case '०':
                        engDate += '0';
                        break;
                    case '७':
                        engDate += '7';
                        break;
                    case '८':
                        engDate += '8';
                        break;
                    case '९':
                        engDate += '9';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
                //console.log(engDate)
            });
            return engDate
        }
    </script>
@endsection
                                                                                                                                                                                                                                                                                                                                                                        