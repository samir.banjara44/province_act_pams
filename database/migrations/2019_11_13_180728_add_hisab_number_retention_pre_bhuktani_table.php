<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHisabNumberRetentionPreBhuktaniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('retention_pre_bhuktani', function (Blueprint $table) {
            $table->integer('hisab_number')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('retention_pre_bhuktani', function (Blueprint $table) {
            $table->dropColumn('hisab_number');
        });
    }
}
