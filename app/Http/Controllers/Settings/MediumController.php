<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Medium;
use App\Repositories\MediumRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MediumController extends Controller
{

    private $mediumRepo;

    public function __construct(MediumRepository $mediumRepo)
    {
        $this->middleware('auth');
        $this->mediumRepo = $mediumRepo;
    }

    public function index()
    {
        $mediums= $this->mediumRepo->get_all_mediums();
        return view('backend.settings.mediums.index', compact('mediums'));
    }

    public function create(){

        return view('backend.settings.mediums.create');
    }
    public function store(Request $request){

        $medium = $this->mediumRepo->create($request->all());
        return redirect(route('admin.mediums'));
    }

}
