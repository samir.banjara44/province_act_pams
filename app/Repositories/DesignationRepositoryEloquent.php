<?php
namespace App\Repositories;

use App\Models\Designation;
use Illuminate\Support\Facades\Auth;

class DesignationRepositoryEloquent implements DesignationRepository
{
    public function create($attributes) {
        $designation = new Designation();
        $designation->name = $attributes['name'];
        $designation->samuha_id = $attributes['taha_id'];  //data base ma taha_id nai बनाउनु पर्ने ,पछि बनाउने गरि
        $designation->status = 1;
        $designation->save();
        return $designation;
    }

    public function get_all_designations()
    {
        return Designation::all();
    }
}