@extends('frontend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> <i class="fas fa-university"></i> चेक विवरण </h1>

            <button type="button" class="btn btn-primary btn-sm btn-show-form" id="showAddForm"><i class="fas fa-plus"></i>
                नयाँ थप गर्ने</button>
        </section>

        <!-- FORM section start -->
        <section class="formSection displayNone" id="showThisForm">
            <div class="row justify-content-md-center">
                <div class="col-md-8 form-bg col-md-offset-2">
                    <button type="button" class="btn btn-danger btn-sm btn-show-form displayNone" id="closeAddForm"><i
                            class="fas fa-times"></i> फर्म रद्ध गर्ने</button>
                    <div class="form-title">चेक विवरण प्रविष्टी गर्ने</div>
                    <form action="{{ route('cheque.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="name">बैङ्कको नाम :</label>
                                <select name="bank_id" class="form-control" required>
                                    <option value="">---बैङ्कको छान्नुहोस्---</option>
                                    @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="address">From :</label>
                                <input type="text" class="form-control" id="from" placeholder="Cheque start number"
                                    name="from" required>
                            </div>
                            <div class="col-md-6">
                                <label for="address">To :</label>
                                <input type="text" class="form-control" id="to" placeholder="cheque end number"
                                    name="to" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2 col-md-offset-5">
                                <button type="submit" class="btn btn-primary btn-show-form btn-block"
                                    style="margin-top: 18px;">थप</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- FORM section ends -->

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body pt-15">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>क्र.स.</th>
                                <th>बैङ्कको नाम</th>
                                <th>चेक नम्बर देखि </th>
                                <th>चेक नम्बर सम्म </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cheques as $index => $cheque)
                                <tr>
                                    <td>{{ ++$index }}</td>
                                    <td>{{ $cheque->bank->name }}</td>
                                    <td class="kalimati">{{ $cheque->from }}</td>
                                    <td class="kalimati">{{ $cheque->to }}</td>

                                    <td>
                                        <a type="button" href="#" class="btn btn-sm btn-primary"><i
                                                class="fa fa-pencil"></i> Edit</a>
                                        <a type="button" href="#" id="" data-bank-id="{{ $bank->id }}"
                                            class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i> Delete</a>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).on('click', '#delete_bank', function() {

            let bank_id = $(this).attr('data-bank-id');
            let url = '{{ route('bank.delete', 123) }}';
            url = url.replace('123', bank_id);
            swal({
                title: "Are you sure?",
                text: "Delete भए पछि Recovere हुदैन",
                icon: "warning",
                buttons: true,
                dangerMode: true,

            }).then((willDelete) => {

                if (willDelete) {

                    $.ajax({

                        url: url,
                        method: 'get',
                        success: function(res) {
                            if (res) {
                                swal("Bank has been deleted!", {
                                    icon: "success",
                                });

                                location.reload();
                            }

                        }
                    })

                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
        })
    </script>
@endsection
