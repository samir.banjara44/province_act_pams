<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivityProgressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_progress', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('office_id');
            $table->integer('budget_sub_head_id');
            $table->integer('budget_details_id');
            $table->text('activity_progress_detail');
            $table->string('date_nep');
            $table->integer('chaumasik_type')->null;
            $table->integer('month')->null;
            $table->integer('status');
            $table->integer('user_id');
            $table->string('fiscal_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_progress');
    }
}
