
@extends('frontend.layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><i class="fas fa-users"></i> पेश्की/भुक्तानी पाउने/धरौटी/व्यक्तिगत खाता </h1>
    
    <button type="button" class="btn btn-primary btn-sm btn-show-form" id="showAddForm"><i class="fas fa-plus"></i> नयाँ थप गर्ने</button>
    
  </section>
  
  <!-- FORM section start -->
  <section class="formSection displayNone" id="showThisForm">
    <div class="row justify-content-md-center">
      <div class="col-md-10 col-md-offset-1 form-bg">

        <button type="button" class="btn btn-danger btn-sm btn-show-form displayNone" id="closeAddForm"><i class="fas fa-times"></i> फर्म रद्ध गर्ने</button>

        <div class="form-title">पेश्की/भुक्तानी पाउने/धरौटी/व्यक्तिगत खाता थप गर्ने</div>
        <form action="{{route('AdvanceAndPayment.store')}}" method="post">
          {{csrf_field()}}

          <div class="form-group">
            <div class="row">

              <div class="col-md-2">
                <label for="party_type" style="display: block;">प्रकार</label>
                <select name="party_type" required class="form-control">
                  @foreach($party_types as $party_type)
                  @if($party_type->id != 5)
                  <option value="{{$party_type->id}}">{{$party_type->name}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
              
              <div class="col-md-5">
                <label for="name_nep">नाम नेपाली</label>
                <input type="text" class="form-control" id="name_nep" placeholder="नाम नेपाली" name="name_nep" required>
              </div>
              
              <div class="col-md-5">
                <label for="name_eng">नाम अंग्रेजी</label>
                <input type="text" class="form-control" id="name_eng" placeholder="नाम अंग्रेजी" name="name_eng">
              </div>
              
            </div>
          </div>
          
          
          <div class="form-group">
            <div class="row">

              <div class="col-md-2">
                <label for="citizen_number">नागरिकता नं.</label>
                <input type="text" class="form-control" id="citizen_number" placeholder="नागरिकता नं." name="citizen_number" >
              </div>
              
              <div class="col-md-2">
                <label for="mobile_number">मोबाइल नं.</label>
                <input type="text" class="form-control" id="mobile_number" placeholder="मोबाइल नं." name="mobile_number" >
              </div>
              
              <div class="col-md-3">
                <label for="vat_number">भ्याट/प्यान नं.</label>
                <input type="text" class="form-control" id="vat_number" placeholder="भ्याट/प्यान नं." name="vat_number" >
              </div>
              
              <div class="col-md-5">
                <label for="vat_office">स्थायी लेखा नम्वर जारी गर्ने कार्यालय र ठेगाना</label>
                <select id="vat_office" class="form-control select2" name="vat_office">
                  <option value="" selected="">.........</option>
                  @foreach($vat_offices as $vat_office)
                  <option value="{{$vat_office->id}}">{{$vat_office->name}}</option>
                  @endforeach
                </select>
              </div>
              
            </div>
          </div>
          
          <div class="form-group">
            <div class="row">

              <div class="col-md-3">
                <label for="bank">बैङ्क</label>
                <select id="bank" name="bank" class="form-control select2">
                  <option value="" selected="">.........</option>
                  @foreach($all_banks as $bank)
                  <option value="{{$bank->id}}">{{$bank->name}}</option>
                  @endforeach
                </select>
              </div>
              
              <div class="col-md-3">
                <label>बैङ्कको शाखा कार्यालय</label>
                <input type="text" class="form-control" name="bank_address" id="bank_address" >
              </div>
              
              <div class="col-md-3">
                <label>बैङ्क खाता नं.</label>
                <input type="text" class="form-control" name="khata_number" id="khata_number">
              </div>
              
              <div class="col-md-3">
                <label>पेयी कोड </label>
                <input type="number" class="form-control" name="payee_code" id="payee_code">
              </div>
              
            </div>
          </div>
          
          <div class="form-group">
            <div class="row">

              <div class="col-md-3">
                <label for="is_advance">पेश्किमा देखाउने</label>
                <select name="is_advance" class="form-control" >
                  <option value="0">हो</option>
                  <option value="2">होइन</option>
                </select>
              </div>
              
              <div class="col-md-3">
                <label for="is_bhuktani">भुक्तानीमा देखाउने</label>
                <select name="is_bhuktani"  class="form-control">
                  <option value="0">हो</option>
                  <option value="2">होइन</option>
                </select>
              </div>
              
              <div class="col-md-3">
                <label for="is_dharauti">धरौटीमा देखाउने</label>
                <select name="is_dharauti"  class="form-control">
                  <option value="0">हो</option>
                  <option value="2">होइन</option>
                </select>
              </div>
              
              <div class="col-md-3">
                <label for="">&nbsp;</label>
                <button type="submit" class="btn btn-primary btn-block"> Save</button>
              </div>
              
            </div>
          </div>
        </form>
        
      </div>
    </div>
  </section>
  <!-- FORM section ends -->
  
  <!-- Main content -->
  <section class="content">
    <div class="panel panel-primary">
      <div class="panel-body pt-10">
        
        <table class="table" style="background-color: #dbdbdb ;color: black;">
          <thead>
            <tr>
              <th>क्र.सं.</th>
              <th>प्रकार</th>
              <th>नाम नेपाली</th>
              <th>पेयी कोड</th>
              <th colspan="2">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $i =1;
            ?>
            @foreach($advanceAndPayments as $advanceAndPayment)
            @if($advanceAndPayment->party_type !=5)
            <tr style="background-color: white;">
              <td>{{$i++}}</td>
              <td>
                @if($advanceAndPayment->get_party_type)
                {{$advanceAndPayment->get_party_type->name}}
                @endif
              </td>
              <td>{{$advanceAndPayment->name_nep}}</td>
              <td>{{$advanceAndPayment->payee_code}}</td>
              
              <td align="center">
                <a type="button" href="{{route('AdvanceAndPayment.edit',$advanceAndPayment->id)}}" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>
              </td>
              <td align="center">
                @if($advanceAndPayment->preBhktani()->count() == 0)
                <a type="button" href="#" id="delete_advance_payment" data-advance-payment-id="{{$advanceAndPayment->id}}" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>
                @endif
              </td>
            </tr>
            @endif
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>

@endsection

@section('scripts')
<script>
  $(document).on('click','#delete_advance_payment',function () {
    
    let advance_payment_id = $(this).attr('data-advance-payment-id');
    let url = '{{route('advance.payment.delete',123)}}';
    url = url.replace('123',advance_payment_id);
    swal({
      title: "Are you sure?",
      text: "Delete भए पछि Recovere हुदैन",
      icon: "warning",
      buttons: true,
      dangerMode: true,
      
    }).then((willDelete) => {
      
      if (willDelete) {
        
        $.ajax({
          
          url : url,
          method : 'get',
          success : function (res) {
            if(res){
              swal("Bank has been deleted!", {
                icon: "success",
              });
              
              location.reload();
            }
            
          }
        })
        
      } else {
        // swal("Your imaginary file is safe!");
      }
    });
  })
</script>


@endsection