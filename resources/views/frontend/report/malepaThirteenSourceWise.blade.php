<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">


<style>
    table {
        border-collapse: collapse;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="font-size:13px">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
                    सभा @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6"
                    style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    @if(Auth::user()->office->department)
                        {{Auth::user()->office->department->name}}
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->name}},
                    , {{Auth::user()->office->district->name}}</td>
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>फाटँबारी</b><br>
                        <b><span class="e-n-t-n-n">{{$fiscalYear->year}}</span> साल {{$month->name}} महिना</b>
                    </div>
                    <div style="float: right; margin-top: -20px">
                        म.ले.प.फा.नं. २१०
                    </div>
                </td>
            </tr>
            <tr>
                <td class="kalimati">बजेट उपशीर्षक न. : <b><span
                                class="kalimati">{{$programs->program_code}}</span></b><br>
                    बजेट उपशीर्षक नाम: <b>{{$programs->name}}</b></td>
                <td class="kalimati" style="text-align: right">आ.व.: <span
                            class="kalimati">{{$fiscalYear->year}}</span></td>
            </tr>

        </table>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table" width="99%" border="1" style="background-color:#dbdbdb; font-size: 12px"
                       id="fatwari_table">
                    <thead>
                    <tr>
                        <th rowspan="2">स्रोत प्रकार</th>
                        <th rowspan="2">स्रोत</th>
                        <th rowspan="2">खर्च संकेत</th>
                        <th rowspan="2" style="width: 10%">विवरण</th>
                        <th rowspan="2" style="width: 10%">अन्तिम बजेट</th>
                        <th>यस महिना सम्मको निकासा</th>
                        <th>गत महिना सम्मको खर्च</th>
                        <th>यस महिनाको खर्च</th>
                        <th>यस महिना सम्मको खर्च</th>
                        <th>पेश्की</th>
                        <th>पेश्की बाहेक खर्च रकम</th>
                        <th>बाँकी बजेट</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sourceTypes as $index=>$sourceType)
                        <?php
                        $i = 1;
                        ?>
                        @foreach($sourceType->getSource($requestData['office'],$requestData['fiscal_year'],$requestData['budget_sub_head'],$sourceType->id) as $source)
                            @foreach($source->getExpenseHead($requestData['office'],$requestData['fiscal_year'],$requestData['budget_sub_head'],$source->id) as $index=>$expenseHead)
                                <tr style="background-color: white">
                                    <td class="kalimati">@if($i){{$sourceType->name}}@endif</td>
                                    <td>{{$source->name}}</td>
                                    <td class=""><span class="e-n-t-n-n">{{$expenseHead->expense_head_by_id->expense_head_code}}</span>
                                    </td>
                                    <td class="">{{$expenseHead->expense_head_by_id->expense_head_sirsak}}</td>
                                    <td class="total-budget kalimati"
                                        style="text-align: right">{{number_format($source->getLastBudget($requestData['fiscal_year'],$expenseHead->expense_head_by_id->expense_head_code,$requestData['budget_sub_head'],$source->id),2)}}</td>
                                    <td class="up-to-this-month-nikasa kalimati" align="right">@moneyFormat($source->get_expense_upto_this_month($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id,$expenseHead->expense_head_by_id->expense_head_code))</td>
                                    <td class="up-to-pre-month-exp kalimati" align="right">@moneyFormat($source->get_expense_upto_prev_month($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id,$expenseHead->expense_head_by_id->expense_head_code))</td>
                                    <td class="this-month-exp kalimati" align="right">@moneyFormat($source->get_expense_this_month($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id,$expenseHead->expense_head_by_id->expense_head_code))</td>
                                    <td class="up-to-this-month-exp kalimati" align="right">@moneyFormat($source->get_expense_upto_this_month($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id,$expenseHead->expense_head_by_id->expense_head_code))</td>
                                    <td class="advance kalimati" align="right">@moneyFormat($source->get_peski_upto_this_month($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id,$expenseHead->id))</td>
                                    <td class="exp-with-out-advance kalimati" align="right">@moneyFormat($source->expense_without_advance($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id,$expenseHead->id,$expenseHead->expense_head_by_id->expense_head_code))</td>
                                    <td class="remain-budget kalimati" align="right">@moneyFormat($source->remain_budget($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id,$expenseHead->id,$expenseHead->expense_head_by_id->expense_head_code))</td>
                                    <?php
                                    $i = 0;
                                    ?>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="4" align="right"><b>{{$source->name}} जम्मा</b></td>
                                <td class="kalimati" align="right">{{number_format($source->getTotalSourecBudget($requestData['fiscal_year'],$requestData['budget_sub_head'],$source->id),2)}}</td>
                                <td class="kalimati" align="right">{{number_format($source->getTotalExpenseUpToThisMonthSourceWise($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id),2)}}</td>
                                <td class="kalimati" align="right">{{number_format($source->getTotalExpenseUpToPrevMonthSourceWise($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id),2)}}</td>
                                <td class="kalimati" align="right">{{number_format($source->getTotalExpenseThisMonthSourceWise($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id),2)}}</td>
                                <td class="kalimati" align="right">{{number_format($source->getTotalExpenseUpToThisMonthSourceWise($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id),2)}}</td>
                                <td class="kalimati" align="right">{{number_format($source->getTotalPeskiUpToThisMonthSourceWise($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id),2)}}</td>
                                <td class="kalimati" align="right">{{number_format($source->getTotalExpenseWithoutPeskiSourceWise($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id),2)}}</td>
                                <td class="kalimati" align="right">{{number_format($source->getTotalRemainBudgetSourceWise($requestData['fiscal_year'],$requestData['budget_sub_head'],$requestData['month'],$source->id),2)}}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    <tr class="second_last">
                        <td colspan="4" style="text-align: right">कुल जम्मा</td>
                        <td style="text-align: right"><span id="total_last_budget"></span></td>
                        <td style="text-align: right"><span id="this_month_nikasa"></span></td>
                        <td style="text-align: right"><span id="up_to_prev_month_exp"></span></td>
                        <td style="text-align: right"><span id="this_month_exp"></span></td>
                        <td style="text-align: right"><span id="up_to_this_month_exp"></span></td>
                        <td style="text-align: right"><span id="advance"></span></td>
                        <td style="text-align: right"><span id="exp_with_out_advance"></span></td>
                        <td style="text-align: right"><span id="remain_budget"></span></td>

                    </tr>
                    <tr class="last">
                        <td colspan="4" style="text-align: right">अन्तिम बजेटको तुलना (%प्रतिशतमा)</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: center"><span class="e-n-t-n-n" id="total_expense_percentage"></span>%
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <table width="99%" style="font-size: 13px">
                    <tr>
                        <td style="padding-left: 52px;">तयार गर्ने :</td>
                        <td>पेश गर्ने :</td>
                        <td>सदर गर्ने :</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">पद :</td>
                        <td>पद :</td>
                        <td>पद :</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">मिति :</td>
                        <td>मिति :</td>
                        <td>मिति :</td>
                    </tr>

                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script>
    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '/')

                    nepaliNo += "/";
            }
        });
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });


</script>

<script>
    $(document).ready(function () {
        let trs = $('#fatwari_table tbody').find('tr:not(.second_last,.last)');
        let total_budget = 0;
        let total_this_month_expense = 0;
        let total_up_to_prev_month_expense = 0;
        let total_up_to_this_month_expense = 0;
        let total_advance = 0;
        let total_exp_with_out_advance = 0;
        let total_remain_budget = 0;
        let expense_percentage = 0;
        let source_total_budget = 0;
        $.each(trs, function () {

            let total_budget_text = $.trim($(this).find('td.total-budget').text());
            total_budget_text = total_budget_text.replace(',','').replace(',','').replace(',','').replace(',','')

            let total_this_month_expense_text = $.trim($(this).find('td.this-month-exp').text());
            total_this_month_expense_text = total_this_month_expense_text.replace(',','').replace(',','').replace(',','');

            let total_up_to_prev_month_expense_text = $.trim($(this).find('td.up-to-pre-month-exp').text());
            total_up_to_prev_month_expense_text = total_up_to_prev_month_expense_text.replace(',','').replace(',','').replace(',','')

            let total_up_to_this_month_expense_text = $.trim($(this).find('td.up-to-this-month-exp').text());
            total_up_to_this_month_expense_text = total_up_to_this_month_expense_text.replace(',','').replace(',','').replace(',','')

            let advance_text = $.trim($(this).find('td.advance').text());
            advance_text = advance_text.replace(',','').replace(',','')

            let exp_with_out_advance_text = $.trim($(this).find('td.exp-with-out-advance').text());
            exp_with_out_advance_text = exp_with_out_advance_text.replace(',','').replace(',','').replace(',','')

            let remain_budget_text = $.trim($(this).find('td.remain-budget').text());
            remain_budget_text = remain_budget_text.replace(',','').replace(',','').replace(',','')

            if(total_budget_text){
                total_budget = parseFloat(total_budget) + parseFloat(total_budget_text);
            }
            if(total_budget_text == ''){
               source_total_budget = total_budget
            }
            if(total_this_month_expense_text){
                total_this_month_expense = parseFloat(total_this_month_expense) + parseFloat(total_this_month_expense_text);
            }
            if(total_up_to_prev_month_expense_text){
                total_up_to_prev_month_expense = parseFloat(total_up_to_prev_month_expense) + parseFloat(total_up_to_prev_month_expense_text);
            }
            if(total_up_to_this_month_expense_text){
                total_up_to_this_month_expense = parseFloat(total_up_to_this_month_expense) + parseFloat(total_up_to_this_month_expense_text);
            }
            if(advance_text){
                total_advance = parseFloat(total_advance) + parseFloat(advance_text);
            }
            if(exp_with_out_advance_text){
                total_exp_with_out_advance = parseFloat(total_exp_with_out_advance) + parseFloat(exp_with_out_advance_text);
            }
            if(remain_budget_text){
                total_remain_budget = parseFloat(total_remain_budget) + parseFloat(remain_budget_text);
            }
            expense_percentage = (parseFloat(total_up_to_this_month_expense) * 100) / parseFloat(total_budget)
        });


        $('#total_last_budget').text((total_budget.toFixed(2))).addClass('kalimati');
        $('#this_month_nikasa').text(total_up_to_this_month_expense.toFixed(2)).addClass('kalimati');
        $('#this_month_exp').text(total_this_month_expense.toFixed(2)).addClass('kalimati');
        $('#up_to_prev_month_exp').text(total_up_to_prev_month_expense.toFixed(2)).addClass('kalimati');
        $('#up_to_this_month_exp').text(total_up_to_this_month_expense.toFixed(2)).addClass('kalimati');
        $('#advance').text(total_advance.toFixed(2)).addClass('kalimati');
        $('#exp_with_out_advance').text(total_exp_with_out_advance.toFixed(2)).addClass('kalimati');
        $('#remain_budget').text(total_remain_budget.toFixed(2)).addClass('kalimati');
        $('#total_expense_percentage').text(expense_percentage.toFixed(3)).addClass('kalimati');
        $('#sourceLastBudget').text(source_total_budget.toFixed(2)).addClass('kalimati');


    })
</script>

<script>
    function convertNepaliToEnglish(input) {
        var charArray = input.split('');
        var engDate = '';
        $.each(charArray, function (key, value) {

            switch (value) {
                case '१':
                    engDate += '1';
                    break;
                case '२':
                    engDate += '2';
                    break;
                case '३':
                    engDate += '3';
                    break;
                case '४':
                    engDate += '4';
                    break;
                case '५':
                    engDate += '5';
                    break;
                case '६':
                    engDate += '6';
                    break;
                case '०':
                    engDate += '0';
                    break;
                case '७':
                    engDate += '7';
                    break;
                case '८':
                    engDate += '8';
                    break;
                case '९':
                    engDate += '9';
                    break;

                case '-':
                    engDate += '-';
                    break;
                case '.':
                    engDate += '.';
                    break
            }
        });

        return engDate

    }
</script>