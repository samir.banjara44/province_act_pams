<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Medium extends Model
{
    protected $table = 'mediums';
    protected $guarded = [];

    public function mofs(){
        return $this->hasMany('App\Models\Mof');
    }

    public function ministries(){
        return $this->hasMany('App\Models\Ministries');
    }

    public function departments(){
        return $this->hasMany('App\Models\Department');
    }

    public function offices(){
        return $this->hasMany('App\Models\Office');
    }

    public function users(){
        return $this->hasMany('App\Users');
    }
}
