<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Float_;

class VoucherDetail extends Model
{
    protected $table = 'voucher_details';
    protected $guarded = [];

    public function voucher(){
        return $this->belongsTo('App\Models\Voucher','journel_id','id');
    }

    public function expense_head(){

        return $this->belongsTo('App\Models\ExpenseHead','expense_head_id','id');
    }

    public function ledgerType(){

        return $this->belongsTo('App\Models\LedgerType');
    }

//    changed to BudgetDetails
    public function mainActivity(){

        return $this->belongsTo('App\Models\BudgetDetails','main_activity_id','id');
    }
    public function budget(){

        return $this->belongsTo('App\Models\Budget','main_activity_id','id');
    }

    public function advancePayment(){

        return $this->belongsTo('App\Models\AdvanceAndPayment','peski_paune_id','id');
    }

    public function party(){

        return $this->belongsTo('App\Models\PartyTypes','party_type','id');
    }


    public function budgetExpenseHead(){

        return $this->hasMany('App\Models\Budget','expense_head_id','id');
    }

    public function getAntimBudget(){

        $budgets = $this->budgetExpenseHead;
        if ($budgets){
            return $budgets->sum('total_budget');
        }
    }

    public function getTotalPreviousMonthExpense($month_id){

       return $expense = VoucherDetail::where('expense_head_id',$this->expense_head_id)
            ->where('fiscal_year',$this->fiscal_year)
            ->where('office_id',$this->office_id)
           ->where('month','<',$month_id)
            ->sum('dr_amount');
    }

    public function getTotalThisMonthExpense($month_id){
        return $expense = VoucherDetail::where('expense_head_id',$this->expense_head_id)
            ->where('fiscal_year',$this->fiscal_year)
            ->where('office_id',$this->office_id)
            ->where('month',$month_id)
            ->sum('dr_amount');
    }

    public function getTotalUptoThisMonthExpense($month_id){
        return $expense = VoucherDetail::where('expense_head_id',$this->expense_head_id)
            ->where('fiscal_year',$this->fiscal_year)
            ->where('office_id',$this->office_id)
            ->where('month','<=',$month_id)
            ->where('dr_or_cr','1')
            ->whereIn('ledger_type_id',[1,4])
            ->sum('dr_amount');
    }

    public function getTotalPeski($month_id){
        return $expense = VoucherDetail::where('expense_head_id',$this->expense_head_id)
            ->where('fiscal_year',$this->fiscal_year)
            ->where('office_id',$this->office_id)
            ->where('month',$month_id)
            ->where('dr_or_cr','1')
            ->where('ledger_type_id','4')
            ->sum('dr_amount');
    }

    public function expense_woth_out_peski($month_id){

       $up_to_this_month_Exp =  $this->getTotalUptoThisMonthExpense($month_id);
       $upto_this_month_peski =  $this->getTotalPeski($month_id);
      return $total_exp_with_out_exp = ((float)$up_to_this_month_Exp) - ((float)$upto_this_month_peski);

    }

    public function remain_budget($month_id){

        $last_budget = $this->getAntimBudget();
        $up_to_this_month_exp = $this->getTotalUptoThisMonthExpense($month_id);
       return $remain_budget = (float)$last_budget - (float)$up_to_this_month_exp;

    }



//    Expense wise report office,budget_sub_head and expense_head wise

    public function getLastBudgetOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        return $last_budget = DB::table('budget')
            ->where('office_id', $office_id)
            ->where('budget_sub_head', $budget_sub_head_id)
            ->where('expense_head_id', $expense_head_id)
            ->sum('total_budget');


    }

    public function getTotalExpenseOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpToYesterdayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id){
        $ldate = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');

    }

    public function getTotalExpenseTodayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id){

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpToThisDayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id){

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalAdvanceOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id){

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.ledger_type_id',4)
            ->sum('dr_amount');
    }

    public function expenseWithOutAdvanceOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {


        $totalExpense = $this->getTotalExpenseUpToThisDayOfOfficeByBudgetSubHead($office_id,$budget_sub_head_id, $expense_head_id);
        $totalAdvance = $this->getTotalAdvanceOfOfficeByBudgetSubHead($office_id,$budget_sub_head_id, $expense_head_id);
        return $totalExpenseWithOutAdvance = (float)$totalExpense - (float)$totalAdvance;
    }

    public function getRemainBudgetOfOfficeByBudgetSubHead($office_id,$budget_sub_head_id,$expense_head_id)
    {

        $totalBudget = $this->getLastBudgetOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id);
        $totalExpense = $this->getTotalExpenseUpToThisDayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id);
        return $remainBudget = (float)$totalBudget - (float)$totalExpense;
    }


    public function isPeski(){
        $peski = VoucherDetail::where('dr_or_cr',1)->where('ledger_type_id',4)->where('peski_paune_id',71)->sum('dr_amount');
        $clear = VoucherDetail::where('dr_or_cr',2)->where('ledger_type_id',9)->where('peski_paune_id',71)->sum('dr_amount');

    }

}
