<style>
    .sub-menu-selected {
        box-shadow: 0px 1px 2px #d26a6a;
        background: #ffdd66 !important;
        /*color: #fff!important;*/
    }

    .sub-menu-selected a {
        color: #000 !important;
    }
</style>

<div class="head-container">
    {{--    Header Start --}}
    <div class="top-nav">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <a href="{{ url('/') }}"><img src="{{ asset('img/nepal-govt-logo.png') }}" height="25px"
                                width="25px" alt=""></a>
                        प्रदेश लेखा व्यवस्थापन प्रणाली, <b>{{ Auth::user()->office->name }}
                            , {{ Auth::user()->province->name }}
                        </b>
                    </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="menu-nav">
        <ul class="nav nav-tabs">
            @if (
                !auth()->user()->hasRole('ministry_super_vision') and
                    auth()->user()->hasRole('admin') or
                    !auth()->user()->hasRole('ministry_super_vision') and
                        auth()->user()->hasRole('ministry'))
                <li class=""><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                <li class="@if (in_array(Route::current()->getName(), [
                        'program',
                        'bank',
                        'advance',
                        'karmachari',
                        'karmachari.create',
                        'program.edit',
                        'bank.edit',
                        'AdvanceAndPayment.create',
                        'AdvanceAndPayment.edit',
                        'karmachari.edit',
                        'program.create',
                        'voucher.signature',
                    ])) active @endif" id="kharid_home"><a data-toggle="tab"
                        class="active" href="#set_up"> <i class="fas fa-cogs"></i>
                        सेटअप</a></li>
                <li class="@if (in_array(Route::current()->getName(), [
                        'budget.create',
                        'budget.create.continue',
                        'budget.edit',
                        'akhtiyari.param',
                        'edit.akhtiyari',
                    ])) active @endif">
                    <a data-toggle="tab" href="#budget"> <i class="fas fa-money-bill-alt"></i> बजेट</a>
                </li>
                <li class="@if (in_array(Route::current()->getName(), [
                        'voucher',
                        'bhuktani',
                        'bhuktani.index',
                        'voucher_accept_create',
                        'bhuktani.create',
                        'voucher.list.edit',
                        'get_voucher_details',
                    ])) active @endif">
                    <a data-toggle="tab" href="#expense"> <i class="fas fa-wallet"></i> लेखाङ्कन</a>
                </li>
                <li class="@if (in_array(Route::current()->getName(), [
                        'retention_bank',
                        'retention_bank_create',
                        'retention_bank_edit',
                        'retention.depositor',
                        'retention.depositor.create',
                        'retention.deposotor.edit',
                        'retention',
                        'retention.record.edit',
                        'retention.bank.guarantee',
                        'retention.bank.guarantee.create',
                        'retention.bank.guarantee.edit',
                        'retention.voucher.index',
                        'retention.voucher',
                    ])) active @endif">
                    @if (Auth::user()->office->id == 1)
                        <a data-toggle="tab" href="#deposit"> <i class="fas fa-coins"></i>
                            धरौटी</a>
                    @endif
                </li>

            @endif
            {{--            Darta Chalani --}}
            @if (Auth::user()->office->id == 4 or Auth::user()->office->id == 47)
                <li class="@if (in_array(Route::current()->getName(), [''])) active @endif" id=""><a data-toggle="tab"
                        class="active" href="#darta_chalani"> <i class="fas fa-cogs"></i>
                        दर्ता चलानी</a></li>
            @endif
            @if (auth()->user()->hasRole('admin') or
                    auth()->user()->hasRole('report') or
                    auth()->user()->hasRole('ministry'))
                <li class="@if (in_array(Route::current()->getName(), [
                        'report.voucher',
                        'report.activity',
                        'report.bhuktani.adesh.param',
                        'report.malepa.five.param',
                        'report.malepa.thirteen.param',
                        'report.malepa.seventeen.param',
                        'report.malepa.twentyTwo.param',
                        'report.malepa.fourteen.param',
                        'report.goswara.dharauti.khata.param',
                        'report.byaktigat.dharauti.khata.param',
                        'report.dharauti.cash.book.param',
                        'report.retentio.bhuktani.adesh.param',
                    ])) active @endif">
                    <a data-toggle="tab" href="#report"> <i class="fas fa-scroll"></i> प्रतिवेदन </a>
                </li>
            @endif
            @if (auth()->user()->hasRole('ministry_super_vision') or
                    auth()->user()->hasRole('ministry'))
                <li class="@if (in_array(Route::current()->getName(), [
                        'ministry.office.list',
                        'ministry.activity.list.parameter',
                        'ministry.office.Used.List.Param',
                    ])) active @endif">
                    <a data-toggle="tab" href="#superVision"><i class="fas fa-vote-yea"></i> अनुगमन</a>
                </li>
            @endif

            <li class="pull-right dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img
                        src="{{ asset('img/user_white.png') }}" height="25px" width="25px"
                        style="background-color: white" alt="">
                    {{ Auth::user()->name }} <span class="caret"></span></a>
                <ul class="dropdown-menu setting-dropdown" style="right: 3px;min-width: 174px;text-align: right;">
                    @if (Auth::user()->roles->first()->name == 'superadmin')
                        <li><a href="{{ route('admin') }}" target="_blank">भित्र</a></li>
                    @endif
                    @if (Auth::user()->roles->first()->name == 'ministry')
                        <li class=""><a href=""><i class="fa fa-dashboard"></i> See All Report </a></li>
                    @endif
                    <li>
                        <a href="{{ route('logout') }}">
                            <i class="fas fa-sign-out-alt"></i>बाहिर जानुहोस
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <div class="tab-content">

            {{-- सेट अप --}}
            <div id="set_up" class="tab-pane fade @if (in_array(Route::current()->getName(), [
                    'program',
                    'bank',
                    'bank.create',
                    'advance',
                    'karmachari',
                    'karmachari.create',
                    'program.create',
                    'program.edit',
                    'bank.edit',
                    'AdvanceAndPayment.create',
                    'AdvanceAndPayment.edit',
                    'karmachari.edit',
                    'voucher.signature',
                    'user.index',
                    'front.user.index',
                    'front.user.create',
                    'user.edit',
                    'branch',
                    'branch.edit',
                    'cheque',
                ])) ) active in @endif">
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['program'])) sub-menu-selected @endif">
                    <a href="{{ route('program') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">बजेट उपशिर्षक</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['bank'])) sub-menu-selected @endif">
                    <a href="{{ route('bank') }}" class="kharidAdeshSearch">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">बैकं</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['advance'])) sub-menu-selected @endif">
                    <a href="{{ route('advance') }}" class="kharidAdeshSearch">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">पेश्की लेजर</span>
                    </a>
                </div>

                <div class="sub-menu @if (in_array(Route::current()->getName(), ['karmachari'])) sub-menu-selected @endif">
                    <a href="{{ route('karmachari') }}" class="kharidAdeshSearch">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">कर्मचारी</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['branch', 'branch.edit'])) sub-menu-selected @endif">
                    <a href="{{ route('branch') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">शाखा</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['voucher.signature'])) sub-menu-selected @endif">
                    <a href="{{ route('voucher.signature') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">भौचर हस्ताक्षर कर्ता</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['cheque'])) sub-menu-selected @endif">
                    <a href="{{ route('cheque') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">चेक प्रविस्टी</span>
                    </a>
                </div>
                <!-- <div class="sub-menu @if (in_array(Route::current()->getName(), ['user.index', 'front.user.index', 'user.edit'])) sub-menu-selected @endif">
                    <a href="{{ route('front.user.index', Auth::user()->office->id) }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">प्रयोग कर्ता बनाउने</span>
                    </a>
                </div> -->
            </div>


            {{--  बजेट  --}}
            <div id="budget" class="tab-pane  fade @if (in_array(Route::current()->getName(), [
                    'budget.create',
                    'budget.create.continue',
                    'budget.edit',
                    'akhtiyari.param',
                    'edit.akhtiyari',
                    'budget.rakamantar',
                ])) active in @endif">
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['akhtiyari.param'])) sub-menu-selected @endif">
                    <a href="{{ route('akhtiyari.param') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">अख्तियारी प्रविस्टी</span>
                    </a>

                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['budget.create'])) sub-menu-selected @endif">
                    <a href="{{ route('budget.create') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">विस्तृत बजेट प्रविस्टी</span>
                    </a>

                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['budget.edit'])) sub-menu-selected @endif">
                    <a href="{{ route('budget.edit') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">बजेट संसोधन</span>
                    </a>

                </div>
                <!-- <div class="sub-menu @if (in_array(Route::current()->getName(), ['budget.rakamantar'])) sub-menu-selected @endif">
                    <a href="{{ route('budget.rakamantar') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">रकमान्तर</span>
                    </a>

                </div> -->
            </div>

            {{--  खर्च भौचरर    --}}
            <div id="expense" class="tab-pane fade @if (in_array(Route::current()->getName(), [
                    'voucher',
                    'bhuktani',
                    'voucher.accept_create',
                    'bhuktani.index',
                    'bhuktani.create',
                    'voucher.list.edit',
                    'get_voucher_details',
                    'voucher.unapprove.parameter',
                    'voucher.unapprove',
                    'grant.voucher.parameter',
                    'grant.voucher',
                    'summary.voucher',
                    'voucher.cheque.print',
                ])) active in @endif">
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['voucher'])) sub-menu-selected @endif">
                    <a href="{{ route('voucher') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">भौचर</span>
                    </a>
                </div>

                <div class="sub-menu @if (in_array(Route::current()->getName(), ['voucher.accept_create'])) sub-menu-selected @endif">
                    <a href="{{ route('voucher.accept_create') }}" class="magFaramSearch">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span class="">भौचर स्विकृत</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['voucher.cheque.print'])) sub-menu-selected @endif">
                    <a href="{{ route('voucher.cheque.print') }}" class="magFaramSearch">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span class="">चेक प्रिन्ट</span>
                    </a>
                </div>
                <!-- <div class="sub-menu @if (in_array(Route::current()->getName(), ['bhuktani', 'bhuktani.index', 'bhuktani.create'])) sub-menu-selected @endif">
                    <a href="{{ route('bhuktani') }}" class="magFaramSearch">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span class="">भुक्तानि आदेश</span>
                    </a>
                </div> -->
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['voucher.list.edit', 'get_voucher_details'])) sub-menu-selected @endif">
                    <a href="{{ route('voucher.list.edit') }}" class="magFaramSearch">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span class="">भौचर संसोधन</span>
                    </a>
                </div>

                <div class="sub-menu @if (in_array(Route::current()->getName(), ['voucher.unapprove.parameter'])) sub-menu-selected @endif">
                    <a href="{{ route('voucher.unapprove.parameter') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">भौचर अस्विकृत</span>
                    </a>
                </div>

                <!-- <div class="sub-menu @if (in_array(Route::current()->getName(), ['grant.voucher.parameter', 'grant.voucher'])) sub-menu-selected @endif">
                    <a href="{{ route('grant.voucher.parameter') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">निकासा भौचर</span>
                    </a>
                </div> -->
                {{--                @if (Auth::user()->office->ministry) --}}
                {{--                    @if (Auth::user()->office->ministry->id == 4) --}}
                {{--                        <div class="sub-menu @if (in_array(Route::current()->getName(), ['summary.voucher'])) sub-menu-selected @endif"> --}}
                {{--                            <a href="{{route('summary.voucher')}}" class="add"> --}}
                {{--                                <i class="fa fa-play-circle" aria-hidden="true"></i> --}}
                {{--                                <span class="">एकमुष्ट भौचर</span> --}}
                {{--                            </a> --}}
                {{--                        </div> --}}
                {{--                    @endif --}}
                {{--                @endif --}}
            </div>


            {{--            धरौटि --}}
            <div id="deposit" class="tab-pane fade @if (in_array(Route::current()->getName(), [
                    'retention_bank',
                    'retention_bank_create',
                    'retention_bank_edit',
                    'retention.depositor',
                    'retention.depositor.create',
                    'retention.deposotor.edit',
                    'retention.record',
                    'retention.record.create',
                    'retention.record.edit',
                    'retention.bank.guarantee',
                    'retention.bank.guarantee.create',
                    'retention.bank.guarantee.edit',
                    'retention.voucher.parameter',
                    'retention.voucher.index',
                    'retention.voucher.details',
                    'retention.voucher.edit',
                    'retention.voucher.approve.param',
                    'retention.bhuktani',
                ])) active in @endif">

                <div class="sub-menu @if (in_array(Route::current()->getName(), ['retention_bank', 'retention_bank_edit'])) sub-menu-selected @endif">
                    <a href="{{ route('retention_bank') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">बैक्ङ खाता</span>
                    </a>
                </div>

                <div class="sub-menu @if (in_array(Route::current()->getName(), [
                        'retention.depositor',
                        'retention.depositor.create',
                        'retention.deposotor.edit',
                    ])) sub-menu-selected @endif">
                    <a href="{{ route('retention.depositor') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">धरौटि जम्मा गर्ने</span>
                    </a>

                </div>


                <div class="sub-menu @if (in_array(Route::current()->getName(), ['retention.record', 'retention.record.create', 'retention.record.edit'])) sub-menu-selected @endif">
                    <a href="{{ route('retention.record') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">धरौटी अभिलेख</span>
                    </a>

                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), [
                        'retention.bank.guarantee',
                        'retention.bank.guarantee.create',
                        'retention.bank.guarantee.edit',
                    ])) sub-menu-selected @endif">
                    <a href="{{ route('retention.bank.guarantee') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">बैक्ङ ग्यारेन्टी</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), [
                        'retention.voucher.index',
                        'retention.voucher.details',
                        'retention.voucher.edit',
                    ])) sub-menu-selected @endif">
                    <a href="{{ route('retention.voucher.index') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">धरौटी भौचर </span>
                    </a>

                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['retention.voucher.approve.param'])) sub-menu-selected @endif">
                    <a href="{{ route('retention.voucher.approve.param') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">धरौटी भौचर स्विकृत</span>
                    </a>

                </div>

                <div class="sub-menu @if (in_array(Route::current()->getName(), ['retention.bhuktani'])) sub-menu-selected @endif">
                    <a href="{{ route('retention.bhuktani') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">धरौटी भुक्तानी आदेश</span>
                    </a>

                </div>
            </div>

            {{-- Darta Chalani --}}
            <div id="darta_chalani" class="tab-pane fade @if (in_array(Route::current()->getName(), ['darta', 'chalani', 'darta.edit', 'chalani.edit'])) ) active in @endif">
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['darta', 'darta.edit'])) sub-menu-selected @endif">
                    <a href="{{ route('darta') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">दर्ता</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['chalani', 'chalani.edit'])) sub-menu-selected @endif">
                    <a href="{{ route('chalani') }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">चलानी</span>
                    </a>
                </div>
            </div>
            {{--  report   --}}
            <div id="report"
                class="tab-pane fade
                @if (in_array(Route::current()->getName(), [
                        'report.voucher',
                        'report.activity',
                        'report.malepa.five.param',
                        'report.malepa.thirteen.param',
                        'report.malepa.seventeen.param',
                        'report.malepa.twentyTwo.param',
                        'report.bhuktani.adesh.param',
                        'report.malepa.five.param',
                        'report.malepa.thirteen.param',
                        'report.malepa.seventeen.param',
                        'report.malepa.twentyTwo.param',
                        'report.malepa.fourteen.param',
                        'report.goswara.dharauti.khata.param',
                        'report.byaktigat.dharauti.khata.param',
                        'report.dharauti.cash.book.param',
                        'report.retention.bhuktani.adesh.param',
                        'report.budget.sheet.param',
                        'report.activity.summary.param',
                        'report.activity.detail.param',
                    ])) active in @endif">
                @if (auth()->user()->hasRole('admin') or
                        auth()->user()->hasRole('ministry'))
                    <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.voucher'])) sub-menu-selected @endif">
                        <a href="{{ route('report.voucher') }}" class="add">
                            <span class="" title="गोश्वारा भौचर"> <i class="fas fa-receipt"></i> म ले प फा न.
                                २०३</span>
                        </a>
                    </div>
                    <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.bhuktani.adesh.param'])) sub-menu-selected @endif">
                        <a href="{{ route('report.bhuktani.adesh.param') }}" class="add">
                            <span class="" title="भुक्तानी आदेश"> <i class="fas fa-receipt"></i> म ले प फा न.
                                २०४</span>
                        </a>
                    </div>

                    <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.malepa.twentyTwo.param'])) sub-menu-selected @endif">
                        <a href="{{ route('report.malepa.twentyTwo.param') }}" class="magFaramSearch">
                            <span class="" title="खाता"> म ले प फा न. २०७</span>
                        </a>
                    </div>
                    <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.budget.sheet.param'])) sub-menu-selected @endif">
                        <a href="{{ route('report.budget.sheet.param') }}" class="add">
                            <span class="" title="बजेट खाता"> <i class="fas fa-receipt"></i> म ले प फा न.
                                २०८</span>
                        </a>
                    </div>

                    <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.malepa.five.param'])) sub-menu-selected @endif">
                        <a href="{{ route('report.malepa.five.param') }}" class="add">
                            <span class="" title="बैङ्क नगदी किताब"> <i class="fas fa-receipt"></i> म ले प फा
                                न. २०९</span>
                        </a>

                    </div>
                    <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.malepa.thirteen.param'])) sub-menu-selected @endif">
                        <a href="{{ route('report.malepa.thirteen.param') }}" class="magFaramSearch">
                            <span class="" title="फांटवारी"> <i class="fas fa-receipt"></i> म ले प फा न.
                                २१०</span>
                        </a>
                    </div>
                    <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.malepa.fourteen.param'])) sub-menu-selected @endif">
                        <a href="{{ route('report.malepa.fourteen.param') }}" class="magFaramSearch">
                            <span class="" title="पेश्की मास्केबारी"> <i class="fas fa-receipt"></i> म ले प फा
                                न. २११ </span>
                        </a>
                    </div>
                    <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.malepa.seventeen.param'])) sub-menu-selected @endif">
                        <a href="{{ route('report.malepa.seventeen.param') }}" class="add">
                            <span class="" title="आर्थिक विवरण"> <i class="fas fa-receipt"></i>म ले प फा न.
                                २१३</span>
                        </a>

                    </div>
                @endif
                @if (Auth::user()->office->id == 1)
                    <div class="sub-menu">
                        <a href="{{ route('report.byaktigat.dharauti.khata.param') }}" class="magFaramSearch">
                            <span class=""> <i class="fas fa-receipt"></i> म ले प फा न. ६०१</span>
                        </a>
                    </div>
                    <div class="sub-menu">
                        <a href="{{ route('report.goswara.dharauti.khata.param') }}" class="magFaramSearch">
                            <span class=""> <i class="fas fa-receipt"></i> म ले प फा न. ६०२</span>
                        </a>
                    </div>
                    <div class="sub-menu">
                        <a href="{{ route('report.dharauti.cash.book.param') }}" class="magFaramSearch">
                            <span class=""> <i class="fas fa-receipt"></i> म ले प फा न. ६०३</span>
                        </a>
                    </div>
                @endif
                @if (Auth::user()->office->id == 1)
                    <div class="sub-menu">
                        <a href="{{ route('report.retention.bhuktani.adesh.param') }}" class="add">
                            <span class=""> <i class="fas fa-receipt"></i> धरौटी भुक्तानी आदेश</span>
                        </a>
                    </div>
                @endif
                {{--                <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.activity.summary.param'])) sub-menu-selected @endif"> --}}
                {{--                    <a href="{{route('report.activity.summary.param')}}" class="magFaramSearch"> --}}
                {{--                        <span class=""> <i class="fas fa-receipt"></i> कार्यक्रम समरि</span> --}}
                {{--                    </a> --}}
                {{--                </div> --}}
                @if (auth()->user()->hasRole('admin') or
                        auth()->user()->hasRole('report') or
                        auth()->user()->hasRole('ministry'))
                    <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.activity'])) sub-menu-selected @endif">
                        <a href="{{ route('report.activity') }}" class="add">
                            <span class="" title="चौमासिक सहित"> <i class="fas fa-receipt"></i>
                                कार्यक्रम</span>
                        </a>

                    </div>
                    <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.activity.detail.param'])) sub-menu-selected @endif">
                        <a href="{{ route('report.activity.detail.param') }}" class="add">
                            <span class="" title="कन्टेन्जेन्सी सहित"> <i class="fas fa-receipt"></i> कार्यक्रम
                                डिटेल</span>
                        </a>
                    </div>
                @endif
            </div>


            {{-- अनुगमन     --}}
            <div id="superVision" class="tab-pane fade @if (in_array(Route::current()->getName(), [
                    'ministry.office.list',
                    'ministry.activity.list.parameter',
                    'ministry.fatwari.parameter',
                    'report.on.source.ledger.parameter',
                    'report.expense.head.wise.expense.parameter',
                    'report.source.wise.annual.report.parameter',
                    'ministry.office.Used.List.Param',
                ])) active in @endif">

                <div class="sub-menu @if (in_array(Route::current()->getName(), ['ministry.office.list'])) sub-menu-selected @endif">
                    <a href="{{ route('ministry.office.list', Auth::user()->ministry_id) }}" class="add">
                        <span class="">कार्यालय</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['ministry.office.Used.List.Param'])) sub-menu-selected @endif">
                    <a href="{{ route('ministry.office.Used.List.Param', Auth::user()->ministry_id) }}"
                        class="add">
                        <span class="">Used Users</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['ministry.activity.list.parameter'])) sub-menu-selected @endif">
                    <a href="{{ route('ministry.activity.list.parameter', Auth::user()->ministry_id) }}"
                        class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">कार्यक्रम अनुसार</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['ministry.fatwari.parameter'])) sub-menu-selected @endif">
                    <a href="{{ route('ministry.fatwari.parameter', Auth::user()->ministry_id) }}" class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">फांटवारी</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.on.source.ledger.parameter'])) sub-menu-selected @endif">
                    <a href="{{ route('report.on.source.ledger.parameter', Auth::user()->ministry_id) }}"
                        class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">स्रोत र खाता अनुसार</span>
                    </a>
                </div>
                {{--                <div class="sub-menu"> --}}
                {{--                    <a href="{{route('report.expense.head.wise.annual.expense.parameter',Auth::user()->ministry_id)}}" class="add"> --}}
                {{--                        <i class="fa fa-play-circle" aria-hidden="true"></i> --}}
                {{--                        <span class="">खर्च शीर्षकगत बजेट र खर्चको वित्तीय बिवरण</span> --}}
                {{--                    </a> --}}
                {{--                </div> --}}
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.expense.head.wise.expense.parameter'])) sub-menu-selected @endif">
                    <a href="{{ route('report.expense.head.wise.expense.parameter', Auth::user()->ministry_id) }}"
                        class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">खर्च शिर्षक अनुसार</span>
                    </a>
                </div>
                <div class="sub-menu @if (in_array(Route::current()->getName(), ['report.source.wise.annual.report.parameter'])) sub-menu-selected @endif">
                    <a href="{{ route('report.source.wise.annual.report.parameter', Auth::user()->ministry_id) }}"
                        class="add">
                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                        <span class="">दातृ निकायगत बजेट र खर्चको आर्थिक बिवरण</span>
                    </a>
                </div>

            </div>

        </div>
    </div>


    <div class="menu-option-container">

    </div>
</div>

<div id="errorMessage" class="error" style="position: absolute;right: 40%;min-width: 300px;padding: 3px;">
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @elseif(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif
</div>
