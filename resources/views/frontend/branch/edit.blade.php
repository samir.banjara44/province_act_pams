@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  {{-- <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> <i class="fas fa-edit"></i> Edit Branch </h1>
      
      <a href="{{route('branch')}}" class="btn btn-sm btn-primary btn-show-form"><i class="fa fa-arrow-left"></i> लिष्टमा जाने</a>
    </section> --}}

    <!-- FORM section start -->
  <section class="formSection">
    <div class="row justify-content-md-center">
      <div class="col-md-8 form-bg col-md-offset-2">
        <div class="form-title">Branch Edit</div>
        <form action="{{route('branch.update', $branch->id)}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Name in English</label>
            <input type="text" class="form-control" id="name_eng" value="{{$branch->name_eng}}" placeholder="Name in English" name="name_eng" required>
              </div>
              <div class="form-group">
                <label for="name">Name in Nepali</label>
                <input type="text" class="form-control kalimati" id="name_nep" value="{{$branch->name_nep}}" placeholder="Name in Nepali" name="name_nep" required>
              </div>
            
            <center>
              <button type="submit" class="btn btn-success btn-show-form"><i class="fas fa-check-square"></i> सम्पादन गर्ने</button>
            </center>
          </form>
      </div>
    </div>
  </section>
  <!-- FORM section ends -->

  </div>

@endsection