<?php

namespace App\Repositories;

use App\Models\AdvanceAndPayment;
use http\Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class AdvancePaymentRepositoryEloquent implements AdvancePaymentRepository
{
    public function create($attributes)
    {

        $office_id = Auth::user()->office_id;
        $advance_payment = new AdvanceAndPayment();
        $advance_payment->party_type = $attributes['party_type'];
        $advance_payment->payee_code = $attributes['payee_code'];
        $advance_payment->name_nep = $attributes['name_nep'];
        $advance_payment->name_eng = $attributes['name_eng'];
        $advance_payment->citizen_number = $attributes['citizen_number'];
        $advance_payment->vat_pan_number = $attributes['vat_number'];
        $advance_payment->vat_office = $attributes['vat_office'];
        $advance_payment->bank = $attributes['bank'];
        $advance_payment->bank_address = $attributes['bank_address'];
        $advance_payment->party_khata_number = $attributes['khata_number'];
        $advance_payment->phone_number = $attributes['mobile_number'];
        $advance_payment->address = $attributes['is_advance'];
        $advance_payment->is_peski = 1;
        $advance_payment->is_dharauti = 1;
        $advance_payment->is_bhuktani = 1;
        $advance_payment->status = 1;
        $advance_payment->office_id = Auth::user()->office->id;
        $advance_payment->save();
        return $advance_payment;
    }

    public function createKarmachariParty($attributes)
    {

        $office_id = Auth::user()->office_id;
        $advance_payment = new AdvanceAndPayment();
        $advance_payment->payee_code = $attributes['payee_code'];
        $advance_payment->karmachari_id = $attributes['karmachari_id'];
        $advance_payment->party_type = $attributes['party_type'];
        $advance_payment->name_nep = $attributes['name_nep'];
        $advance_payment->name_eng = $attributes['name_eng'];
        $advance_payment->citizen_number = $attributes['citizen_number'];
        $advance_payment->phone_number = $attributes['mobile_number'];
        $advance_payment->bank = $attributes['bank'];
        $advance_payment->party_khata_number = $attributes['party_khata_number'];
        $advance_payment->bank_address = $attributes['bank_address'];
        $advance_payment->is_peski = 1;
        $advance_payment->is_dharauti = 1;
        $advance_payment->is_bhuktani = 1;
        $advance_payment->status = 1;
        $advance_payment->office_id = Auth::user()->office->id;
        $advance_payment->save();
        return $advance_payment;
    }

    public function get_by_office_and_party_type($office_id, $party_type)
    {
        return $parties = AdvanceAndPayment::where('office_id', $office_id)
            ->where('party_type', $party_type)
            ->get();
    }

    public function getPartyForRetention($officeId,$partyType){

        return $parties = AdvanceAndPayment::where('office_id', $officeId)
            ->where('party_type', $partyType)
            ->where('status', '=',1)
                       ->whereHas('retentionVoucherDetails')
                       ->with('retentionVoucherDetails')
            ->get();
    }



    public function get_by_id($id)
    {

        return AdvanceAndPayment::findorfail($id);
    }

    public function update($attributes, $id)
    {
        $advance_payment = $this->get_by_id($id);
        $advance_payment->payee_code = $attributes['payee_code'];
        $advance_payment->party_type = $attributes['party_type'];
        $advance_payment->name_nep = $attributes['name_nep'];
        $advance_payment->name_eng = $attributes['name_eng'];
        $advance_payment->citizen_number = $attributes['citizen_number'];
        $advance_payment->vat_pan_number = $attributes['vat_number'];
        $advance_payment->bank = $attributes['bank'];
        $advance_payment->bank_address = $attributes['bank_address'];
        $advance_payment->vat_office = $attributes['vat_office'];
        $advance_payment->party_khata_number = $attributes['khata_number'];
        $advance_payment->phone_number = $attributes['mobile_number'];
        $advance_payment->address = $attributes['is_advance'];
        $advance_payment->is_dharauti = $attributes['is_dharauti'];
        $advance_payment->is_bhuktani = $attributes['is_bhuktani'];
        $advance_payment->save();
        return $advance_payment;
    }

    public function UpdateKarmachariParty($attributed, $id)
    {

        $advanceKarmachari = AdvanceAndPayment::where('karmachari_id', $id)->get();
        $partyId = ($advanceKarmachari[0]->id);
        $advance_payment = AdvanceAndPayment::findorfail($partyId);

        $advance_payment['name_nep'] =   $attributed['name_nepali'];
        $advance_payment['name_eng'] =   $attributed['name_english'];
        $advance_payment['phone_number'] =   $attributed['mobile_number'];
        $advance_payment['vat_pan_number'] =   $attributed['vat_pan'];
        $advance_payment['payee_code'] =   $attributed['payee_code'];
        $advance_payment->bank = $attributed['bank'];
        $advance_payment->party_khata_number = $attributed['khata_number'];
        $advance_payment->bank_address = $attributed['bank_address'];
        return $advance_payment->save();
    }

    public function get_by_office_id($office_id)
    {
        return $advanceAndPaymentList = AdvanceAndPayment::where('office_id', $office_id)->where('status', 1)->with('get_party_type', 'preBhktani')->get();
    }
}
