<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Cheque;
use App\Models\ChequeDetails;
use App\Repositories\BankRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;
use App\helpers\BsHelper;
use App\helpers\FiscalYearHelper;
use App\Http\Exports\MyExport;
use App\Models\PreBhuktani;
// use Excel;
use Maatwebsite\Excel\Facades\Excel;

class ChequeController extends Controller
{
    private $fiscalYearHelper;
    private $bankRepo;

    public function __construct(BankRepository $bankRepo)
    {
        $this->middleware('auth');
        $this->bankRepo = $bankRepo;
        $this->fiscalYearHelper = new FiscalYearHelper();
    }

    public function index()
    {
        $office_id = Auth::user()->office_id;
        $cheques = Cheque::where('office_id', $office_id)->get();
        $banks = Bank::where('office_id', $office_id)->get();
        return view('frontend.cheque.index', compact('cheques', 'banks'));
    }

    public function create()
    {
        $office_id = Auth::user()->office_id;
        $banks = Bank::where('office_id', $office_id)->get();
        return view('frontend.bank.create', compact('banks'));
    }

    public function store(Request $request)
    {
        $from_last_three = substr($request->from, -4);
        $to_last_three = substr($request->to, -4);
        if (($to_last_three - $from_last_three) > 100) {
            return redirect()->back()->with('error', "Cheque number difference must be less than or equal to 100");
        }
        try {
            $current_date = date('Y-m-d');
            $date_array = explode('-', $current_date);
            $bsObj = new BsHelper();
            $nepali_date_arrya = $bsObj->eng_to_nep($date_array[0], $date_array[1], $date_array[2]);
            $today_nepali_date = $nepali_date_arrya['year'] . '-' . $nepali_date_arrya['month'] . '-' . $nepali_date_arrya['date'];

            $cheque = new Cheque();
            $cheque->bank_id = $request->bank_id;
            $cheque->office_id = Auth::user()->office_id;
            $cheque->from = $request->from;
            $cheque->to = $request->to;
            $cheque->user_id = Auth::user()->id;
            $cheque->created_date_bs = $today_nepali_date;
            $cheque->save();

            for ($i = $request->from; $i <= $request->to; $i++) {
                $cheque_number = $i;
                $cheque_details = new ChequeDetails();
                $cheque_details->cheque_id = $cheque->id;
                $cheque_details->cheque_number = $cheque_number;
                $cheque_details->bank_id = $request->bank_id;
                $cheque_details->office_id = Auth::user()->office_id;
                $cheque_details->save();
            }
            return redirect(route('cheque'));
        } catch (\Exception $e) {
            $message = "Can not add to table. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }
    }

    public function edit($id)
    {
        $bank = $this->bankRepo->get_by_id($id);
        return view('frontend.bank.edit', compact('bank'));
    }

    public function chequePrint()
    {
        $current_fiscal_year = $this->fiscalYearHelper->get_active_fiscal_year();
        $office_id = Auth::user()->office_id;
        $cheque_details = ChequeDetails::where('office_id', $office_id)->where('status', 0)->get();
        # TODO: Journel need to be approved
        $pre_bhuktanies = PreBhuktani::where('office_id', $office_id)->where('cheque_print', 0)->where('fiscal_year', $current_fiscal_year->id)->orderBy('journel_id', 'asc')->get();
        $pre_bhuktani_list = collect([]);
        foreach ($pre_bhuktanies as $index => $pre_bhuktani) {
            // Check if the $pre_bhuktani_list already contains an item with the same journel_id and party_id
            $existing_item = $pre_bhuktani_list->first(function ($item) use ($pre_bhuktani) {
                return $item['journal_id'] == $pre_bhuktani->journel_id && $item['party_id'] == $pre_bhuktani->bhuktani_paaune;
            });
            if ($existing_item) {
                $index = $pre_bhuktani_list->search($existing_item);
                $pre_bhuktani_list->put($index, [
                    'pre_bhuktani_id' => $pre_bhuktani->id,
                    'budget_sub_head_name' => $pre_bhuktani->budget_sub_head->name,
                    'journal_id' => $pre_bhuktani->journel_id,
                    'journal_number' => $pre_bhuktani->voucher->jv_number,
                    'journal_date' => $pre_bhuktani->voucher->data_nepali,
                    'party_id' => $pre_bhuktani->bhuktani_paaune,
                    'party_name' => $pre_bhuktani->party->name_nep,
                    'amount' => $pre_bhuktani_list[$index]['amount'] + $pre_bhuktani->amount,
                ]);
            } else {
                $temp = [
                    'pre_bhuktani_id' => $pre_bhuktani->id,
                    "budget_sub_head_name" => $pre_bhuktani->budget_sub_head->name,
                    "journal_id" => $pre_bhuktani->journel_id,
                    "journal_number" => $pre_bhuktani->voucher->jv_number,
                    "journal_date" => $pre_bhuktani->voucher->data_nepali,
                    "party_id" => $pre_bhuktani->bhuktani_paaune,
                    "party_name" => $pre_bhuktani->party->name_nep,
                    "amount" => $pre_bhuktani->amount,
                ];
                $pre_bhuktani_list->push($temp);
            }
        }
        return view('frontend.cheque.print', compact('current_fiscal_year', 'cheque_details', 'pre_bhuktani_list'));
    }

    public function confirmChequePrint(Request $request)
    {
        $journal_id = $request->journal_id;
        $cheque_number_id = $request->cheque_number_id;
        $cheque_number = $request->cheque_number;

        // Cheque print for one party for multiple bhuktani
        $pre_bhuktanies =  PreBhuktani::where('journel_id', $journal_id)->get();
        if ($pre_bhuktanies->contains(function ($item) use ($request) {
            return $item->amount == $request->amount;
        })) {
            // Cheque print for only one pre bhukatni for one party
            $pre_bhuktanies =  PreBhuktani::where('journel_id', $journal_id)->where('id', $request->bhuktani_id)->get();
        }
        foreach ($pre_bhuktanies as $pre_bhuktani) {
            $pre_bhuktani->cheque_print = $cheque_number;
            $pre_bhuktani->save();
        }

        $cheque_details = ChequeDetails::findorfail($cheque_number_id);
        $cheque_details->status = 1;
        $cheque_details->save();
        return json_encode('true');
    }
}
