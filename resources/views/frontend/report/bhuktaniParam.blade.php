@extends('frontend.layouts.app')
@section('content')
    <style>
        table tbody tr:hover {
            background-color: #e1e1e1 !important;;
        }

        table thead th {
            text-align: center;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-file"></i> भुक्तानी आदेश </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body p-15">
                    {{--          {{route('report.bhuktani.adesh')}}--}}
                    <form action="#" method="post" target="_blank">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-3">
                                    <label for="office">कार्यालय : </label>
                                    <select name="office" class="form-control">
                                        <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label>आर्थिक वर्ष : </label>
                                    <select name="fiscal_year" id="fiscal_year" class="form-control">
                                        @foreach($fiscalYears as $fy)
                                            <option value="{{$fy->id}}" @if($fy->id == $fiscalYear->id) selected @endif>{{$fy->year}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label>बजेट उप शिर्षक</label>
                                    <select class="form-control select2" name="budget_sub_head" id="budget_sub_head" required>
                                        <option value="">................</option>
                                        @foreach($programs as $program)
                                            <option value="{{$program->id}}">{{$program->name}}
                                                | {{$program->program_code}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2 center-block">
                                    <label>महिना</label>
                                    <select class="form-control select2" name="month" id="month" required>
                                        <option value="10">बैशाख</option>
                                        <option value="11">जेष्ठ</option>
                                        <option value="12">असार</option>
                                        <option value="1">साउन</option>
                                        <option value="2">भदौ</option>
                                        <option value="3">असोज</option>
                                        <option value="4">कार्तिक</option>
                                        <option value="5">मंसिर</option>
                                        <option value="6">पुस</option>
                                        <option value="7">माघ</option>
                                        <option value="8">फागुन</option>
                                        <option value="9">चैत्र</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="">&nbsp;</label>
                                    <button type="submit" class="btn btn-primary btn-block" id="btn_bhuktani_list">
                                        Submit
                                    </button>
                                </div>

                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
        <section style="padding: 0 13px">
            <div class="panel panel-primary">
                <div class="panel-body p-12">
                    <table class="table table-striped" border="1" id="bhuktani_list_table">
                        <thead style="background-color: #c6c5c5">
                        <tr>
                            <th>सि. नं.</th>
                            <th>भुक्तानि आदेश नं.</th>
                            <th>मिति</th>
                            <th>रकम</th>
                            <th>एक्सन</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
    </section>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#fiscal_year').change(function () {
                $('#budget_sub_head').val(null).trigger('change');
                $('#bhuktani_list_table tbody').html('')
            })
        })
    </script>
    <script>
        $(document).on('click', '#btn_bhuktani_list', function (e) {
            e.preventDefault();
            let data = {};
            data['fiscal_year'] = $('select#fiscal_year').val();
            data['budget_sub_head'] = $('select#budget_sub_head').val();
            data['month'] = $('select#month').val();
            let url = '{{route('get.bhuktani.list')}}';
            $.ajax({
                url: url,
                method: 'GET',
                data: data,
                success: function (res_) {
                    let tr = '';
                    let parseRes = $.parseJSON(res_);
                    if (parseRes.length > 0) {
                        let i = 1;
                        $.each(parseRes, function (key, value) {
                            let url = '{{route('report.bhuktani.adesh',123)}}';
                            url = url.replace('123', this.id);
                            tr += "<tr>" +
                                "<td class='kalimati' style='text-align: center'>" +
                                i +
                                "</td>" +

                                "<td class='activity kalimati' data-id='' style='text-align: center'>" +
                                '<a href="' + url + '" target="_blank" onclick="getBhuktaniReport(event)" data-index="' + key + '" class="kalimati" >' + this.adesh_number + '</a>' +
                                "</td>" +

                                "<td class='byahora' style='text-align: center'>" +
                                '<a href="' + url + '" target="_blank" onclick="getBhuktaniReport(event)" data-index="' + key + '" class="kalimati" >' + this.date_nepali + '</a>' +
                                "</td>" +

                                "<td class='details kalimati' style='text-align: right'>" +
                                this.amount.toFixed(2) +
                                "</td>" +
                                "<td style='text-align: center'>" +
                                '<a href="' + url + '" target="_blank" onclick="getBhuktaniReport(event)" data-index="' + key + '" >हेर्ने</a>' +
                                "</td>";
                            i = i + 1;
                        });
                        $('#bhuktani_list_table').find('tbody').html(tr);
                    } else {
                        tr += "<tr>" +
                            "<td colspan='7' align='center'>" +
                            "विवरण प्रविश्टि भएको छैन !!" +
                            "</td>";
                        $('#bhuktani_list_table').find('tbody').html(tr);
                    }


                }

            })
        })
    </script>


    {{--  currnet month--}}
    <script>
        $(document).ready(function () {
            var currentDate = new Date();
            var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
            let month = currentNepaliDate.bsMonth;
            if (month == 1) {
                $finalmonth = 10;
            }
            if (month == 2) {
                $finalmonth = 11;
            }
            if (month == 3) {
                $finalmonth = 12;
            }
            if (month == 4) {
                $finalmonth = 1;
            }
            if (month == 5) {
                $finalmonth = 2;
            }
            if (month == 6) {
                $finalmonth = 3;
            }
            if (month == 7) {
                $finalmonth = 4;
            }
            if (month == 8) {
                $finalmonth = 5;
            }
            if (month == 9) {
                $finalmonth = 6;
            }
            if (month == 10) {
                $finalmonth = 7;
            }
            if (month == 11) {
                $finalmonth = 8;
            }
            if (month == 12) {
                $finalmonth = 9;
            }

            $('#month').val($finalmonth);
        });


    </script>

@endsection