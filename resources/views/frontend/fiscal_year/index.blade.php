@extends('frontend.layouts.app')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> <i class="fas fa-university"></i> बैङ्क विवरण </h1>

            <button type="button" class="btn btn-primary btn-sm btn-show-form" id="showAddForm"><i class="fas fa-plus"></i> नयाँ थप गर्ने</button>
        </section>

        <!-- FORM section start -->
        <section class="formSection displayNone" id="showThisForm">
            <div class="row justify-content-md-center">
                <div class="col-md-8 form-bg col-md-offset-2">
                    <button type="button" class="btn btn-danger btn-sm btn-show-form displayNone" id="closeAddForm"><i class="fas fa-times"></i> फर्म रद्ध गर्ने</button>
                    <div class="form-title">बैङ्क विवरण प्रविष्टी गर्ने</div>
                    <form action="{{route('bank.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="name">आर्थिक वर्ष :</label>
                                <input type="text" class="form-control" id="name" placeholder="बैङ्क नाम" name="name" required>
                            </div>
                            <div class="col-md-6">
                                <label for="address">कोड :</label>
                                <input type="text" class="form-control" id="address" placeholder="ठेगाना" name="address" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2 col-md-offset-5">
                                <button type="submit" class="btn btn-primary btn-show-form btn-block" style="margin-top: 18px;">थप</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- FORM section ends -->

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body pt-15">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>क्र.स.</th>
                            <th>बैङ्कको नाम</th>
                            <th>ठेगाना</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($fiscalYears as $index=>$fiscalYear)
                            <tr>
                                <td>{{++$index}}</td>
                                <td>{{$fiscalYear->year}}</td>
                                <td>{{$fiscalYear->code}}</td>

                                <td>
                                    <input type="radio" href="{{route('bank.edit',$fiscalYear->id)}}" class="btn btn-sm btn-primary">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')
    <script>
        $(document).on('click','#delete_bank',function () {

            let bank_id = $(this).attr('data-bank-id');
            let url = '{{route('bank.delete',123)}}';
            url = url.replace('123',bank_id);
            swal({
                title: "Are you sure?",
                text: "Delete भए पछि Recovere हुदैन",
                icon: "warning",
                buttons: true,
                dangerMode: true,

            }).then((willDelete) => {

                if (willDelete) {

                    $.ajax({

                        url : url,
                        method : 'get',
                        success : function (res) {
                            if(res){
                                swal("Bank has been deleted!", {
                                    icon: "success",
                                });

                                location.reload();
                            }

                        }
                    })

                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
        })
    </script>


@endsection
