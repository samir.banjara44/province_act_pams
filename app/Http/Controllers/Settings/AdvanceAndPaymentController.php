<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\AdvanceAndPayment;
use App\Repositories\ProgramRepository;
use App\Repositories\AdvancePaymentRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class AdvanceAndPaymentController extends Controller
{

    private $advanceAndPaymentRepo;

    public function __construct(AdvancePaymentRepository $advanceAndPaymentRepo)
    {

        $this->middleware('auth');
        $this->advanceAndPaymentRepo = $advanceAndPaymentRepo;
    }

    public function index()
    {
        $advanceAndPayments= AdvanceAndPayment::all();
        return view('backend.settings.advanceAndPayment.index', compact('advanceAndPayments'));
    }

    public function create(){

        return view('backend.settings.advanceAndPayment.create');
    }
    public function store(Request $request){
        $program = $this->advanceAndPaymentRepo->create($request->all());
        return redirect(route('admin.advance'));
    }

    public function edit($id){
        $advanceandpayment = $this->advanceAndPaymentRepo->get_by_id($id);
        return view('backend.settings.advanceAndPayment.edit',compact('advanceandpayment'));
    }

    public function update(Request $request, $id){
        $advanceandpayment = $this->advanceAndPaymentRepo->update($request->all(),$id);
        return redirect()->back();
    }

}
