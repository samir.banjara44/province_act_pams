<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         Schema::defaultStringLength(191);
        Blade::directive('moneyFormat', function ($money) {
            return "<?php echo number_format($money, 2); ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
         $this->app->bind('App\Repositories\HomeRepository','App\Repositories\HomeRepositoryEloquent');
         $this->app->bind('App\Repositories\ProgramRepository','App\Repositories\ProgramRepositoryEloquent');
         $this->app->bind('App\Repositories\BankRepository','App\Repositories\BankRepositoryEloquent');
         $this->app->bind('App\Repositories\AdvancePaymentRepository','App\Repositories\AdvancePaymentRepositoryEloquent');
         $this->app->bind('App\Repositories\SourceRepository','App\Repositories\SourceRepositoryEloquent');
         $this->app->bind('App\Repositories\BudgetRepository','App\Repositories\BudgetRepositoryEloquent');
         $this->app->bind('App\Repositories\AreaRepository','App\Repositories\AreaRepositoryEloquent');
         $this->app->bind('App\Repositories\SubAreaRepository','App\Repositories\SubAreaRepositoryEloquent');
         $this->app->bind('App\Repositories\MainProgramRepository','App\Repositories\MainProgramRepositoryEloquent');
         $this->app->bind('App\Repositories\DarbandiSrotRepository','App\Repositories\DarbandiSrotRepositoryEloquent');
         $this->app->bind('App\Repositories\DarbandiTypeRepository','App\Repositories\DarbandiTypeRepositoryEloquent');
         $this->app->bind('App\Repositories\SewaRepository','App\Repositories\SewaRepositoryEloquent');
         $this->app->bind('App\Repositories\DesignationRepository','App\Repositories\DesignationRepositoryEloquent');
         $this->app->bind('App\Repositories\TahaRepository','App\Repositories\TahaRepositoryEloquent');
         $this->app->bind('App\Repositories\BudgetSourceLevelRepository','App\Repositories\BudgetSourceLevelRepositoryEloquent');
         $this->app->bind('App\Repositories\MediumRepository','App\Repositories\MediumRepositoryEloquent');
         $this->app->bind('App\Repositories\SourceTypeRepository','App\Repositories\SourceTypeRepositoryEloquent');
         $this->app->bind('App\Repositories\MainActivityRepository','App\Repositories\MainActivityRepositoryEloquent');
         $this->app->bind('App\Repositories\ExpenseHeadRepository','App\Repositories\ExpenseHeadRepositoryEloquent');
         $this->app->bind('App\Repositories\VoucherRepository','App\Repositories\VoucherRepositoryEloquent');
         $this->app->bind('App\Repositories\VoucherDetailsRepository','App\Repositories\VoucherDetailsRepositoryEloquent');
         $this->app->bind('App\Repositories\PreBhuktaniRepository','App\Repositories\PreBhuktaniRepositoryEloquent');
         $this->app->bind('App\Repositories\BhuktaniRepository','App\Repositories\BhuktaniRepositoryEloquent');
         $this->app->bind('App\Repositories\KarmachariRepository','App\Repositories\KarmachariRepositoryEloquent');
         $this->app->bind('App\Repositories\SamuhaRepository','App\Repositories\SamuhaRepositoryEloquent');
         $this->app->bind('App\Repositories\PartyTypesRepository','App\Repositories\PartyTypesRepositoryEloquent');
         $this->app->bind('App\Repositories\RetentionRecordRepository','App\Repositories\RetentionRecordRepositoryEloquent');
         $this->app->bind('App\Repositories\RetentionBankGuaranteeRepository','App\Repositories\RetentionBankGuaranteeRepositoryEloquent');
         $this->app->bind('App\Repositories\VoucherSignatureRepository','App\Repositories\VoucherSignatureRepositoryEloquent');
         $this->app->bind('App\Repositories\RetentionVoucherRepository','App\Repositories\RetentionVoucherRepositoryEloquent');
         $this->app->bind('App\Repositories\RetentionPreBhuktaniRepository','App\Repositories\RetentionPreBhuktaniRepositoryEloquent');
         $this->app->bind('App\Repositories\RetentionBhuktaniRepository','App\Repositories\RetentionBhuktaniRepositoryEloquent');
         $this->app->bind('App\Repositories\DartaRepository','App\Repositories\DartaRepositoryEloquent');
         $this->app->bind('App\Repositories\ChalaniRepository','App\Repositories\ChalaniRepositoryEloquent');
         $this->app->bind('App\Repositories\BranchRepository','App\Repositories\BranchRepositoryEloquent');
         $this->app->bind('App\Repositories\ActivityReportDetailRepository','App\Repositories\ActivityReportDetailRepositoryEloquent');
         $this->app->bind('App\Repositories\ActivityProgressRepository','App\Repositories\ActivityProgressRepositoryEloquent');
    }
}
