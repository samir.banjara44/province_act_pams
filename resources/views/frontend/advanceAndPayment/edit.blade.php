@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-edit"></i> पेश्की/भुक्तानी पाउने/धरौटी/व्यक्तिगत खाता सम्पादन गर्ने </h1>

            <a href="{{route('advance')}}" class="btn btn-sm  btn-primary"><i class="fas fa-arrow-left"></i> लिष्टमा
                जाने</a>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('AdvanceAndPayment.update',$advanceandpayment->id)}}" method="post" id="formUpdate">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="party_type" style="display: block;">प्रकार</label>
                                    <select name="party_type" id="party_type" required class="form-control" @if($preBhuktaniCount >= 1) disabled="disabled"@endif>
                                        @foreach($party_types as $party_type)
                                            @if($party_type->id != 5)
                                                <option value="{{$party_type->id}}"
                                                        @if($party_type->id == $advanceandpayment['party_type'])selected @endif>{{$party_type->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="name_nep">नाम नेपालीमा</label>
                                    <input type="text" class="form-control" id="name_nep" placeholder="नाम नेपालीमा"
                                           name="name_nep" value="{{$advanceandpayment->name_nep}}" required>
                                </div>
                                <div class="col-md-2">
                                    <label for="name_eng">नाम अंग्रेजीमा</label>
                                    <input type="text" class="form-control" id="name_eng" placeholder="नाम अंग्रेजीमा"
                                           name="name_eng" value="{{$advanceandpayment->name_eng}}">
                                </div>
                                <div class="col-md-2">
                                    <label for="mobile_number">मोबाइल नं.</label>
                                    <input type="text" class="form-control" id="mobile_number"
                                           placeholder="मोबाइल नं." name="mobile_number"
                                           value="{{$advanceandpayment->phone_number}}">
                                </div>
                                <div class="col-md-2">
                                    <label for="citizen_number">नागरिकता नं.</label>
                                    <input type="text" class="form-control" id="citizen_number"
                                           placeholder="citizen_number" name="citizen_number"
                                           value="{{$advanceandpayment->citizen_number}}">
                                </div>
                                <div class="col-md-2">
                                    <label for="vat_number">भ्याट/प्यान नं.</label>
                                    <input type="text" class="form-control" id="vat_number"
                                           placeholder="vat_number" name="vat_number"
                                           value="{{$advanceandpayment->vat_pan_number}}">
                                </div>
                                <div class="col-md-2">
                                    <label for="vat_office">स्थायी लेखा नम्वर जारी गर्ने कार्यालय र
                                        ठेगाना</label>
                                    <select id="vat_office" class="form-control select2" name="vat_office">
                                        <option value="" selected="">.........</option>
                                        @foreach($vat_offices as $vat_office)
                                            <option value="{{$vat_office->id}}"
                                                    @if($vat_office->id == $advanceandpayment['vat_office']) selected @endif>{{$vat_office->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="bank">बैङ्क</label>
                                    <select name="bank" class="form-control">
                                        <option value="" selected="">.........</option>
                                        @foreach($all_banks as $bank)
                                            <option value="{{$bank->id}}"
                                                    @if($bank->id == $advanceandpayment['bank']) selected @endif>{{$bank->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label>शाखा कार्यालय</label>
                                    <input type="text" class="form-control" name="bank_address"
                                           id="bank_address" value="{{$advanceandpayment->bank_address}}">
                                </div>
                                <div class="col-md-2">
                                    <label>खाता नं.</label>
                                    <input type="text" class="form-control" name="khata_number"
                                           id="khata_number" value="{{$advanceandpayment->party_khata_number}}">
                                </div>
                                <div class="col-md-2">
                                    <label>पेयी कोड</label>
                                    <input type="number" class="form-control" name="payee_code" id="payee_code"
                                           value="{{$advanceandpayment->payee_code}}">
                                </div>
                                <div class="col-md-2">
                                    <label for="is_advance">पेश्किमा देखाउने</label>
                                    <select name="is_advance" required class="form-control">
                                        <option value="0"
                                                @if($advanceandpayment->is_advance == 0) selected @endif>हो
                                        </option>
                                        <option value="2"
                                                @if($advanceandpayment->is_advance == 2) selected @endif>होइन
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="is_bhuktani">भुक्तानीमा देखाउने</label>
                                    <select name="is_bhuktani" required class="form-control">
                                        <option value="0"
                                                @if($advanceandpayment->is_bhuktani == 0) selected @endif>हो
                                        </option>
                                        <option value="2"
                                                @if($advanceandpayment->is_bhuktani == 2) selected @endif>होइन
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="is_dharauti">धरौटीमा देखाउने</label>
                                    <select name="is_dharauti" required class="form-control">
                                        <option value="0"
                                                @if($advanceandpayment->is_dharauti == 0) selected @endif>हो
                                        </option>
                                        <option value="2"
                                                @if($advanceandpayment->is_dharauti == 2) selected @endif>होइन
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <center>
                                        <label for="">&nbsp;</label>
                                        <button type="submit" class="btn btn-success btn-block" id="btnUpdate"><i
                                                    class="fas fa-check-square"></i> सम्पादन गर्ने
                                        </button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')
    <script>
        $(document).on('click','#btnUpdate', function () {
            $('#party_type').attr("disabled",false);
            $('#formUpdate').submit();
        })
    </script>
@endsection