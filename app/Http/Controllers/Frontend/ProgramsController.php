<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Programs;
use App\Models\Province;
use App\Repositories\ProgramRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class ProgramsController extends Controller
{

    private $programRepo;

    public function __construct(ProgramRepository $programRepo)
    {
        $this->middleware('auth');
        $this->programRepo = $programRepo;
    }

    public function index()
    {
        $programes= $this->programRepo->get_by_office_id();
        return view('frontend.program.index', compact('programes'));
    }

    public function create(){

        return view('frontend.program.create');
    }


    public function store(Request $request){
        try {
            $program = $this->programRepo->create($request->all());
            $requestName = $request->name;
            $message = "Created ".$requestName." Successfully";
            return redirect(route('program'))->with('success',$message);
        } catch (\Exception $e){
            $message = "Can not add to table. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);

        }
    }

    public function edit($id){
        $program = $this->programRepo->get_by_program_id($id);
        return view('frontend.program.edit',compact('program'));
    }

    public function update(Request $request, $id){
        if($request->name && $request->program_code && $request->expense_type) {
            try {
                $program = $this->programRepo->update($request->all(), $id);
                $requestName = $request->name;
                $message = "Created " . $requestName . " Successfully";
                return redirect(route('program'))->with('success', $message);
            } catch (\Exception $e) {
                $message = "Can not update. Please fill all the required fields first before Submitting";
                return redirect()->back()->with('error', $message);
            }
        } else {
            $message = "Can not update. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }

    }

    public function listByOfficeId(){

        $office_id = Auth::user()->office_id;
         return $program_list = $this->programRepo->get_by_office_id($office_id);
    }


    public function delete($program_id){


//        $delete_program = Programs::where('id',$program_id)->delete();
        $update = Programs::findorfail($program_id)->update(['status'=>0]);

        return json_encode($update);
//        return redirect(route('program'));
    }

}
