
@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        प्राप्तीको स्रोत:
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('admin.source')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('admin.source.store')}}" method="post">
            {{csrf_field()}}

            <div class="form-group">
              <label for="name">स्रोत समुह:</label>

              <select class="form-control" name="source_prakar" id="source_prakar">
                @foreach($source_prakars as $source_prakar)
                <option value="{{$source_prakar->id}}">{{$source_prakar->name}}</option>
                  @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="name">प्राप्तीको स्रोत:</label>
              <input type="text" class="form-control" id="name" placeholder="Source Name" name="name">
            </div>
            <div class="form-group">
              <label for="source_code">स्रोत कोड:</label>
              <input type="text" class="form-control" id="source_code" placeholder="Source Code" name="source_code">
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection