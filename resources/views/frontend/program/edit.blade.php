@extends('frontend.layouts.app')
@section('title')
  Program Edit
@stop
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Program Edit

      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('program')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('program.update',$program->id)}}" method="post" id="programCreateForm">
            {{csrf_field()}}
            <div class="form-group">
              <label for="name">Name:</label>
              <input type="text" class="form-control" id="name" required placeholder="Program Name" name="name" value="{{$program->name}}">
            </div>
            <div class="form-group">
              <label for="program_code">Code:</label>
              <input required type="text" class="form-control" id="program_code" placeholder="Program Code" name="program_code" value="{{$program->program_code}}">
            </div>
            <div class="form-group">
              <label for="expense_type">प्रकार:</label>
              <select class="form-control" name="expense_type" required>
                <option value="1" @if($program->expense_type == 1) selected @endif>चालु</option>
                <option value="2" @if($program->expense_type == 2) selected @endif>पुजीं</option>
              </select>
            </div>
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('scripts')
  <script>
    $("#programCreateForm").validate({
      rules: {
        name: {
          required: true,
        },
        program_code: {
          required: true,
        }
      },
      messages: {
        name: "Name Field is Required"
      }
    });
  </script>
@stop