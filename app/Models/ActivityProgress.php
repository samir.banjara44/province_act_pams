<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ActivityProgress extends Model
{
    protected $table = 'activity_progress';
    protected $guarded = [];

    public function getActivityName(){
        return $this->belongsTo('App\Models\BudgetDetails','budget_details_id','id');
    }
}