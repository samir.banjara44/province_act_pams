<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">


<style>
    table {
        border-collapse: collapse;
    }
    tbody tr:hover {
        background-color: #e1e1e1 !important;

    }

    @media print {
        #downloadMe {
            display: none;
        }
    }
    body th {
        background: #e1e1e1 !important;
        position: sticky;
        top: 0;
        box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4);
    }

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <input type="button" style="position: absolute; top:20px;right: 20px" value="Export to Excel" id="downloadMe">
    <section class="content-header">
        <div id="tableHeadingWrapper">
            <table width="99%" id="headingTable" style="font-size:13px">
                <tr>
                    <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                    <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
                        सभा @endif</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
                </tr>
                <tr>
                    <td colspan="6"
                        style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">
                        @if(Auth::user()->office->department)
                            {{Auth::user()->office->department->name}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}, , {{Auth::user()->office->district->name}}</td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div style="width: 100%; text-align: center">
                            <b>कार्यक्रम / परियोजना अनुसार बजेट बिनियोजन</b><br>
                        </div>
                        <div style="float: right; margin-top: -20px">

                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 15px">बजेट उपशिर्षक नाम : <b><span class="kalimati">{{$program->name}}</span></b><br>
                        बजेट उपशिर्षक न.: <b>{{$program->program_code}}</b></td>
                    <td style="text-align: right; padding-right: 20px">आर्थिक वर्ष : <span class="kalimati">{{$fiscalYear->year}}</span></td>
                </tr>
            </table>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body" id="tableWrapper">
                <table width="99%" border="1" id="contentTable" style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th rowspan="3" width="13px">सि.नं.</th>
                        <th rowspan="3" style="width: 20%;">कार्यक्रम/आयोजनाको नाम</th>
                        <th rowspan="3">कार्यक्रम कोड</th>
                        <th rowspan="3">खर्च शीर्षक</th>
                        <th rowspan="3" width="100px">स्रोत</th>
                        <th rowspan="3">लक्ष</th>
                        <th colspan="7">विनियोजन रु.</th>
                        <th colspan="4">खर्च</th>
                        {{--                        <th rowspan="3">बाकींँ</th>--}}
                        <th rowspan="3">कैफियत</th>
                    </tr>

                    <tr>
                        <th colspan="2">प्रथम चौमासिक</th>
                        <th colspan="2">दोश्रो चौमासिक</th>
                        <th colspan="2">तेस्रो चौमासिक</th>
                        <th rowspan="2">जम्मा</th>
                        <th rowspan="2">प्रथम चौमासिक</th>
                        <th rowspan="2">दोश्रो चौमासिक</th>
                        <th rowspan="2">तेस्रो चौमासिक</th>
                        <th rowspan="3">जम्मा</th>
                    </tr>
                    <tr>
                        <td style="text-align: center">बजेट</td>
                        <td style="text-align: center">भार</td>
                        <td style="text-align: center">बजेट</td>
                        <td style="text-align: center">भार</td>
                        <td style="text-align: center">बजेट</td>
                        <td style="text-align: center">भार</td>
                    </tr>

                    </thead>
                    <tbody>
                    @foreach($activities as $index=>$activity)
                        <tr style="background-color: #ffffff">

                            <td class="kalimati" style="text-align: center">{{++$index}}</td>
                            <td>{{$activity->activity}}</td>
                            <td class="kalimati" align="center">{{$activity->activity_code}}</td>
                            <td class="kalimati" style="text-align: center">{{$activity->expense_head}}</td>
                            <td>{{$activity->get_source_type->name}}-
{{--                                {{$activity->get_budget_source_level->name}}--}}
{{--                                -{{$activity->get_source->name}}---}}
                                {{$activity->get_medium->name}}</td>
                            <td class="kalimati" style="text-align: center">{{$activity->total_unit}}</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($activity->first_quarter_budget,2)}}</td>
                            <td class="kalimati" style="text-align: right">{{$activity->first_chaimasik_bhar}}</td>
                            <td class="kalimati" style="text-align: right">{{number_format($activity->second_quarter_budget,2)}}</td>
                            <td class="kalimati" style="text-align: right">{{$activity->second_chaimasik_bhar}}</td>
                            <td class="kalimati" style="text-align: right">{{number_format($activity->third_quarter_budget,2)}}</td>
                            <td class="kalimati" style="text-align: right">{{$activity->third_chaimasik_bhar}}</td>
                            <td class="kalimati" style="text-align: right">{{number_format($activity->total_budget,2)}}</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($activity->firstChaumasikExpense($datas['office'],$datas['budget_sub_head'],$activity->id),2)}}</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($activity->secondChaumasikExpense($datas['office'],$datas['budget_sub_head'],$activity->id),2)}}</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($activity->thirdChaumasikExpense($datas['office'],$datas['budget_sub_head'],$activity->id),2)}}</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($activity->totalExpenseByActivity($datas['office'],$datas['budget_sub_head'],$activity->id),2)}}</td>
                            <td></td>
                            {{--                            <td></td>--}}
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="6" style="text-align: right">जम्मा</td>
                        <td style="text-align: right"><span class="kalimati">{{number_format($first_quarter_budget_total)}}</span></td>
                        <td style="text-align: center"></td>
                        <td style="text-align: right"><span class="kalimati">{{number_format($second_quarter_budget_total)}}</span></td>
                        <td style="text-align: center"></td>
                        <td style="text-align: right"><span class="kalimati">{{number_format($third_quarter_budget_total)}}</span></td>
                        <td style="text-align: center"></td>
                        <td class="kalimati" style="text-align: right">{{$budget_sum}}<span class="kalimati"></span></td>
                        <td class="kalimati" style="text-align: right">{{$total_first_quarter_expense}}</td>
                        <td class="kalimati" style="text-align: right">{{$total_second_quarter_expense}}</td>
                        <td class="kalimati" style="text-align: right">{{$total_third_quarter_expense}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalExpenseByBudgetSubHead}}</td>
                        <td class="kalimati" style="text-align: right"></td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script>
    let changeToNepali = function (text) {
        textTrim = $.trim(text)
        let numbers = textTrim.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (!isNaN(value) && value != ' ') {
                    if (value == 1)
                        nepaliNo += "१";
                    else if (value == 2)

                        nepaliNo += "२";
                    else if (value == 3)

                        nepaliNo += "३";
                    else if (value == 4)

                        nepaliNo += "४";
                    else if (value == 5)

                        nepaliNo += "५";
                    else if (value == 6)

                        nepaliNo += "६";
                    else if (value == 7)

                        nepaliNo += "७";
                    else if (value == 8)

                        nepaliNo += "८";
                    else if (value == 9)

                        nepaliNo += "९";
                    else if (value == 0)

                        nepaliNo += "०";
                    else if (value == ',')

                        nepaliNo += ",";
                    else if (value == '.')

                        nepaliNo += ".";
                    else if (value == '/')

                        nepaliNo += "/";

                }
            }
        });
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });


    var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
            ,
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta charset="utf-8"/><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
            , format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            };

        return function (table, name, filename) {
            var table1 = '';
            if (!table1.nodeType) table1 = document.getElementById('headingTable')
            if (!table.nodeType) table = document.getElementById(table)
            var bigTable = table1.innerHTML + table.innerHTML;
            var ctx = {worksheet: name || 'Worksheet', table: bigTable}

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

    $("#downloadMe").click(function (e) {
        let allTable = $('div[id$=tableHeadingWrapper]').html() + $('div[id$=tableWrapper]').html();
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(allTable));
        e.preventDefault();
    });

</script>