@extends('frontend.layouts.app')

@section('styles')
    <style>
        #budgetStoreCreateFrom label {
            display: block !important;
        }
        th {
            color: white;
            font-size: 14px!important;
        }
    </style>
@endsection
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="font-size: 25px;">

                <small>भौचर ससंोधन </small>
            </h1>
            {{--            <ul class="breadcrumb">--}}
            {{--                <li><a type="button" href="{{route('admin.office')}}" class="btn btn-sm btn-primary"><i--}}
            {{--                                class="fa fa-pencil"></i> List</a></li>--}}
            {{--            </ul>--}}
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="flash-message">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                    </div>
                    <form action="{{route('voucher.store')}}" method="post" id="voucherStoreCreateFrom">
                        {{csrf_field()}}
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td colspan="6">
                                        <div class="form-group" style="width: 150px;">
                                            <label for="fiscal_year"> मिति:</label>
                                            <input type="text" class="form-control" value="{{$voucher->data_nepali}}" name="date" id="date" value="" required style="height: 32px;">
                                        </div>

                                        <div class="form-group" style="width: 265px;">
                                            <label for="program">बजेट उपशिर्षक</label>
                                            <select name="program" class="form-control" id="program" required style="height: 32px;">
                                                <option value="">..................</option>
                                                @foreach($programs as $program)
                                                    <option value="{{$program->id}}">{{$program->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group" style="width: 346px;">
                                            <label for="mainActivityName">कार्यक्रम आयोजनाको नाम</label>
                                            <select name="main_activity_name" class="form-control" id="main_activity_name" required style="height: 32px;">
                                                <option value="">.........................</option>
                                            </select>
                                        </div>

                                    </td>
                                    <td rowspan="5">
                                        <table style="color: red;">
                                            <tr>
                                                <td style="inline-size: 377px;">भौचर न.:{{$voucher->jv_number}}<span id="voucher_number"></span></td>

                                            </tr>
                                            <tr>
                                                <td>बजेट :<span id="total_budget"></span></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>खर्च :</td>
                                                <input type="hidden" id="hidden_expense">
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>पेश्की :</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>मौज्दात :<span id="remain_budget"></span></td>
                                                <input type="hidden" id="hidden_remain_budget" >

                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group" style="width: 150px;">
                                            <label>डेविट/क्रेडिट</label>
                                            <select name="drOrCr" class="form-control" id="DrOrCr" required style="height: 32px;">
                                                <option value="1">डेबिट</option>
                                                <option value="2">क्रेडिट</option>
                                            </select>
                                        </div>
                                        <div class="form-group" style="width:110px;">
                                            <label>व्यहोरा</label>
                                            <select name="byahora" class="form-control" id="byahora" required style="height: 32px;">
                                                @foreach($ledgerTypes as $ledgerType)
                                                    <option value="{{$ledgerType->id}}">{{$ledgerType->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group" style="width:145px;">
                                            <label>हिसाब नं.</label>
                                            <select name="hisab_number" class="form-control" id="hisab_number" required style="height: 32px;">
                                                <option>...................</option>
                                            </select>
                                        </div>
                                        <div class="form-group" style="width: 346px;">
                                            <label>बिबरण</label>
                                            <input type="text" class="form-control" name="details" id="details" required style="height: 32px;">
                                        </div>
                                        <div class="form-group"></div>

                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group" style="width: 150px;">
                                            <label>पेश्कि पाउने प्रकार</label>
                                            <select name="party_type" class="form-control" id="party_type" disabled style="height: 32px;">
                                                <option value="" selected>..............</option>
                                                <option value="1">उपभोक्ता समिति</option>
                                                <option value="2">ठेकेदार</option>
                                                <option value="3">व्यक्तिगत</option>
                                                <option value="4">संस्थागत</option>
                                                <option value="5">कर्मचारी</option>
                                            </select>
                                        </div>
                                        <div class="form-group" style="width: 130px">
                                            <label>पेश्कि पाउने</label>
                                            <select name="party-name" id="party-name" class="form-control" style="height: 32px;">
                                                <option value="" selected>..............</option>

                                            </select>

                                        </div>
                                        <div class="form-group" style="width: 130px">
                                            <label>रकम</label>
                                            <input type="number" name="amount" id="amount" class="form-control" required style="height: 32px;">
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary" id="voucher_submit" style="margin-top: 18px;">थप</button>
                                        </div>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <table class="table" id="voucher_table" border="1">
        <thead>
        <tr style="background-color: #236286;">
            <th>SN</th>
            <th>डे|क्रे</th>
            <th>कार्यक्रम|आयोजनाको नाम</th>
            <th>व्यहोरा हिसाब न।</th>
            <th class="hidden">खर्च शी्षक</th>
            <th>विवरण</th>
            <th>डेबिट रकम</th>
            <th>क्रेडिट रकम</th>
            <th>प्रापक प्रकार</th>
            <th>पेश्क पाउने</th>
            <th class="hidden">Peski Type</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <tr></tr>
        @foreach($voucherDetailsLIst as $key=>$voucherDetail )
            <tr class="{{++$key}} voucher-details" style="background-color: white;">
                <td class="sn">{{$key}}</td>

                @if($voucherDetail->dr_or_cr == 1)
                <td class='dr-cr' data-dr-cr="{{$voucherDetail->dr_or_cr}}">डे.</td>
                    @else
                    <td class='dr-cr' data-dr-cr="{{$voucherDetail->dr_or_cr}}">क्रे.</td>
                @endif

                <td class="activity" data-activity-id="{{$voucherDetail->main_activity_id}}" data-activity-name="{{$voucherDetail->mainActivity->activity}}">@if($voucherDetail->mainActivity){{$voucherDetail->mainActivity->activity}}
                    @endif
                </td>
                <td class="byahora" data-byahora_id="{{$voucherDetail->ledger_type_id}}">
                    @if($voucherDetail->ledgerType){{$voucherDetail->ledgerType->name}}@endif</td>
                <td class='hidden expense_head' data-expense-head_id="{{$voucherDetail->expense_head_id}}">{{$voucherDetail->expense_head_id}}</td>
                <td class="details">{{$voucherDetail->bibaran}}</td>
                <td class="drAmount">{{$voucherDetail->dr_amount}}</td>
                <td class="crAmount">{{$voucherDetail->cr_amount}}</td>
                <td class="partyType" data-party-type="{{$voucherDetail->party_type}}">{{$voucherDetail->party_type}}</td>
                <td class="party" data-party-id = "{{$voucherDetail->peski_paune_id}}">{{$voucherDetail->peski_paune_id}}</td>
                <td class="hidden peski_type" data-peski-type="{{$voucherDetail->peski_type}}">{{$voucherDetail->peski_type}}</td>
                <td class="edit-voucher-td" ><a href="#" class="edit-voucher" data-sn="{{$key}}">Edit</a> | <a href="#" class="delete-row">Delete</a></td>
            </tr>
         @endforeach
        </tbody>
    </table>

    <table id="dr_cr_total_table" width="100%" border="1">
        <tr>
            <td style="text-align: right">
                <span style="margin-right: 20px">जम्मा|</span>
            </td>
            <td id="dr_amount">0|</td>
            <td id="cr_amount">0|</td>
            <td style="margin-right: 20px"><span style="margin-right: 20px">फरक</span></td>
            <td style="color: red" id="amount_difference">0|</td>
        </tr>
    </table>

    <table class="table">
        <tr>
            <td>
                <div class="form-group">
                    <label>कारोवारको संक्षिप्त व्यहोरा</label>
                    <textarea class="form-control shortInfo"  name="shortInfo" id="shortInfo"  style="margin: 0px;width:361px;height: 35px;"></textarea>
                </div>
                <div class="form-group">
                    <label>कारोवारको विस्तृत व्यहोरा</label>
                    <textarea class="form-control detailsInfo" name="detailsInfo" id="detailsInfo" style="    margin: 0px;
    width: 876px;
    height: 35px;"></textarea>

                </div>

            </td>
        </tr>
    </table>

    <table class="table">
        <tr>
            <td colspan="7">
                भुक्तानी प्रायोजनको लागि
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>कार्यक्रम/आयोजना/क्रियाकलापको नाम</label>
                    <select class="form-control bhuktani_main_activity" id="bhuktani_main_activity" >
                    <option>............</option>
{{--                    @foreach($preBhuktanies as $PreBhuktani)--}}
{{--                            <option value="{{$PreBhuktani->main_activity_id}}">{{$PreBhuktani->main_activity->activity}}</option>--}}
{{--                    @endforeach--}}
                    </select>

                </div>
                <div class="form-group">
                    <label>प्रापक प्रकार/प्राप्तकर्ता</label>
                    <select name="bhuktani_party_type" id="bhuktani_party_type" class="form-control">
                        <option value="">..............</option>
                        <option value="1">उपभोक्ता समिति</option>
                        <option value="2">ठेकेदार</option>
                        <option value="3">व्यक्तिगत</option>
                        <option value="4">संस्थागत</option>
                        <option value="5">कर्मचारी</option>
                    </select>

                </div>
                <div class="form-group">
                    <label>प्राप्त कर्ता</label>
                    <select class="form-control bhuktani_party" id="bhuktani_party">
                        <option value="">.................</option>
                    </select>
                </div>
                <div class="form-group">
                    <label> भुक्तानी रकम</label>
                    <input type="number" name="bhuktani_amount" id="bhuktani_amount" class="form-control">
                </div>
                <div class="form-group">
                    <label>अग्रिम कर कट्टी रकम</label>
                    <input type="number" name="advance_tax_deduction_amount" id="advance_tax_deduction_amount" class="form-control">

                </div>
                <div class="form-group">
                    <label>भ्याट रकम</label>
                    <input type="number" name="vat_amount" id="vat_amount" class="form-control">

                </div>
                <div class="form-group">
                    <input type="button" class="btn btn-primary" name="buktaniButton" id="buktaniButton" value="थप"
                           onclick="bhuktanivalidation()" style="margin-top: 18px;">
                </div>
            </td>
        </tr>
    </table>

    <table class="table" id="bhuktani" border="1">
        <thead>
        <tr style="background-color: #236286;">
            <th>सि न</th>
            <th>कार्यक्रम/आयोजना/क्रियाकलापको नाम</th>
            <th>प्रापक प्रकार</th>
            <th> प्राप्त कर्ता</th>
            <th> भुक्तानी रकम</th>
            <th>अग्रिम कर कट्टी रकम</th>
            <th>भ्याट रकम</th>
            <th class="hidden"></th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <tr></tr>
        @foreach($preBhuktanies as $key=>$preBhuktani)
            <tr class="{{++$key}}">
                <td class="sn">{{$key}}</td>
                <td class="bhuktani-main-activity" data-activity="{{$preBhuktani->main_activity_id}}">{{$preBhuktani->main_activity->activity}}</td>
                <td class="bhuktani-party-type" data-party-type="{{$preBhuktani->party_type}}">
                    {{$preBhuktani->partyko_type->name}}
                </td>

                <td class="bhuktani_party" data-bhuktani-party="{{$preBhuktani->bhuktani_paaune}}">
{{--                        @if($preBhuktani->peski_type = 1)--}}
{{--                            {{$preBhuktani->karmachari->name_nepali}}--}}
{{--                        @elseif($preBhuktani->peski_type = 2)--}}
{{--                            {{$preBhuktani->party->name_nep}}--}}
{{--                        @endif--}}
                </td>

                <td class="bhuktani_amount">{{$preBhuktani->amount}}</td>
                <td class="advance_tax_deduction" >{{$preBhuktani->advance_vat_deduction}}</td>
                <td class="vat" >{{$preBhuktani->vat_amount}}</td>
                <td class="hidden peski_type" data-peski-type="{{$preBhuktani->peski_type}}"></td>
                <td class="edit-voucher-td">
                    <a href="#" class="edit-bhuktani-row" data-sn="{{$key}}">Edit</a> | <a href="#" class="delete-bhuktani">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <table class="table-striped" id="bhuktaniAmountTotal" width="100%" border="1">
        <tr>
            <td style="text-align: right;width:769px;padding-right: 10px;">
                <span>जम्मा</span>
            </td>
            <td id="bhuktaniTotal" style="padding-left: 11px;">
                0
            </td>
        </tr>
    </table>
    <div class="submit-buttonss btn-group">
        <button class="btn btn-primary" id="VoucherUpdate" onclick="voucherUpdate()" type="button" disabled> Update</button>
        <button class="btn btn-warning" type="button"> Clear</button>
    </div>
@endsection
@section('scripts')


{{--    Beta Version--}}


{{--बजेट उपशिर्षक change हुदा--}}

<script>
    $(document).ready(function () {
        $('#program').change(function () {
            let program_code = $('#program').val();
            get_main_activity_by_budget_sub_head(program_code);

        })
    })

</script>

{{--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////--}}



{{--Create--}}



{{-- बजेट उपशिर्षक change हुदा  कार्यक्रम आउने  Event--}}
<script>
    $(document).ready(function () {
        $('#program').change(function () {
            let program_code = $('#program').val();
            get_main_activity_by_budget_sub_head(program_code);

        })
    })

</script>

{{-- बजेट उपशिर्षक change हुदा कार्यक्रम आउने  function--}}
<script>
    let get_main_activity_by_budget_sub_head = function (program_code, activity_id = '') {
        let url = '{{route('get_main_activity_by_budget_sub_head',123)}}';
        url = url.replace(123, program_code);
        $.ajax({
            url: url,
            method: 'get',
            success: function (res) {
                let option = '<option value="" selected>.......</option>';
                let activities = $.parseJSON(res);
                console.log(activities);
                if(res){
                    $.each(activities, function () {
                        if (this.id == activity_id) {
                            option += '<option value="' + this.id + '" selected>' + this.expense_head_by_id.expense_head_code +' | '+ this.activity +' |रु. '+ this.total_budget +'</option>'
                            get_expense_head_by_activity_id(this.id);
                        } else {
                            option += '<option value="' + this.id + '">' + this.expense_head_by_id.expense_head_code + ' | '+ this.activity +' | '+ this.total_budget + '</option>'
                        }
                    })
                }
                $('#main_activity_name').html(option).focus();

            }
        })

    }
</script>

{{-- कार्यक्रम change हुदा Event--}}
<script>
    $(document).ready(function () {
        $('#main_activity_name').change(function () {

            let main_activity_id = $('#main_activity_name').val();
            let byahora = $('#byahora').val();
            if(byahora.length < 1) {

                $("#byahora").val("1").change();
            }

            if(main_activity_id != ''){

                get_expense_head_by_activity_id(main_activity_id);

            } else {
                if(byahora == 1 || byahora == 4){
                    $('#hisab_number').html("")
                    $('#details').val('')
                }else {

                    // alert("test what");

                }

            }
        })
    })
</script>

{{--कार्यक्रम change हुदा बजेट र खर्च देखिने Event--}}
<script>
    let global_remain_budget = 0;
    $(document).ready(function () {
        $('#main_activity_name').change(function () {
            let main_activity_id = $('#main_activity_name').val();
            get_budget_by_main_Activity(main_activity_id);
            // get_expense_by_main_activity(main_activity_id);
        })
    });
</script>

{{--  कार्यक्रम change हुदा बजेट र खर्च देखिने Function  --}}
<script>
    let get_budget_by_main_Activity = function () {
        let main_activity_id = $('#main_activity_name').val();
        let url = '{{route('get_budget_and_expe_by_activity',123)}}';
        url = url.replace(123, main_activity_id);
        $.ajax({
            url : url,
            method : 'get',
            success : function (result) {
                let data =  $.parseJSON(result);
                $('#total_budget').text(data['budget']).addClass('e-n-t-n-n');
                $('#total_expense').text(data['expense']).addClass('e-n-t-n-n');
                $('#remain_budget').text(data['remain']).addClass('e-n-t-n-n');
                $('#hidden_remain_budget').val(data['remain']);
                global_remain_budget = data['remain'];
            }
        })
    };
</script>

{{-- get_expense_head_by_activity_id --}}
<script>
    let get_expense_head_by_activity_id = function (main_activity_id) {

        let url = '{{route('get_expense_head_by_activity_id',123)}}';
        url = url.replace(123, main_activity_id);
        $.ajax({
            url: url,
            method: 'get',
            success: function (res) {
                let data = $.parseJSON(res);
                // console.log("test",data[0]);
                let options = '<option selected>.........................</option>';

                options += '<option value="' + data[0].expense_head_by_id.id + '" selected>' + data[0].expense_head_by_id.expense_head_code + ' | ' + data[0].expense_head_by_id.expense_head_sirsak + '</option>'
                var selected_option = $('#byahora option:selected');
                console.log("byahora test",selected_option.val());
                if(selected_option.length == 0 || selected_option.val() == 1 || selected_option.val() == 4){

                    $('#hisab_number').html(options);
                    $('#details').val(data[0].expense_head_by_id.expense_head_sirsak);

                }
            }
        })
    }
</script>









{{--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////--}}


{{--Edit Section--}}

{{--माथिको voucher table बाट कार्यक्रम ल्याउने--}}

<script>
    $(document).ready(function () {

        let trs = $('#voucher_table tbody').find('.voucher-details');
        let data = {};
        $.each(trs,function (key, val) {

            let activity_name =  $(val).find('td.activity').attr('data-activity-name')
            let activity_id =  $(val).find('td.activity').attr('data-activity-id');
            data[activity_id] = activity_name;

        })
        // console.log(data);
         let option = '';
            $.each(data,function (key,val) {

                option += '<option value="'+key+'">'+val +'</option>'
            });

        $('#bhuktani_main_activity').html(option);
        // console.log(data);


    })
</script>

{{--    Edit बाट आएको data लाई show गर्ने--}}
<script>
        $(document).ready(function () {
            $('#program').attr("disabled",'disabled');
            let budget_sub_head = '{{$voucher->budget_sub_head_id}}'

            $("#program option").each(function()
            {
                 if($(this).val() == budget_sub_head){
                    $(this).prop('selected', true);
                     get_main_activity_by_budget_sub_head($(this).val());

                 }
            });

            let shortNarration = '{{$voucher->short_narration}}';
            $('#shortInfo').val(shortNarration);

            let longNarration = '{{$voucher->long_narration}}';
            $('#detailsInfo').val(longNarration);

        })
    </script>




{{--    End Section--}}


{{--////////////////////////////////////////////////////////////////////////////////--}}



{{--व्यहोरा change हुदा--}}
<script>
    $(document).ready(function () {
        $('#byahora').change(function () {

            let main_activity_id = $('#main_activity_name').val();
            let byahora = $('#byahora :selected').val();
            let DrOrCr = $('#DrOrCr').val();

            if (byahora == 4) {

                $('#party_type').prop('disabled', false);
                get_expense_head_by_activity_id(main_activity_id);

            } else if (byahora == 1) {

                get_expense_head_by_activity_id(main_activity_id);

            } else if (DrOrCr == 2 && byahora == 3) {


                $('#party_type').attr("disabled", "disabled");
                get_hisab_number_by_byahora(byahora);

            } else {

                $('#party_type').attr("disabled", "disabled");
                get_hisab_number_by_byahora(byahora);
            }
        })
    })

</script>
{{--Main कार्यक्रम change हुदा--}}
<script>
    $(document).ready(function () {

        $('#main_activity_name').change(function () {

            let main_activity_id = $('#main_activity_name').val();
            let byahora = $('#byahora').val();
            if (byahora != 1) {

                $("#byahora").val("1");
            }

            get_expense_head_by_activity_id(main_activity_id);
        })
    })

</script>
{{--hisab number change huda--}}
<script>
    let getDetailsByHisabNumber = function () {
        let bibran = $('#hisab_number option:selected').text();
        $('#details').val(bibran);
    }
    $(document).ready(function () {
        $('#hisab_number').change(function () {
            getDetailsByHisabNumber();
        })
    })
</script>


{{--    भुक्तानी थप हुदा --}}

<script>
        let getBhuktaniSn = function () {
            return $('#bhuktani tbody').find('tr').length;
        }
        let getBhktaniSerialNumberIf = function () {
            if (editingBhuktaniSN) {
                return editingBhuktaniSN;
            }
            return getBhuktaniSn();
        }

        let preBhuktani = [];
        $remaining = 0;
        $(document).ready(function () {
            $('#buktaniButton').click(function () {
                if(bhuktanivalidation()){
                    let budget_sub_head_id = $('select#program').val();
                    let nepali_date = $('input#date').val();
                    let nepali_date_in_english = convertNepaliToEnglish(nepali_date);
                    let main_activity_name = $('#bhuktani_main_activity option:selected').text();
                    let main_activity_id = $('select#bhuktani_main_activity').val();
                    let party_type_id = $('select#bhuktani_party_type').val();
                    let party_type_text = $('#bhuktani_party_type option:selected').text();
                    let bhuktani_party_text = $('#bhuktani_party option:selected').text();
                    let bhuktani_party_id = $('select#bhuktani_party').val();
                    let bhuktani_amount = $('input#bhuktani_amount').val();
                    let advance_tax_deduction_amount = $('input#advance_tax_deduction_amount').val();
                    let vat_amount = $('input#vat_amount').val();
                    let temp =  get_bhuktani_peski_type();
                    let peski_type = temp;
                    if(peski_type == 1){

                        peski_type = 1;
                    }else if (peski_type == '') {

                        peski_type = 0;
                    } else {
                        peski_type = 2;

                    }
                    if (bhuktani_total_payment_check()) {

                        var tr = "<tr class='" + getBhktaniSerialNumberIf() + "'>" +
                            "<td class='sn'>" + getBhktaniSerialNumberIf() +

                            "</td>" +

                            "<td class='bhuktani-main-activity' data-activity='"+ main_activity_id+"'>" +
                            main_activity_name +
                            "</td>" +

                            "<td class='bhuktani-party-type' data-party-type='"+party_type_id+"'>" +
                            party_type_text +
                            "</td>" +

                            "<td class='bhuktani_party' data-bhuktani-party='"+bhuktani_party_id+"'>" +
                            bhuktani_party_text +
                            "</td>" +

                            "<td class='bhuktani_amount' >" +
                            bhuktani_amount +
                            "</td>" +

                            "<td class='advance_tax_deduction' >" +
                            advance_tax_deduction_amount +
                            "</td>" +
                            "<td class='vat' >" +
                            vat_amount +
                            "</td>" +
                            "<td class='edit-voucher-td'>" +
                            '<a href="#" class="edit-bhuktani-row" data-sn="' + getBhktaniSerialNumberIf() + '">Edit</a> | <a href="#" class="delete-bhuktani">Delete</a>' +
                            "</td>";

                        if (editingBhuktaniSN) {
                            $(tr).css('background-color', '#fff');
                            $(tr).find('td.sn').text(editingBhuktaniSN);
                            $('#bhuktani').find('tr.' + editingBhuktaniSN).replaceWith(tr);

                            preBhuktaniEdit = [];
                            preBhuktaniEdit['budget_sub_head_id'] = budget_sub_head_id;
                            preBhuktaniEdit['main_activity_id'] = main_activity_id;
                            preBhuktaniEdit['party_type_id'] = party_type_id;
                            preBhuktaniEdit['bhuktani_party_id'] = bhuktani_party_id;
                            preBhuktaniEdit['bhuktani_amount'] = bhuktani_amount;
                            preBhuktaniEdit['date_nepali'] = nepali_date;
                            preBhuktaniEdit['date_english'] = nepali_date_in_english;
                            preBhuktaniEdit['advance_tax_deduction'] = advance_tax_deduction_amount;
                            preBhuktaniEdit['vat_amount'] = vat_amount;
                            preBhuktaniEdit['peski_type'] = peski_type;

                            preBhuktani[editingBhuktaniSN - 1] = preBhuktaniEdit;

                        } else {

                            preBhuktani.push({
                                budget_sub_head_id: budget_sub_head_id,
                                main_activity_id: main_activity_id,
                                party_type_id: party_type_id,
                                bhuktani_party_id: bhuktani_party_id,
                                bhuktani_amount: bhuktani_amount,
                                //source
                                //bhidhi
                                //prakar
                                //payee code number
                                date_nepali: nepali_date,
                                date_english: nepali_date_in_english,
                                advance_tax_deduction : advance_tax_deduction_amount,
                                vat_amount : vat_amount,
                                peski_type : peski_type
                            });
                            $('#bhuktani').find('tbody').last('tr').append(tr);
                        }
                        editingBhuktaniSN = 0;
                        bhuktani_total();
                        $('#bhuktani_amount').val(" ")
                        $('#advance_tax_deduction_amount').val(" ")
                        $('#vat_amount').val("")

                    } else {

                        alert("भुक्तानि रकम भन्दा बढी भयो");
                        $('#bhuktani_amount').val("");
                        $('#bhuktani_amount').focus();

                    }

                }
            })
        })
    </script>

{{--    भौचर थप गर्दा हुने  --}}
<script>

        let getSerialNumber = function () {
            return $('#voucher_table tbody').find('tr').length;
        }
        let getSerialNumberIf = function () {
            if (editingSN) {
                return editingSN;
            }
            return getSerialNumber();
        }
        let activityForBhuktani = [];
        let voucherDetails = [];
        let crBankAmount = 0;
        let Dr = 0;
        let Cr = 0;
        let paymentAmount = 0;
        $('#voucher_submit').click(function (e) {
            if (validation()) {
                e.preventDefault();
                let crAmount = 0;
                let drAmount = 0;
                let date = $('input#date').val();
                let budget_sub_head_text = $('#program option:selected').text();
                let budget_sub_head_id = $('select#program :selected').val();
                let activity_name = $('#main_activity_name option:selected').text();
                let activity_id = $('select#main_activity_name').val();
                let dr_or_cr_name = $('#DrOrCr option:selected').text();
                let drOrCr = $('select#DrOrCr').val();
                let byahora_text = $('#byahora option:selected').text();
                let ledger_type_id = $('select#byahora').val(); //व्यहोराको field
                let hisab_number = $('select#hisab_number').val();
                let details = $('input#details').val();
                let party_type_name = $('#party_type option:selected').text();
                let party_type_id = $('select#party_type').val();
                let party_name = $('#party-name option:selected').text();
                let party_id = $('#party-name').val();
                let amount = $('#amount').val();
                let nepali_date = $('#date').val();
                let nepali_date_in_english = convertNepaliToEnglish(nepali_date);
                let temp =  get_peski_type();

                let peski_type = temp;
                if(peski_type == 1){

                    peski_type = 1;
                }else if (peski_type == '') {

                    peski_type = 0;
                } else {

                    peski_type = 2;
                }


                if (drOrCr == '1') {

                    drAmount = $('#amount').val();
                }
                if (drOrCr == '2') {

                    crAmount = $('#amount').val();
                }

                // //भुक्तानी आदेश मा रकम बाडफाडलाई  validation गर्ने रकम , यो रकम भन्दा बढि भुक्तानि गर्न रोक्ने गरि।
                // if (drOrCr == '2' && ledger_type_id == 3) {
                //
                //     paymentAmount = parseFloat(paymentAmount) + parseFloat(amount);
                //
                // }

                //भुक्तानि मा कार्यक्रम देखाउन लाइ
                let obj = activityForBhuktani.find(o => o.activity_id === activity_id);
                if (!obj) {
                    activityForBhuktani.push({
                        activity_name: activity_name,
                        activity_id: activity_id,
                    })
                }

                // console.log(activityForBhuktani[0]);
                if (editingSN) {

                    editVoucherDetails = [];
                    editVoucherDetails['drOrCr'] = drOrCr;
                    editVoucherDetails['budget_sub_head'] = budget_sub_head_id;
                    editVoucherDetails['activity_id'] = activity_id;
                    editVoucherDetails['ledger_type_id'] = ledger_type_id;
                    editVoucherDetails['hisab_number'] = hisab_number;
                    editVoucherDetails['amount'] = amount;
                    editVoucherDetails['date_nepali'] = nepali_date;
                    editVoucherDetails['date_english'] = nepali_date_in_english;
                    editVoucherDetails['bibran'] = details;
                    editVoucherDetails['party_type'] = party_type;
                    editVoucherDetails['party_id'] = party_id;
                    editVoucherDetails['peski_type'] = peski_type;

                    voucherDetails[editingSN - 1] = editVoucherDetails;
                    // console.log(voucherDetails);

                } else {

                    voucherDetails.push({
                        drOrCr: drOrCr,
                        budget_sub_head: budget_sub_head_id,
                        activity_id: activity_id,
                        ledger_type_id: ledger_type_id,
                        hisab_number: hisab_number,
                        amount: amount,
                        date_nepali: nepali_date,
                        date_english: nepali_date_in_english,
                        bibran: details,
                        party_type: party_type,
                        party_id: party_id,
                        peski_type : peski_type
                    });
                }

                let tr = "<tr class='" + getSerialNumberIf() + "'>" +
                    "<td class='sn'>" + getSerialNumberIf() +
                    "</td>" +
                    "<td class='dr-cr' data-dr-cr='" + drOrCr + "'>" +
                    dr_or_cr_name +
                    "</td>" +

                    "<td class='activity' data-activity-id='" + activity_id + "'>" +
                    activity_name +
                    "</td>" +

                    "<td class='byahora' data-byahora_id='" + ledger_type_id + "'>" +
                    byahora_text +
                    "</td>" +

                    "<td class='hidden expense-head' data-expense-head_id='"+hisab_number+"'>" +
                    hisab_number +
                    "</td>" +

                    "<td class='details'>" +
                    details +
                    "</td>" +

                    "<td class='drAmount'>" +
                    drAmount +
                    "</td>" +

                    "<td class='crAmount'>" +
                    crAmount +
                    "</td>" +
                    "<td class='partyType' data-party-type-id='"+ party_type_id +"'>" +
                    party_type_name +
                    "</td>" +
                    "<td class='party' data-party-id='"+party_id+"'>" +
                    party_name +
                    "</td>" +

                    "<td class='edit-voucher-td'>" +
                    '<a href="#" class="edit-voucher" data-sn="' + getSerialNumberIf() + '">Edit</a> | <a href="#" class="delete-row">Delete</a>' +
                    "</td>";

                // $('#voucher_table').find('tbody').first('tr').append(tr);
                if (editingSN) {
                    $(tr).css('background-color', '#fff');
                    $(tr).find('td.sn').text(editingSN);
                    $('#voucher_table').find('tr.' + editingSN).replaceWith(tr);
                } else {

                    $("#voucher_table tr:last").after(tr);
                }
                editingSN = 0;
                dr_cr_total();

                //एक पटक थप गरेपछि बजेट उपशि्रषक disabled हुने गरि
                $('#program').prop("disabled", 'disabled');

            } else {
                // alert('validation failed');
            }

            //भौचर थप गरेपछि माथिको फिल्ड खालि गरेको ।
            clearVoucherField();
            console.log(voucherDetails);
        })
    </script>

{{--  {{--prebhuktani ma peski prakar save huna lai--}}

<script>
    let bhuktani_peski_type=0;
    let get_bhuktani_peski_type = function(){
        let bhuktani_party_type = $('select#bhuktani_party_type').val();

        if(bhuktani_party_type == 5){

            return bhuktani_peski_type = 1;
        }
        else if(bhuktani_party_type == ''){

            return bhuktani_peski_type = 0;

        } else {
            return  bhuktani_peski_type = 2;

        }
    };
    $(document).ready(function () {
        $('#bhuktani_party_type').change(function () {

            get_bhuktani_peski_type();
        })
    })
</script>

{{--//थप गरेको भुक्तानि एडिट गर्न--}}
<script>

        let editingBhuktaniSN;
        $(document).on('click', '.edit-bhuktani-row', function () {
            editingBhuktaniSN = parseInt($(this).attr('data-sn'));
            // console.log(editingBhuktaniSN);
            $(this).parents('tbody').find('tr').css('background-color', '#FFF');
            $(this).parents('tr').css('background-color', '#FFF1CC');
            let bhuktaniTds = $(this).parents('tr').find('td');
            let item = [];
            $.each(bhuktaniTds, function () {
                let className = $(this).prop('class');
                if(className == 'bhuktani-main-activity') {
                    item['bhuktani-main-activity'] = $(this).html();
                }
                if(className == 'bhuktani-party-type') {

                    item['bhuktani-party-type'] = $(this).html();
                    // alert(item['bhuktani-party-type']);

                }
                if(className == 'bhuktani_party') {

                    item['bhuktani_party'] = $(this).html();
                    // alert(item['bhuktani_party']);
                }
                if(className == 'bhuktani_amount') {

                    item['bhuktani_amount'] = $(this).html();
                }

                if(className == 'advance_tax_deduction') {

                    item['advance_tax_deduction'] = $(this).html();
                }
                if(className == 'vat') {

                    item['vat'] = $(this).html();
                }
            })

            let activityOption = $('#bhuktani_main_activity').find('option');
            $.each(activityOption, function () {

                if($.trim($(this).text())== $.trim(item['bhuktani-main-activity'])) {

                    $(this).prop('selected', true);
                }
            });

            let partyTypeOption = $('#bhuktani_party_type').find('option');
            $.each(partyTypeOption, function () {

                if($.trim($(this).text()) === $.trim(item['bhuktani-party-type'])) {
                        // alert("true party type");
                    $(this).prop('selected', true);
                    getBhuktaniPartyByPartyType($(this).val(), item['bhuktani_party']);

                }
            })
            $('#bhuktani_amount').val(item['bhuktani_amount'])
            $('#advance_tax_deduction_amount').val(item['advance_tax_deduction'])
            $('#vat_amount').val(item['vat'])
            // $(e.target).parents('tr').remove();
            return false;

        })
    </script>



{{--   थप गरेको भौचर एडिट गर्न--}}
<script>
        let editingSN;
        $(document).on('click', '.edit-voucher', function () {
            editingSN = parseInt($(this).attr('data-sn'));
            //click गरेको row को tds तानेको।
            let tds = $(this).parents('tr').find('td');
            $(this).parents('tbody').find('tr').css('background-color', '#FFF');
            $(this).parents('tr').css('background-color', '#FFF1CC');
            let item = [];
            let program_id = $('select#program').val();
            $.each(tds, function () {

                let className = $(this).prop('class');
                if (className == 'dr-cr') {
                    item['dr-cr'] = $(this).html();
                    item['dr-cr-id'] = $(this).attr('data-dr-cr');
                }
                if (className == 'activity') {

                    item['activity'] = $(this).html();
                    item['activity_id'] = $(this).attr('data-activity-id');

                    // बजेट खर्च वा पेश्कि छ भने

                }

                if (className == 'byahora') {

                    item['byahora'] = $(this).html();
                    item['byahora_id'] = $(this).attr('data-byahora_id');

                    // व्यहोरामा दायित्व र एकल कोष खाता छ भने।
                    if (item['byahora_id'] == 2 || item['byahora_id'] == 3) {
                        // alert(item['byahora_id']);
                        get_hisab_number_by_byahora(item['byahora_id']);

                    }
                    if (item['byahora_id'] == 1 || item['byahora_id'] == 4) {

                        get_expense_head_by_activity_id(item['activity_id']);

                    }
                }

                if (className == 'details') {
                    item['details'] = $(this).html();
                }

                if (className == 'crAmount' || className == 'drAmount') {
                    let test = parseInt($(this).html());
                    if (test > 0) {
                        item['amount'] = $(this).html();
                    }
                }

                if (className == 'partyType') {

                    item['partyType'] = $(this).html()

                    $('#party_type').prop("disabled", false);
                }
                if (className == 'party') {

                    item['party'] = $(this).html();

                }

            })
// console.log(item);

            let drOrCrOption = $('#DrOrCr').find('option');
            $.each(drOrCrOption, function () {
                // alert($(this).val());
                if ($(this).val() == (item['dr-cr-id'])) {
                    $(this).prop('selected', true);
                }
            })
            $budget_sub_head = $('#program :selected').val();
            // $('#main_activity_name').val(item['activity']);

            let activity_ption = $('#main_activity_name').find('option');
            $.each(activity_ption, function () {
                // console.log(this.text,item['activity'],$.trim(this.text) == $.trim(item['activity']));
                if($.trim(this.text) == $.trim(item['activity'])){
                    // alert("true");
                    $(this).prop('selected',true);
                }
            });
            //व्यहोराको option लेको।
            let byahoraOption = $('#byahora').find('option');
            $.each(byahoraOption, function () {

                if ($.trim(this.text) == $.trim(item['byahora'])) {

                    $(this).prop('selected', true);
                    let byahora = $('select#byahora').val();
                }

            });

            //कार्यक्रमको option लेको।
            let activityOption = $('#main_activity_name').find('option');
            $.each(activityOption, function () {

                if ($.trim(this.text) == $.trim(item['activity'])) {
                    $(this).prop('selected', true);
                }
            })


            let partyTypeOption = $('#party_type').find('option');
            $.each(partyTypeOption, function () {

                if ($(this).text() == item['partyType']) {

                    $(this).prop('selected', true);
                    getPartyByPartyType($(this).val(), item['party']);

                }
            })
            $('#details').val(item['details']);
            $('#amount').val(item['amount']);
            // $(e.target).parents('tr').remove();
            dr_cr_total();
            dr_cr_equal_check();
            return false;

        });
    </script>



{{--थप गरेको भौचर delete  गर्ने--}}
<script>
        let resetSn = function (tabelId) {
            alert("are you sure. It will delete the item");
            let $trs = $('#'+tabelId+' tbody').find('tr');
            $.each($trs, function (key, value) {
                console.log(key, value);
                $(value).removeClass();
                $(value).find('td.sn').html(key);
                $(value).find('td.edit-voucher-td').find('a').first().attr('data-sn', key);
                $(value).attr('class', key);
            });
        };

        $(document).on('click', '.delete-row', function () {

            $(this).closest('tr').remove();
            resetSn('voucher_table');
            dr_cr_total();
            return false;
        })

    </script>


{{--थप गरेको भुक्तानि Delete गर्न--}}
<script>
        $(document).on('click', '.delete-bhuktani', function () {
            $(this).closest('tr').remove();
            resetSn('bhuktani');
            bhuktani_total();
            return false;

        })
    </script>

{{--Function start--}}


{{--dr_cr_equal_check finction--}}
<script>
        let dr_cr_equal_check = function () {
            $('#VoucherUpdate').prop('disabled', false);

            let option = '<option selected>...................</option>';
            $.each(activityForBhuktani, function () {
                option += '<option value="' + this.activity_id + '">' + this.activity_name + '</option>'
                $('#bhuktani_main_activity').html(option);
            })

        }
</script>



{{--    क्रे र एकल कोष खाता amount total गर्ने--}}

<script>
        let get_cr_tsa = function(){
            let cr_tsa =0;
            let trs = $('#voucher_table tbody').find('tr').not(':first');
            $.each(trs, function (key, tr) {
                // alert($(tr).find('td.dr-cr').text());
                if($(tr).find('td.dr-cr').attr('data-dr-cr')==2  && $(tr).find('td.byahora').attr('data-byahora_id')== 3)  {
                     cr_tsa = parseFloat($(tr).find('td.crAmount').text())
                    // console.log(cr_tsa);
                }
            });
            // alert(cr_tsa);
            return cr_tsa;

        }
    </script>

{{--    page edit भएर आउदा  dr_cr_total function लाइ document ready हुदा बोलाको किनकि भौचर save page मा भौचर थप हुदा मात्र यो function call गरेको छ--}}
<script>
    $(document).ready(function () {

        dr_cr_total();
    })
</script>

{{-- Dr and Cr total देखाउने function --}}
<script>
        let dr_cr_total = function () {
            let trs = $('#voucher_table').find('tr');
            let DrAmount = 0;
            let CrAmount = 0;
            $.each(trs, function (key, tr) {
                $.each($(tr).find('td'), function (key_, td) {
                    if ($(td).prop('class') == 'drAmount') {
                        let newDr = $(this).text();
                        if (newDr) {
                            DrAmount += parseFloat(newDr);
                            $('#dr_amount').html(DrAmount);

                        }
                    }
                    if ($(td).prop('class') == 'crAmount') {
                        let newCr = $(this).text();
                        if (newCr) {

                            CrAmount += parseFloat(newCr);
                            $('#dr_amount').html(CrAmount);

                        }
                    }

                })
                Cr_Cr_remain = parseFloat(DrAmount) - parseFloat(CrAmount)
                if (CrAmount == DrAmount) {

                    if (CrAmount == 0 || DrAmount == 0) {

                        $('#VoucherUpdate').prop('disabled', "disabled");
                    } else {

                        $('#VoucherUpdate').prop('disabled', false);

                        let option = '<option selected>...................</option>';
                        $.each(activityForBhuktani, function () {
                            option += '<option value="' + this.activity_id + '">' + this.activity_name + '</option>'
                            $('#bhuktani_main_activity').html(option);
                        })
                    }
                } else {
                    $('#VoucherUpdate').prop('disabled', "disabled");
                }
            });
            $('#dr_amount').text(DrAmount);
            $('#cr_amount').text(CrAmount);
            $('#amount_difference').text(Cr_Cr_remain);
        }

    </script>


{{--Bhuktani amount total calculation--}}
<script>
        let bhuktani_total = function () {

            let trs = $('#bhuktani').find('tr');
            let bhuktaniAmount = 0;
            $.each(trs, function (key, tr) {
                $.each($(tr).find('td'), function (key_, td) {

                    if ($(td).prop('class') == 'bhuktani_amount') {

                        let newAmount = $(this).text();
                        if (newAmount) {
                            bhuktaniAmount += parseFloat(newAmount);

                        }
                    }

                })

            });
            $('#bhuktaniTotal').text(bhuktaniAmount);
        }
    </script>


{{--भौचर update हुने--}}

<script>
        function voucherUpdate() {

            let voucher_id = '{{$voucher->id}}';
            let token = '{{csrf_token()}}';
            let nepali_date = $('#date').val();
            let nepali_date_in_english = convertNepaliToEnglish(nepali_date);
            let budget_sub_head_id = $('#program :selected').val();
            let voucher_number = $('#program').val();
            let narration_short = $('#shortInfo').val();
            let narration_long = $('#detailsInfo').val();
            let tsa_amount = get_cr_tsa();
            let payment_amount = tsa_amount;

            let voucher = [];
            voucher = {
                id : voucher_id,
                voucher_number: voucher_number,
                budget_sub_head_id: budget_sub_head_id,
                payment_amount: payment_amount,
                narration_short: narration_short,
                narration_long: narration_long,
                date_nepali: nepali_date,
                date_english: nepali_date_in_english,
            };

            //Get all VOucher Detials
            let trs = $('#voucher_table tbody').find('tr').not(':first');
            $.each(trs, function () {
                let drOrCr = $(this).find('td.dr-cr').data('dr-cr');
                let budget_sub_head = budget_sub_head_id;
                let activity_id = $(this).find('td.activity').data('id');
                let ledger_type_id = $(this).find('td.byahora').data('byahora');
                let hisab_number = $(this).find('td.expense_head').attr('data-expense-head_id');
                let bibran = $(this).find('td.details').text();
                let dr_amount = $(this).find('td.drAmount').text();
                let cr_amount = $(this).find('td.crAmount').text();
                let amount = 0;
                if(dr_amount > cr_amount){
                    amount = dr_amount;
                } else {

                    amount = cr_amount;

                }
                let party_type = $(this).find('td.partyType').attr('data-party-type-id');
                let party_id = $(this).find('td.party').attr('party-id');
                let peski_type = $(this).find('td.peski_type').attr('data-peski-type');
                let date_nepali = nepali_date;
                let date_english = nepali_date_in_english;
                voucherDetails.push({

                    voucher_id : voucher_id,
                    drOrCr: drOrCr,
                    budget_sub_head: budget_sub_head,
                    activity_id: activity_id,
                    ledger_type_id: ledger_type_id,
                    hisab_number: hisab_number,
                    amount: amount,
                    date_nepali: nepali_date,
                    date_english: nepali_date_in_english,
                    bibran: bibran,
                    party_type :party_type,
                    party_id: party_id,
                    peski_type: peski_type,
                });
            });

            //Get all Bhuktani for update
            let bhuktani_trs = $('#bhuktani tbody').find('tr').not(':first');
            $.each(bhuktani_trs, function () {
                let budget_sub_head_id = $('#program').val();
                let main_activity_id = $(this).find('td.bhuktani-main-activity').attr('data-activity');
                let bhuktani_party_type = $(this).find('td.bhuktani-party-type').attr('data-party-type');
                let bhuktani_party_id = $(this).find('td.bhuktani_party').attr('data-bhuktani-party');
                let bhuktani_amount = $(this).find('td.bhuktani_amount').text();
                let advance_tax_deduction = $(this).find('td.advance_tax_deduction').text();
                let vat_amount = $(this).find('td.vat').text();
                let peski_type = $(this).find('td.peski_type').attr('data-peski-type');
                preBhuktani.push({
                    voucher_id : voucher_id,
                    budget_sub_head_id: budget_sub_head_id,
                    main_activity_id: main_activity_id,
                    bhuktani_party_type : bhuktani_party_type,
                    bhuktani_party_id: bhuktani_party_id,
                    bhuktani_amount: bhuktani_amount,
                    // source :[
                    // bhidhi :
                    // prakar :
                    // payee code number  :
                    date_nepali: nepali_date,
                    date_english: nepali_date_in_english,
                    advance_tax_deduction : advance_tax_deduction,
                    vat_amount : vat_amount,
                    peski_type: peski_type
                })
            });
        console.log(voucherDetails);

            $.ajax({
                method: 'post',
                url: '{{route('voucher.update')}}',
                data: {
                    voucher_id : voucher_id,
                    voucher: voucher,
                    voucherDetails: voucherDetails,
                    preBhuktani: preBhuktani,
                    _token: token,
                },
                success: function (resp) {
                    // console.log(resp);
                    // if(resp){
                        swal({
                            title: "Good job!",
                            text: "You clicked the button!",
                            icon: "success",
                        });

                    // } else {
                    //     alert("failed");
                    //
                    // }
                }
            })

        }

    </script>
{{--    voucher details ma peski prakar save huna lai--}}
<script>
    let peski_type=0;
    let get_peski_type = function(){
        let party_type = $('select#party_type').val();

        if(party_type == 5){

            return peski_type = 1;
        }
        else if(party_type == ''){

            return 0;
        } else {
            return  peski_type = 2;

        }
    };
    $(document).ready(function () {
        $('#party_type').change(function () {
            get_peski_type();
        })
    })
</script>

{{--on change party_type  --}}

<script>
        $(document).ready(function () {
            $('#party_type').change(function () {

                let party_type_id = $('#party_type :selected').val();
                if(party_type_id == 5){

                    getKarmachari();
                } else {
                    getPartyByPartyType(party_type_id);
                }
            })
        })
        let getKarmachari = function () {

            let url = '{{route('get.karmachari')}}';
            $.ajax({

                url : url,
                method : 'get',
                success : function(res){
                    // console.log($.parseJSON(res));
                    let option = '<option selected>....................</option>';
                    $.each($.parseJSON(res), function () {
                        option += '<option value="' + this.id + '">' + this.name_nepali + '</option>'

                    });
                    $('#party-name').html(option);
                }
            });
        }
    </script>


{{--on change party_type for bhuktani ---below voucher  --}}

<script>
        $(document).ready(function () {
            $('#bhuktani_party_type').change(function () {

                let party_type_id = $('#bhuktani_party_type :selected').val();
                if(party_type_id == 5){
                    // alert("test");
                    getKarmachariForBhuktani();
                } else {
                    getBhuktaniPartyByPartyType(party_type_id);
                }

            })
        })
        let getKarmachariForBhuktani = function () {

            let url = '{{route('get.karmachari')}}';
            $.ajax({
                url : url,
                method : 'get',
                success : function(res){
                    // console.log($.parseJSON(res));
                    let option = '<option selected>....................</option>';
                    $.each($.parseJSON(res), function () {

                        option += '<option value="' + this.id + '">' + this.name_nepali + '</option>'

                    });
                    $('#bhuktani_party').html(option);
                }
            });
        }
    </script>




    {{--व्यहोरा change हुदा--}}

    <script>
        $(document).ready(function () {
            $('#byahora').change(function () {

                let main_activity_id = $('#main_activity_name').val();
                let byahora = $('#byahora :selected').val();
                let DrOrCr = $('#DrOrCr').val();

                if (byahora == 4) {

                    $('#party_type').prop('disabled', false);
                    get_expense_head_by_activity_id(main_activity_id);

                } else if (byahora == 1) {

                    get_expense_head_by_activity_id(main_activity_id);

                } else if (DrOrCr == 2 && byahora == 3) {


                    $('#party_type').attr("disabled", "disabled");
                    get_hisab_number_by_byahora(byahora);

                } else {

                    $('#party_type').attr("disabled", "disabled");
                    get_hisab_number_by_byahora(byahora);
                }
            })
        })

    </script>


    {{--Main कार्यक्रम change हुदा--}}
    <script>
        $(document).ready(function () {

            $('#main_activity_name').change(function () {

                let main_activity_id = $('#main_activity_name').val();
                let byahora = $('#byahora').val();
                if (byahora != 1) {

                    $("#byahora").val("1");
                }

                get_expense_head_by_activity_id(main_activity_id);
            })
        })

    </script>


    {{--hisab number change huda--}}
    <script>
        let getDetailsByHisabNumber = function () {
            let bibran = $('#hisab_number option:selected').text();
            $('#details').val(bibran);
        }
        $(document).ready(function () {
            $('#hisab_number').change(function () {
                getDetailsByHisabNumber();
            })
        })
    </script>


    {{--    function start.... सबै function नहुन पनि सक्छ। Manage गर्नु पर्ने छ--}}

    {{--Function for tabke total in bhuktani table and compare with cr. bank value--}}
    <script>
        let bhuktani_total_payment_check = function () {

            let current_payment = parseFloat($('#bhuktani_amount').val());
            let trs = $('#bhuktani').find('tr');

            let bhuktaniAmount = 0;
            $.each(trs, function (key, tr) {
                $.each($(tr).find('td'), function (key_, td) {

                    if($(td).prop('class') == 'bhuktani_amount') {

                        let newAmount = $(this).text();
                        if (newAmount) {
                            bhuktaniAmount += parseFloat(newAmount);
                        }
                    }
                })
            });
            let x;
            let previous_value=0;
            if(editingBhuktaniSN) {
                previous_value = $('table#bhuktani').find('tr'+'.'+editingBhuktaniSN).find('td.bhuktani_amount').text();
                x = parseFloat(bhuktaniAmount) + parseFloat(current_payment) - previous_value;
            }else {
                x = parseFloat(bhuktaniAmount) + parseFloat(current_payment);
            }
            let tsa_amount = get_cr_tsa();
            // alert(tsa_amount);
            return x <= parseFloat(tsa_amount);

        }
    </script>

    {{--function of Change on party Type for Bhuktani--}}
    <script>
        let getBhuktaniPartyByPartyType = function (party_type_id, party_name = '') {
            // alert(party_type_id);
            if(party_type_id == 5){

                getKarmachariForBhuktani();

            } else {

                let url = '{{route('get_party_name_by_party_type_and_office', 123)}}';
                url = url.replace(123, party_type_id);
                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {
                        // console.log($.parseJSON(res));
                        let option = '<option selected>....................</option>';
                        $.each($.parseJSON(res), function () {
                            // console.log(party_name,this.name_nep);
                            if (party_name == this.name_nep) {
                                option += '<option value="' + this.id + '" selected>' + this.name_nep + '</option>'

                            } else {
                                option += '<option value="' + this.id + '">' + this.name_nep + '</option>'

                            }

                        });

                        $('#bhuktani_party').html(option);

                    }
                })

            }
        }

     getKarmachariForBhuktani = function () {

        let url = '{{route('get.karmachari')}}';
        $.ajax({
            url : url,
            method : 'get',
            success : function(res){
                // console.log($.parseJSON(res));
                let option = '<option selected>....................</option>';
                $.each($.parseJSON(res), function () {

                    option += '<option value="' + this.id + '" selected>' + this.name_nepali + '</option>'

                });
                $('#bhuktani_party').html(option);
            }
        });
    }
    </script>

    {{--get  party name for voucher--}}
    <script>
        let getPartyByPartyType = function (party_type_id, party_name = '') {

            let url = '{{route('get_party_name_by_party_type_and_office', 123)}}';
            url = url.replace(123, party_type_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    // console.log($.parseJSON(res));
                    let option = '<option selected>....................</option>';
                    $.each($.parseJSON(res), function () {
                        // console.log(party_name,this.name_nep);
                        if (party_name == this.name_nep) {
                            option += '<option value="' + this.id + '" selected>' + this.name_nep + '</option>'

                        } else {
                            option += '<option value="' + this.id + '">' + this.name_nep + '</option>'

                        }
                    })

                    $('#party-name').html(option);

                }
            })
        }
    </script>


    {{--budget sub head change huda function--}}
    <script>
        let get_main_activity_by_budget_sub_head = function (program_code, activity_id = '') {
            let url = '{{route('get_main_activity_by_budget_sub_head',123)}}';
            url = url.replace(123, program_code);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let option = '<option selected>.......</option>';
                    $.each($.parseJSON(res), function () {
                        if (this.id == activity_id) {
                            option += '<option value="' + this.id + '" selected>' + this.activity + '</option>'
                            get_expense_head_by_activity_id(this.id);
                        } else {
                            option += '<option value="' + this.id + '">' + this.activity + '</option>'
                        }
                        $('#main_activity_name').html(option);
                    })

                }
            })

        }
    </script>

    {{--    expense head function--}}
    <script>
        let get_expense_head_by_activity_id = function (main_activity_id) {
            // alert(main_activity_id);
            let url = '{{route('get_expense_head_by_activity_id',123)}}'
            url = url.replace(123, main_activity_id);
            // console.log(url);
            $.ajax({

                url: url,
                method: 'get',
                success: function (res) {
                    let data = $.parseJSON(res);
                    // console.log(data);
                    let options = '<option selected>.........................</option>';

                    options += '<option value="' + data.id + '" selected>' + data.expense_head_code + '|' + data.expense_head_sirsak + '</option>'

                    $('#hisab_number').html(options);
                    $('#details').val(data.expense_head_sirsak);

                }
            })
        }
    </script>


    {{--Get हिसाब Number By व्यहोरा--}}
    <script>
        let get_hisab_number_by_byahora = function (byahora) {

            $('#hisab_number').val('');
            let ledger_type = byahora;

//बजेट खर्च र पेश्कि हुदा
            let expense_head =  $('tr.'+editingSN).find('td.expense_head').attr('data-expense-head_id');

            if(ledger_type == 1 || ledger_type == 4) {
                // alert(ledger_type);
                let main_activity_id = $('#main_activity_name').val();
                get_expense_head_by_activity_id(main_activity_id);
            }else {

                let url = '{{route('get_expense_head__by_ledger_type',123)}}';
                url = url.replace(123, ledger_type);

                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {
                        // console.log($.parseJSON(res));
                        let option = '<option value="0">.........</option>';
                        let hisab_number = $.parseJSON(res);
                        console.log(hisab_number);
                        if (hisab_number.length > 0) {
                            $.each($.parseJSON(res), function () {
                                if (expense_head == this.id){

                                    option += '<option value="' + this.id + '" selected>' + this.expense_head_sirsak + '</option>'
                                } else {
                                    option += '<option value="' + this.id + '">' + this.expense_head_sirsak + '</option>'

                                }

                            })
                        } else {
                            option += '<option value="' + hisab_number[0].id + '" selected>' + hisab_number[0].expense_head_sirsak + '</option>'

                        }

                        $('#hisab_number').html(option);
                        getDetailsByHisabNumber();

                    }
                })
            }


        }
    </script>



    {{--short narration key up --}}
    <script>
        $(document).ready(function () {
            $('#shortInfo').keyup(function () {
                let shortNarration = $('#shortInfo').val();
                $('#detailsInfo').val(shortNarration);
            })
        })
    </script>


    {{--Dr  Or Cr change    हुदा--}}
    <script>
        $(document).ready(function () {
            $('#DrOrCr').change(function () {
                // $('#byahora').val("");
                // $('#hisab_number').val("");
                // $('#details').val("");
            })
        })
    </script>



{{-- Fixed --}}

    {{-- Validation   --}}
    <script>
        function validation() {
            let flag = 1;
            // program name validation
            let programId = $('#program');
            if (!programId.val()) {
                if (programId.siblings('p').length == 0) {
                    programId.after('<p style="color:red">छान्नुहोस!</p>')
                }
                programId.focus();
                flag = 0;
            } else {
                programId.siblings('p').remove()
            }
            // program name validation end

            //main_activity_name validation
            let mainactivitynameId = $('#main_activity_name');
            let DrOrCr = $('#DrOrCr').val();
            let ledgerType = $('#byahora').val();
            if (DrOrCr == 2 && ledgerType == 3) {
                mainactivitynameId.siblings('p').remove()
            } else {
                if (!mainactivitynameId.val()) {
                    if (mainactivitynameId.siblings('p').length == 0) {
                        mainactivitynameId.after('<p style="color:red">छान्नुहोस!</p>')
                    }
                    mainactivitynameId.focus();
                    flag = 0;
                } else {
                    mainactivitynameId.siblings('p').remove()
                }
            }

            let partyTypeId = $('#party_type');
            if (ledgerType == 4) {

                // alert(partyTypeId.val());
                if (!partyTypeId.val()) {
                    if (partyTypeId.siblings('p').length == 0) {

                        partyTypeId.after('<p style="color:red">छान्नुहोस!</p>')
                    }
                    partyTypeId.focus();
                    flag = 0;
                } else {
                    partyTypeId.siblings('p').remove()

                }
            }

            let partyName = $('#party_type');
            if (ledgerType == 4) {

                // alert(partyName.val());
                if (!partyName.val()) {
                    if (partyName.siblings('p').length == 0) {

                        partyName.after('<p style="color:red">छान्नुहोस!</p>')
                    }
                    partyName.focus();
                    flag = 0;
                } else {
                    partyName.siblings('p').remove()

                }
            }


            //main_activity_name validation end
            //hisab number start
            let hisab_number = $('#hisab_number');
            if (!hisab_number.val()) {
                if (hisab_number.siblings('p').length == 0) {
                    hisab_number.after('<p style="color:red">छान्नुहोस!</p>')
                }
                hisab_number.focus();
                flag = 0;
            } else {
                hisab_number.siblings('p').remove()
            }
            //details validation

            let detailsId = $('#details');
            if (!detailsId.val()) {
                if (detailsId.siblings('p').length == 0) {
                    detailsId.after('<p style="color:red">लेख्नुहोस​!</p>')
                }
                detailsId.focus();
                flag = 0;
            } else {
                detailsId.siblings('p').remove()
            }

            //details validation end


            //amount validation

            let amountsId = $('#amount');

            if ((!amountsId.val() || parseInt(amountsId.val()) < 1) && amountsId.length > 0) {
                if (amountsId.siblings('p').length == 0) {
                    amountsId.after('<p style="color:red">लेख्नुहोस​!</p>')
                }
                amountsId.focus();
                flag = 0;
            } else {
                amountsId.siblings('p').remove()
            }
            //amount validation end
            return flag;



        }

        function bhuktanivalidation() {
            let flag = 1;

            let mainActivityId = $('#bhuktani_main_activity');
            if (!mainActivityId.val()) {
                if (mainActivityId.siblings('p').length == 0) {
                    mainActivityId.after('<p style="color:red">छान्नुहोस!</p>')
                }
                mainActivityId.focus();
                flag = 0;
            } else {
                mainActivityId.siblings('p').remove()
            }

            let partyTypeId = $('#bhuktani_party_type');
            if (!partyTypeId.val()) {
                if (partyTypeId.siblings('p').length == 0) {
                    partyTypeId.after('<p style="color:red">छान्नुहोस!</p>')
                }
                partyTypeId.focus();
                flag = 0;
            } else {
                partyTypeId.siblings('p').remove()
            }

            let partyId = $('#bhuktani_party');
            if (!partyId.val()) {
                if (partyId.siblings('p').length == 0) {
                    partyId.after('<p style="color:red">छान्नुहोस!</p>')
                }
                partyId.focus();
                flag = 0;
            } else {
                partyId.siblings('p').remove()
            }

            let bhuktaniAmount = $('#bhuktani_amount');
            if (!bhuktaniAmount.val()) {
                if (bhuktaniAmount.siblings('p').length == 0) {
                    bhuktaniAmount.after('<p style="color:red">छान्नुहोस!</p>')
                }
                bhuktaniAmount.focus();
                flag = 0;
            } else {
                bhuktaniAmount.siblings('p').remove()
            }

            let advanceTaxDeduction = $('#advance_tax_deduction_amount');
            if (!advanceTaxDeduction.val()) {
                if (advanceTaxDeduction.siblings('p').length == 0) {
                    advanceTaxDeduction.after('<p style="color:red">छान्नुहोस!</p>')
                }
                advanceTaxDeduction.focus();
                flag = 0;
            } else {
                advanceTaxDeduction.siblings('p').remove()
            }

            let vatAmount = $('#vat_amount');
            if (!vatAmount.val()) {
                if (vatAmount.siblings('p').length == 0) {
                    vatAmount.after('<p style="color:red">छान्नुहोस!</p>')
                }
                vatAmount.focus();
                flag = 0;
            } else {
                vatAmount.siblings('p').remove()
            }

            return flag;

        }


    </script>

    {{--Date Picker --}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);
    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            // console.log(input);
            var charArray = input.split('');
            //console.log(charArray);
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1'
                        break
                    case '२':
                        engDate += '2'
                        break
                    case '३':
                        engDate += '3'
                        break
                    case '४':
                        engDate += '4'
                        break
                    case '५':
                        engDate += '5'
                        break
                    case '६':
                        engDate += '6'
                        break
                    case '०':
                        engDate += '0'
                        break
                    case '७':
                        engDate += '7'
                        break
                    case '८':
                        engDate += '8'
                        break
                    case '९':
                        engDate += '9'
                        break

                    case '-':
                        engDate += '-'
                        break
                }
                //console.log(engDate)
            })
            return engDate

        }
    </script>

    {{--Clear Voucher field when  Add button click--}}
    <script>
        function clearVoucherField() {

            // $('#main_activity_name').val('')
            $('#hisab_number').val('')
            $('#details').val('')
            $('#party_type').val('')
            $('#party-name').val('')
            $('#amount').val('')

        }
    </script>
@endsection
