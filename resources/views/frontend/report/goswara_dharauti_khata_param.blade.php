@extends('frontend.layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> म.ले.प.फा.नं. ६०२::गोश्वारा धरौटी खाता </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body p-15">
                <form action="{{route('report.goswara.dharauti.khata')}}" method="post" target="_blank">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="row">

                            <div class="col-md-3 col-md-offset-3">
                                <label for="office">कार्यालय : </label>
                                <select class="form-control" name="office">
                                    <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}</option>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label for="name">आर्थिक वर्ष:</label>
                                <select class="form-control" name="fiscal_year">
                                    <option value="{{$fiscalYear->id}}">{{$fiscalYear->year}}</option>
                                </select>
                            </div>

                            <div class="col-md-1">
                                <label for="">&nbsp;</label>
                                <button type="submit" class="btn btn-primary btn-block">Submit</button>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

@endsection

@section('scripts')

<script>
    $(document).on('change','#program',function () {
            let option = '<option selected>.......</option>'
            $('#khata-prakar').val(option);
            $('#khata').val(option);
        })
</script>

{{--प्रकार change हुदा--}}

<script>
    $(document).ready(function () {
            $('#khata-prakar').change(function () {
                let fiscal_year = $('#fiscal_year').val();
                let budget_sub_head_id = $('#program').val();
                let ledger_type_id = $('#khata-prakar :selected').val();
                let url = "{{route('get_khata_by_khata_prakar',['123','345','456'])}}";

                url = url.replace('123', fiscal_year);
                url = url.replace('345', budget_sub_head_id);
                url = url.replace('456', ledger_type_id);
                $.ajax({

                    url: url,
                    method: 'get',
                    data: {

                        'ledger_type_id': ledger_type_id,
                        'budget_sub_head_id': budget_sub_head_id
                    },
                    success: function (res) {
                        let data = $.parseJSON(res);

                        console.log(data.length);

                        let option = '<option value="" selected>..........</option>';
                        if(data.length > 0){

                            $.each(data, function () {
                                console.log(this);
                                option += '<option value="' + this.id + '">' ;
                                if(this.code)
                                    option +=  this.code +' | ';
                                option += this.sirsak + '</option>';

                                $('#khata').html(option);
                            })
                        } else {

                            $('#khata').html(option);
                        }


                    }
                })
            })
        })
</script>
@endsection