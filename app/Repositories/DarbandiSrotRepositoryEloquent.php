<?php
namespace App\Repositories;

use App\Models\DarbandiSrot;
use Illuminate\Support\Facades\Auth;

class DarbandiSrotRepositoryEloquent implements DarbandiSrotRepository
{
    public function create($attributes) {
        $darbandisrot = new DarbandiSrot();
        $darbandisrot->name = $attributes['name'];
        $darbandisrot->status = 1;
        $darbandisrot->save();
        return $darbandisrot;
    }

    public function get_all_darbandisrots()
    {
        return DarbandiSrot::all();
    }

}