<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FiscalYearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fiscal_years')->insert([
            [
                "year" => "2074/75",
                "code" => "74/75",
                "start_date_ad" => "2017-07-16",
                "end_date_ad" => "2018-07-16",
                "start_date_bs" => "2074-4-1",
                "end_date_bs" => "2075-3-31",
                "status" => 0,
                "user" => 1,
                "created_at" => "2019-05-21T09:08:14.701Z",
                "updated_at" => "2019-05-21T09:08:14.701Z"
            ],
            [
                "year" => "2075/76",
                "code" => "75/76",
                "start_date_ad" => "2018-07-17",
                "end_date_ad" => "2019-07-16",
                "start_date_bs" => "2075-4-1",
                "end_date_bs" => "2076-3-31",
                "status" => 1,
                "user" => 1,
                "created_at" => "2019-05-21T09:07:24.610Z",
                "updated_at" => "2019-05-21T09:07:24.610Z"
            ],
            [
                "year" => "2076/77",
                "code" => "76/77",
                "start_date_ad" => "2019-07-17",
                "end_date_ad" => "2020-07-15",
                "start_date_bs" => "2076-4-1",
                "end_date_bs" => "2077-3-31",
                "status" => 0,
                "user" => 1,
                "created_at" => "2019-05-21T09:07:24.610Z",
                "updated_at" => "2019-05-21T09:07:24.610Z"
            ],
            [
                "year" => "2077/78",
                "code" => "77/78",
                "start_date_ad" => "2020-07-16",
                "end_date_ad" => "2021-07-15",
                "start_date_bs" => "2077-4-1",
                "end_date_bs" => "2078-3-31",
                "status" => 0,
                "user" => 1,
                "created_at" => "2019-05-21T09:07:24.610Z",
                "updated_at" => "2019-05-21T09:07:24.610Z"
            ],
            [

                "year" => "2078/79",
                "code" => "78/79",
                "start_date_ad" => "2021-07-16",
                "end_date_ad" => "2022-07-16",
                "start_date_bs" => "2078-4-1",
                "end_date_bs" => "2079-3-32",
                "status" => 0,
                "user" => 1,
                "created_at" => "2019-05-21T09:07:24.610Z",
                "updated_at" => "2019-05-21T09:07:24.610Z"
            ],
            [
                "year" => "2079/80",
                "code" => "79/80",
                "start_date_ad" => "2022-07-17",
                "end_date_ad" => "2023-07-16",
                "start_date_bs" => "2079-4-1",
                "end_date_bs" => "2080-3-31",
                "status" => 0,
                "user" => 1,
                "created_at" => "2019-05-21T09:07:24.610Z",
                "updated_at" => "2019-05-21T09:07:24.610Z"
            ],
            [
                "year" => "2080/81",
                "code" => "80/81",
                "start_date_ad" => "2023-07-17",
                "end_date_ad" => "2024-07-15",
                "start_date_bs" => "2080-4-1",
                "end_date_bs" => "2081-3-32",
                "status" => 0,
                "user" => 1,
                "created_at" => "2019-05-21T09:07:24.610Z",
                "updated_at" => "2019-05-21T09:07:24.610Z"
            ],
            [
                "year" => "2081/82",
                "code" => "81/82",
                "start_date_ad" => "2024-07-16",
                "end_date_ad" => "2025-07-15",
                "start_date_bs" => "2081-4-1",
                "end_date_bs" => "2082-3-31",
                "status" => 0,
                "user" => 1,
                "created_at" => "2019-05-21T09:07:24.610Z",
                "updated_at" => "2019-05-21T09:07:24.610Z"
            ]
        ]);
    }
}