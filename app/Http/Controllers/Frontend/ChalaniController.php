<?php

namespace App\Http\Controllers\Frontend;

use App\helpers\FiscalYearHelper;
use App\Models\Branch;
use App\Models\Chalani;
use App\Repositories\ChalaniRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\BranchRepository;
use Illuminate\Support\Facades\Auth;

class ChalaniController extends Controller
{
    private $chalaniRepo;
    private $fiscalYearHelper;
    private $branchRepo;

    public function __construct(ChalaniRepository $chalaniRepo,
                                 BranchRepository $branchRepo)
    {
        $this->middleware('auth');
        $this->chalaniRepo = $chalaniRepo;
        $this->branchRepo = $branchRepo;
        $this->fiscalYearHelper = new FiscalYearHelper();
    }

    public function chalaniIndex(){

        $chalanis = $this->chalaniRepo->get_by_office_id();
        $chalaniNumber = $this->chalaniRepo->get_last_number_by_office(Auth::user()->office->id);
        $branch = Branch::where('office_id',Auth::user()->office->id)->get();
        return view('frontend.chalani.index', compact('chalanis','chalaniNumber', 'branch'));
    }
    
    public function chalaniCreate(){
        $chalani_number = $this->chalaniRepo->get_chalani_number(Auth::user()->office_id);
        $branch = $this->branchRepo->get_by_id();
        return view('frontend.chalani.create');
    }

    public function chalaniStore(Request $request)
    {
        $requsetData = $request->all();
        $officeId = Auth::user()->office->id;
        if($request->hasFile('image')){
            $this->validate($request, [
                'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $mytime = date('Ymd_His');
            $image = $request->file('image');
            $getimageName = $officeId.'_'.$mytime.'.'.$request->file('image')->getClientOriginalExtension();

            $image->move(public_path('images'), $getimageName);
            $requsetData['image'] = $getimageName;
        }

        $chalaniNumber = $this->chalaniRepo->get_last_number_by_office(Auth::user()->office->id);
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $requsetData['chalani_number'] = $chalaniNumber;
        $requsetData["letter_number"] = $fiscalYear->year;
        $this->chalaniRepo->create($requsetData);
        return redirect(route('chalani'))->with('message', 'Data successfully added to database');
    }

    
    public function chalaniEdit($id){
        $chalanis = $this->chalaniRepo->get_by_id($id);
        $branch = Branch::where('office_id',Auth::user()->office->id)->get();
        return view('frontend.chalani.edit', compact('chalanis', 'branch'));
    }

    public function chalaniUpdate(Request $request, $id)
    {
        try{
            $chalaniRequest = $request->all();
            $officeId = Auth::user()->office->id;
            if($request->hasFile('image')){
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $mytime = date('Ymd_His');
                $image = $request->file('image');
                $getimageName = $officeId.'_'.$mytime.'.'.$request->file('image')->getClientOriginalExtension();

                $image->move(public_path('images'), $getimageName);
                $chalaniRequest['image'] = $getimageName;
            }
            $chalani = $this->chalaniRepo->update($chalaniRequest, $id);
            return redirect(route('chalani'))->with('message', 'Data successfully Edited');
        } catch (\Exception $e){
            $message = "चलानी सम्पादन भएको छैन!";
            return redirect()->back()->with('error', $message);
        }
    }


    public function letterShow($id){
        $chalani = $this->chalaniRepo->get_by_id($id);
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.chalani.letter_show', compact('chalani'));
    }

    public function show($id){

        $chalani = $this->chalaniRepo->get_by_id($id);
        return view('frontend.chalani.show', compact('chalani'));
    }

    public function showAll($officeId){

        $chalanis = $this->chalaniRepo->get_by_office_id();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.chalani.show_all', compact('chalanis','fiscalYear'));

    }

    public function delete($id)
    {
        $chalani = $this->chalaniRepo->get_by_id($id);
        if($chalani){
            $chalani->delete();
            return json_encode(true);
        } else {
            return json_encode(false);
        }
    }
}

