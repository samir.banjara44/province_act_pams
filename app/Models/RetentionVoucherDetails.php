<?php

namespace App\Models;

use App\helpers\Money_words;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Array_;

class RetentionVoucherDetails extends Model
{
    protected $table = 'retention_vouchers_details';
    protected $guarded = [];


    public function retention_voucher(){

        return $this->belongsTo('App\Models\RetentionVoucher');
    }
    public function retention_record_name(){

        return $this->belongsTo('App\Models\RetentionRecord','retention_record','id');
    }

    public function hisab_number_name(){

        return $this->belongsTo('App\Models\ExpenseHead','hisab_number','id');
    }

    public function advancePayment(){

        return $this->belongsTo('App\Models\AdvanceAndPayment','depositor','id');
    }

    public function party_type_name(){

        return $this->belongsTo('App\Models\PartyTypes','depositor_type','id');
    }

    public function get_amount_in_word(){

        $amount = $this->amount;
        $obj = new Money_words();
        return $words = $obj->convert_number($amount);

    }

    public function getRemainRetention(){

        $income = 0;
        $collaps = 0;
        if($this->dr_or_cr == 2 and $this->byahora == 10 and $this->hisab_number==395){
            $income += floatval($this->amount);
        }
        if($this->dr_or_cr == 2 and $this->byahora == 10 and $this->hisab_number==397){
            $return = floatval($this->amount);
        } else {
            $return = 0;
        }
        if($this->dr_or_cr == 2 and $this->byahora == 9 and $this->hisab_number==398){
            $collaps += floatval($this->amount);
        } else{

            $collaps = 0;
        }
        $remain = $income - $return - $collaps;
        return $remain;


    }
}
