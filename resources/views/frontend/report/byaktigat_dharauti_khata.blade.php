<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">
<style>
    table {
        border-collapse: collapse;

    }

    .accept-table {

    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="font-size:13px" class="">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
                    सभा @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6"
                    style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    @if(Auth::user()->office->department)
                        {{Auth::user()->office->department->name}}
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}, , {{Auth::user()->office->district->name}}</td>
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>व्यक्तिगत धरौटी खाता</b>
                    </div>
                    <div style="float: right; margin-top: -20px">
                        म.ले.प.फा.नं. ६०१
                    </div>
                </td>
            </tr>

            <tr>
                <td class="kalimati" colspan="4">आर्थिक वर्ष :{{$postData['fiscal_year']}}</td>

            </tr>
        </table>

    </section>

    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table width="100%" border="1">
                    <tr>
                        <td style="width:50%">धरौटी राख्ने व्यक्ती /संस्थाको नाम :
                            <b>{{$retentionDatas[0]->advancePayment->name_nep}}</b><span
                                    style="font-weight:bold"></span></td>
                        <td>धरौटीको प्रकार :</td>
                    </tr>
                    <tr>
                        <td>मुख्य सम्पर्क व्यक्ती (संस्थाको हकमा) :</td>
                        <td>धरौटीको न्युनतम अवधि :</td>
                    </tr>
                    <tr>
                        <td>ठेगाना :</td>
                        <td>कारोबार हुने बैंक खाता नं :</td>
                    </tr>
                    <tr>
                        <td>पान नं :</td>
                        <td>बैंकको नाम र ठेगाना :</td>
                    </tr>
                    <tr>
                        <td>सम्पर्क फोन नं :</td>
                        <td>रोक्काको रकम:</td>
                    </tr>
                    <tr>
                        <td>इमेल :</td>
                        <td>रोक्काको कारण / प्रयोजन :</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>रोक्का अवधि :</td>
                    </tr>
                </table>
<br>
                <table border="1" width="100%" style="background-color:#dbdbdb; font-size: 12px" id="goswara_retention">
                    <thead>
                    <tr>
                        <th rowspan="2" width="47px">सि.नं.</th>
                        <th rowspan="2">मिति</th>
                        <th rowspan="2">भौचर नं.</th>
                        <th rowspan="2">प्रयोजन/विवरण</th>
                        <th rowspan="2">संकेत नम्बर</th>
                        <th rowspan="2">प्राप्त धरौटी</th>
                        <th colspan="3">फर्छ्यौट</th>
                        <th rowspan="2">मौज्दात</th>
                        <th rowspan="2">कैफियत</th>
                    </tr>
                    <tr>
                        <th> धरौटी फिर्ता</th>
                        <th> धरौटी सदरस्याहा</th>
                        <th>मौज्दात</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($retentionDatas as $index=>$retentionData)
                        <tr style="background-color: white">
                            <td class="kalimati" style="text-align: center">{{++$index}}</td>
                            <td class="kalimati" style="text-align: center">{{$retentionData->retention_voucher->date_nep}}</td>
                            <td class="kalimati" style="text-align: center">{{$retentionData->retention_voucher->voucher_number}}</td>
                            <td class="kalimati" style="text-align: center">{{$retentionData->retention_record_name->purpose}}</td>
                            <td class="kalimati" style="text-align: center">{{$retentionData->hisab_number_name->expense_head_code}}</td>
                            <td class="kalimati" style="text-align: right">@if($retentionData->dr_or_cr == 2 and $retentionData->byahora == 10 and $retentionData->hisab_number==395){{$retentionData->amount}}@endif</td>
                            <td class="kalimati" style="text-align: right">@if($retentionData->dr_or_cr == 1 and $retentionData->byahora == 10 and $retentionData->hisab_number==397){{$retentionData->amount}}@endif</td>
                            <td class="kalimati" style="text-align: right">@if($retentionData->dr_or_cr == 1 and $retentionData->byahora == 10 and $retentionData->hisab_number==398){{$retentionData->amount}}@endif</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="4" style="text-align: right">जम्मा</td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{$totalRetentionIncome}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalRetentionReturn}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalRetentionCollaps}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </section>

    <!-- /.content -->
</div>


<script>
    $(document).ready(function () {

        let trs = $('#goswara_retention tbody').find('tr').not(':last');
        let remain = 0;
        $.each(trs, function () {
            let remain_temp = $(this).find('td.remain_retention ').text();
            remain = parseFloat(remain) + parseFloat($.trim(remain_temp));

        });
        $('#remain_retention').text(remain);

    })
</script>
