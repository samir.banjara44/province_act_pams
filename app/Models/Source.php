<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Source extends Model
{
    protected $table = 'sources';
    protected $guarded = [];

    public function sourceType(){
        return $this->belongsTo('App\Models\SourceType');
    }

    public function getExpenseHead($officeId,$fiscal_year, $budgetSubHeadId, $source)
    {
        return $budgetByExpenseHead = BudgetDetails::where('office_id', $officeId)
            ->where('budget_sub_head_id', $budgetSubHeadId)
            ->where('fiscal_year', $fiscal_year)
            ->where('source', $source)
            ->groupBy('expense_head_id')
            ->orderBy('expense_head_id')
            ->get();
    }

    public function getLastBudget($fiscal_year,$expenHead, $program_id,$source)
    {
        return $last_budget = Budget::where('budget_sub_head', $program_id)
            ->where('expense_head', $expenHead)
            ->where('source',$source)
            ->where('fiscal_year',$fiscal_year)
            ->sum('total_budget');

    }

    public function getTotalSourecBudget($fiscalYear,$budgetSubHead, $source){
        return $last_budget = Budget::where('budget_sub_head', $budgetSubHead)
            ->where('source', $source)
            ->where('fiscal_year', $fiscalYear)
            ->sum('total_budget');
    }

    public function get_expense_upto_this_month($fiscal_year,$budget_sub_head, $month_post,$source,$expense_head)
    {
        return $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('budget_details','activity_report_details.activity_id','=','budget_details.id')
            ->join('sources','budget_details.source','=','sources.id')
            ->join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
            ->where('vouchers.status', '=',1)
            ->where('budget_details.source', '=',$source)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.expense_head', $expense_head)
            ->where('activity_report_details.fiscal_year', $fiscal_year)
            ->where('activity_report_details.month','<=',$month_post)
            ->sum('activity_report_details.expense');
    }

    public function getTotalExpenseUpToThisMonthSourceWise($fiscal_year,$budget_sub_head, $month_post,$source)
    {
        return $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('budget_details','activity_report_details.activity_id','=','budget_details.id')
            ->join('sources','budget_details.source','=','sources.id')
            ->join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
            ->where('vouchers.status', '=',1)
            ->where('budget_details.source', '=',$source)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.fiscal_year','=',$fiscal_year)
            ->where('activity_report_details.month','<=',$month_post)
            ->sum('activity_report_details.expense');
    }

    public function get_expense_upto_prev_month($fiscal_year,$budget_sub_head, $month_post,$source,$expense_head)
    {
        return $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('budget_details','activity_report_details.activity_id','=','budget_details.id')
            ->join('sources','budget_details.source','=','sources.id')
            ->join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
            ->where('vouchers.status', '=',1)
            ->where('budget_details.source', '=',$source)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.expense_head', $expense_head)
            ->where('activity_report_details.fiscal_year', $fiscal_year)
            ->where('activity_report_details.month','<',$month_post)
            ->sum('activity_report_details.expense');
    }

    public function getTotalExpenseUpToPrevMonthSourceWise($fiscal_year,$budget_sub_head, $month_post,$source)
    {
        return $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('budget_details','activity_report_details.activity_id','=','budget_details.id')
            ->join('sources','budget_details.source','=','sources.id')
            ->join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
            ->where('vouchers.status', '=',1)
            ->where('budget_details.source', '=',$source)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.fiscal_year','=',$fiscal_year)
            ->where('activity_report_details.month','<',$month_post)
            ->sum('activity_report_details.expense');
    }

    public function get_expense_this_month($fiscal_year,$budget_sub_head, $month_post,$source,$expense_head)
    {
        return $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('budget_details','activity_report_details.activity_id','=','budget_details.id')
            ->join('sources','budget_details.source','=','sources.id')
            ->join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
            ->where('vouchers.status', '=',1)
            ->where('budget_details.source', '=',$source)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.expense_head', $expense_head)
            ->where('activity_report_details.fiscal_year', $fiscal_year)
            ->where('activity_report_details.month','=',$month_post)
            ->sum('activity_report_details.expense');
    }

    public function getTotalExpenseThisMonthSourceWise($fiscal_year,$budget_sub_head, $month_post,$source)
    {
        return $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('budget_details','activity_report_details.activity_id','=','budget_details.id')
            ->join('sources','budget_details.source','=','sources.id')
            ->join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
            ->where('vouchers.status', '=',1)
            ->where('budget_details.source', '=',$source)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.fiscal_year','=',$fiscal_year)
            ->where('activity_report_details.month','=',$month_post)
            ->sum('activity_report_details.expense');
    }

    public function get_peski_upto_this_month($fiscal_year,$budget_sub_head, $month_post,$expense_head)
    {
        $advance = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $fiscal_year)
            ->where('voucher_details.office_id', '=', $this->office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.month', '<=', $month_post)
            ->where('voucher_details.expense_head_id', '=', $expense_head)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('ledger_type_id', '4')
            ->sum('voucher_details.dr_amount');

        $advanceClearance = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $this->office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.month', '<=', $month_post)
            ->where('voucher_details.expense_head_id', '=', $this->expense_head_id)
            ->where('voucher_details.dr_or_cr',2)
            ->where('ledger_type_id',9)
            ->sum('voucher_details.cr_amount');
        return $remain = (float)$advance - (float)$advanceClearance;

    }

    public function getTotalPeskiUpToThisMonthSourceWise($fiscal_year,$budget_sub_head, $month_post)
    {
        $advance = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $this->office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.month', '<=', $month_post)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.ledger_type_id', '4')
            ->where('voucher_details.fiscal_year', $fiscal_year)
            ->sum('voucher_details.dr_amount');

        $advanceClearance = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $this->office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.month', '<=', $month_post)
            ->where('voucher_details.dr_or_cr',2)
            ->where('voucher_details.fiscal_year',$fiscal_year)
            ->where('ledger_type_id',9)
            ->sum('voucher_details.cr_amount');
        return $remain = (float)$advance - (float)$advanceClearance;

    }

    public function expense_without_advance($fiscal_year,$budget_sub_head, $month_post,$source,$expense_head_id,$expense_head_code)
    {
        $get_total_expense = $this->get_expense_upto_this_month($fiscal_year,$budget_sub_head, $month_post,$source,$expense_head_code);
        $total_advance = $this->get_peski_upto_this_month($fiscal_year,$budget_sub_head, $month_post,$expense_head_code);
        return $total_exp_with_out_advance = (float)$get_total_expense - (float)$total_advance;
    }

    public function getTotalExpenseWithoutPeskiSourceWise($fiscal_year,$budget_sub_head, $month_post,$source)
    {
        $get_total_expense = $this->getTotalExpenseUpToThisMonthSourceWise($fiscal_year,$budget_sub_head, $month_post,$source);
        $total_advance = $this->getTotalPeskiUpToThisMonthSourceWise($fiscal_year,$budget_sub_head, $month_post);
        return $total_exp_with_out_advance = (float)$get_total_expense - (float)$total_advance;

    }

    public function remain_budget($fiscal_year,$budget_sub_head, $month_post, $source,$expense_head_id,$expense_head_code)
    {
        $total_budget = $this->getLastBudget($fiscal_year,$expense_head_code, $budget_sub_head,$source);
        $get_total_expense = $this->get_expense_upto_this_month($fiscal_year,$budget_sub_head, $month_post,$source,$expense_head_code);
        return $remain = (float)$total_budget - (float)$get_total_expense;
    }

    public function getTotalRemainBudgetSourceWise($fiscal_year, $budget_sub_head, $month_post, $source)
    {
        $total_budget = $this->getTotalSourecBudget($fiscal_year,$budget_sub_head,$source);
        $get_total_expense = $this->getTotalExpenseUpToThisMonthSourceWise($fiscal_year,$budget_sub_head, $month_post,$source);
        return $remain = (float)$total_budget - (float)$get_total_expense;
    }


}
