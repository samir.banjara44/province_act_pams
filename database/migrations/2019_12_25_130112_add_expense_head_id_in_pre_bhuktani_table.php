<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpenseHeadIdInPreBhuktaniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_bhuktani', function (Blueprint $table) {

            $table->integer('expense_head_id')->nullable();

        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_bhuktani', function (Blueprint $table) {

            $table->dropColumn('expense_head_id');
        });
    }
}
