<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\BudgetSourceLevel;
use App\Models\SourceType;
use App\Repositories\BudgetSourceLevelRepository;
use App\Repositories\SourceTypeRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class BudgetSourceLevelController extends Controller
{

    private $budgetSourceLevelRepo;
    private $sourceTypeRepo;

    public function __construct(
        BudgetSourceLevelRepository $budgetSourceLevelRepo,
        SourceTypeRepository $sourceTypeRepo
    )
    {
        $this->middleware('auth');
        $this->budgetSourceLevelRepo = $budgetSourceLevelRepo;
        $this->sourceTypeRepo = $sourceTypeRepo;
    }

    public function index()
    {

        $budgetSourceLevels= BudgetSourceLevel::all();
        return view('backend.settings.budget_source_level.index', compact('budgetSourceLevels'));
    }

    public function create(){

        $sourceTypes = SourceType::all();
        return view('backend.settings.budget_source_level.create',compact('sourceTypes'));
    }
    public function store(Request $request){
        $program = $this->budgetSourceLevelRepo->create($request->all());
        return redirect(route('admin.budget.source'));
    }

    public function get_source_level_by_source_type(Request $request, $source_type_id){
       $source_levels =  $this->budgetSourceLevelRepo->get_all_by_source_type($source_type_id);
       return json_encode($source_levels);

    }

}
