<?php
namespace App\Repositories;

use App\Models\MainProgram;
use Illuminate\Support\Facades\Auth;

class MainProgramRepositoryEloquent implements MainProgramRepository
{
    public function getAllMainProgram(){
        return $mainPrograms = MainProgram::all();
    }
    public function create($attributes) {
        $mainProgram = new MainProgram();
        $mainProgram->name = $attributes['name'];
        $mainProgram->area_id = $attributes['area'];
        $mainProgram->sub_area_id = $attributes['sub_area'];
        $mainProgram->status = 1;
        $mainProgram->save();
        return $mainProgram;
    }

    public function get_all_by_sub_area_id($sub_area_id){

       return $subAreas = MainProgram::where('sub_area_id',$sub_area_id)->get();
    }
}