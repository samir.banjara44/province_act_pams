<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Taha extends Model
{
    protected $table = 'tahas';
    protected $guarded = [];

    public function mofs(){
        return $this->hasMany('App\Models\Mof');
    }

    public function ministries(){
        return $this->hasMany('App\Models\Ministries');
    }

    public function departments(){
        return $this->hasMany('App\Models\Department');
    }

    public function offices(){
        return $this->hasMany('App\Models\Office');
    }

    public function users(){
        return $this->hasMany('App\Users');
    }
}
