@extends('frontend.layouts.app')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1><i class="fas fa-window-restore"></i> प्रोगेस फारम </h1>

    </section>

    <section class="formSection">
        <div class="row justify-content-md-center">
            <div class="col-md-10 form-bg col-md-offset-1">
                <button type="button" class="btn btn-danger btn-sm btn-show-form displayNone" id="closeAddForm"><i
                            class="fas fa-times"></i> फर्म रद्ध गर्ने</button>
                <div class="form-title">बजेट विवरण प्रविष्टी गर्ने</div>
                <form action="{{route('progress.store')}}" method="post" id="formSubmit">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="fiscal_year"> आर्थिक वर्ष:</label>
                                        <select name="fiscal_year" id="fiscal_year" class="form-control kalimati">
                                            @foreach($fiscalYears as $fy)
                                                <option value="{{$fy->id}}" @if($fy->id == $fiscalYear->id) selected @endif class="kalimati">{{$fy->year}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <label for="fiscal_year"> मिति </label>
                                        <input type="text" class="form-control" name="date" id="date" value=""
                                               required>
                                    </div>

                                    <div class="col-md-3">
                                        <label for="expense_center">खर्च केन्द्र</label>
                                        <select id="expense_center" class="form-control" name="expense_center" required>
                                            <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}
                                            </option>
                                        </select>
                                    </div>

                                    <div class="col-md-5">
                                        <label for="budget_sub_head">बजेट उपशिर्षक</label>
                                        <select name="program" class="form-control" id="program"
                                                required>
                                            <option value="">................</option>
                                            @foreach($programs as $program)
                                                <option value="{{$program->id}}" @if($program->id ==
                                                Session::get('budget_sub_head')) selected
                                                        @endif>{{$program->program_code}}
                                                    |{{$program->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <label for="mainActivityName">कार्यक्रम आयोजनाको नाम</label>
                                        <select name="main_activity_id" class="form-control select2"
                                                id="main_activity_id" required>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">चौमासिक प्रकार</label>
                                        <select name="chaumasik_type" class="form-control" id="chaumasik_type" required>
                                            <option value="">................</option>
                                            <option value="1">प्रथम चौमासिक</option>
                                            <option value="2">दोस्रो चौमासिक</option>
                                            <option value="3">तेस्रो चौमासिक</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>कार्यक्रम प्रोग्रेस</label>
                                        <textarea class="form-control" name="progress" id="progress" style="width: 270px"></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">&nbsp;</label>
                                        <button type="button" id="submitBtn" class="btn btn-primary"><i class="fas fa-save"></i>
                                            Save</button>
                                        <a type="button" class="btn btn-primary" href="{{route('activity.progress.create')}}">
                                            Clear </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection

