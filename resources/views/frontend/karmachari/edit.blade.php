@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> <i class="fas fa-edit"></i> पेश्की / भुक्तानी पाउने कर्मचारी खाता विवरण सम्पादन </h1>
            
            <a type="button" href="{{route('karmachari')}}" class="btn btn-sm btn-primary"><i class="fas fa-arrow-left"></i> लिष्टमा जाने</a>
                                
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body karmachari_modal pt-15">
                    <form action="{{route('karmachari.update',$karmachari->id)}}" method="post">
                        {{csrf_field()}}

                        {{--Personal Details--}}
                        <h3>व्यक्तिगत विवरण</h3>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-3">
                                    <label for="name_nepali">पुरा नाम नेपालीमा :</label>
                                    <input type="text" class="form-control" id="name_nepali" placeholder="Full Name (Nepali)" name="name_nepali" value="{{$karmachari->name_nepali}}" required>
                                </div>

                                <div class="col-md-3">
                                    <label for="name_english">पुरा नाम अंग्रेजीमा :</label>
                                    <input type="text" class="form-control" id="name_english" placeholder="Full Name (English)" name="name_english" value="{{$karmachari->name_english}}">
                                </div>

                                <div class="col-md-2">
                                    <label for="gender">लिङ्ग:</label>
                                    <select class="form-control" name="gender" id="gender">
                                        <option value="">....</option>
                                        <option value="0" @if($karmachari->gender == '0') selected @endif>महिला</option>
                                        <option value="1" @if($karmachari->gender == '1') selected @endif>पुरुष</option>
                                        <option value="2" @if($karmachari->gender == '2') selected @endif>अन्य</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="date_of_birth">जन्म मिति :</label>
                                    <input type="text" class="form-control" id="date_of_birth" placeholder="Select Date" name="date_of_birth" value="{{$karmachari->date_of_birth}}">
                                </div>

                                <div class="col-md-2">
                                    <label for="marital_status">वैवाहिक स्थिति:</label>
                                    <select class="form-control" name="marital_status" id="marital_status">
                                        <option value="">....</option>
                                        <option value="0" @if($karmachari->marital_status == 0) selected @endif>एकल</option>
                                        <option value="1" @if($karmachari->marital_status == 1) selected @endif>दम्पति</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        

                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-3">
                                    <label for="bank">बैङ्कको नाम</label>
                                    <select name="bank" class="form-control">
                                        <option value="" selected="">.........</option>
                                        @foreach($all_banks as $bank)
                                            <option value="{{$bank->id}}"
                                                    @if($bank->id == $karmachari['bank']) selected @endif>{{$bank->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="col-md-3">
                                    <label>शाखा कार्यालय</label>
                                    <input type="text" class="form-control" name="bank_address" id="bank_address" value="{{$karmachari->bank_address}}">
                                </div>
                                
                                <div class="col-md-2">
                                    <label>खाता नं.</label>
                                    <input type="text" class="form-control" name="khata_number" id="khata_number" value="{{$karmachari->khata_number}}">
                                </div>
                                
                                <div class="col-md-2">
                                    <label for="vat_pan"> भ्याट/प्यान नं.</label>
                                    <input type="text" class="form-control" id="vat_pan" placeholder="VAT or PAN Number" name="vat_pan" value="{{$karmachari->vat_pan}}">
                                </div>
                                
                                <div class="col-md-2">
                                    <label>पेयी कोड</label>
                                    <input type="number" class="form-control" name="payee_code" id="payee_code" value="@if($karmachari->advancePayment and $karmachari->advancePayment->payee_code){{$karmachari->advancePayment->payee_code}}@endif">
                                </div>
                                
                            </div>
                        </div>

                        {{-- Personal Details End--}}
                        <div class="clearfix"></div>

                        <h3>सम्पर्क विवरण</h3>
                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-2">
                                    <label>प्रदेश</label>
                                    <select class="form-control" id="province" name="province">
                                        <option>.....................</option>
                                        @foreach($provinces as $province)
                                            <option value="{{$province->id}}"
                                                    @if($province->id == $karmachari->prvince_id) selected @endif>{{$province->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="district">जिल्ला :</label>
                                    <select class="form-control select2" name="district" id="district">
                                        <option value="">........</option>
                                        @foreach($districts as $district)
                                            <option value="{{$district->id}}"
                                                    @if($district->id == $karmachari->district_id) selected @endif>{{$district->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="local_level">स्थानीय तह :</label>
                                    <select class="form-control select2" name="local_level" id="local_level">
                                        <option value="">......</option>
                                        @foreach($local_levels as $local_level)
                                            <option value="{{$local_level->id}}"
                                                    @if($local_level->id == $karmachari->staniha_taha) selected @endif>{{$local_level->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="phone_number">फोन नं.:</label>
                                    <input type="text" class="form-control" id="phone_number" placeholder="Phone Number" name="phone_number" value="{{$karmachari->phone_number}}">
                                </div>

                                <div class="col-md-2">
                                    <label for="mobile_number">मोबाइल:</label>
                                    <input type="text" class="form-control" id="mobile_number" placeholder="Mobile Number" name="mobile_number" value="{{$karmachari->mobile_number}}">
                                </div>

                                <div class="col-md-2">
                                    <label for="email_address">इमेल:</label>
                                    <input type="email" class="form-control" id="email_address" placeholder="Email Address" name="email_address" value="{{$karmachari->email_address}}">
                                </div>

                            </div>
                        </div>

                        {{-- Contact Details End --}}

                        <div class="clearfix"></div>

                        {{-- Organizational Details--}}
                        <h3>कार्यालयसम्बन्धी विवरण</h3>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-3">
                                    <label for="darbandi_srot">दरबन्दीको श्रोत:</label>
                                    <select class="form-control" name="darbandi_srot_id" id="darbandi_srot_id">
                                        <option value="">....</option>
                                        @foreach($darbandisrots as $darbandisrot)
                                            <option value="{{$darbandisrot->id}}"
                                                    @if($darbandisrot->id == $karmachari->darbandi_srot_id) selected @endif>
                                                {{$darbandisrot->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for="darbandi_type">दरबन्दीको प्रकार:</label>
                                    <select class="form-control" name="darbandi_type_id" id="darbandi_type_id">
                                        <option></option>
                                        @foreach($darbanditypes as $darbanditype)
                                            <option value="{{$darbanditype->id}}"
                                                    @if($darbanditype->id == $karmachari->darbandi_type_id) selected @endif>{{$darbanditype->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for="sheet_roll_no">सिटरोल नंबर.:</label>
                                    <input type="number" class="form-control" id="sheet_roll_no" placeholder="Sheet Roll Number" name="sheet_roll_no" value="{{$karmachari->sheet_roll_no}}">
                                </div>

                                <div class="col-md-3">
                                    <label for="sewa">सेवा वर्ग:</label>
                                    <select class="form-control" name="sewa_barga" id="sewa_barga">
                                        <option>................</option>
                                        <option value="1" @if($karmachari->sewa_barga == 1) selected @endif>निजामती सेवा
                                        </option>
                                        <option value="2" @if($karmachari->sewa_barga == 2) selected @endif>अन्य</option>
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-3">
                                    <label for="sewa">सेवा:</label>
                                    <select class="form-control" name="sewa_id" id="sewa_id">
                                        <option>................</option>
                                        @foreach($sewas as $sewa)
                                            <option value="{{$sewa->id}}"
                                                    @if($sewa->id == $karmachari->sewa_id) selected @endif >{{$sewa->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for="samuha_id">समूह:</label>
                                    <select class="form-control select2" name="samuha_id" id="samuha_id" style="width: 219px;">
                                        <option value=""></option>
                                        @foreach($samuhas as $samuha)
                                            <option value="{{$samuha->id}}"
                                                    @if($samuha->id == $karmachari->samuha_id) selected @endif >{{$samuha->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for="taha">श्रेणी/तह:</label>
                                    <select class="form-control select2" name="taha_id" id="taha_id">
                                        <option value="">....</option>
                                        @foreach($tahas as $taha)
                                            <option value="{{$taha->id}}"
                                                    @if($taha->id == $karmachari->taha_id)selected @endif >{{$taha->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for="pad">पद:</label>
                                    <input type="text" class="form-control" name="pad_id" id="pad_id" value="{{$karmachari->pad_id}}">

                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-2">
                                    <label for="office_head">कार्यालय प्रमुख:</label>
                                    <select class="form-control" name="office_head" id="office_head">
                                        <option value="1" @if($karmachari->is_office_head == 1) selected @endif>हो</option>
                                        <option value="2" @if($karmachari->is_office_head == 2) selected @endif>होईन</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="acc_head">आर्थिक प्रमुख:</label>
                                    <select class="form-control" name="acc_head" id="acc_head">
                                        <option value="1" @if($karmachari->is_acc_head == 1) selected @endif>हो</option>
                                        <option value="2" @if($karmachari->is_acc_head == 2) selected @endif>होईन</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="is_peski">पेश्किमा देखाउने:</label>
                                    <select class="form-control" name="is_peski" id="is_peski">
                                        <option value="1" @if($karmachari->is_peski == 1) selected @endif>हो</option>
                                        <option value="2" @if($karmachari->is_peski == 2) selected @endif>होईन</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="is_payroll">तलवी देखउने:</label>
                                    <select class="form-control" name="is_payroll" id="is_payroll">
                                        <option value="1" @if($karmachari->is_payroll == 1) selected @endif>हो</option>
                                        <option value="2" @if($karmachari->is_payroll == 2) selected @endif>होईन</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="can_accept">स्विकृत:</label>
                                    <select class="form-control" name="can_accept" id="can_accept">
                                        <option value="1" @if($karmachari->can_accept == 1) selected @endif>हो</option>
                                        <option value="2" @if($karmachari->can_accept == 2) selected @endif>होईन</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="status">सक्रिय:</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1" @if($karmachari->status == 1) selected @endif>हो</option>
                                        <option value="2" @if($karmachari->status == 2) selected @endif>होईन</option>
                                    </select>
                                </div>

                            </div>
                        </div>


                        {{-- Organizational Detail End--}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <center>
                                        <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> सम्पादन गर्ने</button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>





@endsection

@section('scripts')

    <script>

        let karmachari_json = '{!!$karmachari!!}';
        console.log("this is karmachari", karmachari_json);
        let karmachari = $.parseJSON(karmachari_json);


    </script>



    {{--  sewa बाट तह आउने--}}
    <script>

        $(document).ready(function () {

            let sewa_id = $('select#sewa_id').val();
            // get_taha_by_sewa(sewa_id);
        });


        let get_taha_by_sewa = function () {
            let sewa_id = $('select#sewa_id').val();
            let url = '{{route('get_taha_by_sewa',123)}}';
            url = url.replace('123', sewa_id);

            $.ajax({

                method: 'get',
                url: url,
                success: function (res) {

                    // console.log($.parseJSON(res));
                    let tahas = $.parseJSON(res);
                    let options = '<option value="">.........</option>'
                    $.each(tahas, function () {
                        if (karmachari.taha_id == this.id)
                            options += '<option value="' + this.id + '" selected>' + this.name + '</option>';
                        else
                            options += '<option value="' + this.id + '">' + this.name + '</option>';

                    });

                    $('#taha_id').html(options);
                    padByTaha(karmachari.taha_id);


                }
            })

        };

        $('#sewa_id').change(function () {

            get_taha_by_sewa();
        })
    </script>

    {{--  Province_id click हुदा district आउने--}}
    <script>
        $(document).ready(function () {
            $('#province').change(function () {
                let province_id = $('select#province').val();

                let url = '{{route('get.district',123)}}';
                url = url.replace(123, province_id);

                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {
                        let options = '<option selected>....................</option>';
                        // console.log($.parseJSON(res));
                        $.each($.parseJSON(res), function () {

                            options += '<option value="' + this.id + '">' + this.name + '</option>';
                        });

                        $('#district').html(options);
                    }
                })
            })
        })
    </script>

    {{--  District click हुदा local level आउने--}}
    <script>
        $(document).ready(function () {
            $('#district').change(function () {
                let district_id = $('select#district').val();

                let url = '{{route('get.local.level',123)}}';
                url = url.replace(123, district_id);

                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {
                        let options = '<option selected>....................</option>';
                        $.each($.parseJSON(res), function () {

                            options += '<option value="' + this.id + '">' + this.name + '</option>';
                        });

                        $('#local_level').html(options);
                    }
                })
            })
        })
    </script>
    {{--  Date picker--}}
    <script>
        $("#date_of_birth").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        // $("#date_of_birth").val(formatedNepaliDate);

    </script>


    {{--  Sewa bata Samuha Aaune--}}

    <script>

        $(document).ready(function () {
            let sewa_id = $('select#sewa_id').val();
            // samuhaBySewa(sewa_id);
        });


        let samuhaBySewa = function (sewa_id) {

            // alert(sewa_id);
            // alert('inside sewabysamuha sewa id = '+sewa_id);

            if (sewa_id) {
                let url_ = '{{route('samuha.getbysewa', '123')}}';
                url_ = url_.replace('123', sewa_id);
                $.ajax({
                    url: url_,  // Route Name here
                    type: "GET",
                    dataType: "json",
                    success: function (data) {

                        // console.log(data);
                        let options = '<option>.........</option>';

                        $('#samuha_id').empty();
                        $.each(data, function (key, value) {
                            console.log(this);
                            // if(karmachari.samuha_id == this.id)
                            //   options +=  '<option value="'+ this.id +'" selected>'+ this.name+'</option>';
                            // else
                            options += '<option value="' + this.id + '">' + this.name + '</option>';

                        });
                        $('#samuha_id').html(options).change();

                    }
                })
            } else {
                $('$samuha_id').empty();
            }

        };

        $(document).ready(function () {

            $('#sewa_id').on('change', function () {  // Make Separate function with code inside this function and call it from here

                samuhaBySewa($(this).val());
            });

        });
    </script>

    {{--  sreni/taha bata Pad Aaune--}}

    <script>
        let padByTaha = function (taha_id) {
            // alert(taha_id);
            if (taha_id) {
                //alert("samuha id changed");
                let url_ = '{{route('admin.designations.getbyTaha', '123')}}';
                url_ = url_.replace('123', taha_id);
                $.ajax({
                    url: url_,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        //alert('on success');
                        // console.log(data);
                        // $('#pad_id').empty();
                        let options = '<option value="">..............</option>';
                        $.each(data, function (key, val) {
                            // console.log(karmachari.pad_id,this.id);
                            // if (karmachari.pad_id == key)
                            //     options += '<option value="' + key + '" selected>' + val + '</option>';
                            // else
                                options += '<option value="' + key + '">' + val + '</option>';

                        });
                        $('#pad_id').html(options).change();

                    }
                })
            } else {
                $('#pad_id').html(options)
            }
        };


        $(document).ready(function () {

            $('#taha_id').on('change', function () {

                let taha_id = $('#taha_id').val();
                padByTaha(taha_id);
            });

        });

    </script>


    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            // console.log(input);
            var charArray = input.split('');
            //console.log(charArray);
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1'
                        break
                    case '२':
                        engDate += '2'
                        break
                    case '३':
                        engDate += '3'
                        break
                    case '४':
                        engDate += '4'
                        break
                    case '५':
                        engDate += '5'
                        break
                    case '६':
                        engDate += '6'
                        break
                    case '०':
                        engDate += '0'
                        break
                    case '७':
                        engDate += '7'
                        break
                    case '८':
                        engDate += '8'
                        break
                    case '९':
                        engDate += '9'
                        break

                    case '-':
                        engDate += '-'
                        break
                }
                //console.log(engDate)
            })
            return engDate

        }
    </script>

@endsection