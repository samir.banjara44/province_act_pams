<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jv_number');
            $table->integer('office_id');
            $table->integer('budget_sub_head_id');
            $table->float('payement_amount');
            $table->text('short_narration');
            $table->longText('long_narration');
            $table->string('data_nepali');
            $table->dateTime('date_english');
            $table->integer('month');
            $table->integer('user_id');
            $table->integer('status');
            $table->string('fiscal_year');
            $table->integer('prepared_by')->nullable();
            $table->integer('submitted_by')->nullable();
            $table->integer('approved_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');

    }
}
