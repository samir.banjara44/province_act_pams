<?php

namespace App\Http\Controllers\Frontend;

use App\helpers\FiscalYearHelper;
use App\Http\Controllers\Controller;
use App\Models\ActivityReport;
use App\Models\ActivityReportDetail;
use App\Models\Akhtiyari;
use App\Models\Area;
use App\Models\Department;
use App\Models\Budget;
use App\Models\ExpenseHead;
use App\Models\MainProgram;
use App\Models\Medium;
use App\Models\Ministry;
use App\Models\Mof;
use App\Models\Office;
use App\Models\Province;
use App\Models\Programs;
use App\Models\SourceType;
use App\Models\SubArea;
use App\Models\Unit;
use App\Repositories\ActivityProgressRepository;
use App\Repositories\AreaRepository;
use App\Repositories\ExpenseHeadRepository;
use App\Repositories\MainProgramRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\SourceTypeRepository;
use App\Repositories\SubAreaRepository;
use Illuminate\Http\Request;
use App\Repositories\BudgetRepository;
use App\Repositories\ActivityReportDetailRepository;
use Illuminate\Support\Facades\Auth;

class BudgetController extends Controller
{

    private $programRepo;
    private $budgetRepo;
    private $areaRepo;
    private $subAreaRepo;
    private $expenseHeadRepo;
    private $mainProgramRepo;
    private $sourceTypeRepo;
    private $fiscalYearHelper;
    private $activityProgressRepo;
    private $activityReportDetailsRepo;

    public function __construct(

        ProgramRepository $programRepo,
        BudgetRepository $budgetRepo,
        AreaRepository $areaRepo,
        SubAreaRepository $subAreaRepo,
        MainProgramRepository $mainProgramRepo,
        SourceTypeRepository $sourceTypeRepo,
        ExpenseHeadRepository $expenseHeadRepo,
        ActivityProgressRepository $activityProgressRepo,
        ActivityReportDetailRepository $activityReportDetailsRepo
    )
    {

        $this->middleware('auth');
        $this->budgetRepo = $budgetRepo;
        $this->programRepo = $programRepo;
        $this->areaRepo = $areaRepo;
        $this->subAreaRepo = $subAreaRepo;
        $this->mainProgramRepo = $mainProgramRepo;
        $this->mainProgramRepo = $mainProgramRepo;
        $this->expenseHeadRepo = $expenseHeadRepo;
        $this->fiscalYearHelper = new FiscalYearHelper();
        $this->activityProgressRepo = $activityProgressRepo;
        $this->activityReportDetailsRepo = $activityReportDetailsRepo;

    }

    public function create(Request $request)
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $sources = SourceType::all();
        $programs = $this->programRepo->get_programs_for_budget();
        $areas = $this->areaRepo->getAllArea();
        $expenseHeads = $this->expenseHeadRepo->get_expense_head_by_ledger_type();
        $mediums = Medium::all();
        $request->session()->put('budget_sub_head', '');

        return view('frontend.budget.create', compact('programs', 'areas', 'expenseHeads', 'sources', 'mediums', 'fiscalYear','fiscalYears'));
    }

    public function store(Request $request)
    {
        $datas = $request->all();
        $expense_head = $this->expenseHeadRepo->get_by_id($datas['expense_head']);
        $datas['expense_head_code'] = $expense_head['expense_head_code'];
        $request->session()->put('budget_sub_head', $datas['budget_sub_head']);
        $this->budgetRepo->create($datas);
        return redirect(route('budget.create.continue'));
    }


    public function edit()
    {
        session()->forget('budget_sub_head');
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $sources = SourceType::all();
        $programs = $this->programRepo->get_by_office_id();
//        $areas = $this->areaRepo->getAllArea();
        $expenseHeads = $this->expenseHeadRepo->get_expense_head_by_ledger_type();
        $mediums = Medium::all();
        $units = Unit::all();
        return view('frontend.budget.edit', compact('programs', 'expenseHeads', 'sources', 'mediums', 'fiscalYear', 'units','fiscalYears'));
    }

    public function create_continue(Request $request)
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $sources = SourceType::all();
        $programs = $this->programRepo->get_by_office_id();
        $expenseHeads = $this->expenseHeadRepo->get_expense_head_by_ledger_type();
        $mediums = Medium::all();
        $request->session()->remove('success');

        return view('frontend.budget.create', compact('programs', 'expenseHeads', 'sources', 'mediums', 'fiscalYear','fiscalYears'));
    }

    public function getBudgets($fiscalYear,$budgetSubHeadId){
        $budgets =  $this->budgetRepo->getByBudgetSubHead($fiscalYear,$budgetSubHeadId);
        return json_encode($budgets);
    }
    public function get_main_activity_by_budget_sub_head(Request $request, $fiscalYear,$budget_sub_head)
    {
        $office_id = Auth::user()->office->id;
        $budgets = $this->budgetRepo->get_by_budget_sub_head_and_office_id($office_id, $fiscalYear,$budget_sub_head);
        return json_encode($budgets);
    }

    public function get_expense_head_by_activity_id(Request $request, $activity_id)
    {
        $budget = $this->budgetRepo->get_by_id($activity_id);
        return json_encode($budget);
    }


//    budget create greda
    public function get_main_activity_by_akhtiyari(Request $request, $akhtiyari_id)
    {
        $main_activities = $this->budgetRepo->get_by_akhtiyari_id($akhtiyari_id);
        return json_encode($main_activities);

    }


    public function get_total_akhtiyari_by_budget_sub_head($fiscalYear,$budget_sub_head)
    {
        $totalAkhtiyari = $this->budgetRepo->get_total_akhtiyari_by_budget_sub_head($fiscalYear,$budget_sub_head);
        return json_encode($totalAkhtiyari);
    }

    public function get_total_budget_by_akhtiyari(Request $request, $akhtiyari_id)
    {
        $totalBudget = $this->budgetRepo->get_total_budget_by_akhtiyari($akhtiyari_id);
        return json_encode($totalBudget);
    }

    public function akhtiyariIndex()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $sources = SourceType::all();
        $programs = $this->programRepo->get_by_office_id();
        $expenseHeads = $this->expenseHeadRepo->get_expense_head_by_ledger_type();
        $mediums = Medium::all();

        $office_id = Auth::user()->office_id;
        $budget_sub_heads = $this->programRepo->get_by_office_id($office_id);
        return view('frontend.budget.akhtiyari', compact('fiscalYears', 'fiscalYear','programs', 'expenseHeads', 'mediums', 'sources'));
    }

    public function akhtiyariStore(Request $request)
    {
        $akhtiYari = $request->all();
        $akhtiyar = $this->budgetRepo->createAkhtiyari($akhtiYari);
        return redirect(route('akhtiyari.param'));
    }


    public function akhtiyariAddParam($id)
    {
        $budget_sub_head_id = $id;
        $bdget_sub_heads = $this->programRepo->get_by_program_id($budget_sub_head_id);
        return view('frontend.budget.akhtiyariAddParam', compact('bdget_sub_heads'));
    }

    public function getAkhtiyariByBudgetSubHeadId(Request $request, $fiscalYear,$budget_sub_head_id)
    {
        $requestData = $request->all();
        $akhtiyari = $this->budgetRepo->getAkhtiyariByBudgetSubHeadId($fiscalYear,$budget_sub_head_id);
        return json_encode($akhtiyari);
    }

    public function editAkhtiyari($akhtiyari_id)
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $sources = SourceType::all();
        $programs = $this->programRepo->get_by_office_id();
        $expenseHeads = $this->expenseHeadRepo->get_expense_head_by_ledger_type();
        $mediums = Medium::all();
        $office_id = Auth::user()->office_id;
        $budget_sub_heads = $this->programRepo->get_by_office_id($office_id);

        $akhtiyary = $this->budgetRepo->get_akhtiyari_by_akhtiyari_id($akhtiyari_id);
        $totalBudgetOnAkhtiyar = ($akhtiyary->budget->sum('total_budget'));
        return view('frontend.budget.akhtiyari_edit', compact('fiscalYear', 'fiscalYears','programs', 'expenseHeads', 'mediums', 'sources', 'akhtiyary', 'totalBudgetOnAkhtiyar'));

    }

    public function get_budget_details_by_id($activity_id)
    {
        $budgetCollection = [];
        $budgetCollection['details'] = $this->budgetRepo->get_budget_details_by_id($activity_id);
        $budgetDetails = $this->budgetRepo->getBudgetDetailByBudgetId($activity_id);
        $budgetCollection['totalExpense'] = 0;
        foreach ($budgetDetails as $index=>$budgetDetail){
            if(++$index == 1){
                $budgetCollection['activity_expense'] = $this->activityReportDetailsRepo->getExpenseByBudgetDetailsId($budgetDetail->id);
                $budgetCollection['totalExpense'] = $budgetCollection['totalExpense'] + $budgetCollection['activity_expense'];
            } else {
                $budgetCollection['contenjency'] = $this->activityReportDetailsRepo->getExpenseByBudgetDetailsId($budgetDetail->id);
                $budgetCollection['totalExpense'] = $budgetCollection['totalExpense'] + $budgetCollection['contenjency'];
            }

        }
        return json_encode($budgetCollection);
    }

    public function update(Request $request, $id)
    {
        $datas = $request->all();
        $expense_head = $this->expenseHeadRepo->get_by_id($datas['expense_head']);
        $datas['expense_head_code'] = $expense_head['expense_head_code'];
        $budget_create = $this->budgetRepo->update($datas, $id);
        return redirect(route('budget.edit'));
    }


//    Rakamantar
    function rakamantarParam()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $sources = SourceType::all();
        $programs = $this->programRepo->get_programs_for_budget();

        $areas = $this->areaRepo->getAllArea();
        $expenseHeads = $this->expenseHeadRepo->get_expense_head_by_ledger_type();
        $mediums = Medium::all();
        $units = Unit::all();
        return view('frontend.budget.rakamantarParam', compact('programs', 'areas', 'expenseHeads', 'sources', 'mediums', 'fiscalYear', 'units','fiscalYears'));


    }

    public function updateRakamantar(Request $request, $activityId)
    {
        $datas = $request->all();
//      function budget_details भए पनि  data Budget बाटै आको छ। पछि मिलाउने गरि
        $budget = $this->budgetRepo->get_budget_details_by_id($activityId);
        $old_total_budget = $budget->total_budget;

        $expenseHead = ExpenseHead::where('id', $budget->expense_head_id)->where('status', '=', 1)->first();
        $rakamantar_type = 0;
        $difference = 0;
        if ($old_total_budget != $datas['total_budget']) {
            if ($old_total_budget < $datas['total_budget']) {
                $rakamantar_type = 3; // thap
                $difference = (float)$datas['total_budget'] - (float)$old_total_budget;
            }
            if ($old_total_budget > $datas['total_budget']) {
                $rakamantar_type = 4; // Ghat
                $difference = (float)$old_total_budget - (float)$datas['total_budget'];
            }
            $budget->total_budget = $datas['total_budget'];
            $budget->first_quarter_unit = $datas['first_quarter'];
            $budget->second_quarter_unit = $datas['second_quarter'];
            $budget->third_quarter_unit = $datas['third_quarter'];
            $budget->first_quarter_budget = $datas['first_total_budget'];
            $budget->second_quarter_budget = $datas['second_total_budget'];
            $budget->third_quarter_budget = $datas['third_total_budget'];
            $budget->save();

//        Budget Details
            $budgetDetail = $this->budgetRepo->getBudgetDetailByBudgetId($activityId);
            if ($budget->is_contenjency == 1) {
                if ($budgetDetail->count() > 1) {
                    foreach ($budgetDetail as $index => $detail) {
                        $total_budget = $datas['total_budget'] * ($budget->contenjency * 0.01);
                        if (++$index == 1) {
                            $remain = 100 - $budget->contenjency;
                            $total_budget = $datas['total_budget'] * ($remain * 0.01);
                        }
                        $detail->total_budget = $total_budget;
                        if($datas['first_total_budget'] != null){
                            $detail->first_quarter_budget = $total_budget;
                        }
                        if($datas['second_total_budget'] != null){
                            $detail->second_quarter_budget = $total_budget;

                        }
                        if($datas['second_total_budget'] != null){
                            $detail->third_quarter_budget = $total_budget;
                        }
                        $detail->save();
                        $activityReports = ActivityReport::where('activity_id', $detail->id)->first();
                        if ($activityReports) {
                            $activityReports->budget = $total_budget;
                            $activityReports->remain = (float)$datas['total_budget'] - (float)$activityReports->expense;
                            $activityReports->save();
                        }
                        $oldActivityReportDetails = ActivityReportDetail::where('activity_report_id',$activityReports->id)->latest();

                        $activityReportsDetail = new ActivityReportDetail();
                        $activityReportsDetail->activity_report_id = $activityReports->id;
                        $activityReportsDetail->activity_id = $detail->id;

                        if($oldActivityReportDetails->budget < $total_budget){

                            $activityReportsDetail->budget = $total_budget - $oldActivityReportDetails->budget;
                        } else {
                            $activityReportsDetail->budget = $oldActivityReportDetails->budget - $total_budget;

                        }
                        $activityReportsDetail->expense_head = $expenseHead->expense_head_code;
                        $activityReportsDetail->akhtiyari_type = $rakamantar_type;
                        $activityReportsDetail->fiscal_year = $datas['fiscal_year'];
                        $activityReportsDetail->save();
                    }
                }
            }
            else {

                foreach ($budgetDetail as $detail) {
                    $detail->total_budget = $datas['total_budget'];
                    $detail->first_quarter_budget = $datas['first_total_budget'];
                    $detail->second_quarter_budget = $datas['second_total_budget'];
                    $detail->third_quarter_budget = $datas['third_total_budget'];
                    $detail->save();

                    $activityReports = ActivityReport::where('activity_id', $detail->id)->first();
                    if ($activityReports) {
                        $activityReports->budget = $datas['total_budget'];
                        $activityReports->remain = (float)$datas['total_budget'] - (float)$activityReports->expense;
                        $activityReports->save();
                    }

                    $activityReportsDetail = new ActivityReportDetail();
                    $activityReportsDetail->activity_report_id = $activityReports->id;
                    $activityReportsDetail->activity_id = $detail->id;
                    $activityReportsDetail->budget = $difference;
                    $activityReportsDetail->expense_head = $expenseHead->expense_head_code;
                    $activityReportsDetail->akhtiyari_type = $rakamantar_type;
                    $activityReportsDetail->fiscal_year = $datas['fiscal_year'];
                    $activityReportsDetail->save();
                }
            }
        }
        return back();
    }


  public  function akhtiyariUpdate(Request $request, $id)
    {

        $updateAKhtiyari = $this->budgetRepo->updateAkhtiyari($request->all(), $id);
        return redirect(route('akhtiyari.param'));

    }

   public function akhtiyariDelete($activity_id)
    {

        $deleteAkhtiyari = $this->budgetRepo->deleteAkhtiyariById($activity_id);
        return json_encode($deleteAkhtiyari);


    }

   public function delete_activity($id)
    {
        $delete_activity = $this->budgetRepo->delete_activity_by_id($id);
        return json_encode($delete_activity);
    }

    public function activityProgress(){

        $activityProgress = $this->activityProgressRepo->getProgressByOffice(Auth::user()->office->id);
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_programs_for_budget();
        return view('frontend.activityProgress.activityProgress',compact('activityProgress','fiscalYear','programs','fiscalYears'));
    }
   public function activityProgressCreate(){

       $programs = $this->programRepo->get_programs_for_budget();
       $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
       $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
       return view('frontend.activityProgress.activityProgressCreate',compact('programs','fiscalYear','fiscalYears'));
    }

    public function activityProgressStore(Request $request){
        $requestData = $request->all();
        $this->activityProgressRepo->store($requestData);
        return redirect()->back();
    }

    public function activityProgressEdit($activityProgressId){
        $programs = $this->programRepo->get_programs_for_budget();
        $progressReport = $this->activityProgressRepo->findById($activityProgressId);
        return view('frontend.activityProgress.activityProgressEdit',compact('progressReport','programs'));
    }

    public function activityProgressUpdate(Request $request,$activityProgressId){
        $requestData = $request->all();
        $this->activityProgressRepo->update($requestData,$activityProgressId);
        $activityProgress = $this->activityProgressRepo->getProgressByOffice(Auth::user()->office->id);
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_programs_for_budget();
        return redirect()->route('activity.progress');

    }



}
