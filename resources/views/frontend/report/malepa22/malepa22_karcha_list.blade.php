  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <table width="99%" >
        <tr>
          <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
          <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
            सभा @endif</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
        </tr>
        <tr>
          <td colspan="6"
              style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">
            @if(Auth::user()->office->department)
              {{Auth::user()->office->department->name}}
            @endif
          </td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}, , {{Auth::user()->office->district->name}}</td>
        </tr>
        <tr>
          <td colspan="4" style="padding-left:647px;">
            <b>बैङ्क नगदी किताब</b>
          </td>

          <td style="text-align: right;">म.ले.प.फा.नं. ५</td>
        </tr>
        <tr>
          <td>
            <br>
            <br>

          </td>
        </tr>
        <tr>
          <td>आर्थीक वर्ष : २०७६/७७</td>
          <td >महिना :</td>
          <td>बजेट उप शीर्षक : </td>
        </tr>

      </table>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table" width="100%" border="1">
            <thead>
            <tr>
              <th rowspan="2">सि.न.</th>
              <th rowspan="2">खर्च शिर्षक</th>
              <th rowspan="2">शीर्षक</th>
              <th rowspan="2">डेबिट रकम</th>
              <th colspan="2">क्रेडिट रकम</th>
              <th colspan="4">कैफियत</th>

            </tr>

            </thead>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
