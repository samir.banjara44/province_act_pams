<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetentionRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retention_record', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('office_id');
            $table->integer('fiscal_year_id');
            $table->integer('retention_category_id');
            $table->string('purpose');
            $table->integer('information_number');
            $table->dateTime('information_publish_date');
            $table->dateTime('information_last_date');
            $table->integer('budget_sub_head_id')->nullable();
            $table->integer('month');
            $table->integer('activity_id')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retention_record');
    }
}
