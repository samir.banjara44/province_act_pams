<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">


<style>
    table{
        border-collapse: collapse;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table  width="99%" style="font-size:13px">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                {{--          <td colspan="6" style="text-align: center"><b>प्रदेश सरकार</b></td>--}}
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
            </tr>
            @if(Auth::user()->office->department)
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->department->name}}</td>
                </tr>
            @endif
            <tr>
{{--                <td colspan="6" style="text-align: center"><b>{{$office[0]->name}}</b></td>--}}
            </tr>
            <tr>
                <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>स्रोत र खाता अनुसार रिपोर्ट</b><br>
                    </div>
                    <div style="float: right; margin-top: -20px">

                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                    <br>

                </td>
            </tr>
            <tr>
                <td style="text-align: right">आर्थीक वर्ष : <span class="">२०७६/७७</span></td>

            </tr>

        </table>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table id="ledger_source_wise_report_table"  width="99%" border="1" style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr style="font-size: 12px">
                        <th rowspan="3" width="67px">सि.नं.</th>
                        <th rowspan="3" style="width: 14%;">कार्यालय</th>
                        <th colspan="6">बजेट</th>
                        <th rowspan="3">कुल बजेट</th>
                        <th colspan="6" width="237px">खर्च </th>
                        <th rowspan="3">कुल खर्च</th>
                        <th rowspan="3">बजेट बांकी</th>
                        <th rowspan="3">कैफियत </th>
                    </tr>

                    <tr>
                        <th colspan="2">प्रदेश सरकार</th>
                        <th rowspan="2">जम्मा </th>
                        <th colspan="2">सघिंय सरकार</th>
                        <th rowspan="2">जम्मा </th>
                        <th colspan="2">प्रदेश सरकार</th>
                        <th rowspan="2">जम्मा </th>
                        <th colspan="2">सघिंय सरकार</th>
                        <th rowspan="2">जम्मा </th>
                    </tr>
                    <tr>

                        <td style="text-align: center">चालु</td>
                        <td style="text-align: center">पुंजी</td>
                        <td style="text-align: center">चालु</td>
                        <td style="text-align: center">पुंजी</td>
                        <td style="text-align: center">चालु</td>
                        <td style="text-align: center">पुंजी</td>
                        <td style="text-align: center">चालु</td>
                        <td style="text-align: center">पुंजी</td>
                    </tr>

                    </thead>
                    <tbody>
                    @foreach($officeLists as $index=>$office)
                    <tr style="background-color: white">
                        <td class="kalimati" style="text-align: center">{{++$index}}</td>
                        <td>{{$office->name}} , {{$office->district->name}}</td>

                        <td class="kalimati" style="text-align: right">{{$office->getBudgetProvinceChalu($office->id)}}</td>
                        <td class="kalimati" style="text-align: right">{{$office->getBudgetProvincePuji($office->id)}}</td>
                        <td class="kalimati" style="text-align: right">{{$office->getBudgetProvinceTotal($office->id)}}</td>

                        <td class="kalimati" style="text-align: right">{{$office->getBudgetFederalChalu($office->id)}}</td>
                        <td class="kalimati" style="text-align: right">{{$office->getBudgetFederalPuji($office->id)}}</td>
                        <td class="kalimati" style="text-align: right">{{$office->getBudgetFederalTotal($office->id)}}</td>

                        <td class="kalimati" style="text-align: right">{{$office->getTotalBudget($office->id)}}</td>

                        <td class="kalimati" style="text-align: right">{{$office->getExpenseProvinceChalu($office->id)}} </td>
                        <td class="kalimati" style="text-align: right">{{$office->getExpenseProvincePuji($office->id)}}</td>
                        <td class="kalimati" style="text-align: right">{{$office->getExpenseProvinceTotal($office->id)}}</td>

                        <td class="kalimati" style="text-align: right">{{$office->getExpenseFederalChalu($office->id)}}</td>
                        <td class="kalimati" style="text-align: right">{{$office->getExpenseFederalPuji($office->id)}}</td>
                        <td class="kalimati" style="text-align: right">{{$office->getExpenseFederalTotal($office->id)}}</td>

                        <td class="kalimati" style="text-align: right">{{$office->getTotalExpense($office->id)}}</td>
                        <td class="kalimati" style="text-align: right">{{$office->remainBudget($office->id)}}</td>
                        <td></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="2" style="text-align: right">जम्मा</td>
                        <td class="kalimati" style="text-align: right">{{$totalProvinceChaluBudget}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalProvincePujiBudget}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalProvinceBudget}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalFederalChaluBudget}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalFederalPujiBudget}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalFederalBudget}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalBudget}}</td>

                        <td class="kalimati" style="text-align: right">{{$totalProvinceChaluExp}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalProvincePujiExp}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalProvinceExp}}</td>

                        <td class="kalimati" style="text-align: right">{{$totalFederalChaluExp}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalFederalPujiExp}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalFederalExp}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalExp}}</td>
                        <td class="kalimati" style="text-align: right">{{$remainBudget}}</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

<script>
    $(document).ready(function () {

        let trs = $('#ledger_source_wise_report_table tbody').find('tr');

    })
</script>

<script>
    let changeToNepali = function (text) {
        // alert(text);
        textTrim = $.trim(text)
        let numbers = textTrim.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (!isNaN(value) && value != ' ') {
                    if (value == 1)
                        nepaliNo += "१";
                    else if (value == 2)

                        nepaliNo += "२";
                    else if (value == 3)

                        nepaliNo += "३";
                    else if (value == 4)

                        nepaliNo += "४";
                    else if (value == 5)

                        nepaliNo += "५";
                    else if (value == 6)

                        nepaliNo += "६";
                    else if (value == 7)

                        nepaliNo += "७";
                    else if (value == 8)

                        nepaliNo += "८";
                    else if (value == 9)

                        nepaliNo += "९";
                    else if (value == 0)

                        nepaliNo += "०";
                    else if (value == ',')

                        nepaliNo += ",";
                    else if (value == '.')

                        nepaliNo += ".";
                    else if (value == '/')

                        nepaliNo += "/";

                }
            }
        });
        console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });




</script>
