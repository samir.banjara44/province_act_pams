<?php

namespace App\Http\Controllers\Frontend;

use App\helpers\FiscalYearHelper;
use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\FiscalYear;
use App\Repositories\BankRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class FiscalYearController extends Controller
{

    private $bankRepo;

    public function __construct(BankRepository $bankRepo)
    {
        $this->middleware('auth');
        $this->bankRepo = $bankRepo;
        $this->fiscalYearHelper = new FiscalYearHelper();

    }

    public function index($officeId)
    {
        $fiscalYears = FiscalYear::all();
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_year();
        return view('frontend.fiscal_year.index', compact('fiscalYears'));
    }

    public function store(Request $request)
    {

        try {
            $bank = $this->bankRepo->create($request->all());
            $requestName = $request->name;
            $message = "Created " . $requestName . " Successfully";
            return redirect(route('bank'))->with('success', $message);
        } catch (\Exception $e) {
            $message = "Can not add to table. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }
    }
}
