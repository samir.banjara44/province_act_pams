<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Samuha extends Model
{
    protected $table = 'samuhas';
    protected $guarded = [];
}
