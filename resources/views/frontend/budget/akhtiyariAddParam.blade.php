@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="background-color: white">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="text-align: center">
                <small>बजेट उपशिर्षक : {{$bdget_sub_heads->name}}</small><br>
                <small>बजेट उपशिर्षक न.: <span class="e-n-t-n-n">{{$bdget_sub_heads->program_code}}</span></small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td colspan="8" align="center"><a href="">नयाँ बजेट बिनियोजन वा रकमान्तर प्रविष्टि गर्ने</a></td>
                        </tr>
                        <tr>

                            <th>आ.व.</th>
                            <th>विवरण</th>
                            <th>मिति</th>
                            <th>रकम</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                        </tr>
{{--                        @foreach($offices as $office)--}}
{{--                            <tr>--}}
{{--                                <td>{{$office->name}}</td>--}}
{{--                                <td>{{$office->province->name}}</td>--}}
{{--                                <td>{{$office->mof->name}}</td>--}}
{{--                                <td>{{$office->ministry->name}}</td>--}}
{{--                                <td>{{$office->department->name}}</td>--}}
{{--                                <td>{{$office->status}}</td>--}}
{{--                                <td>--}}
{{--                                    <a type="button" href="" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')
    <script>
        let changeToNepali = function (text) {
            let numbers = text.split('');
            let nepaliNo = '';
            $.each(numbers, function (key, value) {
                if (value) {
                    if(value == 1)
                        nepaliNo+="१";
                    else if(value == 2)

                        nepaliNo+="२";
                    else if(value == 3)

                        nepaliNo+="३";
                    else if(value == 4)

                        nepaliNo+="४";
                    else if(value == 5)

                        nepaliNo+="५";
                    else if(value == 6)

                        nepaliNo+="६";
                    else if(value == 7)

                        nepaliNo+="७";
                    else if(value == 8)

                        nepaliNo+="८";
                    else if(value == 9)

                        nepaliNo+="९";
                    else if(value == 0)

                        nepaliNo+="०";
                    else if(value == ',')

                        nepaliNo+=",";
                    else if(value == '.')

                        nepaliNo+=".";
                    else if(value == '/')

                        nepaliNo+="/";
                }
            });
            return nepaliNo;
        };

        // let change_all_to_nepali_number = function(){
        $('.e-n-t-n-n').each(function () {
            let nepaliNo = changeToNepali($(this).text());
            let nepaliVal = changeToNepali($(this).val());

            $(this).text(nepaliNo);
            $(this).val(nepaliVal);
        });
        // }



    </script>
@endsection