<?php

namespace App\Repositories;

use App\helpers\FiscalYearHelper;
use App\Models\ActivityReport;
use App\Models\ActivityReportDetail;
use App\Models\Bhuktani;
use App\Models\BhuktaniVoucher;
use App\Models\Voucher;
use Illuminate\Support\Facades\Auth;
use App\helpers\BsHelper;

class VoucherRepositoryEloquent implements VoucherRepository
{

    public function __construct()
    {
        $this->fiscalYearHelper = new FiscalYearHelper();
    }

    public function getAllVoucher()
    {
        return $areas = Voucher::all();
    }

    public function find($id)
    {
        return Voucher::findorfail($id);
    }

    public function createAlyaVOucher($attributes, $voucher_signature)
    {

        $voucher = new Voucher();
        $voucher->jv_number = $this->get_vouhcer_number_by_budget_sub_head_and_office_id(Auth::user()->office->id, $attributes['fiscal_year'], $attributes['budget_sub_head']);
        $voucher->office_id = Auth::user()->office->id;
        $voucher->budget_sub_head_id = $attributes['budget_sub_head'];
        $voucher->payement_amount = 0;
        $voucher->short_narration = "ग त आवको पेश्की अल्या गरियो";
        $voucher->long_narration = "ग त आवको पेश्की अल्या गरियो";
        $voucher->data_nepali = $attributes['date_nepali'];
        $voucher->user_id = Auth::user()->id;
        $voucher->status = 0;
        $date_eng = $attributes['date_roman'];
        $mymonth = (explode('-', $date_eng));
        $month = intval($mymonth[1]);
        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }
        $voucher->fiscal_year = $attributes['fiscal_year'];
        $date_array = explode('-', $attributes['date_roman']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
        $voucher->date_english = $data_ad_array['year'] . '-' . $data_ad_array['month'] . '-' . $data_ad_array['date'];
        $voucher->month = $finalmonth;
        if ($voucher_signature and $voucher_signature->karmachari_prepare_by) {
            $voucher->prepared_by = $voucher_signature->karmachari_prepare_by->id;
        }
        if ($voucher_signature and $voucher_signature->karmachari_submit_by) {
            $voucher->submitted_by = $voucher_signature->karmachari_submit_by->id;
        }
        if ($voucher_signature and $voucher_signature->karmachari_approved_by) {
            $voucher->approved_by = $voucher_signature->karmachari_approved_by->id;
        }
        $voucher->save();
        return $voucher->id;
    }
    public function create($attributes, $voucher_signature)
    {
        $voucher = new Voucher();
        $voucher->jv_number = $this->get_vouhcer_number_by_budget_sub_head_and_office_id(Auth::user()->office->id, $attributes['fiscal_year'], $attributes['budget_sub_head_id']);
        $voucher->office_id = Auth::user()->office->id;
        $voucher->budget_sub_head_id = $attributes['budget_sub_head_id'];
        $voucher->payement_amount = $attributes['paymentAmount'];
        $voucher->short_narration = $attributes['narration_short'];
        $voucher->long_narration = $attributes['narration_long'];
        $voucher->data_nepali = $attributes['date_nepali'];
        $voucher->user_id = Auth::user()->id;
        $voucher->status = 0;
        $date_eng = $attributes['date_english'];

        $mymonth = (explode('-', $date_eng));
        $month = intval($mymonth[1]);
        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }

        $voucher->fiscal_year = $attributes['fiscal_year'];

        $date_array = explode('-', $attributes['date_english']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
        $voucher->date_english = $data_ad_array['year'] . '-' . $data_ad_array['month'] . '-' . $data_ad_array['date'];
        $voucher->month = $finalmonth;

        if ($voucher_signature and $voucher_signature->karmachari_prepare_by) {
            $voucher->prepared_by = $voucher_signature->karmachari_prepare_by->id;
        }
        if ($voucher_signature and $voucher_signature->karmachari_submit_by) {
            $voucher->submitted_by = $voucher_signature->karmachari_submit_by->id;
        }
        if ($voucher_signature and $voucher_signature->karmachari_approved_by) {
            $voucher->approved_by = $voucher_signature->karmachari_approved_by->id;
        }
        $voucher->save();
        return $voucher->id;
    }

    public function createNikasaVoucher($attributes, $voucher_signature)
    {

        $voucher = new Voucher();
        $voucher->jv_number = $attributes['voucher_number'];
        $voucher->office_id = Auth::user()->office->id;
        $voucher->budget_sub_head_id = $attributes['budget_sub_head'];
        $voucher->payement_amount = $attributes['totalAmount'];
        $voucher->short_narration = "निकासा आम्दानी बाधिंयो।";
        $voucher->long_narration = "निकासा आम्दानी बाधिंयो।";
        $voucher->data_nepali = $attributes['date'];
        $voucher->user_id = Auth::user()->id;
        $voucher->status = 0;
        $mymonth = (explode('-', $attributes['date_english']));
        $month = intval($mymonth[1]);

        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }
        $voucher->fiscal_year = $attributes['fiscal_year'];


        $date_array = explode('-', $attributes['date_english']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
        $voucher->date_english = $data_ad_array['year'] . '-' . $data_ad_array['month'] . '-' . $data_ad_array['date'];
        $voucher->month = $finalmonth;

        if ($voucher_signature and $voucher_signature->karmachari_prepare_by) {

            $voucher->prepared_by = $voucher_signature->karmachari_prepare_by->id;
        }
        if ($voucher_signature and $voucher_signature->karmachari_submit_by) {

            $voucher->submitted_by = $voucher_signature->karmachari_submit_by->id;
        }
        if ($voucher_signature and $voucher_signature->karmachari_approved_by) {

            $voucher->approved_by = $voucher_signature->karmachari_approved_by->id;
        }
        $voucher->save();
        return $voucher->id;
    }

    public function get_vouhcer_number_by_budget_sub_head_and_office_id($office_id, $fiscal_year, $budget_sub_head_id)
    {
        $voucher = Voucher::where('office_id', $office_id)
            ->where('fiscal_year', $fiscal_year)
            ->where('budget_sub_head_id', $budget_sub_head_id)
            ->orderBy('jv_number', 'desc')->first();
        if ($voucher) {
            $jvNumber = $voucher->jv_number;
        } else {
            $jvNumber = 0;
        }
        return $jvNumber + 1;
    }

    public function get_all_voucher_budget_sub_head_id($data)
    {
        $office_id = Auth::user()->office->id;
        return $voucherList = Voucher::where('office_id', $office_id)
            ->where('fiscal_year', $data['fiscal_year'])
            ->where('budget_sub_head_id', $data['budget_sub_head'])
            ->where('status', '=', 1)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function get_all_voucher_budget_sub_head_id_group_by_month($budget_sub_head, $fiscal_year)
    {
        $office_id = Auth::user()->office->id;
        return $voucherList = Voucher::where('office_id', $office_id)
            ->where('budget_sub_head_id', $budget_sub_head)
            ->where('fiscal_year', $fiscal_year)
            ->where('status', '=', 1)
            ->get();
    }

    public function getVoucherByStatusAndBudgetSubHead($fiscal_year, $budget_sub_head_id, $office_id)
    {
        $voucherLists = Voucher::where('office_id', $office_id)
            ->where('fiscal_year', $fiscal_year)
            ->where('budget_sub_head_id', $budget_sub_head_id)
            ->where('status', '=', 0)->get();

        $lastVoucher = Voucher::where('office_id', $office_id)
            ->where('fiscal_year', $fiscal_year)
            ->where('budget_sub_head_id', $budget_sub_head_id)
            ->orderBy('jv_number', 'desc')->first();

        $voucherLists->map(function ($item) use ($lastVoucher) {
            if ($item->id == $lastVoucher->id) {
                $item->IS_LAST = true;
            }
            return $item;
        });
        return $voucherLists;
    }

    public function set_voucher_approved($id)
    {

        $voucher = Voucher::findorfail($id);
        $voucher->status = 1;
        return $voucher->save();
    }

    public function updateVoucher($voucher_data, $voucher_signature)
    {
        $id = $voucher_data['id'];
        $voucher = Voucher::where('id', $id)->first();

        if (array_key_exists('date', $voucher_data)) {
            $voucher->data_nepali = $voucher_data['date_nepali'];
        }
        if (array_key_exists('payment_amount', $voucher_data)) {
            $voucher->payement_amount = $voucher_data['payment_amount'];
        }
        if (array_key_exists('narration_short', $voucher_data)) {
            $voucher->short_narration = $voucher_data['narration_short'];
        }
        if (array_key_exists('narration_long', $voucher_data)) {
            $voucher->long_narration = $voucher_data['narration_long'];
        }
        if (array_key_exists('date_nepali', $voucher_data)) {
            $voucher->data_nepali = $voucher_data['date_nepali'];
        }
        if (array_key_exists('date_nepali', $voucher_data)) {
            $voucher->data_nepali = $voucher_data['date_nepali'];
        }

        $date_eng = $voucher_data['date_english'];
        $myyearfirst = (substr($date_eng, 0, 2));
        $myyearlast = (substr($date_eng, 2, 2));
        $mymonth = (explode('-', $date_eng));
        $month = intval($mymonth[1]);

        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }

        $voucher->fiscal_year = $voucher_data['fiscal_year'];

        $date_array = explode('-', $voucher_data['date_english']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
        $voucher->date_english = $data_ad_array['year'] . '-' . $data_ad_array['month'] . '-' . $data_ad_array['date'];
        $voucher->month = $finalmonth;

        if ($voucher_signature and $voucher_signature->karmachari_prepare_by) {

            $voucher->prepared_by = $voucher_signature->karmachari_prepare_by->id;
        }
        if ($voucher_signature and $voucher_signature->karmachari_submit_by) {

            $voucher->submitted_by = $voucher_signature->karmachari_submit_by->id;
        }
        if ($voucher_signature and $voucher_signature->karmachari_approved_by) {

            $voucher->approved_by = $voucher_signature->karmachari_approved_by->id;
        }

        return $voucher->save();
    }

    public function get_all_pre_month_voucher_by_budget_sub_head_id($budget_sub_head_id, $fiscalYear, $month)
    {
        $office_id = Auth::user()->office->id;
        return $voucherList = Voucher::where('office_id', $office_id)
            ->where('fiscal_year', $fiscalYear)
            ->where('month', $month)
            ->where('budget_sub_head_id', $budget_sub_head_id)->get();
    }


    public function get_all_voucher_by_budget_sub_head_id($budget_sub_head, $fiscalYear, $month)
    {
        $office_id = Auth::user()->office->id;
        return $voucherList = Voucher::where('office_id', $office_id)
            ->where('budget_sub_head_id', $budget_sub_head)
            ->where('fiscal_year', $fiscalYear)
            ->where('month', $month)
            ->where('status', '=', 1)->get();
    }


    public function delete($voucher_id)
    {
        try {
            $voucher = $this->find($voucher_id);
            $voucherCollection = $voucher->details;
            if ($voucherCollection->count() > 0) {
                $data = [];
                $expense = $voucherCollection->where('dr_or_cr', '=', 1)->whereIn('ledger_type_id', [1, 13]);
                if ($expense->count() > 0) {
                    $pefa = $voucherCollection->where('dr_or_cr', 2)->where('ledger_type_id', 9);
                    if ($pefa->count() > 0) {
                        $overExp = $voucherCollection->contains(function ($value, $key) {
                            if ($value['dr_or_cr'] == 2 and $value['ledger_type_id'] == 3) {
                                return true;
                            }
                        });
                        $lessExp = $voucherCollection->contains(function ($value, $key) {
                            if ($value['dr_or_cr'] == 1 and $value['ledger_type_id'] == 3) {
                                return true;
                            }
                        });
                        if ($overExp && $lessExp) {
                            foreach ($voucherCollection as $detail) {
                                if ($detail['dr_or_cr'] == 1 and $detail['ledger_type_id'] == 3) {
                                    $data['income'] = $detail['dr_amount'];
                                }
                                if (($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 2)) {
                                    $data['expense'] = $detail['cr_amount'];
                                }

                                $activityReport = ActivityReport::where('activity_id', $detail['main_activity_id'])->first();
                                if ($activityReport->count() > 0) {
                                    if (array_key_exists('expense', $data)) {

                                        $total_expense = $activityReport->expense = $activityReport->expense - $data['expense'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                        $activityReport->save();

                                        $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $detail['main_activity_id'])->get();
                                        if ($activityReportDetails) {
                                            foreach ($activityReportDetails as $activityReportDetail) {
                                                $activityReportDetail->delete();
                                            }
                                        }
                                    }
                                    if (array_key_exists('income', $data)) {

                                        $total_expense = $activityReport->expense = $activityReport->expense + $data['income'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                        $activityReport->save();

                                        $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $detail['main_activity_id'])->get();
                                        if ($activityReportDetails) {
                                            foreach ($activityReportDetails as $activityReportDetail) {
                                                $activityReportDetail->delete();
                                            }
                                        }
                                    }
                                }
                            }
                        } elseif ($overExp and !$lessExp) {
                            foreach ($voucherCollection as $detail) {
                                //                                    over expense
                                if (($voucherCollection->where('dr_or_cr', 2) and $voucherCollection->where('ledger_type_id', 3))) {
                                    if (($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 2)) {

                                        $activityReport = ActivityReport::where('activity_id', $detail['main_activity_id'])->first();
                                        $total_expense = $activityReport->expense = $activityReport->expense - $detail['cr_amount'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                        $activityReport->save();

                                        $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $detail['main_activity_id'])->get();
                                        if ($activityReportDetails) {
                                            foreach ($activityReportDetails as $activityReportDetail) {
                                                $activityReportDetail->delete();
                                            }
                                        }
                                    }
                                }
                            }
                        } //  less expense
                        elseif ($lessExp and !$overExp) {
                            foreach ($voucherCollection as $detail) {

                                if ($detail['dr_or_cr'] == 1 and $detail['ledger_type_id'] == 3) {

                                    $activityReport = ActivityReport::where('activity_id', $detail['main_activity_id'])->first();
                                    $total_expense = $activityReport->expense = $activityReport->expense + $detail['dr_amount'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $detail['main_activity_id'])->get();
                                    if ($activityReportDetails) {
                                        foreach ($activityReportDetails as $activityReportDetail) {
                                            $activityReportDetail->delete();
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        $adjustExpense = $voucherCollection->where('dr_or_cr', '=', 2)->where('ledger_type_id', '=', 1);
                        if ($adjustExpense->count() > 0) {
                            foreach ($voucherCollection as $attribute) {
                                if ($attribute['dr_or_cr'] == 1 and $attribute['ledger_type_id'] == 1) {

                                    $activityReport = ActivityReport::where('activity_id', $attribute['main_activity_id'])->first();
                                    $total_expense = 0;
                                    if ($activityReport) {
                                        $activityReport->expense = $total_expense = $activityReport->expense - (float)$attribute['dr_amount'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                        $activityReport->save();

                                        $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $attribute['main_activity_id'])->get();
                                        if ($activityReportDetails) {
                                            foreach ($activityReportDetails as $activityReportDetail) {
                                                $activityReportDetail->delete();
                                            }
                                        }
                                    }
                                }
                                if ($attribute['dr_or_cr'] == 2 and $attribute['ledger_type_id'] == 1) {

                                    $activityReport = ActivityReport::where('activity_id', $attribute['main_activity_id'])->first();
                                    $total_expense = 0;
                                    if ($activityReport) {

                                        $activityReport->expense = $total_expense = $activityReport->expense + (float)$attribute['cr_amount'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                        $activityReport->save();

                                        $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $attribute['main_activity_id'])->get();
                                        if ($activityReportDetails) {
                                            foreach ($activityReportDetails as $activityReportDetail) {
                                                $activityReportDetail->delete();
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            foreach ($voucherCollection as $attribute) {

                                if (($attribute['dr_or_cr'] == 1 and $attribute['ledger_type_id'] == 1) || ($attribute['dr_or_cr'] == 1 and $attribute['ledger_type_id'] == 4)) {
                                    $data['expense'] = $attribute['dr_amount'];
                                    $activityReport = ActivityReport::where('activity_id', $attribute['main_activity_id'])->first();
                                    $total_expense = 0;
                                    if ($activityReport) {

                                        $currentExpense = $activityReport->expense;
                                        $activityReport->expense = $total_expense = (float)$currentExpense - $attribute['dr_amount'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                        $activityReport->save();

                                        $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $attribute['main_activity_id'])->get();
                                        if ($activityReportDetails) {
                                            foreach ($activityReportDetails as $activityReportDetail) {
                                                $activityReportDetail->delete();
                                            }
                                        }
                                    }
                                }
                                if ($attribute['dr_or_cr'] == 2 and $attribute['ledger_type_id'] == 1) {

                                    $activityReport = ActivityReport::where('activity_id', $attribute['main_activity_id'])->first();
                                    $total_expense = 0;
                                    if ($activityReport) {

                                        $activityReport->expense = $total_expense = $activityReport->expense + (float)$attribute['cr_amount'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                        $activityReport->save();

                                        $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $attribute['main_activity_id'])->get();
                                        if ($activityReportDetails) {
                                            foreach ($activityReportDetails as $activityReportDetail) {
                                                $activityReportDetail->delete();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $adjustPeski = $voucherCollection->where('dr_or_cr', '=', 2)->where('ledger_type_id', '=', 9);
                    if ($adjustPeski->count() > 0) { {
                            foreach ($voucherCollection as $attribute) {
                                if ($attribute['dr_or_cr'] == 1 and $attribute['ledger_type_id'] == 4) {
                                    $data['expense'] = $attribute['dr_amount'];
                                    $activityReport = ActivityReport::where('activity_id', $attribute['main_activity_id'])->first();
                                    $total_expense = 0;
                                    if ($activityReport) {
                                        $currentExpense = $activityReport->expense;
                                        $activityReport->expense = $total_expense = (float)$currentExpense - $attribute['dr_amount'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                        $activityReport->save();

                                        $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $attribute['main_activity_id'])->get();
                                        if ($activityReportDetails) {
                                            foreach ($activityReportDetails as $activityReportDetail) {
                                                $activityReportDetail->delete();
                                            }
                                        }
                                    }
                                }
                                if ($attribute['dr_or_cr'] == 2 and $attribute['ledger_type_id'] == 9) {
                                    $activityReport = ActivityReport::where('activity_id', $attribute['main_activity_id'])->first();
                                    $total_expense = 0;
                                    if ($activityReport) {
                                        $activityReport->expense = $total_expense = $activityReport->expense + (float)$attribute['cr_amount'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                        $activityReport->save();

                                        $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $attribute['main_activity_id'])->get();
                                        if ($activityReportDetails) {
                                            foreach ($activityReportDetails as $activityReportDetail) {
                                                $activityReportDetail->delete();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        $lastYearPefa = $voucherCollection->where('dr_or_cr', '=', 2)->where('ledger_type_id', '=', 12);
                        if ($lastYearPefa->count() > 0) { {
                                $overExp = $voucherCollection->contains(function ($value, $key) {
                                    if ($value['dr_or_cr'] == 2 and $value['ledger_type_id'] == 3) {
                                        return true;
                                    }
                                });
                                $lessExp = $voucherCollection->contains(function ($value, $key) {
                                    if ($value['dr_or_cr'] == 1 and $value['ledger_type_id'] == 3) {
                                        return true;
                                    }
                                });
                                if ($overExp && $lessExp) {
                                    foreach ($voucherCollection as $detail) {
                                        if ($detail['dr_or_cr'] == 1 and $detail['ledger_type_id'] == 3) {
                                            $data['income'] = $detail['dr_amount'];
                                        }
                                        if (($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 2)) {

                                            $data['expense'] = $detail['cr_amount'];
                                        }

                                        $activityReport = ActivityReport::where('activity_id', $detail['main_activity_id'])->first();
                                        if ($activityReport->count() > 0) {
                                            if (array_key_exists('expense', $data)) {

                                                $total_expense = $activityReport->expense = $activityReport->expense - $data['expense'];
                                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                                $activityReport->save();

                                                $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $detail['main_activity_id'])->get();
                                                if ($activityReportDetails) {
                                                    foreach ($activityReportDetails as $activityReportDetail) {
                                                        $activityReportDetail->delete();
                                                    }
                                                }
                                            }
                                            if (array_key_exists('income', $data)) {
                                                $total_expense = $activityReport->expense = $activityReport->expense + $data['income'];
                                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                                $activityReport->save();

                                                $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $detail['main_activity_id'])->get();
                                                if ($activityReportDetails) {
                                                    foreach ($activityReportDetails as $activityReportDetail) {
                                                        $activityReportDetail->delete();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } elseif ($overExp and !$lessExp) {
                                    foreach ($voucherCollection as $detail) {
                                        // over expense
                                        if (($voucherCollection->where('dr_or_cr', 2) and $voucherCollection->where('ledger_type_id', 3))) {
                                            if (($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 2)) {

                                                $activityReport = ActivityReport::where('activity_id', $detail['main_activity_id'])->first();
                                                if ($activityReport) {
                                                    $total_expense = $activityReport->expense = $activityReport->expense - $detail['cr_amount'];
                                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                                    $activityReport->save();
                                                }
                                                $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $detail['main_activity_id'])->get();
                                                if ($activityReportDetails) {
                                                    foreach ($activityReportDetails as $activityReportDetail) {
                                                        $activityReportDetail->delete();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } //  less expense
                                elseif ($lessExp and !$overExp) {
                                    foreach ($voucherCollection as $detail) {

                                        if ($detail['dr_or_cr'] == 1 and $detail['ledger_type_id'] == 3) {

                                            $activityReport = ActivityReport::where('activity_id', $detail['main_activity_id'])->first();
                                            $total_expense = $activityReport->expense = $activityReport->expense + $detail['dr_amount'];
                                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                            $activityReport->save();

                                            $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $detail['main_activity_id'])->get();
                                            if ($activityReportDetails) {
                                                foreach ($activityReportDetails as $activityReportDetail) {
                                                    $activityReportDetail->delete();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $peski = $voucherCollection->where('dr_or_cr', '=', 1)->where('ledger_type_id', '=', 4);
                            if ($peski->count() > 0) {
                                foreach ($voucherCollection as $attribute) {
                                    if ($attribute['dr_or_cr'] == 1 and $attribute['ledger_type_id'] == 4) {
                                        $data['expense'] = $attribute['dr_amount'];
                                        $activityReport = ActivityReport::where('activity_id', $attribute['main_activity_id'])->first();
                                        $total_expense = 0;
                                        if ($activityReport) {
                                            $currentExpense = $activityReport->expense;
                                            $activityReport->expense = $total_expense = (float)$currentExpense - $attribute['dr_amount'];
                                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                            $activityReport->save();

                                            $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $attribute['main_activity_id'])->get();
                                            if ($activityReportDetails) {
                                                foreach ($activityReportDetails as $activityReportDetail) {
                                                    $activityReportDetail->delete();
                                                }
                                            }
                                        }
                                    }
                                    if ($attribute['dr_or_cr'] == 2 and $attribute['ledger_type_id'] == 1) {

                                        $activityReport = ActivityReport::where('activity_id', $attribute['main_activity_id'])->first();
                                        $total_expense = 0;
                                        if ($activityReport) {

                                            $activityReport->expense = $total_expense = $activityReport->expense + (float)$attribute['cr_amount'];
                                            $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                            $activityReport->save();

                                            $activityReportDetails = ActivityReportDetail::where('voucher_id', $voucher_id)->where('activity_id', $attribute['main_activity_id'])->get();
                                            if ($activityReportDetails) {
                                                foreach ($activityReportDetails as $activityReportDetail) {
                                                    $activityReportDetail->delete();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $nikasa = $voucherCollection->where('dr_or_cr', 2)->where('ledger_type_id', 8);
                if ($nikasa->count() > 0) {
                    $bhuktaniVoucher = BhuktaniVoucher::where('voucher_id', $voucher_id)->first();
                    if ($bhuktaniVoucher) {
                        $bhuktaniIds = json_decode($bhuktaniVoucher->bhuktani_id);
                        if ($bhuktaniIds) {
                            foreach ($bhuktaniIds as $bhuktaniId) {
                                if ($bhuktaniId) {
                                    $bhuktani = Bhuktani::where('id', $bhuktaniId)->get();
                                    if ($bhuktani->count() > 0) {
                                        foreach ($bhuktani as $data)
                                            $data->is_contenjency = 0;
                                        $data->save();
                                    }
                                }
                            }
                        }
                    }
                }

                $voucher->details()->delete();
                $voucher->preBhuktani()->delete();
                $voucher->delete();
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            //            return back()->withError($e->getMessage())->withInput();
            return $e->getMessage();
        }
    }

    public function change_status_by_budget_sub_head_and_office_id($data)
    {
        $vouhcer = Voucher::where('office_id', $data['office_id'])
            ->where('fiscal_year', $data['fiscal_year'])
            ->where('budget_sub_head_id', $data['budget_sub_head'])
            ->where('jv_number', $data['voucher_number'])
            ->first();
        if ($vouhcer) {
            $vouhcer->status = 0;
            $vouhcer->save();
            return $vouhcer->id;
        } else {
            return false;
        }
    }
}
