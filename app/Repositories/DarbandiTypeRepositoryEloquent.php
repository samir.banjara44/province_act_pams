<?php
namespace App\Repositories;

use App\Models\DarbandiType;
use Illuminate\Support\Facades\Auth;

class DarbandiTypeRepositoryEloquent implements DarbandiTypeRepository
{
    public function create($attributes) {
        $darbanditype = new DarbandiType();
        $darbanditype->name = $attributes['name'];
        $darbanditype->status = 1;
        $darbanditype->save();
        return $darbanditype;
    }

    public function get_all_darbanditypes()
    {
        return DarbandiType::all();
    }
}