@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>

                <small>दर्ता गर्ने </small>
            </h1>
            <ul class="breadcrumb">
                <li><a type="button" href="{{route('darta')}}" class="btn btn-sm btn-primary"><i
                                class="fa fa-pencil"></i> सुची हेर्ने</a></li>
            </ul>
        </section>

        <!-- FORM section start -->
        <section class="formSection displayNone" id="showThisForm">
            <div class="row justify-content-md-center">
                <div class="col-md-8 form-bg col-md-offset-2">
                    <form action="{{route('darta.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="name">मिति</label>
                                <input type="text" class="form-control" name="date" id="date" value="" required
                                       style="height: 32px;">
                            </div>
                            <div class="col-md-6">
                                <label for="name">from office name</label>
                                <input type="text" class="form-control" id="name" placeholder="From office name"
                                       name="name" required>
                            </div>
                            <div class="col-md-6">
                                <label for="address">from office address</label>
                                <input type="text" class="form-control" id="address" placeholder="From Office Address"
                                       name="address" required>
                            </div>
                            <div class="col-md-6">
                                <label for="person">from office person</label>
                                <input type="text" class="form-control" id="person"
                                       placeholder="From office Person Name" name="person" required>
                            </div>
                            <div class="col-md-6">
                                <label for="purpose">Purpose</label>
                                <input type="text" class="form-control" id="purpose" placeholder="Purpose"
                                       name="purpose" required>
                            </div>
                            <div class="form-group">
                                <label for="branch">सम्बन्धित शाखा</label>
                                <select name="branch" required class="branch" style="width:100%;" id="branch">
                                    <option>Select Branch:</option>
                                    @foreach($branch as $branch)
                                    <option value="{{ $branch->name_eng }}">{{$branch->name_eng}}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="col-md-6">
                                <label for="date">Prepared_date</label>
                                <input type="text" class="form-control" id="prepared_date" placeholder="Prepared_Date"
                                       name="prepared_date" required>
                            </div>
                            <div class="col-md-6">
                                <label for="date">Received_date</label>
                                <input type="text" class="form-control" id="received_date" placeholder="Received_Date"
                                       name="received_date" required>
                            </div>
                            <div class="col-md-6">
                                <label for="remarks">Remarks</label>
                                <textarea class="form-control" id="remarks" placeholder="Remarks" name="remarks"
                                          required></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2 col-md-offset-5">
                                <button type="button" class="btn btn-primary btn-show-form btn-block" onclick="saveDarta()"
                                        style="margin-top: 18px;">थप
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    <!-- FORM section ends -->

@section('scripts')

    {{--  Date picker--}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });
        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);

    </script>

@endsection