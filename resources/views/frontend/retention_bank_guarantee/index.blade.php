
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        बैङ्क ग्यारेन्टी
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('retention.bank.guarantee.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Create</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>क्र.सं.</th>
              <th>बैक्ङ ग्यारेन्टी न.</th>
              <th>रकम</th>
              <th>प.स/ चलानी नम्बर</th>
              <th>सुचना प्रकाशित मिति</th>
              <th>सुचना अन्तिम मिति</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              @foreach($bank_guarantees as $index=>$bank_guarantee)
                <tr>
                  <td>{{++$index}}</td>
                  <td>{{$bank_guarantee->retention_bank_guarantee_number}}</td>
                  <td>{{$bank_guarantee->amount}}</td>
                  <td>{{$bank_guarantee->reference_number}}</td>
                  <td>{{$bank_guarantee->guarantee_start_date}}</td>
                  <td>{{$bank_guarantee->guarantee_end_date}}</td>
                  <td>
                  <a type="button" href="{{route('retention.bank.guarantee.edit', $bank_guarantee->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection