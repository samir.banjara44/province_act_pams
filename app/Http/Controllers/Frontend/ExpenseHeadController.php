<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ExpenseHead;
use App\Models\Area;
use App\Models\SubArea;
use App\Repositories\ExpenseHeadRepository;
use App\Repositories\MainProgramRepository;
use App\Repositories\AreaRepository;
use App\Repositories\SubAreaRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExpenseHeadController extends Controller
{


    private $expenseHeadRepo;

    public function __construct(

        ExpenseHeadRepository $expenseHeadRepo
    )
    {
        $this->middleware('auth');

        $this->expenseHeadRepo = $expenseHeadRepo;
    }

    public function get_expense_head_by_activity_id(Request $request, $activity_id){
        $expenseHeads = $this->expenseHeadRepo->get_by_activity_id($activity_id);
        return json_encode($expenseHeads);
    }

    public function get_expense_head__by_ledger_type(Request $request, $ledger_type){

        $expenseHeads = $this->expenseHeadRepo->get_by_ledget_type($ledger_type);
        return json_encode($expenseHeads);
    }

    public function get_expense_head_by_only_one(){

        $expense_heads = $this->expenseHeadRepo->get_expense_head_by_ledger_type();
        return json_encode($expense_heads);

    }

    public function get_used_expense_head($budget_sub_head_id){

        $expense_heads = $this->expenseHeadRepo->get_expense_head_by_budget_sub_head($budget_sub_head_id);
        return json_encode($expense_heads);
    }



}
