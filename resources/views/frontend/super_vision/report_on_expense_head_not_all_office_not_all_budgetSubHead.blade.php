<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">
<link href="{{ asset('css/nepali-datepicker.css') }}" rel="stylesheet">
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/nepali-datepicker.js') }}"></script>


<style>
    table {
        border-collapse: collapse;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="font-size:13px">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                {{--                <td colspan="6" style="text-align: center"><b>प्रदेश सरकार</b></td>--}}
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
            </tr>
            @if(Auth::user()->office->department)
                <tr>
                    <td colspan="6" style="text-align: center">
                        @if(Auth::user()->office->department)
                            {{Auth::user()->office->department->name}}</td>
                    @endif
                </tr>
            @endif
            <tr>
                                <td colspan="6" style="text-align: center"><b>{{$office[0]->name}}</b></td>
            </tr>
            <tr>
                {{--                <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>--}}
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>दैनिक खर्च, शिर्षक अनुसार समरी</b><br>
{{--                        <b><span class="e-n-t-n-n">{{$year}}</span> साल {{$month_name}} महिना</b>--}}
                    </div>
                    <div style="float: right; margin-top: -20px">

                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                    <br>

                </td>
            </tr>
            <tr>
                                <td class="kalimati">बजेट उप शीर्षक न. : <b>{{$program->program_code}}</b><br>
                                    बजेट उप शीर्षक नाम: <b>{{$program->name}}</b></td>
                                <td style="text-align: right">आर्थीक वर्ष : <span class="e-n-t-n-n">{{$fiscalYear}}</span><br>
                                    मिति : <span id="date"></span>

                                </td>

            </tr>

        </table>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table" width="99%" border="1" style="background-color:#dbdbdb; font-size: 12px"
                       id="fatwari_table">
                    <thead>
                    <tr>

                        <th>खर्च/वित्तिय संकेत नं</th>
                        <th>खर्च/वित्तिय संकेतको नाम</th>
                        <th>अन्तिम बजेट</th>
                        <th>कुल निकासा</th>
                        <th>हिजो सम्मको खर्च</th>
                        <th>आजको खर्च</th>
                        <th>आज सम्मको खर्च</th>
                        <th>पेश्की</th>
                        <th>पेश्की बाहेक खर्च रकम</th>
                        <th>बाँकी बजेट</th>
                    </tr>

                    </thead>
                    <tbody>
                    @foreach($voucherExpenseHeads as $index=>$voucherExpenseHead)
                        <tr style="background: white;">

                            <td class="kalimati" style="text-align: center">{{ $voucherExpenseHead->expense_head}}</td>
                            <td class="kalimati" >{{ $voucherExpenseHead->expense_head_by_id->expense_head_sirsak}}</td>
                            <td class="kalimati" style="text-align: right">{{ $voucherExpenseHead->getLastBudgetOfOfficeByBudgetSubHead($datas['officeId'],$datas['budgetSUbHead'],$voucherExpenseHead->expense_head_id)}}</td>
                            <td class="kalimati" style="text-align: right">{{ $voucherExpenseHead->getTotalExpenseOfOfficeByBudgetSubHead($datas['officeId'],$datas['budgetSUbHead'],$voucherExpenseHead->expense_head_id)}}</td>
                            <td class="kalimati" style="text-align: right">{{ $voucherExpenseHead->getTotalExpenseUpToYesterdayOfOfficeByBudgetSubHead($datas['officeId'],$datas['budgetSUbHead'],$voucherExpenseHead->expense_head_id)}}</td>
                            <td class="kalimati" style="text-align: right">{{ $voucherExpenseHead->getTotalExpenseTodayOfOfficeByBudgetSubHead($datas['officeId'],$datas['budgetSUbHead'],$voucherExpenseHead->expense_head_id)}}</td>
                            <td class="kalimati" style="text-align: right">{{ $voucherExpenseHead->getTotalExpenseUpToThisDayOfOfficeByBudgetSubHead($datas['officeId'],$datas['budgetSUbHead'],$voucherExpenseHead->expense_head_id)}}</td>
                            <td class="kalimati" style="text-align: right">{{ $voucherExpenseHead->getTotalAdvanceOfOfficeByBudgetSubHead($datas['officeId'],$datas['budgetSUbHead'],$voucherExpenseHead->expense_head_id)}}</td>
                            <td class="kalimati" style="text-align: right">{{ $voucherExpenseHead->expenseWithOutAdvanceOfOfficeByBudgetSubHead($datas['officeId'],$datas['budgetSUbHead'],$voucherExpenseHead->expense_head_id)}}</td>
                            <td class="kalimati" style="text-align: right">{{ $voucherExpenseHead->getRemainBudgetOfOfficeByBudgetSubHead($datas['officeId'],$datas['budgetSUbHead'],$voucherExpenseHead->expense_head_id)}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2" style="text-align: right">जम्मा</td>
                        <td class="kalimati" style="text-align: right">{{$totalFinalBudget}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalExpense}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalExpenseUpToYesterday}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalExpenseToday}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalExpenseUpToToday}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalAdvance}}</td>
                        <td class="kalimati" style="text-align: right">{{$expenseWithOutAdvance}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalremainBudget}}</td>

                    </tr>

                    </tbody>
                </table>
                {{--                <table width="99%" style="font-size: 13px">--}}

                {{--                    <tr>--}}
                {{--                        <td style="padding-left: 52px;">तयार गर्ने :</td>--}}
                {{--                        <td>पेश गर्ने :</td>--}}
                {{--                        <td>सदर गर्ने :</td>--}}
                {{--                    </tr>--}}
                {{--                    <tr>--}}
                {{--                        <td style="padding-left: 52px;">पद :</td>--}}
                {{--                        <td>पद :</td>--}}
                {{--                        <td>पद :</td>--}}
                {{--                    </tr>--}}
                {{--                    <tr>--}}
                {{--                        <td style="padding-left: 52px;">मिति :</td>--}}
                {{--                        <td>मिति :</td>--}}
                {{--                        <td>मिति :</td>--}}
                {{--                    </tr>--}}

                {{--                </table>--}}
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script>
    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '/')

                    nepaliNo += "/";
            }
        });
        // console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });


</script>

<script>
    $(document).ready(function () {
        let trs = $('#fatwari_table tbody').find('tr:not(.second_last,.last)');
        let total_budget = 0;
        let total_this_month_expense = 0;
        let total_up_to_prev_month_expense = 0;
        let total_up_to_this_month_expense = 0;
        let total_advance = 0;
        let total_exp_with_out_advance = 0;
        let total_remain_budget = 0;
        let expense_percentage = 0;
        // alert(trs.length);
        $.each(trs, function () {

            let total_budget_nep = $.trim($(this).find('td.total-budget').text());
            let total_this_month_expense_nep = $.trim($(this).find('td.this-month-exp').text());
            let total_up_to_prev_month_expense_nep = $.trim($(this).find('td.up-to-pre-month-exp').text());
            let total_up_to_this_month_expense_nep = $.trim($(this).find('td.up-to-this-month-exp').text());
            let advance_nep = $.trim($(this).find('td.advance').text());
            let exp_with_out_advance_nep = $.trim($(this).find('td.exp-with-out-advance').text());
            let remain_budget_nep = $.trim($(this).find('td.remain-budget').text());


            total_budget_roman = convertNepaliToEnglish(total_budget_nep);
            total_this_month_expense_roamn = convertNepaliToEnglish(total_this_month_expense_nep);

            total_up_to_prev_month_expense_roamn = convertNepaliToEnglish(total_up_to_prev_month_expense_nep);
            total_up_to_this_month_expense_roamn = convertNepaliToEnglish(total_up_to_this_month_expense_nep);
            advence_roamn = convertNepaliToEnglish(advance_nep);
            exp_with_out_advance_roman = convertNepaliToEnglish(exp_with_out_advance_nep);
            remain_budget_roman = convertNepaliToEnglish(remain_budget_nep);
            // alert(remain_budget_roman);

            total_budget = parseFloat(total_budget) + parseFloat(total_budget_roman);
            total_this_month_expense = parseFloat(total_this_month_expense) + parseFloat(total_this_month_expense_roamn);
            total_up_to_prev_month_expense = parseFloat(total_up_to_prev_month_expense) + parseFloat(total_up_to_prev_month_expense_roamn);
            total_up_to_this_month_expense = parseFloat(total_up_to_this_month_expense) + parseFloat(total_up_to_this_month_expense_roamn);
            total_advance = parseFloat(total_advance) + parseFloat(advence_roamn);
            total_exp_with_out_advance = parseFloat(total_exp_with_out_advance) + parseFloat(exp_with_out_advance_roman);
            total_remain_budget = parseFloat(total_remain_budget) + parseFloat(remain_budget_roman);
            expense_percentage = (parseFloat(total_up_to_this_month_expense) * 100) / parseFloat(total_budget)


        });


        $('#total_last_budget').text(changeToNepali(total_budget.toString()));
        $('#this_month_nikasa').text(changeToNepali(total_up_to_this_month_expense.toString()));
        $('#this_month_exp').text(changeToNepali(total_this_month_expense.toString()));
        $('#up_to_prev_month_exp').text(changeToNepali(total_up_to_prev_month_expense.toString()));
        $('#up_to_this_month_exp').text(changeToNepali(total_up_to_this_month_expense.toString()));
        $('#advance').text(changeToNepali(total_advance.toString()));
        $('#exp_with_out_advance').text(changeToNepali(total_exp_with_out_advance.toString()));
        $('#remain_budget').text(changeToNepali(total_remain_budget.toString()));
        $('#total_expense_percentage').text(changeToNepali(expense_percentage.toFixed(3)));

        // console.log(expense_percentage,'%');

        // alert(total_remain_budget);
    })
</script>

<script>
    function convertNepaliToEnglish(input) {
        // console.log(input);
        var charArray = input.split('');
        console.log('Test', charArray[0]);
        // charArray = charArray[0].split('');
        var engDate = '';
        $.each(charArray, function (key, value) {

            switch (value) {
                case '१':
                    engDate += '1'
                    break
                case '२':
                    engDate += '2'
                    break
                case '३':
                    engDate += '3'
                    break
                case '४':
                    engDate += '4'
                    break
                case '५':
                    engDate += '5'
                    break
                case '६':
                    engDate += '6'
                    break
                case '०':
                    engDate += '0'
                    break
                case '७':
                    engDate += '7'
                    break
                case '८':
                    engDate += '8'
                    break
                case '९':
                    engDate += '9'
                    break

                case '-':
                    engDate += '-'
                    break
                case '.':
                    engDate += '.'
                    break
            }
            // console.log(engDate)
        })

        return engDate

    }
</script>

{{--Date Picker --}}
<script>

    var currentDate = new Date();
    var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
    var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
    $("#date").text(formatedNepaliDate);
</script>