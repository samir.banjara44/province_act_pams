~
@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-check-square"></i> भौचर स्वीकृत गर्ने :: खोज </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary panel-form">
                    <div class="panel-body">
                        <form action="#" method="post">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>आर्थिक वर्ष : </label>
                                        <select name="fiscal_year" id="fiscal_year" class="form-control">
                                            @foreach($fiscalYears as $fy)
                                                <option value="{{$fy->id}}" @if($fy->id == $fiscalYear->id) selected @endif>{{$fy->year}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8">
                                        <label>बजेट उपशिर्षक :</label>
                                        <select name="budget_sub_head" id="budget_sub_head" class="form-control">
                                            <option value="0">..............</option>
                                            @foreach($programs as $program)
                                                <option value="{{$program->id}}">{{$program->name}}
                                                    | {{$program->program_code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <table id="voucher_list" class="table table-striped">
                <thead>
                <th class="text-center">क्र.सं.</th>
                <th class="text-center">बजेट उप शीर्षक</th>
                <th class="text-center">मिति</th>
                <th class="text-center">भौचर नं.</th>
                <th class="text-center">कारोबार रकम</th>
                <th class="text-center">कारोबारको व्यहोरा</th>
                <th class="text-center" colspan="2">कार्य</th>
                </thead>
                <tbody>
                <tr>
                    <td align="center" colspan="7">भौचरको लागि बजेट उपशिर्षक छान्नुहोस्</td>
                </tr>
                </tbody>
            </table>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function () {
            $('#fiscal_year').change(function () {
                $('#budget_sub_head').val('')
                $('#voucher_list tbody').html('')
            })
        })
    </script>
    <script>
        $(document).ready(function () {
            $('#budget_sub_head').change(function () {

                let fiscal_year = $('#fiscal_year').val();
                let budget_sub_head = $('#budget_sub_head :selected').val();
                let budget_sub_head_name = $('#budget_sub_head :selected').text();
                let url = '{{route('get_unapproved_voucher',['123','345'])}}';
                url = url.replace('123', fiscal_year);
                url = url.replace('345', budget_sub_head);

                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {
                        $datas = $.parseJSON(res);
                        let tr = '';
                        if ($datas.length > 0) {
                            let i = 1;
                            $.each($datas, function (key, value) {
                                let url = '{{route('voucher.view', '123')}}';
                                url = url.replace('123', this.id);
                                tr += "<tr>" +
                                    "<td>" +
                                    i +
                                    "</td>" +

                                    "<td class='activity' data-id=''>" +
                                    budget_sub_head_name +
                                    "</td>" +

                                    "<td class='byahora'>" +
                                    this.data_nepali +
                                    "</td>" +

                                    "<td class='details'>" +
                                    this.jv_number +
                                    "</td>" +

                                    "<td class='drAmount'>" +
                                    this.payement_amount +
                                    "</td>" +

                                    "<td class='drAmount'>" +
                                    this.short_narration +
                                    "</td>" +

                                    "<td>" +
                                    '<a href="#" class="voucher-accept" data-id="' + this.id + '" disabled="disabled">स्विकृत गर्ने</a> | <a href="' + url + '" target="_blank">हेर्ने</a>' +
                                    "</td>";
                                i = i + 1;
                            });
                            $('#voucher_list').find('tbody').last('tr').html(tr);

                        } else {
                            tr += "<tr>" +
                                "<td colspan='7' style='text-align: center'>" +
                                "अस्विकृत भौचर भेटीएन" +
                                "</td>";
                            $('#voucher_list').find('tbody').last('tr').html(tr);
                        }
                    }
                })

            })
        })
    </script>
    <script>
        $(document).on('click', '.voucher-accept', function () {
            $voucher_id = parseInt($(this).attr('data-id'));
            let url = "{{route('set.voucher.stauts.update',123)}}";
            url = url.replace(123, $voucher_id);
            let $tr = $(this).closest('tr');
            $.ajax({

                url: url,
                method: 'get',
                success: function (res) {
                    let result = $.parseJSON(res);
                    if (result === 'true') {
                        alert("स्विकृत भयो");
                        $tr.remove();
                    } else {

                        alert("Dr र Cr रकम बराबर छैन । भौचर संसोधन गरि मिलाउनु होला !!")
                    }

                }
            })
        })
    </script>
@endsection