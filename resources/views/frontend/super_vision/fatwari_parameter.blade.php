@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                फाटवारी

            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('fatwari')}}" method="post" target="_blank">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>आर्थिक वर्ष</label>
                            <select class="form-control" name="fiscal_year" id="fiscal_year">
                                @foreach($fiscalYears as $fiscalYear)
                                    <option value="{{$fiscalYear->id}}">{{$fiscalYear->year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>माथिल्लो कार्यालय</label>
                            <select class="form-control" name="ministry" id="ministry">
                                <option value="{{Auth::user()->ministry->id}}">{{Auth::user()->office->name}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>मातहतका कार्यालय</label>
                            <select class="form-control" name="office" id="officeId">
                                <option value="">...............</option>
                                <option value="123">All</option>
                                @foreach($officeLists as $office)
                                    <option value="{{$office->id}}">{{$office->name}}
                                        | {{$office->district->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>बजेट उपशीर्षक</label>
                            <select class="form-control" id="budgetSUbHead" name="budget_sub_head">
                                <option value="">...............................</option>
                            </select>
                        </div>
                        <div class="form-group center-block">
                            <label for="month">महिना</label>
                            <select name="month" id="month" class="form-control">
                                <option value="123">All</option>
                                <option value="10">बैशाख</option>
                                <option value="11">जेष्ठ</option>
                                <option value="12">असार</option>
                                <option value="1">साउन</option>
                                <option value="2">भदौ</option>
                                <option value="3">असोज</option>
                                <option value="4">कार्तिक</option>
                                <option value="5">मंसिर</option>
                                <option value="6">पुस</option>
                                <option value="7">माघ</option>
                                <option value="8">फगुन</option>
                                <option value="9">चैत्र</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="form-control btn btn-primary" value="हेर्ने">
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')

    <script>
        $(document).on('change', '#officeId', function () {
            let office_id = $('#officeId').val();
            let fiscal_year = $('#fiscal_year').val();
            let url = '{{route('get.budget.sub.head',['123','345'])}}';
            url = url.replace('123', office_id);
            url = url.replace('345', fiscal_year);
            $.ajax({
                method: 'get',
                url: url,
                success: function (res) {
                    let data = $.parseJSON(res);
                    let options = '<option value="">.......</option>';
                    $.each(data, function () {
                        options += '<option value="' + this.id + '">' + this.name + ' | ' + this.program_code + '</option>'
                    });
                    $('#budgetSUbHead').html(options);
                }
            })
        })
    </script>


    {{--  currnet month--}}
    <script>

        $(document).ready(function () {
            var currentDate = new Date();
            var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
            let month = currentNepaliDate.bsMonth;
            if (month == 1) {
                $finalmonth = 10;
            }
            if (month == 2) {
                $finalmonth = 11;
            }
            if (month == 3) {
                $finalmonth = 12;
            }
            if (month == 4) {
                $finalmonth = 1;
            }
            if (month == 5) {
                $finalmonth = 2;
            }
            if (month == 6) {
                $finalmonth = 3;
            }
            if (month == 7) {
                $finalmonth = 4;
            }
            if (month == 8) {
                $finalmonth = 5;
            }
            if (month == 9) {
                $finalmonth = 6;
            }
            if (month == 10) {
                $finalmonth = 7;
            }
            if (month == 11) {
                $finalmonth = 8;
            }
            if (month == 12) {
                $finalmonth = 9;
            }

            $('#month').val($finalmonth);
        });


    </script>

@endsection
