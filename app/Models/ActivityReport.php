<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityReport extends Model
{
    protected $table = 'activity_report';
    protected $guarded = [];

    public function getActivityName(){

        return $this->belongsTo('App\Models\BudgetDetails','activity_id','id');
    }
}
