<?php

namespace App\Models;

use App\helpers\Money_words;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    public $IS_LAST = false;

    protected $table = 'vouchers';
    protected $guarded = [];

    protected $appends = array('IS_LAST');

    public function getISLASTAttribute()
    {
        return $this->IS_LAST;
    }

    public function preBhuktani()
    {

        return $this->hasMany('App\Models\PreBhuktani', 'journel_id', 'id');
    }

    public function budget_sub_head()
    {
        return $this->belongsTo('App\Models\Programs');
    }

    public function details()
    {

        return $this->hasMany('App\Models\VoucherDetail', 'journel_id', 'id');
    }

    public function program()
    {

        return $this->belongsTo('App\Models\Programs', 'budget_sub_head_id', 'id');
    }

    public function karmachari_prepare_by()
    {

        return $this->belongsTo('App\Models\Karmachari', 'prepared_by', 'id');
    }

    public function karmachari_submit_by()
    {

        return $this->belongsTo('App\Models\Karmachari', 'submitted_by', 'id');
    }

    public function karmachari_approved_by()
    {

        return $this->belongsTo('App\Models\Karmachari', 'approved_by', 'id');
    }

    public function office()
    {

        return $this->belongsTo('App\Models\Office', 'office_id', 'id');
    }


//    functions
    public function calculate_total_expense()
    {
        $voucherCollection = $this->details;
        $adjustment = collect([]);
        $expense = collect([]);
        $pefa = collect([]);
        $lastYearPefa = collect([]);
        $total_expense = 0.00;
        $total_expense_temp = 0.00;
        $total_income = 0.00;
        $adjust = $voucherCollection->where('dr_or_cr', '=', 2)->whereIn('ledger_type_id', [1, 4]);
        if ($adjust->count() > 0) {
            $total_expense = 0;
        } else {
            $expense = $voucherCollection->where('dr_or_cr', '=', 1)->whereIn('ledger_type_id', [1, 4]);
            $pefa = $voucherCollection->where('dr_or_cr', '=', 2)->where('ledger_type_id', '=', 9);
            $lastYearPefa = $voucherCollection->where('dr_or_cr', '=', 2)->where('ledger_type_id', '=', 12);
        }
        $data = [];


        if ($pefa->count() > 0) {
            $overExp = $voucherCollection->contains(function ($value, $key) {
                if (($value['dr_or_cr'] == 2 and $value['ledger_type_id'] == 3) or  ($value['dr_or_cr'] == 2 and $value['ledger_type_id'] == 2)) {
                    return "over";
                }
            });
            $lessExp = $voucherCollection->contains(function ($value, $key) {
                if ($value['dr_or_cr'] == 1 and $value['ledger_type_id'] == 3) {
                    return "less";
                }
            });

            if ($overExp && $lessExp) {
                $total_income = 0;
                foreach ($voucherCollection as $detail) {

                    if ($detail['dr_or_cr'] == 1 and $detail['ledger_type_id'] == 3) {
                        $total_income += $detail['dr_amount'];
                    }
                    if (($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 2)) {
                        $total_expense += $detail['cr_amount'];
                    }
                }
                $total_expense = (float)$total_expense - (float)$total_income;
                $total_expense = number_format($total_expense,2);
            }
            //over expense
            elseif ($overExp) {
                foreach ($voucherCollection as $detail) {
                    if (($voucherCollection->where('dr_or_cr', 2) and $voucherCollection->where('ledger_type_id', 3))) {
                        if (($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 3) || ($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 2)) {
                            $total_expense += $detail->cr_amount;
                        }
                    }
                }
                $total_expense = number_format($total_expense,2);
            } //  less expense
            elseif ($lessExp) {

                foreach ($voucherCollection as $detail) {
                    if ($detail['dr_or_cr'] == 1 and $detail['ledger_type_id'] == 3) {
                        $total_expense += $detail->dr_amount;
                    }
                }
                $total_expense = number_format($total_expense,2);
                $total_expense = '(' . (string)($total_expense) . ')';
            }
        }
        elseif ($expense->count() > 0) {
//          expense
            foreach ($voucherCollection as $attribute) {

                $expenseHead = ExpenseHead::where('id', $attribute['hisab_number'])->where('status', 1)->first();
                if (($attribute['dr_or_cr'] == 1 and $attribute['ledger_type_id'] == 1) || ($attribute['dr_or_cr'] == 1 and $attribute['ledger_type_id'] == 4)) {
                    $total_expense += $attribute['dr_amount'];
                }

                if ($attribute['dr_or_cr'] == 2 and $attribute['ledger_type_id'] == 1) {
                    $total_expense += $attribute['cr_amount'];
                }
            }
            $total_expense = number_format($total_expense,2);
        }
        elseif ($lastYearPefa->count() > 0) {
                $overExp = $voucherCollection->contains(function ($value, $key) {
                    if ($value['dr_or_cr'] == 2 and $value['ledger_type_id'] == 3) {
                        return "over";
                    }
                });
                $lessExp = $voucherCollection->contains(function ($value, $key) {
                    if ($value['dr_or_cr'] == 1 and $value['ledger_type_id'] == 3) {
                        return "less";
                    }
                });

                if ($overExp && $lessExp) {
                    foreach ($voucherCollection as $detail) {

                        if ($detail['dr_or_cr'] == 1 and $detail['ledger_type_id'] == 3) {

                            $total_expense += $detail['dr_amount'];
                        }
                        if (($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 3) ||  ($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 2)) {
                            $total_expense_temp += $detail['cr_amount'];
                        }
                    }
                    $total_expense = (float)$total_expense_temp - (float)$total_expense;
                }
                elseif ($overExp) {
                    foreach ($voucherCollection as $detail) {
//                      over expense
                            if (($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 3) ||  ($detail['dr_or_cr'] == 2 and $detail['ledger_type_id'] == 2)) {
                                $total_expense += $detail['cr_amount'];
                            }
                    }
                }
                //  less expense
                elseif ($lessExp) {
                    foreach ($voucherCollection as $detail) {

                        if ($detail['dr_or_cr'] == 1 and $detail['ledger_type_id'] == 3) {

                            $total_expense += $detail['dr_amount'];
                        }
                    }
                }
        }
        if(!$total_expense){
            $total_expense = '0.00';
        }
        return $total_expense;
    }

    public function nikasa_check()
    {
        $details = $this->details;
        foreach ($details as $detail) {
            if ($detail->dr_or_cr == 2 && $detail->ledger_type_id == 8) {
                   return $nikasa = $this->details->where('dr_or_cr',2)->where('ledger_type_id',8)->sum('cr_amount');
            }
        }
    }

    public function check_payment()
    {
        $details = $this->details;
        foreach ($details as $detail) {
            if ($detail->dr_or_cr == 2 && $detail->ledger_type_id == 3) {
                return $this->payement_amount;

            }
        }
    }

    public function peski()
    {

        $details = $this->details;
        $peski = 0;
        foreach ($details as $detail) {
            if ($detail->dr_or_cr == 1 && $detail->ledger_type_id == 4) {

                $peski += $detail->dr_amount;

            }
        }
        return $peski;

    }

    public function calculate_total_peski_given()
    {
        $details = $this->details;
        $peskiGiven = 0;
        foreach ($details as $detail) {
            if ($detail->dr_or_cr == 1 && $detail->ledger_type_id == 4) {

                $peskiGiven += $detail->dr_amount;

            }
        }
        return $peskiGiven;
    }

    public function calculate_total_cr_liabilities()
    {
        $details = $this->details;
        $CrLiabilities = 0;
        foreach ($details as $detail) {
            if ($detail->dr_or_cr == 2 && $detail->ledger_type_id == 2) {

                $CrLiabilities += $detail->cr_amount;

            }
        }
        return $CrLiabilities;
    }

    public function calculate_total_dr_liabilities()
    {
        $details = $this->details;
        $drLiabilities = 0;
        foreach ($details as $detail) {
            if ($detail->dr_or_cr == 1 && $detail->ledger_type_id == 2) {

                $drLiabilities += $detail->dr_amount;

            }
        }
        return $drLiabilities;

    }


    public function get_expense_heads()
    {

        $details = $this->details;
        $expense_heads = '';
        foreach ($details as $key => $detail) {
            if ($detail->dr_or_cr == 1 && ($detail->ledger_type_id == 1 || $detail->ledger_type_id == 4)) {
                if (!strpos($expense_heads, (string)($detail->expense_head->expense_head_code))) {
                    if (strlen($expense_heads) > 1) {
                        $expense_heads .= ',';
                    }
                    $expense_heads .= ' ' . (string)($detail->expense_head->expense_head_code);
                }
            }
        }
        return $expense_heads;
    }

    public function isNikasa()
    {
        $nikasa = $this->details()->where('dr_or_cr', 2)->where('ledger_type_id', 8);
        if ($nikasa->count() > 0) {
            return true;
        }
        return false;
    }

    public function isExpense()
    {
        $expense = $this->details()->where('drOrCr', '=', 1)->whereIn('ledger_type_id', [1, 4]);
        $pefa = $this->details()->where('drOrCr', '=', 2)->where('ledger_type_id', '=', 9);
        $lastYearPefa = $this->details()->where('drOrCr', '=', 2)->where('ledger_type_id', '=', 12);
        $expense = $this->details()->whereIn('dr_or_cr',['1',2])->whereIn('ledger_type_id', ['1', '4','9','12','6']);
        if ($expense->count() > 0) {
            return true;
        }
        return false;
    }

    public function thisTotalExp()
    {
         $thisExp = $this->details()->where('dr_or_cr', '=',1)->whereIn('ledger_type_id', ['1', '4'])->sum('dr_amount');
       return $preMonthExp = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->where('vouchers.status', '=',1)
            ->where('vouchers.id', $this->id)
            ->where('activity_report.office_id', $this->office_id)
            ->where('activity_report.budget_sub_head_id', $this->budget_sub_head_id)
            ->sum('activity_report_details.expense');
    }


    public function set_voucher_number()
    {
        $voucher_numbers = '';
        $voucher_numbers .= '' . (string)($this->jv_number);
        return $voucher_numbers;
    }

//    VOucher Report
    public function get_total_dr()
    {
        $details = $this->details;
        $dr_amount = 0;
        return $dr_amount = $details->sum('dr_amount');

    }

    public function get_total_cr()
    {
        $details = $this->details;
        $cr_amount = 0;
        return $cr_amount = $details->sum('cr_amount');
    }

    public function get_amount_in_word()
    {
        $amount = $this->get_total_dr();
        $obj = new Money_words();
        return $words = $obj->convert_number($amount);

    }

    public function get_nikasa_by_budget_expense_head_id($expense_head_id)
    {
        $details = $this->details;
        $cr_amount = $details->where('expense_head_id', $expense_head_id)->where('dr_or_cr', 2)->where('ledger_type_id', 8)->sum('cr_amount');
        return $cr_amount;
    }

    public function get_kharcha_by_budget_expense_head_id($fiscalYear,$budgetSubHead, $expense_head_id, $month,$voucherId)
    {
        $expenseHeads = ExpenseHead::where('id',$expense_head_id)->where('status','=',1)->first();
        return $vouchers = ActivityReportDetail::join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->where('vouchers.status', '=',1)
            ->where('vouchers.id', '=',$voucherId)
            ->where('activity_report.budget_sub_head_id', $budgetSubHead)
            ->where('activity_report_details.fiscal_year', $fiscalYear)
            ->where('activity_report_details.expense_head', $expenseHeads->expense_head_code)
            ->where('activity_report_details.month', '=', $month)
            ->sum('activity_report_details.expense');

    }

    public function getPeskiClearance()
    {
        $details = $this->details;
        $peskiClearance = 0;
        foreach ($details as $detail) {
            if ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 9) {
                $peskiClearance += $detail->cr_amount;
            }
        }

        return $peskiClearance;
    }

    public function getLastYearPeski()
    {
        $details = $this->details;
        $lastYearPeski = 0;
        foreach ($details as $detail) {
            if ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 7) {
                $lastYearPeski += $detail->dr_amount;
            }
        }
        return $lastYearPeski;
    }

    public function getLastYearPeskiClearance()
    {
        $details = $this->details;
        $lastYearPeskiClearance = 0;
        foreach ($details as $detail) {
            if ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 12) {
                $lastYearPeskiClearance += $detail->cr_amount;
            }
        }
        return $lastYearPeskiClearance;
    }

}
