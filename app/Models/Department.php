<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';
    protected $guarded = [];

    public function province(){
        return $this->belongsTo('App\Models\Province');
    }

    public function mof(){
        return $this->belongsTo('App\Models\Mof');
    }

    public function ministry(){
        return $this->belongsTo('App\Models\Ministry');
    }

    public function users(){
        return $this->hasMany('App\Users');
    }

}
