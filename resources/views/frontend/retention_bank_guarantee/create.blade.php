@extends('frontend.layouts.app')
@section('styles')

    <style>
        .form-group {

            position: relative;
        }

        .validation-error {
            position: absolute;
            color: red;
            right: 4px;
            bottom: -23px;
            font-size: 10px;
        }
    </style>
@endsection
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                बैङ्क ग्यारेन्टी प्रविश्टि फारम
            </h1>
            <ul class="breadcrumb">
                <li><a type="button" href="{{route('retention.bank.guarantee')}}" class="btn btn-sm btn-primary"><i
                                class="fa fa-pencil"></i> सुची हेर्ने</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('retention.bank.guarantee.store')}}" id="retentionBankGuaranteeForm"
                          method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="office">कार्यालय:</label>
                                    <select class="form-control" name="office" id="office_id">
                                        <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="fiscal_year">आर्थिक वर्ष</label>
                                    <select id="fiscal_year" class="form-control" name="fiscal_year" required>
                                        <option value="{{$fiscalYear->id}}"
                                                selected>{{$fiscalYear->year}}</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="address">धरौटी अभिलेख/प्रयोजन</label>
                                    <select class="form-control" name="retention_purpose" id="retention_purpose"
                                            required>
                                        <option value="">..............</option>
                                        @foreach($retentionRecordList as $retention)
                                            <option value="{{$retention->id}}">{{$retention->purpose}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="party_type">धरौटी जम्मा गर्नेको प्रकार</label>
                                    <select name="party_type" class="form-control" id="party_type" required>
                                        <option value="">..............</option>
                                        @foreach($party_types as $party_type)
                                            <option value="{{$party_type->id}}">{{$party_type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="party_name">धरौटी जम्मा गर्ने</label>
                                    <select name="party_name" id="party_name" class="form-control" required>
                                        <option value="" selected>..............</option>
                                    </select>
                                </div>

                                <div class="col-md-2" style="width: 150px;">
                                    <label for="info_print_date">बैंक</label>
                                    <select id="bank" name="bank" class="bank form-control" required>
                                        <option value="">.........</option>
                                        @foreach($all_banks as $bank)
                                            <option value="{{$bank->id}}">{{$bank->name}}</option>
                                        @endforeach
                                    </select>

                                </div>

                                <div class="col-md-2">
                                    <label for="reference_number">प.स/ चलानी नम्बर</label>
                                    <input type="number" min="1" class="form-control" id="reference_number"
                                           placeholder="प.स/ चलानी नम्बर" name="reference_number" required>
                                </div>

                                <div class="col-md-2" style="width: 150px;">
                                    <label for="date"> मिति:</label>
                                    <input type="text" class="form-control" name="date" id="date" value="" required
                                    >
                                </div>

                                <div class="col-md-2" style="width: 150px;">
                                    <label for="retention_bank_guarantee_number"> ग्यारेन्टी नं.:</label>
                                    <input type="number" class="form-control" min="1"
                                           name="retention_bank_guarantee_number"
                                           id="retention_bank_guarantee_number" required>
                                </div>

                                <div class="col-md-2" style="width: 150px;">
                                    <label for="amount">रकम:</label>
                                    <input type="number" class="form-control" name="amount" id="amount" value=""
                                           required
                                    >
                                </div>

                                <div class="col-md-2" style="width: 150px;">
                                    <label for="guarantee_start_date"> ग्यारेन्टी लागुहुने मिति:</label>
                                    <input type="text" class="form-control" name="guarantee_start_date"
                                           id="guarantee_start_date" value="" required>
                                </div>

                                <div class="col-md-2" style="width: 150px;">
                                    <label for="guarantee_end_date"> ग्यारेन्टी समाप्तहुने मिति: </label>
                                    <input type="text" class="form-control" name="guarantee_end_date"
                                           id="guarantee_end_date"
                                           value="" required>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" id="btnBankGuarantee" class="btn btn-primary"
                                            style="margin-top: 18px;">Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')
    <script>
        $(document).on('click', '#btnBankGuarantee', function (e) {
            e.preventDefault();
            if (validation()) {
                let data = $('#guarantee_start_date').val();
                let guarantee_start = convertNepaliToEnglish($('#guarantee_start_date').val());
                let guarantee_last = convertNepaliToEnglish($('#guarantee_end_date').val());
                let current_date = convertNepaliToEnglish($('#date').val());
                $('#guarantee_start_date').val(guarantee_start);
                $('#guarantee_end_date').val(guarantee_last);
                $('#date').val(current_date);
                $('#retentionBankGuaranteeForm').submit();
            }
        })
    </script>


    {{--on change party_type for Vocuher --}}
    <script>
        $(document).ready(function () {
            $('#party_type').change(function () {
                let party_type_id = $('select#party_type').val();
                getPartyByPartyType(party_type_id);
            })
        })

    </script>

    {{--get  party name--}}
    <script>
        let getPartyByPartyType = function (party_type_id, party_name = '') {
            let url = '{{route('get_party_name_by_party_type_and_office', 123)}}';
            url = url.replace(123, party_type_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    console.log($.parseJSON(res));
                    let option = '<option value="">....................</option>';
                    $.each($.parseJSON(res), function () {
                        if (party_name == this.name_nep) {
                            option += '<option value="' + this.id + '" selected>' + this.name_nep + '</option>'

                        } else {

                            option += '<option value="' + this.id + '">' + this.name_nep + '</option>'
                        }
                    });
                    $('#party_name').html(option);
                }
            })
        }
    </script>

    {{--Date Picker --}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });
        $("#guarantee_start_date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });
        $("#guarantee_end_date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);


        $("#date").val(formatedNepaliDate);
        $("#guarantee_start_date").val(formatedNepaliDate);
        $("#guarantee_end_date").val(formatedNepaliDate);
    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            var charArray = input.split('');
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1';
                        break;
                    case '२':
                        engDate += '2';
                        break;
                    case '३':
                        engDate += '3';
                        break;
                    case '४':
                        engDate += '4';
                        break;
                    case '५':
                        engDate += '5';
                        break;
                    case '६':
                        engDate += '6';
                        break;
                    case '०':
                        engDate += '0';
                        break;
                    case '७':
                        engDate += '7';
                        break;
                    case '८':
                        engDate += '8';
                        break;
                    case '९':
                        engDate += '9';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
            });
            return engDate

        }
    </script>

    {{--validation--}}

    <script>
        function validation() {
            let flag = 1;
            let partyType = $('#party_type');
            if (!partyType.val()) {
                if (partyType.siblings('p').length == 0) {
                    partyType.after('<p class="validation-error">छान्नुहोस!</p>');
                    partyType.addClass("error");
                }
                partyType.focus();
                flag = 0;
            } else {
                partyType.siblings('p').remove()
            }
            let pretentionPurpose = $('#retention_purpose');
            if (!pretentionPurpose.val()) {
                if (pretentionPurpose.siblings('p').length == 0) {
                    pretentionPurpose.after('<p class="validation-error">छान्नुहोस!</p>');
                    pretentionPurpose.addClass("error");
                }
                pretentionPurpose.focus();
                flag = 0;
            } else {
                pretentionPurpose.siblings('p').remove()
            }
            let party = $('#party_name');
            if (!party.val()) {
                if (party.siblings('p').length == 0) {
                    party.after('<p class="validation-error">छान्नुहोस!</p>');
                    party.addClass("error");
                }
                party.focus();
                flag = 0;
            } else {
                party.siblings('p').remove()
            }
            let bank = $('#bank');
            if (!bank.val()) {
                if (bank.siblings('p').length == 0) {
                    bank.after('<p class="validation-error">छान्नुहोस!</p>');
                    bank.addClass("error");
                }
                bank.focus();
                flag = 0;
            } else {
                bank.siblings('p').remove()
            }

            // let referenceNumber = $('#reference_number');
            // if (!referenceNumber.val()) {
            //     if (referenceNumber.siblings('p').length == 0) {
            //         referenceNumber.after('<p class="validation-error">छान्नुहोस!</p>');
            //         referenceNumber.addClass("error");
            //     }
            //     referenceNumber.focus();
            //     flag = 0;
            // } else {
            //     referenceNumber.siblings('p').remove()
            // }
            // let bankGuaranteeNumber = $('#retention_bank_guarantee_number');
            // if (!bankGuaranteeNumber.val()) {
            //     if (bankGuaranteeNumber.siblings('p').length == 0) {
            //         bankGuaranteeNumber.after('<p class="validation-error">छान्नुहोस!</p>');
            //         bankGuaranteeNumber.addClass("error");
            //     }
            //     bankGuaranteeNumber.focus();
            //     flag = 0;
            // } else {
            //     bankGuaranteeNumber.siblings('p').remove()
            // }
            let amount = $('#amount');
            if (!amount.val()) {
                if (amount.siblings('p').length == 0) {
                    amount.after('<p class="validation-error">छान्नुहोस!</p>');
                    amount.addClass("error");
                }
                amount.focus();
                flag = 0;
            } else {
                amount.siblings('p').remove()
            }

            return flag;
        }
    </script>
@endsection