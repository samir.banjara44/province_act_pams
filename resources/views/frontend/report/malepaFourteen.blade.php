<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{ asset('js/nepali-datepicker.js') }}"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">

<style>
    table {
        border-collapse: collapse;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<style type="text/css" media="print"> @page {
        /*size: landscape;*/
    } </style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="border-collapse: collapse; font-size:13px">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
                    सभा @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6"
                    style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    @if(Auth::user()->office->department)
                        {{Auth::user()->office->department->name}}
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}, , {{Auth::user()->office->district->name}}</td>
            </tr>
            <tr>
                <td class="kalimati" colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>फर्छ्यौट गर्न बांकी पेश्कीको मास्केबारी</b><br>
                        <b class="kalimati">{{$year}} साल {{$month->name}} महिना</b>
                    </div>
                    <div style="float: right; margin-top: -20px">
                        म.ले.प.फा.नं. २११
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td class="kalimati">बजेट उपशिर्षक : @if($program){{$program->name}}@endif
                    -@if($program){{$program->program_code}}@endif</td>
            </tr>

        </table>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table" id="malepa-fourteen-table" width="100%" border="1"
                       style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th rowspan="3" style="width:70px;">पेश्की लिएको मिति</th>
                        <th rowspan="3" style="width:40px;">गो.भौ.न.</th>
                        <th rowspan="3">पेश्कीको विवरण</th>
                        <th rowspan="3">पेश्की लिने ब्यक्ती, फर्म वा कम्पनीको नाम</th>
                        <th rowspan="3">कर्मचारी भए पद</th>
                        <th rowspan="3" style="width:40px;">पेश्की संकेत नं</th>
                        <th rowspan="3">खर्च उपशिर्षक</th>
                        <th colspan="3">चालु आ. व.</th>
                        <th colspan="3">गत आ. व.सम्मको</th>
                        <th colspan="3">जम्मा बाँकी पेश्की</th>
                    </tr>

                    <tr>
                        <th colspan="2">म्याद ननाघेको</th>
                        <th rowspan="2">म्याद नाघेको</th>
                        <th colspan="2">म्याद ननाघेको</th>
                        <th rowspan="2">म्याद नाघेको</th>
                        <th rowspan="2">म्याद ननाघेको</th>
                        <th rowspan="2">म्याद नाघेको</th>
                        <th rowspan="2">जम्मा</th>
                    </tr>
                    <tr>
                        <th>रकम</th>
                        <th>फर्छ्यौटको अन्तिम म्याद</th>
                        <th>रकम</th>
                        <th>फर्छ्यौटको अन्तिम म्याद</th>
                    </tr>
                    <tr>
                        <th style="text-align:center">१</th>
                        <th style="text-align:center">२</th>
                        <th style="text-align:center">३</th>
                        <th style="text-align:center">४</th>
                        <th style="text-align:center">५</th>
                        <th style="text-align:center">६</th>
                        <th style="text-align:center">७</th>
                        <th style="text-align:center">८</th>
                        <th style="text-align:center">९</th>
                        <th style="text-align:center">१०</th>
                        <th style="text-align:center">११</th>
                        <th style="text-align:center">१२</th>
                        <th style="text-align:center">१३</th>
                        <th style="text-align:center">१४=(८+११)</th>
                        <th style="text-align:center">१५=(१०+१३)</th>
                        <th style="text-align:center">१६=(१४+१५)</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td colspan="16" style="background-color: white">१. कर्मचारी पेश्की</td>
                    </tr>

                    @foreach($karmachariAdvance['karmachari'] as $karmachari)
                        <tr style="background-color: #fff">
                            <td class="kalimati">
                                {{$karmachari['date']}}
                            </td>
                            <td class="">
                                <span class="e-n-t-n-n"><a href="{{route('voucher.view',$karmachari['journel_id'])}}" target="_blank" class="kalimati">{{$karmachari['voucher_no']}}</a></span>
                            </td>
                            <td>{{$karmachari['narration']}} | ({{$karmachari['activity']}})</td>
                            <td>{{$karmachari['party_name']}}</td>
                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: center">{{$karmachari['expense_head_title']}}</td>
                            <td class="kalimati" style="text-align: center">{{number_format($karmachari['baki'],2)}}</td>
                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: right;>{{$karmachari['last_year_baki']}}</td>
                            <td></td>
                            <td></td>
                            <td class="remain_advance kalimati;text-align: right>{{number_format($karmachari['last_year_baki'],2)}}</td>
                            <td></td>
                            <td class="kalimati" style="text-align: right"></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['total_peski'],2)}}</td>
                            <td class="kalimati" style="text-align: right"></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['total_peski'],2)}}</td>
                        </tr>
                    @endforeach

                    <tr>
                        <td colspan="7" style="text-align: right">जम्मा</td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachari_remain_peski,2)}}</td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachari_last_remain_peski,2)}}</td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachariAdvance['total_karmachari_peski'],2)}}</td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachariAdvance['total_karmachari_peski'],2)}}</td>
                    </tr>
                    <tr>
                        <td colspan="16" style="background-color: white">२. उपभोक्ता पेश्की</td>
                    </tr>
                    @foreach($karmachariAdvance['upaBhokta'] as $karmachari)
                        <tr style="background-color: #fff">
                            <td class="kalimati">
                                {{$karmachari['date']}}
                            </td>
                            <td class="kalimati">
                                <a href="{{route('voucher.view',$karmachari['journel_id'])}}" target="_blank" class="kalimati">{{$karmachari['voucher_no']}}</a>
                            </td>
                            <td>
                                {{$karmachari['narration']}} | ({{$karmachari['activity']}})
                            </td>
                            <td>
                                {{$karmachari['party_name']}}
                            </td>
                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: center">{{$karmachari['expense_head_title']}}</td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['baki'],2)}}</td>
                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['last_year_baki'],2)}}</td>
                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['total_peski'],2)}}</td>
                            <td></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['total_peski'],2)}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="7" style="text-align: right">जम्मा</td>
                        <td class="kalimati" style="text-align: right">{{number_format($upaBhokta_remain_peski,2)}}</td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($upaBhokta_last_remain_peski,2)}}</td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachariAdvance['total_upabhokta_peski'],2)}}</td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachariAdvance['total_upabhokta_peski'],2)}}</td>
                    </tr>
                    <tr>
                        <td colspan="16" style="background-color: white">३. ठेकेदार</td>
                    </tr>
                    @foreach($karmachariAdvance['thekedar'] as $karmachari)
                        <tr style="background-color: #fff">
                            <td class="kalimati">
                                {{$karmachari['date']}}
                            </td>
                            <td class="kalimati">
                                <a href="{{route('voucher.view',$karmachari['journel_id'])}}" target="_blank" class="kalimati">{{$karmachari['voucher_no']}}</a>
                            </td>
                            <td>
                                {{$karmachari['narration']}} |
                                @if(array_key_exists('activity',$karmachari))({{$karmachari['activity']}})@endif

                            </td>
                            <td>

                                {{$karmachari['party_name']}}
                            </td>

                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: center">{{$karmachari['expense_head_title']}}</td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['baki'],2)}}</td>
                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['last_year_baki'],2)}}</td>
                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['total_peski'],2)}}</td>
                            <td></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['total_peski'],2)}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="7" style="text-align: right">जम्मा</td>
                        <td class="kalimati" style="text-align: right">{{number_format($thekedar_remain_peski,2)}}</td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($thekedar_last_remain_peski,2)}}</td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachariAdvance['total_thekedar_peski'],2)}}</td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachariAdvance['total_thekedar_peski'],2)}}</td>
                    </tr>
                    <tr>
                        <td colspan="16" style="background-color: white">४. व्यक्तिगत</td>
                    </tr>
                    @foreach($karmachariAdvance['byaktigat'] as $karmachari)

                        <tr style="background-color: #fff">
                            <td class="kalimati">
                                {{$karmachari['date']}}
                            </td>
                            <td class="kalimati">
                                <a href="{{route('voucher.view',$karmachari['journel_id'])}}" target="_blank" class="kalimati">{{$karmachari['voucher_no']}}</a>
                            </td>
                            <td>
                                {{$karmachari['narration']}} | ({{$karmachari['activity']}})

                            </td>
                            <td>

                                {{$karmachari['party_name']}}
                            </td>

                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: center">{{$karmachari['expense_head_title']}}</td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['baki'],2)}}</td>
                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['last_year_baki'],2)}}</td>
                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['total_peski'],2)}}</td>
                            <td></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['total_peski'],2)}}</td>
                        </tr>

                    @endforeach
                    <tr>
                        <td colspan="7" style="text-align: right">जम्मा</td>
                        <td class="kalimati" style="text-align: right">{{number_format($byaktigat_remain_peski,2)}}</td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($byaktigat_last_remain_peski,2)}}</td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachariAdvance['total_byagtigat_peski'],2)}}</td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachariAdvance['total_byagtigat_peski'],2)}}</td>
                    </tr>
                    <tr>
                        <td colspan="16" style="background-color: white">५. संस्थागत पेश्की</td>
                    </tr>
                    @foreach($karmachariAdvance['sasthagat'] as $karmachari)
                        <tr style="background-color: #fff">
                            <td class="kalimati">
                                {{$karmachari['date']}}
                            </td>
                            <td class="kalimati" align="center">
                                <a href="{{route('voucher.view',$karmachari['journel_id'])}}" target="_blank" class="kalimati">{{$karmachari['voucher_no']}}</a>
                            </td>
                            <td>
                                {{$karmachari['narration']}} | ({{$karmachari['activity']}})
                            </td>
                            <td>
                                {{$karmachari['party_name']}}
                            </td>

                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: center">{{$karmachari['expense_head_title']}}</td>
                            <td class="kalimati" style="text-align: right">@if($karmachari['baki'] > 0){{number_format($karmachari['baki'],2)}}@endif</td>
                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['last_year_baki'],2)}}</td>
                            <td></td>
                            <td></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['total_peski'],2)}}</td>
                            <td></td>
                            <td class="kalimati" style="text-align: right">{{number_format($karmachari['total_peski'],2)}}</td>

                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="7" style="text-align: right">जम्मा</td>
                        <td class="kalimati" style="text-align: right">{{number_format($sasthagat_remain_peski,2)}}</td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($sasthagat_last_remain_peski,2)}}</td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachariAdvance['total_sasthagat_peski'],2)}}</td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachariAdvance['total_sasthagat_peski'],2)}}</td>
                    </tr>
                    <tr>
                        <th colspan="17" style="background-color: white">&nbsp;</th>
                    </tr>
                    <tr>
                        <td colspan="7" style="text-align: right">कुल जम्मा</td>
                        <td class="kalimati" style="text-align: right">{{number_format($total_remain_peski,2)}}</td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($total_last_remain_peski,2)}}</td>
                        <td></td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachariAdvance['gran_total'],2)}}</td>
                        <td></td>
                        <td class="kalimati" style="text-align: right">{{number_format($karmachariAdvance['gran_total'],2)}}</td>
                    </tr>
                </table>
                <table width="99%" style="font-size: 13px">
                    <tr>
                        <br>
                        <br>
                        <br>
                        <br>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">तयार गर्ने :
                            @if($voucher_signature and $voucher_signature->karmachari_prepare_by)
                                {{$voucher_signature->karmachari_prepare_by->name_nepali}}
                            @endif

                        </td>
                        <td>पेश गर्ने :
                            @if($voucher_signature and $voucher_signature->karmachari_submit_by)
                                {{$voucher_signature->karmachari_submit_by->name_nepali}}
                            @endif
                        </td>
                        <td>सदर गर्ने :
                            @if($voucher_signature and $voucher_signature->karmachari_approved_by)
                                {{$voucher_signature->karmachari_approved_by->name_nepali}}</td>
                        @endif
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">पद :
                            @if($voucher_signature and $voucher_signature->karmachari_prepare_by and $voucher_signature->karmachari_prepare_by)
                                {{$voucher_signature->karmachari_prepare_by->pad_id}}
                            @endif
                        </td>
                        <td>पद :
                            @if($voucher_signature and $voucher_signature->karmachari_submit_by and $voucher_signature->karmachari_submit_by)
                                {{$voucher_signature->karmachari_submit_by->pad_id}}
                            @endif
                        </td>
                        <td>पद :
                            @if($voucher_signature and $voucher_signature->karmachari_approved_by and $voucher_signature->karmachari_approved_by)
                                {{$voucher_signature->karmachari_approved_by->pad_id}}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">मिति :</td>
                        <td>मिति :</td>
                        <td>मिति :</td>
                    </tr>

                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

<script>
    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
            }
        });
        return nepaliNo;
    };

    let change_all_to_nepali_number = function () {
        $('.e-n-t-n-n').each(function () {
            let nepaliNo = changeToNepali($(this).text());
            let nepaliVal = changeToNepali($(this).val());

            $(this).text(nepaliNo);
            $(this).val(nepaliVal);
        });
    }
</script>

<script>
    $(document).ready(function () {

        let remain_advance = 0;
        let trs = $('#malepa-fourteen-table tbody').find('tr');
        $.each(trs, function () {

            remain_advance = $(this).find('td.remain_advance').text();
        });
        // alert(remain_advance);

    })
</script>




