<?php
namespace App\Repositories;

use App\helpers\BsHelper;
use App\Models\AdvanceAndPayment;
use App\Models\RetentionRecord;
use http\Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;

class RetentionRecordRepositoryEloquent implements RetentionRecordRepository
{
    public function create($attributes) {

        $retention_record = new RetentionRecord();
        $retention_record->office_id = Auth::user()->office->id;
        $retention_record->fiscal_year_id = $attributes['fiscal_year'];
        $retention_record->retention_category_id = $attributes['retention_category_id'];
        $retention_record->purpose = $attributes['purpose'];
        $retention_record->information_number = $attributes['information_number'];
        $date_array = explode('-', $attributes['hidden_information_published_date']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $retention_record->information_publish_date = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];


        $date_array = explode('-', $attributes['hiden_information_last_date']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $retention_record->information_last_date = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];

        $date_array = explode('-', $attributes['created_date']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $retention_record->created_at = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];

        $retention_record->status = 1;

        $created_date = $attributes['created_date'];
        $myyearfirst=(substr($created_date, 0,2));
        $myyearlast=(substr($created_date, 2,2));
        $mymonth=(explode('-',$created_date));
        $month=intval($mymonth[1]);
        if($month==1){$finalmonth=10;}
        if($month==2){$finalmonth=11;}
        if($month==3){$finalmonth=12;}
        if($month==4){$finalmonth=1;}
        if($month==5){$finalmonth=2;}
        if($month==6){$finalmonth=3;}
        if($month==7){$finalmonth=4;}
        if($month==8){$finalmonth=5;}
        if($month==9){$finalmonth=6;}
        if($month==10){$finalmonth=7;}
        if($month==11){$finalmonth=8;}
        if($month==12){$finalmonth=9;}

        $retention_record->month = $finalmonth;
        $retention_record->save();
        return $retention_record;


    }


    public function getAll(){

       return $records = RetentionRecord::all();
    }

    public function getByOffice($officeId){
        return $records = RetentionRecord::where('office_id',$officeId)->get();
    }
    public function get_by_id($id){
        $retentionRecord = RetentionRecord::findOrFail($id);
        return $retentionRecord;

    }

    public function update($attributes ,$id) {
        $retentionRecord = $this->get_by_id($id);
        $retentionRecord->purpose = $attributes['purpose'];
        $retentionRecord->information_number = $attributes['information_number'];
        $retentionRecord->save();
        return $retentionRecord;

    }

        
        

}