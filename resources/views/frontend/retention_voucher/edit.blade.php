@extends('frontend.layouts.app')

@section('styles')
    <style>
        #budgetStoreCreateFrom label {
            display: block !important;
        }

        th {
            color: black;
            font-size: 14px !important;
        }

        .form-group {

            position: relative;
        }

        .validation-error {
            position: absolute;
            color: red;
            right: 4px;
            bottom: -23px;
            font-size: 10px;
        }
    </style>
@endsection
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="font-size: 25px;">
               धरौटी भौचर सम्पादन
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="flash-message">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                    </div>
                    <form action="{{route('retention.voucher.details.update',$retention_voucher_details_id)}}"
                          method="post" id="">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-9">
                                <div class="col-md-2" style="width: 150px;">
                                    <label>डेविट/क्रेडिट</label>
                                    <select name="drOrCr" class="form-control" id="DrOrCr" required
                                            style="height: 32px;">
                                        <option value="">..........</option>
                                        <option value="1" @if($retentionVoucherDetails->dr_or_cr == 1) selected @endif>
                                            डेबिट
                                        </option>
                                        <option value="2" @if($retentionVoucherDetails->dr_or_cr == 2) selected @endif>
                                            क्रेडिट
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-2" style="width:110px;">
                                    <label>व्यहोरा</label>
                                    <select name="byahora" class="form-control" id="byahora" required style="height: 32px;">
                                        <option value="">.......</option>
                                        <option value="3" @if($retentionVoucherDetails->byahora == 3) selected @endif>एकल कोष खाता
                                        </option>
                                        <option value="10" @if($retentionVoucherDetails->byahora == 10) selected @endif>धरौटी
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-2" style="width:145px;">
                                    <label>हिसाब नं.</label>
                                    <select name="hisab_number" class="form-control select2" id="hisab_number" required
                                            style="height: 32px;">
                                        @foreach($hisabNumbers as $hisabNumber)
                                            <option value="{{$hisabNumber->id}}"
                                                    @if($hisabNumber->id == $retentionVoucherDetails->hisab_number)selected @endif>{{$hisabNumber->expense_head_sirsak}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2" style="width: 346px;">
                                    <label>बिबरण</label>
                                    <input type="text" class="form-control" name="details" id="details"
                                           value="{{$retentionVoucherDetails->details}}" required style="height: 32px;">
                                </div>
                                <div class="col-md-2">
                                    <label>धरौटी अभिलेख</label>
                                    <select class="form-control" name="retention_record">
                                        <option value="">..............</option>
                                        @foreach($retentionRecords as $record)
                                            <option value="{{$record->id}}"
                                                    @if($record->id == $retentionVoucherDetails->retention_record) selected @endif>{{$record->purpose}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2" style="width: 150px;">
                                    <label>धरौटी प्रकार</label>
                                    <select name="retention_type" class="form-control" id="retention_type"
                                            style="height: 32px;">
                                        <option value="">..............</option>
                                        <option value="1"
                                                @if($retentionVoucherDetails->retention_type ==1) selected @endif>
                                            जमानत
                                        </option>
                                        <option value="2"
                                                @if($retentionVoucherDetails->retention_type ==2) selected @endif>
                                            न्यायिक
                                        </option>
                                        <option value="3"
                                                @if($retentionVoucherDetails->retention_type ==3) selected @endif>
                                            विविध
                                        </option>
                                        <option value="4"
                                                @if($retentionVoucherDetails->retention_type ==4) selected @endif>
                                            लिलाम
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-2" style="width: 150px;">
                                    <label> धरौटी जम्मा गर्नेको प्रकार</label>
                                    <select name="party_type" class="form-control" id="party_type"
                                            style="height: 32px;">
                                        <option value="">..............</option>
                                        <option value="1"
                                                @if($retentionVoucherDetails->depositor_type ==1) selected @endif>
                                            उपभोक्ता समिति
                                        </option>
                                        <option value="2"
                                                @if($retentionVoucherDetails->depositor_type ==2) selected @endif>
                                            ठेकेदार
                                        </option>
                                        <option value="3"
                                                @if($retentionVoucherDetails->depositor_type ==3) selected @endif>
                                            व्यक्तिगत
                                        </option>
                                        <option value="4"
                                                @if($retentionVoucherDetails->depositor_type ==4) selected @endif>
                                            संस्थागत
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-2" style="width: 130px">
                                    <label> धरौटी जम्मा गर्ने</label>
                                    <select name="party" id="party" class="form-control" style="height: 32px;">
                                        <option value="" selected>..............</option>
                                    </select>
                                </div>
                                <div class="col-md-2" style="width: 130px">
                                    <label>भौचर/नगदी रसिद नम्बर</label>
                                    <input type="number" name="bill_number" min="1" id="bill_number"
                                           value="{{$retentionVoucherDetails->bill_number}}" class="form-control"

                                           style="height: 32px;">
                                </div>
                                <div class="col-md-2" style="width: 130px">
                                    <label>रकम</label>
                                    <input type="number" name="amount" min="1" id="amount"
                                           value="{{$retentionVoucherDetails->amount}}" class="form-control" required
                                           style="height: 32px;">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" id="voucher_submit"
                                            style="margin-top: 18px;">Update
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('scripts')
    <script>
        let retentionVOucherDetailsEncode = '{!! $retentionVoucherDetails !!}';
        let retentionVoucherDetail = $.parseJSON(retentionVOucherDetailsEncode);

    </script>

    <script>
        $(document).ready(function () {
            let party_type_id = $('#party_type :selected').val();
            getPartyByPartyType(party_type_id);

        })
    </script>
    <script>
        $(document).ready(function () {
                let byahora = $('#byahora').val();
                get_hisab_number_by_byahora(byahora);
            })
    </script>



    {{--Dr  Or Cr change हुदा--}}
    <script>
        $(document).ready(function () {
            // $('#DrOrCr').change(function () {
            //     $('#byahora').val("");
            //     $('#hisab_number').html("");
            //     $('#details').val("");
            // })
        })
    </script>

    {{--व्यहोरा change हुदा--}}
    <script>
        $(document).ready(function () {
            $('#byahora').change(function () {
                let byahora = $('#byahora').val();
                get_hisab_number_by_byahora(byahora);
            })
        })
    </script>

    {{--on change party_type --}}
    <script>
        $(document).ready(function () {
            $('#party_type').change(function () {
                let party_type_id = $('#party_type :selected').val();
                getPartyByPartyType(party_type_id);
            })
        });
    </script>

    {{--get  party name --}}
    <script>
        let getPartyByPartyType = function (party_type_id, party_name = '') {
            let url = '{{route('get_party_name_by_party_type_and_office', 123)}}';
            url = url.replace(123, party_type_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let resData = $.parseJSON(res)
                    let option = '<option value="">....................</option>';
                    $.each(resData, function () {
                        if (retentionVoucherDetail.depositor == this.id) {
                            option += '<option value="' + this.id + '" selected>' + this.name_nep + '</option>'
                        } else {
                            option += '<option value="' + this.id + '">' + this.name_nep + '</option>'
                        }
                    });
                    $('#party').html(option);
                }
            })
        }
    </script>


    {{--Get हिसाब Number By व्यहोरा--}}
    <script>
        let get_hisab_number_by_byahora = function (byahora) {
            alert(byahora);
            let ledger_type = byahora;
            let url = '{{route('get_expense_head__by_ledger_type',123)}}';
            url = url.replace(123, ledger_type);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let option = '<option value="">.........</option>';
                    let hisab_number = $.parseJSON(res);
                    if (res.length > 0) {
                        if (hisab_number.length > 1) {
                            $.each($.parseJSON(res), function () {
                                if (retentionVoucherDetail.hisab_number == this.id) {
                                    option += '<option value="' + this.id + '" selected>' + this.expense_head_sirsak + '</option>'

                                } else {
                                    option += '<option value="' + this.id + '">' + this.expense_head_sirsak + '</option>'
                                }
                            })
                        $('#hisab_number').html(option);
                        getDetailsByHisabNumber();

                    } else {
                        $('#hisab_number').html(option);

                    }
                }
            })
        }
    </script>

    {{--hisab number change huda--}}
    <script>
        let getDetailsByHisabNumber = function () {

            let bibran = $('#hisab_number option:selected').text();
            // console.log('bibara check', bibran);
            $('input#details').val(bibran);
            $('input#details').closest('div').find('p.validation-error').remove();
        };
        $(document).ready(function () {
            $('#hisab_number').change(function () {
                getDetailsByHisabNumber();
            })
        })
    </script>


    {{--checkRemain funcion--}}
    <script>
        let checkRemain = () => {

            let $hiddenRemainingBudget = $('#hidden_remain_budget');
            let amount = $('input#amount').val();

            let mainActivityId = $('#main_activity_name').val();
            let $existingDebitTds = $('#voucher_table tbody td.activity[data-id="' + mainActivityId + '"]');

            let existingDebitTotal = 0;
            $.each($existingDebitTds, function () {
                if (!$(this).closest('tr').hasClass(editingSN)) {
                    existingDebitTotal += parseFloat($(this).closest('tr').find('td.drAmount').text())
                }
            });

            let remain = $hiddenRemainingBudget.val();
            // $hiddenRemainingBudget.val(remain-existingDebitTotal);
            // $('#remain_budget').text(remain-existingDebitTotal).addClass('e-n-t-n-n');
            // remain = $hiddenRemainingBudget.val();
            // alert(remain)
            if (parseFloat(remain) >= parseFloat(amount)) {

                remain = parseFloat(remain) - parseFloat(amount);

                expense = parseFloat(amount) + existingDebitTotal;
                $('#total_expense').text(expense).addClass('e-n-t-n-n');
                $('#hidden_expense').val(expense);
                $('#hidden_remain_budget').val(remain);
                $('#remain_budget').text(remain).addClass('e-n-t-n-n');

                return true;
            } else {

                alert('मौज्दात भन्दा बढि भयो। बाकी:' + remain);
                $('#amount').focus();
                $('#amount').val(remain);
                return false;
            }
        }
    </script>


    {{-- क्रे र एकल कोष खाता amount total गर्ने--}}
    <script>
        let get_cr_tsa = function () {
            let bhuktani_main_activity = $('#bhuktani_main_activity').val();

            let $mainActivityTds = $('#voucher_table tbody td.activity[data-id="' + bhuktani_main_activity + '"]');

            let cr_tsa = 0;
            let totalDebit = 0;
            let totalCredit = 0;
            $.each($mainActivityTds, function () {
                let $row = $(this).closest('tr');
                let drOrCr = $.trim($row.find('td.dr-cr').text());
                if (drOrCr === 'डेबिट') {
                    let rowDr = parseFloat($row.find('td.drAmount').text());
                    if (rowDr > 0) {
                        totalDebit += rowDr;
                    }
                } else if (drOrCr === 'क्रेडिट') {
                    if ($.trim($row.find('td.byahora').text()) !== "एकल कोष खाता") {
                        let rowCr = parseFloat($row.find('td.crAmount').text());
                        if (rowCr > 0) {
                            totalCredit += rowCr;
                        }
                    }
                }
            });
            cr_tsa = totalDebit - totalCredit;
            // let trs = $('#voucher_table').find('tr');
            // $.each(trs, function (key, tr) {
            //     // $.each($(tr).find('td'),function (key, td) {
            //     //
            //     // })
            //     if ($(tr).find('td.dr-cr').text() == 'क्रेडिट' && $(tr).find('td.byahora').text() == 'एकल कोष खाता') {
            //         cr_tsa += parseFloat($(tr).find('td.crAmount').text())
            //     }
            //
            // });
            return cr_tsa;

        }
    </script>

    {{--dr_cr_equal_check finction--}}
    <script>
        let dr_cr_equal_check = function () {


            $('#VoucherSave').prop('disabled', false);

            let option = '<option selected>...................</option>';
            $.each(activityForBhuktani, function () {
                option += '<option value="' + this.activity_id + '">' + this.activity_name + '</option>';
                $('#bhuktani_main_activity').html(option);
            })

        }
    </script>

    {{-- Dr and Cr total देखाउने function --}}
    <script>
        let dr_cr_total = function () {

            let trs = $('#voucher_table').find('tr');
            let DrAmount = 0;
            let CrAmount = 0;
            let drCrRemaining = 0;
            $.each(trs, function (key, tr) {
                if (!$(this).hasClass(editingSN)) {
                    let newDr = parseFloat($(this).find('td.drAmount').text());
                    if (newDr > 0) {
                        DrAmount += newDr;
                    }

                    let newCr = parseFloat($(this).find('td.crAmount').text());
                    if (newCr > 0) {
                        CrAmount += newCr;
                    }


                }

                // Total nikalne loop, kei error aako xaina aile samma vane delete handim, mathi ko le kaam garxa
                // $.each($(tr).find('td'), function (key_, td) {
                //
                //     if ($(td).prop('class') == 'drAmount') {
                //         let newDr = $(this).text();
                //         if (newDr) {
                //             DrAmount += parseFloat(newDr);
                //             $('#dr_amount').html(DrAmount);
                //
                //         }
                //     }
                //     if ($(td).prop('class') == 'crAmount') {
                //         let newCr = $(this).text();
                //         if (newCr) {
                //
                //             CrAmount += parseFloat(newCr);
                //             $('#dr_amount').html(CrAmount);
                //
                //         }
                //     }
                //
                // })
                drCrRemaining = DrAmount - CrAmount;
                if (CrAmount === DrAmount && CrAmount > 0) {


                    $('#VoucherSave').prop('disabled', false);

                    let option = '<option selected>...................</option>';
                    $.each(activityForBhuktani, function () {
                        option += '<option value="' + this.activity_id + '">' + this.activity_name + '</option>';
                        $('#bhuktani_main_activity').html(option);
                    })


                } else {
                    $('#VoucherSave').prop('disabled', "disabled");
                }
            });
            $('#dr_amount').text(DrAmount);
            $('#cr_amount').text(CrAmount);
            $('#amount_difference').text(drCrRemaining);
        }

    </script>

    {{--short narration key up --}}
    <script>
        $(document).ready(function () {
            $('#shortInfo').keyup(function () {
                let shortNarration = $('#shortInfo').val();
                $('#detailsInfo').val(shortNarration);
            })
        })
    </script>

    {{-- Fixed --}}

    {{--    Validation--}}
    <script>
        $('.form-control').change(function () {
            let val = $(this).val();
            if (val) {
                $(this).closest('div').find('p.validation-error').remove();
            }
        });

        function validation() {
            let flag = 1;
            let drOrCr = $('#DrOrCr').val();
            let byahora = $('#byahora').val();

            let amountId = $('#amount');
            if (!amountId.val()) {
                if (amountId.siblings('p').length == 0) {
                    amountId.after('<p class="validation-error">छान्नुहोस!</p>');
                    amountId.addClass("error");
                }
                amountId.focus();
                flag = 0;
            } else {
                amountId.siblings('p').remove()
            }

            let hisabNumber = $('#hisab_number');
            if (!hisabNumber.val()) {
                if (hisabNumber.siblings('p').length == 0) {
                    hisabNumber.after('<p class="validation-error">छान्नुहोस!</p>');
                    hisabNumber.addClass("error");
                }
                hisabNumber.focus();
                flag = 0;
            } else {
                hisabNumber.siblings('p').remove()
            }

            let details = $('#details');
            if (!details.val()) {
                if (details.siblings('p').length == 0) {
                    details.after('<p class="validation-error">छान्नुहोस!</p>');
                    details.addClass("error");
                }
                details.focus();
                flag = 0;
            } else {
                details.siblings('p').remove()
            }
            if(byahora != 3) {
                let retentionRecord = $('#retention_record');
                if (!retentionRecord.val()) {
                    if (retentionRecord.siblings('p').length == 0) {
                        retentionRecord.after('<p class="validation-error">छान्नुहोस!</p>');
                        retentionRecord.addClass("error");
                    }
                    retentionRecord.focus();
                    flag = 0;
                } else {
                    retentionRecord.siblings('p').remove()
                }

                let retentionType = $('#retention_type');
                if (!retentionType.val()) {
                    if (retentionType.siblings('p').length == 0) {
                        retentionType.after('<p class="validation-error">छान्नुहोस!</p>');
                        retentionType.addClass("error");
                    }
                    retentionType.focus();
                    flag = 0;
                } else {
                    retentionType.siblings('p').remove()
                }

                let partyType = $('#party_type');
                if (!partyType.val()) {
                    if (partyType.siblings('p').length == 0) {
                        partyType.after('<p class="validation-error">छान्नुहोस!</p>');
                        partyType.addClass("error");
                    }
                    partyType.focus();
                    flag = 0;
                } else {
                    partyType.siblings('p').remove()
                }

                let party = $('#party');
                if (!party.val()) {
                    if (party.siblings('p').length == 0) {
                        party.after('<p class="validation-error">छान्नुहोस!</p>');
                        party.addClass("error");
                    }
                    party.focus();
                    flag = 0;
                } else {
                    party.siblings('p').remove()
                }
            }
            let amount = $('#amount');
            if (!amount.val()) {
                if (amount.siblings('p').length == 0) {
                    amount.after('<p class="validation-error">छान्नुहोस!</p>');
                    amount.addClass("error");
                }
                amount.focus();
                flag = 0;
            } else {
                amount.siblings('p').remove()
            }
            return flag;
        }
    </script>
    {{--Date Picker --}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);
    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            // console.log(input);
            var charArray = input.split('');
            //console.log(charArray);
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1';
                        break;
                    case '२':
                        engDate += '2';
                        break;
                    case '३':
                        engDate += '3';
                        break;
                    case '४':
                        engDate += '4';
                        break;
                    case '५':
                        engDate += '5';
                        break;
                    case '६':
                        engDate += '6';
                        break;
                    case '०':
                        engDate += '0';
                        break;
                    case '७':
                        engDate += '7';
                        break;
                    case '८':
                        engDate += '8';
                        break;
                    case '९':
                        engDate += '9';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
                //console.log(engDate)
            });
            return engDate

        }
    </script>

    {{--Clear Voucher and Bhuktani field when  Add button click--}}
    <script>
        function clearVoucherField() {

            // $('#main_activity_name').val('')
            $('#hisab_number').val('');
            $('#details').val('');
            $('#party_type').val('');
            $('#party-name').val('');
            $('#amount').val('')

        }

        function clearBhuktaniField() {
            // alert("blank here")
            // $('#main_activity_name').val('')
            $('select#bhuktani_main_activity').val('');
            $('#bhuktani_party_type').val('');
            $('#bhuktani_party').val('');
            $('#bhuktani_amount').val('');
            $('#advance_tax_deduction_amount').val('');
            $('#vat_amount').val('');
            $('#bank').val('');
            $('#vat_bill_number').val('');
            $('#pratibadhata_number').val('');

        }
    </script>

@endsection
