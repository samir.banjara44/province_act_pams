@extends('frontend.layouts.app')
<style>
    thead th {
        text-align: center;
    }
</style>
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-university"></i> दर्ता विवरण </h1>

            <button type="button" class="btn btn-primary btn-sm btn-show-form" id="showAddForm"><i
                        class="fas fa-plus"></i> नयाँ थप गर्ने
            </button>
                <a type="button" class="btn btn-primary btn-sm btn-show-form" href="{{route('darta.show.all', Auth::user()->office)}}" target="_blank"> सबै हेर्ने</a>
        </section>

        <!-- FORM section start -->
        <section class="formSection displayNone" id="showThisForm">
            <div class="row justify-content-md-center">
                <div class="col-md-8 form-bg col-md-offset-2">
                    <button type="button" class="btn btn-danger btn-sm btn-show-form displayNone" id="closeAddForm"><i
                                class="fas fa-times"></i> फर्म रद्ध गर्ने
                    </button>
                    <div class="form-title">दर्ता विवरण प्रविष्टी गर्ने
                        <a href="https://mail.google.com/" target="_blank"><img src="{{ asset('img/gmail.png')}}" height="25px"
                                                    width="25px" alt=""></a>
                        <a href="https://www.facebook.com/" target="_blank"><img src="{{ asset('img/facebook.png')}}" height="25px"
                                                    width="25px" alt=""></a>
                    </div>
                        <form action="{{route('darta.store')}}" method="post" id="dartaForm" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="name">मिति </label>
                                <input type="text" class="form-control" id="date" placeholder="date" name="date"
                                       required>
                            </div>
                            <div class="col-md-6">
                                <label for="name">दर्ता न.</label>
                                <input type="text" class="form-control kalimati" value="{{$dartaNumber}}"
                                       id="dartanumber" placeholder="दर्ता न."
                                       name="dartanumber" required disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="from_office_name">दर्ता आएको कार्यालय</label>
                                <input type="text" class="form-control" id="from_office_name"
                                       placeholder="दर्ता आएको कार्यालय"
                                       name="name" required>
                            </div>
                            <div class="col-md-6">
                                <label for="address">दर्ता आएको कार्यालय ठेगाना</label>
                                <input type="text" class="form-control" id="from_office_address"
                                       placeholder="दर्ता आएको कार्यालय ठेगाना"
                                       name="address" required>
                            </div>
                            <div class="col-md-6">
                                <label for="person">दर्ता गर्न आएको व्यक्ति</label>
                                <input type="text" class="form-control" id="person"
                                       placeholder="दर्ता गर्न आएको व्यक्ति" name="person">
                            </div>
                            <div class="col-md-6">
                                <label for="purpose">विषय</label>
                                <input type="text" class="form-control" id="purpose" placeholder="विषय"
                                       name="purpose" required>
                            </div>
                            {{-- <div class="col-md-6">
                                <label for="branch">सम्बन्धित शाखा</label>
                                <input type="text" class="form-control" id="branch" placeholder="शाखा" name="branch"
                                       required>
                            </div> --}}
                            <div class="col-md-6">
                                <label for="branch">सम्बन्धित शाखा</label>
                                <select name="branch" required class="form-control branch" style="width:100%;" id="branch">
                                    <option value="">.................</option>
                                    @foreach($branch as $branch)
                                    <option value="{{ $branch->id}}">@if($branch->name_nep){{$branch->name_nep}}@endif</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="col-md-6">
                                <label for="prepared_date">डोकुमेन्ट तयार भएको मिति</label>
                                <input type="text" class="form-control" id="prepared_date" placeholder="Prepared_Date"
                                       name="prepared_date" required>
                            </div>
                            <div class="col-md-6">
                                <label for="received_date">डोकुमेन्ट दर्ता भएको मिति</label>
                                <input type="text" class="form-control" id="received_date" placeholder="Received_Date"
                                       name="received_date">
                            </div>
                            <div class="col-md-6">
                            <label for="file">Upload</label>
                                <input type="file" class="form-control" id="image" placeholder="image"
                                       name="image">
                            </div>
                            <div class="col-md-6">
                                <label for="remarks">कैफियत</label>
                                <textarea class="form-control" id="remarks" placeholder="Remarks"
                                          name="remarks"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2 col-md-offset-5">
                                <button type="submit" id="dartaSubmit" class="btn btn-primary btn-show-form btn-block"
                                        style="margin-top: 18px;">थप
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- FORM section ends -->

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body pt-15">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>सि.न.</th>
                            <th>मिति</th>
                            <th>दर्ता न.</th>
                            <th>दर्ता भएको कार्यालय</th>
                            <th>ठेगाना</th>
                            <th>व्यक्ति</th>
                            <th>प्रायोजन</th>
                            <th>शाखा</th>
                            <th>Image</th>
                            <th>कैफियत</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dartas as $index=>$darta)
                            <tr>
                                <td class="kalimati" align="center">{{++$index}}</td>
                                <td class="kalimati" align="center">{{$darta->date}}</td>
                                <td class="kalimati" align="center">{{$darta->darta_number}}</td>
                                <td>{{$darta->from_office_name}}</td>
                                <td>{{$darta->from_office_address}}</td>
                                <td>{{$darta->from_office_person}}</td>
                                <td>{{$darta->purpose}}</td>
                                <td>@if($darta->getBranch and $darta->getBranch->name_nep){{$darta->getBranch->name_nep}}@endif</td>
                                <td>@if($darta->image)<img src="{{asset('images/').'/'.$darta->image}}" height="30px"
                                         width="30px" alt="">@endif</td>
                                <td>{{$darta->remarks}}</td>
                                <td>
                                    <a type="button" href="{{route('darta.show',$darta->id)}}" target="_blank"
                                       class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> हेर्ने</a>
                                    <a type="button" href="{{route('darta.edit',$darta->id)}}"
                                       class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>

                                    @if(count($dartas) == $index)
                                        <a type="button" href="#" id="delete_darta" data-darta-id="{{$darta->id}}"
                                           class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i> Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')
    {{--  Save darta--}}

    <script>
        $(document).ready(function () {
            $('#dartaSubmit').click(function (e) {
                e.preventDefault();
                if (validation()) {
                    let today_nepali_date = $('#date').val();
                    let nepali_date_in_roman = convertNepaliToEnglish(today_nepali_date);
                    let prepared_date = $('#prepared_date').val();
                    let prepared_date_in_roman = convertNepaliToEnglish(prepared_date);
                    let received_date = $('#received_date').val();
                    let received_date_in_roman = convertNepaliToEnglish(received_date);
                    $('#date').val(nepali_date_in_roman);
                    $('#prepared_date').val(prepared_date_in_roman);
                    $('#received_date').val(received_date_in_roman);
                    $('#dartaForm').submit();
                }

            })
        })
    </script>

    {{--Today date  Date picker--}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });
        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);

    </script>

    {{-- Prepared date, Date picker--}}
    <script>
        $("#prepared_date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });
        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#prepared_date").val(formatedNepaliDate);

    </script>

    {{-- Receive date, Date picker--}}
    <script>
        $("#received_date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });
        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#received_date").val(formatedNepaliDate);

    </script>

    {{--    Darta delete--}}
    <script>
        $(document).on('click', '#delete_darta', function () {
            let dartaId = $(this).attr('data-darta-id');
            let url = '{{route('darta.delete',123)}}';
            url = url.replace(123, dartaId);
            swal({
                title: "दर्ता Delete गर्ने ?",
                text: "एक पटक Delete गरे पछि पुनः प्राप्त गर्न सकिने छैन!",
                icon: "warning",
                buttons: true,
                dangerMode: true,

            }).then((willDelete) => {

                if (willDelete) {
                    $.ajax({
                        url: url,
                        method: 'get',
                        success: function (res) {
                            console.log(res);
                            if ($.parseJSON(res) == true) {
                                swal("दर्ता Delete भयो!", {
                                    icon: "success",
                                });
                                location.reload();
                            } else {
                                swal("दर्ता Delete भएन!", {
                                    icon: "warning",
                                });
                            }
                        }
                    })

                } else {
                    swal("Your imaginary file is safe!");
                }
            });

        })
    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            var charArray = input.split('');
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1';
                        break;
                    case '२':
                        engDate += '2';
                        break;
                    case '३':
                        engDate += '3';
                        break;
                    case '४':
                        engDate += '4';
                        break;
                    case '५':
                        engDate += '5';
                        break;
                    case '६':
                        engDate += '6';
                        break;
                    case '०':
                        engDate += '0';
                        break;
                    case '७':
                        engDate += '7';
                        break;
                    case '८':
                        engDate += '8';
                        break;
                    case '९':
                        engDate += '9';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
            });
            return engDate

        }
    </script>

    {{-- Validation   --}}
    <script>
        function validation() {
            let flag = 1;
            let fromOfficeName = $('#from_office_name');
            if (!fromOfficeName.val()) {
                if (fromOfficeName.siblings('p').length == 0) {
                    fromOfficeName.after('<p style="color:red" class="validation-error">कुन अफिसबाट आएको हो लेख्नुहोस!</p>')
                }
                fromOfficeName.focus();
                flag = 0;
            } else {
                fromOfficeName.siblings('p').remove()
            }

            let fromOfficeAddress = $('#from_office_address');
            if (!fromOfficeAddress.val()) {
                if (fromOfficeAddress.siblings('p').length == 0) {
                    fromOfficeAddress.after('<p style="color:red" class="validation-error">दर्ता हुने कार्यालय लेख्नुहोस!</p>')
                }
                fromOfficeAddress.focus();
                flag = 0;
            } else {
                fromOfficeAddress.siblings('p').remove()
            }

            // let fromOfficePerson = $('#person');
            // if (!fromOfficePerson.val()) {
            //     if (fromOfficePerson.siblings('p').length == 0) {
            //         fromOfficePerson.after('<p style="color:red" class="validation-error">दर्ता गर्न आएको व्यक्ति</p>')
            //     }
            //     fromOfficePerson.focus();
            //     flag = 0;
            // } else {
            //     fromOfficePerson.siblings('p').remove()
            // }

            let purpose = $('#purpose');
            if (!purpose.val()) {
                if (purpose.siblings('p').length == 0) {
                    purpose.after('<p style="color:red" class="validation-error">What is the purpose of the document.</p>')
                }
                purpose.focus();
                flag = 0;
            } else {
                purpose.siblings('p').remove()
            }

            let branch = $('#branch');
            if (!branch.val()) {
                if (branch.siblings('p').length == 0) {
                    branch.after('<p style="color:red" class="validation-error">From which branch</p>')
                }
                branch.focus();
                flag = 0;
            } else {
                branch.siblings('p').remove()
            }
            return flag;
        }
    </script>
@endsection