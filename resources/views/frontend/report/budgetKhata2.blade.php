<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
{{--<link href="{{ asset('css/nepali-datepicker.css') }}" rel="stylesheet">--}}
<link rel="stylesheet" href="{{asset('css/styles.css')}}">

<style>
    table {
        border-collapse: collapse;
        table-layout: fixed;
        font-size: 12px !important;
    }

    body {
        overflow-x: scroll !important;
    }

    .table {
        width: 100%;
        overflow-x: scroll;
    }

    .table2 {
        width: auto;
        overflow-x: scroll;
    }

    tbody tr:hover {
        background-color: #e1e1e1 !important;

    }
    td {
        min-height: 10px;
    }
    body {

        overflow-x: hidden;
    }

    .no-border {
        border: 0px;
    }

    .floatLeft {
        width: 50%;
        float: left;
    }

    .floatRight {
        width: 50%;
        float: right;
    }

    .container {
        overflow: hidden;
    }

    @media print {
        #downloadMe {
            display: none;
        }
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <input type="button" style="position: absolute; top:20px;right: 20px" value="Export to Excel" id="downloadMe">
{{--    <input type="button" style="position: absolute; top:20px;right: 130px;color: blue" value="Fix Report" id="fixReport">--}}
<!-- Content Header (Page header) -->
    <section class="content-header">
        <div id="tableHeadingWrapper">
            <table class="table" style="border-collapse: collapse; font-size:13px; width: 100%">
                <tr>
                    <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                    <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
                        सभा @endif</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
                </tr>
                <tr>
                    <td colspan="6"
                        style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">
                        @if(Auth::user()->office->department)
                            {{Auth::user()->office->department->name}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}, , {{Auth::user()->office->district->name}}</td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div style="width: 100%; text-align: center">
                            <b>बजेट खाता, {{$month_name}}</b>
                        </div>
                        <div style="float: right; margin-top: -20px">
                            म.ले.प.फा.नं. २०८
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="kalimati">बजेट उपशिर्षक : {{$program->name}} - {{$program->program_code}}</td>
                    <td colspan="2" class="kalimati" style="text-align: right">आर्थिक वर्ष : {{$fiscalYear->year}}</td>
                </tr>
            </table>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $b_count = $budgetByExpenseHeads->count() ?>
        <?php $v_count = $thisMonthVouchers->where('status', 0)->count() //only for checking row count. must have to chage this according to $voucher->isNikasa() function ?>
        <?php $v_count2 = $thisMonthVouchers->count() ?>

        <div style="position: relative" id="tableWrapper">
            <table class="table" border="1">
                <tr>
                    <td style="width: 22px; justify-content: center; align-content: center; align-items: center; text-align: center;"
                        rowspan="7">प्रथम खण्ड
                    </td>
                    <td style="width: 29px" rowspan="7">बजेट</td>
                    <td colspan="4" style="text-align:center; width: 400px !important; background-color: #dfdcdc; "><b>खर्च उपशिर्षकको नाम</b></td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td  style="vertical-align: middle;text-align:center; border-bottom: 1px solid #000; height: 20px; width: 90px;background-color: #dfdcdc;">
                           <b class="kalimati">{{$budgetExpenseHead->expense_head}}</b>
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="4" style="text-align:left;height: 60px;background-color: #dfdcdc;"></td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;  height: 30px;background-color: #dfdcdc;">
                            {{$budgetExpenseHead->expense_by_expense_head->expense_head_sirsak}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="4" style="text-align:left;height: 23px;background-color: #dfdcdc">स्विकृत आर्थिक वर्षको बजेट बिनियोजन</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;">
                            {{number_format($budgetExpenseHead->getInitialBudget($datas['office_id'],$datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id),2)}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="4" style="text-align:left;height: 23px;background-color: #dfdcdc">स्विकृत आर्थिक वर्षको थप बजेट बिनियोजन</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;">
                            {{number_format($budgetExpenseHead->getAddBudget($datas['office_id'],$datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id),2)}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="4" style="text-align:left;height: 23px;background-color: #dfdcdc">रकमान्तरबाट थप/घट रकम</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;">
                            {{number_format($budgetExpenseHead->getReduceBudget($datas['office_id'],$datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id),2)}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="4" style="text-align:left;height: 23px;background-color: #dfdcdc">श्रोतान्तरबाट थप/घट</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;">
                            {{number_format($budgetExpenseHead->getReduceBudget($datas['office_id'],$datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id),2)}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="3" style="text-align:left;height: 23px; font-weight: bold; background-color: #dfdcdc;"> जमा कायम भएको बजेट</td>
                    <td style="text-align:right; height: 23px; font-weight: bold; background-color: #dfdcdc;"
                        class="kalimati">{{number_format($totalBudget,2)}}</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;background-color: #dfdcdc;">
                            {{number_format($budgetExpenseHead->getFinalBudget($datas['office_id'],$datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id),2)}}
                        </td>
                    @endforeach
                </tr>
                {{--Second part from here--}}
                <tr>
                    <td rowspan="{{$nikasaCount+4}}">द्वितिय खण्ड</td>
                    <td rowspan="{{$nikasaCount+4}}">निकासा</td>
                    <td align="center" style="background-color: #dfdcdc;">मिति</td>
                    <td align="center" style="background-color: #dfdcdc;">भौ. नं.</td>
                    {{-- <td align="center">संकेत नं.</td>--}}
                    <td align="center" style="background-color: #dfdcdc;">विवरण</td>
                    <td align="center" style="background-color: #dfdcdc;">जम्मा</td>
                    @for($i=0; $i<$b_count; $i++)
                        <td></td>
                    @endfor
                </tr>
                @foreach ($thisMonthVouchers as $voucher)
                    @if($voucher->isNikasa())
                        <tr>
                            <td style="text-align: center">{{$voucher->data_nepali}}</td>
                            <td class="kalimati" style="text-align: center"><a class="kalimati" href="{{route('voucher.view',$voucher->id)}}"  style="text-decoration: none" target="_blank">{{$voucher->jv_number}}</a>
                            </td>
                            {{--                                    <td>2</td>--}}
                            <td>{{$voucher->short_narration}}</td>
                            <td class="kalimati"
                                style="text-align: right">{{number_format($voucher->payement_amount,2)}}</td>
                            @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                                <td class="kalimati" style="padding: 0!important; margin: 0; text-align: right">
                                    {{number_format($voucher->get_nikasa_by_budget_expense_head_id($budgetExpenseHead->expense_head_id),2)}}
                                </td>
                            @endforeach
                        </tr>
                    @endif
                @endforeach
                <tr>
                    <td colspan="3" style="text-align: right;background-color: #dfdcdc;"><b>यो महिनाको निकासा</b></td>
                    <td class="kalimati" style="text-align: right;background-color: #dfdcdc;">{{number_format($thisMonthTotalNikasa,2)}}</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right;background-color: #dfdcdc;">
                            {{number_format($budgetExpenseHead->thisMonthNikasa($datas['office_id'],$datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']),2)}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right;background-color: #dfdcdc;"><b>अघिल्लो महिनासम्मको निकासा</b></td>
                    <td class="kalimati" style="text-align: right;background-color: #dfdcdc;">{{number_format($upToPreMonthTotalNikasa,2)}}
                    </td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right;background-color: #dfdcdc;">
                            {{number_format($budgetExpenseHead->upToPreMonthNikasa($datas['office_id'],$datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']-1),2)}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right;background-color: #dfdcdc;"><b>जम्मा निकासा</b></td>
                    <td class="kalimati" style="text-align: right;background-color: #dfdcdc;">{{number_format($totalNikasa,2)}}</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right;background-color: #dfdcdc;">
                            {{number_format($budgetExpenseHead->totalNikasa($datas['office_id'],$datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']),2)}}
                        </td>
                    @endforeach
                </tr>

                {{--Third part from here--}}
                <tr>
                    <td rowspan="{{$v_count2+5}}">
                        तृतीय खण्ड
                    </td>
                    <td rowspan="{{$v_count2+5}}">खर्च</td>
                    <td></td>
                    <td></td>
                    {{-- <td><p style="width: 60px; -topborder: 1px solid white;">संकेत नं.</p></td>--}}
                    <td></td>
                    <td></td>
                    @for($i=0; $i<$b_count; $i++)
                        <td>
                        </td>
                    @endfor
                </tr>

                @foreach ($thisMonthVouchers as $voucher)
                    @if($voucher->isExpense())
                        <tr>
                            <td align="center">{{$voucher->data_nepali}}</td>
                            <td align="center" class="kalimati"><a class="kalimati"
                                                                   href="{{route('voucher.view',$voucher->id)}}"
                                                                   style="text-decoration: none"
                                                                   target="_blank">{{$voucher->jv_number}}</a></td>
                            {{--                        <td><p style="width: 60px">2</p></td>--}}
                            <td>{{$voucher->short_narration}}</td>
                            <td class="kalimati" align="right">{{($voucher->calculate_total_expense())}}</td>
                            @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                                <td class="kalimati" align="right">
                                    {{number_format($voucher->get_kharcha_by_budget_expense_head_id($datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month'],$voucher->id),2)}}
                                </td>
                            @endforeach
                        </tr>
                    @endif
                @endforeach

                <tr>
                    <td colspan="3" style="text-align: right;background-color: #dfdcdc;"><b>यो महिनाको जम्मा खर्च</b></td>
                    <td class="kalimati" style="text-align: right;background-color: #dfdcdc;">{{number_format($thisMonthTotalExpense,2)}}</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right;background-color: #dfdcdc;">
                            {{number_format($budgetExpenseHead->thisMonthTotalExp($datas['office_id'],$datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']),2)}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right;background-color: #dfdcdc;"><b>अधिल्लो महिनाकोसम्मको जम्मा खर्च</b></td>
                    <td class="kalimati" style="text-align: right;background-color: #dfdcdc;">{{number_format($upToPreMonthTotalExpense,2)}}</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right;background-color: #dfdcdc;">
                            {{number_format($budgetExpenseHead->preMonthTotalExpense($datas['office_id'],$datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']-1),2)}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right;background-color: #dfdcdc;"><b>जम्मा खर्च</b></td>
                    <td class="kalimati" style="text-align: right;background-color: #dfdcdc;">{{number_format($totalExpense,2)}}</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right;background-color: #dfdcdc;">
                            {{number_format($budgetExpenseHead->totalExpense($datas['office_id'],$datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']),2)}}
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right;background-color: #dfdcdc;"><b>बाकीँ बजेट</b></td>
                    <td class="kalimati" style="text-align: right;background-color: #dfdcdc;">{{number_format($remainBudget,2)}}</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right;background-color: #dfdcdc;">
                            {{number_format($budgetExpenseHead->totalRemainBudget($datas['office_id'],$datas['fiscal_year'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']),2)}}
                        </td>
                    @endforeach
                </tr>
            </table>
            <br/>
            <table width="99%" style="font-size: 13px">
                <tr>
                    <td style="padding-left: 52px;">तयार गर्ने :
                        @if($voucher_signature and $voucher_signature->karmachari_prepare_by)
                            {{$voucher_signature->karmachari_prepare_by->name_nepali}}
                        @endif
                    </td>
                    <td>पेश गर्ने :
                        @if($voucher_signature and $voucher_signature->karmachari_submit_by)
                            {{$voucher_signature->karmachari_submit_by->name_nepali}}
                        @endif
                    </td>
                    <td>सदर गर्ने :
                        @if($voucher_signature and $voucher_signature->karmachari_approved_by)
                            {{$voucher_signature->karmachari_approved_by->name_nepali}}</td>
                    @endif
                </tr>
                <tr>
                    <td style="padding-left: 52px;">पद :
                        @if($voucher_signature and $voucher_signature->karmachari_prepare_by)
                            {{$voucher_signature->karmachari_prepare_by->pad_id}}
                        @endif
                    </td>
                    <td>पद :
                        @if($voucher_signature and $voucher_signature->karmachari_submit_by)
                            {{$voucher_signature->karmachari_submit_by->pad_id}}
                        @endif
                    </td>
                    <td>पद :
                        @if($voucher_signature and $voucher_signature->karmachari_approved_by)
                            {{$voucher_signature->karmachari_approved_by->pad_id}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 52px;">मिति :</td>
                    <td>मिति :</td>
                    <td>मिति :</td>
                </tr>

            </table>
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
    $("#downloadMe").click(function (e) {
        let allTable = $('div[id$=tableHeadingWrapper]').html() + $('div[id$=tableWrapper]').html();
        console.log(allTable);
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(allTable));
        e.preventDefault();
    });

    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '/')

                    nepaliNo += "/";
            }
        });
        console.log(nepaliNo);
        return nepaliNo;
    };

    // let change_all_to_nepali_number = function(){
    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });
    // }


</script>

<script>
    $(document).ready(function () {

        let month = $('span.month').text();
        // alert(month);

        $('#fixReport').click(function () {
            let $tables = $('.table');
            $tables.each(function () {
                if ($(this).hasClass('table')) {
                    $(this).addClass('table2');
                    $(this).removeClass('table');
                } else {
                    $(this).removeClass('table2');
                    $(this).addClass('table');
                }
            });
        })

    })
</script>