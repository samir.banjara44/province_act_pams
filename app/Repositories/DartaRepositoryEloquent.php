<?php
namespace App\Repositories;

use App\Models\Darta;
use Illuminate\Support\Facades\Auth;

class DartaRepositoryEloquent implements DartaRepository
{
    public function create($attributes) {
        $darta = new Darta();
        $darta->date = $attributes['date'];
        $date = $attributes['date'];
        $darta->darta_number = $attributes['darta_number'];
        $darta->from_office_name = $attributes['name'];
        $darta->from_office_address = $attributes['address'];
        $darta->from_office_person = $attributes['person'];
        $darta->purpose = $attributes['purpose'];
        $darta->office_id = Auth::user()->office->id;
        $darta->branch = $attributes['branch'];
        $darta->prepared_date = $attributes['prepared_date']; 
        $darta->received_date = $attributes['received_date']; 
        $darta->created_by = Auth::user()->id;
        $darta->remarks = $attributes['remarks'];
        $darta->status = 1;
        if(array_key_exists('image',$attributes)){
            $darta->image = $attributes['image'];
        } else {
            $darta->image = 0;
        }
        $darta->letter_number = $attributes['letter_number'];
        $myyearfirst = (substr($date, 0, 2));
        $myyearlast = (substr($date, 2, 2));
        $explode = explode('-',$date);
        $month = intval($explode[1]);
        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }
        if (intval($month) >= 4) {
            $darta->fiscal_year = $myyearfirst . $myyearlast . "/" . (intval($myyearlast) + 1);
        } else {
            $darta->fiscal_year = $myyearfirst . (intval($myyearlast) - 1) . "/" . $myyearlast;
        }
        $darta->month = $finalmonth;   // after month field create in table
        $darta->save();
        return $darta;
    }
    
    public function update($attributes, $id){

        $darta = $this->get_by_id($id);
        $darta->date = $attributes['date'];
        $date = $attributes['date'];
        $darta->from_office_name = $attributes['name'];
        $darta->from_office_address = $attributes['address'];
        $darta->from_office_person = $attributes['person'];
        $darta->purpose = $attributes['purpose'];
        $darta->office_id = Auth::user()->office->id;
        $darta->branch = $attributes['branch'];
        $darta->prepared_date = $attributes['prepared_date'];
        $darta->received_date = $attributes['received_date'];
        $darta->created_by = Auth::user()->id;
        $darta->remarks = $attributes['remarks'];
        $darta->status = 1;
        if(array_key_exists('image',$attributes)){
            $darta->image = $attributes['image'];
        }

        $myyearfirst = (substr($date, 0, 2));
        $myyearlast = (substr($date, 2, 2));
        $explode = explode('-',$date);
        $month = intval($explode[1]);
        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }
        if (intval($month) >= 4) {
            $darta->fiscal_year = $myyearfirst . $myyearlast . "/" . (intval($myyearlast) + 1);
        } else {
            $darta->fiscal_year = $myyearfirst . (intval($myyearlast) - 1) . "/" . $myyearlast;
        }
        $darta->month = $finalmonth;
        $darta->save();
        return $darta;
    }
    
    public function get_by_office_id(){
        
        $office_id = Auth::user()->office_id;
        return $dartas = Darta::where('office_id', $office_id)->where('status','1')->get();
    }

    public function get_by_id($id){
        return $dartas = Darta::findorfail($id);
    }

    public function get_last_number_by_office($officeId){
        $darta = Darta::where('office_id',$officeId)->latest()->first();

        if($darta){
           $darta_number = $darta->darta_number;
        } else {

            $darta_number = 0;
        }
        return $darta_number + 1;
    }
}