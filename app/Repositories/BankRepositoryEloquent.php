<?php
namespace App\Repositories;

use App\Models\Bank;
use Illuminate\Support\Facades\Auth;

class BankRepositoryEloquent implements BankRepository
{
    public function create($attributes) {
        $bank = new Bank();
        $bank->name = $attributes['name'];
        $bank->address = $attributes['address'];
        $bank->office_id = Auth::user()->office->id;
        $bank->status = 1;
        $bank->save();
        return $bank;
    }

    public function get_by_id($id){
        return Bank::findorfail($id);
    }

    public function update($attributes, $id){
        $bank = $this->get_by_id($id);
        $bank->name = $attributes['name'];
        $bank->address = $attributes['address'];
        $bank->save();
        return $bank;
    }



    public function get_by_office_id(){

        $office_id = Auth::user()->office->id;
        return $banks = Bank::where('office_id',$office_id)->where('status','1')->get();
    }
}