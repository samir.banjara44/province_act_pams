<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\DarbandiSrot;
use App\Repositories\DarbandiSrotRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class DarbandiSrotController extends Controller
{

    private $darbandisrotRepo;

    public function __construct(DarbandiSrotRepository $darbandisrotRepo)
    {
        $this->middleware('auth');
        $this->darbandisrotRepo = $darbandisrotRepo;
    }

    public function index()
    {
        $darbandisrots= $this->darbandisrotRepo->get_all_darbandisrots();
        return view('backend.settings.darbandisrots.index', compact('darbandisrots'));
    }

    public function create(){

        return view('backend.settings.darbandisrots.create');
    }
    public function store(Request $request){

        $darbandisrot = $this->darbandisrotRepo->create($request->all());
        return redirect(route('admin.darbandisrot'));
    }

}
