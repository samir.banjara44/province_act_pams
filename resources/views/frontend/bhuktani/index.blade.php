@extends('frontend.layouts.app')

@section('content')
    <style>
        table thead th {
            text-align: center;
        }

        tbody tr:hover {
            background-color: #e1e1e1 !important;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h2 align="center">
                भुक्तानी आदेश
            </h2>
            <h3>
                <center>बजेट उपशिर्षक : {{$programs->name}}</center>
            </h3>
            <input type="hidden" id="bs-roman" name="bs-roman">
            <input type="hidden" value="{{$programs->id}}" name="budget_sub_head">
            <h4 class="kalimati">
                <center>शिर्षक नं. : {{$programs->program_code}}</center>
            </h4>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <form action="{{route('bhuktani.create')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>आर्थिक वर्ष : </label>
                                        <select name="fiscal_year" id="fiscal_year" class="form-control">
                                            <option value="{{$fiscalYear->id}}" selected>{{$fiscalYear->year}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>बजेट उपशिर्षक :</label>
                                        <select name="budget_sub_head" id="budget_sub_head" class="form-control"
                                        >
                                            <option value="{{$programs->id}}">{{$programs->name}}
                                                | {{$programs->program_code}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3" style="padding-top: 5px;">
                                        <input type="submit" class="btn btn-primary btn-block" id="bhuktaniCreateBtn"
                                               value="नयाँ भुक्तानी आदेश बनाउने">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <a type="button" href="{{route('bhuktani')}}" class="btn btn-sm btn-primary"><i
                        class="fa fa-pencil"></i> पछाडी फर्कने</a>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body pt-10">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>सि.न.</th>
                            <th>मिति</th>
                            <th>भुक्तानी आदेश न.</th>
                            <th>रकम</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="3" align="right"><b>जम्मा</b></td>
                            <td class="" align="right"><b><span class="kalimati">{{number_format($totalBhuktaniAmount,2)}}</span></b></td>
                            <td></td>
                        </tr>
                        @foreach($bhuktanies as $index=>$bhuktani)
                            <tr>
                                <td class="kalimati" align="center">{{++$index}}</td>
                                <td align="center">{{$bhuktani->date_nepali}}</td>
                                <td class="kalimati" align="center">{{$bhuktani->adesh_number}}</td>
                                <td class="kalimati text-right">{{ number_format($bhuktani->amount,2) }}</td>
                                <td style="text-align: center">
                                    <a type="button" href="{{route('report.bhuktani.adesh',$bhuktani->id)}}"
                                       target="_blank" class="btn btn-sm btn-primary">हेर्ने</a> |
                                    @if (count($bhuktanies) - $index == count($bhuktanies)-1)
                                        {{--                       <a type="button" href="{{route('bhuktani.create',$bhuktani->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>--}}
                                        <a type="button" id="bhuktani_delete" data-id= '{{$bhuktani->id}}'

                                           class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#bhuktaniCreateBtn').click(function () {
                let fiscal_year = $('#fiscal_year').val();
                let budget_sub_head = $('#budget_sub_head').val();
                let url = '{{route('bhuktani.create',['123','345'])}}';
                url = url.replace('123', fiscal_year);
                url = url.replace('345', budget_sub_head);
                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {

                    }
                })
            })
        })
    </script>

    <script>
        $(document).ready(function () {
            $('#bhuktani_delete').click(function () {
               let bhuktaniId = $('#bhuktani_delete').attr('data-id');
               let url = '{{route('bhuktani.delete',123)}}'
                url = url.replace(123,bhuktaniId)
                swal({
                    title: "Are you sure?",
                    text: "Delete भए पछि Recovere हुदैन",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,

                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url : url,
                            method : 'get',
                            success : function (res) {
                                if(res){
                                    swal("Voucher has been deleted!", {
                                        icon: "success",
                                    });
                                    location.reload();
                                }
                            }
                        })
                    }
                });
            })
        })
    </script>
@endsection