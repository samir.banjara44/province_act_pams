@extends('frontend.layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> म.ले.प.फा.नं. ६०३ ::धरौटी बैंक नगदी किताब </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="panel panel-primary">
      <div class="panel-body p-15">
        <form action="{{route('report.dharauti.cash.book')}}" method="post" target="_blank">
          {{csrf_field()}}
          <div class="form-group">
            <div class="row">

              <div class="col-md-4 col-md-offset-1">
                <label for="office">कार्यालय : </label>
                <select name="office" class="form-control">
                  <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}</option>
                </select>
              </div>

              <div class="col-md-2">
                <label for="fiscal_year">आर्थिक वर्ष:</label>
                <select name="fiscal_year" class="form-control">
                  <option value="{{$fiscalYear->id}}">{{$fiscalYear->year}}</option>
                </select>
              </div>

              <div class="col-md-2">
                <label>महिना</label>
                <select name="month" id="month" class="form-control select2">
                  <option value="10">बैशाख</option>
                  <option value="11">जेष्ठ</option>
                  <option value="12">असार</option>
                  <option value="1">साउन</option>
                  <option value="2">भदौ</option>
                  <option value="3">असोज</option>
                  <option value="4">कार्तिक</option>
                  <option value="5">मंसिर</option>
                  <option value="6">पुस</option>
                  <option value="7">माघ</option>
                  <option value="8">फगुन</option>
                  <option value="9">चैत्र</option>
                </select>
              </div>

              <div class="col-md-2">
                <label for="">&nbsp;</label>
                <button type="submit" class="btn btn-primary btn-block">Submit</button>
              </div>

            </div>
          </div>

        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>

@endsection

@section('scripts')

{{--  currnet month--}}
<script>
  $(document).ready(function () {
      var currentDate = new Date();
      var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
      // console.log(currentNepaliDate.bsMonth);
      let month = currentNepaliDate.bsMonth;
      if(month==1){$finalmonth=10;}
      if(month==2){$finalmonth=11;}
      if(month==3){$finalmonth=12;}
      if(month==4){$finalmonth=1;}
      if(month==5){$finalmonth=2;}
      if(month==6){$finalmonth=3;}
      if(month==7){$finalmonth=4;}
      if(month==8){$finalmonth=5;}
      if(month==9){$finalmonth=6;}
      if(month==10){$finalmonth=7;}
      if(month==11){$finalmonth=8;}
      if(month==12){$finalmonth=9;}

      $('#month').val($finalmonth);
    });


</script>
@endsection