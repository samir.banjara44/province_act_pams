<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('office_id');
            $table->integer('budget_sub_head_id');
            $table->integer('activity_id');
            $table->float('budget',11,2)->nullable();
            $table->string('expense_head')->nullable();
            $table->float('expense',11,2)->nullable();
            $table->float('remain',11,2)->nullable();
            $table->string('fiscal_year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_report');

    }
}
