@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                कार्यालय सुची

            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>क्र.स.</th>
                            <th style="width: 300px;">कार्यालय</th>
                            <th>जिल्ला</th>
                            <th>कार्यालय कोड</th>
                            {{--                            <th>Action</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($officeLists as $index=>$office)
                            <tr>
                                <td>{{++$index}}</td>
                                <td>{{$office->name}}</td>
                                <td>
                                    @if($office->district)
                                        {{$office->district->name}}
                                    @endif
                                </td>
                                <td class="kalimati">{{$office->office_code}}</td>

                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection
