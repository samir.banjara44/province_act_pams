<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChequeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheque_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cheque_id');
            $table->integer('office_id');
            $table->integer('bank_id');
            $table->string('cheque_number');
            $table->integer('voucher_id')->nullable();
            $table->integer('party_id')->nullable();
            $table->string('assigned_date_bs')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cheque_details');
    }
}
