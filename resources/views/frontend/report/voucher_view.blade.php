<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{ asset('css/styles.css') }}">
<style>
    table {
        border-collapse: collapse;
    }

    .accept-table {}

    .pull-right {
        float: right;
    }

    .fixPrintBtnRightTop {
        position: fixed;
        top: 0;
        right: 0;
    }

    @media print {
        .noprint {
            display: none;
        }
    }

    tr:hover {
        background-color: #e1e1e1 !important;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <button class="fixPrintBtnRightTop pull-right noprint" onclick="window.print()">Print</button>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="font-size:13px" class="">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png') }}"
                    style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                <td colspan="6" style="text-align: center">
                    @if (Auth::user()->office->id != 43)
                        प्रदेश सरकार
                    @else
                        प्रदेश सभा
                    @endif
                </td>
            </tr>

            <tr>
                <td colspan="6" style="text-align: center">
                    @if (Auth::user()->office->ministry)
                        {{ Auth::user()->office->ministry->name }}
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    @if (Auth::user()->office->department)
                        {{ Auth::user()->office->department->name }}
                    @endif
                </td>

            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{ Auth::user()->office->name }}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{ Auth::user()->office->province->name }},
                    {{ Auth::user()->office->district->name }}</td>
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>गोश्वारा भौचर</b>
                    </div>
                    <div style="float: right; margin-top: -20px">
                        म.ले.प.फा.नं. २०३
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="4">आर्थिक वर्ष :<span class="e-n-t-n-n">{{ $fiscalYear->year }}</span></td>
                <td style="text-align: right; ">गोश्वारा भौचर : <span class="e-n-t-n-n">{{ $voucher->jv_number }}</span>
                </td>

            </tr>
            <tr>
                <td colspan="4">बजेट उपशीर्षक : {{ $voucher->program->name }}
                    <span class="e-n-t-n-n">
                        - {{ $voucher->budget_sub_head->program_code }}
                    </span>
                </td>
                <td style="text-align: right; padding-right: 1px;">मिति :{{ $voucher->data_nepali }}</td>
            </tr>
        </table>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table width="100%" class="table" id="voucher_table" border="1"
                    style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                        <tr>
                            <th rowspan="2">क्र.सं.</th>
                            <th width="100px" style="width: 55px" rowspan="2">संकेत/उप-शीर्षक नंम्बर</th>
                            <th rowspan="2" style="width:88px">क्रियाकलाप/ कार्यक्रम संकेत नं:</th>
                            <th rowspan="2" style="width:555px !important">कारोवारको ब्यहोरा</th>
                            <th rowspan="2" style="width:50px !important">खा.पा.नं.</th>
                            <th colspan="4">स्रोत</th>
                            <th rowspan="2" style="width:99px !important">डेबिट</th>
                            <th rowspan="2" style="width:99px !important">क्रेडिट</th>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align:center">तह</td>
                            <td rowspan="1" style="text-align:center">स्रोत व्यहोर्ने संस्था</td>
                            <td rowspan="1" style="text-align:center">प्रकार</td>
                            <td rowspan="1" style="text-align:center">भुक्तानी विधि</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($voucherDetails as $index => $detail)
                            <tr style="background-color: white;">
                                <td style="text-align: center"><span class="e-n-t-n-n">{{ ++$index }}</span></td>
                                <td style="text-align: center">
                                    @if (
                                        ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 1) ||
                                            ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 9) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 13) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 4) ||
                                            ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 8) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 7))
                                        <span class="e-n-t-n-n">{{ $detail->expense_head->expense_head_code }}</span>
                                    @endif
                                </td>
                                <td style="text-align: center">
                                    @if (
                                        ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 1) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 4) ||
                                            ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 8))
                                        <span class="e-n-t-n-n">
                                            @if ($detail->mainActivity)
                                                {{ $detail->mainActivity->id }}
                                            @endif
                                        </span>
                                    @endif
                                </td>
                                <td>
                                    @if (
                                        ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 6) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 13) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 4) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 7) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 2) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 1) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 3) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 8) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 11))
                                        डे. {{ $detail->ledgerType->name }} @if ($detail->advancePayment)
                                            {{ $detail->advancePayment->name_nep }}
                                            @endif @if ($detail->ledger_type_id == 3)
                                                {{ $detail->bibaran }}
                                                @endif @if ($detail->mainActivity and $detail->ledger_type_id != 3)
                                                    [<font size="1px">{{ $detail->mainActivity->sub_activity }}
                                                    </font>]
                                                @endif
                                            @else
                                                <span style="margin-left: 20px">
                                                    क्रे. {{ $detail->ledgerType->name }}
                                                    @if (
                                                        ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 9) ||
                                                            ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 12))
                                                        @if ($detail->advancePayment)
                                                            {{ $detail->advancePayment->name_nep }}
                                                        @endif
                                                    @endif
                                                    @if (
                                                        ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 3) ||
                                                            ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 2) ||
                                                            ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 8) ||
                                                            ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 12))
                                                        {{ $detail->bibaran }}
                                                    @endif
                                                </span>
                                            @endif
                                </td>
                                <td style="text-align:center">
                                </td>
                                <td style="text-align:center">
                                    @if (
                                        ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 1) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 13) ||
                                            ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 9) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 2) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 4) ||
                                            ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 8))
                                        @if ($detail->mainActivity and $detail->mainActivity->get_source_type)
                                            {{ $detail->mainActivity->get_source_type->name }}
                                        @endif
                                    @endif
                                </td>
                                <td style="text-align:center">
                                    @if (
                                        ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 1) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 13) ||
                                            ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 9) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 2) ||
                                            ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 4) ||
                                            ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 8))
                                        @if ($detail->mainActivity and $detail->mainActivity->get_budget_source_level)
                                            {{ $detail->mainActivity->get_budget_source_level->name }}
                                </td>
                        @endif
                        @endif
                        <td style="text-align:center">
                            @if (
                                ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 1) ||
                                    ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 13) ||
                                    ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 9) ||
                                    ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 2) ||
                                    ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 4) ||
                                    ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 8))
                                @if ($detail->mainActivity and $detail->mainActivity->get_source)
                                    {{ $detail->mainActivity->get_source->name }}
                        </td>
                        @endif
                        @endif
                        <td style="text-align:center">
                            @if (
                                ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 1) ||
                                    ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 13) ||
                                    ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 9) ||
                                    ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 2) ||
                                    ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 4))
                                @if ($detail->mainActivity and $detail->mainActivity->get_medium)
                                    {{ $detail->mainActivity->get_medium->name }}
                        </td>
                        @endif
                        @endif
                        <td class="dr-amount" style="text-align:right">
                            @if ($detail->dr_or_cr == 1)
                                <span class="e-n-t-n-n">@moneyFormat($detail->dr_amount)</span>
                            @endif
                        </td>
                        <td class="cr-amount" style="text-align:right">
                            @if ($detail->dr_or_cr == 2)
                                <span class="e-n-t-n-n">@moneyFormat($detail->cr_amount)</span>
                            @endif
                        </td>
                        </tr>

                        @endforeach
                        <tr>

                            <td colspan="9" style="text-align: right">जम्मा</td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->get_total_dr())</span></td>
                            <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->get_total_cr())</span></td>

                        </tr>
                        <tr>
                            <td colspan="11" style="background-color: white;">जम्मा रकम अक्षरमा
                                : {{ $voucher->get_amount_in_word() }} मात्र
                            </td>

                        </tr>
                        <tr>
                            <td colspan="11" style="background-color: white;">कारोबारको
                                व्यहोरा: {{ $voucher->long_narration }}</td>
                        </tr>
                        <tr>
                            <td colspan="11"> कारोवारको पुश्ट्याई विवरण नं २०२ र अन्य भरी संलग्न गरिएको छ।</td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <table class="table" border="1" width="100%" style="background-color:#dbdbdb; font-size: 10px">
                    <tr>
                        <td colspan="9"><b>भुक्तानी प्रयोजनका लागी</b></td>
                    </tr>
                    <tr>
                        <tbody style="font-size: 10px">
                            <th>क्र.सं.</th>
                            <th>भुक्तानी पाउनेको नाम</th>
                            <th style="width: 5%;">बैंक खाता नं</th>
                            <th>पान नं</th>
                            <th>चेक न.</th>
                            <th>भुक्तानी रकम</th>
                            <th>अक्षरेपी रु</th>
                            <th style="width: 13%;">दस्तखत</th>
                            <th style="width: 3%;">संलग्न कागजात संख्या</th>
                            <th>कैफियत</th>
                        </tbody>
                    </tr>
                    @if ($preBhuktanies->count())
                        @foreach ($preBhuktanies as $index => $preBhuktani)
                            <tr style="background-color: white;">
                                <td style="text-align: center"><span class="e-n-t-n-n">{{ ++$index }}</span>
                                </td>
                                <td>
                                    @if ($preBhuktani->party)
                                        {{ $preBhuktani->party->name_nep }}
                                    @endif
                                </td>
                                <td style="text-align: center">
                                    @if ($preBhuktani->party and $preBhuktani->party->party_khata_number)
                                        <span class="e-n-t-n-n">{{ $preBhuktani->party->party_khata_number }}</span>
                                </td>
                        @endif
                        <td style="text-align: center">
                            @if ($preBhuktani->party and $preBhuktani->party->vat_pan_number)
                                <span class="e-n-t-n-n">{{ $preBhuktani->party->vat_pan_number }}</span>
                        </td>
                    @endif
                    </td>
                    <td style="text-align: center" class="kalimati">{{ $preBhuktani->cheque_print }}</td>
                    <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($preBhuktani->amount)</span></td>
                    <td>{{ $preBhuktani->get_amount_in_word() }} मात्र</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="5" style="text-align: right">जम्मा</td>
                        <td style="text-align: right"><span
                                class="e-n-t-n-n">{{ number_format($total_bhuktani, 2, '.', ',') }}</span></td>
                        <td style="text-align: left">{{ $total_bhuktani_word }} मात्र</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endif
                </table>

                <table width="99%" style="font-size: 13px">
                    <tr>

                        <br>
                        <br>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">तयार गर्ने :
                            @if ($voucher->karmachari_prepare_by and $voucher->karmachari_prepare_by->name_nepali)
                                {{ $voucher->karmachari_prepare_by->name_nepali }}
                            @endif
                        </td>
                        <td>पेश गर्ने :
                            @if ($voucher->karmachari_submit_by and $voucher->karmachari_submit_by->name_nepali)
                                {{ $voucher->karmachari_submit_by->name_nepali }}
                            @endif
                        </td>
                        <td>सदर गर्ने :
                            @if ($voucher->karmachari_approved_by and $voucher->karmachari_approved_by->name_nepali)
                                {{ $voucher->karmachari_approved_by->name_nepali }}
                        </td>
                        @endif
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">पद :
                            @if ($voucher->karmachari_prepare_by)
                                {{ $voucher->karmachari_prepare_by->pad_id }}
                            @endif
                        </td>
                        <td>पद :
                            @if ($voucher->karmachari_submit_by)
                                {{ $voucher->karmachari_submit_by->pad_id }}
                            @endif
                        </td>
                        <td>पद :
                            @if ($voucher->karmachari_approved_by)
                                {{ $voucher->karmachari_approved_by->pad_id }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">मिति : {{ $voucher->data_nepali }}</td>
                        <td>मिति : {{ $voucher->data_nepali }}</td>
                        <td>मिति : {{ $voucher->data_nepali }}</td>
                    </tr>

                </table>
            </div>

        </div>

    </section>

    <!-- /.content -->
</div>

<script>
    $(document).ready(function() {

        let trs = $('#voucher_table tbody').find('tr');
        let crAmount = 0;
        let drAmount = 0;
        $.each(trs, function(key, tr) {
            $.each($(tr).find('td'), function(key_, td) {

                if ($(td).prop('class') == 'dr-amount') {

                    let newDrAmount = $(this).text();
                    // console.log(newDrAmount);
                    if (newDrAmount) {
                        drAmount += parseFloat(newDrAmount);
                    }

                }

                if ($(td).prop('class') == 'cr-amount') {

                    let newCrAmount = $(this).text();
                    // console.log(newCrAmount);
                    if (newCrAmount) {

                        crAmount += parseFloat(newCrAmount);
                    }
                }

            })
        });

        // alert(drAmount);
        // alert(crAmount);

        // $('#cr_bank').text(crAmount);
        // $('#cr_bank_two').text(crAmount);
        // $('#liability-dr').text(dr_liability);
        // $('#liability-cr').text(cr_liability);


    })
</script>

<script>
    let changeToNepali = function(text) {
        let numbers = $.trim(text).split('');
        let nepaliNo = '';
        $.each(numbers, function(key, value) {
            if (value) {
                if (!isNaN(value) && value != ' ') {
                    if (value == 1)
                        nepaliNo += "१";

                    else if (value == 2)
                        nepaliNo += "२";

                    else if (value == 3)
                        nepaliNo += "३";

                    else if (value == 4)
                        nepaliNo += "४";

                    else if (value == 5)
                        nepaliNo += "५";

                    else if (value == 6)
                        nepaliNo += "६";

                    else if (value == 7)
                        nepaliNo += "७";

                    else if (value == 8)
                        nepaliNo += "८";

                    else if (value == 9)
                        nepaliNo += "९";

                    else if (value == 0)
                        nepaliNo += "०";
                } else {
                    nepaliNo += value
                }
            }
        });
        // console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function() {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });
</script>
