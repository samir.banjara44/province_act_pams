<?php
namespace App\Repositories;

use App\Models\Chalani;
use Illuminate\Support\Facades\Auth;

class ChalaniRepositoryEloquent implements ChalaniRepository
{
    public function create($attributes) {
        $chalani = new Chalani();
        $chalani->date = $attributes['date'];
        $chalani->office_id = Auth::user()->office->id;
        $chalani->chalani_number = $attributes['chalani_number'];
        $chalani->letter_number = $attributes['letter_number'];
        $chalani->chalan_date = $attributes['chalan_date'];
        if(array_key_exists('body',$attributes)){
            $chalani->body = $attributes['body'];
        }
        $chalani->to_office = $attributes['to_office'];
        $chalani->type = $attributes['type'];
        $chalani->medium = $attributes['medium'];
        $chalani->to_office_address = $attributes['address'];
        $chalani->purpose = $attributes['purpose'];
        $chalani->branch = $attributes['branch'];
        $chalani->user_id = Auth::user()->id;
        if(array_key_exists('image',$attributes)){
            $chalani->image = $attributes['image'];
        }
        $date = $attributes['date'];
        $myyearfirst = (substr($date, 0, 2));
        $myyearlast = (substr($date, 2, 2));
        $explode = explode('-',$date);
        $month = intval($explode[1]);
        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }
        if (intval($month) >= 4) {
            $chalani->fiscal_year = $myyearfirst . $myyearlast . "/" . (intval($myyearlast) + 1);
        } else {
            $chalani->fiscal_year = $myyearfirst . (intval($myyearlast) - 1) . "/" . $myyearlast;
        }
        $chalani->remarks = $attributes['remarks'];
        $chalani->status = 1;
        $chalani->save();
        return $chalani;
    }
    
    public function update($attributes, $id){
        $chalani = $this->get_by_id($id);
        $chalani->date = $attributes['date'];
        $chalani->office_id = Auth::user()->office->id;
        $chalani->chalan_date = $attributes['chalan_date'];
        if(array_key_exists('body',$attributes)){
            $chalani->body = $attributes['body'];
        }
        $chalani->to_office = $attributes['to_office'];
        $chalani->type = $attributes['type'];
        $chalani->medium = $attributes['medium'];
        $chalani->to_office_address = $attributes['address'];
        $chalani->purpose = $attributes['purpose'];
        $chalani->branch = $attributes['branch'];
        $chalani->user_id = Auth::user()->id;
        if(array_key_exists('image',$attributes)){
            $chalani->image = $attributes['image'];
        }
        $date = $attributes['date'];
        $myyearfirst = (substr($date, 0, 2));
        $myyearlast = (substr($date, 2, 2));
        $explode = explode('-',$date);
        $month = intval($explode[1]);
        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }
        if (intval($month) >= 4) {
            $chalani->fiscal_year = $myyearfirst . $myyearlast . "/" . (intval($myyearlast) + 1);
        } else {
            $chalani->fiscal_year = $myyearfirst . (intval($myyearlast) - 1) . "/" . $myyearlast;
        }
        $chalani->remarks = $attributes['remarks'];
        $chalani->save();
        return $chalani;
    }
    
    public function get_by_office_id(){
        
        $office_id = Auth::user()->office_id;
        return $chalanis = Chalani::where('office_id', $office_id)->where('status','1')->get();
    }

    public function get_by_id($id){
        return $chalanis = Chalani::findorfail($id);
    }

    public function get_last_number_by_office($officeId){
        $chalani = Chalani::where('office_id',$officeId)->latest()->first();

        if($chalani){
           $chalani_number = $chalani->chalani_number;
        } else {

            $chalani_number = 0;
        }
        return $chalani_number + 1;
    }
}