<?php
namespace App\Repositories;

use App\Models\Designation;
use App\Models\Taha;
use Illuminate\Support\Facades\Auth;

class TahaRepositoryEloquent implements TahaRepository
{
    public function create($attributes) {
        $taha = new Taha();
        $taha->name = $attributes['name'];
        $taha->sewa_id = $attributes['sewa_id'];
        $taha->status = 1;
        $taha->save();
        return $taha;
    }

    public function get_all_tahas()
    {
        return Taha::all();
    }

    public function get_by_sewa_id($sewa_id){

        return $tahas = Taha::where('sewa_id',$sewa_id)->get();
    }
}