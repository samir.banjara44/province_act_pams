@extends('frontend.layouts.app')

@section('content')
    <style>
        th {
            text-align: center;
        }
    </style>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                धरौटी भौचर स्वीकृत गर्ने :: खोज
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">

            </div>
            <table width="95%" border="1" id="voucher_list">
                <thead>
                <th>सि.न.</th>
                <th>मिति</th>
                <th>भौचर नं.</th>
                <th>कारोबार रकम</th>
                <th>कारोबारको व्यहोरा</th>
                <th colspan="2">कार्य</th>
                </thead>
                <tbody>
                @if($retentionVouchers->count()>0)
                    @foreach($retentionVouchers as $index=>$retentionVoucher)
                        <tr>
                            <td class="kalimati" align="center">{{++$index}}</td>
                            <td class="kalimati" align="center">{{$retentionVoucher->date_nep}}</td>
                            <td class="kalimati" align="center">{{$retentionVoucher->voucher_number}}</td>
                            <td class="kalimati"
                                align="center">@if($retentionVoucher->amount){{$retentionVoucher->amount}}@endif</td>
                            <td>@if($retentionVoucher->short_narration){{$retentionVoucher->short_narration}}@endif</td>
                            <td> <a href="{{route('retention.voucher.view',$retentionVoucher->id)}}" target="_blank">हर्ने</a>
                                |<a href="{{route('retention.voucher.approved',$retentionVoucher->id)}}"> स्विकृित</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" style="text-align: center">अस्विकृत भौचर भेटिएन</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#budget_sub_head').change(function () {

                let fiscal_year = $('#fiscal_year').val();
                let budget_sub_head = $('#budget_sub_head :selected').val();
                let budget_sub_head_name = $('#budget_sub_head :selected').text();
                let url = '{{route('get_unapproved_voucher',['123','345'])}}';
                url = url.replace(123, fiscal_year);
                url = url.replace(345, budget_sub_head);

                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {
                        $datas = $.parseJSON(res);
                        let i = 1;
                        let tr = '';
                        $.each($datas, function (key, value) {
                            let url = '{{route('voucher.view', '123')}}';
                            url = url.replace('123', this.id);
                            tr += "<tr>" +
                                "<td>" +
                                i +
                                "</td>" +

                                "<td class='activity' data-id=''>" +
                                budget_sub_head_name +
                                "</td>" +

                                "<td class='byahora'>" +
                                this.data_nepali +
                                "</td>" +

                                "<td class='details'>" +
                                this.jv_number +
                                "</td>" +

                                "<td class='drAmount'>" +
                                this.payement_amount +
                                "</td>" +

                                "<td class='drAmount'>" +
                                this.short_narration +
                                "</td>" +

                                "<td>" +
                                '<a href="#" class="voucher-accept" data-id="' + this.id + '">स्विकृत गर्ने</a> | <a href="' + url + '" target=" _blank">हेर्ने</a>' +
                                "</td>";
                            i = i + 1;
                        });
                        $('#voucher_list').find('tbody').last('tr').html(tr);

                    }

                })

            })
        })
    </script>
    <script>
        $(document).on('click', '.voucher-accept', function () {
            $voucher_id = parseInt($(this).attr('data-id'));
            let url = "{{route('set.voucher.stauts.update',123)}}";
            url = url.replace(123, $voucher_id);
            let $tr = $(this).closest('tr');
            $.ajax({

                url: url,
                method: 'get',
                success: function (res) {
                    alert("स्विकृत भयो");
                    $tr.remove();

                }
            })
        })
    </script>


@endsection