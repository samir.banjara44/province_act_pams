<?php
namespace App\Repositories;

use App\Models\Sewa;
use Illuminate\Support\Facades\Auth;

class SewaRepositoryEloquent implements SewaRepository
{
    public function create($attributes) {
        $sewa = new Sewa();

        $sewa->name = $attributes['name'];
        $sewa->status = 1;
        $sewa->save();
        return $sewa;
    }

    public function get_all_sewas()
    {
        return Sewa::all();
    }
}