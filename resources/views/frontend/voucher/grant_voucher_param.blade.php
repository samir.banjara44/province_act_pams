@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-business-time"></i> बजेट उपशीर्षक छान्नुहोस् </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <form action="{{route('grant.voucher')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>आर्थिक वर्ष : </label>
                                        <select name="fiscal_year" id="fiscal_year" class="form-control">
                                            @foreach($fiscalYears as $fy)
                                                <option value="{{$fy->id}}"
                                                        @if($fy->id == $fiscalYear->id) selected @endif>{{$fy->year}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>बजेट उपशिर्षक :</label>
                                        <select name="budget_sub_head" id="budget_sub_head" class="form-control">
                                            <option value="0">..............</option>
                                            @foreach($programs as $program)
                                                <option value="{{$program->id}}">{{$program->name}}
                                                    | {{$program->program_code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3" style="padding-top: 20px;">
                                        <input type="submit" class="btn btn-primary btn-block" value="हेर्ने">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <!-- /.content -->
    </div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function () {
            $('#fiscal_year').change(function () {
                $('#budget_sub_head').val('');
                $('#voucher_list tbody').html('')
            })
        })
    </script>

@endsection