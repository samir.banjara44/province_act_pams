
@extends('backend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Province
                <small>Control panel</small>
            </h1>
            <ul class="breadcrumb">
                <li> <a type="button" href="{{route('admin.province')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('admin.province.store')}}" method="post" id="provinceCreateForm">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" placeholder="Province" name="name">
                        </div>

                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection
@section('scripts')
    <script>
        $("#provinceCreateForm").validate({
            rules: {
                name: {
                    required: true,
                }
            },
            messages: {
                name: "Name Field is Required"
            }
        });
    </script>
@stop