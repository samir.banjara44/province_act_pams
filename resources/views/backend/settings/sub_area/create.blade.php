
@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sub Area
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('admin.sub.area')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('admin.sub.area.store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <label for="area">Area:</label>
              <select class="form-control" name="area" id="area">
                @foreach($areas as $area)
                  <option value="{{$area->id}}">{{$area->name}}</option>
                 @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="name">Sub Area Name:</label>
              <input type="text" class="form-control" id="name" placeholder="Sub Area Name" name="name">
            </div>


            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection