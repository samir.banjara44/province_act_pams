<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Programs;
use App\Repositories\ProgramRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class ProgramsController extends Controller
{

    private $programRepo;

    public function __construct(ProgramRepository $programRepo)
    {
        $this->middleware('auth');
        $this->programRepo = $programRepo;
    }

    public function index()
    {
        $programes= Programs::all();
        return view('backend.settings.programs.index', compact('programes'));
    }

    public function create(){

        return view('backend.settings.programs.create');
    }
    public function store(Request $request){

        $program = $this->programRepo->create($request->all());
        return redirect(route('admin.program'));
    }

    public function edit($id){
        $program = $this->programRepo->get_by_program_id($id);
        return view('backend.settings.programs.edit',compact('program'));
    }

    public function update(Request $request, $id)
    {
        $program = $this->programRepo->update($request->all(),$id);
        return redirect(route('admin.program'));
    }

}
