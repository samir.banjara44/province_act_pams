<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mof extends Model
{
    protected $table = 'ministry_of_finances';
    protected $guarded = [];

    public function province(){
        return $this->belongsTo('App\Models\Province');
    }

    public function ministries(){
        return $this->hasMany('App\Models\Ministries');
    }

    public function departments(){
        return $this->hasMany('App\Models\Department');
    }


    public function offices(){
        return $this->hasMany('App\Models\Office');
    }

    public function users(){
        return $this->hasMany('App\Users');
    }

}
