
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> <i class="fas fa-edit"></i> बैङ्कको विवरण सम्पादन </h1>
      
      <a href="{{route('bank')}}" class="btn btn-sm btn-primary btn-show-form"><i class="fa fa-arrow-left"></i> लिष्टमा जाने</a>
    </section>

    <!-- FORM section start -->
  <section class="formSection">
    <div class="row justify-content-md-center">
      <div class="col-md-8 form-bg col-md-offset-2">
        <div class="form-title">बैङ्क विवरण सम्पादन गर्ने</div>
        <form action="{{route('bank.update', $bank->id)}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <div class="row">
                <div class="col-md-6">
                  <label for="name">बैङ्कको नाम:</label>
                  <input type="text" class="form-control" id="name" placeholder="बैङ्कको नाम" name="name" value="{{$bank->name}}" required>
                </div>

                <div class="col-md-6">
                  <label for="address">ठेगाना</label>
                  <input type="text" class="form-control" id="address" placeholder="ठेगाना" name="address" value="{{$bank->address}}" required>
                </div>

              </div>
            </div>
            
            <center>
              <button type="submit" class="btn btn-success btn-show-form"><i class="fas fa-check-square"></i> सम्पादन गर्ने</button>
            </center>
          </form>
      </div>
    </div>
  </section>
  <!-- FORM section ends -->

  </div>

@endsection