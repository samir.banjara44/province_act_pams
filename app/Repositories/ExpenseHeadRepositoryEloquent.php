<?php
namespace App\Repositories;

use App\Models\ExpenseHead;
use App\Models\Budget;
use App\Models\VoucherDetail;
use Illuminate\Support\Facades\Auth;

class ExpenseHeadRepositoryEloquent implements ExpenseHeadRepository
{

    public function getAllExpenseHead(){

       return $areas = ExpenseHead::all();

    }

    public function getById($expense_head_id){

        return $expense_head = ExpenseHead::findorfail($expense_head_id);
    }
    public function create($attributes) {
        $program = new ExpenseHead();
        $program->name = $attributes['name'];
        $program->status = 1;
        $program->save();
        return $program;
    }

    public function get_by_activity_id($activity_id){

        return $expenseHeads = ExpenseHead::where('expense_head_code',$activity_id)->get();

    }
    public function get_by_expense_head_code($expense_head_code){


        return $expenseHeads = ExpenseHead::where('expense_head_code',$expense_head_code)->first();
    }


    public function get_by_ledget_type($ledger_type){

        return $expenseHeads = ExpenseHead::where('ledger_type',$ledger_type)->get();
    }

    public function get_expense_head_by_ledger_type(){

        $ledger_type = 1;
        return $expenseHeadForBudget = ExpenseHead::where('ledger_type',$ledger_type)->where('status',1)
            ->orderBy('expense_head_code','asc')
            ->get();
    }

    public function get_by_id($id){


//        $expense_head = ExpenseHead::select('expense_head_code')->where('id',$id)->get();
      return $expense_head = ExpenseHead::findorfail($id, ['expense_head_code']);

    }

    public function get_expense_head_by_budget_sub_head($budget_sub_head_id){

        $voucherDetails = VoucherDetail::where('budget_sub_head_id',$budget_sub_head_id)
                                        ->where('dr_or_cr',1)
                                        ->where('ledger_type_id',7)
                                        ->groupBy('expense_head_id')
                                        ->get();
        $expenseHead = [];
        foreach ($voucherDetails as $voucherDetail){

            $temp = [];
            $expenseHeads = ExpenseHead::findorfail($voucherDetail->expense_head_id);
            $temp['id'] = $voucherDetail->expense_head_id;
            $temp['expense_head_code'] = $expenseHeads->expense_head_code;
            $temp['expense_head_sirsak'] = $expenseHeads->expense_head_sirsak;
            array_push($expenseHead,$temp);
        }
         return $expenseHead;

    }
}