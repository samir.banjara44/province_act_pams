<?php

namespace App\Http\Controllers\Frontend;

use App\helpers\FiscalYearHelper;
use App\Http\Controllers\Controller;
use App\Models\Budget;
use App\Models\ExpenseHead;
use App\Models\Area;
use App\Models\Month;
use App\Models\Office;
use App\Models\Source;
use App\Models\SourceType;
use App\Models\SubArea;
use App\Models\Voucher;
use App\Repositories\BudgetRepository;
use App\Repositories\ExpenseHeadRepository;
use App\Repositories\MainProgramRepository;
use App\Repositories\AreaRepository;
use App\Repositories\PreBhuktaniRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\ProgramRepositoryEloquent;
use App\Repositories\SourceRepository;
use App\Repositories\SourceTypeRepository;
use App\Repositories\SubAreaRepository;
use App\Repositories\VoucherDetailsRepository;
use App\Repositories\VoucherRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class SuperVisionController extends Controller
{


    private $expenseHeadRepo;
    private $programRepo;
    private $budgetRepo;
    private $programRrepo;
    private $voucheDetailsRrepo;
    private $voucherrepo;
    private $sourceRepo;
    private $sourcetypeRepo;


    public function __construct(

        ProgramRepository $programRepo,
        BudgetRepository $budgetRepo,
        ProgramRepositoryEloquent $programRrepo,
        VoucherRepository $voucherrepo,
        VoucherDetailsRepository $voucheDetailsRrepo,
        PreBhuktaniRepository $preBhuktaniRepository,
        SourceRepository $sourceRepo,
        SourceTypeRepository $sourcetypeRepo


    )

    {
        $this->middleware('auth');
        $this->fiscalYearHelper = new FiscalYearHelper();
        $this->programRepo = $programRepo;
        $this->budgetRepo = $budgetRepo;
        $this->programRrepo = $programRrepo;
        $this->voucherrepo = $voucherrepo;
        $this->voucheDetailsRrepo = $voucheDetailsRrepo;
        $this->sourceRepo = $sourceRepo;
        $this->sourcetypeRepo = $sourcetypeRepo;


    }

    public function getAllOfficeListUnderMinistry($ministryId)
    {

        $office_id = Auth::user()->office_id;
        $officeLists = Office::where('ministry_id', $ministryId)->orderBy('office_code')->get();
        return view('frontend.super_vision.office_list', compact('officeLists'));


    }

    public function getUsedOfficeParameter($ministryId){

        $officeUsedLists = Office::where('ministry_id', $ministryId)->orderBy('office_code')->whereHas('vouchers')->get();
        $officeNotUsedLists = Office::where('ministry_id', $ministryId)->orderBy('office_code')->doesntHave('vouchers')->get();
        $totalProvinceBudget = $this->budgetRepo->getTotalbudgetOfMinistry($ministryId);
        $totalExpMinistry = $this->voucheDetailsRrepo->getTotalExpenseByMinistry($ministryId);
        $totalPercent = ((float)$totalExpMinistry *100)/(float)$totalProvinceBudget;
        return view('frontend.super_vision.office_used_list', compact('officeUsedLists','officeNotUsedLists','totalProvinceBudget','totalExpMinistry','totalPercent'));
    }

    public function getAllActivitiesParameter($ministryId)
    {

        $office_id = Auth::user()->office_id;
        $officeLists = Office::where('ministry_id', $ministryId)->get();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.super_vision.activity_lists_parameter', compact('officeLists', 'fiscalYear'));


    }

    public function getAllBudgetSubHead($fiscal_year,$office_id)
    {
        $budgetSubHeadList = $this->programRepo->get_by_office_id_for_ministry($office_id,$fiscal_year);
        return (json_encode($budgetSubHeadList));
    }

    public function activitiesByProgramId(Request $request)
    {

        $data = $request->all();
        $fiscal_year = $this->fiscalYearHelper->get_fiscal_by_id($data['fiscal_year']);
        if($data['officeId'] == 123){
            $offices = Office::where('ministry_id', 4)->get();
            $ministrywise_total_budget = $this->budgetRepo->getFinalBudgetByMinistry($data['ministry']);
//            $program = $this->programRepo->get_by_program_id($data['budgetSUbHead']);
//            $activities = $this->budgetRepo->get_activities_by_budget_sub_head_and_budget_type($data['budgetSUbHead'], $data['officeId']);
//            $budget_sum = $activities->sum('total_budget');
//            $first_quarter_budget_total = $activities->sum('first_quarter_budget');
//            $second_quarter_budget_total = $activities->sum('second_quarter_budget');
//            $third_quarter_budget_total = $activities->sum('third_quarter_budget');
//
//            $total_first_quarter_expense = $this->voucheDetailsRrepo->getFirstChaumasikTotalExpense($data['officeId'], $data['budgetSUbHead'], $data['fiscal_year']);
//            $total_second_quarter_expense = $this->voucheDetailsRrepo->getSecondChaumasikTotalExpense($data['officeId'], $data['budgetSUbHead'], $data['fiscal_year']);
//            $total_third_quarter_expense = $this->voucheDetailsRrepo->getThirdChaumasikTotalExpense($data['officeId'], $data['budgetSUbHead'], $data['fiscal_year']);
//            $totalExpenseByBudgetSubHead = (float)$total_first_quarter_expense + (float)$total_second_quarter_expense + (float)$total_third_quarter_expense;
            return view('frontend.super_vision.allActivityList', compact('data', 'activities', 'program', 'offices','ministrywise_total_budget' ,'budget_sum', 'first_quarter_budget_total', 'second_quarter_budget_total', 'third_quarter_budget_total', 'total_first_quarter_expense', 'total_second_quarter_expense', 'total_third_quarter_expense', 'totalExpenseByBudgetSubHead'));

        }
        $budget_sub_head_id = $data['budgetSUbHead'];
        $office = Office::where('id', $data['officeId'])->get();
        $program = $this->programRepo->get_by_program_id($data['budgetSUbHead']);
        $activities = $this->budgetRepo->get_activities_by_budget_sub_head_and_budget_type($data['fiscal_year'],$data['budgetSUbHead'], $data['officeId']);
        $budget_sum = $activities->sum('total_budget');
        $first_quarter_budget_total = $activities->sum('first_quarter_budget');
        $second_quarter_budget_total = $activities->sum('second_quarter_budget');
        $third_quarter_budget_total = $activities->sum('third_quarter_budget');

        $total_first_quarter_expense = $this->voucheDetailsRrepo->getFirstChaumasikTotalExpense($data['officeId'], $data['budgetSUbHead'], $data['fiscal_year']);
        $total_second_quarter_expense = $this->voucheDetailsRrepo->getSecondChaumasikTotalExpense($data['officeId'], $data['budgetSUbHead'], $data['fiscal_year']);
        $total_third_quarter_expense = $this->voucheDetailsRrepo->getThirdChaumasikTotalExpense($data['officeId'], $data['budgetSUbHead'], $data['fiscal_year']);
        $totalExpenseByBudgetSubHead = (float)$total_first_quarter_expense + (float)$total_second_quarter_expense + (float)$total_third_quarter_expense;
        return view('frontend.super_vision.activityList', compact('fiscal_year','data', 'activities', 'program', 'office', 'budget_sum', 'first_quarter_budget_total', 'second_quarter_budget_total', 'third_quarter_budget_total', 'total_first_quarter_expense', 'total_second_quarter_expense', 'total_third_quarter_expense', 'totalExpenseByBudgetSubHead'));

    }

//    फटावारी
    public function fatwariParam($ministryId)
    {
        $office_id = Auth::user()->office_id;
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $officeLists = Office::where('ministry_id', $ministryId)->get();

        return view('frontend.super_vision.fatwari_parameter', compact('fiscalYear', 'officeLists','fiscalYears'));
    }

    public function fatwari(Request $request)
    {
        $datas = $request->all();
        if($datas['office']== 123 and $datas['month'] = 123){
            set_time_limit(300);
            $offices = Office::where('ministry_id', 4)->get();
            $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($datas['fiscal_year']);
            $ministryWiseTotalBudget = $this->budgetRepo->getFinalBudgetByMinistry($datas['ministry']);
            $ministryWiseTotalExpense = $this->voucheDetailsRrepo->getTotalExpense($datas['ministry']);
            $expenseInPercentage = (float)($ministryWiseTotalExpense *100)/$ministryWiseTotalBudget;
            $totalExpeRound = number_format( $expenseInPercentage,2, '.', '');
            $totalremainBudget = (float)$ministryWiseTotalBudget - (float)$ministryWiseTotalExpense;

            return view('frontend.super_vision.fatwari_all_office_all_month', compact('year','datas', 'fiscalYear', 'offices', 'month_name', 'month_post', 'program', 'voucherDetails', 'month_id', 'upToLastMonthExp', 'upToThisMonthExp', 'thisMonthExp', 'expense_with_out_peski', 'budgetByExpenseHeads','ministryWiseTotalBudget','ministryWiseTotalExpense','totalExpeRound','totalremainBudget'));

        } else {
            $budget_sub_head_id = $datas['budget_sub_head'];
            $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);
            $office = Office::where('id', $datas['office'])->get();
            $month_post = $datas['month'];
            $month = Month::findorfail($month_post);
            if ($month) {

                $month_name = ($month->name);
                $month_id = $month->id;
            }
            $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($datas['fiscal_year']);
            if ($month_post <= 9) {

                $year = substr($fiscalYear->year, 0, 4);
            } else {

                $year = substr($fiscalYear->year, 0, 4) + 1;
            }
            $budgetByExpenseHeads = $this->voucheDetailsRrepo->get_expense_in_budget_for_ministry($budget_sub_head_id, $fiscalYear, $datas['office']);
            $upToLastMonthVoucherDetailList = $this->voucheDetailsRrepo->searchByGroupBy(['budget_sub_head' => $budget_sub_head_id, 'fiscalYear' => $fiscalYear, 'less_than_this_month' => $month_post, 'status' => 1, 'office_id' => $datas['office']]);
            $upToThisMonthVoucherDetailList = $this->voucheDetailsRrepo->searchByGroupBy(['budget_sub_head' => $budget_sub_head_id, 'fiscalYear' => $fiscalYear, 'up_to_this_month' => $month_post, 'status' => 1, 'office_id' => $datas['office']]);
            $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->searchByGroupBy(['budget_sub_head' => $budget_sub_head_id, 'fiscalYear' => $fiscalYear, 'month' => $month_post, 'status' => 1, 'office_id' => $datas['office']]);
            if ($thisMonthVoucherDetailList) {

                $upToLastMonthExp = $this->voucheDetailsRrepo->get_all_field_total($upToLastMonthVoucherDetailList,$datas);
                $upToThisMonthExp = $this->voucheDetailsRrepo->get_all_field_total($upToThisMonthVoucherDetailList,$datas);
                $thisMonthExp = $this->voucheDetailsRrepo->get_all_field_total($thisMonthVoucherDetailList,$datas);
                $upToThisMonthpeski = $this->voucheDetailsRrepo->get_all_field_total($upToThisMonthVoucherDetailList,$datas);
                $expense_with_out_peski = ($upToThisMonthExp['expense'] - $upToThisMonthExp['peski']);
                return view('frontend.super_vision.fatwari', compact('year','datas', 'fiscalYear', 'office', 'month_name', 'month_post', 'program', 'voucherDetails', 'month_id', 'upToLastMonthExp', 'upToThisMonthExp', 'thisMonthExp', 'expense_with_out_peski', 'budgetByExpenseHeads'));

            }
        }

    }

    //स्रोत र खाता अनुसार
    public function reportOnSourceAndLedgerParam($ministry_id)
    {

        $office_id = Auth::user()->office_id;
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $officeLists = Office::where('ministry_id', $ministry_id)->get();
        return view('frontend.super_vision.report_on_source_and_ledger', compact('fiscalYear', 'officeLists'));

    }

    public function reportOnSourceAndLedger(Request $request)
    {
        $data = $request->all();
        $officeId = $data['officeId'];
        $fiscal_year = $data['fiscal_year'];
        $ministry_id = Auth::user()->ministry_id;
        $today = date('Y-m-d H:i:s');
        if ($officeId == '1212') {

            $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
            $officeLists = Office::where('ministry_id', $ministry_id)->get();
// Province Budget
            $totalProvinceChaluBudget = $this->budgetRepo->getTotalProvinceChaluBudget($ministry_id);
            $totalProvincePujiBudget = $this->budgetRepo->getTotalProvincePujiBudget($ministry_id);
            $totalProvinceBudget = $this->budgetRepo->getTotalProvinceBudget($ministry_id);

// Federal Budget
            $totalFederalChaluBudget = $this->budgetRepo->getTotalFederalChaluBudget($ministry_id);
            $totalFederalPujiBudget = $this->budgetRepo->getTotalFederalPujiBudget($ministry_id);
            $totalFederalBudget = $this->budgetRepo->getTotalFederalBudget($ministry_id);

//            Province Expense
            $totalProvinceChaluExp = $this->voucheDetailsRrepo->gettotalProvinceChaluExpense($ministry_id);
            $totalProvincePujiExp = $this->voucheDetailsRrepo->gettotalProvincePujiExpense($ministry_id);
            $totalProvinceExp = $this->voucheDetailsRrepo->gettotalProvinceExpense($ministry_id);

//            Federal Expense
            $totalFederalChaluExp = $this->voucheDetailsRrepo->getTotalFederalChaluExpense($ministry_id);
            $totalFederalPujiExp = $this->voucheDetailsRrepo->getTotalFederalPujiExpense($ministry_id);
            $totalFederalExp = $this->voucheDetailsRrepo->getTotalFederalExpense($ministry_id);

//            Total Budget
            $totalBudget = $this->budgetRepo->getTotalbudgetOfMinistry($ministry_id);

//            Total Expense

            $totalExp = $this->voucheDetailsRrepo->getTotalExpense($ministry_id);
            $remainBudget = (float)$totalBudget - (float)$totalExp;
            return view('frontend.super_vision.report_on_source_and_ledger_all', compact('fiscal_year', 'today', 'officeLists', 'totalProvinceChaluBudget', 'totalProvincePujiBudget', 'totalProvinceBudget', 'totalFederalChaluBudget', 'totalFederalPujiBudget', 'totalFederalBudget', 'totalBudget', 'totalProvinceChaluExp', 'totalProvincePujiExp', 'totalProvinceExp', 'totalFederalChaluExp', 'totalFederalPujiExp', 'totalFederalExp', 'totalExp', 'remainBudget'));

        } else {

            $office = Office::where('id', $officeId)->get();

            $totalProvinceChaluBudget = $this->budgetRepo->totalProvinceChaluBudgetByOfficeId($data['officeId']);
            $totalProvincePujiBudget = $this->budgetRepo->totalProvincePujiBudgetByOfficeId($data['officeId']);
            $totalProvinceBudgetByOffice = (float)$totalProvinceChaluBudget + (float)$totalProvincePujiBudget;


//            Federal
            $totalFederalChaluBudget = $this->budgetRepo->totalFederalChaluBudgetByOfficeId($data['officeId']);
            $totalFederalPujiBudget = $this->budgetRepo->totalFederalPujiBudgetByOfficeId($data['officeId']);
            $totalFederalBudgetByOffice = (float)$totalFederalChaluBudget + (float)$totalFederalPujiBudget;

//            Total Budget By Office
            $totalBudget = (float)$totalProvinceBudgetByOffice + (float)$totalFederalBudgetByOffice;


//            Expense
//            Province
            $totalProvinceChaluExpnese = $this->voucheDetailsRrepo->totalProvinceChaluExpenseByOffice($data['officeId']);
            $totalProvincePujiExpnese = $this->voucheDetailsRrepo->totalProvincePujiExpenseByOffice($data['officeId']);
            $totalProvinceExpByOffice = (float)$totalProvinceChaluExpnese + (float)$totalProvincePujiExpnese;

//            Federal
            $totalFederalChaluExpnese = $this->voucheDetailsRrepo->totalFederalChaluExpenseByOffice($data['officeId']);
            $totalFederalPujiExpnese = $this->voucheDetailsRrepo->totalFederalPujiExpenseByOffice($data['officeId']);
            $totalFederalExpByOffice = (float)$totalFederalChaluExpnese + (float)$totalFederalPujiExpnese;


//            Total expense
            $totalExpenseByOffice = (float)$totalProvinceExpByOffice + (float)$totalFederalExpByOffice;


            $remainBudgetByOffice = (float)$totalBudget - (float)$totalExpenseByOffice;
            return view('frontend.super_vision.report_on_source_and_ledger_office_wise', compact('fiscal_year', 'officeLists', 'office', 'totalProvinceChaluBudget', 'totalProvincePujiBudget', 'totalProvinceBudgetByOffice', 'totalFederalChaluBudget', 'totalFederalPujiBudget', 'totalFederalBudgetByOffice', 'totalBudget', 'totalProvinceChaluExpnese', 'totalProvincePujiExpnese', 'totalProvinceExpByOffice', 'totalFederalChaluExpnese', 'totalFederalPujiExpnese', 'totalFederalExpByOffice', 'totalExpenseByOffice', 'remainBudgetByOffice'));

        }

    }

//    खर्च शिर्षक अनुसार
    public function reportExpenseHeadWiseReportParam($ministry_id)
    {

        $office_id = Auth::user()->office_id;
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $officeLists = Office::where('ministry_id', $ministry_id)->get();
        return view('frontend.super_vision.report_on_expense_head_param', compact('fiscalYear', 'officeLists'));
    }

    public function reportExpenseHeadWiseReport(Request $request)
    {

        $datas = $request->all();
        if ($datas['officeId'] == 123) {

            $voucherExpenseHeads = $this->voucheDetailsRrepo->get_expense_in_budget_by_ministry($datas['ministry']);
            $totalFinalBudget = $this->budgetRepo->getFinalBudgetByMinistry($datas['ministry']);
            $totalExpenseUpToYesterday = $this->voucheDetailsRrepo->getTotalExpenseUpToYesterday($datas['ministry']);
            $totalTodayExpense = $this->voucheDetailsRrepo->gettotalTodayExpense($datas['ministry']);
            $totalExpenseUpToToday = $this->voucheDetailsRrepo->getTotalExpenseUpToToday($datas['ministry']);
            $totalAdvance = $this->voucheDetailsRrepo->getTotalAdvance($datas['ministry']);
            $expenseWithOutAdvance = (float)$totalExpenseUpToToday - (float)$totalAdvance;
            $totalremainBudget = (float)$totalFinalBudget - (float)$totalExpenseUpToToday;
            return view('frontend.super_vision.report_on_expense_head_all_office', compact('year','officeLists', 'voucherExpenseHeads', 'datas', 'totalFinalBudget', 'totalExpenseUpToYesterday', 'totalTodayExpense', 'totalExpenseUpToToday', 'totalAdvance', 'expenseWithOutAdvance', 'totalremainBudget'));

        }

        if ($datas['officeId'] != 123 and $datas['budgetSUbHead'] == 123) {

            $budgetSubHeadLists = $this->programRepo->get_by_office_id_for_ministry($datas['officeId']);
            $office = Office::where('id', $datas['officeId'])->get();
            $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();

            $totalFinalBudget = $this->budgetRepo->getFinalBudget($datas['officeId']);
            $totalExpense = $this->voucheDetailsRrepo->getTotalExpenseByOffice($datas['officeId']);
            $totalExpenseUpToYesterday = $this->voucheDetailsRrepo->getTotalExpenseUpToYesterdayByOffice($datas['officeId']);
            $totalExpenseToday = $this->voucheDetailsRrepo->getTotalExpenseTodayByOffice($datas['officeId']);
            $totalExpenseUpToToday = $this->voucheDetailsRrepo->getTotalExpenseUpToTodayByOffice($datas['officeId']);
            $totalAdvance = $this->voucheDetailsRrepo->getTotalAdvanceByOffice($datas['officeId']);
            $expenseWithOutAdvance = (float)$totalExpense - (float)$totalAdvance;
            $totalremainBudget = (float)$totalFinalBudget - (float)$totalExpense;
            return view('frontend.super_vision.report_on_expense_head_not_all_office_all_budgetSubHead', compact('fiscalYear', 'budgetSubHeadLists', 'office', 'officeLists', 'voucherExpenseHeads', 'datas', 'totalFinalBudget', 'totalExpenseUpToYesterday', 'totalExpense', 'totalExpenseToday', 'totalExpenseUpToToday', 'totalAdvance', 'expenseWithOutAdvance', 'totalremainBudget'));
        } else {

            $fiscalYear = $datas['fiscal_year'];
            $office = Office::where('id', $datas['officeId'])->get();
            $program = $this->programRepo->get_by_program_id($datas['officeId']);
            $voucherExpenseHeads = $this->voucheDetailsRrepo->get_expense_in_budget_for_ministry($datas['budgetSUbHead'], $datas['fiscal_year'], $datas['officeId']);
            $totalFinalBudget = $this->budgetRepo->getFinalBudgetByBudgetSubHead($datas['officeId'], $datas['budgetSUbHead']);
            $totalExpense = $this->voucheDetailsRrepo->getTotalExpenseByBudgetSubHead($datas['officeId'], $datas['budgetSUbHead']);
            $totalExpenseUpToYesterday = $this->voucheDetailsRrepo->getTotalExpenseUpToYesterdayByBudgetSubHead($datas['officeId'], $datas['budgetSUbHead']);
            $totalExpenseToday = $this->voucheDetailsRrepo->getTotalExpenseTodayByBudgetSubHead($datas['officeId'], $datas['budgetSUbHead']);
            $totalExpenseUpToToday = $this->voucheDetailsRrepo->getTotalExpenseUpToTodayByBudgetSubHead($datas['officeId'], $datas['budgetSUbHead']);
            $totalAdvance = $this->voucheDetailsRrepo->getTotalAdvanceByByBudgetSubHead($datas['officeId'], $datas['budgetSUbHead']);
            $expenseWithOutAdvance = (float)$totalExpense - (float)$totalAdvance;
            $totalremainBudget = (float)$totalFinalBudget - (float)$totalExpense;

            return view('frontend.super_vision.report_on_expense_head_not_all_office_not_all_budgetSubHead', compact('fiscalYear', 'program', 'budgetSubHeadLists', 'office', 'officeLists', 'voucherExpenseHeads', 'datas', 'totalFinalBudget', 'totalExpenseUpToYesterday', 'totalExpense', 'totalExpenseToday', 'totalExpenseUpToToday', 'totalAdvance', 'expenseWithOutAdvance', 'totalremainBudget'));

        }

    }


    public function reportOnExpenseHeadWiseBudgetAndExpenseParam($ministryId)
    {

        $office_id = Auth::user()->office_id;
        $officeLists = Office::where('ministry_id', $ministryId)->get();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.super_vision.report_on_expense_head_wise_budget_and_expense_param', compact('officeLists', 'fiscalYear'));

    }

    public function reportOnExpenseHeadWiseBUdgetAndExpense(Request $request)
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.super_vision.report_on_expense_head_wise_budget_and_expense', compact('officeLists', 'fiscalYear'));

    }


    public function reportOnSourceWiseAnnualReportParam($ministryId)
    {

        $office_id = Auth::user()->office_id;
        $officeLists = Office::where('ministry_id', $ministryId)->get();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.super_vision.report_on_source_wise_annual_report_param', compact('officeLists', 'fiscalYear'));

    }

    public function reportOnSourceWiseAnnualReport(Request $request)
    {
        $datas = $request->all();
        if ($datas['officeId'] == '123') {

            $office_id = Auth::user()->office_id;
            $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();

            $officeLists = Office::where('ministry_id', $datas['ministry'])->whereHas('budget')->get();
            $granTotalInitialBudget = $this->budgetRepo->getTotalInitialBudgetByMinistry($datas['ministry']);
            $granTotalAddBudget = $this->budgetRepo->getTotalAddBudgetByMinistry($datas['ministry']);
            $granReduceAddBudget = $this->budgetRepo->getTotalReduceBudgetByMinistry($datas['ministry']);
            $finalBudget = $this->budgetRepo->getFinalBudgetByMinistry($datas['ministry']);
            $totalExpense = $this->voucheDetailsRrepo->getTotalExpenseByMinistry($datas['ministry']);
            $totalRemainBudget = (float)$finalBudget - (float)$totalExpense;

            return view('frontend.super_vision.report_on_source_wise_annual_report_all_office', compact('fiscalYear', 'datas', 'officeLists', 'budgetSubHeadList', 'granTotalInitialBudget', 'granTotalAddBudget', 'granReduceAddBudget', 'finalBudget', 'totalExpense', 'totalRemainBudget'));

        }
        if ($datas['budgetSUbHead'] == 1234 and $datas['source_type'] == 123) {

            $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
            $office = Office::where('id', $datas['officeId'])->get();
            $budgetSubHeadLists = $this->programRepo->get_by_office_id_for_ministry($datas['officeId']);
            $granTotalInitialBudget = $this->budgetRepo->getTotalInitialBudgetByOffice($datas['officeId']);
            $granTotalAddBudget = $this->budgetRepo->getTotalAddBudgetByOffice($datas['officeId']);
            $granReduceAddBudget = $this->budgetRepo->getTotalReduceBudgetByOffice($datas['officeId']);
            $finalBudget = $this->budgetRepo->getFinalBudget($datas['officeId']);
            $totalExpense = $this->voucheDetailsRrepo->getTotalExpenseByOffice($datas['officeId']);
            $totalRemainBudget = (float)$finalBudget - (float)$totalExpense;
            return view('frontend.super_vision.report_on_source_wise_annual_report_all_budgetSubHead_all_source', compact('fiscalYear', 'office', 'budgetSubHeadLists', 'datas', 'granTotalInitialBudget', 'granTotalAddBudget', 'granReduceAddBudget', 'finalBudget', 'totalExpense', 'totalRemainBudget'));

        }
        if ($datas['budgetSUbHead'] != 1234 and $datas['source_type'] == 123) {

            $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
            $program = $this->programRepo->get_by_program_id($datas['budgetSUbHead']);
            $office = Office::where('id', $datas['officeId'])->get();
            $sourceTypes = $this->sourcetypeRepo->getSourceByOfficeAndBudgetSubHead($datas['officeId'], $datas['budgetSUbHead']);
            return view('frontend.super_vision.report_on_source_wise_annual_report_not_all_budgetSubHead_but_all_source', compact('fiscalYear', 'program', 'office', 'sourceTypes', 'datas'));

        }
        if ($datas['budgetSUbHead'] != 1234 and $datas['source_type'] != 123) {

            $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
            $program = $this->programRepo->get_by_program_id($datas['budgetSUbHead']);
            $office = Office::where('id', $datas['officeId'])->get();
            $sourceTypes = $this->sourcetypeRepo->getSourceByOfficeAndBudgetSubHeadAndSourceType($datas['officeId'], $datas['budgetSUbHead'], $datas['source_type']);
            return view('frontend.super_vision.report_on_source_wise_annual_report_not_all_budgetSubHead_not_all_source', compact('fiscalYear', 'program', 'office', 'sourceTypes', 'datas'));
        }
    }


}
