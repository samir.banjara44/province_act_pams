<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">

<style>
    table {
        border-collapse: collapse;
        font-size: 13px;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table class="kalimati" width="99%">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
                    सभा @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6"
                    style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    @if(Auth::user()->office->department)
                        {{Auth::user()->office->department->name}}
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}, , {{Auth::user()->office->district->name}}</td>
            </tr>
            <tr>
                <td colspan="4" style="padding-left:642px; padding-top: 17px;">
                </td>
                <td style="text-align: right;">म.ले.प.फा.नं. २२</td>
            </tr>
            <tr>
                <td>
                    <br>
                    <br>

                </td>
            </tr>
            <tr style="float: left">
                <td class="kalimati">आ.व. : {{$data['fiscal_year']}}</td>
                <td class="kalimati">खाता :सबै</td>

            </tr>

        </table>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table" id="liability_report_table" width="100%" border="1"
                       style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th>सि.न.</th>
                        <th>शिर्षक</th>
                        <th>डेबिट</th>
                        <th>क्रेडिट</th>
                        <th>बाँकी</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($liabilityHeads as $index=>$liabilityHeads)
                        <tr class="kalimati" style="background-color: white;">
                            <td class="kalimati" style="text-align: center">{{++$index}}</td>
                            <td style="text-align: left">{{$liabilityHeads->expense_head->expense_head_sirsak}}</td>
                            <td class="dr-amount kalimati"
                                style="text-align: right">{{number_format($liabilityHeads->total_dr_amount,2)}}</td>
                            <td class="cr-amount kalimati"
                                style="text-align: right">{{number_format($liabilityHeads->total_cr_amount,2)}}</td>
                            <td class="remain" style="text-align: right"></td>
                        </tr>
                    @endforeach
                    <tr class="kalimati">
                        <td colspan="2" style="text-align: right">जम्मा</td>
                        <td class="kalimati" style="text-align: right">{{number_format($totalDrAmount,2)}}</td>
                        <td class="kalimati" style="text-align: right">{{number_format($totalCrAmount,2)}}</td>
                        <td class="kalimati" style="text-align: right">{{$remain_total}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

{{--  Dr and Cr खो हिसाब काताब  --}}
<script>
    $(document).ready(function () {

        let trs = $('#liability_report_table').find('tr').not(':last');
        let drEng = 0;
        let crEng = 0;
        let dr = 0;
        let cr = 0;
        $.each(trs, function () {
            let remain = 0;
            let drAmount = ($.trim($(this).find('td.dr-amount').text()));
            drAmount = parseFloat(drAmount.replace(/,/g, ""));
            dr += drAmount;

            let crAmount = ($.trim($(this).find('td.cr-amount').text()));
            crAmount = parseFloat(crAmount.replace(/,/g, ""));
            cr += crAmount;
            remain = drAmount - crAmount;
            if (remain < 0) {
                $(this).find('td.remain').text('(' + Math.abs(remain).toFixed(2) + ')').addClass('kalimati');
            } else if (parseFloat(remain)) {
                $(this).find('td.remain').text(remain.toFixed(2)).addClass('kalimati');
            }
        });

        changeAllEntnn();
    })
</script>

{{--change to nep --}}
<script>
    let changeToNepali = function (text) {
        (text = $.trim(text));
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (isNaN(value)) {
                    nepaliNo += value
                } else if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '/')

                    nepaliNo += "/";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '_')

                    nepaliNo += "_";
            }
        });
        console.log(nepaliNo);
        return nepaliNo;
    };
    let changeAllEntnn = function () {
        $('.e-n-t-n-n').each(function () {
            let nepaliNo = changeToNepali($(this).text());
            $(this).text(nepaliNo);
        });
    };
    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        $(this).text(nepaliNo);
    });


</script>


{{--nep to english--}}
<script>
    function convertNepaliToEnglish(input) {
        console.log(input);
        var charArray = input.split('.');
        charArray = charArray[0].split('');
        // console.log('Test',charArray[0]);
        var engDate = '';
        $.each(charArray, function (key, value) {

            switch (value) {
                case '१':
                    engDate += '1';
                    break;
                case '२':
                    engDate += '2';
                    break;
                case '३':
                    engDate += '3';
                    break;
                case '४':
                    engDate += '4';
                    break;
                case '५':
                    engDate += '5';
                    break;
                case '६':
                    engDate += '6';
                    break;
                case '०':
                    engDate += '0';
                    break;
                case '७':
                    engDate += '7';
                    break;
                case '८':
                    engDate += '8';
                    break;
                case '९':
                    engDate += '9';
                    break;

                case '-':
                    engDate += '-';
                    break
            }
            //console.log(engDate)
        });

        return engDate

    }
</script>
