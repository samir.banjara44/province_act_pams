<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>प्रदेश लेखा व्यवस्थापन प्रणाली</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- stopwatch -->
    
   <script src="{{ asset('js/jquery.v1.11.3/jquery.min.js') }}"></script>

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-4.0.0/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mukta:500,600&display=swap" rel="stylesheet"> 
    {{-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> --}}
    <style>
        .login_page_bg{
            background: url('img/bg_img.jpeg');
            font-family: 'Mukta', sans-serif;
        }
        
        .login_page{
            background: linear-gradient(90deg, rgba(121,121,9,0.6320728120349702) 0%, rgba(0,13,36,0.7469187504103203) 50%, rgba(121,121,9,0.7469187504103203) 100%);;
        }

        .display-flex {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }
        
        .ps4-section {
            display: block;
            margin: 20px;
            border: 10px solid #1a3e6c;
            padding: 20px;
        }
        .ps4-name {
            text-align: center;
            font-size: 25px;
        }
        .ps4-section img {
            height: 70px;
            display: block;
            margin: auto;
        }
        .price {
            font-size: 14px;
            color: #ff0012;
        }

        .login_page .panel{
            width: 100%;
            background: url('img/gov_logo.png');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center;
            background-size: 25%
        }

        .login-panel{
            background: rgb(255, 255, 255,0.7);
            padding: 10px;
        }

        .login-panel .login_title{
            margin: 0;
            margin-bottom: 10px;
            padding: 0;
            padding-bottom: 5px;
            border-bottom: 1px solid #ff0012;
            text-align: center;
        }

        .login-panel form label, .login-panel form input {
            font-size: 15px;
            font-weight: normal;
        }

        .input-group-text{
            font-size: 15px;
        }

        .login-panel button, .login-panel a{
            font-size: 14px;
        }

        .has-error .help-block{
            font-size: 12px;
        }

        .login_msg{
            font-size: 14px;
            text-align: center;
            margin: 0;
            margin-bottom: 10px;
            font-weight: 400;
            color: #bd0000;
        }

    </style>
</head>
<body>
    <div id="app">
        @yield('content')
    </div>

    <script src="{{ asset('stopwatch/js/index.js') }}"></script> 
    <script src="{{ asset('stopwatch/js/jsCookie.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('stopwatch/stylesheets/common.css') }}">
    <link rel="stylesheet" href="{{ asset('stopwatch/stylesheets/light.css') }}">
</body>
</html>
