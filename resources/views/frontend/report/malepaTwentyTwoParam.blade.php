@extends('frontend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> म.ले.प.फा.नं. २२ :: खाता </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body p-15">
                    <form action="{{ route('report.malepa.tweentyTwo') }}" method="post" target="_blank">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-3" style="width: 15%">
                                    <label for="name">कार्यालय : </label>
                                    <select class="form-control" name="office">
                                        <option value="{{ Auth::user()->office->id }}">{{ Auth::user()->office->name }}
                                        </option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label>आर्थिक वर्ष : </label>
                                    <select name="fiscal_year" id="fiscal_year" class="form-control kalimati">
                                        @foreach ($fiscalYears as $fy)
                                            <option value="{{ $fy->id }}"
                                                @if ($fy->id == $fiscalYear->id) selected @endif class="kalimati">
                                                {{ $fy->year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>बजेट उप शिर्षक</label>
                                    <select class="form-control" name="program" id="program" required>
                                        <option value="">..........</option>
                                        @foreach ($programs as $program)
                                            <option value="{{ $program->id }}">{{ $program->name }}
                                                | {{ $program->program_code }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label>प्रकार</label>
                                    <select class="form-control" name="khata-prakar" id="khata-prakar" required>
                                        <option value="">....................</option>
                                        <option value="1">खर्च</option>
                                        <option value="2">दायित्व</option>
                                        <option value="3">भुक्तानी पाउने</option>
                                        <option value="4">पेश्की</option>
                                        <option value="5">कार्यक्रम/आयोजनाको नाम</option>
                                        {{--                  <option value="6">मुल्य अभिवृद्धि कर</option> --}}
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label>खाता</label>
                                    <select class="form-control select2" name="khata" id="khata" required>
                                        <option selected value="">......................</option>

                                    </select>
                                </div>
                                <div id="month" class="col-md-2" style="width: 8.666667%">
                                    <label>महिना</label>
                                    <select name="month" class="form-control select2">
                                        <option value="">.................</option>
                                        <option value="1234">सबै महिना</option>
                                        <option value="10">बैशाख</option>
                                        <option value="11">जेष्ठ</option>
                                        <option value="12">असार</option>
                                        <option value="1">साउन</option>
                                        <option value="2">भदौ</option>
                                        <option value="3">असोज</option>
                                        <option value="4">कार्तिक</option>
                                        <option value="5">मंसिर</option>
                                        <option value="6">पुस</option>
                                        <option value="7">माघ</option>
                                        <option value="8">फागुन</option>
                                        <option value="9">चैत्र</option>
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <center>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </center>
                        </div>

                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#fiscal_year').change(function() {
                $('#budget_sub_head').val(null).trigger('change');
                $('#khata-prakar').val('').change();
                $('#khata').val(null).trigger('change');
                $('#month').val(null).trigger('change');
            })
        })
    </script>
    <script>
        $(document).on('change', '#program', function() {
            let option = '<option selected>.......</option>';
            $('#khata-prakar').val(option);
            $('#khata').val(option);
        })
    </script>

    {{-- प्रकार change हुदा --}}

    <script>
        $(document).ready(function() {
            $('#month').hide();
            $('#khata-prakar').change(function() {
                let fiscal_year = $('#fiscal_year').val();
                let budget_sub_head_id = $('#program').val();
                let ledger_type_id = $('#khata-prakar :selected').val();
                if ((ledger_type_id == 1 && budget_sub_head_id) || (ledger_type_id == 4 &&
                        budget_sub_head_id)) {
                    $('#month').show();
                } else {
                    $('#month').hide();
                }
                let url = "{{ route('get_khata_by_khata_prakar', ['123', '234', '456']) }}";
                url = url.replace('123', fiscal_year);
                url = url.replace('234', budget_sub_head_id);
                url = url.replace('456', ledger_type_id);
                $.ajax({
                    url: url,
                    method: 'get',
                    data: {
                        'fiscal_year': fiscal_year,
                        'ledger_type_id': ledger_type_id,
                        'budget_sub_head_id': budget_sub_head_id
                    },
                    success: function(res) {
                        // let data = $.parseJSON(res);
                        console.log("=========", res)
                        let option = '<option value="">..........</option>';
                        if (res.length > 0) {
                            if (ledger_type_id == 1) {
                                option += '<option value="1234">All</option>';
                            }
                            $.each(res, function() {
                                option += '<option value="' + this.id + '">';
                                if (this.code)
                                    option += this.code + ' | ';
                                option += this.sirsak + '</option>';
                                $('#khata').html(option);
                            })
                        } else if (ledger_type_id == 4) {
                            $('#khata').html(option);
                        } else {
                            $('#khata').html(option);
                        }
                    }
                })
            })
        })
    </script>
@endsection
