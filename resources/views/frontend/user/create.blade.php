@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fas fa-edit"></i> प्रयोगकर्ता प्रविश्टी फारम </h1>
            <a href="{{route('front.user.index',Auth::user()->office->id)}}" class="btn btn-sm btn-primary btn-show-form">
                <i class="fa fa-arrow-left"></i>लिष्टमा जाने</a>

        </section>

        <!-- Main content -->
        <section class="formSection">
            <div class="row justify-content-md-center">
                <div class="col-md-8 form-bg col-md-offset-2">
                    <div class="form-title">प्रयोग कर्ता प्रविश्टी गर्ने</div>
                    <form action="{{route('front.user.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="office">कार्यालय</label>
                                    <select name="office_id" class="form-control" id="office">
                                        <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="name">पुरा नाम:</label>
                                    <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                                           required>
                                </div>
                                <div class="col-md-6">
                                    <label for="email">ई-मेल ठगाना:</label>
                                    <input type="text" class="form-control" id="email" placeholder="Email" name="email"
                                           required>
                                </div>
                                <div class="col-md-6">
                                    <label for="username">युजर नेम:</label>
                                    <input type="text" class="form-control" id="username"
                                           name="username" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="password">पासवर्ड:</label>
                                    <input type="password" class="form-control" id="password" placeholder="Password"
                                           name="password" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="role">रोल</label>
                                    <select name="role_id" class="form-control" id="role" required>
                                        <option value="">.........................</option>
                                        <option value="2">Admin</option>
                                        <option value="5">Report</option>
                                        {{--                                <option value="3">ministry</option>--}}
                                        {{--                                <option value="4">ministry_super_vision</option>--}}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <center>
                            <button type="submit" class="btn btn-success btn-show-form"><i
                                        class="fas fa-check-square"></i> Save
                            </button>
                        </center>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('script')
    <script>
        $(document).on('click', '#ministry', function () {
            alert("test");
        })
    </script>

@endsection