<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Samuha;
use App\Models\Sewa;
use App\Repositories\SamuhaRepository;
use Illuminate\Http\Request;

class SamuhaController extends Controller
{

    private $samuhaRepo;

    public function __construct(SamuhaRepository $samuhaRepo)
    {
        $this->middleware('auth');
        $this->samuhaRepo = $samuhaRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $samuhas = Samuha::all();
        return view('backend.settings.samuha.index',compact('samuhas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sewas = Sewa::all();
        return view('backend.settings.samuha.create',compact('sewas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $samuha = $this->samuhaRepo->create($request->all());
        return redirect(route('samuha'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $samuha = $this->samuhaRepo->get_by_id($id);
        $sewas = Sewa::all();
        return view('backend.settings.samuha.edit',compact('samuha','sewas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $samuha = $this->samuhaRepo->update($request,$id);
        //$samuha = $this->samuhaRepo->get_by_id($id);
        return redirect(route('samuha'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_samuhas_by_sewa_id($sewa_id){

        $samuhas = Samuha::where('sewa_id',$sewa_id)->get();
        return json_encode($samuhas);
    }
}
