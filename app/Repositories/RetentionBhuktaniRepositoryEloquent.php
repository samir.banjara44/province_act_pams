<?php
namespace App\Repositories;

use App\helpers\BsHelper;
use App\helpers\Money_words;
use App\Models\Bhuktani;
use App\Models\PreBhuktani;
use App\Models\RetentionBhuktani;
use Illuminate\Support\Facades\Auth;

class RetentionBhuktaniRepositoryEloquent implements RetentionBhuktaniRepository
{

    public function get_bhuktani_adesh_number_by_office($office_id){

        $adesh_number = RetentionBhuktani::where('office_id',$office_id)
            ->latest()
            ->first();
        if($adesh_number){
            $bhuktani_adesh_number = $adesh_number->adesh_number;
        }
        else {
            $bhuktani_adesh_number = 0;
        }
        return $bhuktani_adesh_number + 1;
    }

    public function create($attributes) {

        $bhuktani = new RetentionBhuktani();
        $bhuktani->office_id = Auth::user()->office->id;
        $bhuktani->adesh_number = $attributes['adesh_number'];
        $bhuktani->amount = $attributes['totalAmount'];
        $bhuktani->date_nepali = $attributes['bs_roman'];
        $mymonth=(explode('-',$attributes['bs_roman']));
        $month=intval($mymonth[1]);

        if($month==1){$finalmonth=10;}
        if($month==2){$finalmonth=11;}
        if($month==3){$finalmonth=12;}
        if($month==4){$finalmonth=1;}
        if($month==5){$finalmonth=2;}
        if($month==6){$finalmonth=3;}
        if($month==7){$finalmonth=4;}
        if($month==8){$finalmonth=5;}
        if($month==9){$finalmonth=6;}
        if($month==10){$finalmonth=7;}
        if($month==11){$finalmonth=8;}
        if($month==12){$finalmonth=9;}

            $bhuktani->fiscal_year= $attributes['fiscal_year'];

        $date_array = explode('-', $attributes['bs_roman']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $bhuktani->date_english = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];
        $bhuktani->month = $finalmonth;
        $bhuktani->status = 1;
        $bhuktani->save();
        return $bhuktani->id;
    }

    public function getById($id){

        return $retentionBhuktani = RetentionBhuktani::findorfail($id);
    }

    public function get_by_office($parameter){

        $office_id = Auth::user()->office_id;
        return $retentionBhuktani = RetentionBhuktani::where('fiscal_year',$parameter['fiscal_year'])
            ->where('month',$parameter['month'])
            ->where('status','=',1)
            ->where('office_id',$office_id)->get();

    }

    public function getAllByOffice($office){
        return RetentionBhuktani::where('office_id',$office)->orderBy('id','desc')->get();
    }

    public function get_amount_in_word($amount){
        $obj = new Money_words();
        return $words = $obj->convert_number($amount);

    }

    public function deleteById($bhuktaniId){
        $retentionBhuktani = RetentionBhuktani::findorfail($bhuktaniId);
       return $retentionBhuktani->delete();
    }

}