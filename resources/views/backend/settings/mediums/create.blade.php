
@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        प्राप्तिको विधि प्रविश्टी फारम
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('admin.mediums')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('admin.mediums.store')}}" method="post">
            {{csrf_field()}}
{{--            <div class="form-group">--}}
{{--              <label for="name">Source Type:</label>--}}
{{--              <input type="text" class="form-control" id="name" placeholder="Medium Name" name="name">--}}
{{--              <select class="form-control" name="source_type">--}}
{{--                <option></option>--}}
{{--              </select>--}}
{{--            </div>--}}

            <div class="form-group">
              <label for="name">Nawwme:</label>
              <input type="text" class="form-control" id="name" placeholder="Medium Name" name="name">
            </div>


            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection