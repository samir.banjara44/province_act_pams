<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartyTypes extends Model
{
    protected $table = 'party_types';
    protected $guarded = [];

    public function advancePayment(){

        return $this->belongsTo('App\Models\AdvanceAndPayment');
    }

    public function pre_bhuktani(){

        return $this->belongsTo('App\Models\PreBhuktani');
    }
}
