@extends('frontend.layouts.app')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><i class="fas fa-university"></i> चलानी गरिएको विवरण </h1>
        
        <button type="button" class="btn btn-primary btn-sm btn-show-form" id="showAddForm"><i
            class="fas fa-plus"></i> नयाँ थप गर्ने
        </button>
        <a type="button" class="btn btn-primary btn-sm btn-show-form" href="{{route('chalan.show.all', Auth::user()->office)}}" target="_blank"> सबै हेर्ने</a>
    </section>
    
    <!-- FORM section start -->
    <section class="formSection displayNone" id="showThisForm">
        <div class="row justify-content-md-center">
            <div class="col-md-8 form-bg col-md-offset-2">
                <button type="button" class="btn btn-danger btn-sm btn-show-form displayNone" id="closeAddForm"><i
                    class="fas fa-times"></i> फर्म रद्ध गर्ने
                </button>
                <div class="form-title">चलानी विवरण प्रविष्टी
                    <a href="https://mail.google.com/" target="_blank"><img src="{{ asset('img/gmail.png')}}" height="25px"
                                                                            width="25px" alt=""></a>
                    <a href="https://www.facebook.com/" target="_blank"><img src="{{ asset('img/facebook.png')}}" height="25px"
                                                                             width="25px" alt=""></a>
                </div>
                <form action="{{route('chalani.store')}}" method="post" id="chalanForm" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="date">मिति </label>
                            <input type="text" class="form-control" id="date" placeholder="मिति " name="date"
                            required>
                        </div>
                        <div class="col-md-6">
                            <label for="chalani_number">चलानी न. </label>
                            <input type="text" class="form-control kalimati" id="chalani_number"
                            value="{{$chalaniNumber}}" name="chalani_number" required disabled>
                        </div>
{{--                        <div class="col-md-6">--}}
{{--                            <label for="name">पत्र सङ्ख्या</label>--}}
{{--                            <input type="text" class="form-control" id="letter_number" placeholder=""--}}
{{--                            name="letter_number" required>--}}
{{--                        </div>--}}
                        <div class="col-md-6">
                            <label for="chalan_date">चलान मिति </label>
                            <input type="text" class="form-control kalimati" name="chalan_date" id="chalan_date"
                            placeholder="चलान मिति " name="date" required>
                        </div>
{{--                        <div class="col-md-6">--}}
{{--                            <label for="body">व्याहोरा </label>--}}
{{--                            <textarea class="form-control" id="body" name="body"></textarea>--}}
{{--                        </div>--}}
                        <div class="col-md-6">
                            <label for="to_office">चलानी जाने कार्यालय</label>
                            <input type="text" class="form-control" id="to_office" placeholder="चलानी जाने कार्यालय"
                            name="to_office">
                        </div>
                        <div class="col-md-6">
                            <label for="type">प्रकार</label>
                            <input type="text" class="form-control" id="type" placeholder="प्रकार" name="type">
                        </div>
                        <div class="col-md-6">
                            <label for="medium">पठाउने माध्यम</label>
                            <input type="text" class="form-control" id="medium" placeholder="पठाउने माध्यम"
                            name="medium">
                        </div>
                        <div class="col-md-6">
                            <label for="address">चलानी जाने कार्यालय ठेगाना</label>
                            <input type="text" class="form-control" id="address"
                            placeholder="चलानी जाने कार्यालय ठेगान" name="address">
                        </div>
                        <div class="col-md-6">
                            <label for="purpose">विषय</label>
                            <input type="text" class="form-control" id="purpose" placeholder="विषय"
                            name="purpose" required>
                        </div>
                        <div class="col-md-6">
                            <label for="branch">शाखा</label>
                            <select name="branch" required class="form-control branch" style="width:100%;" id="branch">
                                <option value="">.................</option>
                                @foreach($branch as $branch)
                                <option value="{{ $branch->id}}">@if($branch->name_nep){{$branch->name_nep}}@endif</option>
                                @endforeach
                            </select> 
                        </div>
                        <div class="col-md-6">
                            <label for="file">Upload</label>
                            <input type="file" class="form-control" id="file" placeholder="image"
                                   name="image">
                        </div>
                            <div class="col-md-6">
                                <label for="remarks">कैफियत</label>
                                <textarea class="form-control" id="remarks" placeholder="कैफियत" name="remarks"
                                required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2 col-md-offset-5">
                                <button type="submit" class="btn btn-primary btn-show-form btn-block" id="chalaniSubmit"
                                style="margin-top: 18px;">थप
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- FORM section ends -->
    
    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body pt-15">
                <table class="table table-bordered">
                    <thead>
                        
                        <th rowspan="2">सि.न.</th>
                        <th colspan="2">चलानी</th>
                        <th colspan="2">चलानी हुने पत्रको</th>
                        <th rowspan="2">विषय</th>
                        <th rowspan="2">पत्र पाउने कार्यालयको नाम</th>
                        <th rowspan="2">प्रकार</th>
                        <th rowspan="2">पठाउने माध्यम</th>
                        <th rowspan="2">Image</th>
                        <th rowspan="2">कैफियत</th>
                        <th rowspan="2">Action</th>
                        <tr>
                            <th>नम्बर</th>
                            <th>मिति</th>
                            <th>पत्र संख्या</th>
                            <th>पत्र मिति</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($chalanis as $index=>$chalani)
                        <tr>
                            <td class="kalimati" align="center">{{++$index}}</td>
                            <td class="kalimati" align="center">{{$chalani->chalani_number}}</td>
                            <td class="kalimati" align="center">{{$chalani->date}}</td>
                            <td class="kalimati" align="center">{{$chalani->letter_number}}</td>
                            <td class="kalimati" align="center">{{$chalani->chalan_date}}</td>
                            <td>{{$chalani->purpose}}</td>
                            <td>{{$chalani->to_office}}</td>
                            <td>{{$chalani->type}}</td>
                            <td>{{$chalani->medium}}</td>
                            <td>@if($chalani->image)<img src="{{asset('images/').'/'.$chalani->image}}" height="30px"
                                width="30px" alt="">@endif</td>
                            <td>{{$chalani->remarks}}</td>
                            
                            <td>
{{--                                <a type="button" href="{{route('chalani.letter.show',$chalani->id)}}" target="_blank"--}}
{{--                                    class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> पत्र</a>--}}
                                    <a type="button" href="{{route('chalani.show',$chalani->id)}}" target="_blank"
                                        class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> हेर्ने</a>
                                        <a type="button" href="{{route('chalani.edit',$chalani->id)}}"
                                            class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                                            @if(count($chalanis) == $index)
                                            <a type="button" href="#" id="delete_chalani" data-chalani-id="{{$chalani->id}}"
                                                class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i> Delete</a>
                                                @endif
                                                
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                    <!-- /.content -->
                </div>
                {{--<script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>--}}
                {{--<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>--}}
                {{--<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>--}}
                @endsection
                
                
                @section('scripts')
                {{--    ck editor--}}
                <script>
                    CKEDITOR.replace('body');
                </script>
                {{--    Chalani submit--}}
                <script>
                    $(document).on('click', '#chalaniSubmit', function (e) {
                        e.preventDefault();
                        if (validation()) {
                            let today_nepali_date = $('#date').val();
                            let nepali_date_in_roman = convertNepaliToEnglish(today_nepali_date);
                            let chalan_date = $('#chalan_date').val();
                            let chalan_date_in_roman = convertNepaliToEnglish(chalan_date);
                            
                            $('#date').val(nepali_date_in_roman);
                            $('#chalan_date').val(chalan_date_in_roman);
                            $('#chalanForm').submit();
                        }
                    })
                </script>
                
                {{--    Chalan delete--}}
                <script>
                    $(document).on('click', '#delete_chalani', function () {
                        let dartaId = $(this).attr('data-darta-id');
                        let url = '{{route('chalani.delete',123)}}';
                        url = url.replace(123, dartaId);
                        swal({
                            title: "दर्ता Delete गर्ने ?",
                            text: "एक पटक Delete गरे पछि पुनः प्राप्त गर्न सकिने छैन!",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                            
                        }).then((willDelete) => {
                            if (willDelete) {
                                $.ajax({
                                    url: url,
                                    method: 'get',
                                    success: function (res) {
                                        if ($.parseJSON(res) == true) {
                                            swal("चलानी Delete भयो!", {
                                                icon: "success",
                                            });
                                            location.reload();
                                        } else {
                                            swal("चलानी Delete भएन!", {
                                                icon: "warning",
                                            });
                                        }
                                    }
                                })
                                
                            } else {
                                swal("Your imaginary file is safe!");
                            }
                        });
                        
                    })
                </script>
                
                {{--Today date  Date picker--}}
                <script>
                    $("#date").nepaliDatePicker({
                        dateFormat: "%y-%m-%d",
                        closeOnDateSelect: true
                    });
                    var currentDate = new Date();
                    var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
                    var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
                    $("#date").val(formatedNepaliDate);
                    
                </script>
                {{-- chalan date, Date picker--}}
                <script>
                    $("#chalan_date").nepaliDatePicker({
                        dateFormat: "%y-%m-%d",
                        closeOnDateSelect: true
                    });
                    var currentDate = new Date();
                    var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
                    var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
                    $("#chalan_date").val(formatedNepaliDate);
                    
                </script>
                
                {{--Convert Nepali date to roman date --}}
                <script>
                    function convertNepaliToEnglish(input) {
                        var charArray = input.split('');
                        var engDate = '';
                        $.each(charArray, function (key, value) {
                            switch (value) {
                                case '१':
                                engDate += '1';
                                break;
                                case '२':
                                engDate += '2';
                                break;
                                case '३':
                                engDate += '3';
                                break;
                                case '४':
                                engDate += '4';
                                break;
                                case '५':
                                engDate += '5';
                                break;
                                case '६':
                                engDate += '6';
                                break;
                                case '०':
                                engDate += '0';
                                break;
                                case '७':
                                engDate += '7';
                                break;
                                case '८':
                                engDate += '8';
                                break;
                                case '९':
                                engDate += '9';
                                break;
                                
                                case '-':
                                engDate += '-';
                                break
                            }
                        });
                        return engDate
                        
                    }
                </script>
                
                {{-- Validation   --}}
                <script>
                    function validation() {
                        let flag = 1;
                        // let letter_number = $('#letter_number');
                        // if (!letter_number.val()) {
                        //     if (letter_number.siblings('p').length == 0) {
                        //         letter_number.after('<p style="color:red" class="validation-error">पत्र संख्या लेख्नुहोस!</p>')
                        //     }
                        //     letter_number.focus();
                        //     flag = 0;
                        // } else {
                        //     letter_number.siblings('p').remove()
                        // }
                        
                        // let body = $('#body');
                        // if (!body.val()) {
                            //     if (body.siblings('p').length == 0) {
                                //         body.after('<p style="color:red" class="validation-error">Write down the body</p>')
                                //     }
                                //     body.focus();
                                //     flag = 0;
                                // } else {
                                    //     body.siblings('p').remove()
                                    // }
                                    
                                    let to_office = $('#to_office');
                                    if (!to_office.val()) {
                                        if (to_office.siblings('p').length == 0) {
                                            to_office.after('<p style="color:red" class="validation-error">चलानी जाने कार्यालय लेख्नुहोस!</p>')
                                        }
                                        to_office.focus();
                                        flag = 0;
                                    } else {
                                        to_office.siblings('p').remove()
                                    }
                                    
                                    // let type = $('#type');
                                    // if (!type.val()) {
                                    //     if (type.siblings('p').length == 0) {
                                    //         type.after('<p style="color:red" class="validation-error">प्रकार लेख्नुहोस!</p>')
                                    //     }
                                    //     type.focus();
                                    //     flag = 0;
                                    // } else {
                                    //     type.siblings('p').remove()
                                    // }
                                    
                                    // let medium = $('#medium');
                                    // if (!medium.val()) {
                                    //     if (medium.siblings('p').length == 0) {
                                    //         medium.after('<p style="color:red" class="validation-error">माध्यम लेख्नुहोस!</p>')
                                    //     }
                                    //     medium.focus();
                                    //     flag = 0;
                                    // } else {
                                    //     medium.siblings('p').remove()
                                    // }
                                    
                                    // let address = $('#address');
                                    // if (!address.val()) {
                                    //     if (address.siblings('p').length == 0) {
                                    //         address.after('<p style="color:red" class="validation-error">कार्यालय ठेगाना लेख्नुहोस!</p>')
                                    //     }
                                    //     address.focus();
                                    //     flag = 0;
                                    // } else {
                                    //     address.siblings('p').remove()
                                    // }
                                    
                                    let purpose = $('#purpose');
                                    if (!purpose.val()) {
                                        if (purpose.siblings('p').length == 0) {
                                            purpose.after('<p style="color:red" class="validation-error">प्रायोजन लेख्नुहोस!</p>')
                                        }
                                        purpose.focus();
                                        flag = 0;
                                    } else {
                                        purpose.siblings('p').remove()
                                    }
                                    
                                    let branch = $('#branch');
                                    if (!branch.val()) {
                                        if (branch.siblings('p').length == 0) {
                                            branch.after('<p style="color:red" class="validation-error">शाखा लेख्नुहोस!</p>')
                                        }
                                        branch.focus();
                                        flag = 0;
                                    } else {
                                        branch.siblings('p').remove()
                                    }
                                    return flag;
                                }
                            </script>
                            
                            <script>
                                $(document).on('click', '#delete_chalani', function () {
                                    
                                    let chalani_id = $(this).attr('data-chalani-id');
                                    let url = '{{route('chalani.delete',123)}}';
                                    url = url.replace('123', chalani_id);
                                    swal({
                                        title: "Are you sure?",
                                        text: "Delete भए पछि Recovere हुदैन",
                                        icon: "warning",
                                        buttons: true,
                                        dangerMode: true,
                                        
                                    }).then((willDelete) => {
                                        
                                        if (willDelete) {
                                            
                                            $.ajax({
                                                
                                                url: url,
                                                method: 'get',
                                                success: function (res) {
                                                    console.log($.parseJSON(res));
                                                    if (res) {
                                                        swal("Chalani has been deleted!", {
                                                            icon: "success",
                                                        });
                                                        
                                                        location.reload();
                                                    }
                                                    
                                                }
                                            })
                                            
                                        } else {
                                            // swal("Your imaginary file is safe!");
                                        }
                                    });
                                })
                            </script>
                            
                            @endsection