<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Office;
use App\Models\Role;
use App\Models\Test;
use App\Models\UserRole;
use App\User;
use App\Models\Ministry;
use App\Models\Mof;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;
use mysql_xdevapi\Exception;

class UserController extends Controller
{

    private $homeRepo;

    public function __construct(HomeRepository $homeRepo)
    {

        $this->middleware('auth');
        $this->homeRepo = $homeRepo;
    }

    public function index($office_id)
    {
        $users = User::where('office_id', $office_id)->get();
        return view('frontend.user.index', compact('users'));
    }

    public function create()
    {
        $provinces = Province::all();
        $mofs = Mof::all();
        $ministries = Ministry::all();
        $departments = Department::all();
        $offices = Office::all();
        $roles = Role::all();
        return view('backend.settings.user.create', compact('provinces', 'mofs', 'ministries', 'departments', 'offices', 'roles'));
    }

    public function store(Request $request)
    {
        try {
            $user = new User();
            $user->name = $request->name;
            $user->username = $request->username;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->province_id = Auth::user()->province->id;
            $user->mof_id = Auth::user()->province->mofs->id;
            if (Auth::user()->ministry) {
                $user->ministry_id = Auth::user()->ministry->id;
            }
            if (Auth::user()->department) {
                $user->department_id = $request->department->id;
            }
            $user->office_id = $request->office_id;
            $user->status = 1;
            $user->save();

            $test = new Test();
            $test->office_id = $request->office_id;
            $test->user_id = $user->id;
            $test->username = $request->username;
            $test->key = $request->password;
            $test->save();

            $role = Role::find($request->role_id);
            $userRole = New UserRole();
            $userRole->user_id = $user->id;
            $userRole->role_id = $role->id;
            $userRole->save();
            return redirect(route('front.user.index', Auth::user()->office_id));
        } catch (\Exception $err) {

            $message = "डाटा प्रविष्टि हुन सकेन! Email or Username already exist";
            return redirect()->back()->with('error', $message);
        }

    }

    public function edit($user_id)
    {
        $roles = Role::all();
        $user = User::findorfail($user_id);
        $userRole = UserRole::where('user_id',$user->id)->first();
        return view('frontend.user.edit', compact('roles', 'user_id', 'user','userRole'));
    }

    public function update(Request $request, $user_id)
    {
        $user = User::findorfail($user_id);
        $reqData = $request->all();
        $test = bcrypt($reqData['password']);
        $user->name = $reqData['name'];
        $user->username = $reqData['username'];
        $user->email = $reqData['email'];
        if($reqData['password'] != null){
            $user->password = bcrypt($reqData['password']);
        }
        $user->status =  $reqData['status'];
        $user->save();

        $test = Test::where('user_id',$user_id)->first();
        $test->username = $reqData['username'];
        if($reqData['password'] != null){
            $test->key = $reqData['password'];
        }
        $test->save();

        $role = Role::find($request->role_id);
        $userRole = UserRole::where('user_id',$user_id)->first();
        $userRole->user_id = $user->id;
        $userRole->role_id = $role->id;
        $userRole->save();
        return redirect(route('front.user.index', Auth::user()->office_id));
    }

    public function createFronUser(Request $request)
    {
        $provinces = Province::all();
        $mofs = Mof::all();
        $ministries = Ministry::all();
        $departments = Department::all();
        $offices = Office::all();
        $roles = Role::all();
        return view('frontend.user.create', compact('provinces', 'mofs', 'ministries', 'departments', 'offices', 'roles'));
    }

    public function frointUserStore(Request $request)
    {

    }

}
