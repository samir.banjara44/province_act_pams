<?php

namespace App\Http\Controllers\Frontend;

use App\helpers\FiscalYearHelper;
use App\Http\Controllers\Controller;
use App\Models\AllBank;
use App\Models\Bank;
use App\Models\ExpenseHead;
use App\Models\LedgerType;
use App\Models\Medium;
use App\Models\PartyTypes;
use App\Models\RetentionBankGuarantee;
use App\Models\RetentionRecord;
use App\Models\SourceType;
use App\Models\VatOffice;
use App\Repositories\AdvancePaymentRepository;
use App\Repositories\BankRepository;
use App\Repositories\RetentionBankGuaranteeRepository;
use App\Repositories\RetentionRecordRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\RetentionVoucherRepository;
use App\Repositories\RetentionPreBhuktaniRepository;
use App\Repositories\RetentionBhuktaniRepository;
use const http\Client\Curl\AUTH_ANY;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class RetentionController extends Controller
{

    private $bankRepo;
    private $fiscalYearHelper;
    private $advanceAndPaymentRepo;
    private $retentionRecordRepo;
    private $retentionBankGuaranteeRepo;
    private $programRepo;
    private $retentionVoucherRepo;
    private $retentionPreBhuktaniRepo;
    private $retentionBhuktaniRepo;


    public function __construct(
        BankRepository $bankRepo,
        AdvancePaymentRepository $advanceAndPaymentRepo,
        RetentionRecordRepository $retentionRecordRepo,
        RetentionBankGuaranteeRepository $retentionBankGuaranteeRepo,
        ProgramRepository $programRepo,
        RetentionVoucherRepository $retentionVoucherRepo,
        RetentionPreBhuktaniRepository $retentionPreBhuktaniRepo,
        RetentionBhuktaniRepository $retentionBhuktaniRepo

    ) {

        $this->middleware('auth');
        $this->advanceAndPaymentRepo = $advanceAndPaymentRepo;
        $this->bankRepo = $bankRepo;
        $this->retentionRecordRepo = $retentionRecordRepo;
        $this->retentionBankGuaranteeRepo = $retentionBankGuaranteeRepo;
        $this->fiscalYearHelper = new FiscalYearHelper();
        $this->programRepo = $programRepo;
        $this->retentionVoucherRepo = $retentionVoucherRepo;
        $this->retentionPreBhuktaniRepo = $retentionPreBhuktaniRepo;
        $this->retentionBhuktaniRepo = $retentionBhuktaniRepo;
    }

    //Bank
    public function retensionBankIndex()
    {

        $banks = $this->bankRepo->get_by_office_id();
        return view('frontend.retention_bank.index', compact('banks'));
    }

    public function retensionBankCreate()
    {

        return view('frontend.retention_bank.create');
    }

    public function retensionBankStore(Request $request)
    {

        try {
            $bank = $this->bankRepo->create($request->all());
            $requestName = $request->name;
            $message = "Created " . $requestName . " Successfully";
            return redirect(route('retention_bank'))->with('success', $message);
        } catch (\Exception $e) {
            $message = "Can not add to table. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }
    }

    public function retensionBankEdit($id)
    {

        $bank = $this->bankRepo->get_by_id($id);
        return view('frontend.retention_bank.edit', compact('bank'));
    }

    public function retensionBankUpdate(Request $request, $id)
    {
        try {
            $bank = $this->bankRepo->update($request->all(), $id);
            $requestName = $request->name;
            $message = "Updated " . $requestName . " Successfully";
            return redirect(route('retention_bank'))->with('success', $message);
        } catch (\Exception $e) {
            $message = "Can not update. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }
    }

    //    Depositor
    public function retensionDepositor()
    {

        $office_id = Auth::user()->office->id;
        $advanceAndPayments = $this->advanceAndPaymentRepo->get_by_office_id($office_id);
        return view('frontend.retention_advance_payment.index', compact('advanceAndPayments'));
    }

    public function retensionDepositorCreate()
    {

        $office_id = Auth::user()->office_id;
        $banks = Bank::where('office_id', $office_id)->get();
        $party_types = PartyTypes::where('id', '!=', 5)->get();

        $all_banks = AllBank::all();
        $vat_offices = VatOffice::all();
        return view('frontend.retention_advance_payment.create', compact('party_types', 'banks', 'all_banks', 'vat_offices', 'all_banks'));
    }

    public function retensionDepositorStore(Request $request)
    {
         try {
        $program = $this->advanceAndPaymentRepo->create($request->all());
        $requestName = $request->name_eng;
        $message = "Added " . $requestName . " Successfully";
        return redirect(route('retention.depositor'))->with('success', $message);
         } catch (\Exception $e){
             $message = "Can not add to table. Please fill all the required fields first before Submitting";
             return redirect()->back()->with('error', $message);
         }

    }

    public function retensionDepositorEdit($id)
    {
        $advanceandpayment = $this->advanceAndPaymentRepo->get_by_id($id);
        $office_id = Auth::user()->office_id;
        $banks = Bank::where('office_id', $office_id)->get();
        $party_types = PartyTypes::all();
        $all_banks = AllBank::all();
        $vat_offices = VatOffice::all();
        return view('frontend.retention_advance_payment.edit', compact('advanceandpayment', 'banks', 'party_types', 'all_banks', 'vat_offices'));
    }

    public function retensionDepositorUpdate(Request $request, $id)
    {
        try {
            $advanceandpayment = $this->advanceAndPaymentRepo->update($request->all(), $id);
            $requestName = $request->name_eng;
            $message = "Updated " . $requestName . " Successfully";
            return redirect(route('retention.depositor'))->with('success', $message);
        } catch (\Exception $e) {
            $message = "Can not update. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }
    }


    //    Record
    public function retensionRecordIndex()
    {

        $office_id = Auth::user()->office_id;
        $retentionRecords = RetentionRecord::where('office_id', $office_id)->get();
        return view('frontend.retention_record.index', compact('retentionRecords'));
    }

    public function retentionRecordCreate()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.retention_record.create', compact('office_id', 'fiscalYear'));
    }

    public function retentionRecordStore(Request $request)
    {
        $retentionRecord = $this->retentionRecordRepo->create($request->all());

        return redirect(route('retention.record'));
    }

    public function retentionRecordEdit($id)
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $retentionRecord = $this->retentionRecordRepo->get_by_id($id);
        return view('frontend.retention_record.edit', compact('fiscalYear', 'retentionRecord'));
    }

    public function retentionRecordUpdate(Request $request, $id)
    {

        $retentionRecord = $this->retentionRecordRepo->update($request->all(), $id);
        return redirect(route('retention.record'));
    }

    //    Bank Guarantee
    public function retensionBankGuaranteeIndex()
    {

        $office_id = Auth::user()->office_id;
        $bank_guarantees = RetentionBankGuarantee::where('office_id', $office_id)->get();
        return view('frontend.retention_bank_guarantee.index', compact('bank_guarantees'));
    }

    public function retentionBankGuaranteeCreate()
    {

        $party_types = PartyTypes::where('id', '!=', 5)->get();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $retentionRecordList = $this->retentionRecordRepo->getByOffice(Auth::user()->office->id);
        $all_banks = AllBank::all();
        return view('frontend.retention_bank_guarantee.create', compact('fiscalYear', 'party_types', 'all_banks', 'retentionRecordList'));
    }

    public function retentionBankGuaranteeStore(Request $request)
    {

        $retentionBankGuarantee = $this->retentionBankGuaranteeRepo->create($request->all());
        return redirect(route('retention.bank.guarantee'));
    }


    public function retentionBankGuaranteeEdit($id)
    {

        $retentionBankGuarantee = $this->retentionBankGuaranteeRepo->get_by_id($id);
        $party_types = PartyTypes::all();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $all_banks = AllBank::all();
        return view('frontend.retention_bank_guarantee.edit', compact('retentionBankGuarantee', 'fiscalYear', 'all_banks', 'party_types'));
    }

    public function retentionBankGuaranteeUpdate(Request $request, $id)
    {
        $retentionBankGuarantee = $this->retentionBankGuaranteeRepo->update($request->all(), $id);
        return redirect(route('retention.bank.guarantee'));
    }


    //    Voucher
    public function retentionVoucherParameter()
    {
        $programs = $this->programRepo->get_program_for_voucher();
        return view('frontend.retention_voucher.retention_voucher_parameter', compact('programs'));
    }

    public function retentionVoucherIndex()
    {
        $office = Auth::user()->office;
        $retention_voucher_number = $this->retentionVoucherRepo->get_last_voucher_number();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $retentionVoucherList = $this->retentionVoucherRepo->getVoucherByOfficeId();
        return view('frontend.retention_voucher.index', compact('programs', 'office', 'fiscalYear', 'program', 'retention_voucher_number', 'retentionVoucherList'));
    }

    public function retentionVoucherDetailsCreate($retentionVoucherId)
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $sources = SourceType::all();
        $ledgerTypes = LedgerType::all();
        $expenseHeads = ExpenseHead::all();
        $mediums = Medium::all();
        $all_banks = AllBank::all();
        $office_id = Auth::user()->office_id;
        $retentionRecords = RetentionRecord::where('office_id', $office_id)->get();
        $retentionVoucherDetails = $this->retentionVoucherRepo->getRetentionDetailsByRetentionVoucherId($retentionVoucherId);
        $voucherData = $this->retentionVoucherRepo->getById($retentionVoucherId);
        return view('frontend.retention_voucher.details_create', compact('fiscalYear','programs', 'expenseHeads', 'sources', 'mediums', 'ledgerTypes', 'all_banks', 'retentionRecords', 'retentionVoucherId', 'retentionVoucherDetails', 'retentionVoucherId', 'voucherData'));
    }

    public function retentionVoucherStore(Request $request)
    {
        $data = $request->all();
        $voucher = $this->retentionVoucherRepo->store($data);
        return redirect(route('retention.voucher.index'));
    }

    public function retentionVoucherDetailsStore(Request $request)
    {
        $data = $request->all();
        $voucher = $this->retentionVoucherRepo->storeDetails($data);
        if ($data['byahora'] == 10 and $data['hisab_number'] == 395) {
            $retentionBhuktani = $this->retentionPreBhuktaniRepo->store($data);
        }
        return redirect()->back();
    }

    public function retentionVoucherEdit(Request $request, $retention_voucher_details_id)
    {

        $sources = SourceType::all();
        $ledgerTypes = LedgerType::all();
        $expenseHeads = ExpenseHead::all();
        $mediums = Medium::all();
        $all_banks = AllBank::all();
        $office_id = Auth::user()->office_id;
        $retentionRecords = RetentionRecord::where('office_id', $office_id)->get();
        $retentionVoucherDetails = $this->retentionVoucherRepo->getRetentionDetailsByDetailsId($retention_voucher_details_id);
        $hisabNumbers = ExpenseHead::whereIn('ledger_type', ['3', '4'])->get();
        return view('frontend.retention_voucher.edit', compact('programs', 'expenseHeads', 'retention_voucher_details_id', 'sources', 'mediums', 'ledgerTypes', 'all_banks', 'retentionRecords', 'retentionVoucherId', 'retentionVoucherDetails', 'voucherData', 'hisabNumbers'));
    }

    public function retentionVoucherUpdate(Request $request, $retentionVoucherId)
    {

        $office = Auth::user()->office;
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $all_banks = AllBank::all();
        $data = $request->all();
        $retentionVoucherUpdate = $this->retentionVoucherRepo->update($data, $retentionVoucherId);
        return redirect(route('retention.voucher.index'));
    }

    public function retentionVoucherDetailsUpdate(Request $request, $voucherDetailsId)
    {
        $voucherDetails = $request->all();
        $retentionVoucherUpdate = $this->retentionVoucherRepo->updateRetentionVoucherDetails($voucherDetails, $voucherDetailsId);
        return redirect(route('retention.voucher.index'));
    }

    public function retentionVoucherDetailsDelete($voucherDetailsId)
    {
        $voucherDetail = $this->retentionVoucherRepo->deleteRetentionVoucherDetail($voucherDetailsId);
        return json_encode($voucherDetail);
    }

    public function retentionVoucherApprovedParam()
    {
        $retentionVouchers = $this->retentionVoucherRepo->getUnapprovedVoucher();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.retention_voucher.getList', compact('retentionVouchers', 'fiscalYear'));
    }

    public function retentionVoucherApproved($retentionVoucherId)
    {
        $voucher = $this->retentionVoucherRepo->getById($retentionVoucherId);
        $voucherDetails = $voucher->retention_voucher_details;
        $drAmount = number_format($voucherDetails->where('dr_or_cr',1)->sum('amount'),2);
        $crAmount = number_format($voucherDetails->where('dr_or_cr',2)->sum('amount'),2);
        if(($drAmount == $crAmount) and $drAmount =! 0  and $crAmount !=0){
            $voucher = $this->retentionVoucherRepo->setStatusByVoucherId($retentionVoucherId);
            return json_encode(true);
        }else {
            return json_encode(false);
        }
    }

    public function retentionVoucherView($voucherId)
    {
        $voucher = $this->retentionVoucherRepo->getVoucherById($voucherId);
        $voucherDetails = ($voucher->retention_voucher_details);
        $depositors = $voucherDetails->where('hisab_number', '=', 397);
        $totalDepositorAmount = $depositors->sum('amount');
        $totalDepositorAmountInWord = $this->retentionVoucherRepo->get_amount_in_word($totalDepositorAmount);
        $amountInWod = $this->retentionVoucherRepo->get_amount_in_word($voucher->amount);
        return view('frontend.report.retention_voucher_view', compact('voucher', 'amountInWod', 'depositors', 'totalDepositorAmount', 'totalDepositorAmountInWord'));
    }

    public function retentionBhuktani()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $office_id = Auth::user()->office_id;
        $bhuktani_adesh_number = $this->retentionBhuktaniRepo->get_bhuktani_adesh_number_by_office($office_id);
        $retentionPreBhuktanies = $this->retentionPreBhuktaniRepo->get_pre_bhuktani($office_id);
        $bhuktaniAdeshs = $this->retentionBhuktaniRepo->getAllByOffice($office_id);
        return view('frontend.retention_bhuktani.create', compact('bhuktani_adesh_number', 'retentionPreBhuktanies','fiscalYear','bhuktaniAdeshs'));
    }

    public function retentionBhuktaniStore(Request $request)
    {
        $requestData = $request->all();
        $requestData['totalAmount'] = $this->retentionPreBhuktaniRepo->get_amount_by_multiple_id($request['retentionBhuktani']);
        $bhuktani_id = $this->retentionBhuktaniRepo->create($requestData);
        foreach ($request['retentionBhuktani'] as $retentionPreBhuktaniId) {
            $this->retentionPreBhuktaniRepo->update_status_and_set_bhuktani_id($bhuktani_id, $retentionPreBhuktaniId);
        }
        return redirect()->back();
    }

    public function retentionBhuktaniDelete($bhuktaniId){
        $bhuktani = $this->retentionBhuktaniRepo->deleteById($bhuktaniId);
        $preBhuktanies = $this->retentionPreBhuktaniRepo->getByBhuktaniId($bhuktaniId);
        foreach ($preBhuktanies as $preBhuktani){
            $this->retentionPreBhuktaniRepo->updateStatusAndBhuktaniId($preBhuktani->id);
        }
        if($preBhuktanies){
            return json_encode(true);
        } else {
            return json_encode(false);
        }
    }
}
