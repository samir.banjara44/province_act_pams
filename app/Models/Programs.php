<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Programs extends Model
{
    protected $table = 'programs';
    protected $guarded = [];

    public function voucher()
    {
        return $this->belongsTo('App\Models\Voucher');
    }

    public function budget(){
        return $this->belongsTo('App\Models\Budget','id','budget_sub_head');
    }

    public function bhuktani()
    {
        return $this->belongsTo('App\Models\Voucher');
    }

    public function office()
    {
        return $this->belongsTo('App\Models\Office');
    }


    public function akhtiyari()
    {
        return $this->belongsTo('App\Models\Akhtiyari', 'id', 'budget_sub_head_id');
    }

    public function getSourceType($office_id,$fiscalYear ,$budgetSubHead)
    {
        return $resources = SourceType::join('budget', 'source_types.id', '=', 'budget.source_type')
            ->select(['source_types.id', 'source_types.name'])
            ->where('budget.office_id', $office_id)
            ->where('budget.fiscal_year', $fiscalYear)
            ->where('budget.budget_sub_head', $budgetSubHead)
            ->groupBy('budget.source_type')
            ->get();
    }

    public function getExpenseHead($office_id, $budgetSubHead)
    {
        return $budgetByExpenseHead = Budget::
        where('office_id', $office_id)
            ->where('budget_sub_head', $budgetSubHead)
            ->groupBy('expense_head')
            ->orderBy('expense_head')
            ->get();
    }


    public function getLastBudgetOfOfficeByBudgetSubHead($office_id, $budget_sub_head, $expense_head)
    {

        return $last_budget = DB::table('budget')
            ->where('office_id', $office_id)
            ->where('budget_sub_head', $budget_sub_head)
            ->where('expense_head_id', $expense_head)
            ->sum('total_budget');

    }

//    Office and Budget Sub Head

    public function totalInitialBudgetByBudgetSubHead($fiscal_year,$budget_sub_head)
    {
        return $totalInitialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('akhtiyari.akhtiyari_type', '=',1)
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('budget.budget_sub_head', $budget_sub_head)
            ->sum('budget.total_budget');
    }

    public function totalAddedBudgetByBudgetSubHead($fiscal_year,$budget_sub_head)
    {
        return $totalInitialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('akhtiyari.akhtiyari_type', 2)
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('budget.budget_sub_head', $budget_sub_head)
            ->sum('budget.total_budget');
    }

    public function totalReduceBudgetByBudgetSubHead($fiscal_year,$budget_sub_head)
    {
        return $totalInitialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('akhtiyari.akhtiyari_type', '=',3)
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('budget.budget_sub_head', $budget_sub_head)
            ->sum('budget.total_budget');
    }

    public function getTotalFinalBudgetByOfficeAndBudgetSubHead($office_id,$fiscal_year, $budget_sub_head)
    {
        return $last_budget = DB::table('budget')
            ->where('office_id', $office_id)
            ->where('fiscal_year', $fiscal_year)
            ->where('budget_sub_head', $budget_sub_head)
            ->sum('total_budget');

    }

    public function getTotalExpenseByOfficeAndBudgetSubHead($office_id,$fiscal_year, $budget_sub_head)
    {
        return $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->where('vouchers.status', 1)
            ->where('activity_report.office_id', $office_id)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.fiscal_year',$fiscal_year)
            ->sum('activity_report_details.expense');
    }

    public function getTotalExpenseThisMonthByOfficeAndBudgetSubHead($office_id, $budget_sub_head,$month)
    {
       return  $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->where('vouchers.status', 1)
            ->where('activity_report.office_id', $office_id)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.month',$month)
            ->sum('activity_report_details.expense');
    }

    public function getRemainBudgetByBudgetSubHeadAndOffice($office_id, $fiscal_year,$budget_sub_head)
    {

        $totalBudget = $this->getTotalFinalBudgetByOfficeAndBudgetSubHead($office_id, $fiscal_year,$budget_sub_head);
        $totalExpense = $this->getTotalExpenseByOfficeAndBudgetSubHead($office_id,$fiscal_year, $budget_sub_head);
        return $remain = (float)$totalBudget - (float)$totalExpense;
    }

    public function getTotalExpenseUpTpYesterdayByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {

        $ldate = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseTodayByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpToTodayByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalAdvanceByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('dr_amount');
    }

    public function getTotalExpenseWithOutAdvanceByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {

        $totalExpense = $this->getTotalExpenseUpToTodayByOfficeAndBudgetSubHead($office_id, $budget_sub_head);
        $totalAdvance = $this->getTotalAdvanceByOfficeAndBudgetSubHead($office_id, $budget_sub_head);
        return $totalExpenseWithOutAdvance = (float)$totalExpense - (float)$totalAdvance;
    }

    public function getRemainBudgetByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {
        $totalBudget = $this->getTotalFinalBudgetByOfficeAndBudgetSubHead($office_id, $budget_sub_head);
        $totalExpense = $this->getTotalExpenseUpToTodayByOfficeAndBudgetSubHead($office_id, $budget_sub_head);
        return $remainBudget = (float)$totalBudget - (float)$totalExpense;
    }


//    Activities Report
    public function getActivities($budget_sub_head_id)
    {

        return $activities = BudgetDetails::where('budget_sub_head_id', $budget_sub_head_id)->orderBy('expense_head_id')->get();
    }


    public function getTotalBudgetByBudgetSubHead($budget_Sub_head_id)
    {


        return $budget = BudgetDetails::where('budget_sub_head_id', $budget_Sub_head_id)->sum('total_budget');
    }

    //    Fatbari All office report

    public function getExpenseHeadByOffice($ofice_id, $budget_sub_head_id)
    {

        return $budgetByExpenseHead = Budget::where('office_id', $ofice_id)
            ->where('budget_sub_head', $budget_sub_head_id)
            ->groupBy('expense_head')
            ->orderBy('expense_head')
            ->get();
    }

    public function get_expense_this_month($budget_sub_head, $month_post)
    {
        return $expense = VoucherDetail::
        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $this->office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.month', '=', $month_post)
            ->where('voucher_details.expense_head_id', '=', $this->expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
//            ->where('voucher_details.ledger_type_id','=',1)
            ->whereIn('voucher_details.ledger_type_id', [1, 4])
            ->sum('dr_amount');
    }


}
