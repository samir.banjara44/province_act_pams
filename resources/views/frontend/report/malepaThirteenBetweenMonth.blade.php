<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">


<style>
    table {
        border-collapse: collapse;
    }

    @media print {
        #downloadMe {
            display: none;
        }
    }


    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <input type="button" style="position: absolute; top:20px;right: 20px" value="Export to Excel" id="downloadMe">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div id="tableHeadingWrapper">
            <table width="99%" id="headingTable" style="font-size:13px">
                <tr>
                    <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                    <td colspan="6" style="text-align: center"><b>@if(Auth::user()->office->id != 43)प्रदेश सरकार@else
                                प्रदेश सभा @endif</b></td>
                </tr>
{{--                <tr>--}}
{{--                    <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>--}}
{{--                </tr>--}}
                <tr>
                    <td colspan="6" style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
                </tr>
                @if(Auth::user()->office->department)
                    <tr>
                        <td colspan="6" style="text-align: center">
                            @if(Auth::user()->office->department)
                                {{Auth::user()->office->department->name}}</td>
                        @endif
                    </tr>
                @endif
                <tr>
                    <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->name}}</b></td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->province->name}}, {{Auth::user()->office->district->name}}</b></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div style="width: 100%; text-align: center">
                            <b>फाटँबारी</b><br>
                            <b><span class="e-n-t-n-n">{{$year}}</span> साल {{$monthFrom->name}} महिनादेखि {{$monthTo->name}} सम्म</b>
                        </div>
                        <div style="float: right; margin-top: -20px">
                            म.ले.प.फा.नं. २१०
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td class="kalimati" style="padding-left: 15px">बजेट उपशिर्षक न. : <b class="kalimati">{{$program->program_code}}</b><br>
                        बजेट उप शीर्षक नाम: <b>{{$program->name}}</b></td>
                    <td style="text-align: right; padding-right: 20px">आर्थीक वर्ष : <span class="e-n-t-n-n">{{$fiscalYear->year}}</span></td>
                </tr>
            </table>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body" id="tableWrapper">
                <table class="table" width="99%" border="1" style="background-color:#dbdbdb; font-size: 12px" id="fatwari_table">
                    <thead>
                    <tr>
                        <th>खर्च/वित्तिय संकेत नं</th>
                        <th>खर्च/वित्तिय संकेतको नाम</th>
                        <th>अन्तिम बजेट</th>
                        <th>{{$monthTo->name}}  महिना सम्मको निकासा</th>
                        <th>{{$monthFrom->name}} महिना सम्मको खर्च</th>
                        <th>{{$monthTo->name}} महिनाको खर्च</th>
                        <th>{{$monthTo->name}} महिना सम्मको खर्च</th>
                        <th>पेश्की</th>
                        <th>पेश्की बाहेक खर्च रकम</th>
                        <th>बाँकी बजेट</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($budgetByExpenseHeads as $budgetExpensehead)
                        <tr style="background-color: white">

                            <td style="text-align: right"><span
                                        class="e-n-t-n-n">{{$budgetExpensehead->expense_head}}</span></td>
                            <td style="text-align: left">{{$budgetExpensehead->expense_head_by_id->expense_head_sirsak}}</td>
                            <td class="total-budget" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($budgetExpensehead->getLastBudget($budgetExpensehead->expense_head,$program->id))</span>
                            </td>
                            <td class="up-to-this-month-nikasa" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($budgetExpensehead->get_expense_upto_this_month($datas['fiscal_year'],$datas['budget_sub_head'],$datas['monthTo']))</span>
                            </td>
                            <td class="up-to-pre-month-exp" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($budgetExpensehead->get_expense_upto_prev_month($datas['fiscal_year'],$datas['budget_sub_head'],$datas['monthFrom']+1))</span>
                            </td>
                            <td class="this-month-exp" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($budgetExpensehead->get_expense_this_month($datas['fiscal_year'],$datas['budget_sub_head'],$datas['monthTo']))</span>
                            </td>
                            <td class="up-to-this-month-exp" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($budgetExpensehead->get_expense_upto_this_month($datas['fiscal_year'], $datas['budget_sub_head'],$datas['monthTo']))</span>
                            </td>
                            <td class="advance" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($budgetExpensehead->get_peski_upto_this_month($datas['fiscal_year'],$datas['budget_sub_head'],$datas['monthTo']))</span>
                            </td>
                            <td class="exp-with-out-advance" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($budgetExpensehead->expense_without_advance($datas['fiscal_year'],$datas['budget_sub_head'],$datas['monthTo']))</span>
                            </td>
                            <td class="remain-budget" style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($budgetExpensehead->remain_budget($datas['fiscal_year'],$budgetExpensehead->total_budget,$datas['monthTo'],$budgetExpensehead->expense_head,$program->id,$datas['budget_sub_head']))</span>
                            </td>

                        </tr>
                    @endforeach
                    <tr class="second_last">
                        <td colspan="2" style="text-align: right">कुल जम्मा</td>
                        <td style="text-align: right"><span id="total_last_budget"></span></td>
                        <td style="text-align: right"><span class="e-n-t-n-n" id="this_month_nikasa"></span></td>
                        <td style="text-align: right"><span id="up_to_prev_month_exp"></span></td>
                        <td style="text-align: right"><span class="e-n-t-n-n" id="this_month_exp"></span></td>
                        <td style="text-align: right"><span class="e-n-t-n-n" id="up_to_this_month_exp"></span></td>
                        <td style="text-align: right"><span class="e-n-t-n-n" id="advance"></span></td>
                        <td style="text-align: right"><span class="e-n-t-n-n" id="exp_with_out_advance"></span></td>
                        <td style="text-align: right"><span class="e-n-t-n-n" id="remain_budget"></span></td>

                    </tr>
                    <tr class="last">
                        <td colspan="2" style="text-align: right">अन्तिम बजेटको तुलना (%प्रतिशतमा)</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: center"><span class="e-n-t-n-n" id="total_expense_percentage"></span>%
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <table width="99%" style="font-size: 13px">
                    <tr>
                        <br>
                        <br>
                        <br>
                        <br>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">तयार गर्ने :
                            @if($voucher_signature and $voucher_signature->karmachari_prepare_by)
                                {{$voucher_signature->karmachari_prepare_by->name_nepali}}
                            @endif
                        </td>
                        <td>पेश गर्ने :
                            @if($voucher_signature and $voucher_signature->karmachari_submit_by)
                                {{$voucher_signature->karmachari_submit_by->name_nepali}}
                            @endif
                        </td>
                        <td>सदर गर्ने :
                            @if($voucher_signature and $voucher_signature->karmachari_approved_by)
                                {{$voucher_signature->karmachari_approved_by->name_nepali}}</td>
                        @endif
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">पद :
                            @if($voucher_signature and $voucher_signature->karmachari_prepare_by)
                                {{$voucher_signature->karmachari_prepare_by->pad_id}}
                            @endif
                        </td>
                        <td>पद :
                            @if($voucher_signature and $voucher_signature->karmachari_submit_by)
                                {{$voucher_signature->karmachari_submit_by->pad_id}}
                            @endif
                        </td>
                        <td>पद :
                            @if($voucher_signature and $voucher_signature->karmachari_approved_by)
                                {{$voucher_signature->karmachari_approved_by->pad_id}}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">मिति :</td>
                        <td>मिति :</td>
                        <td>मिति :</td>
                    </tr>

                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script>
    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '/')

                    nepaliNo += "/";
            }
        });
        // console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });


</script>

<script>
    $(document).ready(function () {
        let trs = $('#fatwari_table tbody').find('tr:not(.second_last,.last)');
        let total_budget = 0;
        let total_this_month_expense = 0;
        let total_up_to_prev_month_expense = 0;
        let total_up_to_this_month_expense = 0;
        let total_advance = 0;
        let total_exp_with_out_advance = 0;
        let total_remain_budget = 0;
        let expense_percentage = 0;
        // alert(trs.length);
        $.each(trs, function () {

            let total_budget_nep = $.trim($(this).find('td.total-budget').text());
            let total_this_month_expense_nep = $.trim($(this).find('td.this-month-exp').text());
            let total_up_to_prev_month_expense_nep = $.trim($(this).find('td.up-to-pre-month-exp').text());
            let total_up_to_this_month_expense_nep = $.trim($(this).find('td.up-to-this-month-exp').text());
            let advance_nep = $.trim($(this).find('td.advance').text());
            let exp_with_out_advance_nep = $.trim($(this).find('td.exp-with-out-advance').text());
            let remain_budget_nep = $.trim($(this).find('td.remain-budget').text());


            total_budget_roman = convertNepaliToEnglish(total_budget_nep);
            total_this_month_expense_roamn = convertNepaliToEnglish(total_this_month_expense_nep);

            total_up_to_prev_month_expense_roamn = convertNepaliToEnglish(total_up_to_prev_month_expense_nep);
            total_up_to_this_month_expense_roamn = convertNepaliToEnglish(total_up_to_this_month_expense_nep);
            advence_roamn = convertNepaliToEnglish(advance_nep);
            exp_with_out_advance_roman = convertNepaliToEnglish(exp_with_out_advance_nep);
            remain_budget_roman = convertNepaliToEnglish(remain_budget_nep);
            // alert(remain_budget_roman);

            total_budget = parseFloat(total_budget) + parseFloat(total_budget_roman);
            total_this_month_expense = parseFloat(total_this_month_expense) + parseFloat(total_this_month_expense_roamn);
            total_up_to_prev_month_expense = parseFloat(total_up_to_prev_month_expense) + parseFloat(total_up_to_prev_month_expense_roamn);
            total_up_to_this_month_expense = parseFloat(total_up_to_this_month_expense) + parseFloat(total_up_to_this_month_expense_roamn);
            total_advance = parseFloat(total_advance) + parseFloat(advence_roamn);
            total_exp_with_out_advance = parseFloat(total_exp_with_out_advance) + parseFloat(exp_with_out_advance_roman);
            total_remain_budget = parseFloat(total_remain_budget) + parseFloat(remain_budget_roman);
            expense_percentage = (parseFloat(total_up_to_this_month_expense) * 100) / parseFloat(total_budget)


        });


        $('#total_last_budget').text((changeToNepali(total_budget.toFixed(2))));
        $('#this_month_nikasa').text(changeToNepali(total_up_to_this_month_expense.toFixed(2)));
        $('#this_month_exp').text(changeToNepali(total_this_month_expense.toFixed(2)));
        $('#up_to_prev_month_exp').text(changeToNepali(total_up_to_prev_month_expense.toFixed(2)));
        $('#up_to_this_month_exp').text(changeToNepali(total_up_to_this_month_expense.toFixed(2)));
        $('#advance').text(changeToNepali(total_advance.toFixed(2)));
        $('#exp_with_out_advance').text(changeToNepali(total_exp_with_out_advance.toFixed(2)));
        $('#remain_budget').text(changeToNepali(total_remain_budget.toFixed(2)));
        $('#total_expense_percentage').text(changeToNepali(expense_percentage.toFixed(3)));

        // console.log(expense_percentage,'%');

        // alert(total_remain_budget);
    })
</script>

<script>
    function convertNepaliToEnglish(input) {
        // console.log(input);
        var charArray = input.split('');
        console.log('Test', charArray[0]);
        // charArray = charArray[0].split('');
        var engDate = '';
        $.each(charArray, function (key, value) {

            switch (value) {
                case '१':
                    engDate += '1'
                    break
                case '२':
                    engDate += '2'
                    break
                case '३':
                    engDate += '3'
                    break
                case '४':
                    engDate += '4'
                    break
                case '५':
                    engDate += '5'
                    break
                case '६':
                    engDate += '6'
                    break
                case '०':
                    engDate += '0'
                    break
                case '७':
                    engDate += '7'
                    break
                case '८':
                    engDate += '8'
                    break
                case '९':
                    engDate += '9'
                    break

                case '-':
                    engDate += '-'
                    break
                case '.':
                    engDate += '.'
                    break
            }
            // console.log(engDate)
        })

        return engDate

    }
</script>

<script>
    var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
            ,
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta charset="utf-8"/><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
            , format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            };

        return function (table, name, filename) {
            var table1 = '';
            if (!table1.nodeType) table1 = document.getElementById('headingTable')
            if (!table.nodeType) table = document.getElementById(table)
            var bigTable = table1.innerHTML + table.innerHTML;
            var ctx = {worksheet: name || 'Worksheet', table: bigTable}

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

    $("#downloadMe").click(function (e) {
        let allTable = $('div[id$=tableHeadingWrapper]').html() + $('div[id$=tableWrapper]').html();
        // console.log(allTable);
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(allTable));
        e.preventDefault();
    });
</script>