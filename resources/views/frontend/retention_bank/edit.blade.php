@extends('frontend.layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <i class="fas fa-edit"></i> धरौटी बैङ्क विवरण सम्पादन</h1>
    <ul class="breadcrumb">
      <li> <a type="button" href="{{route('retention_bank')}}" class="btn btn-sm btn-primary"><i
            class="fa fa-pencil"></i> List</a></li>
    </ul>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="panel panel-primary">
      <div class="panel-body">
        <form action="{{route('retention.bank.update', $bank->id)}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <div class="row">

              <div class="col-md-5">
                <label for="name">बैङ्कको नाम:</label>
                <input type="text" class="form-control" id="name" placeholder="Bank Name" name="name"
                  value="{{$bank->name}}" required>
              </div>

              <div class="col-md-5">
                <label for="address">ठेगाना</label>
                <input type="text" class="form-control" id="address" placeholder="address" name="address"
                  value="{{$bank->address}}" required>
              </div>

              <div class="col-md-2">
                <label for="">&nbsp;</label>
                <button type="submit" class="btn btn-primary btn-block">Submit</button>
              </div>

            </div>
          </div>
          <div class="form-group">

          </div>


        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>

@endsection