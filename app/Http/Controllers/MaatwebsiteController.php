<?php

namespace App\Http\Controllers;

use App\helpers\FiscalYearHelper;
use App\Http\Requests;
use App\Models\ActivityReport;
use App\Models\ActivityReportDetail;
use App\Models\Akhtiyari;
use App\Models\Bhuktani;
use App\Models\BudgetDetails;
use App\Models\ExpenseHead;
use App\Models\Office;
use App\Models\PreBhuktani;
use App\Models\Role;
use App\Models\Voucher;
use App\Models\VoucherDetail;
use App\Repositories\BudgetRepository;
use App\Repositories\ProgramRepository;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Input;
use App\Post;
use DB;
use PhpParser\Node\Expr\New_;
use Session;
use Excel;
use App\Models\Programs;
use App\Models\Budget;
use App\helpers\BsHelper;
use App\Repositories\VoucherDetailsRepository;


class MaatwebsiteController extends Controller
{
    private $programRepo;
    private $budgetRepo;
    private $voucherDetailsRepo;


    public function __construct(
        ProgramRepository $programRepo,
        BudgetRepository $budgetRepo,
        VoucherDetailsRepository $voucherDetailsRepo


    )
    {

        $this->programRepo = $programRepo;
        $this->budgetRepo = $budgetRepo;
        $this->voucherDetailsRepo = $voucherDetailsRepo;
        $this->fiscalYearHelper = new FiscalYearHelper();


    }

    public function importExport()
    {
        return view('importExport');
    }

    public function downloadExcel($type)
    {
        $data = Post::get()->toArray();
        return Excel::create('laravelcode', function ($excel) use ($data) {
            $excel->sheet('mySheet', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    public function importExcel(Request $request)
    {
        if ($request->hasFile('import_file')) {
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                $programs_array = $reader->toArray();
                $program_collections = collect($programs_array);
//                dd($program_collections);
                foreach ($program_collections as $row) {
                    if (count($row) != 0) {
                        $data = [];
//                    $data['project_code'] = (string)$row['project_code'];
                        $data['name'] = $row['program_sirsak'];
//                    $data['sub_project_code'] = (string)$row['sub_project_code'];
//                    $data['sub_project_ndesc_office'] = $row['sub_project_ndesc_office'];
                        $officeId = $row['office_id'];
                        $data['office_id'] = $officeId;
                        $data['program_code'] = $row['program_code'];
                        $lastProgramNumber = (substr($data['program_code'], 8));
                        if ($lastProgramNumber == 3) {
                            $data['expense_type'] = 1;
                        } else {

                            $data['expense_type'] = 2;
                        }
                    $data['component_code'] = (int)$row['component_code'];
//                    $data['component_ndesc'] = $row['component_ndesc'];
                        $data['expense_head_code'] = (string)$row['expense_head_code'];
                        $data['expense_head_id'] = (int)$row['expense_head_id'];
//                    $data['activity_code'] = $row['activity_code'];
                        $data['activity_name'] = $row['activity'];

                        $data['amount'] = $row['amount'];
                        $data['source_type'] = (int)$row['source_type'];
                        $data['source_level'] = (int)$row['source_level'];
                        $data['source'] = (int)$row['source'];
                        $data['medium'] = (int)$row['medium'];
                        if (!empty($data)) {

//                        Program check and insert
                            $programs = Programs::where('program_code', $data['program_code'])->where('office_id', $data['office_id'])->first();
                            if (!$programs) {

                                $data['budget_sub_head_id'] = $this->programRepo->create($data, $lastProgramNumber);

                            } else {
                                $data['budget_sub_head_id'] = $programs->id;
                            }

                            $akhtiyari = Akhtiyari::where('budget_sub_head_id', $data['budget_sub_head_id'])->where('office_id', $data['office_id'])->first();
                            if (!$akhtiyari) {

                                $data['akhtiyari_amount'] = $program_collections->where('office_id', $data['office_id'])->where('program_code', $data['program_code'])->sum('amount');
                                $data['akhtiyar_id'] = $this->budgetRepo->createAkhtiyariTemp($data);

                            } else {
                                $data['akhtiyar_id'] = $akhtiyari->id;
                            }
                            $this->budgetRepo->createTemp($data);
                        }
                    }
                }
            });
        }
//        Session::put('success', 'Youe file successfully import in database!!!');
        return back();
    }

    public function updateDivForrestOfficeComponenetCode(Request $request){
        if ($request->hasFile('import_file')) {
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                $programs_array = $reader->toArray();
                $program_collections = collect($programs_array);
                foreach ($program_collections as $row) {
                    if (count($row) != 0) {
                        $data = [];
                        $data['office_id'] = $row['office_id'];
                        $data['program_code'] = $row['program_code'];
                        $data['activity_name'] = $row['activity'];
                        $data['activity_code'] = $row['activity_code'];
                        if (!empty($data)) {
                            $this->budgetRepo->UpdataeImportBudget($data);
                        }
                    }
                }
            });
        }
        return back();
    }
    public function getConditionalActivity(Request $request)
    {
        if ($request->hasFile('import_file')) {
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                $programs_array = $reader->toArray();
                $program_collections = collect($programs_array);
                foreach ($program_collections as $row) {

                    $data = [];
                    $data['project_code'] = (string)$row['activity_code'];
                    $officeId = $row['office_id'];
                    $data['office_id'] = (int)$row['office_id'];
                    $data['program_code'] = (int)$row['program_code'];
                    $data['name'] = $row['budget_sub_head_sirsak'];
                    $lastProgramNumber = (substr($data['program_code'], 8));
                    if ($lastProgramNumber == 3) {
                        $data['expense_type'] = 1;
                    } else {

                        $data['expense_type'] = 2;
                    }
                    $data['expense_head_id'] = (int)$row['expense_head_id'];
                    $data['expense_head_code'] = (int)$row['expense_head_code'];
                    $data['activity_code'] = $row['activity_code'];
                    $data['activity_name'] = $row['activity'];
                    $data['amount'] = $row['amount'];
                    $data['source_type'] = 1;
                    $data['source_level'] = 1;
                    $data['source'] = 2;
                    $data['medium'] = 2;
                    $data['fiscal_year'] = "2076/77";
                    if (!empty($data)) {

//                        Program check and insert
                        $programs = Programs::where('program_code', $data['program_code'])->where('office_id', $data['office_id'])->first();
                        if (!$programs) {

                            $data['budget_sub_head_id'] = $this->programRepo->createImport($data, $lastProgramNumber);
                            var_dump("budget sub head of office " . " " . $data['office_id'] . "<br>");

                        } else {

                            $data['budget_sub_head_id'] = $programs['id'];
                        }
                        $akhtiyari = Akhtiyari::where('budget_sub_head_id', $data['budget_sub_head_id'])->where('office_id', $data['office_id'])->first();
                        if (!$akhtiyari) {

                            $data['akhtiyari_amount'] = $program_collections->where('office_id', $data['office_id'])->where('program_code', $data['program_code'])->sum('amount');
                            $data['akhtiyar_id'] = $this->budgetRepo->createAkhtiyariTemp($data);

                        } else {
                            $data['akhtiyar_id'] = $akhtiyari->id;
                        }

                        $this->budgetRepo->createTemp($data);
                    }
                }
            });
            dd('stop');
        }
//        Session::put('success', 'Youe file successfully import in database!!!');
        return back();
    }


    public function importUser()
    {

        return view('importUser');
    }

    public function storeUser(Request $request)
    {

        if ($request->hasFile('import_file')) {

            Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                $users_array = $reader->toArray();
                $users_collection = collect($users_array);
                foreach ($users_collection as $row) {
                    $user = new User();
                    $user->name = $row['office_name'];
                    $user->username = $row['user_name'];
                    $user->email = $row['user_name'];
                    $user->password = bcrypt($row['password']);
                    $user->province_id = (int)$row['province_id'];
                    $user->mof_id = (int)$row['mof_id'];
                    $user->ministry_id = (int)$row['ministry_id'];
                    $user->department_id = NULL;
                    $user->office_id = (int)$row['office_id'];
                    $user->status = 1;
                    $user->save();
                    $role = Role::find(2);  // admin
                    $user->roles()->attach($role);


                }
            });
        }
//        Session::put('success', 'Youe file successfully import in database!!!');
        return back();
    }

    public function importBudgetDetails_temp()
    {

        $offices = ['1', '4', '43'];
        foreach ($offices as $office) {
            $budgetSubHeads = Programs::where('office_id', $office)->get();

            foreach ($budgetSubHeads as $budgetSubHead) {
                $budgets = Budget::where('budget_sub_head', $budgetSubHead->id)->get();
                foreach ($budgets as $budget) {
                    $budgetDetals = new BudgetDetails();
                    $budgetDetals->budget_sub_head_id = $budgetSubHead->id;
                    $budgetDetals->budget_id = $budget['id'];
                    $budgetDetals->sub_activity = $budget['activity'];
                    $budgetDetals->expense_head_id = $budget['expense_head_id'];
                    $budgetDetals->unit = 1;
                    $budgetDetals->total_budget = $budget['total_budget'];
                    $budgetDetals->first_quarter_unit = NULL;
                    $budgetDetals->second_quarter_unit = NULL;
                    $budgetDetals->third_quarter_unit = 1;
                    $budgetDetals->first_quarter_budget = NULL;
                    $budgetDetals->first_chaimasik_bhar = NULL;
                    $budgetDetals->second_quarter_budget = NULL;
                    $budgetDetals->second_chaimasik_bhar = NULL;
                    $budgetDetals->third_quarter_budget = $budget['total_budget'];
                    $budgetDetals->third_chaimasik_bhar = NULL;
                    $budgetDetals->source_type = $budget['source_type'];
                    $budgetDetals->source_level = $budget['source_level'];
                    $budgetDetals->source = $budget['source'];
                    $budgetDetals->medium = $budget['medium'];
                    $budgetDetals->office_id = $budget['office_id'];
                    $budgetDetals->user_id = 1;
                    $budgetDetals->status = 1;
                    $budgetDetals->fiscal_year = "2076/77";
                    $budgetDetals->date = date('Y-m-d H:i:s');
                    $budgetDetals->save();

                }
            }
        }
        return back();
    }


    public function importBudgetDetails()
    {

//        $offices = ['1','4','43'];
//        foreach ($offices as $office) {
//        $budgetSubHeads = Programs::where('office_id', 12)->get();
//        $i = 0;
//        foreach ($budgetSubHeads as $budgetSubHead) {
//            $budgets = Budget::where('budget_sub_head', $budgetSubHead->id)->get();
//            foreach ($budgets as $budget) {
//                if ($budget['expense_head'] == '31156') {
//                    echo "Pujigat", "<br>";
//                    for ($x = 1; $x <= 2; $x++) {
//                        $amount = $budget['total_budget'] * 0.04;
//                        $sub_activity = $budget['activity'] . " - " . "कन्टेन्जेन्सी";
//                        if ($x == 1) {
//                            $amount = $budget['total_budget'] * 0.96;
//                            $sub_activity = $budget['activity'];
//                        }
//                        $budgetDetals = new BudgetDetails();
//                        $budgetDetals->office_id = $budget['office_id'];
//                        $budgetDetals->budget_sub_head_id = $budgetSubHead->id;
//                        $budgetDetals->budget_id = $budget['id'];
//                        $budgetDetals->sub_activity = $sub_activity;
//                        $budgetDetals->expense_head_id = $budget['expense_head_id'];
//                        $budgetDetals->unit = 1;
////                        $program_amount = $amount;
//                        $budgetDetals->total_budget = (float)$amount;
//                        $budgetDetals->first_quarter_unit = NULL;
//                        $budgetDetals->second_quarter_unit = NULL;
//                        $budgetDetals->third_quarter_unit = 1;
//                        $budgetDetals->first_quarter_budget = NULL;
//                        $budgetDetals->first_chaimasik_bhar = NULL;
//                        $budgetDetals->second_quarter_budget = NULL;
//                        $budgetDetals->second_chaimasik_bhar = NULL;
//                        $budgetDetals->third_quarter_budget = (float)$amount;
//                        $budgetDetals->third_chaimasik_bhar = NULL;
//                        $budgetDetals->source_type = $budget['source_type'];
//                        $budgetDetals->source_level = $budget['source_level'];
//                        $budgetDetals->source = $budget['source'];
//                        $budgetDetals->medium = $budget['medium'];
//                        $budgetDetals->user_id = 1;
//                        $budgetDetals->status = 1;
//                        $budgetDetals->fiscal_year = "2076/77";
//                        $budgetDetals->date = date('Y-m-d H:i:s');
//                        $budgetDetals->save();
//
//                    }
//                } else {
//
//                    $budgetDetals = new BudgetDetails();
//                    $budgetDetals->budget_sub_head_id = $budgetSubHead->id;
//                    $budgetDetals->budget_id = $budget['id'];
//                    $budgetDetals->sub_activity = $budget['activity'];
//                    $budgetDetals->expense_head_id = $budget['expense_head_id'];
//                    $budgetDetals->unit = 1;
//                    $budgetDetals->total_budget = $budget['total_budget'];
//                    $budgetDetals->first_quarter_unit = NULL;
//                    $budgetDetals->second_quarter_unit = NULL;
//                    $budgetDetals->third_quarter_unit = 1;
//                    $budgetDetals->first_quarter_budget = NULL;
//                    $budgetDetals->first_chaimasik_bhar = NULL;
//                    $budgetDetals->second_quarter_budget = NULL;
//                    $budgetDetals->second_chaimasik_bhar = NULL;
//                    $budgetDetals->third_quarter_budget = $budget['total_budget'];
//                    $budgetDetals->third_chaimasik_bhar = NULL;
//                    $budgetDetals->source_type = $budget['source_type'];
//                    $budgetDetals->source_level = $budget['source_level'];
//                    $budgetDetals->source = $budget['source'];
//                    $budgetDetals->medium = $budget['medium'];
//                    $budgetDetals->office_id = $budget['office_id'];
//                    $budgetDetals->user_id = 1;
//                    $budgetDetals->status = 1;
//                    $budgetDetals->fiscal_year = "2076/77";
//                    $budgetDetals->date = date('Y-m-d H:i:s');
//                    $budgetDetals->save();
//                    echo "Chalu", "<br>";
//                }
//            }
//        }

//            }

        $voucher_details = VoucherDetail::whereIn('office_id', ['1', '4', '12'])->get();
        foreach ($voucher_details as $index => $voucher_detail) {

            $budget = $voucher_detail->budget;
            if ($budget) {
                $budget_detail = $this->get_detail_by_budget_id($budget->id);
                if ($budget_detail) {
                    $voucher_detail->main_activity_id = $budget_detail->id;
                    $voucher_detail->save();
                    echo "V_D " . $voucher_detail->id . "from b _ id " . $budget->id . 'detai_id' . $budget_detail->id, "<br>";
                } else {
                    echo "NOOOO Detail for budget =>", $budget->id, "<br>";
                }


            } else {
                echo "No Budget in vouchee detail =>", $voucher_detail->id, "<br>";
            }
        }

        dd("Success");
        return back();
    }

    function get_detail_by_budget_id($budget_id)
    {
        $budget_detail = BudgetDetails::where('budget_id', $budget_id)->where('sub_activity', 'not like', '%कन्टेन्जेन्सी%')->first();
        return $budget_detail;
    }

    function setOperationalActivity()
    {
//        $offices = Office::where('ministry_id', 4)
//            ->where('province_id', 4)
//            ->whereNotIn('id', [9, 12])->get();
//        foreach ($offices as $office) {

        $budgetSubHeads = Programs::where('office_id', 9)->where('program_code', 'like', '%' . 3)->first();
        if ($budgetSubHeads) {
            $activities = Budget::where('budget_sub_head', $budgetSubHeads->id)->where('expense_head', 22212)->get();//several activities
            if ($activities->count() > 0) {
                $activity = $activities[0];
                $total_budget = $activities->sum('total_budget');
                $budget = $this->budgetRepo->createBudgetForOperationalActivity($activity, $total_budget);
                $budgetDetailsSave = $this->budgetRepo->createBudgetDetailsForOperationalActivity($activity, $total_budget, $budget);

                foreach ($activities as $activity) {
                    if ($activity->chaineDetail()->count() > 0) {
                        $activity->chaineDetail()->delete();
                    }
                    $activity->delete();
                }
            }

        }
//        }
        return back();
    }


    public function reSetAkhtiyariTypeInBudget()
    {
        $offices = Office::whereBetween('id', ['9', '42'])->whereNotIn('id', ['11', '12'])->get();
        foreach ($offices as $office) {
            $budgetSubHeads = Programs::where('office_id', $office->id)->get();
            if ($budgetSubHeads) {
                foreach ($budgetSubHeads as $budgetSubHead) {
                    $akhtiyaris = Akhtiyari::where('budget_sub_head_id', $budgetSubHead->id)->get();
                    if ($akhtiyaris) {
                        foreach ($akhtiyaris as $akhtiyari) {
                            $budgets = Budget::where('budget_sub_head', $budgetSubHead->id)->get();
                            foreach ($budgets as $budget) {
                                if ($budget->akhtiyari_type == 1) {
                                    $budget->akhtiyari_type = $akhtiyari->id;
                                    $budget->save();
                                }
                            }
                        }
                    }
                }
            }
        }

        return back();
    }

    public function setActivityReport()
    {
        set_time_limit(0);
        $offices = Office::all();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        foreach ($offices as $office) {
            $budgetSubHeads = Programs::where('id', 32)->get();
            foreach ($budgetSubHeads as $budgetSubHead) {
                $acivities = BudgetDetails::where('id', 174)->get();
                foreach ($acivities as $activity) {
                    $vouchers = Voucher::where('id', 350)->whereHas('details', function (Builder $query) use ($budgetSubHead, $activity) {
                        $query->where('budget_sub_head_id', 32)->where('main_activity_id', 174);
                    })->get();
                    $data = [];
                    if ($vouchers->count() > 0) {
                        foreach ($vouchers as $voucher) {
                            $pefa = $voucher->details->where('main_activity_id', $activity->id)->where('dr_or_cr', 2)->where('ledger_type_id', 9);
                            if ($pefa->count() > 0) {
                                foreach ($voucher->details as $detail) {
                                    if ($voucher->details->where('dr_or_cr', 2) and $voucher->details->where('ledger_type_id', 3)) {
                                        $activityReport = ActivityReport::where('activity_id', $activity->id)->first();
                                        if ($activityReport) {
                                            if ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 3) {

                                                $data['expense'] = $detail->cr_amount;
                                                $currentExpense = $activityReport->expense;
                                                $total_expense = $activityReport->expense = $currentExpense + $data['expense'];
                                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                                $activityReport->save();
                                                $expenseHead = ExpenseHead::where('id', $activity->expense_head_id)->where('status', 1)->first();

                                                $activityReportDetails = new ActivityReportDetail();
                                                $activityReportDetails->activity_report_id = $activityReport->id;
                                                $activityReportDetails->activity_id = $activity->id;
                                                $activityReportDetails->budget = $activity->total_budget;
                                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                                $activityReportDetails->expense = $data['expense'];
                                                $activityReportDetails->voucher_id = $voucher->id;
                                                $activityReportDetails->month = $voucher->month;
                                                $activityReportDetails->fiscal_year = $fiscalYear->year;
                                                $activityReportDetails->save();
                                                var_dump("this is tsa expense" . " " . $data['expense'] . "of voucher" . "" . $voucher->id . "<br>");
                                            }
                                        } else {

                                            $expenseHead = ExpenseHead::where('id', $activity->expense_head_id)->where('status', 1)->first();
                                            if ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 3) {

//                                                        $data['expense'] = $detail->cr_amount;
                                                $activityReport = new ActivityReport();
                                                $activityReport->office_id = $office->id;
                                                $activityReport->budget_sub_head_id = $budgetSubHead->id;
                                                $activityReport->activity_id = $activity->id;
                                                $activityReport->budget = $activity->total_budget;
                                                $activityReport->expense_head = $expenseHead->expense_head_code;
                                                $total_expense = $activityReport->expense = $detail->cr_amount;
                                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                                $activityReport->fiscal_year = $fiscalYear->year;
                                                $activityReport->save();

                                                $activityReportDetails = new ActivityReportDetail();
                                                $activityReportDetails->activity_report_id = $activityReport->id;
                                                $activityReportDetails->activity_id = $activity->id;
                                                $activityReportDetails->budget = $activity->total_budget;
                                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                                $activityReportDetails->expense = $detail->cr_amount;
                                                $activityReportDetails->voucher_id = $voucher->id;
                                                $activityReportDetails->month = $voucher->month;
                                                $activityReportDetails->fiscal_year = $fiscalYear->year;
                                                $activityReportDetails->save();

                                                var_dump("this is test tsa expense" . " " . $detail->cr_amount . "of voucher" . "" . $voucher->id . "<br>");
                                            }
                                        }

                                    }
                                    if ($voucher->details->contains('dr_or_cr', 1) and $voucher->details->contains('ledger_type_id', 3)) {
                                        $activityReport = ActivityReport::where('activity_id', $activity->id)->first();
                                        if ($activityReport) {
                                            if ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 3) {
                                                $data['income'] = $detail->dr_amount;
                                                $currentExpense = $activityReport->total_expense;
                                                if ($currentExpense) {
                                                    $total_expense = $activityReport->expense = $currentExpense - $data['income'];
                                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                                }
                                                $activityReport->save();
                                                $expenseHead = ExpenseHead::where('id', $activity->expense_head_id)->where('status', 1)->first();

                                                $activityReportDetails = new ActivityReportDetail();
                                                $activityReportDetails->activity_report_id = $activityReport->id;
                                                $activityReportDetails->activity_id = $activity->id;
                                                $activityReportDetails->budget = $activity->total_budget;
                                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
//                                                    $activityReportDetails->expense = $data['expense'];
                                                $activityReportDetails->voucher_id = $voucher->id;
                                                $activityReportDetails->month = $voucher->month;
                                                $activityReportDetails->fiscal_year = $fiscalYear->year;
                                                $activityReportDetails->save();
                                                var_dump("this is tsa income" . $data['income'] . "of voucher" . "" . $voucher->id . "<br>");
                                            }

                                        } else {

                                            if ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 3) {
                                                var_dump("this is test for income");
                                                $data['income'] = $detail->dr_amount;
                                                $activityReport = new ActivityReport();
                                                $activityReport->office_id = $office->id;
                                                $activityReport->budget_sub_head_id = $budgetSubHead->id;
                                                $activityReport->activity_id = $activity->id;
                                                $activityReport->budget = $activity->total_budget;
                                                $activityReport->expense_head = $expenseHead->expense_head_code;
//                                                        $total_expense = $activityReport->expense =  $data['income'];
//                                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                                $activityReport->fiscal_year = $fiscalYear->year;
                                                $activityReport->save();

                                                $activityReportDetails = new ActivityReportDetail();
                                                $activityReportDetails->activity_report_id = $activityReport->id;
                                                $activityReportDetails->activity_id = $activity->id;
                                                $activityReportDetails->budget = $activity->total_budget;
                                                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                                $activityReportDetails->income = $data['income'];
                                                $activityReportDetails->voucher_id = $voucher->id;
                                                $activityReportDetails->month = $voucher->month;
                                                $activityReportDetails->fiscal_year = $fiscalYear->year;
                                                $activityReportDetails->save();

                                                var_dump("this yyyy is tsa income" . $data['income'] . "of voucher" . "" . $voucher->id . "<br>");
                                            }
                                        }
                                    }

                                    if (($voucher->details->where('dr_or_cr', 1) and $voucher->details->where('ledger_type_id', 3)) and ($voucher->details->where('dr_or_cr', 2) and $voucher->details->where('ledger_type_id', 3))) {
                                        $activityReport = ActivityReport::where('activity_id', $activity->id)->first();
                                        if ($activityReport) {

                                            if (array_key_exists('expense', $data)) {

                                                $currentExpense = $activityReport->expense;
                                                $total_expense = $activityReport->expense = $currentExpense + $data['expense'];
                                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                            }
                                            if (array_key_exists('income', $data)) {

                                                $currentExpense = $activityReport->total_expense;
                                                $total_expense = $activityReport->expense = $currentExpense - $data['income'];
                                                $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                            }
                                            $activityReport->save();
                                            $expenseHead = ExpenseHead::where('id', $activity->expense_head_id)->where('status', 1)->first();

                                            $activityReportDetails = new ActivityReportDetail();
                                            $activityReportDetails->activity_report_id = $activityReport->id;
                                            $activityReportDetails->activity_id = $activity->id;
                                            $activityReportDetails->budget = $activity->total_budget;
                                            $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                            if (array_key_exists('expense', $data)) {

                                                $activityReportDetails->expense = $data['expense'];
                                            }
                                            if (array_key_exists('income', $data)) {

                                                $activityReportDetails->income = $data['income'];
                                            }
                                            $activityReportDetails->voucher_id = $voucher->id;
                                            $activityReportDetails->month = $voucher->month;
                                            $activityReportDetails->fiscal_year = $fiscalYear->year;
                                            $activityReportDetails->save();
                                        } else {
                                            $total_expense = 0;
                                            if (($voucher->details->where('dr_or_cr', 1) and $voucher->details->where('ledger_type_id', 3)) and ($voucher->details->where('dr_or_cr', 2) and $voucher->details->where('ledger_type_id', 3))) {
                                                if ($activityReport) {

                                                    if (array_key_exists('expense', $data)) {

                                                        $currentExpense = $activityReport->expense;
                                                        $total_expense = $activityReport->expense = $currentExpense + $data['expense'];
                                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                                    }
                                                    if (array_key_exists('income', $data)) {

                                                        $currentExpense = $activityReport->total_expense;
                                                        $total_expense = $activityReport->expense = $currentExpense - $data['income'];
                                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                                    }
                                                    $activityReport->save();
                                                    $expenseHead = ExpenseHead::where('id', $activity->expense_head_id)->where('status', 1)->first();

                                                    $activityReportDetails = new ActivityReportDetail();
                                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                                    $activityReportDetails->activity_id = $activity->id;
                                                    $activityReportDetails->budget = $activity->total_budget;
                                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                                    $activityReportDetails->expense = $data['expense'];
                                                    $activityReportDetails->voucher_id = $voucher->id;
                                                    $activityReportDetails->month = $voucher->month;
                                                    $activityReportDetails->fiscal_year = $fiscalYear->year;
                                                    $activityReportDetails->save();
                                                }
                                            }

                                        }
                                    }
                                }

                            } else {

//                               not pefa
                                foreach ($voucher->details as $voucherDetail) {

                                    $expenseHead = ExpenseHead::where('id', $activity->expense_head_id)->where('status', 1)->first();

                                    if (($voucherDetail->dr_or_cr == 1 and $voucherDetail->ledger_type_id == 1) || ($voucherDetail->dr_or_cr == 1 and $voucherDetail->ledger_type_id == 4)) {

                                        $data['expense'] = $voucherDetail->dr_amount;
                                        var_dump("this is expense amount" . $data['expense'] . "of voucher" . " " . $voucher->id . "<br>");
                                        $activityReport = ActivityReport::where('activity_id', $activity->id)->first();

//                                      var_dump("current amount" . $activityReport->expense . "<br>");
                                        $total_expense = 0;
                                        if ($activityReport) {
                                            if ($voucherDetail->main_activity_id == $activity->id) {

                                                $currentExpense = $activityReport->expense;
                                                $activityReport->expense = $total_expense = (float)$currentExpense + (float)$voucherDetail->dr_amount;
                                                $activityReport->remain = (float)$activity->total_budget - (float)$total_expense;
                                                $activityReport->save();
                                            }
                                        } else {
                                            if ($voucherDetail->main_activity_id == $activity->id) {
                                                $activityReport = new ActivityReport();
                                                $activityReport->office_id = $office->id;
                                                $activityReport->budget_sub_head_id = $budgetSubHead->id;
                                                $activityReport->activity_id = $activity->id;
                                                $activityReport->expense_head = $expenseHead->expense_head_code;
                                                $activityReport->budget = $activity->total_budget;
                                                $activityReport->expense = $voucherDetail->dr_amount;
                                                $activityReport->remain = (float)$activity->total_budget - (float)$data['expense'];
                                                $activityReport->fiscal_year = $fiscalYear->year;
                                                $activityReport->save();
                                            }
                                        }
                                        if ($voucherDetail->main_activity_id == $activity->id) {

                                            $activityReportDetails = new ActivityReportDetail();
                                            $activityReportDetails->activity_report_id = $activityReport->id;
                                            $activityReportDetails->activity_id = $activity->id;
                                            $activityReportDetails->budget = $activity->total_budget;
                                            $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                            $activityReportDetails->expense = $voucherDetail->dr_amount;
                                            $activityReportDetails->voucher_id = $voucher->id;
                                            $activityReportDetails->month = $voucher->month;
                                            $activityReportDetails->fiscal_year = $fiscalYear->year;
                                            var_dump("this is not pefa");
                                            $activityReportDetails->save();
                                        }

                                    }
                                }

                            }
                        }
                    } else {
//                      no vouchers

                        $expenseHead = ExpenseHead::where('id', $activity->expense_head_id)->where('status', 1)->first();

                        $activityReport = new ActivityReport();
                        $activityReport->office_id = $office->id;
                        $activityReport->budget_sub_head_id = $budgetSubHead->id;
                        $activityReport->activity_id = $activity->id;
                        $activityReport->expense_head = $expenseHead->expense_head_code;
                        $activityReport->budget = $activity->total_budget;
                        $activityReport->fiscal_year = $fiscalYear->year;
                        $activityReport->save();

                        $activityReportDetails = new ActivityReportDetail();
                        $activityReportDetails->activity_report_id = $activityReport->id;
                        $activityReportDetails->activity_id = $activity->id;
                        $activityReportDetails->budget = $activity->total_budget;
                        $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                        $activityReportDetails->fiscal_year = $fiscalYear->year;
                        var_dump("this is test");
                        $activityReportDetails->save();
                    }
                }
            }
        }
        dd("done");
        return back();
    }

    public function setBudgetInReportTable()
    {

        set_time_limit(0);
        $offices = Office::where('id', 12);
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();

        foreach ($offices as $office) {

            $activities = BudgetDetails::where('office_id', $office->id)->where('budget_sub_head_id', 25)->get();
            foreach ($activities as $activity) {

                $expenseHead = ExpenseHead::where('id', $activity->expense_head_id)->where('status', 1)->first();
                $activityReport = new ActivityReport();
                $activityReport->office_id = $office->id;
                $activityReport->budget_sub_head_id = $activity->budget_sub_head_id;
                $activityReport->activity_id = $activity->id;
                $activityReport->expense_head = $expenseHead->expense_head_code;
                $activityReport->budget = $activity->total_budget;
                $activityReport->expense = 0;
                $activityReport->remain = $activity->total_budget;
                $activityReport->fiscal_year = $fiscalYear->year;
                $activityReport->save();

                $activityReportDetails = new ActivityReportDetail();
                $activityReportDetails->activity_report_id = $activityReport->id;
                $activityReportDetails->activity_id = $activity->id;
                $activityReportDetails->budget = $activity->total_budget;
                $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                $activityReportDetails->akhtiyari_type = 1;
                $activityReportDetails->fiscal_year = $fiscalYear->year;
                $activityReportDetails->save();
            }
        }

        return back();
    }

    public function setExpenseOnReportTable()
    {
        set_time_limit(0);
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $activities = ActivityReport::all();
        foreach ($activities as $activity) {

            $vouchers = Voucher::whereHas('details', function (Builder $query) use ($activity) {
                $query->where('main_activity_id', $activity->activity_id);
            })->get();
            $data = [];
            if ($vouchers->count() > 0) {

                foreach ($vouchers as $voucher) {
                    $pefa = $voucher->details->where('main_activity_id', $activity->activity_id)->where('dr_or_cr', 2)->where('ledger_type_id', 9);
                    if ($pefa->count() > 0) {

                        foreach ($voucher->details as $detail) {

                            if ($voucher->details->where('dr_or_cr', 1)->where('ledger_type_id', 3)->count() > 0 and ($voucher->details->where('dr_or_cr', 2)->where('ledger_type_id', 3)->count() > 0)) {
                                if (($voucher->details->where('dr_or_cr', 1) and $voucher->details->where('ledger_type_id', 3)) and ($voucher->details->where('dr_or_cr', 2) and $voucher->details->where('ledger_type_id', 3))) {
                                    $activityReport = ActivityReport::where('activity_id', $activity->activity_id)->first();

                                    if (array_key_exists('expense', $data)) {

                                        $currentExpense = $activityReport->expense;
                                        $total_expense = $activityReport->expense = $currentExpense + $data['expense'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    }
                                    if (array_key_exists('income', $data)) {

                                        $currentExpense = $activityReport->total_expense;
                                        $total_expense = $activityReport->expense = $currentExpense - $data['income'];
                                        $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    }
                                    $activityReport->save();
                                    $expenseHead = ExpenseHead::where('expense_head_code', $activity->expense_head)->where('status', 1)->first();
                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activity->activity_id;
                                    $activityReportDetails->budget = $activity->total_budget;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    if (array_key_exists('expense', $data)) {

                                        $activityReportDetails->expense = $data['expense'];
                                    }
                                    if (array_key_exists('income', $data)) {

                                        $activityReportDetails->income = $data['income'];
                                    }
                                    $activityReportDetails->voucher_id = $voucher->id;
                                    $activityReportDetails->month = $voucher->month;
                                    $activityReportDetails->fiscal_year = $fiscalYear->year;
                                    $activityReportDetails->save();
                                    var_dump("this id both");
                                }

                            } else if ($voucher->details->where('dr_or_cr', 2) and $voucher->details->where('ledger_type_id', 3)) {

                                $activityReport = ActivityReport::where('activity_id', $activity->activity_id)->first();
                                if ($detail->dr_or_cr == 2 and $detail->ledger_type_id == 3) {

                                    $data['expense'] = $detail->cr_amount;
                                    $currentExpense = $activityReport->expense;
                                    $total_expense = $activityReport->expense = $currentExpense + $data['expense'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $expenseHead = ExpenseHead::where('expense_head_code', $activity->expense_head)->where('status', 1)->first();
                                    $activityReportDetails = new ActivityReportDetail();

                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activity->activity_id;
                                    $activityReportDetails->budget = $activity->budget;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $data['expense'];
                                    $activityReportDetails->voucher_id = $voucher->id;
                                    $activityReportDetails->month = $voucher->month;
                                    $activityReportDetails->fiscal_year = $fiscalYear->year;
                                    $activityReportDetails->save();
//                                    var_dump("this is tsa expense" . " " . $data['expense'] . "of voucher" . "" . $voucher->id . "<br>");
                                }
                            } else if ($voucher->details->contains('dr_or_cr', 1) and $voucher->details->contains('ledger_type_id', 3)) {

                                $activityReport = ActivityReport::where('activity_id', $activity->id)->first();
                                if ($detail->dr_or_cr == 1 and $detail->ledger_type_id == 3) {

                                    $data['income'] = $detail->dr_amount;
                                    $currentExpense = $activityReport->expense;
                                    $total_expense = $activityReport->expense = $currentExpense - $data['income'];
                                    $activityReport->remain = (float)$activityReport->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $expenseHead = ExpenseHead::where('id', $activity->expense_head_id)->where('status', 1)->first();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activity->activity_id;
                                    $activityReportDetails->budget = $activity->total_budget;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->income = $data['income'];
                                    $activityReportDetails->voucher_id = $voucher->id;
                                    $activityReportDetails->month = $voucher->month;
                                    $activityReportDetails->fiscal_year = $fiscalYear->year;
                                    $activityReportDetails->save();
//                                    var_dump("this is tsa income" . $data['income'] . "of voucher" . "" . $voucher->id . "<br>");
                                }
                            }

                        }

                    } else {

//                      not pefa
                        foreach ($voucher->details as $voucherDetail) {
                            $total_expense = 0;
                            $expenseHead = ExpenseHead::where('expense_head_code', $activity->expense_head)->where('status', 1)->first();
                            if (($voucherDetail->dr_or_cr == 1 and $voucherDetail->ledger_type_id == 1) || ($voucherDetail->dr_or_cr == 1 and $voucherDetail->ledger_type_id == 4)) {

                                $activityReport = ActivityReport::where('activity_id', $activity->activity_id)->first();
                                $data['expense'] = $voucherDetail->dr_amount;
                                if ($voucherDetail->main_activity_id == $activity->activity_id) {

                                    $currentExpense = $activityReport->expense;
                                    $activityReport->expense = $total_expense = (float)$currentExpense + (float)$voucherDetail->dr_amount;
                                    $activityReport->remain = (float)$activity->budget - (float)$total_expense;
                                    $activityReport->save();

                                    $activityReportDetails = new ActivityReportDetail();
                                    $activityReportDetails->activity_report_id = $activityReport->id;
                                    $activityReportDetails->activity_id = $activity->activity_id;
                                    $activityReportDetails->budget = $activity->total_budget;
                                    $activityReportDetails->expense_head = $expenseHead->expense_head_code;
                                    $activityReportDetails->expense = $voucherDetail->dr_amount;
                                    $activityReportDetails->voucher_id = $voucher->id;
                                    $activityReportDetails->month = $voucher->month;
                                    $activityReportDetails->fiscal_year = $fiscalYear->year;
                                    $activityReportDetails->save();
//                                    var_dump("this is only expense" . $data['expense'] . "of voucher" . "" . $voucher->id . "<br>");

                                }
                            }
                        }

                    }
                }
            }

        }
//        dd("finished");

        return back();
    }

    public function getPefaVoucher()
    {

        $ledgertypeid = 9;
        $vouchers = Voucher::whereHas('details', function (Builder $query) use ($ledgertypeid) {
            $query->where('ledger_type_id', $ledgertypeid);
        })->get();
        foreach ($vouchers as $voucher) {

            if ($vouchers->details->where('dr_or_cr', 2) and $vouchers->details->where('ledger_type_id', 3)) {
                var_dump("office" . $vouchers->office_id . "v_n" . $vouchers);
            }
        }

    }

    function setPreSirData()
    {

        $activities = Budget::where('office_id', 12)->where('budget_sub_head', 26)->get(); //25 pani cha
        foreach ($activities as $activity) {

            $total_budget = ($activity->total_budget) / 0.96;
            $budget = Budget::findorfail($activity->id);
            $budget->total_budget = $total_budget;
            $budget->third_quarter_budget = $total_budget;
            $budget->save();


            $activity_details = BudgetDetails::where('budget_id', $activity->id)->get();
            foreach ($activity_details as $index => $activity_detail) {

                $budgetDetails = BudgetDetails::findorfail($activity_detail->id);
                $amount = $total_budget * 0.04;
                if (++$index == 1) {

                    $amount = $total_budget * 0.96;
                }

                $budgetDetails->total_budget = $amount;
                $budgetDetails->third_quarter_budget = $amount;
                $budgetDetails->save();
            }

        }
        return back();
    }

    function setPremSirDataTwo()
    {

        $budgetDetails = BudgetDetails::where('office_id', 12)
            ->where('budget_sub_head_id', 25)
            ->where('created_at', 'like', '2020-01-23%')
            ->whereNotIn('id', ['5813', '5814'])
            ->get();
    }


    public function getUnBalancedVoucher()
    {
        set_time_limit(0);
        $offices = Office::all();
        foreach ($offices as $office) {
            $budgetSubHeads = Programs::where('office_id', $office->id)->get();
            if ($budgetSubHeads) {
                foreach ($budgetSubHeads as $budgetSubHead) {

                    $vouchers = Voucher::where('budget_sub_head_id', $budgetSubHead->id)->get();
                    if ($vouchers) {

                        foreach ($vouchers as $voucher) {
                            if (!$voucher->details->count() < 0) {
                                var_dump("भौचर Details नभएको कार्यालय" . $voucher->office->name_nep . "बजेट उपशिर्षक " . $voucher->budget_sub_head);
                            }

                            $dr_amount = $voucher->details->sum('dr_amount');
                            $cr_amount = $voucher->details->sum('cr_amount');
                            if ($dr_amount != $cr_amount) {

                                var_dump("भौचर बराबर नभएको कार्यालय " . "->" . $voucher->office->name . " , " . "बजेट उपशिर्षक " . " ->" . $voucher->program->program_code . " , " . "voucher number" . "->" . $voucher->jv_number . "<br>");

                            }

                        }
                    }

                }
            }

        }

    }


    public function setBudgetDetailsName()
    {

//        $officeId = [23,24,28,29,30];
//        $budgetSubHeadId = [141,142,143,144,145,146,147,148];
        $officeId = 30; //29,28,23,24
        $budgetSubHeadId = 148;   //141,142,143,144,145,146,147
        $budgets = Budget::where('office_id', $officeId)
            ->where('budget_sub_head', $budgetSubHeadId)->get();
        foreach ($budgets as $budget) {

            $budgetDetails = BudgetDetails::where('budget_id', $budget->id)->get();
            foreach ($budgetDetails as $index => $budgetDetail) {

                if (++$index == 1) {
                    $budgetDetail->sub_activity = $budget->activity;
                } else {
                    $budgetDetail->sub_activity = $budget->activity . " - " . "कन्टेन्जेन्सी";
                }
                $budgetDetail->save();
            }
        }

        $activityReports = ActivityReport::where('office_id', $officeId)->where('budget_sub_head_id', $budgetSubHeadId)->get();
        foreach ($activityReports as $activityReport) {

            $activityReport->remain = $activityReport->budget;
            $activityReport->save();
        }
        return back();
    }

    public function setGorakhaSichaiData()
    {

        $budgets = Budget::where('office_id', '=', 29)
            ->where('budget_sub_head', '=', 142)
            ->where('expense_head', '=', 31171)
            ->get();
        foreach ($budgets as $budget) {

            $budgetDetails = BudgetDetails::where('budget_id', '=', $budget->id)->get();
            foreach ($budgetDetails as $index => $budgetDetail) {
                for ($i = 1; $i < 2; $i++) {

                    $budgetDetail->total_budget = $budget->total_budget * 0.4;
                    $budgetDetail->sub_activity = $budget->activity . " - " . "कन्टेन्जेन्सी";
                    if ($i == 1) {
                        $budgetDetail->total_budget = $budget->total_budget * 0.96;
                        $budgetDetail->sub_activity = $budget->activity;
                    }
                    $budgetDetail->save();
                }

            }
        }
        return back();
    }

    public function setGorakhaSichaiDataActivityReport()
    {
        $budgets = Budget::where('office_id', '=', 29)
            ->where('budget_sub_head', '=', 142)
            ->where('expense_head', '=', 31171)
            ->get();
        foreach ($budgets as $budget) {

            $budgetDetails = BudgetDetails::where('budget_id', '=', $budget->id)->get();
            foreach ($budgetDetails as $index => $budgetDetail) {

                if (++$index == 1) {

                } else {

                    $budgetDetail->total_budget = $budget->total_budget * 0.04;
                    $budgetDetail->save();
                }
            }

            $activityReport = ActivityReport::where('office_id', '=', 29)->where('budget_sub_head', '=', 142)->get();
        }
    }

    public function deleteGorakhaSichainActivityReportDetails(){

        $activityReportDetails = ActivityReportDetail::join('activity_report','activity_report_details.activity_report_id','=','activity_report.id')
                                            ->where('activity_report.office_id','=',29)
                                            ->where('activity_report.budget_sub_head_id','=',64)->get();
        foreach ($activityReportDetails as $activityReportDetail){
            $activityReportDetailsRow = ActivityReportDetail::findorfail($activityReportDetail->id);
            $activityReportDetailsRow->delete();
            $activityReportDetail->save();
        }
        dd("stop");
    }

    public function setActivityReportDetailsGorakhaSichai(Request $request){
        if ($request->hasFile('import_file')) {
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                $programs_array = $reader->toArray();
                $program_collections = collect($programs_array);
                foreach ($program_collections as $row) {

                    if (count($row) != 0) {
                        $data = [];
                        $activityReportDetails = new ActivityReportDetail();
                        $activityReportDetails->id = (int)$row['id'];
                        $activityReportDetails->activity_report_id = (int)$row['activity_report_id'];
                        $activityReportDetails->activity_id = (int)$row['activity_id'];
                        $activityReportDetails->budget = (int)$row['budget'];
                        $activityReportDetails->akhtiyari_type = (int)$row['akhtiyari_type'];
                        $activityReportDetails->expense = (float)$row['expense'];
                        $activityReportDetails->voucher_id = (int)$row['voucher_id'];
                        $activityReportDetails->month = (int)$row['month'];
                        $activityReportDetails->fiscal_year = $row['fiscal_year'];
                        $activityReportDetails->created_at = $row['created_at'];
                        $activityReportDetails->updated_at = $row['updated_at'];
                        $activityReportDetails->save();

                    }
                }
            });
        }
        return back();
    }

    public function fixCurrentPefa(){
        $voucherDetails = VoucherDetail::where('dr_or_cr','=',2)
                                        ->where('ledger_type_id','=',9)
            ->whereNull('voucher_details_id')->get();
        dd($voucherDetails);
    }

    public function checkBhutaniAmount(){
        $office_id = 29;
        $budget_sub_head_id = 64;
        $bhuktanies = Bhuktani::where('office_id',$office_id)->where('budget_sub_head',$budget_sub_head_id)->get();
        foreach ($bhuktanies as $bhuktani){
            $preBhuktanies = PreBhuktani::where('bhuktani_id',$bhuktani->id)->sum('amount');
            $bhuktaniModel = Bhuktani::findorfail($bhuktani->id);
            if(floatval($bhuktani->amount) != floatval($preBhuktanies)){
                var_dump("bhuktani aadesh number"." ".$bhuktani->adesh_number." "."bhutani amount".$bhuktani->amount. "prebhuktani amount".$preBhuktanies ."<br>");
                $bhuktaniModel->amount = $preBhuktanies;
                $bhuktaniModel->save();
            }
        }
        dd("stop");
    }

}