@extends('frontend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> <i class="fas fa-university"></i> चेक प्रिन्ट</h1>
        </section>

        <!-- FORM section start -->

        <!-- FORM section ends -->

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body pt-15">
                    <table class="table table-bordered" id="cheque_print_table">
                        <thead>
                            <tr>
                                <th style="text-align: center">क्र.स.</th>
                                <th style="text-align: center">बजेट उपशिर्षक</th>
                                <th style="text-align: center">भौचर न.</th>
                                <th style="text-align: center">मिति</th>
                                <th style="text-align: center">प्राप्त कर्ता</th>
                                <th style="text-align: center">रकम</th>
                                <th style="text-align: center">चेक न.</th>
                                <th style="text-align: center">प्रिन्ट</th>
                                <th style="text-align: center">हर्ने</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pre_bhuktani_list as $index => $item)
                                <tr>
                                    <td class="kalimati bhuktani_id" data-bhuktani-id={{ $item['pre_bhuktani_id'] }}
                                        style="text-align: center">{{ ++$index }}</td>
                                    <td class="kalimati" style="text-align: center">{{ $item['budget_sub_head_name'] }}</td>
                                    <td class="kalimati" style="text-align: center">{{ $item['journal_number'] }}</td>
                                    <td class="kalimati" style="text-align: center">{{ $item['journal_date'] }}</td>
                                    <td class="kalimati" style="text-align: center">{{ $item['party_name'] }}</td>
                                    <td class="kalimati amount" style="text-align: right" data-amount={{ $item['amount'] }}>
                                        {{ number_format($item['amount']) }}</td>
                                    <td class="cheque_number_id">
                                        <select name="cheque" class="form-control" required>
                                            <option value="">---चेक न. छान्नुहोस्---</option>
                                            @foreach ($cheque_details as $cheque_detail)
                                                <option value="{{ $cheque_detail->id }}">
                                                    {{ $cheque_detail->cheque_number }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="" id="cheque_print"><a href="#">चेक प्रिन्ट</a>
                                    </td>
                                    <td class="journel_id" data-journal-id={{ $item['journal_id'] }}
                                        style="text-align: center"><a
                                            href="{{ route('voucher.view', ['id' => $item['journal_id']]) }}"
                                            target="_blank">View </a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).on('click', '#cheque_print', function() {
            cheque_number = $(this).closest('tr').find('td.cheque_number_id select option:selected').val()
            if (cheque_number) {
                let journal_id = $(this).closest('tr').find('td.journel_id').attr('data-journal-id')
                let cheque_number_id = $(this).closest('tr').find('td.cheque_number_id select option:selected')
                    .val()
                let cheque_number = $(this).closest('tr').find('td.cheque_number_id select option:selected')
                    .text()
                let bhuktani_id = $(this).closest('tr').find('td.bhuktani_id').attr('data-bhuktani-id')

                let amount = $(this).closest('tr').find('td.amount').attr('data-amount')

                let url = '{{ route('cheque.print.confirm') }}';
                let data = {
                    "journal_id": journal_id,
                    "cheque_number_id": cheque_number_id,
                    "cheque_number": cheque_number,
                    "bhuktani_id": bhuktani_id,
                    "amount": amount
                }
                swal({
                    title: "के तपाईँ निश्चित हुनुहुन्छ?",
                    text: "OK गरेपछि चेक नम्बर फर्किदैन",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,

                }).then((willDelete) => {

                    if (willDelete) {
                        $.ajax({
                            url: url,
                            method: 'GET',
                            data: data,
                            success: function(res) {
                                if (res) {
                                    swal("चेक प्रिन्ट भयो!", {
                                        icon: "success",
                                    });

                                    location.reload();
                                }

                            }
                        })

                    } else {
                        swal("चेक प्रिन्ट  भएन!");
                    }
                });
            } else {
                alert("कृपया चेक न. छान्नुहोस्")
            }

        })
    </script>
@endsection
