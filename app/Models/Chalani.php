<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chalani extends Model
{
    protected $table = 'chalani';
    protected $guarded = [];

    public function GetUser()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

}
