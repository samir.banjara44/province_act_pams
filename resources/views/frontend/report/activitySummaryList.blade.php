<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">


<style>
    table {
        border-collapse: collapse;
    }

    @media print {
        #downloadMe {
            display: none;
        }
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <input type="button" style="position: absolute; top:20px;right: 20px" value="Export to Excel" id="downloadMe">
    <section class="content-header">
        {{--      <a id="dlink"  style="display:none;"></a>--}}

        {{--      <input type="button" onclick="tableToExcel('contentTable', 'name', 'myfile.xls')" value="Export to Excel">--}}
        <div id="tableHeadingWrapper">
            <table width="99%" id="headingTable" style="font-size:13px">
                <tr>
                    <img src="{{ asset('img/nepal-govt-logo.png')}}"
                         style="position: absolute;left: 20px;height: 80px;width:100px;" alt="">
                              <td colspan="6" style="text-align: center">@if((Auth::user()->office->id != 43) or (Auth::user()->office->id != 47))प्रदेश सरकार@else प्रदेश सभा @endif</td>
                </tr>
{{--                <tr>--}}
{{--                    <td colspan="6" style="text-align: center"></td>--}}
{{--                </tr>--}}
                <tr>
                    <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43){{Auth::user()->office->ministry->name}}@endif</td>
                </tr>
                @if(Auth::user()->office->department)
                    <tr>
                        <td colspan="6" style="text-align: center">{{Auth::user()->office->department->name}}</td>
                    </tr>
                @endif
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}, {{Auth::user()->office->district->name}}</td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div style="width: 100%; text-align: center">
                            <b>कार्यक्रम / परियोजना अनुसार बजेट बिनियोजन</b><br>
                        </div>
                        <div style="float: right; margin-top: -20px">

                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                        <br>

                    </td>
                </tr>
                <tr>
                    <td>बजेट उपशीर्षक न. : <b><span class="kalimati">{{$program->program_code}}</span></b><br>
                        बजेट उपशीर्षक नाम: <b>{{$program->name}}</b></td>
                    <td style="text-align: right">आर्थीक वर्ष : <span class="">२०७६/७७</span></td>

                </tr>

            </table>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body" id="tableWrapper">
                <table width="99%" border="1" id="contentTable" style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th rowspan="3" width="67px">सि.नं.</th>
                        <th rowspan="3" style="width: 28%;">कार्यक्रम/आयोजनाको नाम</th>
                        <th rowspan="3">खर्च शीर्षक</th>
                        <th colspan="">विनियोजन रु.</th>
                        <th colspan="">खर्च</th>
                        <th rowspan="3">बाकींँ</th>
                        <th rowspan="3">कैफियत</th>
                    </tr>

                    </thead>
                    <tbody>
                    @foreach($activities as $index=>$activity)
                        <tr style="background-color: #ffffff">

                            <td class="kalimati" style="text-align: center">{{++$index}}</td>
                            <td>@if($activity->getActivityName){{$activity->getActivityName->sub_activity}}@endif</td>
                            <td class="kalimati" style="text-align: center">{{$activity->expense_head}}</td>
                            <td class="kalimati" style="text-align: right">{{$activity->budget}}</td>

                            <td class="kalimati" style="text-align: right">{{$activity->expense}}</td>
                            <td class="kalimati" style="text-align: right">{{$activity->remain}}</td>

                            <td></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" style="text-align: right">जम्मा</td>
                        <td class="kalimati" style="text-align: right"><span class="kalimati">{{$totalBudget}}</span></td>
                        <td class="kalimati" style="text-align: right">{{$totalExpense}}</td>
                        <td class="kalimati" style="text-align: right">{{$totalRemain}}</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script>
    let changeToNepali = function (text) {
        // alert(text);
        textTrim = $.trim(text)
        let numbers = textTrim.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (!isNaN(value) && value != ' ') {
                    if (value == 1)
                        nepaliNo += "१";
                    else if (value == 2)

                        nepaliNo += "२";
                    else if (value == 3)

                        nepaliNo += "३";
                    else if (value == 4)

                        nepaliNo += "४";
                    else if (value == 5)

                        nepaliNo += "५";
                    else if (value == 6)

                        nepaliNo += "६";
                    else if (value == 7)

                        nepaliNo += "७";
                    else if (value == 8)

                        nepaliNo += "८";
                    else if (value == 9)

                        nepaliNo += "९";
                    else if (value == 0)

                        nepaliNo += "०";
                    else if (value == ',')

                        nepaliNo += ",";
                    else if (value == '.')

                        nepaliNo += ".";
                    else if (value == '/')

                        nepaliNo += "/";

                }
            }
        });
        console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });


    var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
            ,
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta charset="utf-8"/><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
            , format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            };

        return function (table, name, filename) {
            var table1 = '';
            if (!table1.nodeType) table1 = document.getElementById('headingTable')
            if (!table.nodeType) table = document.getElementById(table)
            var bigTable = table1.innerHTML + table.innerHTML;
            var ctx = {worksheet: name || 'Worksheet', table: bigTable}

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

    $("#downloadMe").click(function (e) {
        let allTable = $('div[id$=tableHeadingWrapper]').html() + $('div[id$=tableWrapper]').html();
        console.log(allTable);
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(allTable));
        e.preventDefault();
    });

</script>