<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBhuktaniVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bhuktani_vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('office_id');
            $table->integer('budget_sub_head_id');
            $table->float('voucher_id');
            $table->text('bhuktani_id');
            $table->dateTime('date_english');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bhuktani_vouchers');
    }
}
