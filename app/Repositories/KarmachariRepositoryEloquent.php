<?php
namespace App\Repositories;

use App\Http\Controllers\Frontend\AdvanceAndPaymentController;
use App\Models\Karmachari;
use Illuminate\Support\Facades\Auth;

class KarmachariRepositoryEloquent implements KarmachariRepository
{
    public function create($attributes) {
        $karmachari = new Karmachari();
        $karmachari->name_nepali = $attributes['name_nepali'];
        $karmachari->name_english = $attributes['name_english'];
        $karmachari->gender = $attributes['gender'];
        $karmachari->date_of_birth = $attributes['date_of_birth'];
        $karmachari->marital_status = $attributes['marital_status'];
        $karmachari->vat_pan = $attributes['vat_pan'];
        $karmachari->prvince_id = $attributes['province'];
        $karmachari->district_id = $attributes['district'];
        $karmachari->staniha_taha = $attributes['local_level'];
        $karmachari->phone_number = $attributes['phone_number'];
        $karmachari->mobile_number = $attributes['mobile_number'];
        $karmachari->email_address = $attributes['email_address'];
        $karmachari->office_id = Auth::user()->office->id;
        $karmachari->darbandi_srot_id = $attributes['darbandi_srot_id'];
        $karmachari->darbandi_type_id = $attributes['darbandi_type_id'];
        $karmachari->sheet_roll_no = $attributes['sheet_roll_no'];
        $karmachari->sewa_barga = $attributes['sewa_barga'];
        $karmachari->sewa_id = $attributes['sewa_id'];
        $karmachari->samuha_id = $attributes['samuha_id'];
        $karmachari->taha_id = $attributes['taha_id'];
        $karmachari->pad_id = $attributes['pad_id'];
        $karmachari->is_office_head = $attributes['office_head'];
        $karmachari->is_acc_head = $attributes['acc_head'];
        $karmachari->is_peski = $attributes['is_peski'];
        $karmachari->is_payroll = $attributes['is_payroll'];
        $karmachari->can_accept = $attributes['can_accept'];
        $karmachari->status =$attributes['status'];
        $karmachari->bank =$attributes['bank'];
        $karmachari->khata_number =$attributes['khata_number'];
        $karmachari->bank_address =$attributes['bank_address'];
        $karmachari->save();
        return $karmachari->id;
    }

    public function get_by_id($id){

      return  $karmachari =  Karmachari::findorfail($id);
    }

    public function update($attributes, $id){
        $karmachari = $this->get_by_id($id);
        $karmachari->name_english = $attributes['name_english'];
        $karmachari->name_nepali = $attributes['name_nepali'];
        $karmachari->gender = $attributes['gender'];
        $karmachari->date_of_birth = $attributes['date_of_birth'];
        $karmachari->marital_status = $attributes['marital_status'];
        $karmachari->vat_pan = $attributes['vat_pan'];
        $karmachari->prvince_id = $attributes['province'];
        $karmachari->district_id = $attributes['district'];
        $karmachari->staniha_taha = $attributes['local_level'];
        $karmachari->phone_number = $attributes['phone_number'];
        $karmachari->mobile_number = $attributes['mobile_number'];
        $karmachari->email_address = $attributes['email_address'];
        $karmachari->darbandi_srot_id = $attributes['darbandi_srot_id'];
        $karmachari->darbandi_type_id = $attributes['darbandi_type_id'];
        $karmachari->sheet_roll_no = $attributes['sheet_roll_no'];
        $karmachari->sewa_barga = $attributes['sewa_barga'];
        $karmachari->sewa_id = $attributes['sewa_id'];
        $karmachari->samuha_id = $attributes['samuha_id'];
        $karmachari->taha_id = $attributes['taha_id'];
        $karmachari->pad_id = $attributes['pad_id'];
        $karmachari->is_office_head = $attributes['office_head'];
        $karmachari->is_acc_head = $attributes['acc_head'];
        $karmachari->is_peski = $attributes['is_peski'];
        $karmachari->is_payroll = $attributes['is_payroll'];
        $karmachari->can_accept = $attributes['can_accept'];
        $karmachari->status =$attributes['status'];
        $karmachari->bank = $attributes['bank'];
        $karmachari->bank_address = $attributes['bank_address'];
        $karmachari->khata_number = $attributes['khata_number'];
        $karmachari->save();
        return $karmachari;

    }

    public function get_by_office_id($office_id){

       return $karmacharList = Karmachari::where('office_id',$office_id)->where('status',1)->get();
    }

    public function getAllKarmachariByApproveStatus(){

        $office_id = Auth::user()->office_id;
       return $karmacharies = Karmachari::where('can_accept',1)
           ->where('office_id',$office_id)
           ->get();

    }


}