<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\District;
use App\Repositories\DistrictRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class DistrictController extends Controller
{

//    private $districtRepo;

//    public function __construct(DistrictRepository $districtRepo)
//    {
//        $this->middleware('auth');
//        $this->districtRepo = $districtRepo;
//    }

    public function get_districts_by_province($province_id)
    {

        $districts= District::where('province_id',$province_id)->get();
        return json_encode($districts);
    }

}
