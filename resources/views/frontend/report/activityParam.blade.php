@extends('frontend.layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><i class="fas fa-search"></i> कार्यक्रम खोज्ने </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="panel panel-primary">
      <div class="panel-body p-15">
        {{--          {{route('report.bhuktani.adesh')}}--}}
        <form action="{{route('get.activity.list')}}" method="post" target="_blank">
          {{csrf_field()}}
          <div class="form-group">
            <div class="row">

              <div class="col-md-4">
                <label for="office">कार्यालय : </label>
                <select name="office" class="form-control">
                  <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}</option>
                </select>
              </div>

              <div class="col-md-2">
                <label>आर्थिक वर्ष : </label>
                <select name="fiscal_year" id="fiscal_year" class="form-control">
                  @foreach($fiscalYears as $fy)
                    <option value="{{$fy->id}}" @if($fy->id == $fiscalYear->id) selected @endif>{{$fy->year}}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-md-4">
                <label>बजेट उप शिर्षक</label>
                <select class="form-control select2" name="budget_sub_head" id="budget_sub_head" required>
                  <option value="">................</option>
                  @foreach($programs as $program)
                  <option value="{{$program->id}}">{{$program->name}} | {{$program->program_code}}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-md-2">
                <label>&nbsp;</label>
                <button type="submit" class="btn btn-primary btn-block" id="btn_activity_list"><i
                    class="fas fa-search"></i> Search</button>

              </div>

            </div>
          </div>

          {{--            <div class="form-group center-block">--}}
          {{--              <label for="budget_type">बजेट प्रकार</label>--}}
          {{--              <select class="form-control" name="month" id="month" required>--}}
          {{--                <option value="10">चालु</option>--}}
          {{--                <option value="10">पुजी</option>--}}
          {{--              </select>--}}
          {{--            </div>--}}

        </form>
        {{--          <a href="{{route('downloadExcel/xls')}}"><button class="btn btn-success">Download Excel
          xls</button></a>--}}
      </div>
    </div>
  </section>
  <!-- /.content -->

</div>

@endsection

@section('scripts')
{{--  <script>--}}
{{--    $(document).on('click','#btn_bhuktani_list',function (e) {--}}

{{--      let data = {};--}}
{{--     data['fiscal_year'] = $('select#fiscal_year').val();--}}
{{--     data['budget_sub_head'] = $('select#budget_sub_head').val();--}}
{{--     data['month'] = $('select#month').val();--}}
{{--      let url = '{{route('get.bhuktani.list')}}';--}}

{{--       console.log(data);--}}
{{--       $.ajax({--}}
{{--         url : url,--}}
{{--         method : 'GET',--}}
{{--         data : data,--}}
{{--         success : function(res_) {--}}
{{--           let tr = '';--}}
{{--           let parseRes = $.parseJSON(res_);--}}
{{--           console.log(parseRes);--}}
{{--           if(parseRes.length>0){--}}
{{--             let i = 1;--}}
{{--             $.each(parseRes, function (key, value) {--}}
{{--               let url = '{{route('report.bhuktani.adesh',123)}}';--}}
{{--               url = url.replace('123',this.id);--}}
{{--               tr += "<tr>" +--}}
{{--                       "<td>" +--}}
{{--                       i +--}}
{{--                       "</td>" +--}}

{{--                       "<td class='activity' data-id=''>" +--}}
{{--                       this.adesh_number +--}}
{{--                       "</td>" +--}}

{{--                       "<td class='byahora'>" +--}}
{{--                       this.date_nepali +--}}
{{--                       "</td>" +--}}

{{--                       "<td class='details'>" +--}}
{{--                       this.amount +--}}
{{--                       "</td>" +--}}


{{--                       "<td>" +--}}
{{--                       '<a href="'+url+'" target="_blank" onclick="getBhuktaniReport(event)" data-index="'+ key +'" >हेर्ने</a>' +--}}
{{--                       "</td>";--}}
{{--               i = i + 1;--}}

{{--             })--}}
{{--             $('#bhuktani_list_table').find('tbody').html(tr);--}}

{{--           } else {--}}

{{--             tr += "<tr>" +--}}
{{--                     "<td colspan='7' align='center'>" +--}}
{{--                        "विवरण प्रविश्टि भएको छैन !!"+--}}
{{--                     "</td>";--}}
{{--             $('#bhuktani_list_table').find('tbody').html(tr);--}}
{{--           }--}}


{{--         }--}}

{{--       })--}}
{{--    })--}}
{{--  </script>--}}


<script>
  {{--$(document).on('click','#btn_activity_list',function (e) {--}}
    {{--  e.preventDefault();--}}
    {{-- let budget_sub_head_id = $('#budget_sub_head').val();--}}

    {{--  let url = '{{route('get.activity.list',123)}}';--}}
    {{--  url = url.replace(123,budget_sub_head_id);--}}

    {{--  $.ajax({--}}

    {{--    method : 'get',--}}
    {{--    url : url,--}}
    {{--    success : function (result) {--}}


    {{--    }--}}

    {{--  })--}}
    {{--})--}}


</script>

@endsection