<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Ministry;
use App\Models\Mof;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;

class DepartmentController extends Controller
{

    private $homeRepo;

    public function __construct(HomeRepository $homeRepo)
    {

        $this->middleware('auth');
        $this->homeRepo = $homeRepo;
    }

    public function index()
    {
        $departments = Department::all();
        return view('backend.settings.department.index', compact('departments'));
    }

    public function create(){
        $provinces = Province::all();
        $mofs = Mof::all();
        $ministries = Ministry::all();
        return view('backend.settings.department.create', compact('provinces', 'mofs', 'ministries'));
    }
    public function store(Request $request){

        $department = new Department();
        $department->name = $request->name;
        $department->province_id = $request->province_id;
        $department->mof_id = $request->mof_id;
        $department->ministry_id = $request->ministry_id;
        $department->status = 1;
        $department->save();
        return redirect(route('admin.department'));
    }


}
