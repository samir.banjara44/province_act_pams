<?php
namespace App\Repositories;

use App\Models\Area;
use App\Models\VoucherSignature;
use Illuminate\Support\Facades\Auth;

class VoucherSignatureRepositoryEloquent implements VoucherSignatureRepository
{


    public function create($attributes) {

        $office_id = Auth::user()->office_id;
        $voucher_signature = new VoucherSignature();
        $voucher_signature->prepare_by = $attributes['prepared_by'];
        $voucher_signature->submit_by = $attributes['submit_by'];
        $voucher_signature->verify_by = $attributes['approved_by'];
        $voucher_signature->office_id =$office_id;
        $voucher_signature->status = 1;
        $voucher_signature->save();
        return $voucher_signature;
    }

    public function get_by_office_id($office_id){

        return $voucher_signatures = VoucherSignature::where('office_id',$office_id)
            ->with('karmachari_prepare_by','karmachari_submit_by','karmachari_approved_by')
            ->orderBy('id', 'desc')
            ->first();
    }
}