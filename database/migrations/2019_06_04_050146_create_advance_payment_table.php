<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvancePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('party_type')->nullable();
            $table->string('name_nep');
            $table->string('name_eng')->nullable();
            $table->string('citizen_number')->nullable();
            $table->string('vat_pan_number')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('address')->nullable();
            $table->integer('bank')->nullable();
            $table->string('bank_address')->nullable();
            $table->string('party_khata_number')->nullable();
            $table->integer('payee_code');
            $table->integer('vat_office')->nullable();
            $table->integer('karmachari_id');
            $table->integer('is_peski')->nullable();
            $table->integer('is_dharauti')->nullable();
            $table->integer('is_bhuktani')->nullable();
            $table->integer('office_id');
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advance_payment');
    }
}
