<?php

namespace App\Repositories;

use App\helpers\BsHelper;
use App\helpers\Money_words;
use App\Models\Bhuktani;
use App\Models\PreBhuktani;
use Illuminate\Support\Facades\Auth;

class BhuktaniRepositoryEloquent implements BhuktaniRepository
{

    public function getAllBhuktani()
    {

        return $areas = Bhuktani::all();

    }

    public function create($attributes)
    {

        $bhuktani = new Bhuktani();
        $bhuktani->office_id = Auth::user()->office->id;
        $bhuktani->budget_sub_head = $attributes['budget_sub_head'];
        $bhuktani->adesh_number = $attributes['adesh_number'];
        $bhuktani->amount = $attributes['totalAmount'];
        $bhuktani->date_nepali = $attributes['date'];
        $mymonth = (explode('-', $attributes['bs-roman']));
        $month = intval($mymonth[1]);
        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }

        $bhuktani->fiscal_year = $attributes['fiscal_year'];

        $date_array = explode('-', $attributes['bs-roman']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
        $bhuktani->date_english = $data_ad_array['year'] . '-' . $data_ad_array['month'] . '-' . $data_ad_array['date'];
        $bhuktani->month = $finalmonth;
        $bhuktani->status = 1;
        $bhuktani->is_contenjency = 0;
        $bhuktani->save();
        return $bhuktani->id;
    }

    public function get_bhuktani_adesh_number_by_program($fiscal_year, $program_id)
    {
        $adesh_number = Bhuktani::
                where('fiscal_year', $fiscal_year)
            ->where('budget_sub_head', $program_id)
            ->latest()
            ->first();
        if ($adesh_number) {
            $bhuktani_adesh_number = $adesh_number->adesh_number;
        } else {
            $bhuktani_adesh_number = 0;
        }
        return $bhuktani_adesh_number + 1;

    }

    public function get_by_budget_sub_head_and_other($parameter)
    {
        $office_id = Auth::user()->office_id;
        return $bhuktani = Bhuktani::where('budget_sub_head', $parameter['budget_sub_head'])
            ->where('fiscal_year', $parameter['fiscal_year'])
            ->where('month', $parameter['month'])
            ->where('status', '=', 1)
            ->where('office_id', $office_id)->get();
    }

    public function get_by_budget_sub_head_id($fiscal_year, $budget_sub_head_id)
    {
        $office_id = Auth::user()->office_id;
        return $bhuktani = Bhuktani::where('fiscal_year', $fiscal_year)
            ->where('budget_sub_head', $budget_sub_head_id)
            ->where('status', '=', 1)
            ->where('is_contenjency', '=', 0)
            ->where('office_id', $office_id)->get();
    }

    public function get_bhuktani_by_bhuktani_id($id)
    {

        return $bhuktani = Bhuktani::findorfail($id);
    }

    public function delete($bhuktani_id)
    {
        $pre_bhuktanis = PreBhuktani::where('bhuktani_id', $bhuktani_id)->get();
        foreach ($pre_bhuktanis as $pre_bhuktani) {

            $data = PreBhuktani::findorfail($pre_bhuktani['id']);
            $data['bhuktani_id'] = 0;
            $data['status'] = 0;
            $data->save();
        }

        $bhuktani = Bhuktani::findorfail($bhuktani_id);
       return $bhuktani->delete();
    }

    public function update_by_bhuktani_amount_and_status($bhuktani_id, $pre_bhuktani_amount)
    {

        $bhuktani = Bhuktani::findorfail($bhuktani_id);
        $update_amount = ($bhuktani['amount']) - floatval($pre_bhuktani_amount);
        $bhuktani->amount = $update_amount;
        $bhuktani->status = 0;
        $bhuktani->save();

        return $bhuktani;


    }

    public function update_by_bhuktani_add_amount_and_status($bhuktani_id, $pre_bhuktani_amount)
    {

        $bhuktani = Bhuktani::findorfail($bhuktani_id);
        $update_amount = ($bhuktani['amount']) + floatval($pre_bhuktani_amount);
        $bhuktani->amount = $update_amount;
        $bhuktani->status = 1;
        $bhuktani->save();
        return $bhuktani;
    }

    public function get_amount_in_word($amount)
    {

        $obj = new Money_words();
        return $words = $obj->convert_number($amount);

    }


    public function get_amount_by_multiple_id($ids)
    {

        return $totalAmount = Bhuktani::wherein('id', $ids)->sum('amount');
    }


    public function updateIsContenjencyById($bhuktani_id)
    {

        $bhuktani = Bhuktani::findorfail($bhuktani_id);
        $bhuktani->is_contenjency = 1;
        return $bhuktani->save();
    }
}