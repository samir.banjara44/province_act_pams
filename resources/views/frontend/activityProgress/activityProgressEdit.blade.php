@extends('frontend.layouts.app')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1><i class="fas fa-university"></i> प्रोग्रेस विवरण </h1>

            <button type="button" class="btn btn-primary btn-sm btn-show-form" id="showAddForm"><i
                        class="fas fa-plus"></i> नयाँ थप गर्ने
            </button>
        </section>
        <section class="formSection">
            <div class="row justify-content-md-center">
                <div class="col-md-10 form-bg col-md-offset-1">
                    <button type="button" class="btn btn-danger btn-sm btn-show-form displayNone" id="closeAddForm"><i
                                class="fas fa-times"></i> फर्म रद्ध गर्ने
                    </button>
                    <div class="form-title">बजेट विवरण प्रविष्टी गर्ने</div>
                    <form action="{{route('progress.update',$progressReport->id)}}" method="post" id="formSubmit">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label for="fiscal_year"> आर्थिक वर्ष:</label>
                                            <select id="fiscal_year" class="form-control" name="fiscal_year" required>
                                                <option value="{{$progressReport->fiscal_year}}" >{{$progressReport->fiscal_year}}</option>
                                            </select>
                                        </div>

                                        <div class="col-md-2">
                                            <label for="fiscal_year"> मिति </label>
                                            <input type="text" class="form-control" name="date" id="date" value=""
                                                   required>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="expense_center">खर्च केन्द्र</label>
                                            <select id="expense_center" class="form-control" name="expense_center"
                                                    required>
                                                <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}
                                                </option>
                                            </select>
                                        </div>

                                        <div class="col-md-5">
                                            <label for="budget_sub_head">बजेट उपशिर्षक</label>
                                            <select name="program" class="form-control" id="program"
                                                    required>
                                                <option value="">................</option>
                                                @foreach($programs as $program)
                                                    <option value="{{$program->id}}" @if($program->id == $progressReport->budget_sub_head_id) selected @endif>{{$program->name}} |{{$program->program_code}} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="mainActivityName">कार्यक्रम आयोजनाको नाम</label>
                                            <select name="main_activity_id" class="form-control select2"
                                                    id="main_activity_id" required>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">चौमासिक प्रकार</label>
                                            <select name="chaumasik_type" class="form-control" id="chaumasik_type"
                                                    required>
                                                <option value="">................</option>
                                                <option value="1" @if($progressReport->chaumasik_type == 1) selected @endif>प्रथम चौमासिक</option>
                                                <option value="2" @if($progressReport->chaumasik_type == 2) selected @endif>दोस्रो चौमासिक</option>
                                                <option value="3" @if($progressReport->chaumasik_type == 3) selected @endif>तेस्रो चौमासिक</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>कार्यक्रम प्रोग्रेस परिमाण</label>
                                            <input type="number" class="form-control" name="progress_quantity" id="progress_quantity" value="{{$progressReport->progress_quantity}}"
                                                   style="width: 189px"/>
                                        </div>
                                        <div class="col-md-3">
                                            <label>कार्यक्रम प्रोग्रेस</label>
                                            <textarea class="form-control" name="progress" id="progress" style="width: 270px">{{$progressReport->activity_progress_detail}}</textarea>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">&nbsp;</label>
                                            <button type="submit" id="submitBtn" class="btn btn-primary"><i class="fas fa-save"></i>Update</button>
                                            <a type="button" class="btn btn-primary" href="{{route('activity.process.edit',$progressReport->id)}}">Clear </a>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div style="float: left">
                                        <li>
                                            कार्यक्रम बजेट : <span id="activityBudgetText" style="color: red"></span>
                                        </li>
                                        <li>
                                            कार्यक्रम खर्च : <span id="activityExpenseText" style="color: red"></span>
                                        </li>
                                        <li>
                                            कन्टेन्जेन्सी खर्च : <span id="contenjencyExpenseText"
                                                                       style="color: red"></span>
                                        </li>
                                        <li>
                                            जम्मा खर्च : <span id="totalExpenseText" style="color: red"></span>
                                        </li>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>

    </div>
@endsection

@section('scripts')
    {{-- बजेट उपशिर्षक change हुदा  कार्यक्रम आउने  Event--}}
    <script>
        $(document).ready(function () {
            $('#program').attr("disabled",false);
            $('#program').change(function () {
                let program_code = $('#program').val();
                get_main_activity_by_budget_sub_head(program_code);
            })
        })
    </script>

    {{-- बजेट उपशिर्षक change हुदा कार्यक्रम आउने  function--}}
    <script>
        let get_main_activity_by_budget_sub_head = function (budgetSubHeadId, activity_id = '') {
            let url = '{{route('get.budget',123)}}';
            url = url.replace(123, budgetSubHeadId);
            let activityProgressJson = '{!! $progressReport !!}'
           let activityProgress = $.parseJSON(activityProgressJson);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let option = '<option value="">.......</option>';
                    let activities = $.parseJSON(res);
                    if (res) {
                        $.each(activities, function () {
                            let activityCode = this.activity_code ? ' | ' + this.activity_code : '';
                            if (this.id == parseInt(activityProgress.budget_details_id)) {
                                option += '<option value="' + this.id + '" selected>' + this.activity + activityCode + this.expense_head + ' |रु. ' + this.total_budget + '</option>';
                            } else {
                                option += '<option value="' + this.id + '">' + this.activity + activityCode + ' | ' + this.expense_head + ' | ' + this.total_budget + '</option>'
                            }
                        })
                    }
                    $('#main_activity_id').html(option).change();
                }
            })
        }
    </script>


    {{--    Activity click --}}
    <script>
        let originalRemaining = 0;
        $(document).ready(function () {
            $(document).on('change', '#main_activity_id', function () {
                let activity_id = $('#main_activity_id').val();
                get_budget_details_by_id(activity_id);
                $('#total_budget').focus();
                $('#activityExpenseText').text('');
                $('#contenjencyExpenseText').text('');
                $('#totalExpenseText').text('');
                $(this).parents('tbody').find('tr').css('background-color', '#FFF');
                $(this).parents('tr').css('background-color', '#cdbb8b');
            });
        });

        let get_budget_details_by_id = function (activity_id) {
            let url = '{{route('get_budget_details_by_id',123)}}';
            url = url.replace('123', activity_id);
            let totalExpenseText = 0;
            $.ajax({
                method: 'get',
                url: url,
                success: function (res) {
                    let budget_details = editing_budget_detail = $.parseJSON(res);
                    let akhtiyari = $('#totalAkhtiyariHiden').val();
                    let biniyojan = $('#totalBiniyojanHiden').val();
                    let remain = originalRemaining + parseFloat(budget_details.details.total_budget);
                    biniyojan_edit = parseFloat(akhtiyari) - parseFloat(remain);

                    $('#totalBiniyojanHiden').val(biniyojan_edit);
                    $('#budget_sub_head_total_budget').text(biniyojan_edit).addClass('e-n-t-n-n');
                    $('#remain').val(remain);
                    $('#remain_budget').text(budget_details.budget_details).addClass('e-n-t-n-n');
                    $('#activityBudgetText').text(budget_details.details.total_budget).addClass('e-n-t-n-n');
                    $('#activityExpenseText').text(budget_details.activity_expense).addClass('e-n-t-n-n');
                    $('#contenjencyExpenseText').text(budget_details.contenjency).addClass('e-n-t-n-n');
                    $('#totalExpenseText').text(budget_details.totalExpense).addClass('e-n-t-n-n');
                    $('#activityExpense').val(budget_details.activity_expense);
                    $('#contenjencyExpense').val(budget_details.contenjency);
                    $('#totalExpense').val(budget_details.totalExpense);
                    $('#activityBudget').val(budget_details.details.total_budget);
                }
            })
        }
    </script>

    <script>
        $(document).ready(function () {
                let programId = $('#program').val();
                get_main_activity_by_budget_sub_head(programId);
        })
    </script>


    {{--date picker --}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });
        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);
    </script>

    {{--    On Submit button click--}}
    <script>
        $(document).on('click','#submitBtn',function (e) {
            if(validation()){
                e.preventDefault()
                let date = $('#date').val();
                let dateRoman = convertNepaliToEnglish(date);
                $('#date').val(dateRoman);
                $('#formSubmit').submit();
            }
        })
    </script>

    {{--    Validation--}}
    <script>
        function validation(){
            let flag = 1;
            let budgetSubHead = $('#program');
            if(!budgetSubHead.val()){
                if(budgetSubHead.siblings('p').length == 0){
                    budgetSubHead.after('<p style="color: red" class="validation-error">बजेट उपशिर्षक छान्नुहोस</p>')
                }
                budgetSubHead.focus();
                flag = 0;
            } else{
                budgetSubHead.siblings('p').remove();
            }

            let activity = $('#main_activity_id');
            if(!activity.val()){
                if(activity.siblings('p').length == 0){
                    activity.after('<p style="color: red" class="validation-error">कार्यक्रम छान्नुहोस</p>')
                }
                activity.focus();
                flag = 0;
            }else{
                activity.siblings('p').remove();
            }
            let chaumasikType = $('#chaumasik_type');
            if(!chaumasikType.val()){
                if(chaumasikType.siblings('p').length == 0){
                    chaumasikType.after('<p style="color: red" class="validation-error">चौमासिक प्रकारा छान्नुहोस</p>')
                }
                chaumasikType.focus();
                flag = 0;
            }else{
                chaumasikType.siblings('p').remove();
            }

            let progress = $('#progress');
            if(!progress.val()){
                if(progress.siblings('p').length == 0){
                    progress.after('<p style="color: red" class="validation-error">कार्यक्रम प्रोग्रेस लेख्नुहोस</p>')
                }
                progress.focus();
                flag = 0;
            }else{
                progress.siblings('p').remove();
            }

            let progressQuantity = $('#progress_quantity');
            if (!progressQuantity.val()) {
                if (progressQuantity.siblings('p').length == 0) {
                    progressQuantity.after('<p style="color: red" class="validation-error">कार्यक्रम प्रोग्रेस परिमाण लेख्नुहोस</p>')
                }
                progressQuantity.focus();
                flag = 0;
            } else {
                progressQuantity.siblings('p').remove();
            }
            return flag;

        }
    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            var charArray = input.split('');
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1';
                        break;
                    case '२':
                        engDate += '2';
                        break;
                    case '३':
                        engDate += '3';
                        break;
                    case '४':
                        engDate += '4';
                        break;
                    case '५':
                        engDate += '5';
                        break;
                    case '६':
                        engDate += '6';
                        break;
                    case '०':
                        engDate += '0';
                        break;
                    case '७':
                        engDate += '7';
                        break;
                    case '८':
                        engDate += '8';
                        break;
                    case '९':
                        engDate += '9';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
            });
            return engDate

        }
    </script>
@endsection
