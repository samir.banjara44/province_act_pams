@extends('frontend.layouts.app')

@section('styles')
    <style>
        #budgetStoreCreateFrom label {
            display: block !important;
        }

        th {
            color: black;
            font-size: 14px !important;
        }

        .form-group {

            position: relative;
        }

        .validation-error {
            position: absolute;
            color: red;
            right: 4px;
            bottom: -23px;
            font-size: 10px;
        }
    </style>
@endsection
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="font-size: 25px;">

                <small>धरौटी भौचर प्रविस्टी</small>
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="flash-message">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                    </div>
                    <form action="{{route('retention.voucher.details.store')}}" method="post"
                          id="voucherStoreCreateFrom">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-2 kalimati">
                                        <label>आर्थीक वर्ष</label>
                                        <select name="fiscal_year" class="form-control">
                                            <option value="{{$fiscalYear->id}}">{{$fiscalYear->year}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="date"> मिति:</label>
                                        <input type="text" class="form-control" name="date" id="date" value="" required
                                               style="height: 32px;">
                                        <input type="hidden" name="roman_date" id="roman_date">
                                        <input type="hidden" name="retention_voucher_id"
                                               value="{{$retentionVoucherId}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label>डेविट/क्रेडिट</label>
                                        <select name="drOrCr" class="form-control" id="DrOrCr" required
                                                style="height: 32px;">
                                            <option value="1">डेबिट</option>
                                            <option value="2">क्रेडिट</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>व्यहोरा</label>
                                        <select name="byahora" class="form-control" id="byahora" required
                                                style="height: 32px;">
                                            <option value="">.......</option>
                                            <option value="3">एकल कोष खाता</option>
                                            <option value="10">धरौटी</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>हिसाब नं.</label>
                                        <select name="hisab_number" class="form-control select2" id="hisab_number"
                                                required
                                                style="height: 32px;">
                                            <option>...................</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>बिबरण</label>
                                        <input type="text" class="form-control" name="details" id="details" required
                                               style="height: 32px;">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>धरौटी अभिलेख</label>
                                        <select class="form-control" name="retention_record" id="retention_record">
                                            <option value="">..............</option>
                                            @foreach($retentionRecords as $record)
                                                <option value="{{$record->id}}">{{$record->purpose}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>धरौटी प्रकार</label>
                                        <select name="retention_type" class="form-control" id="retention_type"
                                                style="height: 32px;">
                                            <option value="">..............</option>
                                            <option value="1">जमानत</option>
                                            <option value="2">न्यायिक</option>
                                            <option value="3">विविध</option>
                                            <option value="4">लिलाम</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label> धरौटी जम्मा गर्नेको प्रकार</label>
                                        <select name="party_type" class="form-control" id="party_type"
                                                style="height: 32px;">
                                            <option value="">..............</option>
                                            <option value="1">उपभोक्ता समिति</option>
                                            <option value="2">ठेकेदार</option>
                                            <option value="3">व्यक्तिगत</option>
                                            <option value="4">संस्थागत</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label> धरौटी जम्मा गर्ने</label>
                                        <select name="party" id="party" class="form-control"
                                                style="height: 32px;">
                                            <option value="" selected>..............</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>भौचर/नगदी रसिद नम्बर</label>
                                        <input type="number" name="bill_number" min="1" id="bill_number"
                                               class="form-control"
                                               required style="height: 32px;">
                                    </div>
                                    <div class="col-md-2">
                                        <label>रकम</label>
                                        <input type="number" name="amount" min="1" id="amount"
                                               class="form-control"
                                               required style="height: 32px;">
                                    </div>
                                    <div class="col-md-4 col-md-offset-8">
                                        <button type="button" class="btn btn-primary btn-block" id="voucher_submit"
                                                style="margin-top: 6px;">थप
                                        </button>
                                    </div>

                                </div>

                            </div>
                            <div class="col-md-3">
                                <table style="color: red;">
                                    <tr>
                                        <input type="hidden" name="voucher_details_id" id="voucher_details_id">
                                        <td style="inline-size: 377px;" class="voucher_number_show">भौचर नं.:
                                            <span id="voucher_number"></span></td>
                                        <td class="voucher_numbmer_td hidden">
                                            <span id="hidden_voucher_numbmer"></span>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>कुल आम्दानी : <span id="total_budget"></span></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>खर्च : <span id="total_expense"></span></td>
                                        <input type="hidden" id="hidden_expense">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>मौज्दात : <span id="remain_budget"></span></td>
                                        <input type="hidden" id="hidden_remain_budget">
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->

        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('retention.voucher.update',$retentionVoucherId)}}" method="post"
                          id="retentionVoucherUpdate">
                        {{csrf_field()}}
                        <table class="table" id="voucher_table" border="1">
                            <thead>
                            <tr style="background-color:  #dbdbdb;" class="kalimati">
                                <th>क्र.स.</th>
                                <th>डे|क्रे</th>
                                <th>धरौटीको प्रकार</th>
                                <th>धरौटी अभिलेख/प्रयोजन</th>
                                <th>व्यहोरा/हिसाब नं.</th>
                                <th>विवरण</th>
                                <th>धरौटी जम्मा गर्ने</th>
                                <th>बैंक भौचर/नगदी रसिद नम्बर</th>
                                <th>डेबिट रकम</th>
                                <th>क्रेडिट रकम</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($retentionVoucherDetails as $index=>$voucher)
                                <tr>
                                    <td>{{++$index}}</td>
                                    <td>
                                        @if($voucher->dr_or_cr == 1)
                                            डे.
                                        @else
                                            क्रे.
                                        @endif
                                    </td>
                                    <td>
                                        @if($voucher->retention_type==1)
                                            जमानत
                                        @elseif($voucher->retention_type==2)
                                            न्यायिक
                                        @elseif($voucher->retention_type==2)
                                            विविध
                                        @elseif($voucher->retention_type==2)
                                            लिलाम
                                        @endif
                                    </td>
                                    <td>
                                        @if($voucher->retention_record_name)
                                            {{$voucher->retention_record_name->purpose}}
                                        @endif
                                    </td>
                                    <td>
                                        @if($voucher->byahora ==3)
                                            एकल कोष
                                            खाता/ @if($voucher->hisab_number_name){{$voucher->hisab_number_name->expense_head_sirsak}}@endif
                                        @else
                                            धरौटी
                                            / @if($voucher->hisab_number_name){{$voucher->hisab_number_name->expense_head_sirsak}}@endif
                                        @endif
                                        {{--                                    {{$voucher->byahora}} | {{$voucher->hisab_number}}--}}
                                    </td>
                                    <td>{{$voucher->details}}</td>
                                    <td>

                                        @if($voucher->party_type_name){{$voucher->party_type_name->name}}
                                        /@endif  @if($voucher->advancePayment){{$voucher->advancePayment->name_nep}}@endif</td>
                                    <td class="kalimati" style="text-align: center">{{$voucher->bill_number}}</td>
                                    <td class="kalimati dr-amount" style="text-align: center">
                                        @if($voucher->dr_or_cr == 1)
                                            {{$voucher->amount}}
                                        @endif
                                    </td>
                                    <td class="kalimati cr-amount" style="text-align: center">
                                        @if($voucher->dr_or_cr == 2)
                                            {{$voucher->amount}}
                                        @endif
                                    </td>
                                    <input type="hidden" id="tsa_amount" name="tsa_amount" value="{{$voucher->amount}}">
                                    <td><a href="{{route('retention.voucher.edit',$voucher->id)}}">Edit</a> | <a href="#" id="retention_detail_delete" data-id="{{$voucher->id}}">Delete</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <table id="dr_cr_total_table" width="100%" border="1">
                            <tr style="background-color: #dbdbdb">
                                <td style="text-align: right">
                                    <span style="margin-right: 20px">जम्मा|</span>
                                </td>
                                <td id="dr_amount">0|</td>
                                <td id="cr_amount">0|</td>
                                <td style="color: red" id="amount_difference">0|</td>
                            </tr>
                        </table>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>कारोवारको संक्षिप्त व्यहोरा</label>
                                            <textarea class="form-control shortInfo" name="shortInfo" id="shortInfo"
                                                      style="margin: 0px; height: 35px; resize: none"></textarea>
                                        </div>
                                        <div class="col-md-8">
                                            <label>कारोवारको विस्तृत व्यहोरा</label>
                                            <textarea class="form-control detailsInfo" name="detailsInfo"
                                                      id="detailsInfo"
                                                      style="margin: 0px; height: 35px; resize: none "></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-offset-6">
                            <input type="submit" id="voucherSave" class="btn btn-primary btn-block" value="Save">
                        </div>
                    </form>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
{{--    Voucher Delete--}}
<script>
    $(document).ready(function () {
        $(document).on('click','#retention_detail_delete',function () {
        let voucherDetailsId = $('#retention_detail_delete').attr('data-id');
        let url = '{{route('retention.voucherDetails.delete',123)}}'
            url = url.replace(123,voucherDetailsId);
            swal({
                title: "Are you sure?",
                text: "Delete भए पछि Recovere हुदैन",
                icon: "warning",
                buttons: true,
                dangerMode: true,

            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url : url,
                        method : 'get',
                        success : function (res) {
                            if(res){
                                swal("Voucher has been deleted!", {
                                    icon: "success",
                                });
                                location.reload();
                            }
                        }
                    })
                }
            });
        })
    })
</script>

{{--    Short Narration--}}
    <script>
        $(document).ready(function () {
            let retentionVoucher = '{!! $voucherData !!}'
            let test = $.parseJSON(retentionVoucher);
            console.log(test.short_narration)
            $('#shortInfo').text(test.short_narration);
            $('#detailsInfo').text(test.long_narration);
        })
    </script>

    {{--Voucher Add--}}
    <script>
        $(document).on('click', '#voucher_submit', function (e) {
            if (validation()) {
                let date_nep_temp = $('#date').val();
                let date_nep = convertNepaliToEnglish(date_nep_temp);

                $('#roman_date').val(date_nep);
                let tsa_amount = $('#tsa_amount').val();
                $('#voucherStoreCreateFrom').submit();
            }
        })
    </script>
    {{--    Voucher Number--}}
    <script>
        $(document).ready(function () {

            let voucherEncode = '{!! $voucherData !!}';
            let voucher = ($.parseJSON(voucherEncode));
            let voucherNumber = voucher.voucher_number;
            $('#voucher_number').text(voucherNumber);
        })
    </script>

    {{-- get_expense_head_by_activity_id --}}
    <script>
        let get_expense_head_by_activity_id = function (main_activity_id) {
            let url = '{{route('get_expense_head_by_activity_id',123)}}';
            url = url.replace(123, main_activity_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let data = $.parseJSON(res);
                    // console.log("test",data[0]);
                    let options = '<option selected>.........................</option>';

                    options += '<option value="' + data[0].expense_head_by_id.id + '" selected>' + data[0].expense_head_by_id.expense_head_code + ' | ' + data[0].expense_head_by_id.expense_head_sirsak + '</option>';
                    var selected_option = $('#byahora option:selected');
                    if (selected_option.length == 0 || selected_option.val() == 1 || selected_option.val() == 4) {

                        $('#hisab_number').html(options);
                        $('#details').val(data[0].expense_head_by_id.expense_head_sirsak);
                        $('input#details').closest('div').find('p.validation-error').remove();

                    }
                }
            })
        }
    </script>

    {{--Dr  Or Cr change हुदा--}}
    <script>
        $(document).ready(function () {
            $('#DrOrCr').change(function () {
                $('#byahora').val("");
                $('#hisab_number').html("");
                $('#details').val("");
            })
        })
    </script>

    {{--व्यहोरा change हुदा--}}
    <script>
        $(document).ready(function () {
            $('#byahora').change(function () {
                let byahora = $('#byahora :selected').val();
                if (byahora == 3) {
                    $('#amount').focus();

                }
                get_hisab_number_by_byahora(byahora);
            })
        })
    </script>
    {{--    Cr Tsa amount set on amoun field--}}
    <script>
        $(document).ready(function () {
            $('#byahora').change(function () {
                let drOrCr = $('#DrOrCr').val();
                let byahora = $('#byahora').val();
                if (drOrCr == 2 && byahora == 3) {
                    let remain = drCrRemaining.toFixed(2) * (-1);
                    $('#amount').val(Math.abs(remain.toFixed(2)))
                }
            })
        })
    </script>

    {{--on change party_type --}}
    <script>
        $(document).ready(function () {
            $('#party_type').change(function () {
                let party_type_id = $('#party_type :selected').val();
                getPartyByPartyType(party_type_id);

            })
        });
    </script>

    {{--get  party name --}}
    <script>
        let getPartyByPartyType = function (party_type_id, party_name = '') {
            let url = '{{route('get_party_name_by_party_type_and_office', 123)}}';
            url = url.replace(123, party_type_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let option = '<option value="">....................</option>';
                    $.each($.parseJSON(res), function () {
                        if (party_name == this.name_nep) {
                            option += '<option value="' + this.id + '" selected>' + this.name_nep + '</option>'
                        } else {
                            option += '<option value="' + this.id + '">' + this.name_nep + '</option>'
                        }
                    });
                    $('#party').html(option);
                }
            })
        }
    </script>


    {{--Get हिसाब Number By व्यहोरा--}}
    <script>
        let get_hisab_number_by_byahora = function (byahora) {
            let ledger_type = byahora;
            let url = '{{route('get_expense_head__by_ledger_type',123)}}';
            url = url.replace(123, ledger_type);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let option = '<option value="">.........</option>';
                    let hisab_number = $.parseJSON(res);

                    if (res.length > 0) {
                        if (hisab_number.length > 1) {
                            $.each($.parseJSON(res), function () {
                                option += '<option value="' + this.id + '">' + this.expense_head_code + ' | ' + this.expense_head_sirsak + '</option>'
                            })
                        } else {
                            option += '<option value="' + hisab_number[0].id + '" selected>' + hisab_number[0].expense_head_sirsak + '</option>'
                        }
                        $('#hisab_number').html(option);
                        getDetailsByHisabNumber();

                    } else {
                        $('#hisab_number').html(option);
                    }
                }
            })
        }
    </script>

    {{--hisab number change huda--}}
    <script>
        let getDetailsByHisabNumber = function () {

            let bibran = $('#hisab_number option:selected').text();
            // console.log('bibara check', bibran);
            $('input#details').val(bibran);
            $('input#details').closest('div').find('p.validation-error').remove();
        };
        $(document).ready(function () {
            $('#hisab_number').change(function () {
                getDetailsByHisabNumber();
            })
        })
    </script>


    {{--checkRemain funcion--}}
    <script>
        let checkRemain = () => {
            let $hiddenRemainingBudget = $('#hidden_remain_budget');
            let amount = $('input#amount').val();

            let mainActivityId = $('#main_activity_name').val();
            let $existingDebitTds = $('#voucher_table tbody td.activity[data-id="' + mainActivityId + '"]');

            let existingDebitTotal = 0;
            $.each($existingDebitTds, function () {
                if (!$(this).closest('tr').hasClass(editingSN)) {
                    existingDebitTotal += parseFloat($(this).closest('tr').find('td.drAmount').text())
                }
            });

            let remain = $hiddenRemainingBudget.val();
            if (parseFloat(remain) >= parseFloat(amount)) {
                remain = parseFloat(remain) - parseFloat(amount);
                expense = parseFloat(amount) + existingDebitTotal;
                $('#total_expense').text(expense).addClass('e-n-t-n-n');
                $('#hidden_expense').val(expense);
                $('#hidden_remain_budget').val(remain);
                $('#remain_budget').text(remain).addClass('e-n-t-n-n');

                return true;
            } else {

                alert('मौज्दात भन्दा बढि भयो। बाकी:' + remain);
                $('#amount').focus();
                $('#amount').val(remain);
                return false;
            }
        }
    </script>


    {{-- Dr and Cr total नहुदा Save Button Disable हुने function --}}
    <script>
        let drCrRemaining = 0;
        $(document).ready(function () {
            getTotalDrAndCr();
        });

        let getTotalDrAndCr = () => {

            let trs = $('#voucher_table tbody').find('tr');
            let dr_amount = 0;
            let cr_amount = 0;
            $.each(trs, function () {

                let dr_temp = $(this).find('td.dr-amount').text();
                dr_temp = $.trim(dr_temp);
                if (dr_temp) {
                    dr_amount += parseFloat(dr_temp);
                }

                let cr_temp = $(this).find('td.cr-amount').text();
                cr_temp = $.trim(cr_temp);
                if (cr_temp) {
                    cr_amount += parseFloat(cr_temp);
                }
                drCrRemaining = parseFloat(cr_amount) - parseFloat(dr_amount);
            });
            if (dr_amount != cr_amount) {
                $('#voucherSave').attr('disabled', true);
            }

            $('#dr_amount').text(dr_amount);
            $('#cr_amount').text(cr_amount);
            let difference = parseFloat(dr_amount) - parseFloat(cr_amount);
            $('#amount_difference').text(difference);
        }

    </script>

    {{--short narration key up --}}
    <script>
        $(document).ready(function () {
            $('#shortInfo').keyup(function () {
                let shortNarration = $('#shortInfo').val();
                $('#detailsInfo').val(shortNarration);
            })
        })
    </script>

    <script>

    </script>


    {{--Date Picker --}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);
    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            // console.log(input);
            var charArray = input.split('');
            //console.log(charArray);
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1';
                        break;
                    case '२':
                        engDate += '2';
                        break;
                    case '३':
                        engDate += '3';
                        break;
                    case '४':
                        engDate += '4';
                        break;
                    case '५':
                        engDate += '5';
                        break;
                    case '६':
                        engDate += '6';
                        break;
                    case '०':
                        engDate += '0';
                        break;
                    case '७':
                        engDate += '7';
                        break;
                    case '८':
                        engDate += '8';
                        break;
                    case '९':
                        engDate += '9';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
                //console.log(engDate)
            });
            return engDate

        }
    </script>

    {{--Clear Voucher and Bhuktani field when  Add button click--}}
    <script>
        function clearVoucherField() {
            $('#hisab_number').val('');
            $('#details').val('');
            $('#party_type').val('');
            $('#party-name').val('');
            $('#amount').val('')
        }
    </script>

    {{--    Validation--}}
    <script>
        $('.form-control').change(function () {
            let val = $(this).val();
            if (val) {
                $(this).closest('div').find('p.validation-error').remove();
            }
        });

        function validation() {
            let flag = 1;
            let drOrCr = $('#DrOrCr').val();
            let byahora = $('#byahora').val();

            let amountId = $('#amount');
            if (!amountId.val()) {
                if (amountId.siblings('p').length == 0) {
                    amountId.after('<p class="validation-error">छान्नुहोस!</p>');
                    amountId.addClass("error");
                }
                amountId.focus();
                flag = 0;
            } else {
                amountId.siblings('p').remove()
            }

            let hisabNumber = $('#hisab_number');
            if (!hisabNumber.val()) {
                if (hisabNumber.siblings('p').length == 0) {
                    hisabNumber.after('<p class="validation-error">छान्नुहोस!</p>');
                    hisabNumber.addClass("error");
                }
                hisabNumber.focus();
                flag = 0;
            } else {
                hisabNumber.siblings('p').remove()
            }

            let details = $('#details');
            if (!details.val()) {
                if (details.siblings('p').length == 0) {
                    details.after('<p class="validation-error">छान्नुहोस!</p>');
                    details.addClass("error");
                }
                details.focus();
                flag = 0;
            } else {
                details.siblings('p').remove()
            }
            if(byahora != 3) {
                let retentionRecord = $('#retention_record');
                if (!retentionRecord.val()) {
                    if (retentionRecord.siblings('p').length == 0) {
                        retentionRecord.after('<p class="validation-error">छान्नुहोस!</p>');
                        retentionRecord.addClass("error");
                    }
                    retentionRecord.focus();
                    flag = 0;
                } else {
                    retentionRecord.siblings('p').remove()
                }

                let retentionType = $('#retention_type');
                if (!retentionType.val()) {
                    if (retentionType.siblings('p').length == 0) {
                        retentionType.after('<p class="validation-error">छान्नुहोस!</p>');
                        retentionType.addClass("error");
                    }
                    retentionType.focus();
                    flag = 0;
                } else {
                    retentionType.siblings('p').remove()
                }

                let partyType = $('#party_type');
                if (!partyType.val()) {
                    if (partyType.siblings('p').length == 0) {
                        partyType.after('<p class="validation-error">छान्नुहोस!</p>');
                        partyType.addClass("error");
                    }
                    partyType.focus();
                    flag = 0;
                } else {
                    partyType.siblings('p').remove()
                }

                let party = $('#party');
                if (!party.val()) {
                    if (party.siblings('p').length == 0) {
                        party.after('<p class="validation-error">छान्नुहोस!</p>');
                        party.addClass("error");
                    }
                    party.focus();
                    flag = 0;
                } else {
                    party.siblings('p').remove()
                }
            }
            let amount = $('#amount');
            if (!amount.val()) {
                if (amount.siblings('p').length == 0) {
                    amount.after('<p class="validation-error">छान्नुहोस!</p>');
                    amount.addClass("error");
                }
                amount.focus();
                flag = 0;
            } else {
                amount.siblings('p').remove()
            }
        return flag;
        }
    </script>


    {{--   On Save Button Click --}}
    <script>
        $(document).on('click', '#voucherSave', function (e) {
            e.preventDefault();
            if (saveValidation()) {
                $('#retentionVoucherUpdate').submit();
            }
        });

        function saveValidation() {

            let flag = 1;
            let short_info = $('#shortInfo');
            if (!short_info.val()) {
                if (short_info.siblings('p').length == 0) {
                    short_info.after('<p class="validation-error">संक्षिप्त व्यहोरा लेख्नुहोस!</p>');
                    short_info.addClass("error");
                }
                short_info.focus();
                flag = 0;
            } else {
                short_info.siblings('p').remove()
            }
            let details_info = $('#detailsInfo');
            if (!details_info.val()) {
                if (details_info.siblings('p').length == 0) {
                    details_info.after('<p class="validation-error">विस्तृत व्यहोरा लेख्नुहोस!</p>');
                    details_info.addClass("error");
                }
                details_info.focus();
                flag = 0;
            } else {
                details_info.siblings('p').remove()
            }

            return flag;

        }
    </script>

@endsection
