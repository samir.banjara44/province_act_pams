<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
{{--<link href="{{ asset('css/nepali-datepicker.css') }}" rel="stylesheet">--}}
<link rel="stylesheet" href="{{asset('css/styles.css')}}">

<style>
    table {
        border-collapse: collapse;
    }

    body {

        overflow-x: hidden;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="border-collapse: collapse; font-size:13px">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
                    सभा @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6"
                    style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    @if(Auth::user()->office->department)
                        {{Auth::user()->office->department->name}}
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}, , {{Auth::user()->office->district->name}}</td>
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>धरौटी भुक्तानी आदेश</b>
                    </div>
                    <div style="float: right; margin-top: -20px">
                        म.ले.प.फा.नं. २०४
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                    <br>

                </td>
            </tr>
            <tr>
                <td class="kalimati" style="text-align: right">आर्थीक वर्ष :
                    <span class="e-n-t-n-n">{{$fiscalYear->year}}</span><br>
                    मिति :
                    {{$bhuktani['date_nepali']}}
                </td>
            </tr>
            <tr>
                <td>श्री प्रदेश लेखा नियन्त्रक कार्यालय,</td>
            </tr>
            <tr>
                <td>{{Auth::user()->office->province->name}},{{Auth::user()->office->district->name}}</td>
            </tr>

            <tr>
                <td style="padding-top: 10px;">देहाय बमोजिमको रकम भुक्तानी/ निकासा को लागी अनुरोध छ ।</td>
            </tr>
        </table>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table" id="malepa-five-table" width="100%" border="1"
                       style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th rowspan="4">क्र स</th>
                        <th rowspan="4">गो भौ.न.</th>
                        <th colspan="2" rowspan="1" style="text-align: center">खर्च उपशिर्षक</th>
                        <th colspan="4">स्रोत विवरण</th>
                        <th colspan="2" rowspan="1">जम्मा रकम</th>
                        <th rowspan="4">भुक्तानि पाउने कोड</th>
                        <th rowspan="4">भुक्तानि पाउने नाम</th>
                        <th rowspan="4">स्थायी लेखा(Pan)न.</th>
                        <th colspan="4" rowspan="1">भुक्तानी माध्यम</th>
                        <th rowspan="4">प्रतिबध्दता न.</th>
                        <th rowspan="4">कैफियत</th>
                    </tr>
                    <tr>
                        <td rowspan="3" style="text-align: center">सकेत न.</td>
                        <td rowspan="3" style="text-align: center">विवरण</td>

                        <td rowspan="3" style="text-align: center">स्रोतको तह</td>
                        <td rowspan="3" style="text-align: center">स्रोत बेहोर्ने सस्था</td>
                        <td rowspan="3" style="text-align: center">स्रोतको किसिम</td>
                        <td rowspan="3" style="text-align: center">निकासा विधि</td>

                        <td rowspan="3" style="text-align: center">अंकमा</td>
                        <td rowspan="3" style="text-align: center">अक्षरमा</td>

                        <td rowspan="1" style="text-align: center">चेक</td>
                        <td rowspan="1" colspan="3" style="text-align: center">बैंक ट्रान्सफर</td>
                    </tr>
                    <tr>
                        <td rowspan="2" style="text-align: center">चेकको किसिम</td>
                        <td rowspan="2" style="text-align: center">बैंकको नाम</td>
                        <td rowspan="2" style="text-align: center">शाखा</td>
                        <td rowspan="2" style="text-align: center">खाता न.</td>
                    </tr>
                    </thead>
                    <tbody>
                    @if($bhuktani->count())
                        @foreach($bhuktani->retentionPreBhuktani as $index =>$data)
                            <tr style="background-color: white;">
                                <td style="text-align: center"><span class="e-n-t-n-n">{{++$index}}</span></td>
                                <td style="text-align: center">
                                    <span class="e-n-t-n-n">{{$data->voucher->voucher_number}}</span></td>
                                <td style="text-align: center">
                                    {{$data->expense_head->expense_head_code}}
                                </td>
                                <td style="text-align: left">
                                    {{$data->expense_head->expense_head_sirsak}}
                                </td>
                                <td>
                                    -
                                </td>
                                <td>
                                    -
                                </td>
                                <td>
                                    -
                                <td>
                                    नगद
                                </td>
                                <td style="text-align: right">
                                    <span class="e-n-t-n-n">@moneyFormat($data['amount'])</span>
                                </td>
                                <td>
                                    {{$data->get_amount_in_word()}} मात्र
                                </td>
                                <td style="text-align: center">
                                    <span class="kalimati">
                                        @if($data->getParty and $data->getParty->payee_code){{$data->getParty->payee_code}} @endif</span>
                                </td>
                                <td>
                                    {{$data->getParty->name_nep}}
                                </td>
                                <td style="text-align: center">
                                    -
                                </td>
                                <td>
                                    ए.सि. पेयी
                                </td>
                                <td>

                                </td>

                                <td>
                                    @if($data->getParty)
                                        {{$data->getParty->bank_address}}
                                    @endif
                                </td>

                                <td>
                                    <span class="e-n-t-n-n">{{$data->getParty->party_khata_number}}</span>
                                </td>
                                <td>
                                    -
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                @endforeach
                                @endif
                                <td colspan="8" style="text-align: right">जम्मा</td>
                                <td style="text-align: right">
                                    <span class="e-n-t-n-n">@moneyFormat($bhuktani['amount'])</span>
                                </td>
                                <td class="">
                                    {{$bhuktani_amount_word}} मात्र
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                    </tbody>

                </table>
                <br>
                <div style="padding: 0 40px;width: 89%;">
                    <table border="1" style="width: 100%">
                        <tr>
                            {{--                            <td class="" colspan="8" rowspan="2" style="font-size: 13px;">--}}
                            {{--                                भूक्तानी आदेशको कुल रकम अक्षरमा- <b>{{$bhuktani_amount_word}}</b>--}}
                            {{--                            </td>--}}

                        </tr>
                    </table>
                </div>
                <br>
                <br>
                <div style="width: 132%;padding: 0px 223px;">
                    <table class="" width="100%">
                        <tr>
                            <td>___________________________</td>
                            <td>___________________________</td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px;">लेखा प्रमुख</td>
                            <td style="font-size: 14px;">कार्यालय प्रमुख</td>
                        </tr>
                    </table>
                </div>

                <p class="kalimati">निकासा दिने कार्यालय प्रयोजनको लागि</p>

                <div style="width: 80%;padding: -1px 210px;">
                    <table border="1" width="30%" align="left" style="font-size: 12px; margin-right: 20px">
                        <thead>
                        <tr style="background-color: #dbdbdb">
                            <th>दर्ता सम्बन्धि विवरण</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>दर्ता नं.:</td>
                        </tr>
                        <tr>
                            <td>टोकन नं.:</td>
                        </tr>
                        <tr>
                            <td>दर्ता समय:</td>
                        </tr>
                        <tr>
                            <td>दर्ता मिति:</td>
                        </tr>
                        <tr>
                            <td>दर्ता गर्ने:</td>
                        </tr>
                        </tbody>
                    </table>
                    &nbsp
                    <table class="" border="1" width="30%" align="left" style="font-size: 12px;margin-right: 20px">
                        <thead>
                        <tr style="background-color: #dbdbdb">
                            <th> निकासा/भुक्तानी सम्बन्धि विवरण</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>निकासा/भुक्तानी विधि :</td>
                        </tr>
                        <tr>
                            <td>चेक न.:</td>
                        </tr>
                        <tr>
                            <td>बैक्ङ टृान्सफर संकेत:</td>

                        </tr>
                        <tr>
                            <td rowspan="2">तयार गर्ने:</td>

                        </tr>
                        </tbody>
                    </table>
                    <table border="1" align="left" width="30%" style="font-size: 12px">
                        <thead>
                        <tr style="background-color: #dbdbdb">
                            <th>विद्धुतीय कारोबारको हकमा</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>प्रिन्ट मिति:</td>
                        </tr>
                        <tr>
                            <td>प्रिन्ट गर्नेको नाम:</td>
                        </tr>
                        <tr>
                            <td>प्रिन्ट गरेको पटक:</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                {{--                <p class="kalimati">विद्धुतीय कारोबारको हकमा</p>--}}
                {{--                <div class="kalimati" style="display: flex;flex-direction: row;flex-wrap: nowrap;justify-content: space-between;margin-right: 25%;">--}}
                {{--                    <p style="">प्रिन्ट मिति</p>--}}
                {{--                    <p style="">प्रिन्ट गर्नेको नाम</p>--}}
                {{--                    <p style="">प्रिन्ट गरेको पटक</p>--}}
                {{--                </div>--}}
                {{--                <table width="99%" style="font-size: 13px">--}}
                {{--                    <tr>--}}
                {{--                        <br>--}}
                {{--                        <br>--}}
                {{--                        <br>--}}
                {{--                        <br>--}}
                {{--                    </tr>--}}
                {{--                    <tr>--}}
                {{--                        <td style="padding-left: 52px;">तयार गर्ने :--}}
                {{--                            @if($voucher->karmachari_prepare_by and $voucher->karmachari_prepare_by->name_nepali)--}}
                {{--                                {{$voucher->karmachari_prepare_by->name_nepali}}--}}
                {{--                            @endif--}}
                {{--                        </td>--}}
                {{--                        <td>पेश गर्ने :--}}
                {{--                            @if($voucher->karmachari_submit_by and $voucher->karmachari_submit_by->name_nepali)--}}
                {{--                                {{$voucher->karmachari_submit_by->name_nepali}}--}}
                {{--                            @endif--}}
                {{--                        </td>--}}
                {{--                        <td>सदर गर्ने :--}}
                {{--                            @if($voucher->karmachari_approved_by and $voucher->karmachari_approved_by->name_nepali)--}}
                {{--                                {{$voucher->karmachari_approved_by->name_nepali}}</td>--}}
                {{--                        @endif--}}
                {{--                    </tr>--}}
                {{--                    <tr>--}}
                {{--                        <td style="padding-left: 52px;">पद :--}}
                {{--                            @if($voucher->karmachari_prepare_by and $voucher->karmachari_prepare_by->get_pad)--}}
                {{--                                {{$voucher->karmachari_prepare_by->get_pad->name}}--}}
                {{--                            @endif--}}
                {{--                        </td>--}}
                {{--                        <td>पद :--}}
                {{--                            @if($voucher->karmachari_submit_by and $voucher->karmachari_submit_by->get_pad)--}}
                {{--                                {{$voucher->karmachari_submit_by->get_pad->name}}--}}
                {{--                            @endif--}}
                {{--                        </td>--}}
                {{--                        <td>पद :--}}
                {{--                            @if($voucher->karmachari_approved_by and $voucher->karmachari_approved_by->get_pad)--}}
                {{--                                {{$voucher->karmachari_approved_by->get_pad->name}}--}}
                {{--                            @endif--}}
                {{--                        </td>--}}
                {{--                    </tr>--}}
                {{--                    <tr>--}}
                {{--                        <td style="padding-left: 52px;">मिति : {{$bhuktani->date_nepali}}</td>--}}
                {{--                        <td>मिति : {{$bhuktani->date_nepali}}</td>--}}
                {{--                        <td>मिति : {{$bhuktani->date_nepali}}</td>--}}
                {{--                    </tr>--}}

                {{--                </table>--}}
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '/')

                    nepaliNo += "/";
            }
        });
        console.log(nepaliNo);
        return nepaliNo;
    };

    // let change_all_to_nepali_number = function(){
    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });
    // }


</script>

<script>
    $(document).ready(function () {

        let month = $('span.month').text();
        // alert(month);

    })
</script>

