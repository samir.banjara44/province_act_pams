<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\This;

class SourceType extends Model
{
    protected $table = 'source_types';
    protected $guarded = [];

    public function sourceLevel()
    {
        return $this->hasMany('App\Models\BudgetSourceLevel');
    }

    public function source()
    {
        return $this->hasMany('App\Models\Source');
    }

    public function mofs()
    {
        return $this->hasMany('App\Models\Mof');
    }

    public function ministries()
    {
        return $this->hasMany('App\Models\Ministries');
    }

    public function departments()
    {
        return $this->hasMany('App\Models\Department');
    }

    public function offices()
    {
        return $this->hasMany('App\Models\Office');
    }

    public function users()
    {
        return $this->hasMany('App\Users');
    }

    public function budget()
    {

        return $this->hasMany('App\Models\Budget', 'source_type', 'id');
    }

    public function getSource($officeId,$fiscalYear ,$budgetSubHeadId,$sourceType)
    {
        return $sources = Source::join('budget', 'sources.id', '=', 'budget.source')
            ->select(['sources.id', 'sources.name'])
            ->where('budget.office_id', $officeId)
            ->where('budget.fiscal_year', $fiscalYear)
            ->where('budget.budget_sub_head', $budgetSubHeadId)
            ->where('budget.source_type', $sourceType)
            ->groupBy('budget.source')
            ->get();
    }

    public function getExpenseHead($officeId, $fiscalYear,$budgetSubHeadId, $source)
    {
        return $budgetByExpenseHead = BudgetDetails::where('office_id', $officeId)
            ->where('fiscal_year', $fiscalYear)
            ->where('budget_sub_head_id', $budgetSubHeadId)
            ->where('source_type', $source)
            ->groupBy('expense_head_id')
            ->orderBy('expense_head_id')
            ->get();
    }

    public function getInitialBudget($officeId, $fiscal_year,$budgetSubHeadId, $expneseHeadId, $sourceTypeId)
    {
        $expenseHead = ExpenseHead::findorfail($expneseHeadId);
        return $addedBudget = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->where('budget_details.source_type',$sourceTypeId)
            ->where('activity_report.budget_sub_head_id', $budgetSubHeadId)
            ->where('activity_report_details.fiscal_year', $fiscal_year)
            ->where('activity_report_details.expense_head', $expenseHead->expense_head_code)
            ->where('activity_report_details.akhtiyari_type', '=', 1)
            ->sum('activity_report_details.budget');
    }

    public function getAddBudget($officeId,$fiscal_year, $budgetSubHeadId, $expneseHeadId, $sourceTypeId)
    {
        $expenseHead = ExpenseHead::findorfail($expneseHeadId);
        return $addedBudget = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->where('budget_details.source_type',$sourceTypeId)
            ->where('activity_report.budget_sub_head_id', $budgetSubHeadId)
            ->where('activity_report_details.fiscal_year', $fiscal_year)
            ->where('activity_report_details.expense_head', $expenseHead->expense_head_code)
            ->where('activity_report_details.akhtiyari_type', '=', 3)
            ->sum('activity_report_details.budget');

    }

    public function getReduceBudget($officeId, $fiscal_year,$budgetSubHeadId, $expneseHeadId, $sourceTypeId)
    {
        $expenseHead = ExpenseHead::findorfail($expneseHeadId);
        return $addedBudget = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->where('budget_details.source_type',$sourceTypeId)
            ->where('activity_report.budget_sub_head_id', $budgetSubHeadId)
            ->where('activity_report_details.fiscal_year', $fiscal_year)
            ->where('activity_report_details.expense_head', $expenseHead->expense_head_code)
            ->where('activity_report_details.akhtiyari_type', '=', 4)
            ->sum('activity_report_details.budget');
    }

    public function getFinalBudget($officeId,$fiscal_year, $budgetSubHeadId, $expneseHeadId, $sourceTypeId)
    {
        return $initialBudget = Budget::
        where('office_id', $officeId)
            ->where('fiscal_year', $fiscal_year)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->where('expense_head_id', $expneseHeadId)
            ->where('source_type', $sourceTypeId)
            ->sum('total_budget');
    }

    public function getTotalExpense($officeId,$fiscal_year, $budgetSubHeadId, $expneseHeadId, $sourceTypeId)
    {
        $expenseHead = ExpenseHead::where('id', $expneseHeadId)->first();
        return $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->where('vouchers.status', '=', 1)
            ->where('activity_report.budget_sub_head_id', $budgetSubHeadId)
            ->where('activity_report_details.fiscal_year', $fiscal_year)
            ->where('activity_report_details.expense_head', $expenseHead->expense_head_code)
            ->where('budget_details.source_type', $sourceTypeId)
            ->sum('activity_report_details.expense');
    }


    public function remainBudget($officeId,$fiscal_year, $budgetSubHeadId, $expneseHeadId, $sourceTypeId)
    {

        $totalFianlBudget = $this->getFinalBudget($officeId, $fiscal_year, $budgetSubHeadId, $expneseHeadId, $sourceTypeId);
        $totalExpense = $this->getTotalExpense($officeId,$fiscal_year, $budgetSubHeadId, $expneseHeadId, $sourceTypeId);
        return $remainBudget = (float)$totalFianlBudget - (float)$totalExpense;
    }

    public function getTotalInitialBudget($officeId,$fiscal_year, $budgetSubHeadId, $sourceTypeId)
    {
        return $totalInitialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $officeId)
            ->where('akhtiyari.akhtiyari_type', '=',1)
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('budget.budget_sub_head', $budgetSubHeadId)
            ->where('budget.source_type', $sourceTypeId)
            ->sum('budget.total_budget');
    }

    public function getTotalAddBudget($officeId,$fiscal_year, $budgetSubHeadId, $sourceTypeId)
    {
        return $totalInitialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $officeId)
            ->where('akhtiyari.akhtiyari_type', '=',2)
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('budget.budget_sub_head', $budgetSubHeadId)
            ->where('budget.source_type', $sourceTypeId)
            ->sum('budget.total_budget');
    }

    public function getTotalReduceBudget($officeId,$fiscal_year, $budgetSubHeadId, $sourceTypeId)
    {

        return $totalInitialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $officeId)
            ->where('akhtiyari.akhtiyari_type', '=',3)
            ->where('budget.fiscal_year', $fiscal_year)
            ->where('budget.budget_sub_head', $budgetSubHeadId)
            ->where('budget.source_type', $sourceTypeId)
            ->sum('budget.total_budget');
    }

    public function getTotalFinalBudget($officeId,$fiscal_year, $budgetSubHeadId, $sourceTypeId)
    {
        return $totalInitialBudget = Budget::where('office_id', $officeId)
            ->where('fiscal_year', $fiscal_year)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->where('source_type', $sourceTypeId)
            ->sum('total_budget');

    }

    public function getGranTotalExpense($officeId,$fiscal_year, $budgetSubHeadId, $sourceTypeId)
    {
        return $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->where('vouchers.status', '=', 1)
            ->where('budget_details.source_type', $sourceTypeId)
            ->where('activity_report.budget_sub_head_id', $budgetSubHeadId)
            ->where('activity_report_details.fiscal_year',$fiscal_year)
            ->sum('activity_report_details.expense');
    }

    public function totalRemainBudget($officeId,$fiscal_year, $budgetSubHeadId, $sourceTypeId)
    {

        $granTotalBudget = $this->getTotalFinalBudget($officeId,$fiscal_year, $budgetSubHeadId, $sourceTypeId);
        $granTotalExp = $this->getGranTotalExpense($officeId,$fiscal_year, $budgetSubHeadId, $sourceTypeId);
        return $granTotalRemain = (float)$granTotalBudget - (float)$granTotalExp;
    }


}
