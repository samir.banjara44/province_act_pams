<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Branch;
use App\Repositories\BranchRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BranchController extends Controller
{
    private $branchRepo;

    public function __construct(BranchRepository $branchRepo)
    {
        $this->middleware('auth');
        $this->branchRepo = $branchRepo;
    }

    public function branchIndex()
    {
        $branchs = $this->branchRepo->get_by_office_id();
        return view('frontend.branch.index', compact('branchs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function branchCreate()
    {
        return view('frontend.branch.create');
    }

    
    public function branchStore(Request $request)
    {

        try {
            $branch = $this->branchRepo->create($request->all());
            $requestName = $request->name;
            $message = "Created " . $requestName . " Successfully";
            return redirect(route('branch'))->with('success', $message);
        } catch (\Exception $e) {
            $message = "Can not add to table. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }
    }

    public function branchEdit($id)
    {
        $branch = $this->branchRepo->get_by_id($id);
        return view('frontend.branch.edit', compact('branch'));
    }

    public function branchUpdate(Request $request, $id)
    {
        try {
            $branch = $this->branchRepo->update($request->all(), $id);
            $requestName = $request->name;
            $message = "Created " . $requestName . " Successfully";
            return redirect(route('branch'))->with('success', $message);
        } catch (\Exception $e) {
            $message = "Can not update. Please fill all the required fields first before Submitting";
            return redirect()->back()->with('error', $message);
        }
    }

    public function branchDelete($id)
    {

        $branch = $this->branchRepo->get_by_id($id);
        if($branch){
            $branch->delete();
            return json_encode(true);
        } else {
            return json_encode(false);
        }
    }
}
