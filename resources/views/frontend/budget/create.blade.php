@extends('frontend.layouts.app')


@section('styles')
    <style>
        #budgetStoreCreateFrom label {
            display: block !important;
        }

        .form-group {

            position: relative;
        }

        th {
            color: white;
            font-size: 12px;
        }

        tbody tr:hover {
            background-color: #e1e1e1 !important;

        }

        .validation-error {
            position: absolute;
            color: red;
            right: 4px;
            bottom: -23px;
            font-size: 10px;
        }
    </style>
@endsection
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="row">
                <div class="col-md-12">
                    <h1><i class="fas fa-briefcase"></i> बजेट प्रविस्टी </h1>

                    <button type="button" class="btn btn-primary btn-sm btn-show-form" id="showAddForm"><i
                                class="fas fa-plus"></i> नयाँ थप गर्ने
                    </button>
                </div>
            </div>
        </section>

        <!-- FORM section start -->
        <section class="formSection displayNone" id="showThisForm">
            <div class="row justify-content-md-center">
                <div class="col-md-10 form-bg col-md-offset-1">
                    <button type="button" class="btn btn-danger btn-sm btn-show-form displayNone" id="closeAddForm"><i
                                class="fas fa-times"></i> फर्म रद्ध गर्ने
                    </button>
                    <div class="form-title">बजेट विवरण प्रविष्टी गर्ने</div>

                    <form action="{{route('budget.store')}}" method="post" id="budgetStoreCreateFrom">
                        {{csrf_field()}}

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="fiscal_year"> आर्थिक वर्ष:</label>
                                            <select id="fiscal_year" class="form-control" name="fiscal_year" required>
                                                @foreach($fiscalYears as $fy)
                                                    <option value="{{$fy->id}}"
                                                            @if($fy->id == $fiscalYear->id) selected @endif>{{$fy->year}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-5">
                                            <label for="expense_center">खर्च केन्द्र</label>
                                            <select id="expense_center" class="form-control" name="expense_center"
                                                    required>
                                                <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}
                                                </option>
                                            </select>
                                        </div>

                                        <div class="col-md-5">
                                            <label for="budget_sub_head">बजेट उपशिर्षक</label>
                                            <select name="budget_sub_head" class="form-control" id="budget_sub_head"
                                                    required>
                                                <option value="">................</option>
                                                @foreach($programs as $program)
                                                    <option value="{{$program->id}}" @if($program->id ==
                                                Session::get('budget_sub_head')) selected
                                                            @endif>{{$program->program_code}}
                                                        |{{$program->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <label>
                                        अख्तियारी :<span id="totalAkhtiyari" style="color: red"></span>
                                    </label>
                                    <input type="hidden" id="totalAkhtiyariHiden">

                                    <label>
                                        विनियोजन बजेट :<span id="budget_sub_head_total_budget"
                                                             style="color: red"></span>
                                    </label>
                                    <input type="hidden" id="totalBiniyojanHiden">

                                    <label>
                                        बाकिँ बजेट :<span id="remain_budget" style="color: red"></span>
                                    </label>
                                    <input type="hidden" id="remain">
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-4">
                                    <label>अख्तियारि</label>
                                    <select class="form-control" name="akhtiyari" id="akhtiyari">
                                        <option value=""></option>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label for="main_activity">कार्यक्रम आयोजनाको नाम</label>
                                    <input type="text" class="form-control" id="main_activity"
                                           placeholder="कार्यक्रमको नाम"
                                           name="main_activity" required>
                                </div>

                                <div class="col-md-4">
                                    <label for="expense_head">खर्च शिर्षक</label>
                                    <select name="expense_head" class="form-control select2" id="expense_head" required>
                                        <option value="">.....................</option>
                                        @foreach($expenseHeads as $expenseHead)
                                            <option value="{{$expenseHead->id}}">
                                                {{$expenseHead->expense_head_sirsak.' '.$expenseHead->expense_head_code}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-2">
                                    <label for="unit">एकाइ</label>
                                    <select name="unit" class="form-control select2" required>
                                        <option value="1">N/A</option>
                                        <option value="2">जना</option>
                                        <option value="3">गोटा</option>
                                        <option value="4">पटक</option>
                                        <option value="5">सङ्ख्या</option>
                                        <option value="6">टोलि</option>
                                        <option value="7">परिवार</option>
                                        <option value="8">थान</option>
                                        <option value="9">सेट</option>
                                        <option value="10">वटा</option>
                                        <option value="11">महिना</option>
                                        <option value="12">शिक्षक</option>
                                        <option value="13">विषय</option>
                                        <option value="14">लाईन</option>
                                        <option value="15">दिन</option>
                                        <option value="16">जिन्ला</option>
                                        <option value="17">क्षेत्र</option>
                                        <option value="18">कोठा</option>
                                        <option value="19">कक्षा</option>
                                        <option value="20">मिटर</option>
                                        <option value="21">लिटर</option>
                                        <option value="22">ब.मिटर</option>
                                        <option value="23">मेट्िक टन</option>
                                        <option value="24">घन मिटर</option>
                                        <option value="25">रोपनी</option>
                                        <option value="26">हेक्टर</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="total_budget">कुल जम्मा बजेट</label>
                                    <input type="number" class="form-control" id="total_budget" placeholder="कुल जम्मा"
                                           name="total_budget">
                                </div>
                                <div class="col-md-2">
                                    <label for="total_unit">कुल परिमाण</label>
                                    <input type="number" class="form-control" id="total_unit" placeholder=""
                                           name="total_unit" min="0">
                                </div>
                                <div class="col-md-2">
                                    <label for="first_quarter">प्रथम चौमासिक एकाई</label>
                                    <input type="number" class="form-control Quarter" id="first_quarter" placeholder=""
                                           name="first_quarter">
                                </div>
                                <div class="col-md-2">
                                    <label for="second_quarter">दोस्रो चौमासिक एकाई</label>
                                    <input type="number" class="form-control Quarter" id="second_quarter" placeholder=""
                                           name="second_quarter">
                                </div>
                                <div class="col-md-2">
                                    <label for="third_quarter">तेस्रो चौमासिक एकाई</label>
                                    <input type="number" class="form-control" id="third_quarter" placeholder=""
                                           name="third_quarter">
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-2">
                                    <label for="first_total_budget">प्रथम चौमासिक बजेट</label>
                                    <span class="firstBhar" id="firstBhar"
                                          style="position: absolute; color: red; font-size: 11px; right: 25px; top: 0;">भार</span>

                                    <input type="number" class="form-control" value="0" id="first_total_budget"
                                           placeholder="" name="first_total_budget">
                                    <input type="hidden" name="firstChaumasikBharHidden" id="firstChaumasikBharHidden">
                                </div>

                                <div class="col-md-2">
                                    <label for="second_total_budget">दोस्रो चौमासिक बजेट</label>
                                    <span class="secondBhar" id="secondBhar"
                                          style="position: absolute;color: red;   font-size: 11px; right: 25px;  top: 0;">भार</span>
                                    <input type="number" value="0" class="form-control" id="second_total_budget"
                                           placeholder="" name="second_total_budget">
                                    <input type="hidden" name="secondChaumasikBharHidden"
                                           id="secondChaumasikBharHidden">
                                </div>

                                <div class="col-md-2">
                                    <label for="third_total_budget">तेस्रो चौमासिक बजेट</label>
                                    <span class="thirdBhar" id="thirdBhar"
                                          style="position: absolute; color: red; font-size: 11px; right: 25px;  top: 0;">भार</span>
                                    <input type="number" class="form-control" id="third_total_budget" placeholder=""
                                           name="third_total_budget" value="0">
                                    <input type="hidden" name="thirdChaumasikBharHidden" id="thirdChaumasikBharHidden">
                                </div>
                                <div class="col-md-2">
                                    <label for="">कन्टेन्जेनसी रकम % </label>
                                    <input type="checkbox" name="contenjency_check_box" id="contenjency_check_box">
                                    <i class="fas fa-futbol"></i>

                                    <input type="hidden" name="hidden_contenjency_percent" class="form-control"
                                           id="hidden_contenjency_percent">
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-2">
                                    <label for="source_type">स्रोत प्रकार</label>
                                    <select name="source_type" class="form-control" id="source_type" required>
                                        <option value="">...............</option>
                                        @foreach($sources as $source)
                                            <option value="{{$source->id}}">{{$source->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="source_level">बजेट स्रोत तह</label>
                                    <select name="source_level" class="form-control select2" id="source_level" required>
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="source">स्रोत</label>
                                    <select name="source" class="form-control select2" id="source" required>
                                        <option value="">-----</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="medium">प्राप्ति विधि</label>
                                    <select name="medium" class="form-control medium" id="medium" required>
                                        @foreach($mediums as $medium)
                                            <option value="{{$medium->id}}">{{$medium->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            {{--                        <div class="row">--}}

                            {{--                            <div class="col-md-2">--}}
                            {{--                                <label for="medium">PROJECT_NDESC</label>--}}
                            {{--                                <input type="text" class="form-control" name="PROJECT_NDESC" id="PROJECT_NDESC">--}}
                            {{--                            </div>--}}


                            {{--                            <div class="col-md-2">--}}
                            {{--                                <label for="medium">SUB_PROJECT_NDESC (Office)</label>--}}
                            {{--                                <input type="text" class="form-control" name="PROJECT_NDESC" id="PROJECT_NDESC">--}}
                            {{--                            </div>--}}

                            {{--                            <div class="col-md-2">--}}
                            {{--                                <label for="medium">COMPONENT_CODE</label>--}}
                            {{--                                <input type="text" class="form-control" name="PROJECT_NDESC" id="PROJECT_NDESC">--}}
                            {{--                            </div>--}}

                            {{--                            <div class="col-md-2">--}}
                            {{--                                <label for="medium">COMPONENT_NDESC</label>--}}
                            {{--                                <input type="text" class="form-control" name="PROJECT_NDESC" id="PROJECT_NDESC">--}}
                            {{--                            </div>--}}

                            {{--                            <div class="col-md-2">--}}
                            {{--                                <label for="medium">ACTIVITY_CODE</label>--}}
                            {{--                                <input type="text" class="form-control" name="PROJECT_NDESC" id="PROJECT_NDESC">--}}
                            {{--                            </div>--}}

                            <div class="col-md-2">
                                <label for="">&nbsp;</label>
                                <button type="submit" id="submitbtn" class="btn btn-primary"><i class="fas fa-save"></i>
                                    Save
                                </button>
                                <a type="button" class="btn btn-primary" href="{{route('budget.create')}}">
                                    Clear </a>
                            </div>

                        </div>
                        {{--                    </div>--}}
                    </form>

                </div>
            </div>
        </section>

        <div class="col-md-12 mb-20">
            <div class="row" style="padding: 0px 128px">
                <table class="table" id="budget-table" border="1"
                       style="background-color: #236286; margin-bottom: 25px;">
                    <thead>
                    <th>सि.न.</th>
                    <th>खर्च शिर्षक</th>
                    <th>विवरण</th>
                    <th>कार्यक्रम</th>
                    <th>रकम</th>
                    </thead>
                    <tbody>
                    <tr></tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <div class="modal fade" id="general_activity_modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <label for="model_area">क्षेत्र</label>
                    <input type="text" name="model_area" id="model_area" readonly>
                </div>
                <div class="modal-body">
                    <label for="model_sub_area">उप क्षेत्र</label>
                    <input type="text" name="model_sub_area" id="model_sub_area" readonly>
                </div>
                <div class="modal-body">
                    <label for="model_main_program">मुख्य कार्यक्रम</label>
                    <input type="text" name="model_main_program" id="model_main_program" readonly>
                </div>
                <div class="modal-body">
                    <label for="model_general_activity">मुख्य क्रियाकलाप</label>
                    <input type="text" name="model_general_activity" id="model_general_activity">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            id="general_activity_modal_close">Close
                    </button>
                    <button type="button" class="btn btn-default" id="general_activity_modalSubmit">Add</button>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('scripts')

    {{--    Fiscal Year change --}}
    <script>
        $(document).ready(function () {
            $('#fiscal_year').change(function () {
                $('#budget_sub_head').val('');
                $('#budget-table tbody').html('');
            })
        })
    </script>
    {{-- contenjency check box event--}}
    <script>
        $(document).on('change', '#contenjency_check_box', function () {
            if (this.checked) {
                let contenjencyPercent = '<input type="number" class="contenjency_percent" id="contenjency_percent">';
                $('#hidden_contenjency_percent').attr('type', 'number');
                $('#hidden_contenjency_percent').show();
                $('#hidden_contenjency_percent').html(contenjencyPercent);
            } else {

                $('#hidden_contenjency_percent').attr('type', 'hidden');
                $('#hidden_contenjency_percent').val('');

            }
        })
    </script>

    <script>
        $(document).ready(function () {
            $('#budget_sub_head').trigger('change');
        })
    </script>


    {{--Create गरे पछि फेरि बजेट शिर्षक selected भएर बस्छ  त्यसको लागि अख्तियारि पनि select हुने गरि--}}
    <script>
        $(document).ready(function () {
            let data = $('#budget_sub_head').val();
            getAkhtiyariByBudgetSubHead();
        })
    </script>

    {{--budget_sub_head click हुदा  total akhtiyari aaune--}}
    <script>
        $(document).on('change', '#budget_sub_head', function () {
            let fiscal_year = $('#fiscal_year').val();
            let budget_sub_head = $('select#budget_sub_head').val();
            let url = '{{route('get_total_akhtiyari_by_budget_sub_head',['123','345'])}}';
            url = url.replace('123', fiscal_year);
            url = url.replace('345', budget_sub_head);
            $.ajax({
                method: 'get',
                url: url,
                success: function (res) {
                    $('#totalAkhtiyari').text(res).addClass('e-n-t-n-n');
                    $('#totalAkhtiyariHiden').val(res)
                }
            })

        })
    </script>

    {{-- Page reload हुदा  प्र.चौ., दि.चौ., तृि.चौ. disable हुने--}}
    <script>
        $(document).ready(function () {

            // $('input#total_unit').prop("disabled", true);
            // $('input#first_quarter').prop("disabled", true);
            // $('input#second_quarter').prop("disabled", true);
            // $('input#third_quarter').prop("disabled", true);
        })

    </script>

    {{--अख्तियारि  प्रकार click हुदा कुल बजेट देखाउने--}}
    <script>
        let totalBudgetOnChangeAkhtiyari = function () {

            let akhtiyari = $('select#akhtiyari').val();
            let url = '{{route('get_total_budget_by_akhtiyari',123)}}';
            url = url.replace('123', akhtiyari);
            $.ajax({
                url: url,
                method: 'get',

                success: function (res) {

                    let total_budget = ($.parseJSON(res));
                    $('#budget_sub_head_total_budget').text(total_budget).addClass('e-n-t-n-n');
                    $('#totalBiniyojanHiden').val(total_budget);

                    let total_akhtiyari = $('#totalAkhtiyariHiden').val();
                    let total_biniyojan = $('#totalBiniyojanHiden').val();
                    let remain_budget = parseFloat(total_akhtiyari) - parseFloat(total_biniyojan);
                    $('#remain_budget').text(remain_budget);
                    $('#remain').val(remain_budget);

                    convertToNepaliNumber()
                }
            })
        };

        $(document).ready(function () {
            totalBudgetOnChangeAkhtiyari();

            $('#akhtiyari').change(function () {

                totalBudgetOnChangeAkhtiyari();
                let total_khtiyari = $('#totalAkhtiyariHiden').val();
                let total_biniyojan = $('#totalBiniyojanHiden').val();

                $('#total_budget').val('0');
                $('#first_total_budget').val('0');
                $('#second_total_budget').val('0');
                $('#third_total_budget').val('0')

            })
        })

    </script>

    {{--  getAkhtiyariByBudgetSubHead  --}}
    <script>
        let getAkhtiyariByBudgetSubHead = () => {
            let budget_sub_head = $('select#budget_sub_head').val();
            let fiscalYear = $('#fiscal_year').val();
            let url = '{{route('getAkhtiyari',['123','345'])}}';
            url = url.replace('123', fiscalYear);
            url = url.replace('345', budget_sub_head);
            $.ajax({
                method: 'get',
                url: url,
                success: function (res) {
                    let akhtiyari = $.parseJSON(res);
                    let options = '<option>......</option>';
                    if (akhtiyari.length > 0) {
                        if (akhtiyari.length > 1 || akhtiyari.length == 0) {
                            $.each($.parseJSON(res), function () {
                                options += '<option value="' + this.id + '">' + this.detail + '|' + this.amount + '</option>'
                            });
                            $('#akhtiyari').html(options);
                            $('#akhtiyari').focus();
                        } else {
                            options += '<option value="' + akhtiyari[0].id + '" selected>' + akhtiyari[0].detail + '|' + akhtiyari[0].amount + '</option>';
                            $('#akhtiyari').html(options).change();
                            $('#budget-table').focus();
                        }

                    } else {
                        $('#akhtiyari').html(options).change();
                    }
                }
            })
        }

    </script>

    {{--बजेट उपशिर्षक  change हुदा  अख्तियाराि आउने--}}
    <script>
        $(document).on('change', '#budget_sub_head', function () {
            getAkhtiyariByBudgetSubHead();
        });
    </script>

    {{--अख्तियारि change हुदा कार्यकर्म देखाउने तल टेबलमा--}}
    <script>
        let getActivityOnAkhtiyarChange = function () {
            let i = 1;
            let akhtiyari = $('select#akhtiyari').val();
            let url = '{{route('get_main_activity_by_akhtiyari',123)}}';
            url = url.replace(123, akhtiyari);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let parseRes = $.parseJSON(res);
                    let tr = '';
                    if (parseRes.length > 0) {

                        $.each($.parseJSON(res), function (key, value) {
                            var akhtiyari = this.budget_sub_head;
                            tr += "<tr style='background-color: white'>" +
                                "<td align='center' class='kalimati'>" +
                                i +
                                "</td>" +
                                "<td class='kalimati'>" +
                                this.expense_head +
                                "</td>" +
                                "<td class='byahora'>" +
                                this.expense_head_by_id.expense_head_sirsak +
                                "</td>" +

                                "<td class='details'>" +
                                this.activity +
                                "</td>" +

                                "<td class='drAmount kalimati' align='right'>" +
                                this.total_budget.toFixed(2) +
                                "</td>";
                            i = i + 1;
                        });

                        $('#budget-table').find('tbody').html(tr);
                    } else {

                        tr += "<tr style='background-color: white;'>" +
                            "<td colspan='5' align='center'>" +
                            "विवरण प्रविश्टि भएको छैन !!" +
                            "</td>";
                        $('#budget-table').find('tbody').html(tr);
                    }

                }
            })
        };

        $('#akhtiyari').change(function () {

            getActivityOnAkhtiyarChange()
        });

        if ('{{Session::get('budget_sub_head')}}') {

            getActivityOnAkhtiyarChange()
        }

    </script>

    {{--भार सम्बन्धि--}}
    <script>
        $(document).on('keyup', '#first_total_budget', function () {

            let totalAkhtiyari = $('#totalAkhtiyariHiden').val();
            let total_budget = $('#total_budget').val();
            if (totalAkhtiyari.length > 0 && total_budget.length > 0) {
                get_total_chaumasik(this);
                let first_chaumasik_budget = $('#first_total_budget').val();

                let bhar = (parseFloat(first_chaumasik_budget) * 100) / (parseFloat(totalAkhtiyari));
                if (bhar) {
                    $('#firstBhar').text('भार: ' + bhar.toFixed(3));
                    $('#firstChaumasikBharHidden').val(bhar.toFixed(3));
                }

            }

        });

        $(document).on('keyup', '#second_total_budget', function () {

            let totalAkhtiyari = $('#totalAkhtiyariHiden').val();
            let total_budget = $('#total_budget').val();
            if (totalAkhtiyari.length > 0 && total_budget.length > 0) {
                get_total_chaumasik(this);
                let second_chaumasik_budget = $('#second_total_budget').val();
                let bhar = (parseFloat(second_chaumasik_budget) * 100) / (parseFloat(totalAkhtiyari));
                if (bhar) {
                    $('#secondBhar').text('भार: ' + bhar.toFixed(3));
                    $('#secondChaumasikBharHidden').val(bhar.toFixed(3));
                }

            }
        });

        $(document).on('keyup', '#third_total_budget', function () {

            let totalAkhtiyari = $('#totalAkhtiyariHiden').val();
            let total_budget = $('#total_budget').val();

            if (totalAkhtiyari.length > 0 && total_budget.length > 0) {
                get_total_chaumasik(this);
                let third_chaumasik_budget = $('#third_total_budget').val();
                let bhar = (parseFloat(third_chaumasik_budget) * 100) / (parseFloat(totalAkhtiyari));
                if (bhar) {
                    $('#thirdBhar').text('भार: ' + bhar.toFixed(3));
                    $('#thirdChaumasikBharHidden').val(bhar.toFixed(3));
                }

            }
        });


        let get_total_chaumasik = function (this_) {

            let total_budget = $('#total_budget').val();
            let first_chaumasik_budget_temp = $('#first_total_budget').val();
            let second_chaumasik_budget_temp = $('#second_total_budget').val();
            let third_chaumasik_budget_temp = $('#third_total_budget').val();

            let first_chaumasik_budget = 0;
            let second_chaumasik_budget = 0;
            let third_chaumasik_budget = 0;
            if (first_chaumasik_budget_temp) {

                first_chaumasik_budget = first_chaumasik_budget_temp;

            } else {

                first_chaumasik_budget = 0;
            }

            if (second_chaumasik_budget_temp) {

                second_chaumasik_budget = second_chaumasik_budget_temp
            } else {

                second_chaumasik_budget = 0;
            }

            if (third_chaumasik_budget_temp) {

                third_chaumasik_budget = third_chaumasik_budget_temp
            } else {

                third_chaumasik_budget = 0;
            }

            let total_chaumasik = parseFloat(first_chaumasik_budget) + parseFloat(second_chaumasik_budget) + parseFloat(third_chaumasik_budget);


            if (total_budget >= total_chaumasik) {
                return true;

            } else {

                alert("कुल जम्मा भन्दा बढी भयो!!");
                let total_chaumasik = parseFloat(first_chaumasik_budget) + parseFloat(second_chaumasik_budget) + parseFloat(third_chaumasik_budget) - parseFloat($(this_).val());

                let remain = parseFloat(total_budget) - parseFloat(total_chaumasik);

                $(this_).val(0);
                $(this_).val(remain).keyup();
                // $(this_).trigger('change');

            }

        }
    </script>

    {{-- Get Sub Area on change of Area    --}}
    <script>
        $(document).ready(function () {
            $('#area').change(function () {
                var area_id = $('#area').val();
                if (area_id == 5) {
                    let expense_head = $('#expense_head').html();
                    $('#general_activity').html(expense_head);
                    get_sub_area_for_karyalaya_prasasanik(area_id);

                } else {
                    get_sub_area(area_id);

                }

            })
        });

        let get_sub_area = function () {
            var area_id = $('#area').val();
            let url = '{{route('admin.get_sub_area_by_area', 123)}}';
            url = url.replace(123, area_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let options = '<option value="">......................</option>';
                    $.each($.parseJSON(res), function () {
                        options += '<option value="' + this.id + '">' + this.name + '</option>'
                    });
                    let general_activity = '<option value="0">........</option><option value="add" class="test" id="add-main-activity">add</option>';

                    $('#general_activity').html(general_activity);
                    $('#sub_area').html(options);
                    $('#main_program').html("");


                }
            })
        };
        let get_sub_area_for_karyalaya_prasasanik = function () {
            var area_id = $('#area').val();
            let url = '{{route('admin.get_sub_area_by_area',123)}}';
            url = url.replace(123, area_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let options = '';
                    $.each($.parseJSON(res), function () {
                        options += '<option selected value="' + this.id + '">' + this.name + '</option>'
                    });
                    let general_activity = '<option value="0">........</option><option value="add" class="test" id="add-main-activity">add</option>';

                    // $('#general_activity').html(general_activity).addClass('select2');
                    $('#sub_area ').html(options);
                    $('#main_program ').html(options);

                }
            })
        }
    </script>

    {{--Get Main Program(मुख्य कार्यक्रम)  on change of  SubArea    --}}
    <script>
        $(document).ready(function () {
            $('#sub_area').change(function () {
                let subSubAreaId = $('#sub_area').val();
                let url = '{{route('admin.get_main_program_by_sub_area',123)}}';
                url = url.replace(123, subSubAreaId);
                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {

                        let options = '<option selected>--------</option>';
                        $.each($.parseJSON(res), function () {
                            options += '<option value="' + this.id + '">' + this.name + '</option>'
                        });

                        $('#main_program').html(options);

                    }
                })

            })
        })
    </script>

    {{-- Get Main Activity(मुख्या क्रियाकलाप)  on change of  Main Program --}}
    <script>
        $(document).ready(function () {
            $('#main_program').change(function () {
                let main_programId = $('#main_program').val();
                let url = '{{route('admin.get_general_activity_by_main_program_and_office_id',123)}}';
                url = url.replace(123, main_programId);
                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {

                        let options = '<option selected>-----------</option>';
                        options += '<option value="add" style="font-style: italic; color: blue; text-decoration: underline">Add New</option>';

                        $.each($.parseJSON(res), function () {
                            options += '<option value="' + this.id + '">' + this.name + '</option>'
                        });
                        $('#general_activity').html(options).addClass('select2');

                    }
                })

            })
        })
    </script>

    {{--  General Activity  Model submit--}}
    <script>
        $(document).ready(function () {
            $('#general_activity_modalSubmit').click(function () {
                let area_id = $('#area option:selected').val();
                let sub_area_id = $('#sub_area option:selected').val();
                let main_program_id = $('#main_program option:selected').val();
                let general_activity = $('#model_general_activity').val();
                data = {};
                data['_token'] = '{{csrf_token()}}';
                data['area_id'] = area_id;
                data['sub_area_id'] = sub_area_id;
                data['main_program_id'] = main_program_id;
                data['general_activity'] = general_activity;
                let url = '{{route('main.activity.store')}}';
                $.ajax({
                    url: url,
                    method: 'post',
                    data: data,
                    success: function (res) {
                        let options = '';
                        let main_Activity = $.parseJSON(res);
                        options += '<option value="' + main_Activity.id + '" selected>' + main_Activity.name + '</option>';

                        $('#general_activity').append(options);
                        $('#general_activity_modal').modal('hide');


                    }
                })
            })
        })
    </script>

    {{-- Model को  मुख्य क्रियाकलाप  key up function--}}
    <script>
        $(document).ready(function () {

            $('#model_general_activity').keyup(function () {

                let main_activity = $('#model_general_activity').val();

                $('#main_activity').val(main_activity);

            })
        })
    </script>

    {{--  मुख्या क्रियाकलापको  add click हुदा    --}}
    <script>
        $(document).ready(function () {
            $('select#general_activity').change(function () {
                if ($(this).val() == 'add') {
                    $('#general_activity_modal').modal('show');
                    let area = $('#area option:selected').text();
                    let sub_area = $('#sub_area option:selected').text();
                    let main_Activity = $('#main_program option:selected').text();

                    $('#model_area').val(area);
                    $('#model_sub_area').val(sub_area);
                    $('#model_main_program').val(main_Activity);
                }
            });

            $('#general_activity_modal_close').click(function () {
                $('select#general_activity').val('');
            })
        })
    </script>

    {{--प्रति एकाई key up--}}
    <script>
        $('#per_unit_cost').keyup(function () {

            let per_uni_rate = $('input#per_unit_cost').val();
            // key_up_on_per_unit_cost_and_total_unit();
            if (per_uni_rate != '') {

                // $('input#total_unit').prop("disabled",false);

            } else {
                // $('input#total_unit').prop("disabled",true);

            }
        })
    </script>

    {{-- Total Unit key Up function--}}
    <script>
        // let key_up_on_per_unit_cost_and_total_unit = () =>{
        //
        //     let per_unit_cost = $('#per_unit_cost').val();
        //     let total_unit = $('#total_unit').val();
        //     let first_quarter_data = $('#first_quarter').val();
        //     let second_quarter_data = $('#second_quarter').val();
        //     let third_quarter_data = $('#third_quarter').val();
        //     if(total_unit != ''){
        //
        //         let total_unit = $('#total_unit').val();
        //         let per_unit_cost = $('#per_unit_cost').val();
        //         if(per_unit_cost != '' & total_unit != ''){
        //
        //             let total_budget = parseFloat(per_unit_cost) * parseInt(total_unit);
        //
        //             // $('#total_budget').val(total_budget).change();
        //             $('#third_quarter').val(total_unit);
        //             // $('#third_total_budget').val(total_budget);
        //             $('input#first_quarter').prop("disabled", false);
        //             $('input#second_quarter').prop("disabled", false);
        //             $('input#third_quarter').prop("disabled", false);
        //
        //         } else {
        //
        //             $('input#first_quarter').prop("disabled", true);
        //             $('input#second_quarter').prop("disabled", true);
        //             $('input#third_quarter').prop("disabled", true);
        //         }
        //     } else {
        //
        //         $('input#first_quarter').prop("disabled", true);
        //         $('input#second_quarter').prop("disabled", true);
        //         $('input#third_quarter').prop("disabled", true);
        //     }
        //
        // };
        // $(document).ready(function () {
        //     $('#total_unit').keyup(function () {
        //         key_up_on_per_unit_cost_and_total_unit();
        //
        //     })
        // })
    </script>

    {{--    TOtal Budget change garda--}}
    <script>
        $(document).on('keyup', '#total_budget', function () {

            let remain_budget = $('#remain').val();
            let current_budget = $('#total_budget').val();
            if (current_budget.length > 0)
                if (parseFloat(remain_budget) >= parseFloat(current_budget)) {
                    $('#first_total_budget').val("");
                    $('#second_total_budget').val("");
                    $('#third_total_budget').val("");
                    return true;
                } else {

                    alert("बजेट भन्दा बढी भयो!!", +remain_budget);
                    $('#first_total_budget').val("");
                    $('#second_total_budget').val("");
                    $('#third_total_budget').val("");
                    $('#total_budget').val("");
                }
        })

    </script>

    {{--    First and Second Quarter key up function--}}
    <script>
        // $(document).ready(function () {
        //     $('.Quarter').keyup(function () {
        //
        //         let test = $(this).attr('id');
        //         let unit = $(this).val();
        //         let per_unit_cost = $('#per_unit_cost').val();
        //         let total_unit = $('#total_unit').val();
        //         let first_quarter_unit_temp = $('#first_quarter').val();
        //         first_quarter_unit = parseInt(first_quarter_unit_temp) || 0;
        //
        //         let second_quarter_unit_temp = $('#second_quarter').val();
        //         second_quarter_unit = parseInt(second_quarter_unit_temp) || 0;
        //
        //         let third_quarter_unit = $('#third_quarter').val();
        //         let first_second_sum = parseFloat(first_quarter_unit) + parseFloat(second_quarter_unit);
        //         if (parseFloat(total_unit) >= first_second_sum) {
        //
        //             let update_third_quarter_unit = parseFloat(total_unit) - first_second_sum;
        //             $('#third_quarter').val(update_third_quarter_unit);
        //             let third_total_budget = parseFloat(per_unit_cost) * parseFloat(update_third_quarter_unit);
        //             $('#third_total_budget').val(third_total_budget).change();
        //
        //             if ($(this).attr('id') == 'first_quarter') {
        //
        //                 let first_quarter_budget = parseFloat(first_quarter_unit) * parseFloat(per_unit_cost);
        //                 $('#first_total_budget').val(first_quarter_budget).change();
        //             }
        //             if (test == 'second_quarter') {
        //
        //                 let third_quarter_budget = parseFloat(second_quarter_unit) * parseFloat(per_unit_cost);
        //                 $('#second_total_budget').val(third_quarter_budget).change();
        //             }
        //
        //         } else {
        //             alert("जम्मा एकाइ भन्दा बढि भयो")
        //             let quarter = $(this).attr('id');
        //
        //             $('#' + quarter).val("");
        //         }
        //
        //
        //     })
        // })
    </script>


    {{--स्रोत प्रकार को आधारमा स्रोत आउने--}}
    <script>
        $(document).ready(function () {
            $('#source_type').change(function () {
                let sourceTypeId = $('#source_type').val();

                let url = '{{route('admin.get_source_by_source_type',123)}}';
                url = url.replace(123, sourceTypeId);
                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {
                        let options = '<option value="">............</option>';
                        $.each($.parseJSON(res), function () {
                            options += '<option value="' + this.id + '">' + this.name + '</option>'
                        });
                        $('#source').html(options);

                    }
                })
            })
        })
    </script>

    {{--स्रोत प्रकार को आधारमा बजेट स्रोत तह आउने--}}
    <script>
        $(document).ready(function () {
            $('#source_type').change(function () {
                let sourceTypeId = $('#source_type').val();

                let url = '{{route('admin.get_source_level_by_source_type',123)}}';
                url = url.replace(123, sourceTypeId);

                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (res) {
                        parseResult = $.parseJSON(res);
                        let options = '<option value="">............</option>';
                        if (parseResult.length > 1) {
                            $.each(parseResult, function () {
                                options += '<option value="' + this.id + '">' + this.name + '</option>'
                            });
                        } else {
                            options += '<option value="' + parseResult[0].id + '" selected>' + parseResult[0].name + '</option>'

                        }
                        $('#source_level').html(options);

                    }
                })
            })
        })
    </script>

    {{--Sunmit button click हुदा--}}
    <script>
        $(document).ready(function () {
            $('#submitbtn').click(function (e) {
                e.preventDefault();
                let totalAkhtiyari = $('#totalAkhtiyariHiden').val();
                let totalBiniyojan = $('#totalBiniyojanHiden').val();
                let current_amount = $('#total_budget').val();

                if (validation()) {
                    if (parseFloat(totalAkhtiyari) >= parseFloat(totalBiniyojan) + parseFloat(current_amount)) {
                        $('#budgetStoreCreateFrom').submit();

                    } else {
                        alert("माफ गर्नुहोस, अख्तियारि भन्दा बढी भयो!");
                        $('#per_unit_cost').focus();
                    }
                }
            })
        })
    </script>

    {{-- Validation   --}}
    <script>
        function validation() {

            let flag = 1;
            // budget_sub_head validation
            let budget_sub_headId = $('#budget_sub_head');

            if (!budget_sub_headId.val()) {
                if (budget_sub_headId.siblings('p').length == 0) {
                    budget_sub_headId.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                budget_sub_headId.focus();
                flag = 0;
            } else {
                budget_sub_headId.siblings('p').remove()
            }
            // budget_sub_head validation end

            // area validation
            // let areaId = $('#area');
            // if (!areaId.val()) {
            //     if (areaId.siblings('p').length == 0) {
            //         areaId.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
            //     }
            //     areaId.focus();
            //     flag = 0;
            // } else {
            //     areaId.siblings('p').remove()
            // }
            // area validation end


            // sub_area validation
            // let sub_areaId = $('#sub_area');
            // if (!sub_areaId.val()) {
            //     if (sub_areaId.siblings('p').length == 0) {
            //         sub_areaId.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
            //     }
            //     sub_areaId.focus();
            //     flag = 0;
            // } else {
            //     sub_areaId.siblings('p').remove()
            // }
            // sub_area validation end

            // main_program validation
            // let main_programId = $('#main_program');
            //
            // if (!main_programId.val()) {
            //     if (main_programId.siblings('p').length == 0) {
            //         main_programId.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
            //     }
            //     main_programId.focus();
            //     flag = 0;
            // } else {
            //     main_programId.siblings('p').remove()
            // }
            // main_program validation end

            // general_activity validation
            // let general_activityId = $('#general_activity');
            // if (!general_activityId.val()) {
            //     if (general_activityId.siblings('p').length == 0) {
            //         general_activityId.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
            //     }
            //     general_activityId.focus();
            //     flag = 0;
            // } else {
            //     general_activityId.siblings('p').remove()
            // }
            // general_activity validation end

            // main_activity validation
            let main_activityId = $('#main_activity');

            if (!main_activityId.val()) {
                if (main_activityId.siblings('p').length == 0) {
                    main_activityId.after('<p style="color:red" class="validation-error">लेख्नुहोस​!</p>')
                }
                main_activityId.focus();
                flag = 0;
            } else {
                main_activityId.siblings('p').remove()
            }
            // main_activity validation end

            // expense_head validation
            let expense_headId = $('#expense_head');
            if (!expense_headId.val()) {
                if (expense_headId.siblings('p').length == 0) {
                    expense_headId.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                expense_headId.focus();
                flag = 0;
            } else {
                expense_headId.siblings('p').remove()
            }
            // expense_head validation end

            // per_unit_cost validation
            // let per_unit_costId = $('#per_unit_cost');

            // if (!per_unit_costId.val()) {
            //     if (per_unit_costId.siblings('p').length == 0) {
            //         per_unit_costId.after('<p style="color:red" class="validation-error">लेख्नुहोस​!</p>')
            //     }
            //     per_unit_costId.focus();
            //     flag = 0;
            // } else {
            //     per_unit_costId.siblings('p').remove()
            // }
            // per_unit_cost validation end

            // total_unit validation
            let total_unitId = $('#total_unit');
            if (!total_unitId.val()) {
                if (total_unitId.siblings('p').length == 0) {
                    total_unitId.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                total_unitId.focus();
                flag = 0;
            } else {
                total_unitId.siblings('p').remove()
            }
            //total_unit validation end

            // source_type validation
            let source_typeId = $('#source_type');

            if (!source_typeId.val()) {
                if (source_typeId.siblings('p').length == 0) {
                    source_typeId.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                source_typeId.focus();
                flag = 0;
            } else {
                source_typeId.siblings('p').remove()
            }
            // source_type validation end

            // source_level validation
            let source_levelId = $('#source_level');

            if (!source_levelId.val()) {
                if (source_levelId.siblings('p').length == 0) {
                    source_levelId.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                source_levelId.focus();
                flag = 0;
            } else {
                source_levelId.siblings('p').remove()
            }
            // source_level validation end

            // source validation
            let sourceId = $('#source');

            if (!sourceId.val()) {
                if (sourceId.siblings('p').length == 0) {
                    sourceId.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                sourceId.focus();
                flag = 0;
            } else {
                sourceId.siblings('p').remove()
            }
            // source validation end

            // medium validation
            let mediumId = $('#medium');

            if (!mediumId.val()) {
                if (mediumId.siblings('p').length == 0) {
                    mediumId.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                }
                mediumId.focus();
                flag = 0;
            } else {
                mediumId.siblings('p').remove()
            }

            let contenjency_percent = $('#hidden_contenjency_percent');
            let check_checked = $('input[type=checkbox]').prop('checked');
            if (check_checked) {

                if (!contenjency_percent.val()) {
                    if (contenjency_percent.siblings('p').length == 0) {
                        contenjency_percent.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
                    }
                    contenjency_percent.focus();
                    flag = 0;
                } else {
                    contenjency_percent.siblings('p').remove()
                }
            }

            // medium validation end
            return flag;
        }
    </script>


    <script>
        changeToNepali = function (text) {
            let numbers = text.split('');
            let nepaliNo = '';
            $.each(numbers, function (key, value) {
                if (value) {
                    if (value == 1)
                        nepaliNo += "१";
                    else if (value == 2)

                        nepaliNo += "२";
                    else if (value == 3)

                        nepaliNo += "३";
                    else if (value == 4)

                        nepaliNo += "४";
                    else if (value == 5)

                        nepaliNo += "५";
                    else if (value == 6)

                        nepaliNo += "६";
                    else if (value == 7)

                        nepaliNo += "७";
                    else if (value == 8)

                        nepaliNo += "८";
                    else if (value == 9)

                        nepaliNo += "९";
                    else if (value == 0)

                        nepaliNo += "०";
                    else if (value == ',')

                        nepaliNo += ",";
                    else if (value == '.')

                        nepaliNo += ".";
                }
            });
            return nepaliNo;
        };

        let convertToNepaliNumber = () => {
            $('.e-n-t-n-n').each(function () {
                let nepaliNo = changeToNepali($(this).text());
                let nepaliVal = changeToNepali($(this).val());

                $(this).text(nepaliNo).removeClass('e-n-t-n-n');
                $(this).val(nepaliVal).removeClass('e-n-t-n-n');
            });
        }


    </script>
@endsection