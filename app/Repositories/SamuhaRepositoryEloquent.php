<?php
namespace App\Repositories;

use App\Models\Samuha;
use Illuminate\Support\Facades\Auth;

class SamuhaRepositoryEloquent implements SamuhaRepository
{
    public function create($attributes) {
        $samuha = new Samuha();
        $samuha->name = $attributes['name'];
        $samuha->sewa_id = $attributes['sewa_id'];
        $samuha->status = 1;
        $samuha->save();
        return $samuha;
    }

    public function get_all_samuha()
    {
        return Samuha::all();
    }

    public function get_by_id($id){
        return Samuha::findorfail($id);
    }

    public function update($attributes, $id){
        $samuha = $this->get_by_id($id);
        $samuha->name = $attributes['name'];
        $samuha->sewa_id = $attributes['sewa_id'];
        $samuha->save();
        return $samuha;
    }
}