@extends('backend.layouts.app')
@section('title')
  Program Create
@stop
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Program
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('admin.province')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('admin.program.update',$program->id)}}" method="post" id="programEditForm">
            {{csrf_field()}}
            <div class="form-group">
              <label for="name">Name:</label>
              <input type="text" class="form-control" id="name" placeholder="Program Name" name="name" value="{{$program->name}}">
            </div>
            <div class="form-group">
              <label for="program_code">Code:</label>
              <input type="text" class="form-control" id="program_code" placeholder="Program Code" name="program_code" value="{{$program->program_code}}">
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('scripts')
  <script>
    $("#programCreateForm").validate({
      rules: {
        name: {
          required: true,
        },
        program_code: {
          required: true,
        }
      },
      messages: {
        name: "Name Field is Required"
      }
    });
  </script>
@stop