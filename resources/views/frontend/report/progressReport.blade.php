<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">


<style>
    table {
        border-collapse: collapse;
    }

    tbody tr:hover {
        background-color: #e1e1e1 !important;

    }

    @media print {
        #downloadMe {
            display: none;
        }
    }

    body th {
        background: #e1e1e1 !important;
        position: sticky;
        top: 0;
        box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4);
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <input type="button" style="position: absolute; top:20px;right: 20px" value="Export to Excel" id="downloadMe">
    <section class="content-header">
        <div id="tableHeadingWrapper">
            <table width="99%" id="headingTable" style="font-size:13px">
                <tr>
                    <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                    <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else
                            प्रदेश
                            सभा @endif</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
                </tr>
                <tr>
                    <td colspan="6"
                        style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">
                        @if(Auth::user()->office->department)
                            {{Auth::user()->office->department->name}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->name}},
                        , {{Auth::user()->office->district->name}}</td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div style="width: 100%; text-align: center">
                            <b>कार्यक्रम / परियोजना अनुसार बजेट बिनियोजन</b><br>
                        </div>
                        <div style="float: right; margin-top: -20px">

                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>

                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 13px;">बजेट उपशीर्षक न. : <span
                                class="kalimati">{{$program->program_code}}</span>
                        <br/>
                        बजेट उपशीर्षक नाम: <span class="kalimati">{{$program->name}}</span></td>
                    <td style="text-align: right;padding-right: 26px;">आर्थीक वर्ष : <span
                                class="kalimati">{{$fiscalYear->year}}</span></td>
                </tr>

            </table>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body" id="tableWrapper">
                <table width="99%" border="1" id="progressReportTable"
                       style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th rowspan="3" width="13px">क्र.स.</th>
                        <th rowspan="3" style="width: 20%;">कार्यक्रम/आयोजनाको नाम</th>
                        <th rowspan="3">कार्यक्रम कोड</th>
                        <th colspan="3">वार्षिक लक्ष्य</th>
                        <th colspan="3">@if($data['quarter'] == 1) पहिलो @elseif($data['quarter'] == 2) दोस्रो @else
                                तेस्रो @endif लक्ष्य
                        </th>
                        <th colspan="3">@if($data['quarter'] == 1) पहिलो @elseif($data['quarter'] == 2) दोस्रो @else
                                तेस्रो @endif चौमासिक प्रगति
                        </th>
                        <th rowspan="2">सुचाकमा आधारित प्रमुख प्रतिफल</th>
                        <th rowspan="3" width="100px">कैफियत</th>
                    </tr>

                    <tr>
                        <th>परिमाण</th>
                        <th>भार</th>
                        <th>बजेट</th>
                        <th>परिमाण</th>
                        <th>भार</th>
                        <th>बजेट</th>
                        <th>परिमाण</th>
                        <th>खर्च रकम</th>
                        <th>भारित</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($activities as $index=>$activity)
                        <tr style="background-color: #ffffff">
                            <td class="kalimati" align="center">{{++$index}}</td>
                            <td>{{$activity->activity}}</td>
                            <td class="kalimati" align="center">{{$activity->activity_code}}</td>
                            <td class="quantity kalimati" align="center">{{$activity->total_unit}}</td>
                            <td class="total_bhar" align="right"></td>
                            <td class="total_budget kalimati"
                                align="right">{{number_format($activity->total_budget,2)}}</td>
                            <td class="quarter_quantity kalimati"
                                align="center">@if($data['quarter'] == 1) {{$activity->first_quarter_unit}} @elseif($data['quarter'] == 2) {{$activity->second_quarter_unit}} @else {{$activity->third_quarter_unit}} @endif</td>
                            <td class="quarter_bhar kalimati"
                                align="center">@if($data['quarter'] == 1) {{$activity->first_chaimasik_bhar}} @elseif($data['quarter'] == 2) {{$activity->second_chaimasik_bhar}} @else {{$activity->third_chaimasik_bhar}} @endif</td>
                            <td class="quarter_budget kalimati"
                                align="right">@if($data['quarter'] == 1) {{number_format($activity->first_quarter_budget,2)}} @elseif($data['quarter'] == 2) {{number_format($activity->second_quarter_budget,2)}} @else {{number_format($activity->third_quarter_budget,2)}} @endif</td>
                            <td class="progress_quantity kalimati"
                                align="center">@if($activity->getProgressDetails){{$activity->getProgressDetails->progress_quantity}}@endif</td>
                            <td class="quarter_expense kalimati" align="right">
                                @if($data['quarter'] == 1){{number_format($activity->firstChaumasikExpense($data['office'],$data['budget_sub_head'],$activity->id),2)}}
                                @elseif($data['quarter'] == 2){{number_format($activity->secondChaumasikExpense($data['office'],$data['budget_sub_head'],$activity->id),2)}}
                                @else
                                    {{number_format($activity->thirdChaumasikExpense($data['office'],$data['budget_sub_head'],$activity->id),2)}}
                                @endif
                            </td>
                            <td class="expenseBhar" align="right"></td>
                            <td>@if($activity->getProgressDetails){{$activity->getProgressDetails->activity_progress_detail}}@endif</td>
                            <td></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" style="text-align: right">जम्मा</td>
                        <td style="text-align: right"><span id="totalAnnualQuantity"></span></td>
                        <td style="text-align: center"><span id="totalAnnualBhar"></span></td>
                        <td class="kalimati" style="text-align: right"><span id="totalAnnualBudget"></span></td>
                        <td class="kalimati" style="text-align: right"><span id="totalQuarterQuantity"></span></td>
                        <td class="kalimati" style="text-align: right"><span id="totalQuarterBhar"></span></td>
                        <td class="kalimati" style="text-align: right"><span id="totalQuarterBudget"></span></td>
                        <td class="kalimati" style="text-align: right"><span id="totalExpQuarterQuantity"></span></td>
                        <td class="kalimati" style="text-align: right"><span id="totalQuarterExpense"></span></td>
                        <td class="kalimati" style="text-align: right"><span id="totalExpQuarterBhar"></span></td>
                        <td class="kalimati" style="text-align: right"></td>
                        <td class="kalimati" style="text-align: right"></td>
                        <input type="hidden" id="totalAkhtiyariHiden">
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script>
    $(document).ready(function () {
        let totalBhar = 0;
        let quarterBhar = 0;
        let quarterExpBhar = 0;
        let granTotalQuantity = 0;
        let granTotalBudget = 0;
        let granTotalBhar = 0;
        let granTotalQuaterBudget = 0;
        let granTotalQuaterBhar = 0;
        let granTotalQuarterQuantity = 0;
        let grantTotalProgressQuantity = 0;
        let granTotalQuarterExpense = 0;
        let granTotalExpeneBhar = 0;
        let trs = $('#progressReportTable tbody').find('tr').not('last');
        $.each(trs, function () {
            let totalAkhtiyari = '{!! $totalAkhtiyari !!}';

            let totalQuantity = $.trim($(this).find('td.quantity').text());
            totalQuantity = totalQuantity.replace(',', '').replace(',', '');
            totalQuantity = parseFloat(totalQuantity);

            let totalBudget = $.trim($(this).find('td.total_budget').text());
            totalBudget = totalBudget.replace(',', '').replace(',', '');
            totalBhar = parseFloat((totalBudget * 100) / totalAkhtiyari);

            let quarterBudget = $.trim($(this).find('td.quarter_budget').text());
            quarterBudget = parseFloat(quarterBudget.replace(',', '').replace(',', ''));
            quarterBhar = parseFloat((quarterBudget * 100) / totalAkhtiyari);

            let quarterExpense = $.trim($(this).find('td.quarter_expense').text());
            quarterExpense = parseFloat(quarterExpense.replace(',', '').replace(',', ''));
            quarterExpBhar = parseFloat((quarterExpense * 100) / totalAkhtiyari);

            let quarterQuantity = $.trim($(this).find('td.quarter_quantity').text());
            quarterQuantity = parseFloat(quarterQuantity.replace(',', '').replace(',', ''));

            let progressQuantity = $.trim($(this).find('td.progress_quantity').text());
            progressQuantity = parseFloat(progressQuantity.replace(',', '').replace(',', ''));

            let  quarterExpence = $.trim($(this).find('td.quarter_expense').text());
            quarterExpence = parseFloat(quarterExpence.replace(',','').replace(',',''));



            if (totalQuantity) {
                granTotalQuantity += totalQuantity;
            }
            if (totalBudget) {
                granTotalBudget += parseFloat(totalBudget);
            }
            if (quarterBhar) {
                granTotalBhar += parseFloat(totalBhar)
            }
            if (quarterBudget) {
                granTotalQuaterBudget += parseFloat(quarterBudget)
            }

            if (quarterBhar) {
                granTotalQuaterBhar += parseFloat(quarterBhar)
            }
            if (quarterQuantity) {
                granTotalQuarterQuantity += parseFloat(quarterQuantity);
            }
            if(progressQuantity){
                grantTotalProgressQuantity += progressQuantity;
            }
            if(quarterExpense){
                granTotalQuarterExpense += quarterExpense;
            }

            $(this).find('td.total_bhar').text(totalBhar.toFixed(2)).addClass('kalimati');
            $(this).find('td.quarter_bhar').text(quarterBhar.toFixed(2)).addClass('kalimati');
            $(this).find('td.expenseBhar').text(quarterExpBhar.toFixed(2)).addClass('kalimati')

            let  expenceBhar = $.trim($(this).find('td.expenseBhar').text());
            expenceBhar = parseFloat(expenceBhar.replace(',','').replace(',',''));
            console.log(expenceBhar);
            if(expenceBhar){
                granTotalExpeneBhar += parseFloat(expenceBhar)
            }
        });
        $('#totalAnnualQuantity').text(granTotalQuantity.toFixed(2)).addClass('kalimati');
        $('#totalAnnualBudget').text(granTotalBudget.toFixed(2)).addClass('kalimati');
        $('#totalAnnualBhar').text(granTotalBhar.toFixed(2)).addClass('kalimati');
        $('#totalQuarterBudget').text(granTotalQuaterBudget.toFixed(2)).addClass('kalimati');
        $('#totalQuarterBhar').text(granTotalQuaterBhar.toFixed(2)).addClass('kalimati');
        $('#totalQuarterQuantity').text(granTotalQuarterQuantity.toFixed(2)).addClass('kalimati');
        $('#totalExpQuarterQuantity').text(grantTotalProgressQuantity.toFixed(2)).addClass('kalimati');
        $('#totalQuarterExpense').text(granTotalQuarterExpense.toFixed(2)).addClass('kalimati');
        $('#totalExpQuarterBhar').text(granTotalExpeneBhar.toFixed(2)).addClass('kalimati');
    })
</script>


<script>
    let changeToNepali = function (text) {
        // alert(text);
        textTrim = $.trim(text);
        let numbers = textTrim.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (!isNaN(value) && value != ' ') {
                    if (value == 1)
                        nepaliNo += "१";
                    else if (value == 2)

                        nepaliNo += "२";
                    else if (value == 3)

                        nepaliNo += "३";
                    else if (value == 4)

                        nepaliNo += "४";
                    else if (value == 5)

                        nepaliNo += "५";
                    else if (value == 6)

                        nepaliNo += "६";
                    else if (value == 7)

                        nepaliNo += "७";
                    else if (value == 8)

                        nepaliNo += "८";
                    else if (value == 9)

                        nepaliNo += "९";
                    else if (value == 0)

                        nepaliNo += "०";
                    else if (value == ',')

                        nepaliNo += ",";
                    else if (value == '.')

                        nepaliNo += ".";
                    else if (value == '/')

                        nepaliNo += "/";

                }
            }
        });
        return nepaliNo;
    };

    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });


    var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
            ,
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta charset="utf-8"/><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
            , format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            };

        return function (table, name, filename) {
            var table1 = '';
            if (!table1.nodeType) table1 = document.getElementById('headingTable');
            if (!table.nodeType) table = document.getElementById(table);
            var bigTable = table1.innerHTML + table.innerHTML;
            var ctx = {worksheet: name || 'Worksheet', table: bigTable};

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })();

    $("#downloadMe").click(function (e) {
        let allTable = $('div[id$=tableHeadingWrapper]').html() + $('div[id$=tableWrapper]').html();
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(allTable));
        e.preventDefault();
    });

</script>