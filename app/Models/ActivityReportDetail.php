<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ActivityReportDetail extends Model
{
    protected $table = 'activity_report_details';
    protected $guarded = [];

    public function getThisVoucherAExpense($expense_head_code)
    {
        return $voucherExpense = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->select('activity_report_details.expense_head', 'activity_report_details.expense', 'vouchers.data_nepali', 'vouchers.jv_number', 'vouchers.short_narration', 'vouchers.id')
            ->where('vouchers.status', '=', 1)
            ->where('activity_report_details.voucher_id', $this->id)
            ->where('activity_report_details.expense_head', $expense_head_code)
            ->sum('activity_report_details.expense');
    }

    public function voucher()
    {
        return $this->belongsTo('App\Models\Voucher', 'voucher_id', 'id');
    }

    public function budgetDetails()
    {
        return $this->belongsTo('App\Models\BudgetDetails', 'activity_id', 'id');
    }

    public function get_expense_head()
    {
        return $this->belongsTo('App\Models\ExpenseHead', 'expense_head', 'expense_head_code');
    }


    public function get_total_expense_by_expense_head($fiscal_year,$expense_head_code){

        $office_id = Auth::user()->office->id;
        return $expenseHeadExpense = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_report_id', '=', 'activity_report.id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('budget_details', 'activity_report_details.activity_id', '=', 'budget_details.id')
            ->where('vouchers.status', '=', 1)
            ->where('vouchers.office_id', '=',$office_id )
            ->where('activity_report_details.fiscal_year', $fiscal_year)
            ->where('activity_report_details.expense_head', $expense_head_code)
            ->sum('activity_report_details.expense');
    }

}
