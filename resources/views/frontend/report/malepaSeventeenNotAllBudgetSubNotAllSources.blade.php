<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">


<style>
    table {
        border-collapse: collapse;
    }
    @media print {
        #downloadMe {
            display: none;
        }
    }
    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <input type="button" style="position: absolute; top:20px;right: 20px" value="Export to Excel" id="downloadMe">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div id="tableHeadingWrapper">
            <table width="99%" style="font-size:13px">
                <tr>
                    <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                    <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else
                            प्रदेश
                            सभा @endif</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
                </tr>
                <tr>
                    <td colspan="6"
                        style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">
                        @if(Auth::user()->office->department)
                            {{Auth::user()->office->department->name}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->name}},
                         {{Auth::user()->office->district->name}}</td>
                </tr>
                <tr>
                    <td class="kalimati" colspan="6" style="text-align: center"><b>कार्यालय कोड: <span
                                    class="kalimati">{{Auth::user()->office->office_code}}</span></b></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div style="width: 100%; text-align: center">
                            <b>दातृ निकायगत बजेट र खर्चको आर्थिक बिवरण</b><br>
                        </div>
                        <div style="float: right; margin-top: -20px; padding-right: 21px">
                            म ले प फा न. २१३
                        </div>
                </td>
            </tr>
            <tr>
                <div>
                    <td style="text-align: center">
                        <b>आ.व. : <span class="e-n-t-n-n">{{$fiscalYear->year}}</span></b>
                    </td>
                </div>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td class="kalimati" style="padding-left: 17px;">बजेट उपशिर्षक न. : <b><span
                                class="kalimati">{{$program->program_code}}</span></b><br>
                    बजेट उपशिर्षक नाम: <b>{{$program->name}}</b></td>
            </tr>
        </table>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="panel-body" id="tableWrapper">
                    <table class="table" width="99%" border="1" style="background-color:#dbdbdb; font-size: 12px"
                           id="fatwari_table">
                        <thead>
                        <tr>
                            <th rowspan="2">दातृ निकाय</th>
                            <th rowspan="2">खर्च संकेत</th>
                            <th rowspan="2" style="width: 10%">विवरण</th>
                            <th colspan="2">स्रोतको</th>
                            <th rowspan="2">शुरु बजेट</th>
                            <th colspan="2">संशोधन /रकमान्तर/श्रोतान्तर बाट</th>
                            <th rowspan="2">अन्तिम बजेट</th>
                            <th rowspan="2">निकासा</th>
                            <th rowspan="2">जम्मा खर्च</th>
                            <th rowspan="2">बाँकी बजेट</th>
                            <th rowspan="2">गत वर्षको खर्च</th>
                        </tr>
                        <tr>
                            <th>प्रकार</th>
                            <th style="width: 3%">भुक्तानी विधि</th>
                            <th>थप</th>
                            <th>घट</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sourceTypes as $index=>$sourceType)
                            <?php
                            $i = 1;
                            ?>
                            @foreach($sourceType->getExpenseHead($datas['office_id'],$datas['fiscal_year'],$datas['program'],$sourceType->id) as $index=>$expenseHead)
                                <tr style="background-color: white">

                                    <td class="kalimati">@if($i){{$sourceType->name}}@endif</td>
                                    <td class=""><span
                                                class="e-n-t-n-n">{{$expenseHead->expense_head_by_id->expense_head_code}}</span>
                                    </td>
                                    <td class="">{{$expenseHead->expense_head_by_id->expense_head_sirsak}}</td>
                                    <td>@if($sourceType->id == 2)प्रदेश सरकार @endif</td>
                                    <td>@if($sourceType->id == 2)नगद @endif</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->getInitialBudget($datas['office_id'],$datas['fiscal_year'],$datas['program'],$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->getAddBudget($datas['office_id'],$datas['fiscal_year'],$datas['program'],$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->getReduceBudget($datas['office_id'],$datas['fiscal_year'],$datas['program'],$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->getFinalBudget($datas['office_id'],$datas['fiscal_year'],$datas['program'],$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->getTotalExpense($datas['office_id'],$datas['fiscal_year'],$datas['program'],$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->getTotalExpense($datas['office_id'],$datas['fiscal_year'],$datas['program'],$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td class="kalimati"
                                        style="text-align: right">{{number_format($sourceType->remainBudget($datas['office_id'],$datas['fiscal_year'],$datas['program'],$expenseHead->expense_head_id,$sourceType->id),2)}}</td>
                                    <td></td>

                                <?php
                                $i = 0;
                                ?>
                                @endforeach
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align: right">जम्मा</td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->getTotalInitialBudget($datas['office_id'],$datas['fiscal_year'],$datas['program'],$sourceType->id),2)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->getTotalAddBudget($datas['office_id'],$datas['fiscal_year'],$datas['program'],$sourceType->id),2)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->getTotalReduceBudget($datas['office_id'],$datas['fiscal_year'],$datas['program'],$sourceType->id),2)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->getTotalFinalBudget($datas['office_id'],$datas['fiscal_year'],$datas['program'],$sourceType->id),2)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->getGranTotalExpense($datas['office_id'],$datas['fiscal_year'],$datas['program'],$sourceType->id),2)}}</td>
                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->getGranTotalExpense($datas['office_id'],$datas['fiscal_year'],$datas['program'],$sourceType->id),2)}}</td>

                                <td class="kalimati"
                                    style="text-align: right">{{number_format($sourceType->totalRemainBudget($datas['office_id'],$datas['fiscal_year'],$datas['program'],$sourceType->id),2)}}</td>
                                <td></td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>


                <table width="99%" style="font-size: 13px">

                    <tr>
                        <td style="padding-left: 52px;">तयार गर्ने :</td>
                        <td>पेश गर्ने :</td>
                        <td>सदर गर्ने :</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">पद :</td>
                        <td>पद :</td>
                        <td>पद :</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">मिति :</td>
                        <td>मिति :</td>
                        <td>मिति :</td>
                    </tr>

                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script>
    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '/')

                    nepaliNo += "/";
            }
        });
        // console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });


</script>

<script>
    $(document).ready(function () {
        let trs = $('#fatwari_table tbody').find('tr:not(.second_last,.last)');
        let total_budget = 0;
        let total_this_month_expense = 0;
        let total_up_to_prev_month_expense = 0;
        let total_up_to_this_month_expense = 0;
        let total_advance = 0;
        let total_exp_with_out_advance = 0;
        let total_remain_budget = 0;
        let expense_percentage = 0;
        // alert(trs.length);
        $.each(trs, function () {

            let total_budget_nep = $.trim($(this).find('td.total-budget').text());
            let total_this_month_expense_nep = $.trim($(this).find('td.this-month-exp').text());
            let total_up_to_prev_month_expense_nep = $.trim($(this).find('td.up-to-pre-month-exp').text());
            let total_up_to_this_month_expense_nep = $.trim($(this).find('td.up-to-this-month-exp').text());
            let advance_nep = $.trim($(this).find('td.advance').text());
            let exp_with_out_advance_nep = $.trim($(this).find('td.exp-with-out-advance').text());
            let remain_budget_nep = $.trim($(this).find('td.remain-budget').text());


            total_budget_roman = convertNepaliToEnglish(total_budget_nep);
            total_this_month_expense_roamn = convertNepaliToEnglish(total_this_month_expense_nep);

            total_up_to_prev_month_expense_roamn = convertNepaliToEnglish(total_up_to_prev_month_expense_nep);
            total_up_to_this_month_expense_roamn = convertNepaliToEnglish(total_up_to_this_month_expense_nep);
            advence_roamn = convertNepaliToEnglish(advance_nep);
            exp_with_out_advance_roman = convertNepaliToEnglish(exp_with_out_advance_nep);
            remain_budget_roman = convertNepaliToEnglish(remain_budget_nep);
            // alert(remain_budget_roman);

            total_budget = parseFloat(total_budget) + parseFloat(total_budget_roman);
            total_this_month_expense = parseFloat(total_this_month_expense) + parseFloat(total_this_month_expense_roamn);
            total_up_to_prev_month_expense = parseFloat(total_up_to_prev_month_expense) + parseFloat(total_up_to_prev_month_expense_roamn);
            total_up_to_this_month_expense = parseFloat(total_up_to_this_month_expense) + parseFloat(total_up_to_this_month_expense_roamn);
            total_advance = parseFloat(total_advance) + parseFloat(advence_roamn);
            total_exp_with_out_advance = parseFloat(total_exp_with_out_advance) + parseFloat(exp_with_out_advance_roman);
            total_remain_budget = parseFloat(total_remain_budget) + parseFloat(remain_budget_roman);
            expense_percentage = (parseFloat(total_up_to_this_month_expense) * 100) / parseFloat(total_budget)


        });


        $('#total_last_budget').text(changeToNepali(total_budget.toString()));
        $('#this_month_nikasa').text(changeToNepali(total_up_to_this_month_expense.toString()));
        $('#this_month_exp').text(changeToNepali(total_this_month_expense.toString()));
        $('#up_to_prev_month_exp').text(changeToNepali(total_up_to_prev_month_expense.toString()));
        $('#up_to_this_month_exp').text(changeToNepali(total_up_to_this_month_expense.toString()));
        $('#advance').text(changeToNepali(total_advance.toString()));
        $('#exp_with_out_advance').text(changeToNepali(total_exp_with_out_advance.toString()));
        $('#remain_budget').text(changeToNepali(total_remain_budget.toString()));
        $('#total_expense_percentage').text(changeToNepali(expense_percentage.toFixed(3)));

        // console.log(expense_percentage,'%');

        // alert(total_remain_budget);
    })
</script>

<script>
    function convertNepaliToEnglish(input) {
        // console.log(input);
        var charArray = input.split('');
        console.log('Test', charArray[0]);
        // charArray = charArray[0].split('');
        var engDate = '';
        $.each(charArray, function (key, value) {

            switch (value) {
                case '१':
                    engDate += '1';
                    break;
                case '२':
                    engDate += '2';
                    break;
                case '३':
                    engDate += '3';
                    break;
                case '४':
                    engDate += '4';
                    break;
                case '५':
                    engDate += '5';
                    break;
                case '६':
                    engDate += '6';
                    break;
                case '०':
                    engDate += '0';
                    break;
                case '७':
                    engDate += '7';
                    break;
                case '८':
                    engDate += '8';
                    break;
                case '९':
                    engDate += '9';
                    break;

                case '-':
                    engDate += '-';
                    break;
                case '.':
                    engDate += '.';
                    break
            }
            // console.log(engDate)
        });

        return engDate

    }
</script>

<script>
    $("#downloadMe").click(function (e) {
        let allTable = $('div[id$=tableHeadingWrapper]').html() + $('div[id$=tableWrapper]').html();
        // console.log(allTable);
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(allTable));
        e.preventDefault();
    });
</script>