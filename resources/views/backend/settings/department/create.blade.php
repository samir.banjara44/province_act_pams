@extends('backend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Department
                <small>Control panel</small>
            </h1>
            <ul class="breadcrumb">
                <li><a type="button" href="{{route('admin.department')}}" class="btn btn-sm btn-primary"><i
                                class="fa fa-pencil"></i> List</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('admin.department.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" placeholder="Department" name="name">
                        </div>
                        <div class="form-group">
                            <label for="province">Province</label>
                            <select name="province_id" class="form-control">
                                @foreach($provinces as $province)
                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="mof">MOF</label>
                            <select name="mof_id" class="form-control" id="mof">
                                @foreach($mofs as $mof)
                                    <option value="{{$mof->id}}">{{$mof->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ministry">Ministry</label>
                            <select name="ministry_id" class="form-control" id="ministry">
                                @foreach($ministries as $ministry)
                                    <option value="{{$ministry->id}}">{{$ministry->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection