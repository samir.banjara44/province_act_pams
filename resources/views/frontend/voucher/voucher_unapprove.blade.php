@extends('frontend.layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><i class="fas fa-calendar-times"></i> भौचर अस्विकृत गर्ने। </h1>

  </section>
  <section class="content">
    <div class="panel panel-primary">
      <div class="panel-body">
        <form action="{{route('voucher.unapprove')}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <div class="row">

              <div class="col-md-2">
                <label>कार्यालय</label>
                <select class="form-control select2" name="office_id">
                  <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}</option>
                </select>
              </div>
              <div class="col-md-2">
                <label>आर्थिक वर्ष : </label>
                <select name="fiscal_year" id="fiscal_year" class="form-control">
                  @foreach($fiscalYears as $fy)
                    <option value="{{$fy->id}}"
                            @if($fy->id == $fiscalYear->id) selected @endif>{{$fy->year}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-3">
                <label>बजेट उपशिर्षक</label>
                <select class="form-control select2" name="budget_sub_head" id="budget_sub_head" required>
                  <option value="">.................</option>
                  @foreach($programs as $program)
                  <option value="{{$program->id}}">{{$program->name}} | {{$program->program_code}}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-md-2">
                <label>भौचर नं.</label>
                <input type="number" name="voucher_number" class="form-control" required>
              </div>

              <div class="col-md-2">
                <label for="">&nbsp;</label>
                <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-times-circle"></i> अस्विकृत
                  गर्ने</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
  <script>
    $(document).ready(function () {
      $('#fiscal_year').change(function () {
        $('#budget_sub_head').val(null).trigger('change');
      })
    })
  </script>
@endsection