<?php

namespace App\Http\Controllers\Frontend;

use App\helpers\FiscalYearHelper;
use App\Http\Controllers\Controller;
use App\Models\ActivityReport;
use App\Models\AllBank;
use App\Models\Area;
use App\Models\Bank;
use App\Models\DarbandiSrot;
use App\Models\DarbandiType;
use App\Models\Department;
use App\Models\Budget;
use App\Models\Designation;
use App\Models\ExpenseHead;
use App\Models\MainProgram;
use App\Models\Medium;
use App\Models\Ministry;
use App\Models\Mof;
use App\Models\Month;
use App\Models\Office;
use App\Models\PartyTypes;
use App\Models\PreBhuktani;
use App\Models\Province;
use App\Models\Programs;
use App\Models\Samuha;
use App\Models\Sewa;
use App\Models\Source;
use App\Models\SourceType;
use App\Models\SubArea;
use App\Models\Taha;
use App\Models\VatOffice;
use App\Models\Voucher;
use App\Models\LedgerType;
use App\Models\VoucherDetail;
use App\Repositories\AreaRepository;
use App\Repositories\MainProgramRepository;
use App\Repositories\PreBhuktaniRepository;
use App\Repositories\BhuktaniRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\SourceTypeRepository;
use App\Repositories\SubAreaRepository;
use App\Repositories\VoucherDetailsRepository;
use App\Repositories\KarmachariRepository;
use App\Repositories\VoucherRepository;
use Illuminate\Http\Request;
use App\Repositories\BudgetRepository;
use App\Repositories\VoucherSignatureRepository;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ExpenseHeadRepository;


class VoucherController extends Controller
{

    private $programRepo;
    private $budgetRepo;
    private $areaRepo;
    private $subAreaRepo;
    private $mainProgramRepo;
    private $sourceTypeRepo;
    private $voucherSignatureRepo;
    private $bhuktaniRepo;
    private $expenseHeadRepo;


    public function __construct(

        ProgramRepository $programRepo,
        BudgetRepository $budgetRepo,
        AreaRepository $areaRepo,
        SubAreaRepository $subAreaRepo,
        MainProgramRepository $mainProgramRepo,
        SourceTypeRepository $sourceTypeRepo,
        VoucherRepository $voucherRepo,
        VoucherDetailsRepository $voucherDetailsRepo,
        PreBhuktaniRepository $preBhuktaniRepo,
        KarmachariRepository $karmachariRepo,
        VoucherSignatureRepository $voucherSignatureRepo,
        BhuktaniRepository $bhuktaniRepo,
        ExpenseHeadRepository $expenseHeadRepo

    ) {

        $this->middleware('auth');
        $this->voucherRepo = $voucherRepo;
        $this->voucherDetailsRepo = $voucherDetailsRepo;
        $this->preBhuktaniRepo = $preBhuktaniRepo;
        $this->budgetRepo = $budgetRepo;
        $this->programRepo = $programRepo;
        $this->areaRepo = $areaRepo;
        $this->subAreaRepo = $subAreaRepo;
        $this->mainProgramRepo = $mainProgramRepo;
        $this->karmachariRepo = $karmachariRepo;
        //        $this->$sourceTypeRepo = $sourceTypeRepo;
        $this->mainProgramRepo = $mainProgramRepo;
        $this->voucherSignatureRepo = $voucherSignatureRepo;
        $this->bhuktaniRepo = $bhuktaniRepo;
        $this->fiscalYearHelper = new FiscalYearHelper();
    }

    public function create()
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $provinces = Province::all();
        $offices = Office::all();
        $darbandisrots = DarbandiSrot::all();
        $darbanditypes = DarbandiType::all();
        $sewas = Sewa::all();
        $samuhas = Samuha::all();
        $pads = Designation::all();
        $tahas = Taha::all();

        $sources = SourceType::all();
        $ledgerTypes = LedgerType::where('status', 1)->get();
        $programs = $this->programRepo->get_program_for_voucher();
        $expenseHeads = ExpenseHead::all();
        $mediums = Medium::all();

        //for advance payment modal
        $office_id = Auth::user()->office_id;
        $party_types = PartyTypes::all();
        $all_banks = AllBank::all();
        $vat_offices = VatOffice::all();

        return view('frontend.voucher.create', compact('programs', 'expenseHeads', 'sources', 'mediums', 'ledgerTypes', 'all_banks', 'office_id', 'party_types', 'vat_offices', 'provinces', 'offices', 'darbandisrots', 'darbanditypes', 'sewas', 'samuhas', 'pads', 'tahas', 'fiscalYears', 'fiscalYear'));
    }

    public function summaryVoucherCreate()
    {

        $provinces = Province::all();
        $offices = Office::all();
        $darbandisrots = DarbandiSrot::all();
        $darbanditypes = DarbandiType::all();
        $sewas = Sewa::all();
        $samuhas = Samuha::all();
        $pads = Designation::all();
        $tahas = Taha::all();

        $sources = SourceType::all();
        $ledgerTypes = LedgerType::where('status', 1)->get();
        $programs = $this->programRepo->get_program_for_voucher();
        $expenseHeads = ExpenseHead::all();
        $mediums = Medium::all();

        //        for advance payment modal
        $office_id = Auth::user()->office_id;
        //        $banks = Bank::where('office_id', $office_id)->get();
        $party_types = PartyTypes::all();
        $all_banks = AllBank::all();
        $vat_offices = VatOffice::all();

        return view('frontend.voucher.summary_create', compact('programs', 'expenseHeads', 'sources', 'mediums', 'ledgerTypes', 'all_banks', 'office_id', 'party_types', 'vat_offices', 'provinces', 'offices', 'darbandisrots', 'darbanditypes', 'sewas', 'samuhas', 'pads', 'tahas'));
    }

    public function store(Request $request)
    {
        $datas = $request->all();
        //      Voucher Save
        $office_id = Auth::user()->office_id;
        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id($office_id);
        $voucherId = $this->voucherRepo->create($request['voucher'], $voucher_signature);

        //        VOucher Details Save
        $this->voucherDetailsRepo->create($request['voucherDetails'], $request['voucher'], $voucherId);

        //        Pre Bhuktani save
        if (array_key_exists('preBhuktani', $datas) and sizeof($datas['preBhuktani']) > 0) {

            foreach ($datas['preBhuktani'] as $preBhuktani) {
                $preBhuktani['voucher_id'] = $voucherId;
                $budget = $this->budgetRepo->get_by_id_for_pre_bhuktani($preBhuktani['main_activity_id']);
                $preBhuktani['expense_head_id'] = $budget['expense_head_id'];
                $this->preBhuktaniRepo->create($preBhuktani);
            }
        }
        $request->session()->put('success', 'Created Successfully');
        return response()->json(['success' => 'Successfully Created']);
    }
    public function storeAlyaVoucher(Request $request)
    {
        $requestData = $request->all();
        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id(Auth::user()->office->id);
        $requestData['lastYear'] = $this->fiscalYearHelper->get_fiscal_by_id($requestData['fiscal_year'] - 1);
        $alyaVoucher = $this->voucherRepo->createAlyaVOucher($requestData, $voucher_signature);
        $voucherDetails = $this->voucherDetailsRepo->createAlyaVoucherDetails($requestData, $alyaVoucher);
        return json_encode($voucherDetails);
    }

    public function get_voucher_by_budget_sub_head_and_office(Request $request, $fiscal_year, $budget_sub_head_id)
    {
        $office_id = Auth::user()->office->id;
        $voucherNumber = $this->voucherRepo->get_vouhcer_number_by_budget_sub_head_and_office_id($office_id, $fiscal_year, $budget_sub_head_id);
        return json_encode($voucherNumber);
    }

    public function voucher_accept_create()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $office_id = Auth::user()->office->id;
        $programs = $this->programRepo->get_program_for_report();  // office repo मै तानेको  छ।

        return view('frontend.voucher.getList', compact('programs', 'fiscalYear', 'fiscalYears'));
    }

    public function get_voucher_list_by_status(Request $request, $fiscal_year, $budget_sub_head)
    {
        $office_id = Auth::user()->office->id;
        $voucherLists = $this->voucherRepo->getVoucherByStatusAndBudgetSubHead($fiscal_year, $budget_sub_head, $office_id);
        return json_encode($voucherLists);
    }

    public function set_approved(Request $request, $voucher_id)
    {
        $voucher = $this->voucherRepo->find($voucher_id);
        $voucherDetail = $voucher->details;
        $dr_amount = number_format($voucherDetail->sum('dr_amount'), 2);
        $cr_amount = number_format($voucherDetail->sum('cr_amount'), 2);
        if ($dr_amount == $cr_amount) {
            $voucher_unapproved = $this->voucherRepo->set_voucher_approved($voucher_id);
            $pre_bhuktanies = $this->preBhuktaniRepo->get_preBhuktani_by_voucher_id($voucher_id);
            if ($pre_bhuktanies->count() > 0) {

                if ($pre_bhuktanies[0]->bhuktani != null) {
                    if ($pre_bhuktanies->count() > 0) {
                        foreach ($pre_bhuktanies as $pre_bhuktani) {

                            $update_pre_bhuktani = $this->preBhuktaniRepo->update_status_one_by_id($pre_bhuktani['id']);
                        }
                        $pre_bhuktani_amount = $pre_bhuktanies->sum('amount');
                        $bhuktani_id = $pre_bhuktanies[0]->bhuktani_id;
                        $update_hunktani = $this->bhuktaniRepo->update_by_bhuktani_add_amount_and_status($bhuktani_id, $pre_bhuktani_amount);
                    }
                }
            }
            return json_encode('true');
        } else {
            return json_encode('false');
        }
    }

    public function voucher_edit_parameter()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $office_id = Auth::user()->office->id;
        $programs = $this->programRepo->get_program_for_voucher();  // office, repo मै तानेको  छ।
        $all_banks = AllBank::all();
        return view('frontend.voucher.editList', compact('programs', 'fiscalYear', 'all_banks', 'fiscalYears'));
    }

    public function update(Request $request)
    {
        $dataList = $request->all();
        $voucher_id = $dataList['voucher_id'];
        $voucher = $dataList['voucher'];
        $voucherDetails = $dataList['voucherDetails'];
        $office_id = Auth::user()->office_id;
        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id($office_id);
        if (array_key_exists('voucher', $dataList)) {
            $this->voucherRepo->updateVoucher($voucher, $voucher_signature);
        }

        if (array_key_exists('voucherDetails', $dataList)) {
            $this->voucherDetailsRepo->updateVoucherDetails($voucher_id, $voucherDetails);
        }

        if (array_key_exists('preBhuktani', $dataList)) {
            $preBhuktani = $dataList['preBhuktani'];
            $voucher = Voucher::findorfail($voucher_id);
            $preBhuktaniData = PreBhuktani::where('journel_id', $voucher_id)->get();
            $voucher->preBhuktani()->delete();
            $cheque_print = 0;
            if ($preBhuktaniData[0]->cheque_print != 0) {
                $cheque_print = $preBhuktaniData[0]->cheque_print;
            }
            foreach ($preBhuktani as $pre) {
                $budget = $this->budgetRepo->get_by_id_for_pre_bhuktani($pre['main_activity_id']);
                $pre['cheque_print'] = $cheque_print;
                $pre['expense_head_id'] = $budget['expense_head_id'];
                $this->preBhuktaniRepo->updatePreBhuktani($voucher_id, $pre, $preBhuktaniData);
            }
        } else {

            $this->preBhuktaniRepo->delete_by_vouhcer_id($voucher_id);
        }
        $data = [];
        $data['route'] = route('voucher');
        $data['success'] = "Success";
        return json_encode($data);
    }

    public function get_budget_and_expe_by_activity($activity_id)
    {
        $data = [];
        $data['budget'] = $this->budgetRepo->get_total_budget_by_Activity_id($activity_id);
        $data['expense'] = ActivityReport::where('activity_id', $activity_id)->sum('expense');
        $data['advance'] = $this->voucherDetailsRepo->get_advance_by_activity_id($activity_id);
        $data['remain'] = ActivityReport::where('activity_id', $activity_id)->sum('remain');
        return json_encode($data);
    }

    public function delete($id)
    {
        $voucher_delete = $this->voucherRepo->delete($id);
        return json_encode($voucher_delete);
    }

    public function getVoucherSignature()
    {
        $karmacharies = $this->karmachariRepo->getAllKarmachariByApproveStatus();
        $office_id = Auth::user()->office_id;
        return view('frontend.voucher.voucher_signature', compact('karmacharies', 'office_id'));
    }

    public function voucherSignatureCreate(Request $request)
    {
        $voucher_signature = $this->voucherSignatureRepo->create($request->all());
        return redirect()->back();
    }

    public function voucherSignatureList(Request $request, $office_id)
    {

        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id($office_id);
        return json_encode($voucher_signature);
    }

    public function voucherUnApprovedParam()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $office_id = Auth::user()->office_id;
        $programs = $this->programRepo->get_program_for_voucher();
        return view('frontend.voucher.voucher_unapprove', compact('programs', 'office_id', 'fiscalYear', 'fiscalYears'));
    }

    public function voucherUnApprove(Request $request)
    {
        $data = $request->all();
        $voucher_id = $this->voucherRepo->change_status_by_budget_sub_head_and_office_id($data);
        if ($voucher_id) {
            //            $pre_bhuktanies = $this->preBhuktaniRepo->get_preBhuktani_by_voucher_id($voucher_id);

            //            if ($pre_bhuktanies->count() > 0) {
            //                if ($pre_bhuktanies[0]->bhuktani != null) {
            //                    foreach ($pre_bhuktanies as $pre_bhuktani) {
            //                        $update_pre_bhuktani = $this->preBhuktaniRepo->update_status_by_id($pre_bhuktani['id']);
            //                    }
            //
            //                    $pre_bhuktani_amount = $pre_bhuktanies->sum('amount');
            //                    $bhuktani_id = $pre_bhuktanies[0]->bhuktani_id;
            //                    $update_hunktani = $this->bhuktaniRepo->update_by_bhuktani_amount_and_status($bhuktani_id, $pre_bhuktani_amount);
            //                }
            //            }
            return redirect()->back()->with('success', 'Voucher Un-approved successfully');
        }

        return redirect()->back()->with('error', 'canot find voucher');
    }

    public function grantVoucherParam()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $office_id = Auth::user()->office_id;
        $programs = $this->programRepo->get_program_for_voucher();
        return view('frontend.voucher.grant_voucher_param', compact('programs', 'office_id', 'fiscalYears', 'fiscalYear'));
    }

    public function grantVoucher(Request $request)
    {
        $data = $request->all();
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($data['fiscal_year']);
        $office_id = Auth::user()->office_id;
        $programs = $this->programRepo->getById($data['budget_sub_head']);
        $bhuktanies = $this->bhuktaniRepo->get_by_budget_sub_head_id($data['fiscal_year'], $data['budget_sub_head']);
        return view('frontend.voucher.grant_voucher_create', compact('programs', 'office_id', 'bhuktanies', 'fiscalYear'));
    }

    public function grantVoucherStore(Request $request)
    {
        $data = $request->all();
        $office_id = Auth::user()->office_id;
        $data['date_english'] = $data['bs-roman'];
        $myPrebhuktanis = PreBhuktani::whereIn('bhuktani_id', $data['bhuktani'])
            ->where('office_id', $office_id)
            ->groupBy('main_activity_id')
            ->selectRaw('*,sum(amount) as sum')
            ->pluck('sum', 'main_activity_id');
        $encodedBhuktaniId = json_encode($data['bhuktani']);
        $data['amount'] = $data['totalAmount'] = array_sum($myPrebhuktanis->toArray());
        $data['voucher_number'] = $this->voucherRepo->get_vouhcer_number_by_budget_sub_head_and_office_id($office_id, $data['fiscal_year'], $data['budget_sub_head']);
        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id($office_id);
        $voucher_id = $this->voucherRepo->createNikasaVoucher($data, $voucher_signature);

        $data['drOrCr'] = 1;
        $data['ledger_type_id'] = 3;
        $this->voucherDetailsRepo->createNikasaVoucherDetails($data, $voucher_id);  // Debit wala
        $this->voucherDetailsRepo->createBhuktaniVoucher($data, $voucher_id, $encodedBhuktaniId);  // Debit wala
        foreach ($myPrebhuktanis as $key => $value) {

            $data['activity_id'] = $key;
            $data['amount'] = $value;
            $data['drOrCr'] = 2;
            $data['ledger_type_id'] = 8;
            $budget = $this->budgetRepo->get_by_id($key);
            $this->voucherDetailsRepo->createNikasaVoucherDetails($data, $voucher_id, $budget);  // Credit
        }

        foreach ($data['bhuktani'] as $key => $value) {
            $updateBhuktani = $this->bhuktaniRepo->updateIsContenjencyById($value);
        }
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $office_id = Auth::user()->office_id;
        $programs = $this->programRepo->get_program_for_voucher();
        return view('frontend.voucher.grant_voucher_param', compact('programs', 'office_id', 'fiscalYears', 'fiscalYear'));
    }
}
