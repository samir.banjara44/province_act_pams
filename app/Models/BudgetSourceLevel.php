<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BudgetSourceLevel extends Model
{
    protected $table = 'budget_source_levels';
    protected $guarded = [];

    public function source_type(){
        return $this->belongsTo('App\Models\SourceType');
    }


}
