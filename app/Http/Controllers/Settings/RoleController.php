<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Province;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{

    private $homeRepo;

    public function __construct(HomeRepository $homeRepo)
    {

        $this->middleware('auth');
        $this->homeRepo = $homeRepo;
    }

    public function index()
    {
        $roles = Role::all();
        return view('backend.settings.role.index', compact('roles'));
    }

    public function create(){
        return view('backend.settings.role.create');
    }
    public function store(Request $request){

        $role = new Role();
        $role->name = $request->name;
        $role->save();
        return redirect(route('admin.role'));
    }

}
