<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocalLevel extends Model
{
    protected $table = 'local_levels';
    protected $guarded = [];



    public function district(){
        return $this->belongsTo('App\District');
    }
}
