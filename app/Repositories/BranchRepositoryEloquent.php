<?php
namespace App\Repositories;

use App\Models\Branch;
use Illuminate\Support\Facades\Auth;

class BranchRepositoryEloquent implements BranchRepository
{
    public function create($attributes) {
        $branch = new Branch();
        $branch->name_eng = $attributes['name_eng'];
        $branch->name_nep = $attributes['name_nep'];
        $branch->office_id = Auth::user()->office->id;
        $branch->user_id = Auth::user()->id;
        $branch->status = 1;
        $branch->save();
        return $branch;
    }

    public function get_by_id($id){
        return $branch = Branch::findorfail($id);
    }

    public function update($attributes, $id){
        $branch = $this->get_by_id($id);
        $branch->name_eng = $attributes['name_eng'];
        $branch->name_nep = $attributes['name_nep'];
        $branch->save();
        return $branch;
    }



    public function get_by_office_id(){

        $office_id = Auth::user()->office->id;
        return $branchs = branch::where('office_id',$office_id)->where('status','1')->get();
    }

    public function get_by_user_id(){

        $user_id = Auth::user()->id;
        return $branchs = branch::where('id',$user_id)->where('status','1')->get();
    }
}