<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\DarbandiType;
use App\Repositories\DarbandiTypeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DarbandiTypeController extends Controller
{

    private $darbanditypeRepo;

    public function __construct(DarbandiTypeRepository $darbanditypeRepo)
    {
        $this->middleware('auth');
        $this->darbanditypeRepo = $darbanditypeRepo;
    }

    public function index()
    {
        $darbanditypes= $this->darbanditypeRepo->get_all_darbanditypes();
        return view('backend.settings.darbanditypes.index', compact('darbanditypes'));
    }

    public function create(){

        return view('backend.settings.darbanditypes.create');
    }
    public function store(Request $request){

        $darbanditype = $this->darbanditypeRepo->create($request->all());
        return redirect(route('admin.darbanditype'));
    }

}
