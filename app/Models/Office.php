<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\helpers\BsHelper;

class Office extends Model
{
    protected $table = 'offices';
    protected $guarded = [];

    public function province()
    {
        return $this->belongsTo('App\Models\Province');
    }

    public function mof()
    {
        return $this->belongsTo('App\Models\Mof');
    }

    public function ministry()
    {
        return $this->belongsTo('App\Models\Ministry');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    public function users()
    {
        return $this->hasMany('App\Users');
    }

    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id', 'id');
    }

    public function budget()
    {

        return $this->hasMany('App\Models\Budget');
    }

    public function vouchers()
    {

        return $this->hasMany('App\Models\Voucher');
    }

    public function getBudgetSubHead($officeId)
    {
        return $programs = Programs::where('office_id', $officeId)->where('status', 1)->with('akhtiyari')->get();

    }

//    Province Budget
    public function getBudgetProvinceChalu($office_id)
    {
        return $provinceBudgetChalu = Budget::where('office_id', $office_id)
            ->where('source_type', 2)
            ->whereBetween('expense_head', ['21000', '29000'])->sum('total_budget');
    }

    public function getBudgetProvincePuji($office_id)
    {
        return $provinceBudgetChalu = Budget::where('office_id', $office_id)
            ->where('source_type', 2)
            ->whereBetween('expense_head', ['30000', '31511'])->sum('total_budget');
    }

//    Province Expense
    public function getExpenseProvinceChalu($office_id)
    {
        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('budget.source_type', 2)
            ->where('voucher_details.office_id', '=', $office_id)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');

    }

    public function getExpenseProvincePuji($office_id)
    {
        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('budget.source_type', 2)
            ->where('voucher_details.office_id', '=', $office_id)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');

    }


//    Federal
    public function getBudgetFederalChalu($office_id)
    {
        return $provinceBudgetChalu = Budget::where('office_id', $office_id)
            ->where('source_type', 1)
            ->whereBetween('expense_head', ['21000', '30000'])->sum('total_budget');
    }

    public function getBudgetFederalPuji($office_id)
    {
        return $provinceBudgetChalu = Budget::where('office_id', $office_id)
            ->where('source_type', 1)
            ->whereBetween('expense_head', ['30000', '31511'])->sum('total_budget');
    }


    public function getExpenseFederalChalu($office_id)
    {
        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->select('voucher_details.*')
            ->where('budget.source_type', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');

    }

    public function getExpenseFederalPuji($office_id)
    {
        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->select('voucher_details.*')
            ->where('budget.source_type', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');

    }

    public function getBudgetProvinceTotal($Office_id)
    {

        return $totalProvinceBudget = Budget::where('office_id', $Office_id)
            ->where('source_type', 2)
            ->sum('total_budget');
    }

    public function getBudgetFederalTotal($office_id)
    {

        return $totalProvinceBudget = Budget::where('office_id', $office_id)
            ->where('source_type', 1)
            ->sum('total_budget');
    }

    public function getTotalBudget($office_id)
    {

        $totalProvinceBudget = $this->getBudgetProvinceTotal($office_id);
        $totalFederalBudget = $this->getBudgetFederalTotal($office_id);
        return $totalBudget = (float)$totalProvinceBudget + (float)$totalFederalBudget;
    }

    public function getExpenseProvinceTotal($office_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('budget.source_type', 2)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');

    }

    public function getExpenseFederalTotal($office_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('budget.source_type', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');

    }

    public function getTotalExpense($office_id)
    {

        $totalProvinceExp = $this->getExpenseProvinceTotal($office_id);
        $totalFederalExp = $this->getExpenseFederalTotal($office_id);
        return $totalExpense = (float)$totalProvinceExp + (float)$totalFederalExp;
    }

    public function remainBudget($office_id)
    {

        $totalBudget = $this->getTotalBudget($office_id);
        $totaExpense = $this->getTotalExpense($office_id);

        return $remainBudget = (float)$totalBudget - (float)$totaExpense;
    }

    public function getInitialBudgetOfOffice($office_id)
    {

        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $office_id)
            ->where('akhtiyari.akhtiyari_type', 1)
            ->sum('budget.total_budget');
    }

    public function getAddBudgetOfOffice($office_id)
    {

        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $office_id)
            ->where('akhtiyari.akhtiyari_type', 2)
            ->sum('budget.total_budget');
    }

    public function getReduceBudgetOfOffice($office_id)
    {

        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $office_id)
            ->where('akhtiyari.akhtiyari_type', 3)
            ->sum('budget.total_budget');
    }

    public function getFinalBudgetOfOffice($office_id)
    {

        return $initialBudget = Budget::
        where('budget.office_id', $office_id)
            ->sum('budget.total_budget');
    }

    public function getPercentages($office_id)
    {

        $totalBudget = $this->getFinalBudgetOfOffice($office_id);
        $totalExpense = $this->getExpenseOfOffice($office_id);
        return $percentage = (float)($totalExpense * 100) / (float)$totalBudget;
    }

    public function getExpenseOfOffice($office_id)
    {

        return $expense = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->select('activity_report_details.*')
            ->where('vouchers.status', '=', 1)
            ->where('activity_report.office_id', '=', $office_id)
            ->sum('activity_report_details.expense');

//        return $initialBudget = VoucherDetail::join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
//            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
//            ->where('vouchers.status',1)
//            ->where('voucher_details.office_id', $office_id)
//            ->where('dr_or_cr', 1)
//            ->where('ledger_type_id', ['1', '4'])
//            ->sum('dr_amount');
    }

    public function getRemainBudgetOfOffice($office_id)
    {

        $totalBudget = $this->getFinalBudgetOfOffice($office_id);
        $totalExpense = $this->getExpenseOfOffice($office_id);
        return $remainBudget = (float)$totalBudget - (float)$totalExpense;
    }

    public function getLastDateTransaction($office_id)
    {

        $lastDate = Voucher::where('office_id', $office_id)->orderBy('created_at', 'desc')->first();
        $bsObj = new BsHelper();
        $explodeData = explode('-', $lastDate->created_at->format('Y-m-d'));
        $nep_date = $bsObj->eng_to_nep($explodeData[0], $explodeData[1], $explodeData[2]);
        return $nep = $nep_date["year"] . '-' . $nep_date["month"] . '-' . $nep_date["date"];


    }

    public function getLastVoucher($office_id)
    {

        $vouchers = Voucher::where('office_id', $office_id)->orderBy('jv_number', 'desc')->first();
        return $vouchers->jv_number;
    }


    public function getBudgetSubHeadByOffice($office_id)
    {

        return $budgetSubHeads = Programs::where('office_id', $office_id)->get();

    }
}
