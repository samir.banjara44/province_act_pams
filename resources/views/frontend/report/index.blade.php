
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bank

      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('bank.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Create</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>Name</th>
              <th>Adress</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($banks as $bank)
              <tr>
                <td>{{$bank->name}}</td>
                <td>{{$bank->address}}</td>

                <td>
                  <a type="button" href="{{route('admin.program.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection