<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Darta;
use App\Models\Branch;
use App\Repositories\DartaRepository;
use App\Repositories\BranchRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\helpers\FiscalYearHelper;
use Carbon\Carbon;


class DartaController extends Controller
{
    private $dartaRepo;
    private $branchRepo;

    public function __construct(DartaRepository $dartaRepo, BranchRepository $branchRepo)
    {
        $this->middleware('auth');
        $this->dartaRepo = $dartaRepo;
        $this->branchRepo = $branchRepo;
        $this->fiscalYearHelper = new FiscalYearHelper();
    }

    public function dartaIndex()
    {
        $dartas = $this->dartaRepo->get_by_office_id();
        $dartaNumber = $this->dartaRepo->get_last_number_by_office(Auth::user()->office->id);
        $branch = $this->branchRepo->get_by_office_id();
        return view('frontend.darta.index', compact('dartas', 'dartaNumber', 'branch'));
    }

    public function dartaCreate(){
        return view('frontend.darta.create');
    }

    public function dartaStore(Request $request)
    {
        $requsetData = $request->all();
        $officeId = Auth::user()->office->id;
        if($request->hasFile('image')){
            $this->validate($request, [
                'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $mytime = date('Ymd_His');
            $image = $request->file('image');
            $getimageName = $officeId.'_'.$mytime.'.'.$request->file('image')->getClientOriginalExtension();

            $image->move(public_path('images'), $getimageName);
            $requsetData['image'] = $getimageName;
        }
        $dartaNumber = $this->dartaRepo->get_last_number_by_office(Auth::user()->office->id);
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $requsetData['letter_number'] = $fiscalYear->year;
        $requsetData['darta_number'] = $dartaNumber;
        $this->dartaRepo->create($requsetData);
        return redirect(route('darta'))->with('message', 'Data successfully added to database');
    }

    public function dartaEdit($id)
    {
        $darta = $this->dartaRepo->get_by_id($id);
        $branch = Branch::where('office_id',Auth::user()->office->id)->get();
        return view('frontend.darta.edit', compact('darta', 'branch'));
    }

    public function dartaUpdate(Request $request, $dartaId)
    {

        try{
            $dartaRequest = $request->all();
            $officeId = Auth::user()->office->id;
            if($request->hasFile('image')){
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $mytime = date('Ymd_His');
                $image = $request->file('image');
                $getimageName = $officeId.'_'.$mytime.'.'.$request->file('image')->getClientOriginalExtension();

                $image->move(public_path('images'), $getimageName);
                $dartaRequest['image'] = $getimageName;

            }
            $darta = $this->dartaRepo->update($dartaRequest, $dartaId);
            return redirect(route('darta'))->with('message', 'Data successfully Edited');
        } catch (\Exception $e){
            $message = "दर्ता सम्पादन भएको छैन!";
            return redirect()->back()->with('error', $message);
        }
    }

    public function show($id){

        $darta = $this->dartaRepo->get_by_id($id);
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.darta.show', compact('darta','fiscalYear'));
    }

    public function showAll($officeId){
        $dartas = $this->dartaRepo->get_by_office_id();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.darta.show_all', compact('dartas','fiscalYear'));

    }

    public function delete($id){
        $darta = $this->dartaRepo->get_by_id($id);
        if($darta){
            $darta->delete();
            return json_encode(true);
        } else {
            return json_encode(false);
        }
    }
}
