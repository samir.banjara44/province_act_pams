@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
              धरौटी अभिलेख प्रविशटी
            </h1>
            <ul class="breadcrumb">
                <li><a type="button" href="{{route('retention.record')}}" class="btn btn-sm btn-primary"><i
                                class="fa fa-pencil"></i> सुची हेर्ने</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('retention.record.update',$retentionRecord->id)}}" id="retentionRecordForm"
                          method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="office">कार्यालय:</label>
                                    <select class="form-control" name="office" id="office_id">
                                        <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="fiscal_year">आर्थिक वर्ष</label>
                                    <select id="fiscal_year" class="form-control" name="fiscal_year" required>
                                        <option value="{{$fiscalYear->id}}"
                                                selected>{{$fiscalYear->year}}</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="date">मिति</label>
                                    <input type="text" class="form-control" id="date" name="date" required>
                                    <input type="hidden" class="form-control" id="created_date" name="created_date">
                                </div>

                                <div class="col-md-2">
                                    <label for="address">धरौटीको वर्ग</label>
                                    <select class="form-control" name="retention_category_id" id="retention_category_id">
                                        <option value="1">विविध</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="purpose">प्रयोजन/वापत</label>
                                    <input type="text" class="form-control" id="purpose" placeholder="प्रयोजन"
                                           name="purpose" value="{{$retentionRecord->purpose}}" required>
                                </div>

                                <div class="col-md-2">
                                    <label for="information_number">सूचना नम्बर/संकेत</label>
                                    <input type="number" class="form-control" id="information_number"
                                           placeholder="सूचना नम्बर/संकेत" name="information_number"
                                           value="{{$retentionRecord->information_number}}" required>
                                </div>
                                <div class="col-md-2">
                                    <label for="information_published_date">सूचना प्रकाशित मिति</label>
                                    <input type="text" class="form-control" id="information_published_date"
                                           placeholder="सूचना प्रकाशित मिति" name="information_published_date" required>
                                    <input type="hidden" class="form-control" id="hidden_information_published_date"
                                           name="hidden_information_published_date">
                                </div>
                                <div class="col-md-2">
                                    <label for="information_last_date">सुचनाको अन्तिम मिति</label>
                                    <input type="text" class="form-control" id="information_last_date"
                                           placeholder="सुचनाको अन्तिम मिति" name="information_last_date">
                                    <input type="hidden" class="form-control" id="hiden_information_last_date"
                                           name="hiden_information_last_date">
                                </div>

                                <div class="col-md-2">
                                    <button type="submit" id="btnRetentionRecordSave" class="btn btn-primary"
                                            style="margin-top: 18px;">Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')

    <script>
        $(document).on('click', '#btnRetentionRecordSave', function (e) {
            e.preventDefault();
            let information_published_date_roamn = convertNepaliToEnglish($('#information_published_date').val());
            let information_last_date_roamn = convertNepaliToEnglish($('#information_last_date').val());
            let created_date = convertNepaliToEnglish($('#date').val());
            $('#hidden_information_published_date').val(information_published_date_roamn);
            $('#hiden_information_last_date').val(information_published_date_roamn);
            $('#created_date').val(created_date);

            $('#retentionRecordForm').submit();

        })
    </script>

    {{--get  party name --}}
    <script>
        let getPartyByPartyType = function (party_type_id, party_name = '') {
            let url = '{{route('get_party_name_by_party_type_and_office', 123)}}';
            url = url.replace(123, party_type_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let option = '<option value="">....................</option>';
                    $.each($.parseJSON(res), function () {
                        if (party_name == this.name_nep) {
                            option += '<option value="' + this.id + '" selected>' + this.name_nep + '</option>'

                        } else {
                            option += '<option value="' + this.id + '">' + this.name_nep + '</option>'
                        }
                    });
                    $('#party-name').html(option);
                }
            })
        }
    </script>

    {{--Date Picker --}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);

        $("#information_published_date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        $("#information_last_date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        $("#date").val(formatedNepaliDate);
        $("#information_published_date").val(formatedNepaliDate);
        $("#information_last_date").val(formatedNepaliDate);

    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            var charArray = input.split('');
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1';
                        break;
                    case '२':
                        engDate += '2';
                        break;
                    case '३':
                        engDate += '3';
                        break;
                    case '४':
                        engDate += '4';
                        break;
                    case '५':
                        engDate += '5';
                        break;
                    case '६':
                        engDate += '6';
                        break;
                    case '०':
                        engDate += '0';
                        break;
                    case '७':
                        engDate += '7';
                        break;
                    case '८':
                        engDate += '8';
                        break;
                    case '९':
                        engDate += '9';
                        break;

                    case '-':
                        engDate += '-';
                        break
                }
            });
            return engDate

        }
    </script>

    {{-- Validation   --}}
    <script>
      function validation() {
        let flag = 1;
        let category = $('#retention_category_id');
        if (!category.val()) {
          if (category.siblings('p').length == 0) {
            category.after('<p style="color:red" class="validation-error">छान्नुहोस!</p>')
          }
          category.focus();
          flag = 0;
        } else {
          category.siblings('p').remove()
        }

        let purpose = $('#purpose');
        if (!purpose.val()) {
          if (purpose.siblings('p').length == 0) {
            purpose.after('<p style="color:red" class="validation-error">लेख्नुहोस!</p>')
          }
          purpose.focus();
          flag = 0;
        } else {
          purpose.siblings('p').remove()
        }

        // let infoNumber = $('#information_number');
        // if(!infoNumber.val()){
        //   if(infoNumber.siblings('p').length == 0){
        //     infoNumber.after('<p style="color:red" class="validation-error">लेख्नुहोस्! </p> ')
        //   }
        //   infoNumber.focus();
        //   flag = 0;
        // } else {
        //   infoNumber.siblings('p').remove();
        // }

        return flag;
      }
    </script>
@endsection