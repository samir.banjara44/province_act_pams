<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Budget extends Model
{
    protected $table = 'budget';
    protected $guarded = [];

    public function budgetArea()
    {

        return $this->belongsTo('App\Models\Area', 'area', 'id');
    }

    public function chaineDetail()
    {
        return $this->hasMany('App\Models\BudgetDetails', 'budget_id', 'id');
    }

    public function voucherDetails()
    {
        return $this->hasMany('App\Models\VoucherDetail');
    }

    public function getVoucherDetails()
    {

        return $this->hasMany('App\Models\VoucherDetail', 'main_activity_id', 'id');

    }

    public function expense_by_expense_head()
    {

        return $this->hasOne('App\Models\ExpenseHead', 'expense_head_code', 'expense_head');
    }


    public function expense_head_by_id()
    {

        return $this->hasOne('App\Models\ExpenseHead', 'id', 'expense_head_id');
    }

    public function get_source()
    {

        return $this->hasOne('App\Models\Source', 'id', 'source');

    }

    public function get_source_type()
    {

        return $this->hasOne('App\Models\SourceType', 'id', 'source_type');
    }

    public function get_budget_source_level()
    {

        return $this->hasOne('App\Models\BudgetSourceLevel', 'id', 'source_level');
    }

    public function get_medium()
    {

        return $this->hasOne('App\Models\Medium', 'id', 'medium');
    }

    public function getProgressDetails(){
        return $this->hasOne('App\Models\ActivityProgress','budget_details_id','id');
    }

    public function getDetail(){

       return $budgetDetails = $this->chaineDetail;
    }

    public function hasDetails(){

       return $details = $this->chaineDetail()->get();
    }



    public function getLastBudget($expenHead, $program_id)
    {
        return $last_budget = Budget::
                            where('fiscal_year', $this->fiscal_year)
                                ->where('budget_sub_head', $program_id)
                                ->where('expense_head', $expenHead)
                                ->sum('total_budget');

    }

    public function get_expense_this_month($fiscal_year,$budget_sub_head, $month_post)
    {
        return $vouchers = ActivityReportDetail::join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
                            ->join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
                            ->where('vouchers.status', '=',1)
                            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
                            ->where('activity_report_details.fiscal_year', $fiscal_year)
                            ->where('activity_report_details.expense_head', $this->expense_head)
                            ->Where('activity_report_details.month', '=', $month_post)
                            ->sum('activity_report_details.expense');
    }

    public function get_expense_upto_prev_month($fiscal_year,$budget_sub_head, $month_post)
    {
        return $vouchers = ActivityReportDetail::join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
            ->join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->where('vouchers.status', 1)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.fiscal_year', $fiscal_year)
            ->where('activity_report_details.expense_head', $this->expense_head)
            ->where('activity_report_details.month', '<', $month_post)
            ->sum('activity_report_details.expense');

    }

    public function get_expense_upto_this_month($fiscal_year,$budget_sub_head, $month_post)
    {
        return $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
                        ->join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
                        ->where('vouchers.status', '=',1)
                        ->where('activity_report.budget_sub_head_id', $budget_sub_head)
                        ->where('activity_report_details.fiscal_year', $fiscal_year)
                        ->where('activity_report_details.expense_head', $this->expense_head)
                        ->where('activity_report_details.month','<=',$month_post)
                        ->sum('activity_report_details.expense');
    }

    public function get_peski_upto_this_month($fiscal_year,$budget_sub_head, $month_post)
    {
         $advance = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $fiscal_year)
            ->where('voucher_details.office_id', '=', $this->office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.month', '<=', $month_post)
            ->where('voucher_details.expense_head_id', '=', $this->expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('ledger_type_id', '4')
            ->sum('voucher_details.dr_amount');

         $advanceClearance = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $fiscal_year)
            ->where('voucher_details.office_id', '=', $this->office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.month', '<=', $month_post)
            ->where('voucher_details.expense_head_id', '=', $this->expense_head_id)
            ->where('voucher_details.dr_or_cr',2)
            ->where('ledger_type_id',9)
            ->sum('voucher_details.cr_amount');
         return $remain = (float)$advance - (float)$advanceClearance;

    }

    public function expense_without_advance($fiscal_year,$budget_sub_head, $month_post)
    {

        $get_total_expense = $this->get_expense_upto_this_month($fiscal_year,$budget_sub_head, $month_post);
        $total_advance = $this->get_peski_upto_this_month($fiscal_year,$budget_sub_head, $month_post);
        return $total_exp_with_out_advance = (float)$get_total_expense - (float)$total_advance;

    }

    public function remain_budget($fiscal_year,$total_budget, $month_post, $expenHead, $program_id, $budget_sub_head)
    {
        $total_budget = $this->getLastBudget($expenHead, $program_id);
        $get_total_expense = $this->get_expense_upto_this_month($fiscal_year,$budget_sub_head, $month_post);
        return $remain = (float)$total_budget - (float)$get_total_expense;
    }

    public function getExpenseHead($officeId, $budgetSubHeadId)
    {

        return $budgetByExpenseHead = Budget::where('office_id', $officeId)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->groupBy('expense_head')
            ->orderBy('expense_head')
            ->get();
    }

//    Fatwari between month
    public function get_expense_till_this_month($budget_sub_head, $MonthTo)
    {
        return $vouchers = ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
            ->where('vouchers.status', '=',1)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.expense_head', $this->expense_head)
            ->where('activity_report_details.month','<=',$MonthTo)
            ->sum('activity_report_details.expense');
    }

    public function get_expense_till_prev_month($budget_sub_head, $monthFrom)
    {
        return $vouchers = ActivityReportDetail::join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
            ->join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->where('vouchers.status', 1)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.expense_head', $this->expense_head)
            ->where('activity_report_details.month', '<=', $monthFrom)
            ->sum('activity_report_details.expense');

    }
    public function get_expense_to_month($budget_sub_head, $toMonth)
    {
        return $vouchers = ActivityReportDetail::join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
            ->join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->where('vouchers.status', 1)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head)
            ->where('activity_report_details.expense_head', $this->expense_head)
            ->where('activity_report_details.month', '=', $toMonth)
            ->sum('activity_report_details.expense');

    }




//    Ministry Report

    public function getLastBudgetOfMinistry($ministryId, $expenseHead)
    {

        return $last_budget = Budget::join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministryId)
            ->where('budget.expense_head_id', $expenseHead)
            ->sum('budget.total_budget');
    }


    public function getTotalExpenseOfMinistry($ministryId, $expenseHeadId)
    {

        $ldate = date('Y-m-d H:i:s');
        $ldate_y = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministryId)
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', '=', $this->expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpToYesterdayOfMinistry($ministryId, $expenseHeadId)
    {

        $ldate = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministryId)
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', '=', $expenseHeadId)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseTodayOfMinistry($ministryId, $expenseHeadId)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministryId)
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', '=', $expenseHeadId)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpToThisDayOfMinistry($ministryId, $expenseHeadId)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministryId)
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', '=', $expenseHeadId)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalAdvanceOfMinistry($ministryId, $expenseHeadId)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministryId)
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', '=', $expenseHeadId)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('dr_amount');
    }

    public function expenseWithOutAdvance($ministryId, $expenseHeadId)
    {


        $totalExpense = $this->getTotalExpenseUpToThisDayOfMinistry($ministryId, $expenseHeadId);
        $totalAdvance = $this->getTotalAdvanceOfMinistry($ministryId, $expenseHeadId);
        return $totalExpenseWithOutAdvance = (float)$totalExpense - (float)$totalAdvance;
    }

    public function getRemainBudget($ministryId, $expenseHeadId)
    {

        $totalBudget = $this->getLastBudgetOfMinistry($ministryId, $expenseHeadId);
        $totalExpense = $this->getTotalExpenseUpToThisDayOfMinistry($ministryId, $expenseHeadId);
        return $remainBudget = (float)$totalBudget - (float)$totalExpense;
    }


//    Office and Budget Sub Head wise

    public function getLastBudgetOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        return $last_budget = DB::table('budget')
            ->where('office_id', $office_id)
            ->where('budget_sub_head', $budget_sub_head_id)
            ->where('expense_head_id', $expense_head_id)
            ->sum('total_budget');


    }

    public function getTotalExpenseOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpToYesterdayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {
        $ldate = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');

    }

    public function getTotalExpenseTodayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpToThisDayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalAdvanceOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('dr_amount');
    }

    public function expenseWithOutAdvanceOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {


        $totalExpense = $this->getTotalExpenseUpToThisDayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id);
        $totalAdvance = $this->getTotalAdvanceOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id);
        return $totalExpenseWithOutAdvance = (float)$totalExpense - (float)$totalAdvance;
    }

    public function getRemainBudgetOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        $totalBudget = $this->getLastBudgetOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id);
        $totalExpense = $this->getTotalExpenseUpToThisDayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id);
        return $remainBudget = (float)$totalBudget - (float)$totalExpense;
    }


//    Activity Wise Budget and expense report By Ministry
    public function firstChaumasikExpense($office_id, $budgetSubHead, $activityId)
    {
        $budgetDetails = BudgetDetails::where('budget_id',$activityId)->get();
        $thisMonthExp = 0;
        foreach ($budgetDetails as $budgetDetail){
            $thisMonthExp = $thisMonthExp + ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
                    ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
                    ->where('vouchers.status', '=', 1)
                    ->where('activity_report.office_id', $office_id)
                    ->where('activity_report.budget_sub_head_id', $budgetSubHead)
                    ->where('activity_report_details.activity_id',$budgetDetail->id)
                    ->whereBetween('activity_report_details.month',array(1,4))
                    ->sum('activity_report_details.expense');
        }
        return $thisMonthExp;
    }

    public function secondChaumasikExpense($office_id, $budgetSubHead, $activityId)
    {
        $budgetDetails = BudgetDetails::where('budget_id',$activityId)->get();
        $thisMonthExp = 0;
        foreach ($budgetDetails as $budgetDetail){
            $thisMonthExp = $thisMonthExp + ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
                    ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
                    ->where('vouchers.status', '=', 1)
                    ->where('activity_report.office_id', $office_id)
                    ->where('activity_report.budget_sub_head_id', $budgetSubHead)
                    ->where('activity_report_details.activity_id',$budgetDetail->id)
                    ->whereBetween('activity_report_details.month',array(5,8))
                    ->sum('activity_report_details.expense');
        }
        return $thisMonthExp;
    }

    public function thirdChaumasikExpense($office_id, $budgetSubHead, $activityId)
    {
        $budgetDetails = BudgetDetails::where('budget_id',$activityId)->get();
        $thisMonthExp = 0;
        foreach ($budgetDetails as $budgetDetail){
            $thisMonthExp = $thisMonthExp + ActivityReportDetail::join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
                ->join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
                ->where('vouchers.status', '=', 1)
                ->where('activity_report.office_id', $office_id)
                ->where('activity_report.budget_sub_head_id', $budgetSubHead)
                ->where('activity_report_details.activity_id',$budgetDetail->id)
                ->whereBetween('activity_report_details.month',array(9,12))
                ->sum('activity_report_details.expense');
        }
        return $thisMonthExp;
    }


    public function totalExpenseByActivity($office_id, $budgetSubHead, $activityId)
    {
        $firstChaumasikExpense = $this->firstChaumasikExpense($office_id, $budgetSubHead, $activityId);
        $secondChaumasikExpense = $this->secondChaumasikExpense($office_id, $budgetSubHead, $activityId);
        $thirdChaumasikExpense = $this->thirdChaumasikExpense($office_id, $budgetSubHead, $activityId);
        return $totalExpnes = (float)$firstChaumasikExpense + (float)$secondChaumasikExpense + (float)$thirdChaumasikExpense;
    }


//-------------------------------------------

    public function getInitialBudget($officeId, $fiscalYear,$budgetSubHeadId, $expneseHeadId)
    {
        return $initialBudget = Budget::
        where('office_id', $officeId)
            ->where('akhtiyari_type', '=',1)
            ->where('fiscal_year', $fiscalYear)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->where('expense_head_id', $expneseHeadId)
            ->sum('total_budget');
    }

    public function getAddBudget($officeId, $fiscalYear,$budgetSubHeadId, $expneseHeadId)
    {
        return $initialBudget = Budget::
        where('office_id', $officeId)
            ->where('akhtiyari_type', '=',2)
            ->where('fiscal_year', $fiscalYear)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->where('expense_head_id', $expneseHeadId)
            ->sum('total_budget');
    }

    public function getReduceBudget($officeId, $fiscalYear,$budgetSubHeadId, $expneseHeadId)
    {
        return $initialBudget = Budget::
        where('office_id', $officeId)
            ->where('akhtiyari_type', '=',3)
            ->where('fiscal_year', $fiscalYear)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->where('expense_head_id', $expneseHeadId)
            ->sum('total_budget');
    }

    public function getFinalBudget($officeId, $fiscalYear,$budgetSubHeadId, $expneseHeadId)
    {
        return $initialBudget = Budget::
        where('office_id', $officeId)
            ->where('fiscal_year', $fiscalYear)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->where('expense_head_id', $expneseHeadId)
            ->sum('total_budget');
    }

    public function getTotalExpense($officeId, $budgetSubHeadId, $expneseHeadId)
    {
        return $initialBudget = VoucherDetail::join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('dr_or_cr', 1)
            ->where('ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function thisMonthTotalExpense($officeId, $budgetSubHeadId, $expneseHeadId, $month)
    {
        return $initialBudget = VoucherDetail::
        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('voucher_details.month', $month)
            ->where('dr_or_cr', 1)
            ->where('ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function thisMonthTotalExp($office_id,$fiscal_year,$budget_sub_head_id,$expenseHeadId,$month){

        $expenseHeads = ExpenseHead::where('id',$expenseHeadId)->where('status','=',1)->first();
        return $vouchers = ActivityReportDetail::join('vouchers', 'activity_report_details.voucher_id', '=', 'vouchers.id')
            ->join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->where('vouchers.status', '=',1)
            ->where('activity_report.budget_sub_head_id', $budget_sub_head_id)
            ->where('activity_report_details.fiscal_year', $fiscal_year)
            ->where('activity_report_details.expense_head', $expenseHeads->expense_head_code)
            ->where('activity_report_details.month', '=', $month)
            ->sum('activity_report_details.expense');

    }

    public function preMonthTotalExpense($officeId,$fiscalYear ,$budgetSubHeadId, $expneseHeadId, $month)
    {
        return $vouchers = ActivityReportDetail::join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
            ->join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->where('vouchers.status', 1)
            ->where('activity_report.budget_sub_head_id', $budgetSubHeadId)
            ->where('activity_report_details.fiscal_year', $fiscalYear)
            ->where('activity_report_details.expense_head', $this->expense_head)
            ->Where('activity_report_details.month', '<=', $month)
            ->sum('activity_report_details.expense');

//        return $initialBudget = VoucherDetail::
//        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
//            ->select('voucher_details.*')
//            ->where('voucher_details.office_id', $officeId)
//            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
//            ->where('vouchers.status', 1)
//            ->where('voucher_details.expense_head_id', $expneseHeadId)
//            ->where('voucher_details.month', '<=', $month)
//            ->where('dr_or_cr', 1)
//            ->where('ledger_type_id', ['1', '4'])
//            ->sum('dr_amount');
    }

    public function totalExpense($officeId,$fiscalYear ,$budgetSubHeadId, $expneseHeadId, $month)
    {
        return $vouchers = ActivityReportDetail::join('vouchers','activity_report_details.voucher_id','=','vouchers.id')
            ->join('activity_report', 'activity_report_details.activity_id', '=', 'activity_report.activity_id')
            ->where('vouchers.status', '=',1)
            ->where('activity_report.budget_sub_head_id', $budgetSubHeadId)
            ->where('activity_report_details.fiscal_year', $fiscalYear)
            ->where('activity_report_details.expense_head', $this->expense_head)
            ->Where('activity_report_details.month', '<=', $month)
            ->sum('activity_report_details.expense');

//        return $initialBudget = VoucherDetail::
//        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
//            ->select('voucher_details.*')
//            ->where('voucher_details.office_id', $officeId)
//            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
//            ->where('vouchers.status', 1)
//            ->where('voucher_details.expense_head_id', $expneseHeadId)
//            ->where('voucher_details.month', '<=', $month)
//            ->where('dr_or_cr', 1)
//            ->where('ledger_type_id', ['1', '4'])
//            ->sum('dr_amount');
    }

    public function totalRemainBudget($officeId,$fiscalYear ,$budgetSubHeadId, $expneseHeadId, $month)
    {
        $totalBudget = $this->getFinalBudget($officeId, $fiscalYear,$budgetSubHeadId, $expneseHeadId);
        $totalExpense = $this->totalExpense($officeId, $fiscalYear,$budgetSubHeadId, $expneseHeadId, $month);
        return $remainBudget = (float)$totalBudget - (float)$totalExpense;
    }

    public function thisMonthNikasa($officeId, $fiscalYear,$budgetSubHeadId, $expneseHeadId, $month)
    {
        return $initialBudget = VoucherDetail::
        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.fiscal_year', $fiscalYear)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('vouchers.month', $month)
            ->where('dr_or_cr','=' ,2)
            ->where('ledger_type_id', '=',8)
            ->sum('cr_amount');
    }

    public function upToPreMonthNikasa($officeId,$fiscalYear ,$budgetSubHeadId, $expneseHeadId, $month)
    {

        return $initialBudget = VoucherDetail::
        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.fiscal_year', $fiscalYear)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('vouchers.month', '<=', $month)
            ->where('dr_or_cr', '=',2)
            ->where('ledger_type_id', '=',8)
            ->sum('cr_amount');
    }

    public function totalNikasa($officeId, $fiscalYear,$budgetSubHeadId, $expneseHeadId, $month)
    {

        return $initialBudget = VoucherDetail::
        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.fiscal_year', $fiscalYear)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('vouchers.month', '<=', $month)
            ->where('dr_or_cr', '=',2)
            ->where('ledger_type_id', '=','8')
            ->sum('cr_amount');
    }

//    nikasa for budget sheet

    public function get_nikasa_by_budget_expense_head_id($voucherId,$expenseHeadId){


        $cr_amount = VoucherDetail::where('journel_id',$voucherId)
            ->where('expense_head_id', $expenseHeadId)
            ->where('dr_or_cr',2)
            ->where('ledger_type_id', 8)
            ->sum('cr_amount');
        return $cr_amount;
    }

//    contenjency expense


    public function totalActivityExpense(){

       return $expens = Budget::join('budget_details','budget.id','=','budget_details.budget_id')
            ->join('activity_report','budget_details.id','=','activity_report.activity_id')
            ->where('budget.id','=',$this->id)->sum('activity_report.expense');

    }

    public function totalActivityRemain(){

       return $expens = Budget::join('budget_details','budget.id','=','budget_details.budget_id')
            ->join('activity_report','budget_details.id','=','activity_report.activity_id')
            ->where('budget.id','=',$this->id)->sum('activity_report.remain');

    }

}
