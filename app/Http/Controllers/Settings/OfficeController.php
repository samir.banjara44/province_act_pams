<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\District;
use App\Models\Office;
use App\Models\Ministry;
use App\Models\Mof;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;

class OfficeController extends Controller
{

    private $homeRepo;

    public function __construct(HomeRepository $homeRepo)
    {

        $this->middleware('auth');
        $this->homeRepo = $homeRepo;
    }

    public function index()
    {
        $offices = Office::all();
        return view('backend.settings.office.index', compact('offices'));
    }

    public function create()
    {
        $provinces = Province::all();
        $mofs = Mof::all();
        $ministries = Ministry::all();
        $departments = Department::all();
        $districts = District::all();
        return view('backend.settings.office.create', compact('provinces', 'mofs', 'ministries', 'departments','districts'));
    }

    public function store(Request $request)
    {
        $office = new Office();
        $office->name = $request->name;
        $office->province_id = $request->province_id;
        $office->mof_id = $request->mof_id;
        $office->ministry_id = $request->ministry_id;
        $office->department_id = $request->department_id;
        $office->district_id = $request->district_id;
        $office->office_code = $request->office_code;
        $office->status = 1;
        $office->save();
        return redirect(route('admin.office'));
    }


}
