
@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        पेश्की/भुक्तानी पाउने/धरौटी व्यक्तिगत खाता
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('admin.AdvanceAndPayment.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Create</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>प्रकार</th>
              <th>नाम नेपाली</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($advanceAndPayments as $advanceAndPayment)
              <tr>
                <td>
                  {{$advanceAndPayment->party_type}}
                </td>
                <td>{{$advanceAndPayment->name_nep}}</td>

                <td>
                  <a type="button" href="{{route('admin.AdvanceAndPayment.edit',$advanceAndPayment->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection