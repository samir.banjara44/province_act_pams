<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert([
            ["name" => "N/A"],
            ["name" => "जना"],
            ["name" => "गोटा"],
            ["name" => "पटक"],
            ["name" => "सङ्ख्या"],
            ["name" => "टोलि"],
            ["name" => "परिवार"],
            ["name" => "थान"],
            ["name" => "सेट"],
            ["name" => "महिना"],
            ["name" => "शिक्षक"],
            ["name" => "विषय"],
            ["name" => "लाईन"],
            ["name" => "दिन"],
            ["name" => "जिन्ला"],
            ["name" => "क्षेत्र"],
            ["name" => "कोठा"],
            ["name" => "कक्षा"],
            ["name" => "मिटर"],
            ["name" => "लिटर"],
            ["name" => "ब.मिटर"],
            ["name" => "मेट्रिक टन"],
            ["name" => "घन मिटर"],
            ["name" => "रोपनी"],
            ["name" => "हेक्टर"],

        ]);
    }
}

