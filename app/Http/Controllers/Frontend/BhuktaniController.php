<?php

namespace App\Http\Controllers\Frontend;

use App\helpers\FiscalYearHelper;
use App\Http\Controllers\Controller;
use App\Models\Bhuktani;
use App\Models\PreBhuktani;
use App\Models\Programs;
use App\Models\Voucher;
use App\Repositories\BhuktaniRepository;
use App\Repositories\PreBhuktaniRepository;
use App\Repositories\ProgramRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class BhuktaniController extends Controller
{
    private $bankRepo;
    private $programRepo;
    private $preBhuktaniRepo;
    private $bhuktaniRepo;


    public function __construct(
        BhuktaniRepository $bankRepo,
        ProgramRepository $programRepo,
        PreBhuktaniRepository $preBhuktaniRepo,
        BhuktaniRepository $bhuktaniRepo
    )
    {

        $this->middleware('auth');
        $this->bankRepo = $bankRepo;
        $this->programRepo = $programRepo;
        $this->preBhuktaniRepo = $preBhuktaniRepo;
        $this->bhuktaniRepo = $bhuktaniRepo;
        $this->fiscalYearHelper = new FiscalYearHelper();

    }

//    public function bhuktaniParam

    public function preIndex()
    {
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_voucher();
        return view('frontend.bhuktani.pre_index', compact('programs','fiscalYear','fiscalYears'));
    }

    public function index(Request $request)
    {
        $data = $request->all();
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($data['fiscal_year']);
        $fiscalYears = $this->fiscalYearHelper->get_fiscal_year();
        $office_id = Auth::user()->office_id;
        $programs = $this->programRepo->get_by_program_id($data['budget_sub_head']);
        $bhuktanies = Bhuktani::where('office_id',$office_id)->where('fiscal_year','=',$data['fiscal_year'])->where('budget_sub_head',$data['budget_sub_head'])->orderBy('adesh_number','desc')->get();
        $totalBhuktaniAmount = $bhuktanies->sum('amount');
        return view('frontend.bhuktani.index', compact('bhuktanies','program_id','programs','fiscalYear','data','fiscalYears','totalBhuktaniAmount'));
    }
    public function indexBack($fiscal_year, $budget_sub_head)
    {
        $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($fiscal_year);
        $office_id = Auth::user()->office_id;
        $programs = $this->programRepo->get_by_program_id($budget_sub_head);
        $bhuktanies = Bhuktani::where('office_id',$office_id)->where('fiscal_year','=',$fiscal_year)->where('budget_sub_head',$budget_sub_head)->orderBy('adesh_number','desc')->get();
        $totalBhuktaniAmount = $bhuktanies->sum('amount');
        return view('frontend.bhuktani.index', compact('bhuktanies','program_id','programs','fiscalYear','data','totalBhuktaniAmount'));
    }

    public function create(Request $request){
        try{
            $data = $request->all();
            $fiscalYear = $this->fiscalYearHelper->get_fiscal_by_id($data['fiscal_year']);
            $program = $this->programRepo->get_by_program_id($data['budget_sub_head']);
            $bhuktani_adesh_number = $this->bhuktaniRepo->get_bhuktani_adesh_number_by_program($data['fiscal_year'],$data['budget_sub_head']);
            $preBhuktanies = $this->preBhuktaniRepo->get_by_program($data['fiscal_year'],$data['budget_sub_head']);
            $vouchers = Voucher::where('budget_sub_head_id',$data['budget_sub_head']);
            return view('frontend.bhuktani.create',compact('fiscalYear','program','preBhuktanies','bhuktani_adesh_number'));

        } catch (\Exception $e){
            $message = "डाटा प्रविष्टि भएको छैन!";
            return redirect()->back()->with('error', $message);
        }
    }

    public function store(Request $request){

        $request['totalAmount'] = $this->preBhuktaniRepo->get_amount_by_multiple_id($request['bhuktani']);
        $bhuktani_id = $this->bhuktaniRepo->create($request->all());
        foreach ($request['bhuktani'] as $pre_bhuktani){
           $this->preBhuktaniRepo->update_status_and_set_bhuktani_id($bhuktani_id,$pre_bhuktani);
       }
        return redirect()->route('bhuktani.index.back',[$request['fiscal_year'],$request['budget_sub_head']]);
    }

    public function get_bhuktani(Request $request){

        $parameter = $request->all();
        $bhuktani = $this->bhuktaniRepo->get_by_budget_sub_head_and_other($parameter);
        return json_encode($bhuktani);


    }

    public function bhuktani_delete($bhuktani_id){
       $bhuktaniDelete =  $this->bhuktaniRepo->delete($bhuktani_id);
        if($bhuktaniDelete){
            return json_encode('true');
        }
        else {
            return json_encode('false');
        }

    }

}
