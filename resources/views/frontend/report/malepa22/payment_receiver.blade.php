<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">
<style>
    table {
        border-collapse: collapse;
        font-size: 13px;
    }

</style>
<div class="content-wrapper">
    <section class="content-header">
        <table class="kalimati" width="99%">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                <td colspan="6" style="text-align: center">@if(Auth::user()->office->id != 43)प्रदेश सरकार@else प्रदेश
                    सभा @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6"
                    style="text-align: center">@if(Auth::user()->office->ministry){{Auth::user()->office->ministry->name}} @endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    @if(Auth::user()->office->department)
                        {{Auth::user()->office->department->name}}
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->name}},
                    , {{Auth::user()->office->district->name}}</td>
            </tr>
            <tr>
                <td style="text-align: right; padding-right: 12px">म.ले.प.फा.नं. २०७</td>
            </tr>

            <tr style="float: left">
                <td class="kalimati" style="padding-left: 12px;">आ.व. : {{$fiscalYear->year}}</td>
            </tr>
            <tr>
                <td class="kalimati" style="padding-left: 12px;">खाता : {{$party_name}}</td>
            </tr>

        </table>
    </section>
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table width="100%"  border="1">
                    <thead>
                    <tr style="background-color:#dbdbdb; font-size: 12px">
                        <th>सि.न.</th>
                        <th>भौचर न.</th>
                        <th>मिति</th>
                        <th>कारोवारको व्यहोरा</th>
                        <th>भुक्तानी रकम</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($preBhuktaniList as $index=>$preBhuktani)
                        <tr style="background-color: white;">
                            <td class="kalimati" align="center">{{++$index}}</td>
                            <td align="center"><a href="{{route('voucher.view',$preBhuktani->voucher->id)}}" target="_blank" class="kalimati">{{$preBhuktani->voucher->jv_number}}</a></td>
                            <td class="kalimati" align="center">{{$preBhuktani->voucher->data_nepali}}</td>
                            <td class="kalimati" align="left">{{$preBhuktani->voucher->short_narration}}</td>
                            <td class="kalimati" align="right">{{number_format($preBhuktani->amount,2)}}</td>
                        </tr>
                    @endforeach
                    <tr style="background-color:#dbdbdb">
                        <td colspan="4" align="right">जम्मा</td>
                        <td class="kalimati" align="right">{{number_format($total_bhuktani,2)}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
