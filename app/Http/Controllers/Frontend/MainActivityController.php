<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\MainActivity;
use App\Models\Area;
use App\Models\SubArea;
use App\Repositories\MainActivityRepository;
use App\Repositories\MainProgramRepository;
use App\Repositories\AreaRepository;
use App\Repositories\SubAreaRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainActivityController extends Controller
{

    private $mainProgramRepo;
    private $areaRepo;
    private $subAreaRepo;
    private $mainActivityRepo;

    public function __construct(
        areaRepository $areaRepo,
        subAreaRepository $SubAreaRepo,
        MainProgramRepository $mainProgramRepo,
        MainActivityRepository $mainActivityRepo
    )
    {
        $this->middleware('auth');

        $this->mainActivityRepo = $mainActivityRepo;
    }


    public function create(){

        $areas = Area::all();
        $subAreas = SubArea::all();

        return view('backend.settings.main_program.create',compact('areas','subAreas'));
    }
    public function store(Request $request){
        $program = $this->mainActivityRepo->create($request->all());
        return json_encode($program);
    }

    public function get_general_activity_by_main_program_and_office_id(Request $request,$main_program_id) {
        $office_id = Auth::user()->office->id;
        $mainActivities = $this->mainActivityRepo->get_by_main_program_and_office($main_program_id,$office_id);
        return json_encode($mainActivities);
    }



}
